#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include <cmath>

#include "VisionShared.h"
#include "VisionUtils.h"
#include "VisionIface.h"
#include "TinyXML/tinyxml.h"

#if _WIN32
#include <io.h>
#include <direct.h>
#define access _access
#endif

#if __linux
#include <sys/stat.h> // for mkdir
#include <dirent.h>
#include <errno.h>
#endif

#include <utility>
using std::swap;

using std::min;
using std::max;

float V_cosd(float theta) {
	theta = (float)((theta*2*M_PI)/360.0);
	return cos(theta);
}

float V_sind(float theta) {
	theta = (float)((theta*2*M_PI)/360.0);
	return sin(theta);
}

bool VI_isPower(int n,int p) {
	ASSERT( n > 0 );
	while(n != 1) {
		if(n % p != 0)
			return false;
		n /= p;
	}
	return true;
}

int VI_getNextPower(int n,int p) {
	ASSERT( n > 0 );
	int v = 1;
	while( v < n )
		v *= p;
	return v;
}

/* For examples of a vector class with operator overloading, see:
* - http://www.gamasutra.com/features/19990702/data_structures_02.htm
* - OpenGL Game Programming (Hawkins, Astle) pages 653-659.
*/
Vector3D Vector3D::forwards(0,0,-1);
Vector3D Vector3D::backwards(0,0,1);
Vector3D Vector3D::right(1,0,0);
Vector3D Vector3D::left(-1,0,0);
Vector3D Vector3D::upwards(0,1,0);
Vector3D Vector3D::downwards(0,-1,0);

//const float V_TOL = 0.00000001;

Vector3D Vector3D::perpendicular() const {
	float mag = magnitude();
	if( mag < V_TOL_LINEAR ) {
		return *this;
	}
	Vector3D min_axis = Vector3D(1.0f, 0.0f, 0.0f);
	float min_comp = this->x;
	if( this->y < min_comp ) {
		min_axis = Vector3D(0.0f, 1.0f, 0.0f);
		min_comp = this->y;
	}
	if( this->z < min_comp ) {
		min_axis = Vector3D(0.0f, 0.0f, 1.0f);
		min_comp = this->z;
	}
	return min_axis ^ *this;
}

void Vector3D::rotateBy(const Rotation3D &r) {
	Matrix mat;
	mat.setRotation(r);
	mat.transformVector(this);
}

void Vector3D::rotateBy(float x,float y,float z) {
	Matrix mat;
	Rotation3D r;
	r.rotateToEuler(x,y,z);
	mat.setRotation(r);
	mat.transformVector(this);
}

/* Drops the vector onto a line of direction 'n' that passes through 'o'.
* 'n' must be normalised.
* Formula: nv = ((v - o) DOT n) n + o
*/
void Vector3D::dropOnLine(const Vector3D &o,const Vector3D &n) {
	this->subtract(o);
	float dot = this->x * n.x + this->y * n.y + this->z * n.z;
	this->x = dot * n.x;
	this->y = dot * n.y;
	this->z = dot * n.z;
	this->add(o);
}

void Vector3D::parallelComp(const Vector3D &n) {
	Vector3D zero(0.0,0.0,0.0);
	this->dropOnLine(zero,n);
}

void Vector3D::perpComp(const Vector3D &n) {
	Vector3D zero(0.0,0.0,0.0);
	Vector3D v = *this;
	v.dropOnLine(zero,n);
	this->subtract(v);
}

float Vector3D::distFromLine(const Vector3D &o,const Vector3D &n) const {
	Vector3D nv(this->x,this->y,this->z);
	nv.dropOnLine(o,n);
	nv.subtract(*this);
	float dist = sqrt( nv.square() );
	return dist;
}

float Vector3D::distFromLineSq(const Vector3D &o,const Vector3D &n) const {
	Vector3D nv(this->x,this->y,this->z);
	nv.dropOnLine(o,n);
	nv.subtract(*this);
	float dist = nv.square();
	return dist;
}

Vector3D Vector3D::closestOnLine(const Vector3D &a,const Vector3D &b) const {
	Vector3D c(this->x,this->y,this->z);
	c -= a;
	Vector3D v = b - a;
	float d = v.magnitude();
	v /= d;
	float t = v.dot(c);
	if(t < 0)
		return a;
	if(t > d)
		return b;
	v.scale(t);
	v.add(a);
	return v;
}

/* Returns true iff the vector is in the polygon defined by 'points'. The
* following conditions, which are NOT checked by the function, should be true:
* - The 'points' should be coplanar.
* - The Vector3D (ie, 'this') should be coplanar with the polygon.
*/
bool Vector3D::pointInPolygon(const Vector3D *points,int n_points) const {
	bool inside = true;
	Vector3D centre(points[0]);
	int i=0;
	for(i=1;i<n_points;i++)
		centre.add(points[i]);
	centre.scale( 1.0f / (float)n_points );

	// test against each edge
	for(i=0;i<n_points;i++) {
		const Vector3D *p0 = &points[i];
		const Vector3D *p1 = &points[i==(n_points-1) ? 0 : i+1];
		Vector3D n = *p1;
		n.subtract(*p0);
		n.normalise();
		// n is now a unit vector in direction of the edge
		Vector3D perp(*this);
		perp.subtract(*p0);
		perp.perpComp(n);

		Vector3D perpC = centre;
		perpC.subtract(*p0);
		perpC.perpComp(n);

		float p_sq = perp.square();
		if(p_sq < V_TOL_LINEAR) {
			// within radius of boundary
			continue;
		}
		if(perpC.dot(perp) < 0.0) {
			inside = false; // opposite side of edge to centre
			break;
		}
	}
	return inside;
}

/* Find the point on the boundary on the polygon defined by 'points' that is
* closest to the 'this' vector (which may be inside or outside the polygon).
*/
Vector3D Vector3D::closestPointOnPolygon(const Vector3D *points,int n_points) const {
	const Vector3D *p0 = &points[n_points-1];
	const Vector3D *p1 = &points[0];
	Vector3D closest_pt = this->closestOnLine(*p0, *p1);
	Vector3D thisvec(this->x,this->y,this->z);
	Vector3D diff(closest_pt - thisvec);
	float closest_dist_sq = diff.square();
	// test against each edge
	for(int i=0;i<n_points-1;i++) {
		p0 = &points[i];
		p1 = &points[i+1];
		Vector3D pt = this->closestOnLine(*p0, *p1);
		diff = pt - thisvec;
		const float dist_sq = diff.square();
		if(dist_sq < closest_dist_sq) {
			closest_pt = pt;
			closest_dist_sq = dist_sq;
		}
	}
	return closest_pt;
}

/* Intersect a swept sphere with a plane.
* 'a', 'b' are the bounding points of the sweep.
* 'r' is the radius of the sphere.
* 'n' is the normal of the plane (must be a unit vector).
* 'D' is the distance of the plane (ie, p.n, where p is a point on the plane).
*
* If an intersection occurs, the function will try to split the sweep range
* into two, such that each new swept sphere contains the volume occupied by
* what the result if the plane had split the swept sphere exactly.
* In some cases (where the angle between the sweep direction and the plane is
* too shallow), the sweep range cannot be split - we have to stay with the
* sweep range we currently have.
*
* Returns:
* true iff an intersection occurs.
* 'full' is set to true iff the sweep range cannot be split, as described
* above.
* 'intersection1' is the end point of the first split range, 'intersection2'
* the start point of the second split range. These are only relevant if 'full'
* is true, otherwise they are undefined.
*/
bool Vector3D::intersectSweptSphereWithPlane(bool *full,Vector3D *intersection1,Vector3D *intersection2,const Vector3D &a,const Vector3D &b,float r,const Vector3D &n,float D) {
	float an = a.dot(n);
	float bn = b.dot(n);
	/**full = true;
	return true;*/
	if(an * bn > V_TOL_LINEAR) {
		/*if( abs(an - D) > r + TOL )
		return false;
		if( abs(bn - D) > r + TOL )
		return false;*/
		if( abs(an - D) > r + V_TOL_LINEAR && abs(bn - D) > r + V_TOL_LINEAR )
			return false;
	}
	// bah - for now just do a 'full' intersection..
	*full = true;
	return true;

	Vector3D c(b);
	c.subtract(a);
	c.normalise();
	float costheta = abs( c.dot(n) );
	float sintheta = sqrt(1.0f - costheta*costheta);
	if(costheta <= V_TOL_LINEAR) {
		/*float h = abs( r * sintheta );
		if( abs(an - D) - h <= TOL ) {
		*full = true;
		return true;
		}
		if( abs(bn - D) - h <= TOL ) {
		*full = true;
		return true;
		}
		return false;*/
		*full = true;
		return true;
	}
	float oneovercos = 1.0f / costheta;
	float p = (an - D) * oneovercos;
	float m = r * sintheta * oneovercos;
	Vector3D l(b);
	l.subtract(a);
	float length = l.magnitude();
	if( p + m < - r - V_TOL_LINEAR || p - m > length + r + V_TOL_LINEAR ) {
		// shouldn't happen!
		return false;
	}
	*full = false;
	float i1 = min( p + m, length );
	i1 = max( V_TOL_LINEAR, p + m );
	float i2 = max( p - m, 0.0f );
	i2 = min( length - V_TOL_LINEAR, p - m );
	*intersection1 = b;
	intersection1->subtract(a);
	*intersection2 = *intersection1;

	intersection1->scale(i1);
	intersection2->scale(i2);

	intersection1->add(a);
	intersection2->add(a);

	return true;
}

void Sphere::expand(const Sphere &sphere) {
	if( this->radius == 0.0f ) {
		*this = sphere;
	}
	else if( sphere.radius != 0.0f ) {
		/*float dist = (sphere.centre - this->centre).magnitude() + sphere.radius;
		if( dist > radius ) {
			this->setRadius(dist);
		}*/
		// re-centre between the two spheres
		Vector3D diff = sphere.centre - this->centre;
		float length = diff.magnitude();
		if( length < V_TOL_LINEAR ) {
			// coi spheres
			if( sphere.radius > this->radius ) {
				this->setRadius(sphere.radius);
			}
		}
		else {
			diff /= length;
			float sphere_top = length + sphere.radius;
			float sphere_bottom = length - sphere.radius;
			float this_top = this->radius;
			float this_bottom = - this->radius;
			float new_top = max(sphere_top, this_top);
			float new_bottom = min(sphere_bottom, this_bottom);
			this->centre += diff * 0.5f * ( new_bottom + new_top );
			this->setRadius( 0.5f * ( new_top - new_bottom ) );
		}
		/*else if( length <= this->radius && length <= sphere.radius ) {
			// one sphere inside the other
			if( sphere.radius > this->radius ) {
				this->setRadius(sphere.radius);
				this->centre = sphere.centre;
			}
			// else no change
		}
		else {
			diff /= length;
			length += this->radius + sphere.radius; // total length encompassing both spheres
			length *= 0.5; // want midpoint
			this->centre += diff * ( length - this->radius ); // move centre to midpoint
			this->radius = length;
		}*/
	}
}


void Box::getAxes(Vector3D *x_axis, Vector3D *y_axis, Vector3D *z_axis) const {
	Vector3D aabb_X = getX();
	Vector3D aabb_Y = getY();
	Vector3D aabb_Z = getZ();
	float len_X = aabb_X.magnitude();
	float len_Y = aabb_Y.magnitude();
	float len_Z = aabb_Z.magnitude();
	// cope with "flat" objects
	if( len_X < V_TOL_LINEAR && len_Y < V_TOL_LINEAR && len_Z < V_TOL_LINEAR ) {
		aabb_X = Vector3D(1.0f, 0.0f, 0.0f);
		aabb_Y = Vector3D(0.0f, 1.0f, 0.0f);
		aabb_Z = Vector3D(0.0f, 0.0f, 1.0f);
	}
	else if( len_X < V_TOL_LINEAR && len_Y < V_TOL_LINEAR ) {
		ASSERT( len_Z >= V_TOL_LINEAR );
		aabb_Z /= len_Z;
		aabb_X = aabb_Z.perpendicular();
		aabb_Y = aabb_Z ^ aabb_X;
	}
	else if( len_Y < V_TOL_LINEAR && len_Z < V_TOL_LINEAR ) {
		ASSERT( len_X >= V_TOL_LINEAR );
		aabb_X /= len_X;
		aabb_Y = aabb_X.perpendicular();
		aabb_Z = aabb_X ^ aabb_Y;
	}
	else if( len_Z < V_TOL_LINEAR && len_X < V_TOL_LINEAR ) {
		ASSERT( len_Y >= V_TOL_LINEAR );
		aabb_Y /= len_Y;
		aabb_Z = aabb_Y.perpendicular();
		aabb_X = aabb_Y ^ aabb_Z;
	}
	else if( len_X < V_TOL_LINEAR ) {
		ASSERT( len_Y >= V_TOL_LINEAR && len_Z >= V_TOL_LINEAR );
		aabb_Y /= len_Y;
		aabb_Z /= len_Z;
		aabb_X = aabb_Y ^ aabb_Z;
	}
	else if( len_Y < V_TOL_LINEAR ) {
		ASSERT( len_Z >= V_TOL_LINEAR && len_X >= V_TOL_LINEAR );
		aabb_Z /= len_Z;
		aabb_X /= len_X;
		aabb_Y = aabb_Z ^ aabb_X;
	}
	else if( len_Z < V_TOL_LINEAR ) {
		ASSERT( len_X >= V_TOL_LINEAR && len_Y >= V_TOL_LINEAR );
		aabb_X /= len_X;
		aabb_Y /= len_Y;
		aabb_Z = aabb_X ^ aabb_Y;
	}
	else {
		aabb_X /= len_X;
		aabb_Y /= len_Y;
		aabb_Z /= len_Z;
	}
	*x_axis = aabb_X;
	*y_axis = aabb_Y;
	*z_axis = aabb_Z;
}

void Box::expand(float distance) {
	Vector3D aabb_X;
	Vector3D aabb_Y;
	Vector3D aabb_Z;
	getAxes(&aabb_X, &aabb_Y, &aabb_Z);
	aabb_X *= distance;
	aabb_Y *= distance;
	aabb_Z *= distance;

	corners[0] -= aabb_X;
	corners[3] -= aabb_X;
	corners[4] -= aabb_X;
	corners[6] -= aabb_X;
	corners[1] += aabb_X;
	corners[2] += aabb_X;
	corners[5] += aabb_X;
	corners[7] += aabb_X;

	corners[0] -= aabb_Y;
	corners[1] -= aabb_Y;
	corners[4] -= aabb_Y;
	corners[5] -= aabb_Y;
	corners[2] += aabb_Y;
	corners[3] += aabb_Y;
	corners[6] += aabb_Y;
	corners[7] += aabb_Y;

	corners[0] -= aabb_Z;
	corners[1] -= aabb_Z;
	corners[2] -= aabb_Z;
	corners[3] -= aabb_Z;
	corners[4] += aabb_Z;
	corners[5] += aabb_Z;
	corners[6] += aabb_Z;
	corners[7] += aabb_Z;
}

bool Box::collisionLine(float *intersection, const Vector3D &pos, const Vector3D &dir) const {
	// Slabs Method
	// see 3D Game Engine Programming, Zerbst, page 140+
	if( intersection != NULL )
		*intersection = 0.0f;
	Vector3D aabb_centre = calculateCentre();
	Vector3D aabb_X = getX();
	Vector3D aabb_Y = getY();
	Vector3D aabb_Z = getZ();
	float len_X = aabb_X.magnitude();
	float len_Y = aabb_Y.magnitude();
	float len_Z = aabb_Z.magnitude();
	// cope with "flat" objects
	if( len_X < V_TOL_LINEAR ) {
		ASSERT( len_Y >= V_TOL_LINEAR && len_Z >= V_TOL_LINEAR );
		aabb_Y /= len_Y;
		aabb_Z /= len_Z;
		aabb_X = aabb_Y ^ aabb_Z;
	}
	else if( len_Y < V_TOL_LINEAR ) {
		ASSERT( len_Z >= V_TOL_LINEAR && len_X >= V_TOL_LINEAR );
		aabb_Z /= len_Z;
		aabb_X /= len_X;
		aabb_Y = aabb_Z ^ aabb_X;
	}
	else if( len_Z < V_TOL_LINEAR ) {
		ASSERT( len_X >= V_TOL_LINEAR && len_Y >= V_TOL_LINEAR );
		aabb_X /= len_X;
		aabb_Y /= len_Y;
		aabb_Z = aabb_X ^ aabb_Y;
	}
	else {
		aabb_X /= len_X;
		aabb_Y /= len_Y;
		aabb_Z /= len_Z;
	}
	float tmin = - V_TOL_LARGE;
	float tmax = V_TOL_LARGE;
	Vector3D vcP = aabb_centre - pos;
	float e, f, t1, t2;

	// 1
	e = aabb_X % vcP;
	f = aabb_X % dir;
	if( fabs(f) > V_TOL_LINEAR ) {
		//LOG("    1\n");
		t1 = (e + len_X) / f;
		t2 = (e - len_X) / f;
		if( t1 > t2 ) {
			swap(t1, t2);
		}
		if( t1 > tmin )
			tmin = t1;
		if( t2 < tmax )
			tmax = t2;
		if( tmin - tmax > V_TOL_LINEAR ) {
			return false;
		}
		if( tmax < - V_TOL_LINEAR ) {
			return false;
		}
	}
	else {
		// parallel
		if( - e - len_X > V_TOL_LINEAR  || - e + len_X < - V_TOL_LINEAR ) {
			return false;
		}
	}

	// 2
	e = aabb_Y % vcP;
	f = aabb_Y % dir;
	if( fabs(f) > V_TOL_LINEAR ) {
		//LOG("    2\n");
		t1 = (e + len_Y) / f;
		t2 = (e - len_Y) / f;
		if( t1 > t2 ) {
			swap(t1, t2);
		}
		if( t1 > tmin )
			tmin = t1;
		if( t2 < tmax )
			tmax = t2;
		if( tmin - tmax > V_TOL_LINEAR ) {
			return false;
		}
		if( tmax < - V_TOL_LINEAR ) {
			return false;
		}
	}
	else {
		// parallel
		if( - e - len_Y > V_TOL_LINEAR  || - e + len_Y < - V_TOL_LINEAR ) {
			return false;
		}
	}

	// 3
	e = aabb_Z % vcP;
	f = aabb_Z % dir;
	if( fabs(f) > V_TOL_LINEAR ) {
		//LOG("    3\n");
		t1 = (e + len_Z) / f;
		t2 = (e - len_Z) / f;
		if( t1 > t2 ) {
			swap(t1, t2);
		}
		if( t1 > tmin )
			tmin = t1;
		if( t2 < tmax )
			tmax = t2;
		if( tmin - tmax > V_TOL_LINEAR ) {
			return false;
		}
		if( tmax < - V_TOL_LINEAR ) {
			return false;
		}
	}
	else {
		// parallel
		if( - e - len_Z > V_TOL_LINEAR  || - e + len_Z < - V_TOL_LINEAR ) {
			return false;
		}
	}

	if( intersection != NULL ) {
		if( tmin > 0.0f )
			*intersection = tmin;
		else
			*intersection = tmax;
	}
	return true;
}

void Quaternion::setEuler(float rx,float ry,float rz) {
	// x = pitch, y = yaw, z = roll
	float cx = V_cosd(rx / 2.0f);
	float sx = V_sind(rx / 2.0f);
	float cy = V_cosd(ry / 2.0f);
	float sy = V_sind(ry / 2.0f);
	float cz = V_cosd(rz / 2.0f);
	float sz = V_sind(rz / 2.0f);

	this->w = cx * cy * cz + sx * sy * sz;
	this->x = sx * cy * cz - cx * sy * sz;
	this->y = cx * sy * cz + sx * cy * sz;
	this->z = - cx * cy * sz - sx * sy * cz;

	/*Matrix mat;
	mat.setRotationMatrix(rx,ry,rz);
	this->set(&mat);*/
}

void Quaternion::set(Matrix *matrix) {
	float tr = matrix->trace();
	// check the diagonal
	/*if( tr == -1.0 )
	printf("!!!\n");*/
	//if( tr > -1.0 ) {
	if( tr > 0.0 ) {
		float s = sqrt (tr + 1.0f);
		this->w = s / 2.0f;
		s = 0.5f / s;
		this->x = (matrix->getElement(2,1) - matrix->getElement(1,2)) * s;
		this->y = (matrix->getElement(0,2) - matrix->getElement(2,0)) * s;
		this->z = (matrix->getElement(1,0) - matrix->getElement(0,1)) * s;
	} else {
		// diagonal is negative
		int i, j, k;
		int nxt[3] = {1, 2, 0};
		float q[3];
		i = 0;
		if( matrix->getElement(1,1) > matrix->getElement(0,0) )
			i = 1;
		if( matrix->getElement(2,2) > matrix->getElement(i,i) )
			i = 2;
		j = nxt[i];
		k = nxt[j];
		float s = sqrt( ( matrix->getElement(i,i) - ( matrix->getElement(j,j) + matrix->getElement(k,k) ) ) + 1.0f );
		q[i] = 0.5f * s;
		if( s != 0.0 )
			s = 0.5f / s;
		this->w = ( matrix->getElement(j,k) - matrix->getElement(k,j) ) * s;
		q[j] = ( matrix->getElement(i,j) + matrix->getElement(j,i) ) * s;
		q[k] = ( matrix->getElement(i,k) + matrix->getElement(k,i) ) * s;
		this->x = q[0];
		this->y = q[1];
		this->z = q[2];
	}
}

void Quaternion::setAngleAxis(float ax,float ay,float az,float angle) {
	float sin_a = sin( angle / 2 );
	float cos_a = cos( angle / 2 );
	this->x = ax * sin_a;
	this->y = ay * sin_a;
	this->z = az * sin_a;
	this->w = cos_a;
}

/*
* rz is currently unreliable!
*/
void Quaternion::convertToEuler(float *rx,float *ry,float *rz) const {
	/*float Tx = 2.0 * x * z + 2.0 * w * y;
	float Ty = 2.0 * y * z - 2.0 * w * x;
	float Tz = w*w - x*x - y*y + z*z;*/
	double Tx = 2.0 * x * z + 2.0 * w * y;
	double Ty = 2.0 * y * z - 2.0 * w * x;
	double Tz = 1 - 2 * ( x * x + y * y );
	Tx = - Tx;
	Ty = - Ty;
	Tz = - Tz;
	//float Tz = w*w - x*x - y*y + z*z;
	if( Tx == 0 && Tz == 0 )
		*ry = 0;
	else
		*ry = (float)(( atan2( Tx, -Tz ) * 180.0f ) / M_PI);
	//*ry = ( atan2( Tx, Tz ) * 180.0f ) / M_PI;
	double mag = sqrt( Tx*Tx + Tz*Tz );
	*rx = (float)(( atan2( Ty, mag ) * 180.0f ) / M_PI);

	double cx = V_cosd(- (*rx) / 2.0f);
	double sx = V_sind(- (*rx) / 2.0f);
	double cy = V_cosd((*ry) / 2.0f);
	double sy = V_sind((*ry) / 2.0f);
	double A = cx * cy;
	double B = - sx * sy;
	//double C = sx * cy;
	//double D = cx * sy;
	//double E = cx * sy;
	//double F = - sx * cy;
	double G = - sx * sy;
	double H = cx * cy;
	/*float det = A * D - B * C;
	float cz = ( w * D -  x * B ) / det;
	float sz = ( x * A - w * C ) / det;*/
	/*float det = A * F - B * E;
	float cz = ( w * F -  y * B ) / det;
	float sz = ( y * A - w * E ) / det;*/
	double det = A * H - B * G;
	double cz = ( w * H -  z * B ) / det;
	double sz = ( z * A - w * G ) / det;
	if( sz == 0.0 && cz == 0.0 ) {
		//printf("");
		*rz = 0;
	}
	else
		*rz = (float)(- 2.0f * ( atan2( sz, cz ) * 180.0f ) / M_PI);

	//*rx = - (*rx);
	*ry = - (*ry);
	*rz = - (*rz);

	//r->check();
}


void Quaternion::convertToMatrix(IRotationMatrix *matrix) const {
	float xx = x * x;
	float xy = x * y;
	float xz = x * z;
	float xw = x * w;
	float yy = y * y;
	float yz = y * z;
	float yw = y * w;
	float zz = z * z;
	float zw = z * w;

	matrix->setElement(0, 0, 1 - 2 * ( yy + zz ));
	matrix->setElement(0, 1, 2 * ( xy - zw ));
	matrix->setElement(0, 2, 2 * ( xz + yw ));

	matrix->setElement(1, 0, 2 * ( xy + zw ));
	matrix->setElement(1, 1, 1 - 2 * ( xx + zz ));
	matrix->setElement(1, 2, 2 * ( yz - xw ));

	matrix->setElement(2, 0, 2 * ( xz - yw ));
	matrix->setElement(2, 1, 2 * ( yz + xw ));
	matrix->setElement(2, 2, 1 - 2 * ( xx + yy ));
}

void Quaternion::convertToMatrixInverse(IRotationMatrix *matrix) const {
	float xx = x * x;
	float xy = x * y;
	float xz = x * z;
	float xw = x * w;
	float yy = y * y;
	float yz = y * z;
	float yw = y * w;
	float zz = z * z;
	float zw = z * w;

	matrix->setElement(0, 0, 1 - 2 * ( yy + zz ));
	matrix->setElement(1, 0, 2 * ( xy - zw ));
	matrix->setElement(2, 0, 2 * ( xz + yw ));

	matrix->setElement(0, 1, 2 * ( xy + zw ));
	matrix->setElement(1, 1, 1 - 2 * ( xx + zz ));
	matrix->setElement(2, 1, 2 * ( yz - xw ));

	matrix->setElement(0, 2, 2 * ( xz - yw ));
	matrix->setElement(1, 2, 2 * ( yz + xw ));
	matrix->setElement(2, 2, 1 - 2 * ( xx + yy ));
}

/*void Quaternion::convertToGLMatrix(GLfloat glmat[16]) {

double xx = x * x;

double xy = x * y;

double xz = x * z;

double xw = x * w;

double yy = y * y;

double yz = y * z;

double yw = y * w;

double zz = z * z;

double zw = z * w;


glmat[0] = 1 - 2 * ( yy + zz );

glmat[4] = 2 * ( xy - zw );

glmat[8] = 2 * ( xz + yw );


glmat[1] = 2 * ( xy + zw );

glmat[5] = 1 - 2 * ( xx + zz );

glmat[9] = 2 * ( yz - xw );


glmat[2] = 2 * ( xz - yw );

glmat[6] = 2 * ( yz + xw );

glmat[10] = 1 - 2 * ( xx + yy );


glmat[3] = 0;

glmat[7] = 0;
glmat[11] = 0;

glmat[12] = 0;
glmat[13] = 0;
glmat[14] = 0;
glmat[15] = 1;
}*/


/*void Quaternion::convertInverseToGLMatrix(GLfloat glmat[16]) {

double xx = x * x;

double xy = x * y;

double xz = x * z;

double xw = x * w;

double yy = y * y;

double yz = y * z;

double yw = y * w;

double zz = z * z;

double zw = z * w;


glmat[0] = 1 - 2 * ( yy + zz );

glmat[1] = 2 * ( xy - zw );

glmat[2] = 2 * ( xz + yw );


glmat[4] = 2 * ( xy + zw );

glmat[5] = 1 - 2 * ( xx + zz );

glmat[6] = 2 * ( yz - xw );


glmat[8] = 2 * ( xz - yw );

glmat[9] = 2 * ( yz + xw );

glmat[10] = 1 - 2 * ( xx + yy );


glmat[3] = 0;

glmat[7] = 0;
glmat[11] = 0;

glmat[12] = 0;
glmat[13] = 0;
glmat[14] = 0;
glmat[15] = 1;
}*/


Vector3D Quaternion::getHeading() const {

	// TODO: optimise
	Matrix mat;
	this->convertToMatrix(&mat);
	Vector3D dir(0,0,-1);
	mat.transformVector(&dir);
	return dir;
}


Vector3D Quaternion::getUp() const {

	// TODO: optimise
	Matrix mat;
	this->convertToMatrix(&mat);
	Vector3D dir(0,1,0);
	mat.transformVector(&dir);
	return dir;
}


float Quaternion::dot(const Quaternion& q) const {

	return ( w * q.w + x * q.x + y * q.y + z * q.z );

}


Quaternion Quaternion::slerp(const Quaternion& q, float t) const {

	float scale0, scale1;

	float cosa = this->dot(q);

	float minus = 1.0;

	if( cosa < 0.0 ) {

		cosa = - cosa;

		minus = -1.0;

	}


	if( 1.0 - cosa > V_TOL_ANGULAR ) {

		float angle = acos(cosa);

		float sina = sin(angle);

		scale0 = sin((1.0f - t) * angle) / sina;

		scale1 = sin(t * angle) / sina;

	}

	else {

		scale0 = 1.0f - t;

		scale1 = t;

	}

	scale1 *= minus;


	Quaternion res;

	res.w = scale0 * w + scale1 * q.w;

	res.x = scale0 * x + scale1 * q.x;

	res.y = scale0 * y + scale1 * q.y;

	res.z = scale0 * z + scale1 * q.z;

	/*float mag = res.magnitude();

	if( mag + V_TOL_ANGULAR < 1.0 ) {

	printf("");

	}

	res.w /= mag;

	res.x /= mag;

	res.y /= mag;

	res.z /= mag;*/

	return res;

}


Quaternion Quaternion::operator*(const Quaternion& q) const {

	/*return Quaternion(
	w * q.w - x * q.x - y * q.y - z * q.z,
	w * q.x + x * q.w - y * q.z + z * q.y,
	w * q.y + x * q.z + y * q.w - z * q.x,
	w * q.z - x * q.y + y * q.x + z * q.w
	);*/
	return Quaternion(
		w * q.w - x * q.x - y * q.y - z * q.z,
		w * q.x + x * q.w + y * q.z - z * q.y,
		w * q.y - x * q.z + y * q.w + z * q.x,
		w * q.z + x * q.y - y * q.x + z * q.w
		);
}



Rotation3D::Rotation3D() {
	rotateToIdentity();
}

Rotation3D::Rotation3D(const Quaternion &quat) : quat(quat) {
}

Rotation3D::Rotation3D(float x, float y, float z) {
	rotateToEuler(x, y, z);
}

void Rotation3D::rotate(float x,float y,float z) {
	Quaternion adj;
	adj.setEuler(x,y,-z);
	quat = adj * quat;
}

void Rotation3D::rotateLocal(float x,float y,float z) {
	Quaternion adj;
	adj.setEuler(x,y,-z);
	quat = quat * adj;
}

void Rotation3D::rotateToEuler(float x,float y,float z) {
	quat.setEuler(x,y,-z);
}

void Rotation3D::rotateToDirection(const Vector3D &axis, const Vector3D &dir) {
	// rotates so that the vector (0.0f, 0.0f, -1.0f) looks towards the projection of dir into the plane perpendicular to axis, by rotating about axis
	Vector3D proj_dir = dir;
	proj_dir.perpComp(axis);
	float mag_proj_dir = proj_dir.magnitude();
	if( mag_proj_dir < V_TOL_LINEAR ) {
		return; // dir is parallel to axis, can't rotate
	}
	proj_dir /= mag_proj_dir;

	Vector3D proj_start(0.0f, 0.0f, -1.0f);
	proj_start.perpComp(axis);
	float maj_proj_start = proj_start.magnitude();
	if( maj_proj_start < V_TOL_LINEAR ) {
		return; // start is parallel to axis, can't rotate
	}
	proj_start /= maj_proj_start;

	float sin_angle = (proj_dir ^ proj_start) % axis;
	float cos_angle = proj_start % proj_dir;
	float angle = atan2(sin_angle, cos_angle);
	quat.setAngleAxis(axis.x, axis.y, axis.z, angle);
	//VI_log("%f, %f, %f, %f\n", quat.x, quat.y, quat.z, quat.w);
}

void Rotation3D::rotateToDirection(const Vector3D &dir) {
	// rotates so that the vector (0.0f, 0.0f, -1.0f) looks towards the dir, by the shortest rotation. If dir is equal to (0.0f, 0.0f, 1.0f), the rotation is about (0.0f, 1.0f, 0.0f);
	Vector3D start(0.0f, 0.0f, -1.0f);
	float cos_angle = start % dir;
	if( cos_angle >= 1.0f - V_TOL_LINEAR ) {
		return; // already done
	}
	else if( cos_angle <= -1.0f + V_TOL_LINEAR ) {
		// antiparallel case
		this->rotateToDirection(Vector3D(0.0f, 1.0f, 0.0f), dir);
		return;
	}
	Vector3D axis = dir ^ start;
	float sin_angle = axis.magnitude();
	axis.normaliseSafe();

	float angle = atan2(sin_angle, cos_angle);
	quat.setAngleAxis(axis.x, axis.y, axis.z, angle);
	//VI_log("%f, %f, %f, %f\n", quat.x, quat.y, quat.z, quat.w);
}

void Rotation3D::getEuler(float *x,float *y,float *z) const {
	quat.convertToEuler(x,y,z);
}

void Rotation3D::getMatrix(IRotationMatrix *matrix) const {
	quat.convertToMatrix(matrix);
}

void Rotation3D::getMatrixInverse(IRotationMatrix *matrix) const {
	quat.convertToMatrixInverse(matrix);
}

/*void Rotation3D::multGLMatrixTranspose() {
	GLfloat glmat[16];
	quat.convertInverseToGLMatrix(glmat);
	glMultMatrixf(glmat);
}

void Rotation3D::multGLMatrix() {
	//GLfloat glmat[16];
	//quat.convertToGLMatrix(glmat);
	//glMultMatrixf(glmat);
	Vector3D pos(0,0,0);
	GraphicsEnvironment::getSingleton()->renderer->transform(&pos, &quat, false);
}*/

void Matrix::transformVector(Vector3D *v) const {
	/*float nx =		v->x * m[0][0] +
		v->y * m[1][0] +
		v->z * m[2][0] ;
	float ny =		v->x * m[0][1] +
		v->y * m[1][1] +
		v->z * m[2][1] ;
	float nz =		v->x * m[0][2] +
		v->y * m[1][2] +
		v->z * m[2][2] ;*/
	float nx =
		v->x * m[0][0] +
		v->y * m[0][1] +
		v->z * m[0][2];
	float ny =
		v->x * m[1][0] +
		v->y * m[1][1] +
		v->z * m[1][2];
	float nz =
		v->x * m[2][0] +
		v->y * m[2][1] +
		v->z * m[2][2];

	v->x=nx;
	v->y=ny;
	v->z=nz;
}

void IRotationMatrix::setRotation(const Rotation3D &r) {
	r.getMatrix(this);
}

void IRotationMatrix::setRotationInverse(const Rotation3D &r) {
	r.getMatrixInverse(this);
}

void Matrix4d::transformVector(Vector3D *v) const {
	/*nx =		v->x * m[0][0] +
		v->y * m[1][0] +
		v->z * m[2][0] +
		m[3][0] ;
	ny =		v->x * m[0][1] +
		v->y * m[1][1] +
		v->z * m[2][1] +
		m[3][1] ;
	nz =		v->x * m[0][2] +
		v->y * m[1][2] +
		v->z * m[2][2] +
		m[3][2] ;
	v->x=nx;
	v->y=ny;
	v->z=nz;*/
	float nx =
		v->x * m[0][0] +
		v->y * m[0][1] +
		v->z * m[0][2] +
		m[0][3];
	float ny =
		v->x * m[1][0] +
		v->y * m[1][1] +
		v->z * m[1][2] +
		m[1][3];
	float nz =
		v->x * m[2][0] +
		v->y * m[2][1] +
		v->z * m[2][2] +
		m[2][3];

	v->x = nx;
	v->y = ny;
	v->z = nz;
}

void Matrix4d::transformDirection(Vector3D *v) const {
	float nx =
		v->x * m[0][0] +
		v->y * m[0][1] +
		v->z * m[0][2];
	float ny =
		v->x * m[1][0] +
		v->y * m[1][1] +
		v->z * m[1][2];
	float nz =
		v->x * m[2][0] +
		v->y * m[2][1] +
		v->z * m[2][2];

	v->x = nx;
	v->y = ny;
	v->z = nz;
}

void Matrix4d::premultiply(const Matrix4d &that) {
	float new_matrix[4][4];
	for(int i=0;i<4;i++) {
		for(int j=0;j<4;j++) {
			new_matrix[i][j] = 0;
			for(int k=0;k<4;k++)
				new_matrix[i][j] += that.m[i][k] * this->m[k][j];
		}
	}
	for(int i=0;i<4;i++) {
		for(int j=0;j<4;j++) {
			this->m[i][j] = new_matrix[i][j];
		}
	}
}

Matrix4d * Matrix4d::multiply(const Matrix4d &that) const {
	Matrix4d * out = new Matrix4d();
	for(int i=0;i<4;i++) {
		for(int j=0;j<4;j++) {
			/*out->m[j][i] = 0;
			for(int k=0;k<4;k++)
				out->m[j][i] += this->m[k][i] * that->m[j][k];*/
			out->m[i][j] = 0;
			for(int k=0;k<4;k++)
				out->m[i][j] += this->m[i][k] * that.m[k][j];
		}
	}
	return out;
}

void Matrix4d::setAngleAxisRotation(float x,float y,float z,float angle) {
	//int a = QV_convertDegrees(angle);
	float norm = sqrt(x*x + y*y + z*z);
	x = x/norm;
	y = y/norm;
	z = z/norm;
	float ca=V_cosd(angle);
	float sa=V_sind(angle);
	//printf("%d , %d\n",ca,sa);
	/*if(y!=0 || z!=0) {
		m[0][0]=x*x*(1.0f - ca) + ca;
		m[0][1]=x*y*(1.0f - ca) - (z * sa)/sqrt(y*y+z*z);
		m[0][2]=x*z*(1.0f - ca) + (y * sa)/sqrt(y*y+z*z);
	}
	m[0][3]=0;
	if(x!=0 || z!=0) {
		m[1][0]=y*x*(1.0f - ca) + (z * sa)/sqrt(x*x+z*z);
		m[1][1]=y*y*(1.0f - ca) + ca;
		m[1][2]=y*z*(1.0f - ca) - (x * sa)/sqrt(x*x+z*z);
	}
	m[1][3]=0;
	if(x!=0 || y!=0) {
		m[2][0]=z*x*(1.0f - ca) - (y * sa)/sqrt(x*x+y*y);
		m[2][1]=z*y*(1.0f - ca) + (x * sa)/sqrt(x*x+y*y);
		m[2][2]=z*z*(1.0f - ca) + ca;
	}
	m[2][3]=0;
	m[3][0]=0;
	m[3][1]=0;
	m[3][2]=0;
	m[3][3]=1.0f;*/
	//say();
	if(y!=0 || z!=0) {
		m[0][0]=x*x*(1.0f - ca) + ca;
		m[1][0]=x*y*(1.0f - ca) - (z * sa)/sqrt(y*y+z*z);
		m[2][0]=x*z*(1.0f - ca) + (y * sa)/sqrt(y*y+z*z);
	}
	m[3][0]=0;
	if(x!=0 || z!=0) {
		m[0][1]=y*x*(1.0f - ca) + (z * sa)/sqrt(x*x+z*z);
		m[1][1]=y*y*(1.0f - ca) + ca;
		m[2][1]=y*z*(1.0f - ca) - (x * sa)/sqrt(x*x+z*z);
	}
	m[3][1]=0;
	if(x!=0 || y!=0) {
		m[0][2]=z*x*(1.0f - ca) - (y * sa)/sqrt(x*x+y*y);
		m[1][2]=z*y*(1.0f - ca) + (x * sa)/sqrt(x*x+y*y);
		m[2][2]=z*z*(1.0f - ca) + ca;
	}
	m[3][2]=0;
	m[0][3]=0;
	m[1][3]=0;
	m[2][3]=0;
	m[3][3]=1.0f;
}

// Perlin noise

#define B 0x100
#define BM 0xff

#define N 0x1000
#define NP 12   /* 2^N */
#define NM 0xfff

static int p[B + B + 2];
static float g3[B + B + 2][3];
static float g2[B + B + 2][2];
static float g1[B + B + 2];
static int start = 1;

static void normalize2(float v[2])
{
	float s;

	s = sqrt(v[0] * v[0] + v[1] * v[1]);
	v[0] = v[0] / s;
	v[1] = v[1] / s;
}

static void normalize3(float v[3])
{
	float s;

	s = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	v[0] = v[0] / s;
	v[1] = v[1] / s;
	v[2] = v[2] / s;
}

void VI_initPerlin()
{
	start = 0;
	int i, j, k;

	for (i = 0 ; i < B ; i++) {
		p[i] = i;

		g1[i] = (float)((rand() % (B + B)) - B) / B;

		for (j = 0 ; j < 2 ; j++)
			g2[i][j] = (float)((rand() % (B + B)) - B) / B;
		normalize2(g2[i]);

		for (j = 0 ; j < 3 ; j++)
			g3[i][j] = (float)((rand() % (B + B)) - B) / B;
		normalize3(g3[i]);
	}

	while (--i) {
		k = p[i];
		p[i] = p[j = rand() % B];
		p[j] = k;
	}

	for (i = 0 ; i < B + 2 ; i++) {
		p[B + i] = p[i];
		g1[B + i] = g1[i];
		for (j = 0 ; j < 2 ; j++)
			g2[B + i][j] = g2[i][j];
		for (j = 0 ; j < 3 ; j++)
			g3[B + i][j] = g3[i][j];
	}
}

#define s_curve(t) ( t * t * (3.0f - 2.0f * t) )

#define lerp(t, a, b) ( a + t * (b - a) )

#define setup(i,b0,b1,r0,r1)\
	t = vec[i] + N;\
	b0 = ((int)t) & BM;\
	b1 = (b0+1) & BM;\
	r0 = t - (int)t;\
	r1 = r0 - 1.0f;

float VI_perlin_noise1(float arg)
{
	int bx0, bx1;
	float rx0, rx1, sx, t, u, v, vec[1];

	vec[0] = arg;
	if (start) {
		VI_initPerlin();
	}

	setup(0, bx0,bx1, rx0,rx1);

	sx = s_curve(rx0);

	u = rx0 * g1[ p[ bx0 ] ];
	v = rx1 * g1[ p[ bx1 ] ];

	return lerp(sx, u, v);
}

float VI_perlin_noise2(float vec[2])
{
	int bx0, bx1, by0, by1, b00, b10, b01, b11;
	float rx0, rx1, ry0, ry1, *q, sx, sy, a, b, t, u, v;
	register int i, j;

	if (start) {
		VI_initPerlin();
	}

	setup(0, bx0,bx1, rx0,rx1);
	setup(1, by0,by1, ry0,ry1);

	i = p[ bx0 ];
	j = p[ bx1 ];

	b00 = p[ i + by0 ];
	b10 = p[ j + by0 ];
	b01 = p[ i + by1 ];
	b11 = p[ j + by1 ];

	sx = s_curve(rx0);
	sy = s_curve(ry0);

#define at2(rx,ry) ( rx * q[0] + ry * q[1] )

	q = g2[ b00 ] ; u = at2(rx0,ry0);
	q = g2[ b10 ] ; v = at2(rx1,ry0);
	a = lerp(sx, u, v);

	q = g2[ b01 ] ; u = at2(rx0,ry1);
	q = g2[ b11 ] ; v = at2(rx1,ry1);
	b = lerp(sx, u, v);

	return lerp(sy, a, b);
}

float VI_perlin_noise3(float vec[3])
{
	int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11;
	float rx0, rx1, ry0, ry1, rz0, rz1, *q, sy, sz, a, b, c, d, t, u, v;
	register int i, j;

	if (start) {
		VI_initPerlin();
	}

	setup(0, bx0,bx1, rx0,rx1);
	setup(1, by0,by1, ry0,ry1);
	setup(2, bz0,bz1, rz0,rz1);

	i = p[ bx0 ];
	j = p[ bx1 ];

	b00 = p[ i + by0 ];
	b10 = p[ j + by0 ];
	b01 = p[ i + by1 ];
	b11 = p[ j + by1 ];

	t  = s_curve(rx0);
	sy = s_curve(ry0);
	sz = s_curve(rz0);

#define at3(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] )

	q = g3[ b00 + bz0 ] ; u = at3(rx0,ry0,rz0);
	q = g3[ b10 + bz0 ] ; v = at3(rx1,ry0,rz0);
	a = lerp(t, u, v);

	q = g3[ b01 + bz0 ] ; u = at3(rx0,ry1,rz0);
	q = g3[ b11 + bz0 ] ; v = at3(rx1,ry1,rz0);
	b = lerp(t, u, v);

	c = lerp(sy, a, b);

	q = g3[ b00 + bz1 ] ; u = at3(rx0,ry0,rz1);
	q = g3[ b10 + bz1 ] ; v = at3(rx1,ry0,rz1);
	a = lerp(t, u, v);

	q = g3[ b01 + bz1 ] ; u = at3(rx0,ry1,rz1);
	q = g3[ b11 + bz1 ] ; v = at3(rx1,ry1,rz1);
	b = lerp(t, u, v);

	d = lerp(sy, a, b);

	return lerp(sz, c, d);
}

VI_XMLTreeNode::VI_XMLTreeNode() : has_data(false) {
}

VI_XMLTreeNode::~VI_XMLTreeNode() {
	for(vector<VI_XMLTreeNode *>::iterator iter = children.begin(); iter != children.end(); ++iter) {
		VI_XMLTreeNode *child = *iter;
		delete child;
	}
}

void VI_XMLTreeNode::setName(const char *name) {
	this->name = name;
}
string VI_XMLTreeNode::getName() const {
	return name;
}

void VI_XMLTreeNode::addAttribute(const char *name, const char *value) {
	attributes[name] = value;
}
string VI_XMLTreeNode::getAttribute(const char *name) {
	return attributes[name];
}

void VI_XMLTreeNode::setData(const char *data) {
	this->has_data = true;
	this->data = data;
}

bool VI_XMLTreeNode::hasData() const {
	return has_data;
}

string VI_XMLTreeNode::getData() const {
	return data;
}

/*void VI_XMLTreeNode::setIntData(int int_data) {
	this->has_int_data = true;
	this->int_data = int_data;
}

bool VI_XMLTreeNode::hasIntData() const {
	return has_int_data;
}

int VI_XMLTreeNode::getIntData() const {
	return int_data;
}

void VI_XMLTreeNode::setDoubleData(int double_data) {
	this->has_double_data = true;
	this->double_data = double_data;
}

bool VI_XMLTreeNode::hasDoubleData() const {
	return has_double_data;
}

double VI_XMLTreeNode::getDoubleData() const {
	return double_data;
}*/

size_t VI_XMLTreeNode::getNChildren() const {
	return children.size();
}
VI_XMLTreeNode *VI_XMLTreeNode::getChild(size_t i) {
	return children.at(i);
}
const VI_XMLTreeNode *VI_XMLTreeNode::getChild(size_t i) const {
	return children.at(i);
}
void VI_XMLTreeNode::addChild(VI_XMLTreeNode *child) {
	children.push_back(child);
}

VI_XMLTreeNode *VI_XMLTreeNode::findNode(const char *match, bool recurse) {
	if( this->name == match ) {
		return this;
	}
	for(vector<VI_XMLTreeNode *>::iterator iter = children.begin(); iter != children.end(); ++iter) {
		VI_XMLTreeNode *child = *iter;
		if( recurse ) {
			VI_XMLTreeNode *found = child->findNode(match, true);
			if( found != NULL )
				return found;
		}
		else {
			if( child->getName() == match ) {
				return child;
			}
		}
	}
	return NULL;
}

void parseXMLNode(const TiXmlNode *parent, VI_XMLTreeNode *vi_tree) {
	if( parent == NULL ) {
		return;
	}

	switch( parent->Type() ) {
		case TiXmlNode::TINYXML_DOCUMENT:
			break;
		case TiXmlNode::TINYXML_ELEMENT:
			{
				VI_XMLTreeNode *vi_child = new VI_XMLTreeNode();
				vi_child->setName( parent->Value() );
				const TiXmlElement *element = parent->ToElement();
				const TiXmlAttribute *attribute = element->FirstAttribute();
				while( attribute != NULL ) {
					vi_child->addAttribute( attribute->Name(), attribute->Value() );
					attribute = attribute->Next();
				}
				// add to the tree
				vi_tree->addChild(vi_child);
				vi_tree = vi_child;
			}
			break;
		case TiXmlNode::TINYXML_COMMENT:
			break;
		case TiXmlNode::TINYXML_UNKNOWN:
			break;
		case TiXmlNode::TINYXML_TEXT:
			{
				/*
				// add to the existing node, not a child
				vi_tree->setData( parent->Value() );
				*/
				VI_XMLTreeNode *vi_child = new VI_XMLTreeNode();
				vi_child->setData( parent->Value() );
				// add to the tree
				vi_tree->addChild(vi_child);
				vi_tree = vi_child;
			}
			break;
		case TiXmlNode::TINYXML_DECLARATION:
			break;
	}

	for(const TiXmlNode *child=parent->FirstChild();child!=NULL;child=child->NextSibling())  {
		parseXMLNode(child, vi_tree);
	}
}

VI_XMLTreeNode *VI_parseXML(const char *filename) {
	TiXmlDocument doc(filename);
	if( !doc.LoadFile() ) {
		VI_log("failed to load XML file: %s\n", filename);
		return NULL;
	}

	VI_XMLTreeNode *tree = new VI_XMLTreeNode();
	parseXMLNode(&doc, tree);

	return tree;
}

/* Creates a folder if it doesn't exist (n.b., doesn't have to be in the appliation's folder).
 * Returns false if the folder doesn't exist, and we failed to create it.
 */
bool VI_createFolder(const char *folder) {
	bool ok = true;
	if( access(folder, 0) != 0 ) {
		VI_log("create folder: %s\n", folder);
#if _WIN32
		int res = mkdir(folder);
#elif __linux
        //mode_t mask = umask(0);
        //VI_log("mask %d\n", mask);
        //VI_log("mask %d\n", umask(0));
		int res = mkdir(folder, S_IRWXU | S_IRWXG | S_IRWXO);
		//umask(mask);
#else
		FIXME
#endif
		if( res != 0 ) {
			VI_log("mkdir failed: %d\n", errno);
			ok = false;
		}
		else if( access(folder, 0) != 0 ) {
			VI_log("still can't access folder!\n");
			ok = false;
		}
	}
	return ok;
}
