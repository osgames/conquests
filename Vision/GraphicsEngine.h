#pragma once

#include <vector>
using std::vector;
#include <set>
using std::set;

#include "Misc.h" // for Color
#include "VisionShared.h" // for V_BLENDTYPE_t

class Shader;
class ShaderEffect;
class SceneGraphNode;
class RenderData;
class RenderableInfo;
class ShadowPlane;
class World;
class Sector;
class ObjectData;
class GraphicsEnvironment;
class Renderer;
class FramebufferObject;
class TransformationMatrix;

//const int REFLECTION_TEXTURE_SIZE = 512;

class Renderable {
public:
	SceneGraphNode *node;
	RenderData *renderData; // if non-NULL, we only render this RenderData
	int sort_preference; // lower means more likely to draw first

	Renderable(SceneGraphNode *node);
};

typedef set<SceneGraphNode *> RQShadowplanesCollection;
typedef RQShadowplanesCollection::iterator RQShadowplanesIter;

typedef vector<SceneGraphNode *> RQEntityCollection;
typedef RQEntityCollection::iterator RQEntityIter;

typedef vector<Renderable> RQRenderableCollection;
typedef RQRenderableCollection::iterator RQRenderableIter;

class RenderQueue {
	friend class Graphics3D;
	/*vector<Renderable> *renderQueue;
	vector<Renderable> *renderQueue_nonopaque;*/
	RQRenderableCollection *renderQueue;
	RQRenderableCollection *renderQueue_nonopaque;
public:
	RQEntityCollection *renderQueue_lights;
	RQEntityCollection *renderQueue_infinite;
	RQEntityCollection *renderQueue_shadowcasters;
	//vector<Entity *> *renderQueue_nonopaque;
	//vector<Entity *> *renderQueue_shadowplanes;
	RQShadowplanesCollection *renderQueue_shadowplanes;

	RenderQueue();
	~RenderQueue();

	void flushRenderQueue() const;
	void addOpaque(SceneGraphNode *node) const;
	void addOpaque(Renderable *renderable) const;
	size_t nOpaque() const;
	Renderable *getOpaque(size_t i) const;

	void addNonOpaque(SceneGraphNode *node) const;
	void addNonOpaque(Renderable *renderable) const;
};

class Graphics3DShaders {
	friend class Graphics3D;

	// general rendering shaders
	bool has_general;
	/*Shader *vertex_shader_ambient;
	Shader *pixel_shader_ambient;
	Shader *vertex_shader_directional;
	Shader *pixel_shader_directional;
	Shader *vertex_shader_point;
	Shader *pixel_shader_point;*/
	const ShaderEffect *shader_ambient;
	const ShaderEffect *shader_directional;
	const ShaderEffect *shader_point;

	// hdr shaders
	/*const Shader *hdr_vertex_shader;
	const Shader *hdr_pixel_shader;
	const Shader *blurw_pixel_shader;
	const Shader *blurh_hdr_pixel_shader;*/
	const ShaderEffect *hdr_shader;

	// cloud shaders
	bool has_clouds;
	/*const Shader *vertex_shader_clouds;
	const Shader *pixel_shader_clouds;*/
	const ShaderEffect *shader_clouds;

	// atmosphere rendering
	bool has_atmosphere;
	/*const Shader *vertex_shader_skyfromatmos;
	const Shader *pixel_shader_skyfromatmos;*/
	const ShaderEffect *shader_atmos;

	// bump shaders
	bool has_bump;
	/*const Shader *bump_vertex_shader_directional;
	const Shader *bump_vertex_shader_point;
	const Shader *bump_pixel_shader_directional;
	const Shader *bump_pixel_shader_point;*/
	const ShaderEffect *bump_shader_directional;
	const ShaderEffect *bump_shader_point;

	bool has_hardware_animation;
	const ShaderEffect *hardware_animation_shader_ambient;
	const ShaderEffect *hardware_animation_shader_directional;
	const ShaderEffect *hardware_animation_shader_point;

	// shadow volumes shader (not used atm)
	//Shader *vertex_shader_shadow_point;

	// water shaders (simple non-reflective)
	//Shader *water_simple_pixel_shader;
	// water shaders (complex reflective)
	bool has_water;
	/*const Shader *water_vertex_shader_ambient;
	const Shader *water_pixel_shader_ambient;
	const Shader *water_vertex_shader_directional;
	const Shader *water_pixel_shader_directional;
	const Shader *water_vertex_shader_point;
	const Shader *water_pixel_shader_point;*/
	const ShaderEffect *water_shader_ambient;
	const ShaderEffect *water_shader_directional;
	const ShaderEffect *water_shader_point;

	// splatting
	bool has_splat;
	/*const Shader *splat_pixel_shader_ambient;
	const Shader *splat_pixel_shader_directional;
	const Shader *splat_pixel_shader_point;*/
	const ShaderEffect *splat_shader_ambient;
	const ShaderEffect *splat_shader_directional;
	const ShaderEffect *splat_shader_point;

	Graphics3DShaders();
	void clear();
};

class Graphics3D {
private:
	GraphicsEnvironment *genv;
	World *world;
	//int width,height;
	//float z_near,z_far;
	//bool persp;
	bool use_shadow_volumes; // if true, this means that any projected shadow receivers are ignored
	FramebufferObject *hdr_fbo; // used for rendering into
	FramebufferObject *temp_fbo; // used as a spare, e.g., for bloom effect
	bool hdr_enabled;
	bool bloom_enabled;
	bool want_hdr;
	bool failed_to_load_shaders; // true if failed to load any of the shaders
	TransformationMatrix *worldToEyeMatrix;
	SceneGraphNode *active_light;

	void set3DMode(bool mode_3d);
	void createHDRBuffers();
	void renderHDRToScreen();
	void doObjectShadow(SceneGraphNode *aobj,ShadowPlane *shadowplane,int light_index);
	void setViewport();
	/*void disableLights();
	void disableLight(int index);
	void enableOneLight(int n_light,Entity *light_ent);
	void enableLight(int index);
	void enableLights();*/
	void drawSky(RenderQueue *queue,SceneGraphNode *portal);
	void setShaders(SceneGraphNode *light_ent,ObjectData *obj);
	void doProjectiveShadows(Sector *sector);
	void drawMirrors();
	void drawFog();
	void drawOpaque(int recursion);
	void drawTransparent();
	void drawSector(Sector *sector,SceneGraphNode *portal,int recursion);
	void setClipPlane(const double pl[4]);
	void unsetClipPlane();
	void resetClipPlane();
	void buildRenderQueue(Sector *sector);

	// Set fixed function constants
	//void setFixedFunctionLighting(Entity *light_ent, ObjectData *obj);
	// The following functions only set shader constants
	void setShaderFrameAlpha(const float frame_alpha) const;
	void setShaderMatSpecular(const float material_specular[4]) const;
	void setShaderMatColor(const float color[4]) const;
	void setShaderLightPos(float p1, float p2, float p3, float d1, float d2, float d3) const;
	void setShaderLightAmbient(float rf, float gf, float bf) const;
	void setShaderLightDiffuse(float rf, float gf, float bf) const;
	void setShaderLightSpecular(float rf, float gf, float bf) const;
	void setShaderLightAtten(float a, float b, float c) const;

	// shaders
	//vector<Shader *> *shaders;

public:
	Graphics3D(GraphicsEnvironment *genv, bool want_hdr);
	~Graphics3D();

	Graphics3DShaders std_shaders;

	/*bool shader_clipping;
	float shader_clipping_plane[4];*/

	RenderQueue *c_queue;

	int frames;
	int frame_time;
	//int current_time;
	float fps;

	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}
	void init();
	bool failedToLoadShaders() const {
		return this->failed_to_load_shaders;
	}
	void ReSizeGLScene(float z_near, float z_far, int width, int height, bool persp, float fovy);
	//void freeShaders();
	void setPerspective(bool persp, float fovy);
	void setUseShadowVolumes(bool use_shadow_volumes) {
		this->use_shadow_volumes = use_shadow_volumes;
	}
	bool useShadowVolumes() const {
		return this->use_shadow_volumes;
	}
	/*const Shader *getAmbientVertexShader() const;
	const Shader *getAmbientPixelShader() const;*/
	bool hasCloudShaders() const {
		return std_shaders.has_clouds;
	}
	/*const Shader *getCloudsVertexShader() const {
		return std_shaders.vertex_shader_clouds;
	}
	const Shader *getCloudsPixelShader() const {
		return std_shaders.pixel_shader_clouds;
	}*/
	/*const Shader *getCloudsVertexShader() const;
	const Shader *getCloudsPixelShader() const;*/
	const ShaderEffect *getCloudsShader() const {
		return std_shaders.shader_clouds;
	}
	bool hasAtmosphereShaders() const {
		return std_shaders.has_atmosphere;
	}
	/*const Shader *getAtmosVertexShader() const {
		return std_shaders.vertex_shader_skyfromatmos;
	}
	const Shader *getAtmosPixelShader() const {
		return std_shaders.pixel_shader_skyfromatmos;
	}*/
	/*const Shader *getAtmosVertexShader() const;
	const Shader *getAtmosPixelShader() const;*/
	const ShaderEffect *getAtmosShader() const {
		return std_shaders.shader_atmos;
	}
	bool hasSplatShaders() const {
		return std_shaders.has_splat;
	}
	/*const Shader *getSplatAmbientPixelShader() const {
		return std_shaders.splat_pixel_shader_ambient;
	}
	const Shader *getSplatDirectionalPixelShader() const {
		return std_shaders.splat_pixel_shader_directional;
	}
	const Shader *getSplatPointPixelShader() const {
		return std_shaders.splat_pixel_shader_point;
	}*/
	/*const Shader *getSplatAmbientPixelShader() const;
	const Shader *getSplatDirectionalPixelShader() const;
	const Shader *getSplatPointPixelShader() const;*/
	const ShaderEffect *getSplatAmbientShader() const {
		return std_shaders.splat_shader_ambient;
	}
	const ShaderEffect *getSplatDirectionalShader() const {
		return std_shaders.splat_shader_directional;
	}
	const ShaderEffect *getSplatPointShader() const {
		return std_shaders.splat_shader_point;
	}
	bool hasWaterShaders() const {
		return std_shaders.has_water;
	}
	/*const Shader *getWaterAmbientVertexShader() const {
		return std_shaders.water_vertex_shader_ambient;
	}
	const Shader *getWaterAmbientPixelShader() const {
		return std_shaders.water_pixel_shader_ambient;
	}
	const Shader *getWaterDirectionalVertexShader() const {
		return std_shaders.water_vertex_shader_directional;
	}
	const Shader *getWaterDirectionalPixelShader() const {
		return std_shaders.water_pixel_shader_directional;
	}
	const Shader *getWaterPointVertexShader() const {
		return std_shaders.water_vertex_shader_point;
	}
	const Shader *getWaterPointPixelShader() const {
		return std_shaders.water_pixel_shader_point;
	}*/
	/*const Shader *getWaterAmbientVertexShader() const;
	const Shader *getWaterAmbientPixelShader() const;
	const Shader *getWaterDirectionalVertexShader() const;
	const Shader *getWaterDirectionalPixelShader() const;
	const Shader *getWaterPointVertexShader() const;
	const Shader *getWaterPointPixelShader() const;*/
	const ShaderEffect *getWaterAmbientShader() const {
		return std_shaders.water_shader_ambient;
	}
	const ShaderEffect *getWaterDirectionalShader() const {
		return std_shaders.water_shader_directional;
	}
	const ShaderEffect *getWaterPointShader() const {
		return std_shaders.water_shader_point;
	}
	bool hasBumpShaders() const {
		return std_shaders.has_bump;
	}
	/*const Shader *getBumpDirectionalVertexShader() const {
		return std_shaders.bump_vertex_shader_directional;
	}
	const Shader *getBumpPointVertexShader() const {
		return std_shaders.bump_vertex_shader_point;
	}
	const Shader *getBumpDirectionalPixelShader() const {
		return std_shaders.bump_pixel_shader_directional;
	}
	const Shader *getBumpPointPixelShader() const {
		return std_shaders.bump_pixel_shader_point;
	}*/
	/*const Shader *getBumpDirectionalVertexShader() const;
	const Shader *getBumpDirectionalPixelShader() const;
	const Shader *getBumpPointVertexShader() const;
	const Shader *getBumpPointPixelShader() const;*/
	const ShaderEffect *getBumpDirectionalShader() const {
		return std_shaders.bump_shader_directional;
	}
	const ShaderEffect *getBumpPointShader() const {
		return std_shaders.bump_shader_point;
	}
	bool hasHardwareAnimationShaders() const {
		return std_shaders.has_hardware_animation;
	}
	const ShaderEffect *getHardwareAnimationAmbientShader() const {
		return std_shaders.hardware_animation_shader_ambient;
	}
	const ShaderEffect *getHardwareAnimationDirectionalShader() const {
		return std_shaders.hardware_animation_shader_directional;
	}
	const ShaderEffect *getHardwareAnimationPointShader() const {
		return std_shaders.hardware_animation_shader_point;
	}

	// callbacks
	// The Entity callbacks are called for every rendered entity
	void setShaderConstants(const SceneGraphNode *node) const;
	void setFixedFunctionConstants(const SceneGraphNode *node);
	// The RenderableInfo callbacks are called with further information (and will be multiple times, for objects with submeshes)
	void setShaderConstants(const RenderableInfo *renderable_info) const;
	void setFixedFunctionConstants(const RenderableInfo *renderable_info);

	void setBlend(V_BLENDTYPE_t blend_type, bool lighting_pass);

	World *getWorld() {
		return world;
	}
	const World *getWorld() const {
		return world;
	}
	void setWorld(World *world);
	GraphicsEnvironment *getGraphicsEnvironment() {
		return this->genv;
	}
	Renderer *getRenderer();
	const Renderer *getRenderer() const;
	/*float getZNear() const {
		return z_near;
	}
	float getZFar() const {
		return z_far;
	}*/
	bool isHDREnabled() const {
		return this->hdr_enabled;
	}
	/*void setWantHDR(bool want_hdr) {
		this->want_hdr = want_hdr;
	}*/
	void buildRenderQueue(SceneGraphNode *aobj);
	/*Vector2D getScreenSizeAtDist(float z) const;
	Vector3D getDirectionOfPoint(int x,int y) const;
	Vector3D getDirectionOfRotatedPoint(Rotation3D *rot,int x,int y) const;
	void eyeToScreen(int *x,int *y,const Vector3D *eye) const;*/
	//void setSpecular(const float specular[4]);
	//void setSpecularOff();
	/*const Frustum *getFrustum() const {
		return &frustum;
	};*/

	void setZ(float z_near,float z_far, float fovy);

	void drawFrame();
	void clearFrame();

	int getRenderToTextureSize() const;
	/*void getViewOrientation(Vector3D *right,Vector3D *up) const;
	void getViewDirection(Vector3D *dir) const;*/
	const TransformationMatrix *getWorldToEyeMatrix() const {
		return this->worldToEyeMatrix;
	}
};
