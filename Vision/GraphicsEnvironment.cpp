//---------------------------------------------------------------------------
#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include <cassert>
//#include <ctime>

#include "GraphicsEnvironment.h"
#include "Renderer.h"
#include "GraphicsEngine.h"
#include "InputHandler.h"
#include "Panel.h"
#include "Object3D.h" // for ObjectData::RenderPhase
#include "World.h"
#include "Entity.h"

//---------------------------------------------------------------------------

GraphicsEnvironment *GraphicsEnvironment::singleton = NULL;

/*bool Frustum::WorldPointInFrustum(Renderer *renderer, float x, float y, float z) const {
	// assumes that the current transformation matrix defines the world-to-eye matrix
	renderer->pushMatrix();
	renderer->translate(x,y,z);
	TransformationMatrix *matrix = renderer->createTransformationMatrix();
	matrix->save();
	bool rtn = PointInFrustum((*matrix)[12],(*matrix)[13],(*matrix)[14]);
	renderer->popMatrix();
	delete matrix;
	return rtn;
}*/

bool Frustum::PointInFrustum( float x, float y, float z ) const {
	for( int p = 0; p < 6; p++ )
		if( frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3] <= 0 )
			return false;
	return true;
}

float Frustum::SphereInFrustum( float x, float y, float z, float radius ) const {
	float d;
	//return 1;

	/*if( x*x + y*y + z*z <= radius*radius ) {
	d = frustum[5][0] * x + frustum[5][1] * y + frustum[5][2] * z + frustum[5][3];
	return d + radius; // inside
	}*/

	for( int p = 0; p < 6; p++ ) {
		d = frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3];
		if( d <= -radius )
			return 0; // outside
	}
	return d + radius; // inside

	/*
	// RIGHT
	d = frustum[0][0] * x + frustum[0][2] * z;
	if( d <= - radius )
	return 0;
	// LEFT
	d = frustum[1][0] * x + frustum[1][2] * z;
	if( d <= - radius )
	return 0;
	// BOTTOM
	d = frustum[2][1] * y + frustum[2][2] * z;
	if( d <= - radius )
	return 0;
	// TOP
	d = frustum[3][1] * y + frustum[3][2] * z;
	if( d <= - radius )
	return 0;
	// FAR
	d = frustum[4][2] * z + frustum[4][3];
	if( d <= - radius )
	return 0;
	// NEAR
	d = frustum[5][2] * z + frustum[5][3];
	if( d <= - radius )
	return 0;

	return d + radius;
	*/
}

bool Frustum::infiniteShadowVolumeInFrustum(float px,float py,float pz,float radius,float dx,float dy,float dz) const {
	//return true;
	const int infinity_c = 100; // TODO: hardcoded here and in RenderData
	float p2x = px + dx * infinity_c;
	float p2y = py + dy * infinity_c;
	float p2z = pz + dz * infinity_c;
	for(int p=0;p<6;p++ ) {
		float d = frustum[p][0] * px + frustum[p][1] * py + frustum[p][2] * pz + frustum[p][3];
		if( d <= -radius ) {
			// sphere is outside - now check direction
			if( frustum[p][0]*dx + frustum[p][1]*dy + frustum[p][2]*dz < 0 ) {
				// points away - extended cylinder won't intersect
				return false;
			}
			// now check if far sphere is also outside of this plane
			float d2 = frustum[p][0] * p2x + frustum[p][1] * p2y + frustum[p][2] * p2z + frustum[p][3];
			if( d2 <= -radius ) {
				return false;
			}
		}
	}
	// inside
	return true;
}

bool Frustum::BoxInFrustum(Vector3D pts[8]) const {
	bool out = false;
	for(int p = 0; p < 6 && !out; p++) {
		/*int in_count = 8;
		for(int i = 0; i < 8; i++) {
		float x = pts[i].x;
		float y = pts[i].y;
		float z = pts[i].z;
		if( frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3] <= 0 )
		in_count--;
		}
		if( in_count == 0 )
		return false;*/
		//bool out = true;
		out = true;
		// is every point outside of this plane?
		for(int i = 0; i < 8 && out; i++) {
			float x = pts[i].x;
			float y = pts[i].y;
			float z = pts[i].z;
			if( frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3] > 0 )
				out = false;
		}
		// if out, then we'll exit the loop, as no need to test other planes
		// if !out, we still need to test other planes
	}
	//return true;
	return !out;
}

bool GraphicsEnvironment::init() {
	//this->inputMode = INPUTMODE_EXTERNAL;
	//this->mouse_image = NULL;
	//this->created = false;
	this->request_quit = false;
	this->mouse_texture = NULL;
	this->popup = NULL;
	/*this->width = 0;
	this->height = 0;
	this->multisample = 0;
	this->fullscreen = false;*/
	//this->zoom=0.0f;
	this->panel = NULL;
	this->modal_panel = NULL;
	this->g = NULL;
	this->g3 = NULL;
	this->sound = NULL;
	this->renderFunc = NULL;
	this->mouseMotionEventFunc = NULL;
	this->mouseMotionEventData = NULL;

	this->stats_poly_count = 0;
	this->stats_vertex_count = 0;
	this->stats_distinct_vertex_count = 0;
	this->stats_render_calls_count = 0;

	this->g = NULL;
	this->g3 = NULL;
	this->renderer = NULL;
	this->inputSystem = NULL;

	this->is_fading = false;
	this->fade_start = 0;
	this->fade_end = 0;
	this->fade_timestart_ms = 0;
	this->fade_timeend_ms = 0;

	if( singleton != NULL ) {
		Vision::setError(new VisionException(this,VisionException::V_TOO_MANY_GRAPHICSENVIRONMENT_CLASSES,"Only one GraphicsEnvironment classes is allowed at a time"));
		return false;
	}
	else {
		singleton = this;
	}

	return true;
}

/*GraphicsEnvironment::GraphicsEnvironment(int width,int height,int multisample,bool fullscreen) : VisionObject() {
	//this->hInstance = hInstance;

	if(!init())
		return;

	this->width=width;
	this->height=height;
	if( multisample == 1 )
		multisample = 0; // enabling multisampling with 1x AA seems slower (on NVIDIA 8600GT)!
	this->multisample=multisample;
	this->fullscreen=fullscreen;
	//this->inputMode = INPUTMODE_INTERNAL;
}*/

GraphicsEnvironment::GraphicsEnvironment(VisionPrefs visionPrefs) : VisionObject() {
	this->init();

	if( visionPrefs.multisample == 1 ) {
		visionPrefs.multisample = 0; // enabling multisampling with 1x AA seems slower (on NVIDIA 8600GT)!
	}
	this->visionPrefs = visionPrefs;
}

/*GraphicsEnvironment::GraphicsEnvironment(HDC hDC,int width,int height,int multisample) : VisionObject() {
this->hInstance = NULL;

if(!init())
return;

this->width = width;
this->height = height;
this->multisample=multisample;
this->hDC = hDC;
//this->inputMode = INPUTMODE_INTERNAL;

if(!initialiseOpenGL(true))
return;

this->created = true;
}*/

/*GraphicsEnvironment::GraphicsEnvironment(int width,int height,int multisample) : VisionObject() {
if(!init())
return;

this->width = width;
this->height = height;
this->multisample=multisample;

if(!initialiseOpenGL(false))
return;

this->created = true;
}*/

GraphicsEnvironment::~GraphicsEnvironment() {
	/*delete g;
	delete g3;
	delete renderer;
	delete inputSystem;*/
	// sub-systems now deleted in sub-class destructor, as they must be destroyed first!

	assert(singleton != NULL);
	singleton = NULL;
}

size_t GraphicsEnvironment::memUsage() {
	size_t size = sizeof(this);
	size += g3->memUsage();
	/*size += mouseInputHandler->memUsage();
	size += keyboardInputHandler->memUsage();*/
	size += inputSystem->memUsage();
	return size;
}

//const int bits = 16;
const int bits = 32;
//const int multisample = 0;
//const int multisample = bits > 16 ? 2 : 0; // multisampling seems to require 32 bits

/*bool GraphicsEnvironment::setFullScreen(bool fullscreen) {
this->fullscreen = fullscreen;

if(fullscreen) {
DEVMODE dmScreenSettings;					// Device Mode
memset(&dmScreenSettings,0,sizeof(dmScreenSettings));		// Makes Sure Memory's Cleared
dmScreenSettings.dmSize=sizeof(dmScreenSettings);		// Size Of The Devmode Structure
dmScreenSettings.dmPelsWidth	= width;			// Selected Screen Width
dmScreenSettings.dmPelsHeight	= height;			// Selected Screen Height
dmScreenSettings.dmBitsPerPel	= bits;				// Selected Bits Per Pixel
dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;
// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
{
this->fullscreen = false;
}
}
else {
ChangeDisplaySettings(NULL,0);
ShowCursor(TRUE);
}
if (fullscreen) {
//dwExStyle=WS_EX_APPWINDOW;					// Window Extended Style
//dwStyle=WS_POPUP;						// Windows Style
ShowCursor(FALSE);						// Hide Mouse Pointer
}
else {
//dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
//dwStyle=WS_OVERLAPPEDWINDOW;					// Windows Style
}
//AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size
return true;
}*/

GraphicsEnvironment *GraphicsEnvironment::getSingleton() {
	if( singleton == NULL ) {
		Vision::setError(new VisionException(NULL,VisionException::V_NO_GRAPHICSENVIRONMENT,"No GraphicsEnvironment class active"));
	}
	return singleton;
}

void GraphicsEnvironment::setZ(float z_near,float z_far) {
	this->g3->setZ(z_near, z_far, fovy);
}

void GraphicsEnvironment::setWorld(World *world) {
	g3->setWorld(world);
}

/*void GraphicsEnvironment::setVisible(bool visible) {
	if(visible) {
		ShowWindow(hWnd,SW_SHOW);
		SetForegroundWindow(hWnd);
		SetFocus(hWnd);
	}
	else
		ShowWindow(hWnd,SW_HIDE);
}*/

void GraphicsEnvironment::resize(int width,int height) {
	if( this->visionPrefs.resolution_width == width && this->visionPrefs.resolution_height == height ) {
		return;
	}
	if( width == 0 )
		width = 1;
	if( height == 0 )
		height = 1;
	this->visionPrefs.resolution_width=width;
	this->visionPrefs.resolution_height=height;

	g3->ReSizeGLScene(renderer->getZNear(), renderer->getZFar(), width, height, renderer->getPersp(), fovy);
	if( panel != NULL ) {
		panel->setSize(width, height);
		//panel->validate();
	}
}

void GraphicsEnvironment::drawFrame() {
	checkError("GraphicsEnvironment::drawFrame enter");
	this->stats_poly_count = 0;
	this->stats_vertex_count = 0;
	this->stats_distinct_vertex_count = 0;
	this->stats_render_calls_count = 0;

	//LOG("GraphicsEnvironment::drawFrame()\n");
	if( !this->isActive() ) {
		//LOG("not active\n");
		return;
	}

	this->drawFrameStart();
	/*if( !this->drawFrameStart() ) {
		// device no longer active
		//LOG("device not active\n");
		return;
	}*/

	g3->clearFrame();
	_DEBUG_CHECK_ERROR_

	renderer->beginScene();
	g3->drawFrame();
	_DEBUG_CHECK_ERROR_

	//checkGLError("GraphicsEnvironment::drawFrame out");
	/*#ifdef _WIN32
	if(hWnd != NULL)
	inputHandler->setMouse(this);
	#endif*/
	/*if( this->inputMode == INPUTMODE_INTERNAL ) {
	#ifdef _WIN32
	if(hWnd != NULL)
	inputHandler->setMouse(this);
	#endif
	}*/

	if( panel != NULL ) {
		//this->popup = NULL;
		panel->paint(g);
		if( this->popup != NULL ) {
			this->popup->drawPopup(g);
		}
		//panel->input(inputHandler);
		_DEBUG_CHECK_ERROR_
	}
	//V_IMAGE_draw_with_mask(cursor,x - cursOffX,y + h - cursOffY);
	/*if( this->mouse_image != NULL ) {
		//this->mouse_image->drawWithMask(this->inputHandler->mouseX, this->inputHandler->mouseY + this->mouse_image->h);
		// TODO:
		this->mouse_image->drawWithMask(this->inputSystem->getMouse()->mouseX, this->inputSystem->getMouse()->mouseY + this->mouse_image->h); // original
		_DEBUG_CHECK_ERROR_
	}*/

	if( is_fading ) {
		int time_ms = Vision::getGameTimeMS();
		if( time_ms >= fade_timeend_ms ) {
			//LOG("FADE END: %d , %d, %d\n", time_ms, fade_timestart_ms, fade_timeend_ms);
			is_fading = false;
		}
		else {
			float frac = ((float)(time_ms - fade_timestart_ms)) / (float)(fade_timeend_ms - fade_timestart_ms);
			int fade = (int)((1.0f-frac) * fade_start + frac * fade_end);
			//LOG("FADE: %d\n", fade);
			if( fade < 0 )
				fade = 0;
			else if( fade > 255 )
				fade = 255;
			// fade effect
			float vertex_data[] = {
				0.0f, 0.0f, 0.0f,
				(float)this->getWidth(), 0.0f, 0.0f,
				(float)this->getWidth(), (float)this->getHeight(), 0.0f,
				0.0f, (float)this->getHeight(), 0.0f
			};
			unsigned char color_data[] = {0, 0, 0, static_cast<unsigned char>(fade)};
			this->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_ONE_BY_SRCALPHA);
			this->getRenderer()->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, NULL, 4, color_data, true, 4);
			this->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
		}
	}

	if( this->mouse_texture != NULL ) {
		this->mouse_texture->draw(this->inputSystem->getMouse()->mouseX, this->inputSystem->getMouse()->mouseY + this->mouse_texture->getHeight());
		_DEBUG_CHECK_ERROR_
	}
	checkError("GraphicsEnvironment::drawFrame exit");
}

void GraphicsEnvironment::drawFrameTest() {
	checkError("GraphicsEnvironment::drawFrameTest enter");
	this->stats_poly_count = 0;
	this->stats_vertex_count = 0;
	this->stats_distinct_vertex_count = 0;
	this->stats_render_calls_count = 0;

	g3->clearFrame();
	_DEBUG_CHECK_ERROR_

	renderer->beginScene();
	renderer->drawFrameTest();
	_DEBUG_CHECK_ERROR_
}

void GraphicsEnvironment::handleInput() {
	if( !this->isActive() ) {
		return;
	}
	//inputHandler->handled_event = false;
	//inputHandler->readQueue();
	inputSystem->getMouse()->handled_event = false;
	inputSystem->getKeyboard()->handled_event = false;
	inputSystem->getMouse()->readQueue(this);
	inputSystem->getKeyboard()->readQueue(this);
	_DEBUG_CHECK_ERROR_
	/*if(hWnd != NULL) {
		//inputHandler->setMouse(this);
		inputSystem->getMouse()->setMouse(this);
	}*/
	if(panel != NULL) {
		inputSystem->pushed_action = NULL;
		inputSystem->pushed_mouse_enter_action = NULL;
		inputSystem->pushed_mouse_exit_action = NULL;
		this->popup = NULL;
		if( modal_panel != NULL )
			modal_panel->input(inputSystem);
		else
			panel->input(inputSystem);
		/*if( this->popup != NULL ) {
			this->popup->drawPopup(g);
		}*/
		if( inputSystem->pushed_action != NULL ) {
			(*inputSystem->pushed_action)(inputSystem->pushed_action_src);
		}
		if( inputSystem->pushed_mouse_enter_action != NULL ) {
			(*inputSystem->pushed_mouse_enter_action)(inputSystem->pushed_mouse_enter_action_src);
		}
		if( inputSystem->pushed_mouse_exit_action != NULL ) {
			(*inputSystem->pushed_mouse_exit_action)(inputSystem->pushed_mouse_exit_action_src);
		}
		_DEBUG_CHECK_ERROR_
	}

	/*if( inputSystem->getKeyboard()->isPressed(V_ESCAPE) ) {
		LOG("Post quit\n");
		this->request_quit = true;
		//return;
	}*/

	//inputHandler->flushMouse();
	inputSystem->getMouse()->update();
	inputSystem->getKeyboard()->update();
	_DEBUG_CHECK_ERROR_
}

void GraphicsEnvironment::handleInput(int mouseX,int mouseY,bool mouseL,bool mouseM,bool mouseR) {
	//inputHandler->handled_event = false;
	//inputHandler->setMouse(mouseX,mouseY,mouseL,mouseM,mouseR);
	inputSystem->getMouse()->handled_event = false;
	inputSystem->getKeyboard()->handled_event = false;
	//inputSystem->getMouse()->setMouse(mouseX,mouseY,mouseL,mouseM,mouseR);
	inputSystem->getMouse()->setMouse(mouseX,mouseY/*,mouseL,mouseM,mouseR*/);
	if( panel != NULL ) {
		if( modal_panel != NULL )
			modal_panel->input(inputSystem);
		else
			panel->input(inputSystem);
	}
	//inputHandler->flushMouse();
}

void GraphicsEnvironment::flushInput() {
	inputSystem->getMouse()->flush();
	inputSystem->getKeyboard()->flush();
}

/*void GraphicsEnvironment::swapBuffers() {
	SwapBuffers( hDC );
}*/

/*void GraphicsEnvironment::clearFrame() {
	g3->clearFrame();
}*/

/*bool GraphicsEnvironment::setMouse(int mouseX,int mouseY,bool mouseL,bool mouseM,bool mouseR) {
if( this->inputMode == INPUTMODE_EXTERNAL ) {
inputHandler->setMouse(mouseX,mouseY,mouseL,mouseM,mouseR);
return true;
}
return false;
}*/

VI_Panel *GraphicsEnvironment::getPanel() const {
	return panel;
}

void GraphicsEnvironment::setPanel(VI_Panel *panel) {
	Panel *d_panel = dynamic_cast<Panel *>(panel); // cast to the internal representation
	this->panel = d_panel;
	if( this->panel != NULL ) {
		/*this->panel->setX(0);
		this->panel->setY(0);*/
		this->panel->setPos(0, 0);
		this->panel->setSize(getWidth(), getHeight());
		//this->panel->validate();
	}
}

VI_Panel *GraphicsEnvironment::getModalPanel() const {
	return modal_panel;
}

void GraphicsEnvironment::setModalPanel(VI_Panel *panel) {
	Panel *d_panel = dynamic_cast<Panel *>(panel); // cast to the internal representation
	this->modal_panel = d_panel;
}

void GraphicsEnvironment::setMouseTexture(VI_Texture *mouse_texture) {
	Texture *d_mouse_texture = static_cast<Texture *>(mouse_texture); // cast to the internal representation
	this->mouse_texture = d_mouse_texture;
}


/*void GraphicsEnvironment::drawEntity(SceneGraphNode *node) {
g3->doObject(node, Graphics3D::RENDER_OPAQUE);
}*/

bool GraphicsEnvironment::failedToLoadShaders() const {
	return g3->failedToLoadShaders();
}

bool GraphicsEnvironment::hasShaders() const {
	return this->renderer->hasShaders();
}

float GraphicsEnvironment::getFPS() const {
	return g3->fps;
}

void GraphicsEnvironment::getMouseInfo(int *x,int *y,bool buttons[3]) const {
	MouseInputHandler *ih = this->getInputSystem()->getMouse();
	*x = ih->mouseX;
	*y = ih->mouseY;
	buttons[0] = ih->isLMousePressed();
	buttons[1] = ih->isMMousePressed();
	buttons[2] = ih->isRMousePressed();
}

/*void GraphicsEnvironment::getMouseClick(bool buttons[3]) const {
	MouseInputHandler *ih = this->getInputSystem()->getMouse();
	buttons[0] = ih->isLMouseClicked();
	buttons[1] = ih->isMMouseClicked();
	buttons[2] = ih->isRMouseClicked();
}

bool GraphicsEnvironment::readMouseWheelUp() const {
	MouseInputHandler *ih = this->getInputSystem()->getMouse();
	return ih->readMouseWheelUp();
}

bool GraphicsEnvironment::readMouseWheelDown() const {
	MouseInputHandler *ih = this->getInputSystem()->getMouse();
	return ih->readMouseWheelDown();
}*/

void GraphicsEnvironment::setUseShadowVolumes(bool use_shadow_volumes) {
	this->g3->setUseShadowVolumes(use_shadow_volumes);
}

void GraphicsEnvironment::linkWorld(VI_World *world) {
	World *d_world = static_cast<World *>(world); // cast to the internal representation
	if( d_world != NULL ) {
		d_world->setGraphicsEnvironment(this);
	}
	this->setWorld(d_world);
}

VI_World *GraphicsEnvironment::getWorld() {
	return this->g3->getWorld();
}

const VI_World *GraphicsEnvironment::getWorld() const {
	return this->g3->getWorld();
}

VI_Font *GraphicsEnvironment::createFont(const char *family,int pt,int style) const {
	FontBuffer *fb = this->renderer->createFont(family,pt,style);
	return fb;
}

void GraphicsEnvironment::render(VI_SceneGraphNode *node, float x, float y, float z) {
	//Entity *d_entity = static_cast<Entity *>(entity); // cast to the internal representation
	SceneGraphNode *d_node = dynamic_cast<SceneGraphNode *>(node); // cast to the internal representation
	this->getRenderer()->pushMatrix();
	this->getRenderer()->translate(x, y, z);
	d_node->render(this, ObjectData::RENDER_NONOPAQUE, false, false, NULL);
	this->getRenderer()->popMatrix();
}

void GraphicsEnvironment::setFade(float duration, unsigned char fade_start, unsigned char fade_end) {
	this->is_fading = true;
	this->fade_timestart_ms = Vision::getGameTimeMS();
	this->fade_timeend_ms = (int)(fade_timestart_ms + duration * 1000.0f);
	this->fade_start = fade_start;
	this->fade_end = fade_end;
}

/*GraphicsEnvironment *GraphicsEnvironment::getGenv(HWND hWnd) {
	ASSERT( singleton != NULL );
	ASSERT( singleton->hWnd == NULL || singleton->hWnd == hWnd ); // singleton->hWnd will be NULL if not yet set (happens in fullscreen mode, as this callback is called when creating the window)
	return singleton;
}*/

Vector2D GraphicsEnvironment::getScreenSizeAtDist(float z) const {
	double H = 2.0 * tan( M_PI * fovy / 360.0 ) * z;
	//double W = H * this->width / this->height; // ?
	double W = H * this->getWidth() / this->getHeight(); // ?
	return Vector2D((float)W, (float)H);
}

Vector3D GraphicsEnvironment::getDirectionOfPoint(int x,int y) const {
	const int width = this->getWidth();
	const int height = this->getHeight();
	double sx = x - width / 2.0;
	double sy = height / 2.0 - y;
	double Z = this->renderer->getZNear();
	double H = 2.0 * tan( M_PI * fovy / 360.0 ) * Z;
	double Y = sy * H / height;
	double W = H * width / height; // ?
	double X = sx * W / width;
	Vector3D vec((float)X,(float)Y,(float)-Z);
	vec.normalise();
	return vec;
}

Vector3D GraphicsEnvironment::getDirectionOfRotatedPoint(const Rotation3D &rot,int x,int y) const {
	Vector3D vec = this->getDirectionOfPoint(x,y);
	vec.rotateBy(rot);
	return vec;
}

Vector3D GraphicsEnvironment::getDirectionOfPoint(const VI_SceneGraphNode *node, int sx, int sy) const {
	//SceneGraphNode *d_node = static_cast<SceneGraphNode *>(node); // cast to the internal representation
	//SceneGraphNode *d_node = dynamic_cast<SceneGraphNode *>(node); // cast to the internal representation
	Vector3D vec = this->getDirectionOfRotatedPoint(node->getRotation(),sx,sy);
	// recurse up the hierarchy, if necessary
	const SceneGraphNode *parent = dynamic_cast<const SceneGraphNode *>(node->getParentNode()); // cast to internal representation
	while(parent != NULL) {
		Rotation3D rot = parent->getRotation();
		vec.rotateBy(rot);
		//parent = parent->getParent();
		parent = dynamic_cast<SceneGraphNode *>(parent->getParentNode()); // cast to internal representation
	}
	return vec;
}

void GraphicsEnvironment::localToScreen(int *x, int *y, const VI_SceneGraphNode *node, const Vector3D &local) const {
	const SceneGraphNode *d_node = dynamic_cast<const SceneGraphNode *>(node); // cast to internal representation
	Vector3D eye = d_node->localToEyePos(local);
	eyeToScreen(x, y, eye);
}

Vector3D GraphicsEnvironment::worldToEye(const Vector3D &world) const {
	const TransformationMatrix *matrix = this->g3->getWorldToEyeMatrix();
	ASSERT( matrix != NULL );
	Vector3D eye = matrix->transformVec(world);
	return eye;
}

void GraphicsEnvironment::worldToScreen(int *x,int *y,const Vector3D &world) const {
	Vector3D eye = this->worldToEye(world);
	eyeToScreen(x, y, eye);
}

void GraphicsEnvironment::eyeToScreen(int *x,int *y,const Vector3D &eye) const {
	/*double T = tan( M_PI * fovy / 360.0 );
	double scale = this->height / (2.0 * T);
	int sx = (int)(- scale * eye->x / eye->z);
	int sy = (int)(scale * eye->y / eye->z);
	*x = sx + this->width/2;
	*y = sy + this->height/2;*/
	const int width = this->getWidth();
	const int height = this->getHeight();
	double Z = this->renderer->getZNear();
	double scale = - Z / eye.z;
	double X = eye.x * scale;
	double Y = eye.y * scale;
	double H = tan( M_PI * fovy / 360.0 ) * Z;
	double W = H * width / height; // ?
	double sx = X * width / (2.0 * W);
	double sy = Y * height / (2.0 * H);
	*x = (int)sx + width / 2;
	*y = height/2 - (int)sy;

	/*Vector3D test = getDirectionOfPoint(*x, *y);
	printf("%f, %f, %f\n", test.x, test.y, test.z);*/
}
