//---------------------------------------------------------------------------
#include <cassert>
#include <ctime>

#include "VisionIface.h"

#include "VisionPrefs.h"

#include "GraphicsEnvironment.h"
#include "OpenGL/SDL_GLGraphicsEnvironment.h"
#ifdef _WIN32
#include "DirectX/SDL_D3D9GraphicsEnvironment.h"
#endif

#include "Sound.h"

#include "GraphicsEngine.h"
#include "Terrain.h"

#include "Renderer.h"

#include "Panel.h"

#include "InputHandler.h"

#include "World.h"
#include "Entity.h"
#include "Object3D.h"

#include "Texture.h"

//---------------------------------------------------------------------------

/*class DummyObject : public VisionObject {
public:
	DummyObject() : VisionObject() {
	}
	virtual ~DummyObject() {
	}
	virtual V_CLASS_t getClass() {
		return V_CLASS_UNKNOWN;
	}
	virtual bool isSubClass(V_CLASS_t v_class) {
		return false;
	}
	virtual int memUsage() {
		return 0;
	}
};*/

/*void VI_addtag() {
	//Vision::addTag(NULL);
	//new SkyBox();
	//new DummyObject();
	//int size = sizeof(DummyObject);
	VI_log("VI_addtag()\n");
	int size = 65536;
	new char[size];
}*/

void VI_initialise(const char *application_name) {
	Vision::initialise(application_name);
}

string VI_getApplicationDataFilename(const char *folder, const char *name) {
	string filename = Vision::getApplicationDataFilename(folder, name);
	return filename;
}

char *VI_getApplicationFilename(const char *name) {
	char *filename = Vision::getApplicationFilename(name);
	return filename;
}

void VI_cleanup() {
	Vision::cleanup();
}

void VI_check() {
	Vision::check();
}

void VI_set_persistence_all(int level) {
	Vision::setPersistenceAll(level);
}

void VI_set_default_persistence(int level) {
	Vision::setDefaultPersistence(level);
}

void VI_debug_mem_usage_all() {
	Vision::debugMemUsage();
}

void VI_lock() {
	Vision::lock();
}

void VI_unlock() {
	Vision::unlock();
}

bool VI_islocked() {
	return Vision::isLocked();
}

void VI_flush(int level) {
	Vision::flush(level);
}

void VI_flush_all() {
	Vision::flushAll();
}

size_t VI_getNextCreationId() {
	return Vision::getNextCreationId();
}

void VI_flushSinceCreationId(size_t creation_id) {
	Vision::flushSinceCreationId(creation_id);
}

int VI_getNTags() {
	return Vision::getNTags();
}

int VI_getNTagsInUse() {
	return Vision::getNTagsInUse();
}

void VI_update(int time) {
	Vision::update(time);
}

int VI_getGameTimeMS() {
	return Vision::getGameTimeMS();
}

int VI_getGameTimeLastFrameMS() {
	return Vision::VI_getGameTimeLastFrameMS();
}

bool VI_keydown(int v_keycode) {
	return KEY_DOWN(v_keycode);
}

bool VI_keyany() {
	//return InputHandler::keyAny();
	GraphicsEnvironment *ge = GraphicsEnvironment::getSingleton();
	//InputHandler *ih = ge->getInputHandler();
	KeyboardInputHandler *ih = ge->getInputSystem()->getKeyboard();
	return ih->keyAny();
}

/*bool VI_keypressed(int v_keycode) {
	GraphicsEnvironment *ge = GraphicsEnvironment::getSingleton();
	KeyboardInputHandler *ih = ge->getInputSystem()->getKeyboard();
	return ih->wasPressed(v_keycode);
}*/

void VI_createThreads(int n_threads, VI_ThreadFunction **function, void **data) {
	createThreads(n_threads, function, data);
}

void VI_pushMessage(void *message) {
	Vision::pushMessage(message);
}

bool VI_hasMessages() {
	return Vision::hasMessages();
}

void *VI_peekMessage() {
	return Vision::peekMessage();
}

void *VI_popMessage() {
	return Vision::popMessage();
}

void VI_movementSimpleXY(VI_SceneGraphNode *node,int time) {
	SceneGraphNode *d_node = dynamic_cast<SceneGraphNode *>(node); // cast to the internal representation
	static Movement movement;
	movement.inputXY(d_node->getPoint(), time);
}

void VI_movementSimpleXYhover(VI_SceneGraphNode *node,int time) {
	SceneGraphNode *d_node = dynamic_cast<SceneGraphNode *>(node); // cast to the internal representation
	static Movement movement;
	movement.inputXYHover(d_node->getPoint(), time);
}

VI_Sound *VI_Sound::create() {
	LOG("VI_Sound::create()\n");

	Sound *sound = new SDL_Sound();

	/*if( !sound->isInitialised() ) {
		LOG("    Error: Failed to initialise sound\n");
		delete sound;
		return NULL;
	}*/
	return sound;
}

void VI_GraphicsEnvironment::render(VI_GraphicsEnvironment_Update update) {
	this->setVisible(true);

	static bool quit = false;

	quit = false;
	bool fin = false;
	int t_start = this->getRealTimeMS();
	// TODO: make more OS-independent; and needs to call handleInput()?
	while( !quit && !fin ) {
		// check for messages
#ifdef _WIN32
		MSG msg;
		if ( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) ) {
			// handle or dispatch messages
			if ( msg.message == WM_QUIT ) {
				quit = true;
			} else {
				TranslateMessage( &msg );
				DispatchMessage( &msg );
			}
		}
		else
#endif
		{
			if( this->isActive() ) {
				//int t_start = this->getRealTimeMS();
				t_start = this->getRealTimeMS();
				this->drawFrame();
				this->swapBuffers();
				this->handleInput();
				int time_diff = this->getRealTimeMS() - t_start;
				//t_start = this->getRealTimeMS();

				VI_update(time_diff);
				if( !quit && !fin && update != NULL )
					fin = !update(time_diff);
			}
#ifdef _WIN32
			else {
				WaitMessage();
			}
#endif
		}
	}
}

VI_HAnimationInfo *VI_HAnimationInfo::create(const char *name, float start, float end) {
	AnimationInfo *animationinfo = new AnimationInfo();
	animationinfo->setName(name);
	animationinfo->start = start;
	animationinfo->end = end;
	return animationinfo;
}

VI_Mesh *VI_Mesh::create(const char *name) {
	return create(name, 1);
}

VI_Mesh *VI_Mesh::create(const char *name, int n_frames) {
	Mesh *obj = new Mesh(n_frames);
	obj->setName(name);
	if( !Vision::isLocked() ) {
		obj->prepare();
	}
	return obj;
}

/*VI_VObject *VI_VObject::create() {
	return new Object3D();
}*/

VI_VObject *VI_VObject::create(const char *name) {
	return create(name, 1);
}

VI_VObject *VI_VObject::create(const char *name, int n_frames) {
	Object3D *obj = new Object3D(n_frames);
	obj->setName(name);
	if( !Vision::isLocked() ) {
		obj->prepare();
	}
	return obj;
}

VI_Particlesystem *VI_Particlesystem::createSnowstorm(int max_particles,VI_Texture *texture,float width,float height,float depth) {
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	Snowstorm *ss = new Snowstorm(max_particles, d_texture, width, height, depth);
	if( !Vision::isLocked() ) {
		ss->prepare();
	}
	return ss;
}

VI_GeneralParticlesystem *VI_GeneralParticlesystem::create(int max_particles) {
	NWNParticleSystem *ps = new NWNParticleSystem(max_particles);
	if( !Vision::isLocked() ) {
		ps->prepare();
	}
	return ps;
}

/*VI_VObject *VI_Modeller::createGrid(float w,float d,unsigned short nx,unsigned short ny,bool mirror,bool share) {
	LOG("VI_createGrid(%f,%f,%d,%d,%d,%d)\n",w,d,nx,ny,mirror,share);
	VI_VObject *obj = Modeller::createGrid(w,d,nx,ny,mirror,share);

	return obj;
}

VI_VObject *VI_Modeller::createCuboid(float w,float h,float d) {
	VI_VObject *obj = Modeller::createCuboid(w, h, d);

	return obj;
}

VI_VObject *VI_Modeller::createSphere(float r,unsigned short d) {
	return Modeller::createSphere(r, d);
}

void VI_Modeller::deformAttractor(VI_Object *obj, Vector3D centre, float radius, float scale) {
	return Modeller::deformAttractor(obj, centre, radius, scale);
}*/

VI_GraphicsEnvironment *VI_createGraphicsEnvironment(int width,int height,int multisample,bool fullscreen,bool hdr) {
	VisionPrefs visionPrefs; // fill defaults in constructor
	visionPrefs.resolution_width = width;
	visionPrefs.resolution_height = height;
	visionPrefs.fullscreen = fullscreen;
	visionPrefs.multisample = multisample;
	visionPrefs.hdr = hdr;
	return VI_createGraphicsEnvironment(&visionPrefs);
}

VI_GraphicsEnvironment *VI_createGraphicsEnvironment(VisionPrefs *visionPrefs) {
	GraphicsEnvironment *ge = NULL;
#ifdef _WIN32
	if( visionPrefs->api == VISIONAPI_OPENGL ) {
		ge = new SDL_GLGraphicsEnvironment(*visionPrefs);
	}
	else {
		ge = new SDL_D3D9GraphicsEnvironment(*visionPrefs);
	}
#else
	ge = new SDL_GLGraphicsEnvironment(*visionPrefs);
#endif
	//ASSERT( ge != NULL );
	//ASSERT( ge->isCreated() ); // any errors should have been thrown as an exception now!
	return ge;
}

VI_World *VI_createWorld() {
	World *world = new World();
	return world;
}

VI_Sky *VI_createSky(const char *filename,const char *ext) {
	SkyBox *sky = new SkyBox();
	/*if( !sky->init(filename,ext) ) {
		delete sky;
		return NULL;
	}*/
	sky->init(filename, ext);
	return sky;
}

VI_Sky *VI_createSky1(const char *filename) {
	SkyBox *sky = new SkyBox();
	/*if( !sky->init1(filename) ) {
		delete sky;
		return NULL;
	}*/
	sky->init1(filename);
	return sky;
}

VI_Sky *VI_createSky1(VI_Texture *texture) {
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	SkyBox *sky = new SkyBox();
	sky->init1(d_texture);
	return sky;
}

VI_Clouds *VI_createClouds() {
	Clouds *clouds = new Clouds();
	return clouds;
}

VI_Atmosphere *VI_createAtmosphere(VI_SceneGraphNode *light_entity) {
	if( GraphicsEnvironment::getSingleton()->getGraphics3D()->hasAtmosphereShaders() == NULL ) {
		return NULL; // not supported
	}

	//Entity *d_light_entity = static_cast<Entity *>(light_entity); // cast to the internal representation
	SceneGraphNode *d_light_entity = dynamic_cast<SceneGraphNode *>(light_entity); // cast to the internal representation
	Atmosphere *atmosphere = new Atmosphere(d_light_entity);
	return atmosphere;
}

VI_Terrain *VI_createTerrain(VI_GraphicsEnvironment *genv,int width,int depth,float xscale,float zscale,float hscale) {
	LOG("VI_createTerrain(%d,%d,%d,%f,%f,%f)\n",genv,width,depth,xscale,zscale,hscale);
	GraphicsEnvironment *d_genv = static_cast<GraphicsEnvironment *>(genv); // cast to the internal representation
	TerrainEngine *terrain = new TerrainEngine(d_genv->getGraphics3D(),xscale,zscale,hscale);
	terrain->create(width, depth);
	// no need to prepare(), as done automatically by create()
	return terrain;
}

VI_Terrain *VI_createTerrainRandom(VI_GraphicsEnvironment *genv,int width,int depth,float xscale,float zscale,float hscale,const int base_heights[4],int min_height,int max_height,float roughness,int seed) {
	LOG("VI_createTerrainRandom(%d,%d,%d,%f,%f,%f)\n",genv,width,depth,xscale,zscale,hscale);
	GraphicsEnvironment *d_genv = static_cast<GraphicsEnvironment *>(genv); // cast to the internal representation
	TerrainEngine *terrain = new TerrainEngine(d_genv->getGraphics3D(),xscale,zscale,hscale);
	terrain->createRandom(width, depth, base_heights, min_height, max_height, roughness, seed);
	// no need to prepare(), as done automatically by createRandom()
	LOG("done\n");
	return terrain;
}

VI_Image *VI_createImage(const char *filename) {
	Image2D *image = Image2D::loadImage(filename);
	return image;
}

VI_Image *VI_createImage(int w,int h) {
	Image2D *image = new Image2D(w, h);
	return image;
}

VI_Image *VI_Image::load(const char *filename) {
	Image2D *image = Image2D::loadImage(filename);
	return image;
}

VI_Image *VI_Image::createBlank(int w, int h, unsigned char r, unsigned char g, unsigned char b, bool alpha, unsigned char a) {
	Image2D *image = new Image2D(w, h, r, g, b, alpha, a);
	return image;
}

VI_Image *VI_Image::createNoise(int w,int h) {
	const unsigned char filter_max[3] = {255, 0, 0};
	const unsigned char filter_min[3] = {0, 0, 0};
	Image2D *image = Image2D::createNoise(w, h, 3.2f, 3.2f, filter_max, filter_min, V_NOISEMODE_PERLIN, 1, false);
	return image;
}

VI_Image *VI_Image::createNoise(int w,int h,float scale_u,float scale_v,const unsigned char filter_max[3],const unsigned char filter_min[3],V_NOISEMODE_t noisemode,int n_iterations,bool maximise) {
	Image2D *image = Image2D::createNoise(w, h, scale_u, scale_v, filter_max, filter_min, noisemode, n_iterations, maximise);
	return image;
}

VI_Image *VI_Image::createRadial(int w,int h,const unsigned char filter_max[3],const unsigned char filter_min[3],bool squared,bool alpha) {
	Image2D *image = Image2D::createRadial(w, h, filter_max, filter_min, squared, alpha);
	return image;
}

/*VI_Image *VI_createImageNoise(int w,int h) {
	//Image2D *image = Image2D::createNoise(w, h);
	const unsigned char filter_max[3] = {255, 0, 0};
	const unsigned char filter_min[3] = {0, 0, 0};
	Image2D *image = Image2D::createNoise(w, h, 3.2f, 3.2f, filter_max, filter_min, V_NOISEMODE_PERLIN, 1);
	return image;
}

VI_Image *VI_createImageNoise(int w,int h,float scale,const unsigned char filter_max[3],const unsigned char filter_min[3],V_NOISEMODE_t noisemode,int n_iterations) {
	Image2D *image = Image2D::createNoise(w, h, scale, scale, filter_max, filter_min, noisemode, n_iterations);
	return image;
}

VI_Image *VI_createImageNoise(int w,int h,float scale_u,float scale_v,const unsigned char filter_max[3],const unsigned char filter_min[3],V_NOISEMODE_t noisemode,int n_iterations) {
	Image2D *image = Image2D::createNoise(w, h, scale_u, scale_v, filter_max, filter_min, noisemode, n_iterations);
	return image;
}

VI_Image *VI_createImageRadial(int w,int h,const unsigned char filter_max[3],const unsigned char filter_min[3],bool squared,bool alpha) {
	Image2D *image = Image2D::createRadial(w, h, filter_max, filter_min, squared, alpha);
	return image;
}*/

VI_Texture *VI_createTexture(const char *filename) {
	Texture *tx = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(filename);
	return tx;
}

VI_Texture *VI_createTextureWithMask(const char *filename,unsigned char r,unsigned char g,unsigned char b) {
	//rgb_struct maskcol = { r, g, b };
	unsigned char maskcol[] = {r, g, b};
	Texture *tx = GraphicsEnvironment::getSingleton()->getRenderer()->loadTextureWithMask(filename, maskcol);
	return tx;
}

VI_Texture *VI_createTexture(VI_Image *image) {
	Texture *tx = GraphicsEnvironment::getSingleton()->getRenderer()->createTexture(image);
	return tx;
}

VI_Texture *VI_createTextureWithMask(VI_Image *image,unsigned char r,unsigned char g,unsigned char b) {
	unsigned char maskcol[] = {r, g, b};
	Texture *tx = GraphicsEnvironment::getSingleton()->getRenderer()->createTextureWithMask(image, maskcol);
	return tx;
}

VI_Billboard *VI_createBillboard(float width,float height,VI_Texture *texture,bool blend,int n_billboards) {
	Texture *d_texture = dynamic_cast<Texture *>(texture); // cast to the internal representation
	BillBoard *bb = new BillBoard(d_texture, width, height, blend, n_billboards);
	if( !Vision::isLocked() ) {
		bb->prepare();
	}
	return bb;
}

VI_Billboard *VI_loadBillboardWithMask(const char *filename,float width,float height,unsigned char r,unsigned char g,unsigned char b,int n_billboards) {
	LOG("V_BILLBOARD_load_with_mask(%s,%f,%f,%d,%d,%d,%d)\n",filename,width,height,r,g,b,n_billboards);

	//rgb_struct maskcol = { r, g, b };
	unsigned char maskcol[] = {r, g, b};
	Texture *tx = GraphicsEnvironment::getSingleton()->getRenderer()->loadTextureWithMask(filename, maskcol);

	BillBoard *bb = new BillBoard(tx, width, height, false, n_billboards);
	if( !Vision::isLocked() ) {
		bb->prepare();
	}
	return bb;
}

VI_SceneGraphNode *VI_createLight(VI_Light **light,float aR,float aG,float aB,float dR,float dG,float dB,float x,float y,float z) {
	Light *l = new Light(aR,aG,aB,dR,dG,dB);
	if( !Vision::isLocked() ) {
		l->prepare();
	}
	*light = l;
	//Entity *obj = new Entity(l);
	SceneGraphNode *obj = new SceneGraphNode();
	obj->setObject(l);
	Vector3D v(x, y, z);
	obj->setPosition(v);
	return obj;
}

VI_SceneGraphNode *VI_createExplosion(VI_Explosion **explosion,VI_Texture *texture,float aR,float aG,float aB,float dR,float dG,float dB,float x,float y,float z) {
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	Explosion *ex = new Explosion(d_texture,aR,aG,aB,dR,dG,dB);
	if( !Vision::isLocked() ) {
		ex->prepare();
	}
	*explosion = ex;
	//Entity *obj = new Entity(ex);
	SceneGraphNode *obj = new SceneGraphNode();
	obj->setObject(ex);
	Vector3D v(x, y, z);
	obj->setPosition(v);
	return obj;
}

/*VI_Entity *VI_createEntity() {
	return new Entity();
}*/

VI_SceneGraphNode *VI_createSceneGraphNode() {
	return new SceneGraphNode();
}

/*VI_Entity *VI_createEntity(VI_Object *obj) {
	return new Entity(obj);
}*/

VI_SceneGraphNode *VI_createSceneGraphNode(VI_Object *obj) {
	SceneGraphNode *node = new SceneGraphNode();
	node->setObject(obj);
	return node;
}

VI_ShadowPlane *VI_createShadowPlane(Vector3D normal,float D,bool shadow,bool reflection) {
	Plane plane;
	plane.normal = normal;
	plane.D = D;
	ShadowPlane *sp = new ShadowPlane(&plane, shadow, reflection, false);
	return sp;
}

VI_Panel *VI_createPanel() {
	Panel *panel = new Panel();
	return panel;
}

VI_Button *VI_createButton(const char *text,const VI_Font *font) {
	const FontBuffer *d_font = static_cast<const FontBuffer *>(font); // cast to the internal representation
	Button *button = new Button(text, d_font);
	return button;
}

VI_ImageButton *VI_createImageButton(VI_Texture *texture) {
	ImageButton *imagebutton = new ImageButton(texture);
	return imagebutton;
}

VI_ImageButton *VI_createImageButton(VI_Texture *texture, int width, int height) {
	ImageButton *imagebutton = new ImageButton(texture, width, height);
	return imagebutton;
}

VI_Cycle *VI_createCycle(char **entries,const VI_Font *font) {
	const FontBuffer *d_font = static_cast<const FontBuffer *>(font); // cast to the internal representation
	Cycle *cycle = new Cycle(entries, d_font);
	return cycle;
}

VI_Cycle *VI_createCycle(const VI_Font *font) {
	const FontBuffer *d_font = static_cast<const FontBuffer *>(font); // cast to the internal representation
	Cycle *cycle = new Cycle(NULL, d_font);
	return cycle;
}

VI_Listbox *VI_createListbox(int width,int height,char **entries,const VI_Font *font) {
	const FontBuffer *d_font = static_cast<const FontBuffer *>(font); // cast to the internal representation
	Listbox *listbox = new Listbox(width, height, entries, d_font);
	return listbox;
}

VI_Listbox *VI_createListbox(int width,int height,const std::string *entries,int n_entries,const VI_Font *font) {
	const FontBuffer *d_font = static_cast<const FontBuffer *>(font); // cast to the internal representation
	Listbox *listbox = new Listbox(width, height, entries, n_entries, d_font);
	return listbox;
}

VI_Stringgadget *VI_createStringgadget(const VI_Font *font,int width) {
	const FontBuffer *d_font = static_cast<const FontBuffer *>(font); // cast to the internal representation
	StringGadget *stringgadget = new StringGadget(d_font, width);
	return stringgadget;
}

VI_Textfield *VI_createTextfield(const VI_Font *font,int width,int height) {
	const FontBuffer *d_font = static_cast<const FontBuffer *>(font); // cast to the internal representation
	TextField *textfield = new TextField(d_font, width, height);
	return textfield;
}
