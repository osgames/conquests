#pragma once

#include "VisionIface.h"
#include "Resource.h"
#include "Object3D.h" // for ObjectData::RenderPhase
#include "Misc.h" // for Point3DChangedPositionListener

#include <string>
using std::string;

class GraphicsEnvironment;
class RenderQueue;
class World;
class SpatialIndex;
class Quadtree;
class SceneGraphNode;
class TransformationMatrix;

class Material {
	V_BLENDTYPE_t blendtype;
	bool force_color;
	unsigned char alpha;
	Color color;
	// TODO: force specular
public:
	Material() {
		this->blendtype = V_BLENDTYPE_FADE;
		this->force_color = false;
		this->alpha = 255;
	}

	void setBlendtype(V_BLENDTYPE_t blendtype) {
		this->blendtype = blendtype;
	}
	void force(unsigned char alpha,Color color) {
		this->force_color = true;
		this->alpha = alpha;
		this->color = color;
	}
	void unforce() {
		this->force_color = false;
	}

	V_BLENDTYPE_t getBlendtype() const {
		return this->blendtype;
	}
	bool isColorForced() const {
		return this->force_color;
	}
	unsigned char getAlpha() const {
		return this->alpha;
	}
	Color *getColor() {
		return &this->color;
	}
	const Color *getColor() const {
		return &this->color;
	}
};

class PositionKey {
public:
	float time;
	Vector3D pos;

	PositionKey(float time, Vector3D pos) {
		this->time = time;
		this->pos = pos;
	}
	PositionKey(PositionKey *from) {
		*this = *from;
	}
	virtual size_t memUsage() const {
		size_t size = sizeof(this);
		return size;
	}
};

class RotationKey {
public:
	float time;
	Rotation3D rot;

	RotationKey(float time, Rotation3D rot) {
		this->time = time;
		this->rot = rot;
	}
	RotationKey(RotationKey *from) {
		*this = *from;
	}
	virtual size_t memUsage() const {
		size_t size = sizeof(this);
		return size;
	}
};

class AnimationInfo : public VI_HAnimationInfo {
	string name;
public:
	/*vector<PositionKey *> *position_keys;
	vector<RotationKey *> *rotation_keys;*/
	/*vector<PositionKey *> position_keys;
	vector<RotationKey *> rotation_keys;*/
	vector<PositionKey> position_keys;
	vector<RotationKey> rotation_keys;
	float start, end;

	AnimationInfo();
	AnimationInfo(AnimationInfo *from);
	virtual ~AnimationInfo();
	virtual size_t memUsage() const;

	virtual bool equals(const char *name) const;
	virtual Vector3D getPosition(float time) const;
	virtual Rotation3D getRotation(float time) const;

	// interface
	virtual void setName(const char *name);
	virtual const char *getName() const {
		return this->name.c_str();
	}
	virtual void addPositionKey(float time,Vector3D pos);
	virtual void addRotationKey(float time,Rotation3D rot);
};

class Particle {
public:
	Vector3D pos;
	Vector3D velocity;
	Vector3D acceleration;

	float size;
	Color color;
	unsigned char alpha;
	int birthTime_ms;

	Particle();

	void reset();
};

typedef void (VI_Entity_Shader_Callback) (const GraphicsEnvironment *genv, const ShaderEffect *shader, const SceneGraphNode *node);

class SceneGraphNode : public VisionObject, public virtual VI_SceneGraphNode, public Point3DChangedPositionListener {
	friend class ParticleSystem;
private:
	virtual void inheritHAnimationFrom(VI_SceneGraphNode *from,bool top);
protected:
	ObjectData *obj;

	World *world;
	vector<SceneGraphNode *> children;
	SceneGraphNode *parent;
	SpatialIndex *spatialindex;
	float animationspeed;
	//Sphere superboundingsphere;
	bool moving;
	Point3D velocity;
	Point3D force;
	float mass;
	bool expire_when_ps_done;

	Point3D point;
	Point3D old_point;
	bool has_default_hanimation;
	Point3D hanimation_def_point; // used for default value if requested hierarchical animation not present

	bool loop_frames;

	VI_SceneGraphNode_Update *updateFunc;
	void *updateData;

	TransformationMatrix *matrix;

	bool requires_update;
	bool kill_later;
	bool visible;

	vector<AnimationInfo *> animationinfos;
	AnimationInfo *c_animationinfo;
	float animationtime;
	V_SOURCETYPE_t sourcetype;
	map<string, float> properties_float;

	int time_existed_ms; // how old

	float texture_x, texture_y;
	float texture_x_sp, texture_y_sp;

	Particle *particles; // for particle systems
	int n_particles;
	bool emit_new_particles;

	int vertexanim_state;
	int vertexanim_start_frame;
	int vertexanim_end_frame;
	int vertexanim_c_frame;
	float vertexanim_frame_fraction;
	float vertexanim_frame_speed;

	Material material; // to possibly override anything specified by the ObjectData
	float range;

	VI_Entity_Shader_Callback *shader_callback;

	//void calculateSuperBoundingRadius();
	//void calculateSuperBoundingSphere();
	virtual bool hasActiveParticleSystem() const;
	virtual bool getThisExtent(float *low,float *high,const Vector3D &n,bool firstFrameOnly) const;
public:

	SceneGraphNode();
	//SceneGraphNode(SceneGraphNode *from);

	virtual ~SceneGraphNode();

	virtual V_CLASS_t getClass() const { return V_CLASS_SCENEGRAPHNODE; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_SCENEGRAPHNODE );
	}
	virtual size_t memUsage();

	virtual void setOldPosition(const Vector3D &v) {
		this->old_point.moveTo(v);
	}
	/*virtual Rotation3D *rotation() {
	return &this->point.rot;
	}*/
	Point3D *getPoint() {
		return &this->point;
	}
	const Point3D *getPoint() const {
		return &this->point;
	}
	virtual void multGLMatrix() const;
	virtual void multGLMatrixInverse() const {
		this->point.multGLMatrixInverse();
	}
	virtual void calculateUpdateRequired();
	virtual void setWorld(World *world);

	/*virtual void setName(const char *name);
	virtual const char *getName() const {
		return this->name.c_str();
	}*/
	virtual bool equals(const char *name) const;

	virtual bool renderRequired(GraphicsEnvironment *genv);
	virtual void render(GraphicsEnvironment *genv, ObjectData::RenderPhase phase, bool lighting_pass, bool use_shadowmesh, RenderData *renderData) /*const*/;
	virtual void renderQueue(RenderQueue *queue, bool outofview) /*const*/;
	virtual bool hasLight() const;
	virtual void getShadows(RenderQueue *queue, GraphicsEnvironment *genv, SceneGraphNode *skip);
	virtual void castShadow(Renderer *renderer, GraphicsEnvironment *genv, const Vector3D &lightpos, bool infinite) const;
	virtual const AnimationInfo *getCAnimationInfo() const {
		return this->c_animationinfo;
	}

	virtual bool doKills();
	/*virtual Vector3D *getRenderedWorldPos() {
	return &worldpos;
	}*/
	/*virtual void setRenderedWorldPos(Vector3D *pos) {
	this->worldpos = *pos;
	}*/
	virtual Vector3D localToEyePos(const Vector3D &pos) const;
	virtual ShadowPlane *getShadowPlane();
	virtual const TransformationMatrix *getMatrix() const {
		return this->matrix;
	}
	virtual TransformationMatrix *getMatrix() {
		return this->matrix;
	}
	virtual void setMatrix(TransformationMatrix *matrix) {
		this->matrix = matrix;
	}
	virtual SpatialIndex *getSpatialIndex() {
		return this->spatialindex;
	}
	virtual const SpatialIndex *getSpatialIndex() const {
		return this->spatialindex;
	}
	virtual void setSpatialIndex(SpatialIndex *spatialindex) {
		this->spatialindex = spatialindex;
	}
	/*void initAnimationInfos() {
		if( this->animationinfos == NULL ) {
			this->animationinfos = new vector<AnimationInfo *>();
		}
	}*/
	virtual bool emitsNewParticles() const {
		return this->emit_new_particles;
	}
	virtual int emitParticle(int n);
	virtual int getNParticles() const {
		return this->n_particles;
	}
	Particle *getParticles() {
		return this->particles;
	}
	const Particle *getParticles() const {
		return this->particles;
	}
	void killParticle(int i) {
		this->particles[i] = this->particles[--this->n_particles];
	}
	virtual int getTimeExistedMS() const {
		return this->time_existed_ms;
	}

	// Point3DChangedPositionListener
	virtual void changedPosition();

	virtual int getCVertexAnimFrame() const {
		return this->vertexanim_c_frame;
	}
	virtual void setCVertexAnimFrame(int c_frame) {
		this->vertexanim_c_frame = c_frame;
	}
	float getVertexAnimFrameFraction() const {
		return this->vertexanim_frame_fraction;
	}
	virtual int getVertexAnimEndFrame() const {
		return this->vertexanim_end_frame;
	}
	virtual void setVertexAnimFrames();

	const Material *getMaterial() const {
		return &this->material;
	}
	/*Material *getMaterial() {
		return &this->material;
	}*/

	void setShaderCallback(VI_Entity_Shader_Callback *shader_callback) {
		this->shader_callback = shader_callback;
	}
	virtual void collidePlane(const Vector3D &normal,float mew,float cor);

	//static void moveForwards(World *world,SceneGraphNode *node);

	// interface
	virtual VI_SceneGraphNode *copy() const;
	virtual VI_Object *getObject() const {
		return obj;
	}
	virtual void setObject(VI_Object *v_obj);
	virtual Vector3D getPosition() const {
		bool is_infinite = false;
		Vector3D pos = this->point.getPosition(&is_infinite);
		return pos;
	}
	virtual Vector3D getOldPosition() const {
		bool is_infinite = false;
		return this->old_point.getPosition(&is_infinite);
	}
	virtual Vector3D getCurrentWorldPos() const;
	virtual Vector3D getLastRenderedEyePos() const;
	virtual void localToWorldMatrix(Matrix4d *out_matrix) const;
	virtual Vector3D localToWorldPos(const Vector3D &pos) const;
	//virtual Vector3D worldToLocalPos(Vector3D pos) const;
	virtual Vector3D worldToLocalDir(const Vector3D &dir) const;
	virtual Rotation3D getRotation() const {
		return this->point.getRotation();
	}
	virtual bool getExtent(float *low,float *high,const Vector3D &p,const Vector3D &n,bool firstFrameOnly,bool do_children) const;
	virtual bool isVisible() const {
		return this->visible;
	}
	virtual V_SOURCETYPE_t getSourcetype() const {
		return this->sourcetype;
	}
	virtual void setSourcetype(V_SOURCETYPE_t sourcetype) {
		this->sourcetype = sourcetype;
	}

	virtual void setPosition(float x,float y,float z) {
		this->point.moveTo(x, y, z);
	}
	virtual void setInfinite(bool infinite) {
		this->point.setInfinite(infinite);
	}
	virtual void setPosition(const Vector3D &v) {
		this->point.moveTo(v);
	}
	virtual void move(float x,float y,float z) {
		this->point.move(x, y, z);
	}
	virtual void move(Vector3D *v) {
		this->point.move(v);
	}
	virtual void moveDirection(Vector3D v,float d) {
		this->point.moveDirection(v, d);
	}
	virtual void moveDirectionY(Vector3D v,float d) {
		this->point.moveDirectionY(v, d);
	}
	virtual void setRotation(const Rotation3D &rotation) {
		this->point.rotateTo(rotation);
	}
	virtual void rotate(float x,float y,float z) {
		this->point.rotate(x, y, z);
	}
	virtual void rotateLocal(float x,float y,float z) {
		this->point.rotateLocal(x, y, z);
	}
	virtual void rotateToEuler(float x,float y,float z) {
		this->point.rotateToEuler(x, y, z);
		//this->calculateSuperBoundingSphere();
	}
	virtual void rotateToDirection(const Vector3D &axis, const Vector3D &dir) {
		this->point.rotateToDirection(axis, dir);
	}
	virtual void rotateToDirection(const Vector3D &dir) {
		this->point.rotateToDirection(dir);
	}
	virtual void setVisible(bool visible) {
		this->visible = visible;
	}

	virtual void addChildNode(VI_SceneGraphNode *node);
	virtual size_t getNChildNodes() const;
	virtual VI_SceneGraphNode *getChildNode(size_t i);
	virtual const VI_SceneGraphNode *getChildNode(size_t i) const;
	virtual VI_SceneGraphNode *findChildNode(const char *name);
	virtual bool removeChildNode(VI_SceneGraphNode *node);
	virtual void removeAllChildNodes();
	virtual VI_SceneGraphNode *getParentNode() const {
		return this->parent;
	}
	virtual void detachFromParentNode();

	virtual void kill();
	virtual void setVertexFrameSpeed(float frame_speed, bool do_children);
	virtual void setVertexAnimState(int state, bool do_children);
	virtual void setVertexAnimFrames(int start_frame, int end_frame, bool do_children);
	virtual int getVertexAnimState() const {
		return vertexanim_state;
	}
	virtual void setAnimLoop(bool loop, bool do_children);
	virtual void setSpeculari(unsigned char r, unsigned char g, unsigned char b, bool do_children);
	virtual void setShadowCaster(bool enabled,bool do_children);
	virtual void enableParticlesystem(bool enabled,bool do_children);
	virtual void expireWhenPsDone(bool enabled) {
		this->expire_when_ps_done = enabled;
	}
	virtual void scale(float sx,float sy,float sz, bool do_children);
	virtual void forceColori(unsigned char r, unsigned char g, unsigned char b, unsigned char alpha, bool do_children);
	virtual void unforce(bool do_children);
	virtual void getForceColori(V_BLENDTYPE_t *blendtype,unsigned char *r,unsigned char *g,unsigned char *b,unsigned char *alpha) const;
	virtual void setBlendtype(V_BLENDTYPE_t blendtype, bool do_children);
	virtual void inheritHAnimationFrom(VI_SceneGraphNode *from);
	virtual bool setHAnimation(const char *animname, bool forcereset);
	virtual void setHAnimationSpeed(float animationspeed);
	virtual void addHAnimationInfo(VI_HAnimationInfo *animationinfo);
	virtual float getHAnimationTime() const {
		return this->animationtime;
	}
	virtual void setHAnimationTime(float animationtime, bool do_children);
	virtual void adoptHAnimation(bool do_children);
	virtual void setDefaultHAnimationPosition(bool do_children);
	virtual void setUpdateFunc(VI_SceneGraphNode_Update *updateFunc, void *updateData) {
		this->updateFunc = updateFunc;
		this->updateData = updateData;
		calculateUpdateRequired();
	}
	/*virtual void *getUpdateData() {
		return this->updateData;
	}*/

	virtual bool isAnimationFinished() const;

	virtual void setAllToIdentity(bool do_this);
	virtual void setVelocity(float x,float y,float z) {
		//this->velocity.pos.set(x,y,z);
		this->velocity.moveTo(x,y,z);
		//checkMoving();
		calculateUpdateRequired();
	}
	virtual void setForce(float x,float y,float z) {
		//this->force.pos.set(x,y,z);
		this->force.moveTo(x,y,z);
		//checkMoving();
		calculateUpdateRequired();
	}
	/*virtual void getVelocity(float *x,float *y,float *z) const {
		bool is_infinite = false;
		Vector3D pos = this->velocity.getPosition(&is_infinite);
		*x = pos.x;
		*y = pos.y;
		*z = pos.z;
	}
	virtual void getForce(float *x,float *y,float *z) const {
		bool is_infinite = false;
		Vector3D pos = this->force.getPosition(&is_infinite);
		*x = pos.x;
		*y = pos.y;
		*z = pos.z;
	}*/
	virtual Vector3D getVelocity() const {
		bool is_infinite = false;
		Vector3D pos = this->velocity.getPosition(&is_infinite);
		return pos;
	}
	virtual Vector3D getForce() const {
		bool is_infinite = false;
		Vector3D pos = this->force.getPosition(&is_infinite);
		return pos;
	}
	virtual void halt(); // o3p

	virtual bool collision(const VI_SceneGraphNode *node) const;
	virtual bool collisionLine(float *intersection, const Vector3D &pos, const Vector3D &dir, float line_thickness) const;
	virtual bool collisionLineSeg(float *intersection, const Vector3D &p0, const Vector3D &p1, float line_thickness) const;

	virtual void update(int time_ms, bool do_children);

	/*virtual float getBoundingRadius() const;
	virtual float getBoundingRadiusSq() const;*/
	virtual Sphere getBoundingSphere() const;
	//virtual float getSuperBoundingRadius() const { return superboundingradius; }
	/*virtual Sphere getSuperBoundingSphere() const {
		return superboundingsphere;
	}*/
	virtual Sphere calculateSuperBoundingSphere() const;
	virtual Sphere calculateWorldSuperBoundingSphere() const;

	virtual void setPropertyFloat(const char *name, float value) {
		this->properties_float[name] = value;
	}
	virtual float getPropertyFloat(const char *name) const {
		//float value = this->properties_float[name];
		map<string, float>::const_iterator iter = this->properties_float.find(name);
		if( iter == this->properties_float.end() )
			return 0;
		return iter->second;
	}
	virtual void setTextureSpeed(float texture_x_sp, float texture_y_sp) {
		this->texture_x_sp = texture_x_sp;
		this->texture_y_sp = texture_y_sp;
		calculateUpdateRequired();
	}
};

/*class Entity : public SceneGraphNode, public VI_Entity {
	//friend class ParticleSystem;

protected:

	//void init();
	//virtual bool getThisExtent(float *low,float *high,const Vector3D &n,bool firstFrameOnly) const;
public:
	//V_Update *update_tag;

	Entity();
	Entity(VI_Object *od);
	virtual ~Entity();

	virtual V_CLASS_t getClass() const { return V_CLASS_ENTITY; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_ENTITY || SceneGraphNode::isSubClass(v_class) );
	}

	// interface

	//virtual VI_Entity *copyEntity() const;
};*/

/*class SpatialGrid2D : public SceneGraphNode {
public:
SpatialGrid2D();
virtual ~SpatialGrid2D();
virtual V_CLASS_t getClass() const { return V_CLASS_SPATIALGRID2D; };
virtual bool isSubClass(V_CLASS_t v_class) {
return ( this->getClass() == v_class || SceneGraphNode::isSubClass(v_class) );
}
};*/

class SpatialIndex {
protected:
	float x, y, z;

	//void renderVector(Vector *vector,Graphics3D *g,Graphics3D::RenderPhase phase,bool lighting_pass);
	//void buildRenderQueueVector(Vector *vector,Graphics3D *g);
	void buildRenderQueueVector(vector<SceneGraphNode *> *vector, GraphicsEnvironment *genv);
public:
	virtual void addNode(SceneGraphNode *node)=0;
	virtual void updateNode(SceneGraphNode *node);
	virtual void removeNode(SceneGraphNode *node)=0;
	//virtual void render(Graphics3D *g,Graphics3D::RenderPhase phase,bool lighting_pass)=0;
	virtual void buildRenderQueue(GraphicsEnvironment *genv)=0;
	virtual size_t getNodeUnoptCount() const { return 0; }
};

class SpatialGrid2D : public SpatialIndex {
	float width, depth, height;
	float radius;
	int n_x, n_z;
	bool *visible;
	vector<SceneGraphNode *> **nodes;
	vector<SceneGraphNode *> *node_other;
	//GLuint *occlusion_queries;

public:
	SpatialGrid2D(float x, float y, float z, float width, float height, float depth, int n_x, int n_z);
	virtual ~SpatialGrid2D();
	virtual void addNode(SceneGraphNode *node);
	virtual void removeNode(SceneGraphNode *node);
	//virtual void render(Graphics3D *g,Graphics3D::RenderPhase phase,bool lighting_pass);
	virtual void buildRenderQueue(GraphicsEnvironment *genv);
	virtual size_t getNodeUnoptCount() const;
};

class Quadtreenode {
public:
	Quadtree *quadtree;
	vector<SceneGraphNode *> *nodes;
	int level;
	int index_x, index_z;
	Quadtreenode *parent;
	Quadtreenode *children[4];
	float x, y, z;
	float width, height, depth;
	bool dont_recurse_lower;

	Quadtreenode(Quadtree *quadtree,int level,bool right,bool bottom,Quadtreenode *parent);
	void set(float x,float y,float z,float width,float height,float depth) {
		this->x = x;
		this->y = y;
		this->z = z;
		this->width = width;
		this->height = height;
		this->depth = depth;
	}
	void distFrom(float *dx,float *dz,float px,float pz) const {
		*dx = 0;
		*dz = 0;
		if( px < x )
			*dx = x - px;
		else if( px > x+width )
			*dx = px - x - width;
		if( pz < z )
			*dz = z - pz;
		else if( pz > z+depth )
			*dz = pz - z - depth;
	}
	~Quadtreenode() {
		delete this->nodes;
		for(int i=0;i<4;i++)
			delete this->children[i];
	}

	void createChildren(int levels) {
		if( levels == 1 )
			return;
		for(int i=0;i<4;i++) {
			this->children[i] = new Quadtreenode(quadtree, level+1, (i%2)==1, i/2==1, this);
		}
		this->children[0]->set(x, y, z, width/2.0f, height, depth/2.0f);
		this->children[1]->set(x+width/2.0f, y, z, width/2.0f, height, depth/2.0f);
		this->children[2]->set(x, y, z+depth/2.0f, width/2.0f, height, depth/2.0f);
		this->children[3]->set(x+width/2.0f, y, z+depth/2.0f, width/2.0f, height, depth/2.0f);
		for(int i=0;i<4;i++) {
			this->children[i]->createChildren(levels-1);
		}
	}
	bool addNode(SceneGraphNode *node);
	bool removeNode(SceneGraphNode *node);
	//void render(Graphics3D *g,Graphics3D::RenderPhase phase,bool lighting_pass);
	void buildRenderQueue(GraphicsEnvironment *genv);
	bool isVisible(GraphicsEnvironment *genv);
};

class Quadtree : public SpatialIndex {
	friend class Quadtreenode;
	float width, depth, height;
	vector<SceneGraphNode *> *node_other;

	bool addNodeLevel(SceneGraphNode *node,int level,int n);
public:
	Quadtreenode *quadtreenodes;
	int levels;

	Quadtree(float x, float y, float z, float width, float height, float depth, int levels);
	virtual ~Quadtree();
	virtual void addNode(SceneGraphNode *node);
	virtual void removeNode(SceneGraphNode *node);
	//virtual void render(Graphics3D *g,Graphics3D::RenderPhase phase,bool lighting_pass);
	void buildRenderQueue(GraphicsEnvironment *genv);
	virtual size_t getNodeUnoptCount() const;
};
