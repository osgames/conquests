#pragma once

#include "VisionUtils.h"
#include "VisionShared.h"
#include "Object3D.h"

class Color;
class Renderer;
class GraphicsEnvironment;
class Texture;
class VertexArray;

//const int MAX_SPLATS = 4;

class RenderData : public RenderableInfo {
public:
	static const int MAX_TEXTURES = 5;
protected:
	//friend unsigned __stdcall castShadow_thread(void *ptr);
	friend void castShadow_thread(void *ptr);

	int n_textures;
	Texture *textures[MAX_TEXTURES];

	int last_frame;
	float last_frac;

	void renderCore(Renderer *renderer, GraphicsEnvironment *genv, int frame, int frame2, float frac/*, bool force_color, const Color *forced_color, unsigned char forced_alpha*/);
	//void doArrays(Graphics3D *g3);

	// Help data
	/*float *faces_planes; // size 4 * n_frames * n_polys
	int *faces_adj_faces; // size n_renderlength
	bool *faces_boundary; // size n_renderlength
	bool *edges_skip; // size 3 * n_polys; can we skip this edge, for the shadow extrusions?
	bool *visible_polys; // size n_polys; work data for is a poly visible
	unsigned short *shadow_edges; // temp buffer for which edges to extrude (NEW algorithm only)
	bool *shadow_vertices_used;*/ // temp buffer for which vertices to extrude (NEW algorithm only)
	vector<float> faces_planes; // size 4 * n_frames * n_polys
	vector<int> faces_adj_faces; // size n_renderlength
	vector<unsigned char> faces_boundary; // (bool) size n_renderlength
	vector<unsigned char> edges_skip; // (bool) size 3 * n_polys; can we skip this edge, for the shadow extrusions?
	vector<unsigned char> visible_polys; // (bool) size n_polys; work data for is a poly visible
	vector<unsigned short> shadow_edges; // temp buffer for which edges to extrude (NEW algorithm only)
	vector<unsigned char> shadow_vertices_used; // (bool) temp buffer for which vertices to extrude (NEW algorithm only)
	bool shadow_closed;
	void clearHelpData();

	// shadow volume vertex arrays
	bool need_shadows;
	VertexArray *vao_shadow_vertices;
	/*float *va_shadow_vertices;
	float *va_shadow_normals;*/
	vector<float> va_shadow_vertices;
	vector<float> va_shadow_normals;
	VertexArray *vao_shadow_indices;
	//unsigned short *va_shadow_indices;
	vector<unsigned short> va_shadow_indices;
	int n_shadow_renderlength;

	bool need_tangents;
	int tangent_uv_channel;

	bool uses_hardware_animation;
	int hardware_animation_channel;

	int n_frames;
	unsigned short n_renderlength;
	float material_specular[4];

	bool solid;
	bool blending;
	V_BLENDTYPE_t blend_type;
	//bool alpha_channel;

	size_t n_vertices;

	// vertex arrays
	VertexArray *vao_vertices;
	VertexArray *vao_vertices_stream;
	VertexArray *vao_normals;
	VertexArray *vao_tangents;
	VertexArray *vao_texcoords;
	VertexArray *vao_colors;
	VertexArray *vao_indices;
	// raw local data
	vector<float> va_vertices;
	vector<float> va_vertices_stream;
	vector<float> va_normals;
	vector<float> va_tangents;
	vector<float> va_texcoords;
	vector<unsigned char> va_colors;
	vector<unsigned short> va_indices;

	static void interpolateVertices_thread(void *ptr);
	void freeShadowData();
	void castShadowSetVisible(int frame, const Vector3D &lightpos, bool infinite, bool shader);
	int findShadowEdges();
	static void shadowExtrudeVertices_thread(void *ptr);
	void castShadowConstructVertices(int frame, int frame2, float frac, const Vector3D &lightpos, bool infinite);
	void castShadowConstructIndices(int n_shadow_edges);
	int castShadowConstruct(int frame, int frame2, float frac, const Vector3D &lightpos, bool infinite, bool shader);
	void renderShadow(Renderer *renderer, GraphicsEnvironment *genv, int n_shadow_vertices) const;
public:
	//Texture *texture;
	//Texture *secondary_texture;
	//Texture *bump_texture;
	/*int n_splats; // nb - if using splats, we can't have secondary or bump textures
	Texture *splat_textures[MAX_SPLATS]; // 0th splat is the alphamap*/
	//float splat_detail_scale;

	//bool splatting; // splatting means drawing on top of a base mesh with alpha blending
	// kind of like the blending flag, but it's draw in the opaque pipeline!
	//unsigned char cols[4]; // specifies RGBA colour for vertex buffers if colour array not set

	void free();

	RenderData();
	~RenderData() {
		free();
	}
	virtual size_t memUsage();
	void initVertexArrays(int n_frames,size_t n_vertices,/*bool colors,bool alpha_channel,*/bool normals/*,bool texcoords*//*,bool texcoords_secondary = false*/);

	int getNFrames() const {
		return this->n_frames;
	}
	unsigned short getRenderlength() const {
		return n_renderlength;
	}

	void setRenderlength(bool allocate_indices, int n_renderlength);
	// getMaterialSpecular is part of RenderableInfo interface, below
	void setMaterialSpecular(const float material_specular[4]) {
		for(int i=0;i<4;i++) {
			this->material_specular[i] = material_specular[i];
		}
	}

	bool compatible(const RenderData *that) const;
	void freeVBOs();
	void simplify();
	void shareVertices();
	void compact();
	void combine(const RenderData *that);
	//static RenderData *createShadowMesh(const RenderData *rd, int n_rd);
	RenderData *copy(bool need_shadows,bool colors,bool normals,bool texcoords) const;

	//void setBumpTexture(Texture *bump_texture
	void setNeedTangents(bool need_tangents, int tangent_uv_channel) {
		this->need_tangents = need_tangents;
		this->tangent_uv_channel = need_tangents ? tangent_uv_channel : -1;
	}
	void setHardwareAnimation(bool uses_hardware_animation, int hardware_animation_channel) {
		this->uses_hardware_animation = uses_hardware_animation;
		this->hardware_animation_channel = uses_hardware_animation ? hardware_animation_channel : -1;
		if( uses_hardware_animation && n_frames == 1 ) {
			Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "hardware animation not allowed for only 1 frame"));
		}
	}
	void setNTextures(int n_textures) {
		this->n_textures = n_textures;
	}
	int getNTextures() const {
		return this->n_textures;
	}
	void setTexture(int index, Texture *texture);
	Texture *getTexture(int index) const;

	void setNVertices(size_t n_vertices) {
		this->n_vertices = n_vertices;
	}
	size_t getNVertices() const {
		return this->n_vertices;
	}
	void setVertex(int frame, size_t indx, const Vector3D &vec) {
		ASSERT( frame >= 0 && frame < n_frames );
		ASSERT( indx >= 0 && indx < n_vertices );
		this->va_vertices[3*n_vertices*frame + 3*indx + 0] = vec.x;
		this->va_vertices[3*n_vertices*frame + 3*indx + 1] = vec.y;
		this->va_vertices[3*n_vertices*frame + 3*indx + 2] = vec.z;
	}
	Vector3D getVertex(int frame,size_t indx) const {
		ASSERT( frame >= 0 && frame < n_frames );
		ASSERT( indx >= 0 && indx < n_vertices );
		Vector3D vec;
		vec.x = this->va_vertices[3*n_vertices*frame + 3*indx + 0];
		vec.y = this->va_vertices[3*n_vertices*frame + 3*indx + 1];
		vec.z = this->va_vertices[3*n_vertices*frame + 3*indx + 2];
		return vec;
	};
	void setNormal(int frame, size_t indx, const Vector3D &vec) {
		ASSERT( frame >= 0 && frame < n_frames );
		ASSERT( indx >= 0 && indx < n_vertices );
		this->va_normals[3*n_vertices*frame + 3*indx + 0] = vec.x;
		this->va_normals[3*n_vertices*frame + 3*indx + 1] = vec.y;
		this->va_normals[3*n_vertices*frame + 3*indx + 2] = vec.z;
	}
	Vector3D getNormal(int frame, size_t indx) const {
		ASSERT( frame >= 0 && frame < n_frames );
		ASSERT( indx >= 0 && indx < n_vertices );
		Vector3D vec;
		vec.x = this->va_normals[3*n_vertices*frame + 3*indx + 0];
		vec.y = this->va_normals[3*n_vertices*frame + 3*indx + 1];
		vec.z = this->va_normals[3*n_vertices*frame + 3*indx + 2];
		return vec;
	}
	void setTangent(int frame, size_t indx, const Vector3D &vec) {
		ASSERT( frame >= 0 && frame < n_frames );
		ASSERT( indx >= 0 && indx < n_vertices );
		this->va_tangents[3*n_vertices*frame + 3*indx + 0] = vec.x;
		this->va_tangents[3*n_vertices*frame + 3*indx + 1] = vec.y;
		this->va_tangents[3*n_vertices*frame + 3*indx + 2] = vec.z;
	}
	Vector3D getTangent(int frame, size_t indx) const {
		ASSERT( frame >= 0 && frame < n_frames );
		ASSERT( indx >= 0 && indx < n_vertices );
		Vector3D vec;
		vec.x = this->va_tangents[3*n_vertices*frame + 3*indx + 0];
		vec.y = this->va_tangents[3*n_vertices*frame + 3*indx + 1];
		vec.z = this->va_tangents[3*n_vertices*frame + 3*indx + 2];
		return vec;
	}
	void setTexCoord(size_t indx, float u, float v) {
		ASSERT( indx >= 0 && indx < n_vertices );
		this->va_texcoords[2*indx + 0] = u;
		this->va_texcoords[2*indx + 1] = v;
	}
	Vector3D getTexCoord(size_t indx) const {
		ASSERT( indx >= 0 && indx < n_vertices );
		Vector3D vec;
		vec.x = this->va_texcoords[2*indx + 0];
		vec.y = this->va_texcoords[2*indx + 1];
		vec.z = 0.0;
		return vec;
	};
	void setColor(size_t indx, int component, unsigned char value) {
		ASSERT( indx >= 0 && indx < n_vertices );
		int dim = getDim();
		ASSERT( component >= 0 && component < dim );
		this->va_colors[dim*indx + component] = value;
	}
	unsigned char getColor(size_t indx, int component) const {
		ASSERT( indx >= 0 && indx < n_vertices );
		int dim = getDim();
		ASSERT( component >= 0 && component < dim );
		return this->va_colors[dim*indx + component];
	}
	void setVaIndex(unsigned short indx, unsigned short value) {
		ASSERT( indx >= 0 && indx < n_renderlength );
		ASSERT( this->va_indices.size() != 0 );
		this->va_indices[indx] = value;
	}
	void addVaIndex(unsigned short value) {
		this->va_indices.push_back(value);
	}
	void clearVaIndices() {
		this->va_indices.clear();
	}
	unsigned short getVaIndex(unsigned short indx) const {
		ASSERT( indx >= 0 && indx < n_renderlength );
		//if( this->va_indices == NULL )
		if( this->va_indices.size() == 0 )
			return indx;
		return this->va_indices[indx];
	}

	bool hasAlphaChannel() const {
		//return this->alpha_channel;
		return true;
	}
	int getDim() const {
		//return this->alpha_channel ? 4 : 3;
		return 4;
	}
	void setSolid(bool solid) {
		this->solid = solid;
	}
	void setBlending(bool blending) {
		this->blending = blending;
	}
	bool isBlending() const {
		return this->blending;
	}
	void setBlendType(V_BLENDTYPE_t blend_type) {
		this->blend_type = blend_type;
	}

	void prepare(bool need_shadows);
	void render(Renderer *renderer, GraphicsEnvironment *genv, int frame, int frame2, float frac, /*bool force_color, const Color *forced_color, unsigned char forced_alpha,*/ bool lighting_pass);
	void castShadow(Renderer *renderer, GraphicsEnvironment *genv, int frame, int frame2, float frac, const Vector3D &lightpos, bool infinite, bool shader);

	// RenderableInfo interface
	virtual Texture *getFirstTexture() const {
		if( n_textures > 0 )
			return textures[0];
		return NULL;
	}
	virtual const float *getMaterialSpecular() const {
		return material_specular;
	}
};
