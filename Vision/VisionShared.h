#pragma once

#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include <cmath>

#include <string>
using std::string;

//extern bool x_DEBUG;
//const bool V_DEBUG = true;

bool V_logText(const char *text,...);

#ifndef V_log
//#define V_log x_DEBUG = V_DEBUG && V_logText
#define V_log V_logText
#endif

void V_assert_failed();

#ifndef ASSERT
#define ASSERT(test) {                                 \
        bool v = test;                                 \
        if( v )                                        \
                ((void)0);                             \
        else {                                         \
                V_log("ASSERTION FAILED:\n");          \
                V_log("%s\n", #test);                  \
				V_log("File: %s\n", __FILE__);         \
				V_log("Line: %d\n", __LINE__);         \
				V_assert_failed();                     \
				throw "Assertion failure";             \
        }                                              \
}
#endif

#ifndef T_ASSERT
//#define T_ASSERT(test) ASSERT(test) // for testing
#define T_ASSERT(test) // for release
#endif

/*#define VISION_ERROR(err, code, text)                      \
	Vision::setError(new VisionException(NULL,code,text)); \
	return err;*/
#define VISION_ERROR(err, code, text)                      \
	Vision::setError(new VisionException(NULL,code,text));

#define ERROR_INTERFACE(err) VISION_ERROR(err, VisionException::V_INTERFACE_ERROR, "Vision Interface Error")

typedef size_t V_TAG_t;

class VisionException {
public:
	enum ExceptionType {
		V_GENERAL_ERROR,
		V_PROGRAMMING_ERROR, // something which shouldn't happen, and is a bug!

		V_ALREADY_INITIALISED,
		V_NOT_INITIALISED,
		V_TOO_MANY_GRAPHICSENVIRONMENT_CLASSES,
		V_NO_GRAPHICSENVIRONMENT,
		V_DISPLAYMODE_NOT_AVAILABLE,
		V_FAILED_TO_OPEN_WINDOW,
		V_FAILED_TO_INIT_3D,
		V_GL_ERROR,
		V_DIRECT3D_ERROR,
		V_DIRECT3D_LOSTDEVICE,
		V_CG_ERROR,
		V_FONT_ERROR,
		V_PREFS_ERROR,

		V_RENDERER_INVALID_STATE,
		V_RENDERER_NOT_SUPPORTED,

		V_INVALID_VISIONOBJECT,

		V_SOUND_ERROR,

		V_INPUT_ERROR,

		V_FILE_ERROR,
		V_RENDER_ERROR,
		V_IMPORT_VISION_MESH_ERROR,
		V_IMPORT_3DS_ERROR,
		V_IMPORT_MD2_ERROR,
		V_IMPORT_RWX_MDL_ERROR,
		V_IMPORT_NWN_MDL_ERROR,
		V_MODELLER_ERROR,
		V_MODEL_ERROR,

		V_OBJ_LOD_ERROR,
		V_OBJ_NOT_PREPARED_BOUNDS,

		V_SCREENCOORDS_OUT_OF_BOUNDS,

		V_TOO_MANY_VERTICES,

		V_POSITION_KEY_ERROR,
		V_ROTATION_KEY_ERROR,

		V_NOT_YET_SUPPORTED,
		V_OUT_OF_MEMORY,
		V_VECTOR_ERROR,
		V_ILLEGAL_DELETE,
		V_ILLEGAL_FOR_THIS_SUBCLASS,

		V_SPATIALINDEX_ERROR,

		V_TERRAIN_TOO_MANY_TEXTURE_SPLATS,

		V_IMAGE_ERROR,
		V_IMAGE_NOISEFUNC_ERROR,
		V_IMAGE_UNSUPPORTED_FILEFORMAT,

		V_TEXTURE_NON_POWER_2,

		V_INTERFACE_ERROR,
		V_RECEIVED_INVALID_TAG,
		V_RECEIVED_INCORRECT_TYPE,

		V_NOT_LOCKED,
		V_LOCKED,

		V_NO_MESSAGES,

		V_TRACETIMING_ERROR
	};

	const void *source;
	ExceptionType type;
	string message;
	VisionException(const void *source,ExceptionType type,const char *message);
};

enum V_CLASS_t {
	V_CLASS_UNKNOWN = -2,
	V_CLASS_VOID = 1,
	V_CLASS_ROOT = 0,

	V_CLASS_GRAPHICSENV = 1000,

	V_CLASS_GRAPHICS2D = 2000,

	V_CLASS_FONT = 3000,

	V_CLASS_WORLD = 4000,

	V_CLASS_SKY = 5000,

	V_CLASS_IMAGE = 6000,

	//V_CLASS_FACE = 7000,

	V_CLASS_OBJECTDATA = 8000,
	V_CLASS_MESH = 8100,
	V_CLASS_VOBJECT = 8200,
	//V_CLASS_MD2OBJECT = 8300,
	V_CLASS_LIGHT = 8300,
	V_CLASS_EXPLOSION = 8310,
	V_CLASS_PARTICLESYSTEM = 8400,
	V_CLASS_SNOWSTORM = 8410,
	V_CLASS_NWNPARTICLESYSTEM = 8420,
	V_CLASS_BILLBOARD = 8500,

	V_CLASS_SCENEGRAPHNODE = 9000,
	V_CLASS_ENTITY = 9100,
	V_CLASS_SPATIALGRID2D = 9200,

	V_CLASS_TERRAIN = 10000,

	V_CLASS_PANEL = 11000,
	V_CLASS_FLOWPANEL = 11100,
	V_CLASS_BUTTON = 11200,
	V_CLASS_IMAGEBUTTON = 11300,
	V_CLASS_CYCLE = 11400,
	V_CLASS_LISTBOX = 11500,
	V_CLASS_STRINGGADGET = 11600,
	V_CLASS_TEXTFIELD = 11610,

	V_CLASS_SHADOWPLANE = 12000,

	V_CLASS_ATMOSPHERE = 13000,

	V_CLASS_TEXTURE = 14000,

	V_CLASS_CLOUDS = 15000,

	V_CLASS_SOUND = 16000

};

enum V_STATUS_t {
	V_STATUS_UNKNOWN = -1,
	V_STATUS_OK = 0,
	V_STATUS_FAIL = 1,
	V_STATUS_BAD_ENUM = 2,
	V_STATUS_INVALID_TAG = 3,
	V_STATUS_INCORRECT_TYPE = 4
};

enum V_SOURCETYPE_t {
	V_SOURCETYPE_UNKNOWN = 0,
	V_SOURCETYPE_NWN = 1
};

enum V_ANIMSTATE_t {
	V_ANIMSTATE_MD2_IDLE = 0,
	V_ANIMSTATE_MD2_CROUCH = 1,
	V_ANIMSTATE_MD2_RUN = 2,
	V_ANIMSTATE_MD2_JUMP = 3,
	V_ANIMSTATE_MD2_FIRE = 4,
	V_ANIMSTATE_MD2_DIE = 5,
	V_ANIMSTATE_MD2_ALL = 6,
	V_ANIMSTATE_MD2_NSTATES = 7
};
#define CHECK_ANIMSTATE(v)              \
	if( v != V_ANIMSTATE_MD2_IDLE &&    \
	v != V_ANIMSTATE_MD2_CROUCH &&      \
	v != V_ANIMSTATE_MD2_RUN &&         \
	v != V_ANIMSTATE_MD2_JUMP &&        \
	v != V_ANIMSTATE_MD2_FIRE &&        \
	v != V_ANIMSTATE_MD2_DIE &&         \
	v != V_ANIMSTATE_MD2_ALL ) {        \
	ERROR_INTERFACE(V_STATUS_BAD_ENUM); \
	}
//return V_STATUS_BAD_ENUM;           \

enum V_COLORMODE_t {
	V_COLORMODE_NONE = 0,
	V_COLORMODE_GLOBAL
	//, V_COLORMODE_PER_TEXTURE
};
#define CHECK_COLORMODE(v)              \
	if( v != V_COLORMODE_NONE           \
	&& v != V_COLORMODE_GLOBAL          \
	) {                                 \
	ERROR_INTERFACE(V_STATUS_BAD_ENUM); \
	}
	//&& 	v != V_COLORMODE_PER_TEXTURE \

enum V_BLENDTYPE_t {
	//V_BLENDTYPE_NONE = 0,
	V_BLENDTYPE_FADE = 1,
	V_BLENDTYPE_FADE_SRC = 2,
	V_BLENDTYPE_FADE_DST = 3
};
#define CHECK_BLENDTYPE(v)              \
	if( v != V_BLENDTYPE_FADE &&        \
	v != V_BLENDTYPE_FADE_SRC &&        \
	v != V_BLENDTYPE_FADE_DST ) {       \
	ERROR_INTERFACE(V_STATUS_BAD_ENUM); \
	}
//return V_STATUS_BAD_ENUM;           \

enum V_SKYMODE_t {
	V_SKYMODE_FULL = 0,
	V_SKYMODE_BOTTOMLESS = 1,
	V_SKYMODE_HALF = 2
};
#define CHECK_SKYMODE(v)                \
	if( v != V_SKYMODE_FULL &&          \
	v != V_SKYMODE_BOTTOMLESS &&        \
	v != V_SKYMODE_HALF ) {             \
	ERROR_INTERFACE(V_STATUS_BAD_ENUM); \
	}

enum V_NOISEMODE_t {
	V_NOISEMODE_PERLIN = 0,
	V_NOISEMODE_SMOKE = 1,
	V_NOISEMODE_PATCHY = 2,
	V_NOISEMODE_MARBLE = 3,
	V_NOISEMODE_WOOD = 4,
	V_NOISEMODE_CLOUDS = 5
};
#define CHECK_NOISEMODE(v)              \
	if( v != V_NOISEMODE_PERLIN &&      \
	v != V_NOISEMODE_SMOKE &&           \
	v != V_NOISEMODE_PATCHY &&          \
	v != V_NOISEMODE_MARBLE &&          \
	v != V_NOISEMODE_WOOD ) {           \
	ERROR_INTERFACE(V_STATUS_BAD_ENUM); \
	}

class Graphics2D;

//typedef void (V_Action) (V_TAG_t source);
//typedef void (V_Update) (V_TAG_t entity,int time);

typedef void (VI_ThreadFunction) (void *ptr);

class LevelSquareInfo {
public:
	bool solid;
};

/*#define V_TOL_MACHINE 0.000000000000000001f // 1.0e-18
#define V_TOL_ANGULAR 0.000001f // 1.0e-6
#define V_TOL_LINEAR 0.0001f // 1.0e-4
#define V_TOL_LARGE 1000.0f // 1.0e3*/
const float V_TOL_MACHINE = 0.000000000000000001f; // 1.0e-18
const float V_TOL_ANGULAR = 0.000001f; // 1.0e-6
const float V_TOL_LINEAR = 0.0001f; // 1.0e-4
const float V_TOL_LARGE = 1000.0f; // 1.0e3

inline bool V_TOL_equal(float a, float b, float tol) {
	return fabs(a - b) <= tol;
}

// key codes
// When using SDL, we remap the codes to our own internal codes, so we can set them to what we like; must be 0-255 though!

#define V_ESCAPE 128
// function keys
#define V_F1 129
#define V_F2 130
#define V_F3 131
#define V_F4 132
#define V_F5 133
#define V_F6 134
#define V_F7 135
#define V_F8 136
#define V_F9 137
#define V_F10 138
#define V_F11 139
#define V_F12 140
// left side special keys
#define V_BACKQUOTE 141
#define V_TAB 142
#define V_CAPSLOCK 143
#define V_LSHIFT 144
#define V_BACKSLASH 145
#define V_LCONTROL 146
#define V_LOS 147 // left "OS" key - Windows key, etc
#define V_LALT 148
// right side special keys
#define V_SUBTRACT 149
#define V_EQUALS 150
#define V_BACKSPACE 151
#define V_LEFTBRACKET 152
#define V_RIGHTBRACKET 153
#define V_RETURN 154
#define V_SEMICOLON 155
#define V_SINGLEQUOTE 156
#define V_HASH 157
#define V_COMMA 158
#define V_PERIOD 159
#define V_SLASH 160
#define V_RSHIFT 161
#define V_RALT 162
#define V_ROS 163 // right "OS" key - Windows key, etc
#define V_MENU 164
#define V_RCONTROL 165
// other special keys
#define V_PRINT 166
#define V_SCROLLLOCK 167
#define V_PAUSE 168
#define V_INSERT 169
#define V_HOME 170
#define V_PGUP 171
#define V_DELETE 172
#define V_END 173
#define V_PGDOWN 174
// arrow keys
#define V_LEFT 175
#define V_RIGHT 176
#define V_UP 177
#define V_DOWN 178
// numeric keypad special keys
#define V_NUMLOCK 179
#define V_NUMPAD_DIVIDE 180
#define V_NUMPAD_MULTIPLY 181
#define V_NUMPAD_MINUS 182
#define V_NUMPAD_PLUS 183
// 184 reserved for V_NUMPAD_ENTER
#define V_NUMPAD_PERIOD 185
// numeric keypad
#define V_NUMPAD0 186
#define V_NUMPAD1 187
#define V_NUMPAD2 188
#define V_NUMPAD3 189
#define V_NUMPAD4 190
#define V_NUMPAD5 191
#define V_NUMPAD6 192
#define V_NUMPAD7 193
#define V_NUMPAD8 194
#define V_NUMPAD9 195
// alphanumeric keys
#define V_SPACE ' ' // 32
#define V_A 'A' // 65
#define V_B 'B'
#define V_C 'C'
#define V_D 'D'
#define V_E 'E'
#define V_F 'F'
#define V_G 'G'
#define V_H 'H'
#define V_I 'I'
#define V_J 'J'
#define V_K 'K'
#define V_L 'L'
#define V_M 'M'
#define V_N 'N'
#define V_O 'O'
#define V_P 'P'
#define V_Q 'Q'
#define V_R 'R'
#define V_S 'S'
#define V_T 'T'
#define V_U 'U'
#define V_V 'V'
#define V_W 'W'
#define V_X 'X'
#define V_Y 'Y'
#define V_Z 'Z' // 90
#define V_0 '0' // 48
#define V_1 '1'
#define V_2 '2'
#define V_3 '3'
#define V_4 '4'
#define V_5 '5'
#define V_6 '6'
#define V_7 '7'
#define V_8 '8'
#define V_9 '9' // 57
