#pragma once

#include <Cg/cg.h>

#include "../Renderer.h"

class CGShader : public Shader {
/*protected:
	CGparameter cgparam_ModelViewProj;
	CGparameter cgparam_ModelViewIT;
	CGparameter cgparam_ModelView;*/
public:
	CGShader(bool vertex_shader);
	virtual ~CGShader();

	CGprogram cgProgram;
	CGprofile cgProfile;

	//void init();
	// set predefined variables
	//virtual void setTexture(const Texture *texture) const;
	/*virtual void setMatSpecular(const float specular[4]) const;
	virtual void setLightPos(float p1, float p2, float p3, float d1, float d2, float d3) const;
	virtual void setLightAmbient(float rf, float gf, float bf) const;
	virtual void setLightDiffuse(float rf, float gf, float bf) const;
	virtual void setLightSpecular(float rf, float gf, float bf) const;
	virtual void setLightAtten(float a, float b, float c) const;*/
	//virtual void setBumpTexture(const Texture *texture) const;
	//virtual void setHardwareAnimationAlpha(float alpha) const;

	virtual void SetUniformParameter1f(const char *pszParameter, float p1) const;
	virtual void SetUniformParameter2f(const char *pszParameter, float p1, float p2) const;
	virtual void SetUniformParameter3f(const char *pszParameter, float p1, float p2, float p3) const;
	virtual void SetUniformParameter4f(const char *pszParameter, float p1, float p2, float p3, float p4) const;
	/*virtual void SetUniformParameter2fv(const char *pszParameter, const float v[2]) const;
	virtual void SetUniformParameter3fv(const char *pszParameter, const float v[3]) const;*/
	virtual void SetUniformParameter4fv(const char *pszParameter, const float v[4]) const;
};

class CGFXShaderEffect : public ShaderEffect {
protected:
	CGparameter cgparam_ModelViewProj;
	CGparameter cgparam_ModelViewIT;
	CGparameter cgparam_ModelView;

	CGeffect effect;
	CGtechnique technique;
public:
	CGFXShaderEffect(CGcontext cg_context, const char *file, const char **args);
	virtual ~CGFXShaderEffect();

	void init();

	virtual void enable(Renderer *renderer) const;
	virtual void SetUniformParameter1f(const char *parameter, float p1) const;
	virtual void SetUniformParameter2f(const char *parameter, float p1, float p2) const;
	virtual void SetUniformParameter3f(const char *parameter, float p1, float p2, float p3) const;
	virtual void SetUniformParameter4f(const char *parameter, float p1, float p2, float p3, float p4) const;
	virtual void SetUniformParameter4fv(const char *parameter, const float v[4]) const;
};
