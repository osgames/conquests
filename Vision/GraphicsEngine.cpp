//---------------------------------------------------------------------------
#include <ctime>
#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include <cmath>
#include <cassert>

#include <algorithm>
using std::swap;
using std::min;

#include "GraphicsEngine.h"
#include "Renderer.h"
#include "GraphicsEnvironment.h"
#include "Entity.h"
#include "World.h"
#include "Terrain.h"
#include "RenderData.h"

//---------------------------------------------------------------------------

//const float splat_detail_scale_c = 100.0f; // scale for terrain splatting
//const float splat_detail_scale_c = 8.0f; // scale for terrain splatting; for SECTION_SIZE 64
//const float splat_detail_scale_c = 4.0f; // scale for terrain splatting; for SECTION_SIZE 32 (original)
//const float splat_detail_scale_c = 2.0f; // scale for terrain splatting; for SECTION_SIZE 16

Graphics3DShaders::Graphics3DShaders() {
	clear();
}

void Graphics3DShaders::clear() {
	memset(this, 0, sizeof(Graphics3DShaders));
}

Graphics3D::Graphics3D(GraphicsEnvironment *genv, bool want_hdr) {
	this->genv=genv;
	this->world = NULL;
	this->failed_to_load_shaders = false;
	this->active_light = NULL;
	this->use_shadow_volumes = false;
	this->want_hdr = want_hdr;
	this->hdr_enabled = false;
	this->bloom_enabled = false;
	this->hdr_fbo = NULL;
	this->temp_fbo = NULL;
	this->frames = 0;
	this->frame_time = 0;
	//this->current_time = 0;
	this->fps = 0.0;
	this->worldToEyeMatrix = NULL; // created lazily
	this->std_shaders.clear();
	this->c_queue = NULL;
}

Graphics3D::~Graphics3D() {
	delete hdr_fbo;
	delete temp_fbo;
	delete worldToEyeMatrix;
}

Renderer *Graphics3D::getRenderer() {
	return this->genv->getRenderer();
}

const Renderer *Graphics3D::getRenderer() const {
	return this->genv->getRenderer();
}

void Graphics3D::setBlend(V_BLENDTYPE_t blend_type, bool lighting_pass) {
	if( blend_type == V_BLENDTYPE_FADE_SRC )
		getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
	else if( blend_type == V_BLENDTYPE_FADE_DST )
		getRenderer()->setBlendMode(lighting_pass ? Renderer::RENDERSTATE_BLEND_BOTH : Renderer::RENDERSTATE_BLEND_ONE_BY_ONEMINUSSRCALPHA);
	else if( blend_type == V_BLENDTYPE_FADE )
		getRenderer()->setBlendMode(lighting_pass ? Renderer::RENDERSTATE_BLEND_ADDITIVE : Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
	else {
		ASSERT(false);
	}
}

void Graphics3D::createHDRBuffers() {
	hdr_enabled = false;
	bloom_enabled = false;
	//if( RendererInfo::framebufferObject && this->std_shaders.hdr_vertex_shader != NULL && this->std_shaders.hdr_pixel_shader != NULL ) {
	if( RendererInfo::framebufferObject && this->std_shaders.hdr_shader != NULL ) {
		//if( false ) {
		LOG("creating HDR framebuffer...\n");

		/*int tex_width = genv->getAA()*width;
		int tex_height = genv->getAA()*height;*/
		/*int tex_width = width;
		int tex_height = height;*/
		int tex_width = genv->getWidth();
		int tex_height = genv->getHeight();

		//bool hdr_ok = genv->getRenderer()->generateHdrFbo(tex_width, tex_height);
		this->hdr_fbo = genv->getRenderer()->createFramebufferObject();
		bool hdr_ok = this->hdr_fbo->generate(tex_width, tex_height, true);

		if( !hdr_ok ) {
			LOG("Failed to create Framebuffer object!\n");
			//Vision::framebufferObject = false;
		}
		else {
			LOG("    done\n");
			hdr_enabled = true;

			/*if(
				this->std_shaders.blurw_pixel_shader != NULL && this->std_shaders.blurh_hdr_pixel_shader != NULL ) {

					this->temp_fbo = genv->getRenderer()->createFramebufferObject();
					bool temp_ok = this->temp_fbo->generate(tex_width, tex_height, false);
					if( !temp_ok ) {
						LOG("Failed to create temp Framebubber object!\n");
					}
					else {
						LOG("created temp Framebuffer okay\n");
						bloom_enabled = true;
					}
			}*/
		}
	}
	else {
		LOG("HDR not available\n");
	}
}

void Graphics3D::setZ(float z_near, float z_far, float fovy) {
	/*this->z_near = z_near;
	this->z_far = z_far;*/
	//ReSizeGLScene(width,height);
	ReSizeGLScene(z_near, z_far, genv->getWidth(), genv->getHeight(), genv->getRenderer()->getPersp(), fovy);
}

void Graphics3D::setPerspective(bool persp, float fovy) {
	//this->persp = persp;
	//this->ReSizeGLScene(this->width, this->height);
	ReSizeGLScene(genv->getRenderer()->getZNear(), genv->getRenderer()->getZFar(), genv->getWidth(), genv->getHeight(), persp, fovy);
}

void Graphics3D::setViewport() {
	/*if( this->hdr_enabled ) {
	// viewpoint size must take into account multisampling
	glViewport(0, 0, genv->getAA()*width, genv->getAA()*height);
	}
	else*/ {
		//glViewport(0, 0, width, height);
		//genv->getRenderer()->setViewport(width, height);
		genv->getRenderer()->setViewport(genv->getWidth(), genv->getHeight());
}
}

/* Called when the scene is resized.
*/
void Graphics3D::ReSizeGLScene(float z_near, float z_far, int width, int height, bool persp, float fovy) {
	LOG("Graphics3D::ReSizeGLScene(%f,%f,%d,%d,%d,%f)\n", z_near, z_far, width, height, persp, fovy);
	if( GraphicsEnvironment::existsOrHasContext() ) {
		// check needed, as this function can be called during initialisation of GraphicsEnvironment
		this->genv->checkError("GraphicsEnvironment::ReSizeGLScene BEGIN");
	}
	/*this->width = width;
	this->height = height;*/

	if( this->want_hdr ) {
		//genv->getRenderer()->freeHDRBuffers();
		createHDRBuffers();
	}

	// [note - even if using hdr, we don't want to take into account multisampling at this stage,
	// so we don't use setViewport(). - OUTDATED now that we don't do supersampling]

	//genv->getRenderer()->resizeScene(frustum.frustum, width, height, persp, z_near, z_far, fovy);
	//genv->getRenderer()->resizeScene(frustum.frustum, genv->getWidth(), genv->getHeight(), persp, z_near, z_far, fovy);
	genv->getRenderer()->resizeScene(genv->getFrustum()->frustum, genv->getWidth(), genv->getHeight(), persp, z_near, z_far, fovy);

	if( GraphicsEnvironment::existsOrHasContext() ) {
		this->genv->checkError("GraphicsEnvironment::ReSizeGLScene END");
	}
}


/* Must be called to set the 'world' that this Graphics3D should render.
*/
void Graphics3D::setWorld(World *world) {
	this->world = world;
	genv->getRenderer()->switchToModelview();
	genv->getRenderer()->loadIdentity();
}

Renderable::Renderable(SceneGraphNode *node) : node(node), renderData(NULL), sort_preference(0) {
}

RenderQueue::RenderQueue() {
	this->renderQueue = new RQRenderableCollection();
	this->renderQueue_lights = new RQEntityCollection();
	this->renderQueue_infinite = new RQEntityCollection();
	this->renderQueue_shadowcasters = new RQEntityCollection();
	this->renderQueue_nonopaque = new RQRenderableCollection();
	this->renderQueue_shadowplanes = new RQShadowplanesCollection();
}

RenderQueue::~RenderQueue() {
	delete this->renderQueue;
	delete this->renderQueue_lights;
	delete this->renderQueue_infinite;
	delete this->renderQueue_shadowcasters;
	delete this->renderQueue_nonopaque;
	delete this->renderQueue_shadowplanes;
	//delete [] this->lights;
}

void RenderQueue::flushRenderQueue() const {
	this->renderQueue->clear();
	this->renderQueue_lights->clear();
	this->renderQueue_infinite->clear();
	this->renderQueue_shadowcasters->clear();
	this->renderQueue_nonopaque->clear();
	this->renderQueue_shadowplanes->clear();
}

void RenderQueue::addOpaque(SceneGraphNode *node) const {
	Renderable renderable(node);
	addOpaque(&renderable);
}

void RenderQueue::addOpaque(Renderable *renderable) const {
	this->renderQueue->push_back(*renderable);
}

void RenderQueue::addNonOpaque(SceneGraphNode *node) const {
	Renderable renderable(node);
	addNonOpaque(&renderable);
}

void RenderQueue::addNonOpaque(Renderable *renderable) const {
	this->renderQueue_nonopaque->push_back(*renderable);
}

size_t RenderQueue::nOpaque() const {
	return this->renderQueue->size();
}

Renderable *RenderQueue::getOpaque(size_t i) const {
	return &this->renderQueue->at(i);
}

void Graphics3D::buildRenderQueue(SceneGraphNode *aobj) {
	genv->getRenderer()->pushMatrix();
	aobj->multGLMatrix();

	if( aobj->renderRequired(genv) ) {
		//VI_log("render: %s\n", aobj->getName());
		aobj->renderQueue(this->c_queue, false);
	}
	else {
		//VI_log("don't render: %s\n", aobj->getName());
		// TODO: have a smaller radius, where we get lighting only if outside of this smaller radius
		// for now we use this to get the shadow casters
		aobj->renderQueue(this->c_queue, true);
		//aobj->renderQueue(this, false);
	}
	//children
	for(size_t i=0;i<aobj->getNChildNodes();i++) {
		SceneGraphNode *child = dynamic_cast<SceneGraphNode *>(aobj->getChildNode(i));
		buildRenderQueue(child);
	}
	genv->getRenderer()->popMatrix();
}

void Graphics3D::buildRenderQueue(Sector *sector) {
	V__ENTER;
	if( sector->spatialindex != NULL ) {
		sector->spatialindex->buildRenderQueue(this->genv);
	}
	else {
		buildRenderQueue(sector->rootObject);
	}
	if( world->getTerrain() != NULL ) {
		TerrainEngine *d_terrain = static_cast<TerrainEngine *>(world->getTerrain()); // cast to the internal representation
		d_terrain->renderQueue(this->c_queue, genv);
	}
	V__EXIT;
}

/* Render 'obj', and all its children.
*/
/*void Graphics3D::doObject(SceneGraphNode *aobj,Graphics3D::RenderPhase phase,bool lighting_pass) {
glPushMatrix();
aobj->multGLMatrix();

if( aobj->renderRequired(this) // needs to be first, as this sets the mvmatrix!
//|| phase == RENDER_LIGHTING
) {
aobj->render(this,phase,lighting_pass,NULL);
}
//children
for(int i=0;i<aobj->getNChildren();i++) {
doObject(aobj->getChildAt(i),phase,lighting_pass);
}
glPopMatrix();
}*/

#if 0
void Graphics3D::disableLights() {
	genv->getRenderer()->disableLights();
}

void Graphics3D::disableLight(int index) {
	// n.b., argument is 0, not index, as this is the counterpart of Graphics3D::enableLight() (we always use the Renderer's light index 0, when setting lights one by one).
	genv->getRenderer()->disableLight(0);
}

void Graphics3D::enableOneLight(int n_light,Entity *light_ent) {
	//if( getRenderer()->getActiveVertexShader() != NULL || getRenderer()->getActivePixelShader() ) {
	if( getRenderer()->getActiveShader() != NULL ) {
		// the engine should be coded so that we only change lights when shaders are not set - so we know that we
		// can set the shader variables for the lights when enabling the shader (as opposed to worrying about
		// updating the variables when we change the light)
		LOG("shouldn't be setting light when shaders enabled!\n");
		ASSERT(false);
	}
	Light *light = dynamic_cast<Light *>(light_ent->getObject());

	// position
	//Vector3D pos = light_ent->getCurrentWorldPos();
	Vector3D pos = light_ent->getLastRenderedEyePos();
	bool infinite = light_ent->getPoint()->isInfinite();
	float p[3];
	p[0] = pos.x;
	p[1] = pos.y;
	p[2] = pos.z;

	//genv->getRenderer()->enableLight(n_light, light, p, infinite);
	float lightAmbient[4], lightDiffuse[4], lightSpecular[4];
	light->getLightAmbient().floatArray(lightAmbient);
	light->getLightDiffuse().floatArray(lightDiffuse);
	light->getLightSpecular().floatArray(lightSpecular);
	const float *lightAttenuation = light->getLightAttenuation();
	genv->getRenderer()->enableLight(n_light, lightAmbient, lightDiffuse, lightSpecular, lightAttenuation, p, infinite);
}

void Graphics3D::enableLight(int index) {
	/*genv->getRenderer()->pushMatrix();
	genv->getRenderer()->loadIdentity();*/
	Entity *light_ent = this->c_queue->renderQueue_lights->at(index);
	enableOneLight(0, light_ent);
	//genv->getRenderer()->popMatrix();
}

void Graphics3D::enableLights() {
	/*genv->getRenderer()->pushMatrix();
	genv->getRenderer()->loadIdentity();*/
	size_t n_lights = this->c_queue->renderQueue_lights->size();
	n_lights = min(n_lights, (size_t)RendererInfo::maxLights);
	for(size_t l_index=0;l_index<n_lights;l_index++) {
		Entity *light_ent = this->c_queue->renderQueue_lights->at(l_index);
		enableOneLight(l_index, light_ent);
	}
	//genv->getRenderer()->popMatrix();
}
#endif

void Graphics3D::drawSky(RenderQueue *queue,SceneGraphNode *portal) {
	V__ENTER;
	//if( world->getAtmosphere() != NULL || world->getSky() != NULL || world->getClouds() != NULL || world->isHorizonEnabled() ) {
	if( world->getAtmosphere() != NULL || world->getSky() != NULL || world->getClouds() != NULL ) {
		Clouds *d_clouds = static_cast<Clouds *>(world->getClouds()); // cast to the internal representation
		SkyBox *d_sky = static_cast<SkyBox *>(world->getSky()); // cast to the internal representation
		Atmosphere *d_atmos = static_cast<Atmosphere *>(world->getAtmosphere()); // cast to the internal representation
		genv->getRenderer()->pushMatrix();

		// so that the viewpoint is at the centre of the viewpoint
		genv->getRenderer()->loadIdentity();

		Rotation3D rot = world->getViewpoint()->getRotation();
		//rot.multGLMatrixTranspose();
		this->getRenderer()->transformInverse(rot.getQuaternion());
		// recurse up the hierarchy, if necessary
		VI_SceneGraphNode *parent = world->getViewpoint()->getParentNode();
		while(parent != NULL) {
			rot = parent->getRotation();
			//rot.multGLMatrixTranspose();
			this->getRenderer()->transformInverse(rot.getQuaternion());
			parent = parent->getParentNode();
		}
		if( portal != NULL ) {
			genv->getRenderer()->scale(1.0, -1.0, 1.0); // TODO: hardcoded
		}

		/* Drawing atmosphere before skybox causes bug on NVIDIA when not using shaders!
		* Alpha from atmosphere pixel shader always appears to be set to 1, meaning skybox isn't drawn with alpha blending
		*/
//#if 0
		bool blend = false;
		if( world->getSky() != NULL ) {
			d_sky->render(this->getRenderer());
			blend = true;
		}
		if( world->getAtmosphere() != NULL ) {
			if( genv->hasShaders() ) {
				// need shaders
				if( blend ) {
					genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_BOTH);
				}
				d_atmos->render(this, portal);
				if( blend ) {
					genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
				}
			}
		}
		if( world->getClouds() != NULL ) {
			if( genv->hasShaders() && this->hasCloudShaders() ) {
				// need shaders
				blend = true;
				// the clouds are always blended (if no skybox or atmosphere, we blend with the background color)
				//genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_BOTH);
				//genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
				genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				d_clouds->render(this);
				genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			}
		}
//#endif
#if 0
		bool blend = false;
		if( world->getSky() != NULL ) {
			d_sky->render(this->getRenderer());
			blend = true;
		}
		if( world->getClouds() != NULL ) {
			if( genv->hasShaders() && this->hasCloudShaders() ) {
				// need shaders

				blend = true;
				// the clouds need to be blended with the:
				// * skybox if there is one
				// * or the background colour, if there's no atmosphere
				if( world->getSky() != NULL || world->getAtmosphere() == NULL ) {
					//genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_BOTH);
					genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				}
				d_clouds->render(this);
				if( d_sky != NULL || d_atmos == NULL ) {
					genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
				}
			}
		}

		if( world->getAtmosphere() != NULL ) {
			if( genv->hasShaders() ) {
				// need shaders
				if( blend ) {
					genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_BOTH);
					//genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_ONE_BY_SRCALPHA);
					//genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_REVERSE_TRANSPARENCY);
				}
				d_atmos->render(this, portal);
				if( blend ) {
					genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
				}
			}
		}
#endif

		genv->getRenderer()->popMatrix();
	}

	// infinite distance objects
	genv->getRenderer()->pushMatrix();
	for(RQEntityIter iter = queue->renderQueue_infinite->begin();iter != queue->renderQueue_infinite->end();++iter) {
		SceneGraphNode *node = *iter;
		node->getMatrix()->set(15, 1);
		node->getMatrix()->load();
		node->render(this->genv, ObjectData::RENDER_OPAQUE, false, false, NULL);
	}
	genv->getRenderer()->popMatrix();
	V__EXIT;
}

void Graphics3D::setShaderFrameAlpha(const float frame_alpha) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		shader->SetUniformParameter1f("frame_alpha", frame_alpha);
	}
}

// n.b., passed as floats in the range [0, 1] rather than [0, 255] (so we can do easy multiplication in the shader; also makes it compatible with fixed function)
void Graphics3D::setShaderMatSpecular(const float material_specular[4]) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		shader->SetUniformParameter4fv("material_specular", material_specular);
	}
}

// n.b., passed as floats in the range [0, 1] rather than [0, 255] (so we can do easy multiplication in the shader; also makes it compatible with fixed function)
void Graphics3D::setShaderMatColor(const float color[4]) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		shader->SetUniformParameter4fv("material_color", color);
	}
}

void Graphics3D::setShaderLightPos(float p1, float p2, float p3, float d1, float d2, float d3) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		shader->SetUniformParameter4f("lt.LightPos", p1, p2, p3, 0.0f);
		shader->SetUniformParameter4f("lightpos", p1, p2, p3, 0.0f);
		shader->SetUniformParameter4f("lt.LightVec", d1, d2, d3, 0.0f);
	}
}

void Graphics3D::setShaderLightAmbient(float rf, float gf, float bf) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		shader->SetUniformParameter4f("lt.AmbLight", rf, gf, bf, 1.0f);
	}
}

void Graphics3D::setShaderLightDiffuse(float rf, float gf, float bf) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		shader->SetUniformParameter4f("lt.DiffLight", rf, gf, bf, 1.0f);
	}
}

void Graphics3D::setShaderLightSpecular(float rf, float gf, float bf) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		shader->SetUniformParameter4f("lt.SpecLight", rf, gf, bf, 1.0f);
	}
}

void Graphics3D::setShaderLightAtten(float a, float b, float c) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		shader->SetUniformParameter3f("lt.AttenLight", a, b, c);
	}
}

/*void Graphics3D::setFixedFunctionLighting(Entity *light_ent, ObjectData *obj) {
	if( getRenderer()->getActiveShader() != NULL ) {
		// this function only affected fixed function pipeline
		return;
	}
	if( !obj->renderWithLighting() ) {
		getRenderer()->enableFixedFunctionLighting(false);
	}
	else if( light_ent == NULL ) {
		getRenderer()->enableFixedFunctionLighting(true);
		float col[4] = {1.0f, 1.0f, 1.0f, 1.0f};
		world->getAmbience(&col[0], &col[1], &col[2]);
		getRenderer()->setFixedFunctionAmbientLighting(col);
		getRenderer()->disableFixedFunctionLight(0);
	}
	else {
		// for OpenGL at least, we need to switch to the identity matrix, as the position of the light is transformed by the current matrix!
		genv->getRenderer()->pushMatrix();
		genv->getRenderer()->loadIdentity();

		getRenderer()->enableFixedFunctionLighting(true);
		Light *light = dynamic_cast<Light *>(light_ent->getObject());
		float col[4] = {0.0f, 0.0f, 0.0f, 0.0f};
		getRenderer()->setFixedFunctionAmbientLighting(col);

		Vector3D pos = light_ent->getLastRenderedEyePos();
		bool infinite = light_ent->getPoint()->isInfinite();
		float p[3] = {pos.x, pos.y, pos.z};

		float lightAmbient[4], lightDiffuse[4], lightSpecular[4];
		light->getLightAmbient().floatArray(lightAmbient);
		light->getLightDiffuse().floatArray(lightDiffuse);
		light->getLightSpecular().floatArray(lightSpecular);
		const float *lightAttenuation = light->getLightAttenuation();
		getRenderer()->enableFixedFunctionLight(0, lightAmbient, lightDiffuse, lightSpecular, lightAttenuation, p, infinite);

		genv->getRenderer()->popMatrix();
	}
}*/

void Graphics3D::setShaderConstants(const SceneGraphNode *node) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		this->setShaderFrameAlpha(node->getVertexAnimFrameFraction());
		shader->setMatrices("mx.ModelViewProj", "mx.ModelViewIT", "mx.ModelView");

		ObjectData *d_obj = dynamic_cast<ObjectData *>(node->getObject()); // cast to the internal representation
		if( !d_obj->renderWithLighting() ) {
			// such objects should only ever be rendered with an ambient shader
			// so we simulate no lighting effects by setting the ambient to white
			this->setShaderLightAmbient(1.0f, 1.0f, 1.0f);
		}
		else if( this->active_light == NULL ) {
			float col[3] = {1.0f, 1.0f, 1.0f};
			world->getAmbience(&col[0], &col[1], &col[2]);
			this->setShaderLightAmbient(col[0], col[1], col[2]);
		}
		else {
			Light *light = dynamic_cast<Light *>(this->active_light->getObject());
			Color ambient = light->getLightAmbient();
			Color diffuse = light->getLightDiffuse();
			Color specular = light->getLightSpecular();
			Vector3D pos = this->active_light->getLastRenderedEyePos();

			this->setShaderLightAmbient(ambient.getRf(), ambient.getGf(), ambient.getBf());
			this->setShaderLightDiffuse(diffuse.getRf(), diffuse.getGf(), diffuse.getBf());
			this->setShaderLightSpecular(specular.getRf(), specular.getGf(), specular.getBf());
			this->setShaderLightAtten(light->getLightAttenuation(0), light->getLightAttenuation(1), light->getLightAttenuation(2));
			this->setShaderLightPos(pos.x, pos.y, pos.z, -pos.x, -pos.y, -pos.z);
		}

		const Material *material = node->getMaterial();
		float material_color[4] = {1.0f, 1.0f, 1.0f, 1.0f};
		if( material->isColorForced() ) {
			material_color[0] = material->getColor()->getRf();
			material_color[1] = material->getColor()->getGf();
			material_color[2] = material->getColor()->getBf();
			material_color[3] = ((float)material->getAlpha())/(float)255.0f;
		}
		this->setShaderMatColor(material_color);

		shader->SetUniformParameter1f("time", ((float)Vision::getGameTimeMS())/1000.0f);
		shader->SetUniformParameter1f("isYTextureInverted", this->getRenderer()->isYTextureInverted() ? 1.0f : 0.0f);

		// set to sensible defaults (as these aren't always set by the lower level rendering functions...)
		float material_specular[4] = {0.0f, 0.0f, 0.0f, 0.0f};
		this->setShaderMatSpecular(material_specular);
	}
}

void Graphics3D::setFixedFunctionConstants(const SceneGraphNode *node) {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader == NULL ) {
		// n.b., material colour only supported in shader (the fixed function version would override the vertex colours, where as we want to multiply)
		ObjectData *d_obj = dynamic_cast<ObjectData *>(node->getObject()); // cast to the internal representation
		if( !d_obj->renderWithLighting() ) {
			getRenderer()->enableFixedFunctionLighting(false);
		}
		else if( this->active_light == NULL ) {
			getRenderer()->enableFixedFunctionLighting(true);
			float col[4] = {1.0f, 1.0f, 1.0f, 1.0f};
			world->getAmbience(&col[0], &col[1], &col[2]);
			getRenderer()->setFixedFunctionAmbientLighting(col);
			getRenderer()->disableFixedFunctionLight(0);
		}
		else {
			// for OpenGL at least, we need to switch to the identity matrix, as the position of the light is transformed by the current matrix!
			genv->getRenderer()->pushMatrix();
			genv->getRenderer()->loadIdentity();

			getRenderer()->enableFixedFunctionLighting(true);
			Light *light = dynamic_cast<Light *>(this->active_light->getObject());
			float col[4] = {0.0f, 0.0f, 0.0f, 0.0f};
			getRenderer()->setFixedFunctionAmbientLighting(col);

			Vector3D pos = this->active_light->getLastRenderedEyePos();
			bool infinite = this->active_light->getPoint()->isInfinite();
			float p[3] = {pos.x, pos.y, pos.z};

			float lightAmbient[4], lightDiffuse[4], lightSpecular[4];
			light->getLightAmbient().floatArray(lightAmbient);
			light->getLightDiffuse().floatArray(lightDiffuse);
			light->getLightSpecular().floatArray(lightSpecular);
			const float *lightAttenuation = light->getLightAttenuation();
			getRenderer()->enableFixedFunctionLight(0, lightAmbient, lightDiffuse, lightSpecular, lightAttenuation, p, infinite);

			genv->getRenderer()->popMatrix();
		}
	}
}

void Graphics3D::setShaderConstants(const RenderableInfo *renderable_info) const {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader != NULL ) {
		this->setShaderMatSpecular(renderable_info->getMaterialSpecular());
		//shader->SetUniformParameter1f("textureID", (renderData->getNTextures() > 0 && renderData->getTexture(0) != NULL) ? 1.0f : 0.0f);
		shader->SetUniformParameter1f("textureID", renderable_info->getFirstTexture() != NULL ? 1.0f : 0.0f);
	}
}

void Graphics3D::setFixedFunctionConstants(const RenderableInfo *renderable_info) {
	const ShaderEffect *shader = this->getRenderer()->getActiveShader();
	if( shader == NULL ) {
		getRenderer()->setFixedFunctionMatSpecular(renderable_info->getMaterialSpecular());
	}
}

void Graphics3D::setShaders(SceneGraphNode *light_ent, ObjectData *obj) {
	// enables the shaders for this object, and also sets the active light
	V__ENTER;
	const ShaderEffect *std_shader = light_ent == NULL ? std_shaders.shader_ambient : light_ent->getPoint()->isInfinite() ? std_shaders.shader_directional : std_shaders.shader_point;
	const ShaderEffect *this_shader = (obj != NULL) ? ( light_ent == NULL ? obj->getShaderAmbient() : light_ent->getPoint()->isInfinite() ? obj->getShaderDirectional() : obj->getShaderPoint() ) : NULL;

	if( this_shader == NULL ) {
		this_shader = std_shader;
	}

	const ShaderEffect *c_shader = getRenderer()->getActiveShader();
	// should enable, to be sure we set uniform constants (since the functions check them for the "active" shader
	if( this_shader != c_shader ) {
		if( this_shader != NULL ) {
			this_shader->enable(this->getRenderer());
		}
		else {
			this->getRenderer()->disableShaders();
		}
	}

	this->active_light = light_ent;
	/*if( this_shader != NULL && this_shader != c_shader ) {
		// okay to only set when setting a new shader
		this_shader->SetUniformParameter1f("time", ((float)Vision::getGameTimeMS())/1000.0f);
		this_shader->SetUniformParameter1f("isYTextureInverted", this->getRenderer()->isYTextureInverted() ? 1.0f : 0.0f);
	}

	if( this_shader != NULL ) {
		// should always set lighting, even if using the same shader as before, as the light may have changed!
		if( !obj->renderWithLighting() ) {
			// such objects should only ever be rendered with an ambient shader
			// so we simulate no lighting effects by setting the ambient to white
			this->setShaderLightAmbient(1.0f, 1.0f, 1.0f);
		}
		else if( light_ent == NULL ) {
			float col[3] = {1.0f, 1.0f, 1.0f};
			world->getAmbience(&col[0], &col[1], &col[2]);
			this->setShaderLightAmbient(col[0], col[1], col[2]);
		}
		else {
			Light *light = dynamic_cast<Light *>(light_ent->getObject());
			Color ambient = light->getLightAmbient();
			Color diffuse = light->getLightDiffuse();
			Color specular = light->getLightSpecular();
			Vector3D pos = light_ent->getLastRenderedEyePos();

			this->setShaderLightAmbient(ambient.getRf(), ambient.getGf(), ambient.getBf());
			this->setShaderLightDiffuse(diffuse.getRf(), diffuse.getGf(), diffuse.getBf());
			this->setShaderLightSpecular(specular.getRf(), specular.getGf(), specular.getBf());
			this->setShaderLightAtten(light->getLightAttenuation(0), light->getLightAttenuation(1), light->getLightAttenuation(2));
			this->setShaderLightPos(pos.x, pos.y, pos.z, -pos.x, -pos.y, -pos.z);
		}

		// material colors must be set to sensible defaults (as these aren't always set by the lower level rendering functions...)
		float material_color[4] = {1.0f, 1.0f, 1.0f, 1.0f};
		float material_specular[4] = {0.0f, 0.0f, 0.0f, 0.0f};
		this->setShaderMatColor(material_color);
		this->setShaderMatSpecular(material_specular);
	}
	else {
		setFixedFunctionLighting(light_ent, obj);
	}*/

	// n.b., we still have to set the lighting info etc above, as it may have changed, even if using the same shader

	V__EXIT;
}

/* Render shadow of 'obj', and all its children.
*/
void Graphics3D::doObjectShadow(SceneGraphNode *aobj,ShadowPlane *shadowplane,int light_index) {
	V__ENTER;
	SceneGraphNode *light = this->c_queue->renderQueue_lights->at(light_index);

	aobj->multGLMatrix();

	/*if( aobj->getClass() == V_CLASS_ENTITY // yuck
		&& light->collision(aobj) ) {
			Entity *entity = static_cast<Entity *>(aobj);
			ObjectData *d_obj = dynamic_cast<ObjectData *>(entity->getObject()); // cast to the internal representation

			if( d_obj != NULL && d_obj->getShadowPlane() != shadowplane && d_obj->isShadowCaster() ) {
				aobj->render(this->genv, ObjectData::RENDER_STENCIL, false, true, NULL);
			}
	}*/
	if( light->collision(aobj) ) {
		ObjectData *d_obj = dynamic_cast<ObjectData *>(aobj->getObject()); // cast to the internal representation

		if( d_obj != NULL && d_obj->getShadowPlane() != shadowplane && d_obj->isShadowCaster() ) {
			aobj->render(this->genv, ObjectData::RENDER_STENCIL, false, true, NULL);
		}
	}

	//children
	for(size_t i=0;i<aobj->getNChildNodes();i++) {
		genv->getRenderer()->pushMatrix();
		SceneGraphNode *child = dynamic_cast<SceneGraphNode *>(aobj->getChildNode(i));
		doObjectShadow(child,shadowplane,light_index);
		genv->getRenderer()->popMatrix();
	}
	V__EXIT;
}

void Graphics3D::doProjectiveShadows(Sector *sector) {
	V__ENTER;
	SceneGraphNode *root = sector->rootObject;
	for(RQShadowplanesIter iter = c_queue->renderQueue_shadowplanes->begin();iter != c_queue->renderQueue_shadowplanes->end();++iter) {
		SceneGraphNode *mirror = *iter;
		ObjectData *d_obj = dynamic_cast<ObjectData *>(mirror->getObject()); // cast to the internal representation
		ShadowPlane *shadow = d_obj == NULL ? NULL : d_obj->getShadowPlane();
		// shadow shouldn't be NULL!
		if( !shadow->isShadow() ) {
			continue; // no shadows - draw later on
		}
		for(size_t j=0;j<this->c_queue->renderQueue_lights->size();j++) {
			// do opaque objects' shadows
			genv->getRenderer()->enableFixedFunctionLighting(false);
			genv->getRenderer()->enableTexturing(false);
			genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READONLY);

			genv->getRenderer()->enableColorMask(false);
			// mark out regions where shadows are
			genv->getRenderer()->clear(false, false, true);
			genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_REPLACE);
			SceneGraphNode *light = this->c_queue->renderQueue_lights->at(j);
			float lightPos[4];
			Vector3D lightWorldPos = light->getCurrentWorldPos();
			lightPos[0] = lightWorldPos.x;
			lightPos[1] = lightWorldPos.y;
			lightPos[2] = lightWorldPos.z;
			lightPos[3] = 1.0;
			const Plane *plane = shadow->getPlane();
			//TransformationMatrix *shadowMat = this->genv->getRenderer()->createTransformationMatrix();
			TransformationMatrix *shadowMat = this->genv->getRenderer()->getStaticTransformationMatrix();

			float dot = plane->normal.x * lightPos[0] + plane->normal.y * lightPos[1] + plane->normal.z * lightPos[2] - plane->D * lightPos[3];

			(*shadowMat)[0] = dot - lightPos[0] * plane->normal.x;
			(*shadowMat)[4] = - lightPos[0] * plane->normal.y;
			(*shadowMat)[8] = - lightPos[0] * plane->normal.z;
			(*shadowMat)[12] = lightPos[0] * plane->D;

			(*shadowMat)[1] = - lightPos[1] * plane->normal.x;
			(*shadowMat)[5] = dot - lightPos[1] * plane->normal.y;
			(*shadowMat)[9] = - lightPos[1] * plane->normal.z;
			(*shadowMat)[13] = lightPos[1] * plane->D;

			(*shadowMat)[2] = - lightPos[2] * plane->normal.x;
			(*shadowMat)[6] = - lightPos[2] * plane->normal.y;
			(*shadowMat)[10] = dot - lightPos[2] * plane->normal.z;
			(*shadowMat)[14] = lightPos[2] * plane->D;

			(*shadowMat)[3] = - lightPos[3] * plane->normal.x;
			(*shadowMat)[7] = - lightPos[3] * plane->normal.y;
			(*shadowMat)[11] = - lightPos[3] * plane->normal.z;
			(*shadowMat)[15] = dot + lightPos[3] * plane->D;

			genv->getRenderer()->pushMatrix();
			shadowMat->mult();
			doObjectShadow(root,shadow,j);
			genv->getRenderer()->popMatrix();

			genv->getRenderer()->enableFixedFunctionLighting(true);
			genv->getRenderer()->enableTexturing(true);
			genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);

			// enable new light
			/*enableLight(j);
			float col[4] = {0.0, 0.0, 0.0, 0.0};
			genv->getRenderer()->setAmbient(col);*/

			genv->getRenderer()->enableColorMask(true);
			// only draw where stencil isn't; disallow overdraw
			genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_DRAWATNOSTENCIL);

			genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);

			// now draw in the mirror with lighting
			genv->getRenderer()->setDepthBufferFunc(Renderer::RENDERSTATE_DEPTHFUNC_EQUAL);
			genv->getRenderer()->pushMatrix();
			mirror->getMatrix()->load();
			ObjectData *d_obj = dynamic_cast<ObjectData *>(mirror->getObject()); // cast to the internal representation
			setShaders(light, d_obj);
			mirror->render(this->genv, ObjectData::RENDER_STENCIL, false, false, NULL);
			genv->getRenderer()->popMatrix();

			genv->getRenderer()->setDepthBufferFunc(Renderer::RENDERSTATE_DEPTHFUNC_LESS);
			genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);

			/*float col[4] = {0.0, 0.0, 0.0, 0.0};
			//world->lightAmbient.floatArray(col);
			world->getAmbience(&col[0], &col[1], &col[2]);
			col[3] = 1.0f;
			genv->getRenderer()->setAmbient(col);
			disableLight(j);*/
			//this->setFixedFunctionLighting(NULL, d_obj);
			//delete shadowMat;

			// disable shaders
			/*const Shader *c_vertex_shader = getRenderer()->getActiveVertexShader();
			const Shader *c_pixel_shader = getRenderer()->getActivePixelShader();
			if( c_vertex_shader != NULL )
				c_vertex_shader->disable();
			if( c_pixel_shader != NULL )
				c_pixel_shader->disable();*/
			this->getRenderer()->disableShaders();
		}
		this->active_light = NULL;
		genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_NONE);
	}
	V__EXIT;
}

void Graphics3D::drawMirrors() {
	V__ENTER;
	// draw mirrors for stencil method (done separately as we need to enable blending)
	for(RQShadowplanesIter iter = c_queue->renderQueue_shadowplanes->begin();iter != c_queue->renderQueue_shadowplanes->end();++iter) {
		SceneGraphNode *mirror = *iter;
		ObjectData *d_obj = dynamic_cast<ObjectData *>(mirror->getObject()); // cast to the internal representation
		ShadowPlane *shadow = d_obj == NULL ? NULL : d_obj->getShadowPlane();
		// shadow shouldn't be NULL!
		//shadow->shadow = false;
		if( shadow->isReflection() && !shadow->isRenderToTexture() ) {
			// need to blend in with reflection
			genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
		}
		//if( use_shadow_volumes || !shadow->shadow )
		{
			/*// if using projected shadows, we won't be using shaders
			// NB - bug is that projected shadows won't work with render_to_texture mode!*/
			ObjectData *d_obj = dynamic_cast<ObjectData *>(mirror->getObject()); // cast to the internal representation
			setShaders(NULL, d_obj);
		}
		genv->getRenderer()->pushMatrix();
		mirror->getMatrix()->load();
		mirror->render(this->genv, ObjectData::RENDER_STENCIL, false, false, NULL);
		genv->getRenderer()->popMatrix();
		if( shadow->isReflection() && !shadow->isRenderToTexture() ) {
			genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
		}
	}
	/*const Shader *c_vertex_shader = getRenderer()->getActiveVertexShader();
	const Shader *c_pixel_shader = getRenderer()->getActivePixelShader();
	if( c_vertex_shader != NULL )
		c_vertex_shader->disable();
	if( c_pixel_shader != NULL )
		c_pixel_shader->disable();*/
	this->getRenderer()->disableShaders();
	V__EXIT;
}

void Graphics3D::drawFog() {
	const Fog *fog = this->world->getFog();
	if( fog->isEnabled() ) {
		if( fog->fogtype == Fog::FOG_LINEAR ) {
			GraphicsEnvironment::getSingleton()->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_LINEAR, fog->start, fog->end, fog->density);
		}
		else if( fog->fogtype == Fog::FOG_EXP ) {
			GraphicsEnvironment::getSingleton()->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_EXP, fog->start, fog->end, fog->density);
		}
		else if( fog->fogtype == Fog::FOG_EXP2 ) {
			GraphicsEnvironment::getSingleton()->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_EXP2, fog->start, fog->end, fog->density);
		}
		float fog_rgba[4] = {0.0f, 0.0f, 0.0f, 0.0f};
		fog->floatArray(fog_rgba);
		GraphicsEnvironment::getSingleton()->getRenderer()->setFogColor(fog_rgba);
	}
	else {
		GraphicsEnvironment::getSingleton()->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_NONE, 0.0f, 0.0f, 0.0f);
	}
}

int sortRenderQueue(const void *ptr1, const void *ptr2) {
	const Renderable *p1 = (const Renderable *)ptr1;
	const Renderable *p2 = (const Renderable *)ptr2;
	const SceneGraphNode *e1 = p1->node;
	const SceneGraphNode *e2 = p2->node;

	// reduce shader swaps
	const ObjectData *obj1 = dynamic_cast<const ObjectData *>(e1->getObject()); // cast to the internal representation
	const ObjectData *obj2 = dynamic_cast<const ObjectData *>(e2->getObject()); // cast to the internal representation
	if( obj1->getShaderAmbient() < obj2->getShaderAmbient() )
		return -1;
	else if( obj1->getShaderAmbient() > obj2->getShaderAmbient() )
		return 1;
	else if( obj1->getShaderDirectional() < obj2->getShaderDirectional() )
		return -1;
	else if( obj1->getShaderDirectional() > obj2->getShaderDirectional() )
		return 1;
	else if( obj1->getShaderPoint() < obj2->getShaderPoint() )
		return -1;
	else if( obj1->getShaderPoint() > obj2->getShaderPoint() )
		return 1;

	// reduce texture swaps
	if( p1->renderData != NULL && p2->renderData != NULL ) {
		if( p1->renderData->getNTextures() < p2->renderData->getNTextures() ) {
			return -1;
		}
		else if( p1->renderData->getNTextures() > p2->renderData->getNTextures() ) {
			return 1;
		}
		for(int i=0;i<p1->renderData->getNTextures();i++) {
			if( p1->renderData->getTexture(i) < p2->renderData->getTexture(i) ) {
				return -1;
			}
			else if( p1->renderData->getTexture(i) > p2->renderData->getTexture(i) ) {
				return 1;
			}
		}
		/*if( p1->renderData->texture < p2->renderData->texture ) {
			return -1;
		}
		else if( p1->renderData->texture > p2->renderData->texture ) {
			return 1;
		}*/
	}
	else if( p1->renderData == NULL && p2->renderData != NULL ) {
		return -1;
	}
	else if( p1->renderData != NULL && p2->renderData == NULL ) {
		return 1;
	}

	// sort by Z distance (draw closest first, for performance)
	// just go by the local origins, rather than the entity centres, for simplicity
	float dist1 = - e1->getMatrix()->get(14);
	float dist2 = - e2->getMatrix()->get(14);
	if( dist1 < dist2 )
		return -1;
	else if( dist1 > dist2 )
		return 1;

	return 0;
}

bool stl_sortRenderQueue(const Renderable& p1, const Renderable& p2) {
	return sortRenderQueue(&p1, &p2) < 0;
}

#if 0
int sortRenderQueueTransparent(const void *ptr1, const void *ptr2) {
	const Renderable *p1 = (const Renderable *)ptr1;
	const Renderable *p2 = (const Renderable *)ptr2;
	const Entity *e1 = p1->entity;
	const Entity *e2 = p2->entity;
	LOG("%d , %d\n", e1, e2);
	LOG("    %s\n", e1->getName());
	LOG("    %s\n", e2->getName());

	int res = 0;
	bool done = false;

	// sort by Z distance (draw furthest away first, to improve realism)
	// use the entities' centres, rather than their local origins, to get better results
	Sphere sphere1 = e1->getBoundingSphere();
	Sphere sphere2 = e2->getBoundingSphere();
	Vector3D c1 = sphere1.centre;
	Vector3D c2 = sphere2.centre;
	Vector3D eye1 = e1->localToEyePos(&c1);
	Vector3D eye2 = e2->localToEyePos(&c2);
	float dist1 = - eye1.z;
	float dist2 = - eye2.z;
	/*if( (c1 - c2).magnitude() >= sphere1.radius + sphere2.radius ) {
		// bounding spheres don't intersect, okay to do simple test
		if( dist1 < dist2 )
			res = 1; // e2 is further away
		else if( dist1 > dist2 )
			res = -1; // e1 is further away
		done = true;
	}*/
	for(int i=0;i<2 && !done;i++) {
		const Entity *te1 = i==0 ? e1 : e2;
		const Entity *te2 = i==0 ? e2 : e1;
		//if( te1->getObject()->getBoundingAABB
		Box aabb;
		Box aabb2;
		if( te1->getObject()->getBoundingAABB(&aabb) ) {
		//if( te1->getObject()->getBoundingAABB(&aabb) && !te2->getObject()->getBoundingAABB(&aabb2) ) {
			Vector3D aabb_O = aabb.getCorner(0);
			Vector3D aabb_X = aabb.getCorner(1);
			Vector3D aabb_Y = aabb.getCorner(3);
			Vector3D aabb_Z = aabb.getCorner(4);
			Vector3D aabb_O2 = aabb.getCorner(7);
			aabb_O = te1->localToEyePos(&aabb_O);
			aabb_X = te1->localToEyePos(&aabb_X);
			aabb_Y = te1->localToEyePos(&aabb_Y);
			aabb_Z = te1->localToEyePos(&aabb_Z);
			aabb_O2 = te1->localToEyePos(&aabb_O2);
			aabb_X -= aabb_O;
			aabb_Y -= aabb_O;
			aabb_Z -= aabb_O;
			float magX = aabb_X.magnitude();
			float magY = aabb_Y.magnitude();
			float magZ = aabb_Z.magnitude();
			int n_non_zero = 0;
			if( magX >= V_TOL_LINEAR )
				n_non_zero++;
			if( magY >= V_TOL_LINEAR )
				n_non_zero++;
			if( magZ >= V_TOL_LINEAR )
				n_non_zero++;
			if( n_non_zero <= 1 ) {
				// line or degenerate, can't handle
			}
			else {
				if( n_non_zero == 2 ) {
					// planar - work out zero axis from other 2
					if( magX < V_TOL_LINEAR ) {
						aabb_Y /= magY;
						aabb_Z /= magZ;
						aabb_X = aabb_Y ^ aabb_Z;
					}
					else if( magY < V_TOL_LINEAR ) {
						aabb_Z /= magZ;
						aabb_X /= magX;
						aabb_Y = aabb_Z ^ aabb_X;
					}
					else if( magZ < V_TOL_LINEAR ) {
						aabb_X /= magX;
						aabb_Y /= magY;
						aabb_Z = aabb_X ^ aabb_Y;
					}
				}
				else {
					aabb_X /= magX;
					aabb_Y /= magY;
					aabb_Z /= magZ;
				}
				Plane planes[6];
				planes[0].set(- aabb_X, aabb_O);
				planes[1].set(- aabb_Y, aabb_O);
				planes[2].set(- aabb_Z, aabb_O);
				planes[3].set(aabb_X, aabb_O2);
				planes[4].set(aabb_Y, aabb_O2);
				planes[5].set(aabb_Z, aabb_O2);
				Sphere sphere = te2->getBoundingSphere();
				Vector3D eye_sphere_centre = te2->localToEyePos(&sphere.centre);
				for(int j=0;j<6 && !done;j++) {
					if( j != 1 && j != 4 ) {
						continue;
					}
					if( - planes[j].D > V_TOL_LINEAR ) {
						// viewpoint is outside the aabb, on the outside of this plane
						int opp_j = j <= 2 ? (j+3) : (j-3);
						if( eye_sphere_centre % planes[j].normal - planes[j].D > V_TOL_LINEAR ) {
							// te1 is further away
							res = (i==0) ? -1 : 1;
							//res = -res;
							LOG("    box test %d , %d: %d\n", i, j, res);
							done = true;
						}
						else if( eye_sphere_centre % planes[opp_j].normal - planes[opp_j].D > V_TOL_LINEAR ) {
							// te2 is further away
							res = (i==0) ? 1 : -1;
							//res = -res;
							LOG("    box test opp %d , %d: %d\n", i, j, res);
							done = true;
						}
					}
				}
			}
		}
	}

	if( !done ) {
		if( dist1 < dist2 )
			res = 1; // e2 is further away, want to draw e2 first, so "e2 is less than e1"
		else if( dist1 > dist2 )
			res = -1; // e1 is further away, want to draw e1 first, so "e1 is less than e2"
		done = true;
	}
	//res = -res;

	// no point having any more checks, as will be rare to have two objects exactly the same distance

	//LOG("%d %d : %d\n", e1, e2, res);
	//LOG("    res = %d\n", res);
	return res;
}
#endif

int sortRenderQueueTransparent(const void *ptr1, const void *ptr2) {
	const Renderable *p1 = (const Renderable *)ptr1;
	const Renderable *p2 = (const Renderable *)ptr2;
	const SceneGraphNode *e1 = p1->node;
	const SceneGraphNode *e2 = p2->node;
	/*LOG("%d , %d\n", e1, e2);
	LOG("    %s\n", e1->getName());
	LOG("    %s\n", e2->getName());*/

	int res = 0;

	if( p1->sort_preference < p2->sort_preference ) {
		// draw e1 first
		//LOG("sort preference: draw e1 %d first\n", e1);
		res = -1;
	}
	else if( p1->sort_preference > p2->sort_preference ) {
		// draw e2 first
		//LOG("sort preference: draw e2 %d first\n", e2);
		res = 1;
	}
	else {
		// sort by Z distance (draw furthest away first, to improve realism)
		// use the entities' centres, rather than their local origins, to get better results
		Sphere sphere1 = e1->getBoundingSphere();
		Sphere sphere2 = e2->getBoundingSphere();
		Vector3D c1 = sphere1.centre;
		Vector3D c2 = sphere2.centre;
		Vector3D eye1 = e1->localToEyePos(c1);
		Vector3D eye2 = e2->localToEyePos(c2);
		float dist1 = - eye1.z;
		float dist2 = - eye2.z;

		if( dist1 < dist2 ) {
			//LOG("distance: draw e2 %d first\n", e2);
			res = 1; // e2 is further away, want to draw e2 first, so "e2 is less than e1"
		}
		else if( dist1 > dist2 ) {
			//LOG("distance: draw e1 %d first\n", e1);
			res = -1; // e1 is further away, want to draw e1 first, so "e1 is less than e2"
		}
	}

	// no point having any more checks, as will be rare to have two objects exactly the same distance

	/*LOG("%d %d : %d\n", e1, e2, res);
	LOG("    res = %d\n", res);*/
	return res;
}

bool stl_sortRenderQueueTransparent(const Renderable& p1, const Renderable& p2) {
	return sortRenderQueueTransparent(&p1, &p2) < 0;
}

void Graphics3D::drawOpaque(int recursion) {
	//LOG("drawOpaque enter: %d\n", recursion);
	V__ENTER;
	_DEBUG_CHECK_ERROR_;
	//genv->checkGLError("Graphics3D::drawOpaque BEGIN");
	//qsort(queue->renderQueue->getData(), queue->renderQueue->size(), sizeof(void *), sortRenderQueue);
	//qsort(c_queue->renderQueue->begin(), c_queue->renderQueue->size(), sizeof(void *), sortRenderQueue);
	//qsort(&*c_queue->renderQueue->begin(), c_queue->renderQueue->size(), sizeof(Renderable), sortRenderQueue);
	//LOG(">>> %d\n", c_queue->renderQueue->size());
	sort(c_queue->renderQueue->begin(), c_queue->renderQueue->end(), stl_sortRenderQueue);
	{
		// enable fog for ambient pass
		//world->getFog()->draw();
		drawFog();

		//this->setFixedFunctionLighting(NULL, NULL);
		genv->getRenderer()->pushMatrix();
		for(size_t i=0;i<c_queue->nOpaque();i++) {
			Renderable *renderable = &c_queue->renderQueue->at(i);
			SceneGraphNode *node = renderable->node;
			//ObjectData *obj = node->getObject();
			ObjectData *d_obj = dynamic_cast<ObjectData *>(node->getObject()); // cast to the internal representation
			setShaders(NULL, d_obj);
			node->getMatrix()->load();
			_DEBUG_CHECK_ERROR_;
			node->render(this->genv, ObjectData::RENDER_OPAQUE, false, false, renderable->renderData);
			_DEBUG_CHECK_ERROR_;
		}
		genv->getRenderer()->popMatrix();
		this->getRenderer()->disableShaders();
		if( world->getFog()->isEnabled() )
			genv->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_NONE, 0.0f, 0.0f, 0.0f);

		//return; // uncomment for ambient only
		//LOG(">>> %d\n", c_queue->renderQueue_lights->size());

		// lighting pass
		_DEBUG_CHECK_ERROR_;
		for(size_t j=0;j<this->c_queue->renderQueue_lights->size();j++) {

			SceneGraphNode *light_ent = this->c_queue->renderQueue_lights->at(j);

			bool infinite = light_ent->getPoint()->isInfinite();
			Vector3D lightEyePos = light_ent->getLastRenderedEyePos(); // position in eye space
			//LOG("%f , %f, %f\n", lightEyePos.x, lightEyePos.y, lightEyePos.z);
			float light_radius = light_ent->getBoundingSphere().radius;
			/*if( !infinite && ( -lightEyePos.z + light_radius < this->z_near ||
			-lightEyePos.z - light_radius > this->z_far ) ) {
			// should have been clipped away?
			continue;
			}*/
			bool scissor_test = false;
			int scissor_x0, scissor_y0, scissor_x1, scissor_y1;
			//if( false ) {
			if( !infinite && abs(lightEyePos.z) > V_TOL_LINEAR ) {
				scissor_test = true;
				Vector3D scissor_left = lightEyePos;
				Vector3D scissor_right = lightEyePos;
				Vector3D scissor_bottom = lightEyePos;
				Vector3D scissor_top = lightEyePos;
				scissor_left.x -= light_radius;
				scissor_right.x += light_radius;
				scissor_bottom.y -= light_radius;
				scissor_top.y += light_radius;
				/*MouseInputHandler *mouse = this->genv->getInputSystem()->getMouse();
				Vector3D mouse_eye = this->getDirectionOfPoint(mouse->mouseX, mouse->mouseY);
				scissor_x0 = 0;
				scissor_y0 = 0;
				this->eyeToScreen(&scissor_x1, &scissor_y1, &mouse_eye);*/
				int dummy;
				genv->eyeToScreen(&scissor_x0, &dummy, scissor_left);
				genv->eyeToScreen(&scissor_x1, &dummy, scissor_right);
				genv->eyeToScreen(&dummy, &scissor_y1, scissor_bottom);
				genv->eyeToScreen(&dummy, &scissor_y0, scissor_top);
				if( scissor_x0 > scissor_x1 )
					swap(scissor_x0, scissor_x1);
				else if( scissor_x0 == scissor_x1 )
					scissor_test = false;
				if( scissor_y0 > scissor_y1 )
					swap(scissor_y0, scissor_y1);
				else if( scissor_y0 == scissor_y1 )
					scissor_test = false;
				/*assert(scissor_x1 > scissor_x0);
				assert(scissor_y1 > scissor_y0);*/
				/*scissor_y0 = this->height - scissor_y0;
				scissor_y1 = this->height - scissor_y1;*/
			}
			scissor_test = false;
			if( scissor_test ) {
				//genv->getRenderer()->enableScissor(0, 0, this->width/2.0, this->height/2.0);
				//LOG("%d , %d , %d", scissor_y0, scissor_y1, scissor_y1-scissor_y0);
				genv->getRenderer()->enableScissor(scissor_x0, scissor_y1, scissor_x1-scissor_x0, scissor_y1-scissor_y0);
				//genv->getRenderer()->enableScissor(scissor_x0, genv->getHeight()-scissor_y1, scissor_x1-scissor_x0, scissor_y1-scissor_y0);
			}

			if( use_shadow_volumes && c_queue->renderQueue_shadowcasters->size() > 0 )
			{
				_DEBUG_CHECK_ERROR_;
				genv->getRenderer()->clear(false, false, true);
				// draw shadow volumes
				genv->getRenderer()->enableTexturing(false);
				genv->getRenderer()->enableFixedFunctionLighting(false);
				genv->getRenderer()->enableColorMask(false);
				genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READONLY);

				//bool infinite = light_ent->getPoint()->isInfinite();
				//Vector3D lightEyePos = light_ent->getCurrentEyePos(); // position in eye space
				if( infinite ) {
					lightEyePos.normalise();
				}

				/*if( vertex_shader_shadow_point != NULL ) {
				vertex_shader_shadow_point->enable();
				}*/

				_DEBUG_CHECK_ERROR_;
				genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_SHADOWPASS_ONE);
				_DEBUG_CHECK_ERROR_;

				// 1st pass
				genv->getRenderer()->pushMatrix();
				for(size_t i=0;i<c_queue->renderQueue_shadowcasters->size();i++) {
					SceneGraphNode *node = c_queue->renderQueue_shadowcasters->at(i);
					if( !light_ent->collision(node) ) {
						continue;
					}
					// TODO: clipping for (point) shadow volumes
					if( infinite ) {
						Sphere boundingSphere = node->getObject()->getBoundingSphere();
						Vector3D centre = node->localToEyePos(boundingSphere.centre);
						//if( !frustum.infiniteShadowVolumeInFrustum(centre.x, centre.y, centre.z, boundingSphere.radius, -lightEyePos.x, -lightEyePos.y, -lightEyePos.z) ) {
						if( !genv->getFrustum()->infiniteShadowVolumeInFrustum(centre.x, centre.y, centre.z, boundingSphere.radius, -lightEyePos.x, -lightEyePos.y, -lightEyePos.z) ) {
							//printf("");
							continue;
						}
					}
					_DEBUG_CHECK_ERROR_;
					// need to convert the lightpos into the node's local space
					Vector3D lightPos = lightEyePos;
					//LOG("***\n");
					//LOG("%f , %f, %f\n", lightPos.x, lightPos.y, lightPos.z);

					// TODO: improve performance
					Matrix m;
					float values[16];
					for(int indx=0;indx<16;indx++) {
						values[indx] = node->getMatrix()->get(indx);
						//LOG(">>> matrix[%d] = %f\n", indx, values[indx]);
					}
					m.setOpenGLTranspose(values);
					//m.setOpenGLTranspose(node->mvmatrix);

					if( !infinite ) {
						Vector3D nodeEyePos = node->getLastRenderedEyePos();
						//LOG("eye %f , %f, %f\n", nodeEyePos.x, nodeEyePos.y, nodeEyePos.z);
						lightPos -= nodeEyePos;
						//LOG("%f , %f, %f\n", lightPos.x, lightPos.y, lightPos.z);
					}
					m.transformVector(&lightPos);
					node->getMatrix()->load();
					//LOG("%f , %f, %f\n", lightPos.x, lightPos.y, lightPos.z);
					node->castShadow(this->getRenderer(), this->genv, lightPos, infinite);
				}
				genv->getRenderer()->popMatrix();

				if( genv->getRenderer()->nShadowPasses() == 1 ) {
					genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_SHADOWPASS_OFF);
				}
				//else if( false ) {
				else {
					// 2nd pass
					_DEBUG_CHECK_ERROR_
					genv->getRenderer()->setFrontFace( recursion % 2 != 0 );
					genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_SHADOWPASS_TWO);
					genv->getRenderer()->pushMatrix();
					for(size_t i=0;i<c_queue->renderQueue_shadowcasters->size();i++) {
						SceneGraphNode *node = c_queue->renderQueue_shadowcasters->at(i);
						if( !light_ent->collision(node) ) {
							continue;
						}
						// TODO: clipping for shadow volumes
						// need to convert the lightpos into the node's local space
						Vector3D lightPos = lightEyePos;

						// TODO: improve performance
						Matrix m;
						float values[16];
						for(int indx=0;indx<16;indx++)
							values[indx] = node->getMatrix()->get(indx);
						m.setOpenGLTranspose(values);
						//m.setOpenGLTranspose(node->mvmatrix);

						if( !infinite ) {
							Vector3D nodeEyePos = node->getLastRenderedEyePos();
							lightPos -= nodeEyePos;
						}
						m.transformVector(&lightPos);
						node->getMatrix()->load();
						node->castShadow(this->getRenderer(), this->genv, lightPos, infinite);
					}
					genv->getRenderer()->popMatrix();
					genv->getRenderer()->setFrontFace( recursion % 2 == 0 );
				}
				_DEBUG_CHECK_ERROR_

				genv->getRenderer()->enableTexturing(true);
				genv->getRenderer()->enableFixedFunctionLighting(true);
				genv->getRenderer()->enableColorMask(true);
				genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);

				/*{
					const Shader *c_vertex_shader = getRenderer()->getActiveVertexShader();
					const Shader *c_pixel_shader = getRenderer()->getActivePixelShader();
					if( c_vertex_shader != NULL )
						c_vertex_shader->disable();
					if( c_pixel_shader != NULL )
						c_pixel_shader->disable();
				}*/
				this->getRenderer()->disableShaders();
			}

			_DEBUG_CHECK_ERROR_
			/*float col[4] = {0.0, 0.0, 0.0, 0.0};
			genv->getRenderer()->setAmbient(col);
			enableLight(j);*/
			//this->setFixedFunctionLighting(light_ent, NULL);
			//genv->getRenderer()->enableColorMask(true);
			if( use_shadow_volumes && c_queue->renderQueue_shadowcasters->size() > 0 )
			{
				// only draw where stencil isn't; disallow overdraw
				genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_DRAWATNOSTENCIL);
			}
			genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
			genv->getRenderer()->setDepthBufferFunc(Renderer::RENDERSTATE_DEPTHFUNC_EQUAL);
			genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READONLY);
			_DEBUG_CHECK_ERROR_
			/*if( scissor_test ) {
			//genv->getRenderer()->enableScissor(0, 0, this->width/2.0, this->height/2.0);
			genv->getRenderer()->enableScissor(scissor_x0, scissor_y0, scissor_x1-scissor_x0, scissor_y1-scissor_y0);
			}*/

			// draw in mirrors with lighting
			for(RQShadowplanesIter iter = c_queue->renderQueue_shadowplanes->begin();iter != c_queue->renderQueue_shadowplanes->end();++iter) {
				SceneGraphNode *mirror = *iter;
				if( !light_ent->collision(mirror) ) {
					continue;
				}
				ObjectData *d_obj = dynamic_cast<ObjectData *>(mirror->getObject()); // cast to the internal representation
				ShadowPlane *shadow = d_obj == NULL ? NULL : d_obj->getShadowPlane();
				// projected-shadow-receivers have already been drawn
				if( !shadow->isReflection() || use_shadow_volumes ) {
					ObjectData *d_obj = dynamic_cast<ObjectData *>(mirror->getObject()); // cast to the internal representation
					setShaders(light_ent, d_obj);
					genv->getRenderer()->pushMatrix();
					mirror->getMatrix()->load();
					mirror->render(this->genv, ObjectData::RENDER_STENCIL, true, false, NULL);
					genv->getRenderer()->popMatrix();
				}
			}
			/*{
				const Shader *c_vertex_shader = getRenderer()->getActiveVertexShader();
				const Shader *c_pixel_shader = getRenderer()->getActivePixelShader();
				if( c_vertex_shader != NULL )
					c_vertex_shader->disable();
				if( c_pixel_shader != NULL )
					c_pixel_shader->disable();
			}*/
			this->getRenderer()->disableShaders();

			// draw in opaque objects with lighting
			genv->getRenderer()->pushMatrix();
			for(RQRenderableIter iter = c_queue->renderQueue->begin();iter != c_queue->renderQueue->end();++iter) {
				Renderable *renderable = &(*iter);
				SceneGraphNode *node = renderable->node;
				if( !light_ent->collision(node) ) {
					continue;
				}
				node->getMatrix()->load();
				//ObjectData *obj = node->getObject();
				ObjectData *d_obj = dynamic_cast<ObjectData *>(node->getObject()); // cast to the internal representation
				/*if( obj->vertex_shader_ambient != NULL || obj->pixel_shader_ambient != NULL ) {
				printf("");
				continue; // custom shaders don't support lighting atm
				}*/
				setShaders(light_ent, d_obj);
				node->render(this->genv, ObjectData::RENDER_OPAQUE, true, false, renderable->renderData);
			}
			genv->getRenderer()->popMatrix();

			if( scissor_test )
			{
				genv->getRenderer()->disableScissor();
			}
			genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			genv->getRenderer()->setDepthBufferFunc(Renderer::RENDERSTATE_DEPTHFUNC_LESS);
			genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);
			//world->lightAmbient.floatArray(col);
			/*world->getAmbience(&col[0], &col[1], &col[2]);
			col[3] = 1.0f;
			genv->getRenderer()->setAmbient(col);
			disableLight(j);*/
			//this->setFixedFunctionLighting(NULL);
			/*{
				const Shader *c_vertex_shader = getRenderer()->getActiveVertexShader();
				const Shader *c_pixel_shader = getRenderer()->getActivePixelShader();
				if( c_vertex_shader != NULL )
					c_vertex_shader->disable();
				if( c_pixel_shader != NULL )
					c_pixel_shader->disable();
			}*/
			this->getRenderer()->disableShaders();
		}
		this->active_light = NULL;
		if( use_shadow_volumes && c_queue->renderQueue_shadowcasters->size() > 0 )
		{
			genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_NONE);
		}

	}
	_DEBUG_CHECK_ERROR_;
	V__EXIT;
	//LOG("drawOpaque exit: %d\n", recursion);
}

void Graphics3D::drawTransparent() {
	//return;
	// [
	/*if( vertex_shader_ambient != NULL ) {
	cgGLSetParameter4f( vertex_shader_ambient->cgparam_AmbLight, world->lightAmbient.getRf(), world->lightAmbient.getGf(),  world->lightAmbient.getBf(), 1.0f );
	vertex_shader_ambient->enable();
	}*/
	// no pixel shader for ambient
	// ]
	if( c_queue->renderQueue_nonopaque->size() == 0 ) {
		return; // quick exit
	}

	V__ENTER;

	// For transparent objects, we can't use the z-buffer, and instead have to simulate it by z-sorting.
	// This also means we need to draw ambient then lighting passes for each object (since the primary sort is via z-distance).
	// Compared with opaque rendering, where we draw all the objects in ambient, then do the objects for each light (since it's better for performance, to minimise shader changes, and also helps with stencil shadows).
	// Note that we don't support rendering shadows onto transparent objects - we could do, but it would be bad for performance, having to repeatedly mark out the stencil/shadow regions for every light, for every object!
	// It's easier to not bother - most the time this shouldn't be obvious on transparent objects.
	// Note that transparent objects can cast shadows, but the shadows are solid rather than partial - i.e., we don't supporting letting some light through, it's all or nothing (which is really a limitation of stencil shadows).
	// Transparent objects can never be rendered perfectly (due to the z buffer issues, and also no shadowing) - in particular, objects with a high (e.g., 254) alpha will look wrong - it's better to avoid nbnq opaque objects, and simply set them to be opaque.
	// For objects that have lower alpha, then the deficiencies will be far less obvious.

	genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READONLY);

	// z-sorting
	if( world->getTerrain() != NULL && world->getTerrain()->hasSea() ) {
		float sea_height = world->getTerrain()->getSeaHeight();
		const VI_SceneGraphNode *vp = world->getViewpoint();
		Vector3D vp_world_pos = vp->getCurrentWorldPos();
		bool underwater = vp_world_pos.y < sea_height;
		for(RQRenderableIter iter = c_queue->renderQueue_nonopaque->begin();iter != c_queue->renderQueue_nonopaque->end();++iter) {
			Renderable *renderable = &*iter;
			SceneGraphNode *node = renderable->node;
			//Vector3D world_pos = node->getCurrentWorldPos();
			// better to use the world pos of the centre
			Vector3D world_pos = node->localToWorldPos( node->getBoundingSphere().centre );
			if( world_pos.y - V_TOL_LINEAR > sea_height ) {
				renderable->sort_preference = 1; // draw later if above water (and viewpoint is above water)
			}
			else if( world_pos.y + V_TOL_LINEAR < sea_height ) {
				renderable->sort_preference = -1; // draw earlier if underwater (and viewpoint is above water)
			}
			if( underwater ) {
				renderable->sort_preference = - renderable->sort_preference; // if underwater, drawing order is reversed
			}
			//LOG("%d : %s : sort preference %d\n", node, node->getName(), renderable->sort_preference);
		}
	}

	sort(c_queue->renderQueue_nonopaque->begin(), c_queue->renderQueue_nonopaque->end(), stl_sortRenderQueueTransparent);
	/*LOG("transparent nodes:\n");
	for(RQRenderableIter iter = c_queue->renderQueue_nonopaque->begin();iter != c_queue->renderQueue_nonopaque->end();++iter) {
		Entity *node = (*iter).entity;
		LOG("    %d : %s\n", node, node->getName());
	}*/
	
	genv->getRenderer()->pushMatrix();
	for(RQRenderableIter iter = c_queue->renderQueue_nonopaque->begin();iter != c_queue->renderQueue_nonopaque->end();++iter) {
		SceneGraphNode *node = (*iter).node;
		node->getMatrix()->load();
		ObjectData *d_obj = dynamic_cast<ObjectData *>(node->getObject()); // cast to the internal representation
		// draw ambient light
		setShaders(NULL, d_obj);
		_DEBUG_CHECK_ERROR_;
		node->render(this->genv, ObjectData::RENDER_NONOPAQUE, false, false, NULL);
		_DEBUG_CHECK_ERROR_;

		// lighting pass
		//if( false ) // uncomment for ambient only
		for(size_t j=0;j<this->c_queue->renderQueue_lights->size();j++) {

			SceneGraphNode *light_ent = this->c_queue->renderQueue_lights->at(j);

			bool infinite = light_ent->getPoint()->isInfinite();
			Vector3D lightEyePos = light_ent->getLastRenderedEyePos(); // position in eye space
			float light_radius = light_ent->getBoundingSphere().radius;

			_DEBUG_CHECK_ERROR_
			/*float col[4] = {0.0, 0.0, 0.0, 0.0};
			genv->getRenderer()->setAmbient(col);
			enableLight(j);*/
			//this->setFixedFunctionLighting(light_ent);
			//genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
			_DEBUG_CHECK_ERROR_

			if( !light_ent->collision(node) ) {
				continue;
			}
			setShaders(light_ent, d_obj);
			_DEBUG_CHECK_ERROR_;
			node->render(this->genv, ObjectData::RENDER_NONOPAQUE, true, false, NULL);
			_DEBUG_CHECK_ERROR_;

			//genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			/*world->getAmbience(&col[0], &col[1], &col[2]);
			col[3] = 1.0f;
			genv->getRenderer()->setAmbient(col);
			disableLight(j);*/
			//this->setFixedFunctionLighting(NULL);
		}
		this->active_light = NULL;
	}
	genv->getRenderer()->popMatrix();
	this->getRenderer()->disableShaders();
	genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
	genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);

	V__EXIT;
}

inline float sgn(float a) {
	if (a > 0.0F) return (1.0F);
	if (a < 0.0F) return (-1.0F);
	return (0.0F);
}

void Graphics3D::setClipPlane(const double pl[4]) {
	genv->getRenderer()->enableClipPlane(pl);

	/*if( !genv->hasShaders() ) {
	// no shaders
	return;
	}

	// user clip planes seem to be supported for fixed pipeline, but not always
	// when using shaders (e.g., NVIDIA 8600GT, Intel GMA950).
	// we need to set with _both_ methods, because a scene can mix shaders and non-shaders!

	TransformationMatrix *model_matrix = this->genv->getRenderer()->createTransformationMatrix();

	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->translate((float)(pl[3]*pl[0]), (float)(pl[3]*pl[1]), (float)(pl[3]*pl[2]));
	model_matrix->save();
	genv->getRenderer()->popMatrix();
	Vector3D new_p = model_matrix->getPos();

	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->translate((float)pl[0], (float)pl[1], (float)pl[2]);
	model_matrix->save();
	genv->getRenderer()->popMatrix();
	Vector3D new_n = model_matrix->getPos();
	model_matrix->save();
	new_n.x -= (*model_matrix)[12];
	new_n.y -= (*model_matrix)[13];
	new_n.z -= (*model_matrix)[14];
	assert( new_n.magnitude() > 0 );
	new_n.normalise();
	delete model_matrix;

	{
	// shader clipping
	this->shader_clipping = true;
	this->shader_clipping_plane[0] = new_n.x;
	this->shader_clipping_plane[1] = new_n.y;
	this->shader_clipping_plane[2] = new_n.z;
	this->shader_clipping_plane[3] = new_n % new_p;
	return;
	}*/

	// oblique code

	/*float matrix[16];
	// Grab the current projection matrix from OpenGL
	glGetFloatv(GL_PROJECTION_MATRIX, matrix);

	pl[0] = new_n.x;
	pl[1] = new_n.y;
	pl[2] = new_n.z;
	pl[3] = new_n % new_p;
	//pl[3] = 2;

	//pl[3] = 10;
	//assert( pl[3] <= 0 );
	//glGetFloatv(GL_MODELVIEW_INVERSE_MATRIX, model_matrix);

	// Calculate the clip-space corner point opposite the clipping plane
	// as (sgn(clipPlane.x), sgn(clipPlane.y), 1, 1) and
	// transform it into camera space by multiplying it
	// by the inverse of the projection matrix
	float q_x = (sgn((float)pl[0]) + matrix[8]) / matrix[0];
	float q_y = (sgn((float)pl[1]) + matrix[9]) / matrix[5];
	float q_z = -1.0f;
	float q_w = (1.0f + matrix[10]) / matrix[14];

	// Calculate the scaled plane vector
	//Vector4D c = clipPlane * (2.0F / Dot(clipPlane, q));
	float dot = q_x * (float)pl[0] + q_y * (float)pl[1] + q_z * (float)pl[2] + q_w * (float)pl[3];
	float scale = 2.0f / dot;

	//scale *= -5.0;
	//scale = 100;
	// Replace the third row of the projection matrix
	matrix[2] = (float)pl[0] * scale;
	matrix[6] = (float)pl[1] * scale;
	matrix[10] = (float)pl[2] * scale + 1.0f;
	//matrix[10] = pl[2] * scale - 1.0F;
	//matrix[10] = 0;
	//matrix[10] = pl[2] * scale;
	matrix[14] = (float)pl[3] * scale;

	//matrix[2] = matrix[10] = 0;
	//matrix[14] = - matrix[6];

	// Load it back into OpenGL
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadMatrixf(matrix);
	glMatrixMode(GL_MODELVIEW);
	//trace_extractFrustum();
	*/
}

void Graphics3D::unsetClipPlane() {
	genv->getRenderer()->disableClipPlane();

	//this->shader_clipping = false;
	return;
}

void Graphics3D::resetClipPlane() {
	genv->getRenderer()->reenableClipPlane();
	/*if( genv->hasShaders() ) {
	this->shader_clipping = true;
	}*/
}

void Graphics3D::drawSector(Sector *sector,SceneGraphNode *portal,int recursion) {
	V__ENTER;
	_DEBUG_CHECK_ERROR_;
	SceneGraphNode *root = sector->rootObject;
	const int MAX_RECURSION = 1;
	RenderQueue *queue = new RenderQueue(); // TODO: preallocate
	this->c_queue = queue;

	//if( recursion < MAX_RECURSION && world->getShadowNodes()->size() > 0 ) {
	if( recursion < MAX_RECURSION ) {
		//root->getShadows(this,shadows,&n_shadows,MAX_SHADOWS,portal);
		root->getShadows(this->c_queue, genv, portal);
	}

	TerrainEngine *d_terrain = static_cast<TerrainEngine *>(world->getTerrain()); // cast to the internal representation
	if( recursion < MAX_RECURSION && d_terrain != NULL && d_terrain->getSeaReflection() != NULL ) {
		// TODO: improve
		//shadows[n_shadows++] = world->terrain->entity_sea_dummy;
		//this->c_queue->renderQueue_shadowplanes->addIfAbsent(world->terrain->entity_sea_dummy);
		/*if( find(this->c_queue->renderQueue_shadowplanes->begin(), this->c_queue->renderQueue_shadowplanes->end(), world->terrain->entity_sea_dummy) == this->c_queue->renderQueue_shadowplanes->end() ) {
		this->c_queue->renderQueue_shadowplanes->push_back(world->terrain->entity_sea_dummy);
		}*/
		//addIfAbsent_vec(this->c_queue->renderQueue_shadowplanes, world->terrain->entity_sea_dummy);
		this->c_queue->renderQueue_shadowplanes->insert(d_terrain->getSeaReflection());
		if( d_terrain->getSeaReflection()->getMatrix() == NULL ) {
			d_terrain->getSeaReflection()->setMatrix( this->getRenderer()->createTransformationMatrix() );
		}
		//world->terrain->entity_sea_dummy->getShadows(this,shadows,&n_shadows,MAX_SHADOWS,portal);
	}

	//int n_shadows = c_queue->renderQueue_shadowplanes->size();
	if( recursion >= MAX_RECURSION ) {
		c_queue->renderQueue_shadowplanes->clear();
		//n_shadows = 0;
	}

	// mirrors/reflections - render to texture method (water)
	if( c_queue->renderQueue_shadowplanes->size() > 0 && genv->hasShaders() && this->hasWaterShaders() ) {
		bool set_res = false;
		//for(size_t i=0;i<c_queue->renderQueue_shadowplanes->size();i++) {
		for(RQShadowplanesIter iter = c_queue->renderQueue_shadowplanes->begin();iter != c_queue->renderQueue_shadowplanes->end();++iter) {
			// determine visibility of shadowplane
			SceneGraphNode *mirror = *iter;
			ObjectData *d_obj = dynamic_cast<ObjectData *>(mirror->getObject()); // cast to the internal representation
			ShadowPlane *shadow = d_obj == NULL ? NULL : d_obj->getShadowPlane();
			// shadow shouldn't be NULL!
			if( !shadow->isReflection() || !shadow->isRenderToTexture() ) {
				continue;
			}
			if( !shadow->needUpdate() ) {
				continue;
			}
			//continue;

			//if( false )
			{
				//if( !Vision::framebufferObject ) {
				if( !set_res ) {
					// first time - buffers already cleared
					if( recursion == 0 && this->hdr_enabled ) {
						// if using HDR, for recursed levels, we just render as non-HDR
						// to get HDR for the reflection, we'd have to make sure we copy from the HDR FBO to the reflection texture
						// but for now, this works well enough...
						genv->getRenderer()->popAttrib();
						genv->getRenderer()->unbindFramebuffer();
					}
					int texture_size = this->getRenderToTextureSize();
					genv->getRenderer()->setViewport(texture_size, texture_size);
					set_res = true;
				}
				else {
					// need to reclear buffers
					this->clearFrame();
				}
			}

			const Plane *plane = shadow->getPlane();
			double pl[4] = { -plane->normal.x, -plane->normal.y, -plane->normal.z, -plane->D };
			setClipPlane(pl);

			genv->getRenderer()->pushMatrix();
			Vector3D point = plane->normal * plane->D;
			genv->getRenderer()->translate(point.x, point.y, point.z);
			genv->getRenderer()->scale(1.0, -1.0, 1.0); // TODO: transformation is hardcoded atm
			genv->getRenderer()->translate(-point.x, -point.y, -point.z);

			genv->getRenderer()->setFrontFace(false); // TODO: fix for multiple recursions
			drawSector(sector, mirror, recursion+1);
			this->c_queue = queue; // need to reset!
			genv->getRenderer()->setFrontFace(true); // TODO: fix for multiple recursions
			genv->getRenderer()->popMatrix();

			unsetClipPlane();

			//if( false )
			{
				int texture_size = this->getRenderToTextureSize();
				//this->clearFrame(); // test
				shadow->getRenderTexture()->copyTo(texture_size, texture_size);
			}
			//return; // uncomment to see the reflection texture (for non-HDR)
		}
		if( set_res )
		{
			this->setViewport(); // OpenGL needs setting the viewport before enabling the HDR FBO!
			if( recursion == 0 && this->hdr_enabled ) {
				// reenable the HDR FBO
				this->hdr_fbo->bind();
				genv->getRenderer()->pushAttrib(true);
			}
			// need to reclear frame
			this->clearFrame();
		}
	}
	/*if( recursion == 0 )
	return;*/

	if( recursion > 0 ) {
		unsetClipPlane();
	}
	//genv->getRenderer()->disableFog(); // don't render fog on the skybox etc
	if( world->getFog()->isEnabled() )
		genv->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_NONE, 0.0f, 0.0f, 0.0f); // don't render fog on the skybox etc
	drawSky(queue, portal);
	if( recursion > 0 ) {
		resetClipPlane();
	}

	// mirrors/reflections - stencil method
	/*if( n_shadows > 1 )
	n_shadows = 1; // only one mirror for now*/
	//if( false )
	for(RQShadowplanesIter iter = c_queue->renderQueue_shadowplanes->begin();iter != c_queue->renderQueue_shadowplanes->end();++iter) {
		// determine visibility of shadowplane
		SceneGraphNode *mirror = *iter;
		ObjectData *d_obj = dynamic_cast<ObjectData *>(mirror->getObject()); // cast to the internal representation
		ShadowPlane *shadow = d_obj == NULL ? NULL : d_obj->getShadowPlane();
		// shadow shouldn't be NULL!
		if( !shadow->isReflection() || shadow->isRenderToTexture() ) {
			continue;
		}
		//continue;

		// do opaque objects' reflections
		{
			// mark out where to draw reflections
			genv->getRenderer()->clear(false, false, true);
			genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_REPLACE);
			genv->getRenderer()->enableColorMask(false);
			genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_NONE);

			genv->getRenderer()->pushMatrix();
			mirror->getMatrix()->load();
			mirror->render(this->genv, ObjectData::RENDER_STENCIL, false, false, NULL);
			genv->getRenderer()->popMatrix();

			genv->getRenderer()->enableColorMask(true);
			genv->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);
			// only draw where stencil is; allow overdraw
			genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_DRAWATSTENCIL_OVERDRAW);
		}
		genv->getRenderer()->pushMatrix();
		const Plane *plane = shadow->getPlane();
		const double pl[4] = { -plane->normal.x, -plane->normal.y, -plane->normal.z, -plane->D };
		setClipPlane(pl);
		Vector3D point = plane->normal * plane->D;
		genv->getRenderer()->translate(point.x, point.y, point.z);
		//genv->getRenderer()->translate(0, 0.1, 0);
		genv->getRenderer()->scale(1.0f, -1.0f, 1.0f); // TODO: transformation is hardcoded atm
		genv->getRenderer()->translate(-point.x, -point.y, -point.z);
		genv->getRenderer()->setFrontFace(false); // TODO: fix for multiple recursions
		drawSector(sector, mirror, recursion+1);
		this->c_queue = queue; // need to reset!
		genv->getRenderer()->setFrontFace(true); // TODO: fix for multiple recursions
		unsetClipPlane();
		genv->getRenderer()->popMatrix();
		genv->getRenderer()->setStencilMode(Renderer::RENDERSTATE_STENCIL_NONE);
	}

	buildRenderQueue(sector);

	if( recursion > 0 ) {
		/*
		Shadow volumes disabled on recursion, due to conflicts with stencil buffer usage.
		Really needs using render to texture for recursion to work.
		Note that recursing 1 level works with shadow volumes, but it's a bit of a hack:
		the objects are drawn in ambient pass with the reflection stencil still enabled,
		and only after is it blown away. But when we have the lighting pass, due to drawing
		with z-equal, we will only draw where we are supposed to. This falls apart for 2
		levels of recursion!
		TODO: okay for render-to-texture?
		*/
		queue->renderQueue_shadowcasters->clear();
	}

	// only ambient lighting for now
	//disableLights();

	drawMirrors();

	if( !use_shadow_volumes ) {
		doProjectiveShadows(sector);
	}

	// do opaque objects
	drawOpaque(recursion);

	// reenable fog for the rest
	//world->getFog()->draw();
	this->drawFog();

	// do transparent objects
	drawTransparent();

	delete queue;
	this->c_queue = NULL;
	_DEBUG_CHECK_ERROR_;
	V__EXIT;
}

/*void Graphics3D::setSpecular(const float specular[4]) {
	genv->getRenderer()->setSpecular(specular);
}

void Graphics3D::setSpecularOff() {
	genv->getRenderer()->setSpecularOff();
}*/

/* Dummy function - used when things aren't working too well..
*/
void test() {
}

void Graphics3D::set3DMode(bool mode_3d) {
	genv->getRenderer()->enableCullFace(mode_3d);
	genv->getRenderer()->enableTexturing(mode_3d);
	genv->getRenderer()->enableFixedFunctionLighting(mode_3d);
	genv->getRenderer()->setDepthBufferMode(mode_3d ? Renderer::RENDERSTATE_DEPTH_READWRITE : Renderer::RENDERSTATE_DEPTH_NONE);
	//genv->getRenderer()->setColor4(255, 255, 255, 255); // make sure that color is reset, both on enter and exit
}

/* Render the frame!
*/
void Graphics3D::drawFrame() {
	//LOG("drawFrame\n");
	V__ENTER;
	_DEBUG_CHECK_ERROR_;

	const int frame_count = 20;
	if( frames == 0 ) {
		frame_time = genv->getRealTimeMS();
	}
	else if( frames % frame_count == 0 ) {
		int new_frame_time = genv->getRealTimeMS();
		int time = new_frame_time - frame_time;
		if(time != 0)
			fps = (frame_count*1000.0f)/((float)time);
		frame_time = new_frame_time;
	}
	frames++;
	//this->current_time = genv->getRealTimeMS();

	//test();
	//return;
	/* We disable the early exit with NULL world, as this somehow messes up the alt-tab behaviour on Direct3D9 with "Conquests" on fullscreen mode - seems to
	   be a problem with DrawPrimitiveUP calls being messed up. We should investigate getting rid of DrawPrimitiveUP calls to see if that helps, but in the
	   meantime we disable this early exit. (It's probably better to avoid exiting earlier anyway, for more consistent behaviour.)
	   The problem shows up both on Intel GMA (Windows XP), and NVIDIA 8600 (Windows 7). Problem occurs with both SDL and non-SDL.
	*/
	/*if( world == NULL ) {
		return;
	}*/
	if( world != NULL && Vision::isLocked() ) {
		// we allow Vision to be locked if no world is set (needed for Dungeon)
		Vision::setError(new VisionException(this, VisionException::V_LOCKED, "Graphics3D::drawFrame() not allowed when locked"));
	}
	//return; // test

	//hdr_enabled = false;
	if( this->hdr_enabled ) {
		this->hdr_fbo->bind();
		genv->getRenderer()->pushAttrib(true);
		this->setViewport();
		this->clearFrame();
	}

	_DEBUG_CHECK_ERROR_;
	genv->getRenderer()->switchToProjection();
	genv->getRenderer()->popMatrix();
	genv->getRenderer()->switchToModelview();

	_DEBUG_CHECK_ERROR_;

	this->set3DMode(true);

	_DEBUG_CHECK_ERROR_;

	genv->getRenderer()->enableMultisample(true); // GL_MULTISAMPLE_ARB has no effect on crappy ATI, but this is needed for other cards
	_DEBUG_CHECK_ERROR_;

	/*
	// disabled for now, as causes OpenGL error on non-NVIDIA cards - how to test if this hint is available?
	GLenum hints = GL_NICEST;
	glHint(GL_MULTISAMPLE_FILTER_HINT_NV, hints);
	*/

	_DEBUG_CHECK_ERROR_;

	/*if( world != NULL ) {
		// ambient light
		float col[4];
		//world->lightAmbient.floatArray(col);
		world->getAmbience(&col[0], &col[1], &col[2]);
		col[3] = 1.0f;
		genv->getRenderer()->setFixedFunctionAmbientLighting(col);
	}*/

	_DEBUG_CHECK_ERROR_;

	// do viewpoint
	genv->getRenderer()->loadIdentity();
	if( world != NULL ) {
		//world->viewPoint->multGLMatrixInverse();
		SceneGraphNode *vp = dynamic_cast<SceneGraphNode *>(world->getViewpoint()); // cast to internal representation
		vp->multGLMatrixInverse();
		// recurse up the hierarchy, if necessary
		SceneGraphNode *parent = dynamic_cast<SceneGraphNode *>(world->getViewpoint()->getParentNode()); // cast to internal representation
		while(parent != NULL) {
			parent->multGLMatrixInverse();

			//parent = parent->getParent();
			parent = dynamic_cast<SceneGraphNode *>(parent->getParentNode()); // cast to internal representation
		}
	}
	if( this->worldToEyeMatrix == NULL ) {
		this->worldToEyeMatrix = this->getRenderer()->createTransformationMatrix();
	}
	this->worldToEyeMatrix->save();

	if( world != NULL ) {
		drawSector(world->getSector(), NULL, 0);
	}

	if( genv->renderFunc != NULL ) {
		(*genv->renderFunc)();
	}

	// reset
	genv->getRenderer()->switchToProjection();
	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->ortho2D(genv->getWidth(), genv->getHeight());
	genv->getRenderer()->switchToModelview();
	genv->getRenderer()->loadIdentity();
	this->active_light = NULL;

	this->set3DMode(false);

	this->renderHDRToScreen();

	// disable so that 2D/text rendering is sharp
	genv->getRenderer()->enableMultisample(false); // GL_MULTISAMPLE_ARB has no effect on crappy ATI, but this is needed for other cards
	V__EXIT;
}

void Graphics3D::renderHDRToScreen() {
	if( this->hdr_enabled ) {
		const int width = genv->getWidth();
		const int height = genv->getHeight();

		genv->getRenderer()->popAttrib();
		genv->getRenderer()->unbindFramebuffer();
		genv->getRenderer()->enableTexturing(true);

		//genv->getRenderer()->enableHdrFbo();
		this->hdr_fbo->enableTexture();
		//bloom_enabled = false;

		/*if( bloom_enabled ) {
			this->std_shaders.blurw_pixel_shader->enable();
			this->getRenderer()->getActivePixelShader()->SetUniformParameter1f("tx_step_x", 1.0f/(float)width);

			// first of a two-phase process, so we must direct the output into the temp framebuffer
			this->temp_fbo->bind();
			genv->getRenderer()->pushAttrib(true);
			this->setViewport();
			this->clearFrame();
		}
		else*/ {
			// we need a vertex shader, otherwise this doesn't work on Direct3D9 / NVIDIA 8600.
			/*this->std_shaders.hdr_vertex_shader->enable();
			this->std_shaders.hdr_pixel_shader->enable();*/
			this->std_shaders.hdr_shader->enable(this->getRenderer());
		}

		//this->blurw_pixel_shader->enable();
		//this->blurh_hdr_pixel_shader->enable();
		//this->c_pixel_shader->SetUniformParameter1f("tx_step_y", 1.0f/(float)height);

		float texcoord_data[] = {
			0.0f, 1.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f
		};
		float texcoord_data_inv[] = {
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f
		};
		// the hdr vertex shader doesn't transform vertex, so we hand the transformed vertices in
		float vertex_data[] = {
			-1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			-1.0f, -1.0f, 0.0f
		};
		bool inverted = this->getRenderer()->isYTextureInverted();
		this->getRenderer()->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, inverted ? texcoord_data_inv : texcoord_data, 4);

		//this->getRenderer()->getActivePixelShader()->disable();
		//this->std_shaders.hdr_shader->disable(this->getRenderer());
		this->getRenderer()->disableShaders();

		/*if( bloom_enabled ) {
			genv->getRenderer()->popAttrib();
			genv->getRenderer()->unbindFramebuffer();

			// second phase - render from temp_fbo into screen
			this->temp_fbo->enableTexture();
			this->std_shaders.blurh_hdr_pixel_shader->enable();

			this->getRenderer()->getActivePixelShader()->SetUniformParameter1f("tx_step_y", 1.0f/(float)height);

			this->getRenderer()->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4);

			this->getRenderer()->getActivePixelShader()->disable();
			this->getRenderer()->getActiveVertexShader()->disable();
		}*/

		//this->getRenderer()->getActiveVertexShader()->disable();
		genv->getRenderer()->enableTexturing(false);

	}

}

void Graphics3D::clearFrame() {
	V__ENTER;
	if( world != NULL ) {
		if( world->getSky() == NULL ) {
			if( world->getAtmosphere() ) {
				if( world->getClouds() != NULL ) {
					// since we'll be blending the atmosphere over the clouds, and the cloud layer doesn't fill the screen, we need a black background first!
					genv->getRenderer()->setClearColor(0.0,0.0,0.0,0.0);
					genv->getRenderer()->clear(true, true, false);
				}
				else {
					// atmosphere fills the screen
					genv->getRenderer()->clear(false, true, false);
				}
			}
			else {
				// no sky, no atmosphere: use user specified back colour
				const Color *backColor = world->getBackColor();
				genv->getRenderer()->setClearColor(backColor->getRf(),backColor->getGf(),backColor->getBf(),0.0);
				genv->getRenderer()->clear(true, true, false);
			}
		}
		else {
			genv->getRenderer()->clear(false, true, false);
		}
	}
	else {
		genv->getRenderer()->clear(true, true, false);
	}
	V__EXIT;
}

int Graphics3D::getRenderToTextureSize() const {
	int size = 512;
	// mustn't be greater than the window width/height, as we currently work by rendering to the window, then copying to a texture!
	while( size > genv->getWidth() )
		size /= 2;
	while( size > genv->getHeight() )
		size /= 2;
	return size;
}

/*Vector2D Graphics3D::getScreenSizeAtDist(float z) const {
	double H = 2.0 * tan( M_PI * fovy / 360.0 ) * z;
	//double W = H * this->width / this->height; // ?
	double W = H * genv->getWidth() / genv->getHeight(); // ?
	return Vector2D((float)W, (float)H);
}

Vector3D Graphics3D::getDirectionOfPoint(int x,int y) const {
	const int width = genv->getWidth();
	const int height = genv->getHeight();
	double sx = x - width / 2.0;
	double sy = height / 2.0 - y;
	double Z = genv->getRenderer()->getZNear();
	double H = 2.0 * tan( M_PI * fovy / 360.0 ) * Z;
	double Y = sy * H / height;
	double W = H * width / height; // ?
	double X = sx * W / width;
	Vector3D vec((float)X,(float)Y,(float)-Z);
	vec.normalise();
	return vec;
}

Vector3D Graphics3D::getDirectionOfRotatedPoint(Rotation3D *rot,int x,int y) const {
	Vector3D vec = this->getDirectionOfPoint(x,y);
	vec.rotateBy(rot);
	return vec;
}

void Graphics3D::eyeToScreen(int *x,int *y,const Vector3D *eye) const {
	const int width = genv->getWidth();
	const int height = genv->getHeight();
	double Z = genv->getRenderer()->getZNear();
	double scale = - Z / eye->z;
	double X = eye->x * scale;
	double Y = eye->y * scale;
	double H = tan( M_PI * fovy / 360.0 ) * Z;
	double W = H * width / height; // ?
	double sx = X * width / (2.0 * W);
	double sy = Y * height / (2.0 * H);
	*x = (int)sx + width / 2;
	*y = height/2 - (int)sy;

}*/

void Graphics3D::init() {
	_DEBUG_CHECK_ERROR_;

	this->failed_to_load_shaders = false;
	genv->getRenderer()->setToDefaults();
	_DEBUG_CHECK_ERROR_;
	genv->getRenderer()->clear(true, true, true);
	_DEBUG_CHECK_ERROR_;
	this->set3DMode(false);
	_DEBUG_CHECK_ERROR_;

	string shader_path = "shaders/";
	{
		FILE *file = fopen("vision_paths.config", "r");
		if( file != NULL ) {
			LOG("found vision_paths.config\n");
			const int max_line_c = 4095;
			char line[max_line_c+1] = "";
			char shaders_key_c[] = "shaders=";
			while( fgets(line, max_line_c, file) != NULL ) {
				if( line[strlen(line)-1] == '\n' ) {
					line[strlen(line)-1] = '\0'; // strip ending \n
				}
				if( line[strlen(line)-1] == '\r' ) {
					line[strlen(line)-1] = '\0'; // strip ending \r
				}
				if( strncmp(line, shaders_key_c, strlen(shaders_key_c)) == 0 ) {
					shader_path = &line[strlen(shaders_key_c)];
					LOG("    set shaders path to: %s\n", shader_path.c_str());
				}
			}
		}
	}

	if( genv->hasShaders() && genv->wantStdShaders() )
	{
		try {
			// ambient (always per vertex)
			this->std_shaders.shader_ambient = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_ambient_vs", "main_ambient_ps");

			// directional per pixel
			this->std_shaders.shader_directional = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_perpixel_vs", "main_directional_ps");

			// point per pixel
			this->std_shaders.shader_point = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_perpixel_vs", "main_point_ps");

			std_shaders.has_general = true;
		}
		catch(VisionException *) {
			// we need all of the general shaders to work together (we can't mix and match shaders, due to z-buffering issues etc)
			// also remember that some of these general shaders are required for some of the more advanced effects below
			LOG("failed to load one or more basic shaders - disabling all of them\n");
			this->failed_to_load_shaders = true;
			// no need to delete - will be done at the end when the Renderer is deleted
			std_shaders.shader_ambient = NULL;
			std_shaders.shader_directional = NULL;
			std_shaders.shader_point = NULL;
		}

		// bump shader
		if( std_shaders.has_general && this->genv->getVisionPrefs()->bumpmapping ) {
			// need general shaders for ambient
			try {
				this->std_shaders.bump_shader_directional = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_perpixel_bump_vs", "main_bump_directional_ps");
				this->std_shaders.bump_shader_point = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_perpixel_bump_vs", "main_bump_point_ps");
				std_shaders.has_bump = true;
			}
			catch(VisionException *) {
				LOG("failed to load one or more bump shaders - disabling all of them\n");
				this->failed_to_load_shaders = true;
				// no need to delete - will be done at the end when the Renderer is deleted
				std_shaders.bump_shader_directional = NULL;
				std_shaders.bump_shader_point = NULL;
			}
		}

		// hardware animation shader
		if( this->genv->getVisionPrefs()->hardwarevertexanimation ) {
			try {
				this->std_shaders.hardware_animation_shader_ambient = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_ambient_hwanimation_vs", "main_ambient_ps");
				this->std_shaders.hardware_animation_shader_directional = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_perpixel_hwanimation_vs", "main_directional_ps");
				this->std_shaders.hardware_animation_shader_point = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_perpixel_hwanimation_vs", "main_point_ps");
				std_shaders.has_hardware_animation = true;
			}
			catch(VisionException *) {
				LOG("failed to load one or more hardware animation shaders - disabling all of them\n");
				this->failed_to_load_shaders = true;
				// no need to delete - will be done at the end when the Renderer is deleted
				std_shaders.hardware_animation_shader_ambient = NULL;
				std_shaders.hardware_animation_shader_directional = NULL;
				std_shaders.hardware_animation_shader_point = NULL;
			}
		}

		// water shaders
		if( this->genv->getVisionPrefs()->watereffects ) {
			// full replacement set of shaders, so general shaders not required
			try {
				this->std_shaders.water_shader_ambient = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_refl_vs", "main_refl_ps");

				this->std_shaders.water_shader_directional = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_refl_vs", "main_refl_directional_ps");

				this->std_shaders.water_shader_point = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_refl_vs", "main_refl_point_ps");

				std_shaders.has_water = true;
			}
			catch(VisionException *) {
				LOG("failed to load one or more water shaders - disabling all of them\n");
				this->failed_to_load_shaders = true;
				// no need to delete - will be done at the end when the Renderer is deleted
				std_shaders.water_shader_ambient = NULL;
				std_shaders.water_shader_directional = NULL;
				std_shaders.water_shader_point = NULL;
			}
		}

		// splatting shaders
		if( this->genv->getVisionPrefs()->terrainsplatting ) {
			try {
				this->std_shaders.splat_shader_ambient = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_ambient_vs", "main_splat_ambient_ps");

				this->std_shaders.splat_shader_directional = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_perpixel_vs", "main_splat_directional_ps");

				this->std_shaders.splat_shader_point = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "simple.cg").c_str(), "main_perpixel_vs", "main_splat_point_ps");

				std_shaders.has_splat = true;
			}
			catch(VisionException *) {
				LOG("failed to load one or more splatting shaders - disabling all of them\n");
				this->failed_to_load_shaders = true;
				// no need to delete - will be done at the end when the Renderer is deleted
				std_shaders.splat_shader_ambient = NULL;
				std_shaders.splat_shader_directional = NULL;
				std_shaders.splat_shader_point = NULL;
			}
		}

		// hdr shader
		if( RendererInfo::framebufferObject ) {
			// don't need general shaders
			try {
				this->std_shaders.hdr_shader = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "effects.cg").c_str(), "main_hdr_vs", "main_hdr_ps");
			}
			catch(VisionException *) {
				LOG("failed to load one or more hdr shaders - disabling all of them\n");
				this->failed_to_load_shaders = true;
				// no need to delete - will be done at the end when the Renderer is deleted
				std_shaders.hdr_shader = NULL;
			}
		}

		// cloud shaders
		{
			try {
				this->std_shaders.shader_clouds = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "atmosphere.cg").c_str(), "main_clouds_vs", "main_clouds_ps");
				//this->std_shaders.shader_clouds = this->getRenderer()->createShaderEffectCGFX(VI_getApplicationDataFilename(shader_path.c_str(), "clouds.cgfx").c_str(), NULL);
				std_shaders.has_clouds = true;
			}
			catch(VisionException *) {
				LOG("failed to load one or more cloud shaders - disabling all of them\n");
				this->failed_to_load_shaders = true;
				// no need to delete - will be done at the end when the Renderer is deleted
				std_shaders.shader_clouds = NULL;
			}
		}

		// atmosphere shaders
		if( this->genv->getVisionPrefs()->atmosphere ) {
			// don't need general shaders
			try {
				this->std_shaders.shader_atmos = this->getRenderer()->createShaderEffectSimpleCG(VI_getApplicationDataFilename(shader_path.c_str(), "atmosphere.cg").c_str(), "main_atmos_vs", "main_atmos_ps");
				this->std_shaders.has_atmosphere = true;
			}
			catch(VisionException *) {
				LOG("failed to load one or more atmosphere shaders - disabling all of them\n");
				this->failed_to_load_shaders = true;
				// no need to delete - will be done at the end when the Renderer is deleted
				std_shaders.shader_atmos = NULL;
			}
		}
	}

	_DEBUG_CHECK_ERROR_;

}

/*void Graphics3D::getViewOrientation(Vector3D *right,Vector3D *up) const {
//TransformationMatrix mat;
TransformationMatrix *mat = this->genv->getRenderer()->createTransformationMatrix();
mat->save();
right->set((*mat)[0],(*mat)[4],(*mat)[8]);
up->set((*mat)[1],(*mat)[5],(*mat)[9]);
delete mat;
}

void Graphics3D::getViewDirection(Vector3D *dir) const {
TransformationMatrix *mat = this->genv->getRenderer()->createTransformationMatrix();
mat->save();
dir->set((*mat)[2],(*mat)[6],(*mat)[10]);
delete mat;
}*/

/*void Fog::draw() const {
	if(this->enabled) {
		if( this->fogtype == FOG_LINEAR ) {
			GraphicsEnvironment::getSingleton()->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_LINEAR, start, end, density);
		}
		else if( this->fogtype == FOG_EXP ) {
			GraphicsEnvironment::getSingleton()->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_EXP, start, end, density);
		}
		else if( this->fogtype == FOG_EXP2 ) {
			GraphicsEnvironment::getSingleton()->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_EXP2, start, end, density);
		}
		float fog[4] = {0.0f, 0.0f, 0.0f, 0.0f};
		this->floatArray(fog);
		GraphicsEnvironment::getSingleton()->getRenderer()->setFogColor(fog);
	}
	else {
		GraphicsEnvironment::getSingleton()->getRenderer()->setFogMode(Renderer::RENDERSTATE_FOG_NONE, 0.0f, 0.0f, 0.0f);
	}
}*/
