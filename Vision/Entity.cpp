//---------------------------------------------------------------------------
//#include <ctime>
#include <cstring>
#include <cassert>

#include "Entity.h"
#include "VisionUtils.h"
#include "GraphicsEnvironment.h"
#include "GraphicsEngine.h"
#include "Renderer.h"
#include "Object3D.h"

//---------------------------------------------------------------------------

AnimationInfo::AnimationInfo() {
	/*this->position_keys = new vector<PositionKey *>();
	this->rotation_keys = new vector<RotationKey *>();*/
	this->start = 0.0;
	this->end = 0.0;
}

AnimationInfo::AnimationInfo(AnimationInfo *from) {
	*this = *from;
#if 0
	// but we need to re-allocate the position and rotation keys!
	this->position_keys.clear();
	this->rotation_keys.clear();
	/*this->position_keys = new vector<PositionKey *>();
	this->rotation_keys = new vector<RotationKey *>();*/
	for(size_t i=0;i<from->position_keys.size();i++) {
		PositionKey *key = from->position_keys.at(i);
		PositionKey *copy = new PositionKey(key);
		this->position_keys.push_back(copy);
	}
	for(size_t i=0;i<from->rotation_keys.size();i++) {
		RotationKey *key = from->rotation_keys.at(i);
		RotationKey *copy = new RotationKey(key);
		this->rotation_keys.push_back(copy);
	}
#endif
}

AnimationInfo::~AnimationInfo() {
	/*for(size_t i=0;i<this->position_keys.size();i++) {
		PositionKey *key = this->position_keys.at(i);
		delete key;
	}*/
	//delete this->position_keys;

	/*for(size_t i=0;i<this->rotation_keys.size();i++) {
		RotationKey *key = this->rotation_keys.at(i);
		delete key;
	}*/
	//delete this->rotation_keys;
}

void AnimationInfo::setName(const char *name) {
	/*strncpy(this->name, name, NAMELEN);
	this->name[NAMELEN] = '\0';*/
	this->name = name;
}

size_t AnimationInfo::memUsage() const {
	size_t size = sizeof(this);
	/*size += position_keys.size() * sizeof(void *);
	size += rotation_keys.size() * sizeof(void *);
	for(size_t i=0;i<position_keys.size();i++) {
		PositionKey *key = this->position_keys.at(i);
		size += key->memUsage();
	}
	for(size_t i=0;i<rotation_keys.size();i++) {
		RotationKey *key = this->rotation_keys.at(i);
		size += key->memUsage();
	}*/
	for(size_t i=0;i<position_keys.size();i++) {
		const PositionKey *key = &this->position_keys.at(i);
		size += key->memUsage();
	}
	for(size_t i=0;i<rotation_keys.size();i++) {
		const RotationKey *key = &this->rotation_keys.at(i);
		size += key->memUsage();
	}
	return size;
}

bool AnimationInfo::equals(const char *name) const {
	return ( stricmp(this->name.c_str(), name) == 0 );
	//return this->name == name;
}

void AnimationInfo::addPositionKey(float time,Vector3D pos) {
	//PositionKey *key = new PositionKey(time, pos);
	PositionKey key(time, pos);
	this->position_keys.push_back(key);
}

void AnimationInfo::addRotationKey(float time,Rotation3D rot) {
	//RotationKey *key = new RotationKey(time, rot);
	RotationKey key(time, rot);
	this->rotation_keys.push_back(key);
}

Vector3D AnimationInfo::getPosition(float time) const {
	if( position_keys.size() == 1 ) {
		//PositionKey *first_key = this->position_keys.at(0);
		const PositionKey *first_key = &this->position_keys.at(0);
		return first_key->pos;
	}
	for(size_t i=1;i<position_keys.size();i++) {
		/*PositionKey *prev_key = this->position_keys.at(i-1);
		PositionKey *this_key = this->position_keys.at(i);*/
		const PositionKey *prev_key = &this->position_keys.at(i-1);
		const PositionKey *this_key = &this->position_keys.at(i);
		if( time >= prev_key->time && time <= this_key->time ) {
			float alpha = ( time - prev_key->time ) / ( this_key->time - prev_key->time );
			Vector3D pos = prev_key->pos * (1.0f - alpha) + this_key->pos * alpha;
			return pos;
		}
	}
	/*PositionKey *first_key = this->position_keys.at(0);
	PositionKey *last_key = this->position_keys.at(position_keys.size()-1);*/
	const PositionKey *first_key = &this->position_keys.at(0);
	const PositionKey *last_key = &this->position_keys.at(position_keys.size()-1);
	if( time <= first_key->time )
		return first_key->pos;
	if( time >= last_key->time )
		return last_key->pos;
	VisionException *ve = new VisionException(this, VisionException::V_POSITION_KEY_ERROR, "Can't find Position Key");
	Vision::setError(ve);
	return Vector3D(0.0, 0.0, 0.0);
}

Rotation3D AnimationInfo::getRotation(float time) const {
	if( rotation_keys.size() == 1 ) {
		//RotationKey *first_key = this->rotation_keys.at(0);
		const RotationKey *first_key = &this->rotation_keys.at(0);
		return first_key->rot;
	}
	for(size_t i=1;i<rotation_keys.size();i++) {
		/*RotationKey *prev_key = this->rotation_keys.at(i-1);
		RotationKey *this_key = this->rotation_keys.at(i);*/
		const RotationKey *prev_key = &this->rotation_keys.at(i-1);
		const RotationKey *this_key = &this->rotation_keys.at(i);
		if( time >= prev_key->time && time <= this_key->time ) {
			float alpha = ( time - prev_key->time ) / ( this_key->time - prev_key->time );
			Rotation3D rot = prev_key->rot.slerp(this_key->rot, alpha);
			return rot;
		}
	}
	/*RotationKey *first_key = this->rotation_keys.at(0);
	RotationKey *last_key = this->rotation_keys.at(rotation_keys.size()-1);*/
	const RotationKey *first_key = &this->rotation_keys.at(0);
	const RotationKey *last_key = &this->rotation_keys.at(rotation_keys.size()-1);
	if( time <= first_key->time )
		return first_key->rot;
	if( time >= last_key->time )
		return last_key->rot;
	VisionException *ve = new VisionException(this, VisionException::V_ROTATION_KEY_ERROR, "Can't find Rotation Key");
	Vision::setError(ve);
	return Rotation3D();
}


SceneGraphNode::SceneGraphNode() {
	this->obj=NULL;
	this->kill_later = false;
	this->expire_when_ps_done = false;
	this->visible = true;
	this->parent = NULL;
	this->world = NULL;
	point.moveTo(0.0f,0.0f,0.0f);
	point.rotateToIdentity();
	point.registerChangedPositionListener(this);
	this->moving = false;
	this->mass = 1.0f;
	this->halt();
	//this->superboundingradius = 0.0f;
	this->requires_update = false;
	this->spatialindex = NULL;
	//this->animationinfos = NULL;
	this->c_animationinfo = NULL;
	this->animationspeed = 1.0f; // now in frames per second
	this->animationtime = 0.0f;
	this->has_default_hanimation = false;
	this->loop_frames = true;
	this->updateFunc = NULL;
	this->updateData = NULL;
	this->sourcetype = V_SOURCETYPE_UNKNOWN;
	this->matrix = NULL; // created lazily
	this->time_existed_ms = 0;

	this->texture_x = 0.0;
	this->texture_y = 0.0;
	this->texture_x_sp = 0.0;
	this->texture_y_sp = 0.0;

	this->particles = NULL;
	this->n_particles = 0;
	this->emit_new_particles = true;

	this->vertexanim_c_frame = 0;
	this->vertexanim_state = 0;
	this->vertexanim_start_frame = 0;
	this->vertexanim_end_frame = 0;
	this->vertexanim_frame_speed = 0;
	this->vertexanim_frame_fraction = 0;
	this->setVertexAnimFrames();

	this->range = V_TOL_LARGE;

	this->shader_callback = NULL;
}

size_t SceneGraphNode::memUsage() {
	size_t size = sizeof(this);
	// TODO: spatialindex?
	size += children.size() * sizeof(void *);
	//if( animationinfos != NULL )
	{
		size += animationinfos.size() * sizeof(void *);
		for(size_t i=0;i<animationinfos.size();i++) {
			//AnimationInfo *animationinfo = (AnimationInfo *)animationinfos->get(i);
			AnimationInfo *animationinfo = this->animationinfos.at(i);
			size += animationinfo->memUsage();
		}
	}
	return size;
}

void SceneGraphNode::setObject(VI_Object *v_obj) {
	ObjectData *d_obj = dynamic_cast<ObjectData *>(v_obj); // cast to the internal representation
	this->obj = d_obj;

	//this->calculateSuperBoundingSphere();
	this->setVertexAnimFrames();
	if( this->obj != NULL ) {
		this->obj->initSceneGraphNode(this);
	}
}

void SceneGraphNode::multGLMatrix() const {
	this->point.multGLMatrix();
}

void SceneGraphNode::changedPosition() {
	SceneGraphNode *node = this;
	while( node->spatialindex == NULL && node->parent != NULL ) {
		node = node->parent;
	}
	if( node->spatialindex != NULL ) {
		node->spatialindex->updateNode(node);
	}
	/*if( this->parent != NULL ) {
		//this->parent->calculateSuperBoundingRadius();
		this->parent->calculateSuperBoundingSphere();
	}*/
}

void SceneGraphNode::calculateUpdateRequired() {
	if( this->getClass() == V_CLASS_ENTITY ) {
		this->requires_update = false;
	}
	if( this->updateFunc != NULL )
		this->requires_update = true;
	else if( this->vertexanim_frame_speed > 0.0 )
		this->requires_update = true;
	else if( this->texture_x_sp > 0.0 )
		this->requires_update = true;
	else if( this->texture_y_sp > 0.0 )
		this->requires_update = true;

	bool is_infinite = false;
	this->moving = ( !velocity.getPosition(&is_infinite).isZero() || !force.getPosition(&is_infinite).isZero() );
	if( this->getClass() == V_CLASS_SCENEGRAPHNODE ) {
		this->requires_update = false;
	}
	if( this->moving )
		this->requires_update = true;
	else if( animationspeed > 0.0 && this->c_animationinfo != NULL ) {
		this->requires_update = true;
	}
	// TODO: only update if changed
	/*if( world != NULL )
		world->checkUpdateNode(this);*/
}

/*void SceneGraphNode::calculateSuperBoundingRadius() {
	//this->superboundingradius = this->getBoundingRadius();
	this->superboundingradius = this->getBoundingSphere().radius;
	for(size_t i=0;i<this->children.size();i++) {
		SceneGraphNode *child = this->children.at(i);
		Vector3D pos = child->getPosition();
		float subrad = child->getSuperBoundingRadius();
		subrad += pos.magnitude();
		if( subrad > this->superboundingradius ) {
			this->superboundingradius = subrad;
		}
	}
}*/

Sphere SceneGraphNode::calculateSuperBoundingSphere() const {
	//this->superboundingsphere = this->getBoundingSphere();
	Sphere superboundingsphere = this->getBoundingSphere();
	for(size_t i=0;i<this->children.size();i++) {
		SceneGraphNode *child = this->children.at(i);
		Sphere child_superboundingsphere = child->calculateSuperBoundingSphere();
		Vector3D pos = child->getPosition();
		child_superboundingsphere.centre += pos;
		superboundingsphere.expand(child_superboundingsphere);
	}
	/*this->superboundingradius = this->getBoundingSphere().radius;
	for(size_t i=0;i<this->children.size();i++) {
		SceneGraphNode *child = this->children.at(i);
		Vector3D pos = child->getPosition();
		float subrad = child->getSuperBoundingRadius();
		subrad += pos.magnitude();
		if( subrad > this->superboundingradius ) {
			this->superboundingradius = subrad;
		}
	}*/
	return superboundingsphere;
}

bool SceneGraphNode::getThisExtent(float *low,float *high,const Vector3D &n,bool firstFrameOnly) const {
	*low = 0;
	*high = 0;
	if( this->obj != NULL ) {
		this->obj->getExtent(low, high, n, firstFrameOnly);
		return true;
	}
	return false;
}

bool SceneGraphNode::getExtent(float *low,float *high,const Vector3D &p,const Vector3D &n,bool firstFrameOnly,bool do_children) const {
	*low = 0;
	*high = 0;

	Vector3D trans_p = p;
	Vector3D trans_n = n;
	// we need to invert the transformation and apply to trans_n, so move then rotate
	bool is_infinite = false;
	trans_p -= this->point.getPosition(&is_infinite);
	Matrix mat;
	mat.setRotationInverse(this->point.getRotation());
	mat.transformVector(&trans_p);
	mat.transformVector(&trans_n);
	bool any = this->getThisExtent(low, high, trans_n, firstFrameOnly);
	if( any ) {
		float dot_p = trans_p % trans_n;
		*low -= dot_p;
		*high -= dot_p;
	}

	if( do_children ) {
		for(size_t i=0;i<this->children.size();i++) {
			const SceneGraphNode *child = this->children.at(i);
			float child_low = 0.0, child_high = 0.0;
			if( child->getExtent(&child_low, &child_high, trans_p, trans_n, firstFrameOnly, do_children) ) {
				if( !any ) {
					*low = child_low;
					*high = child_high;
					any = true;
				}
				else {
					if( child_low < *low )
						*low = child_low;
					if( child_high > *high )
						*high = child_high;
				}
			}
		}
	}
	return any;
}

bool SceneGraphNode::renderRequired(GraphicsEnvironment *genv) {
	/*if( this->matrix == NULL )
		this->matrix = genv->getRenderer()->createTransformationMatrix();
	this->matrix->save();
	return false;*/

	V__ENTER;
	//GLfloat mvmatrix[16];
	//glGetFloatv(GL_MODELVIEW_MATRIX,mvmatrix);
	if( this->matrix == NULL )
		this->matrix = genv->getRenderer()->createTransformationMatrix();
	this->matrix->save();
	//return false;
	//return true;
	if( this->getObject() == NULL ) {
		V__EXIT;
		return false;
	}
	if( !this->visible ) {
		V__EXIT;
		return false;
	}
	/*if( this->getObject()->getBoundingRadius() < 0 )
	return true; // hacky way of saying always visible*/
	//return true;
	/*if( strcmp(this->getName(), "Terrain Chunk Node") == 0 ) {
		VI_log("terrain: %d\n", this);
	}*/
	Sphere boundingSphere = this->getObject()->getBoundingSphere();
	float radius = boundingSphere.radius;
	Vector3D centre_epos;
	if( abs(boundingSphere.centre.x) > 0 || abs(boundingSphere.centre.y) > 0 || abs(boundingSphere.centre.z) > 0 ) {
		//TransformationMatrix *temp_matrix = g->getRenderer()->createTransformationMatrix();
		/*TransformationMatrix *temp_matrix = g->getRenderer()->getStaticTransformationMatrix();
		g->getRenderer()->pushMatrix();
		g->getRenderer()->translate(boundingSphere.centre.x, boundingSphere.centre.y, boundingSphere.centre.z);
		temp_matrix->save();
		g->getRenderer()->popMatrix();
		centre_epos = temp_matrix->getPos();*/
		centre_epos = this->matrix->transformVec(boundingSphere.centre);
		//delete temp_matrix;
	}
	else {
		//centre_epos.set(mvmatrix[12],mvmatrix[13],mvmatrix[14]);
		centre_epos = matrix->getPos();
	}
	if( centre_epos.z < 0 ) {
		Vector2D screenSize = genv->getScreenSizeAtDist(-centre_epos.z);
		float pixel_w = genv->getWidth() * radius / screenSize.x;
		float pixel_h = genv->getHeight() * radius / screenSize.y;
		float pixel_size = pixel_w < pixel_h ? pixel_w : pixel_h;
		//LOG("pixel_size %f\n", pixel_size);
		if( pixel_size < 0.5f ) {
			//LOG("too small! %f X %f\n", pixel_w, pixel_h);
			V__EXIT;
			return false;
		}
	}
	float near_dist = (-centre_epos.z) - radius;
	if(
		( this->range <= V_TOL_LINEAR || near_dist < this->range )
		//&& near_dist < g->getZFar()
		&& genv->getFrustum()->SphereInFrustum(centre_epos.x,centre_epos.y,centre_epos.z,radius)!=0
		) {
			/*
			AABB boundingAABB = this->getObject()->getBoundingAABB();
			//if( false )
			if( boundingAABB.available ) {
			// TODO: check bounding AABB
			GLfloat temp_mvmatrix[16];
			Vector3D pts[8];
			for(int i=0;i<8;i++) {
			glPushMatrix();
			glTranslatef(boundingAABB.corners[i].x, boundingAABB.corners[i].y, boundingAABB.corners[i].z);
			glGetFloatv(GL_MODELVIEW_MATRIX,temp_mvmatrix);
			glPopMatrix();
			pts[i].x = temp_mvmatrix[12];
			pts[i].y = temp_mvmatrix[13];
			pts[i].z = temp_mvmatrix[14];
			}
			if( !g->BoxInFrustum(pts) ) {
			LOG("clipped by box test : %s\n", this->getObject()->getName());
			Vector3D pos = this->getPosition();
			for(int i=0;i<8;i++) {
			LOG("%d : %f , %f , %f\n", i, boundingAABB.corners[i].x, boundingAABB.corners[i].y, boundingAABB.corners[i].z);
			LOG("    -> %f , %f , %f\n", pos.x + boundingAABB.corners[i].x, pos.y + boundingAABB.corners[i].y, pos.z + boundingAABB.corners[i].z);
			LOG("--> %d : %f , %f , %f\n", i, pts[i].x, pts[i].y, pts[i].z);
			}
			return false;
			}
			}*/

			// check vertices
			/*if( false )
			{
			ObjectData *obj = this->getObject();
			if( obj->getNRenderData() > 0 ) {
			bool out = true;
			for(int i=0;i<obj->getNRenderData() && out;i++) {
			RenderData *rd = obj->getRenderData(i);
			for(int j=0;j<rd->getNVertices() && out;j++) {
			// TODO: assuming frame 0!
			GLfloat temp_mvmatrix[16];
			Vector3D vx = rd->getVertex(0, j);
			glPushMatrix();
			glTranslatef(vx.x, vx.y, vx.z);
			glGetFloatv(GL_MODELVIEW_MATRIX,temp_mvmatrix);
			glPopMatrix();
			if( g->PointInFrustum(temp_mvmatrix[12],temp_mvmatrix[13],temp_mvmatrix[14]) ) {
			out = false;
			}
			}
			}
			if( out ) {
			//LOG("clipped by vx test!!!\n");
			return false;
			}
			}
			}*/
			V__EXIT;
			return true;
	}

	V__EXIT;
	return false;
}

bool SceneGraphNode::hasActiveParticleSystem() const {
	if( emit_new_particles || n_particles > 0 )
		return true;

	for(size_t i=0;i<this->children.size();i++) {
		const SceneGraphNode *child = this->children.at(i);
		if( child->hasActiveParticleSystem() )
			return true;
	}
	return false;
}

void SceneGraphNode::setWorld(World *world) {
	this->world = world;
	if( world != NULL ) {
		//world->checkUpdateNode(this);
		/*if( this->getShadowPlane() != NULL ) {
			world->addShadowNode(this);
		}*/
	}
	for(size_t i=0;i<this->children.size();i++) {
		SceneGraphNode *child = this->children.at(i);
		child->setWorld(world);
	}
}

VI_SceneGraphNode *SceneGraphNode::copy() const {
	V__ENTER
	/*SceneGraphNode *copy = NULL;
	if( this->getClass() == V_CLASS_ENTITY ) {
		copy = new Entity();
		const Entity *c_this = static_cast<const Entity *>(this);
		Entity *c_copy = static_cast<Entity *>(copy);
		int s_tag = copy->tag;
		TransformationMatrix *s_matrix = copy->matrix;
		*c_copy = *c_this;
		copy->tag = s_tag;
		copy->matrix = s_matrix;
		c_copy->setVertexAnimFrames();
		if( c_copy->getObject() != NULL ) {
			ObjectData *d_obj = dynamic_cast<ObjectData *>(c_copy->getObject()); // cast to the internal representation
			d_obj->initEntity(c_copy);
		}
	}
	else {
		copy = new SceneGraphNode();
		int s_tag = copy->tag;
		TransformationMatrix *s_matrix = copy->matrix;
		*copy = *this;
		copy->tag = s_tag;
		copy->matrix = s_matrix;
	}

	copy->parent = NULL;

	copy->c_animationinfo = NULL;
	//if( this->animationinfos != NULL )
	{
		//copy->animationinfos = new vector<AnimationInfo *>();
		copy->animationinfos.clear();
		for(size_t i=0;i<this->animationinfos.size();i++) {
			AnimationInfo *animationinfo = this->animationinfos.at(i);
			AnimationInfo *animationinfo_copy = new AnimationInfo(animationinfo);
			copy->animationinfos.push_back(animationinfo_copy);
			if( this->c_animationinfo == animationinfo ) {
				copy->c_animationinfo = animationinfo_copy;
			}
		}
	}

	//copy->children = new vector<SceneGraphNode *>();
	copy->children.clear();
	for(size_t i=0;i<this->children.size();i++) {
		const SceneGraphNode *child = this->children.at(i);
		//SceneGraphNode *child_copy = child->copy();
		//SceneGraphNode *child_copy = static_cast<SceneGraphNode *>(child->copy());
		SceneGraphNode *child_copy = dynamic_cast<SceneGraphNode *>(child->copy());
		//copy->children->add(child_copy);
		copy->children.push_back(child_copy);
		child_copy->parent = copy;
	}*/

	SceneGraphNode *copy = new SceneGraphNode();
	int s_tag = copy->tag;
	TransformationMatrix *s_matrix = copy->matrix;
	*copy = *this;
	copy->tag = s_tag;
	copy->matrix = s_matrix;
	copy->setVertexAnimFrames();
	if( copy->getObject() != NULL ) {
		ObjectData *d_obj = dynamic_cast<ObjectData *>(copy->getObject()); // cast to the internal representation
		d_obj->initSceneGraphNode(copy);
	}

	copy->parent = NULL;

	copy->c_animationinfo = NULL;
	//if( this->animationinfos != NULL )
	{
		//copy->animationinfos = new vector<AnimationInfo *>();
		copy->animationinfos.clear();
		for(size_t i=0;i<this->animationinfos.size();i++) {
			AnimationInfo *animationinfo = this->animationinfos.at(i);
			AnimationInfo *animationinfo_copy = new AnimationInfo(animationinfo);
			copy->animationinfos.push_back(animationinfo_copy);
			if( this->c_animationinfo == animationinfo ) {
				copy->c_animationinfo = animationinfo_copy;
			}
		}
	}

	copy->children.clear();
	for(size_t i=0;i<this->children.size();i++) {
		const SceneGraphNode *child = this->children.at(i);
		//SceneGraphNode *child_copy = child->copy();
		//SceneGraphNode *child_copy = static_cast<SceneGraphNode *>(child->copy());
		SceneGraphNode *child_copy = dynamic_cast<SceneGraphNode *>(child->copy());
		//copy->children->add(child_copy);
		copy->children.push_back(child_copy);
		child_copy->parent = copy;
	}

	V__EXIT
	return copy;
}

SceneGraphNode::~SceneGraphNode() {
	if( particles != NULL ) {
		delete [] particles;
	}
	delete this->matrix;
	//if( this->animationinfos != NULL )
	{
		for(size_t i=0;i<this->animationinfos.size();i++) {
			AnimationInfo *animationinfo = this->animationinfos.at(i);
			delete animationinfo;
		}
		//delete this->animationinfos;
	}

	this->detachFromParentNode(); // detach from any parent
	for(size_t i=0;i<children.size();i++) {
		children.at(i)->parent = NULL; // needed so we don't remove from this node's children vector when deleting the child
		delete children.at(i);
	}
	//delete this->children;
}

/*void SceneGraphNode::setName(const char *name) {
	this->name = name;
}*/

bool SceneGraphNode::equals(const char *name) const {
	return ( stricmp(this->name.c_str(), name) == 0 );
	//return this->name == name;
}

VI_SceneGraphNode *SceneGraphNode::findChildNode(const char *name) {
	V__ENTER;
	if( this->equals(name) ) {
		V__EXIT;
		return this;
	}

	VI_SceneGraphNode *node = NULL;
	for(size_t i=0;i<children.size() && node==NULL;i++) {
		SceneGraphNode *child = this->children.at(i);
		node = child->findChildNode(name);
	}
	V__EXIT;
	return node;
}

void SceneGraphNode::inheritHAnimationFrom(VI_SceneGraphNode *from) {
	this->inheritHAnimationFrom(from, true);
}

void SceneGraphNode::inheritHAnimationFrom(VI_SceneGraphNode *from,bool top) {
	SceneGraphNode *d_from = dynamic_cast<SceneGraphNode *>(from); // cast to the internal representation
	VI_SceneGraphNode *node = this->findChildNode(d_from->getName());
	if( node == NULL && top ) {
		node = this;
	}
	SceneGraphNode *d_node = dynamic_cast<SceneGraphNode *>(node); // cast to the internal representation
	//if( node != NULL && d_from->animationinfos != NULL ) {
	if( node != NULL ) {
		/*if( d_node->animationinfos == NULL ) {
			d_node->animationinfos = new vector<AnimationInfo *>();
		}*/
		for(size_t i=0;i<d_from->animationinfos.size();i++) {
			AnimationInfo *animationinfo = d_from->animationinfos.at(i);
			bool found = false;
			for(size_t j=0;j<d_node->animationinfos.size() && !found;j++) {
				AnimationInfo *this_animationinfo = d_node->animationinfos.at(j);
				if( this_animationinfo->equals( animationinfo->getName() ) ) {
					found = true;
				}
			}
			if( !found ) {
				AnimationInfo *animationinfo_copy = new AnimationInfo(animationinfo);
				d_node->animationinfos.push_back(animationinfo_copy);
				// shift positions to be relative to this node
				//const vector<PositionKey *> *position_keys = &animationinfo_copy->position_keys;
				vector<PositionKey> *position_keys = &animationinfo_copy->position_keys;
				for(size_t j=0;j<position_keys->size();j++) {
					//PositionKey *key = static_cast<PositionKey *>(position_keys->get(j));
					//PositionKey *key = position_keys->at(j);
					PositionKey *key = &position_keys->at(j);
					Vector3D shift = node->getPosition() - d_from->getPosition();
					key->pos += shift;
				}
			}
		}
	}

	for(size_t i=0;i<d_from->children.size();i++) {
		SceneGraphNode *child = d_from->children.at(i);
		this->inheritHAnimationFrom(child, false);
	}

}

void SceneGraphNode::addChildNode(VI_SceneGraphNode *node) {
	//SceneGraphNode *d_node = static_cast<SceneGraphNode *>(node); // cast to the internal representation
	SceneGraphNode *d_node = dynamic_cast<SceneGraphNode *>(node); // cast to the internal representation
	if( d_node->parent != NULL ) {
		d_node->detachFromParentNode();
	}
	children.push_back(d_node);
	d_node->parent = this;
	d_node->setWorld(this->world);
	/*
	if( obj->getShadowPlane() != NULL ) {
	world->addShadowNode(obj);
	}
	world->updateNode(obj);*/
	//this->calculateSuperBoundingRadius();
	//this->calculateSuperBoundingSphere();
}

VI_SceneGraphNode *SceneGraphNode::getChildNode(size_t i) {
	return children.at(i);
}

const VI_SceneGraphNode *SceneGraphNode::getChildNode(size_t i) const {
	return children.at(i);
}

size_t SceneGraphNode::getNChildNodes() const {
	return children.size();
}

void SceneGraphNode::detachFromParentNode() {
	if( this->parent != NULL ) {
		this->parent->removeChildNode(this);
	}
}

bool SceneGraphNode::removeChildNode(VI_SceneGraphNode *node) {
	SceneGraphNode *d_node = dynamic_cast<SceneGraphNode *>(node); // cast to the internal representation
	for(size_t i=0;i<children.size();i++) {
		if( children.at(i) == d_node ) {
			children.erase(children.begin() + i);
			ASSERT( d_node->parent == this );
			d_node->parent = NULL;
			if( d_node->spatialindex != NULL ) {
				d_node->spatialindex->removeNode(d_node);
			}
			//this->calculateSuperBoundingSphere();
			return true;
		}

	}
	ASSERT( d_node->parent != this );
	return false;
}

void SceneGraphNode::removeAllChildNodes() {
	while( this->children.size() > 0 ) {
		SceneGraphNode *child = this->children.at(0);
		this->removeChildNode(child);
	}
}

bool SceneGraphNode::collision(const VI_SceneGraphNode *node) const {
	//return false;
	//return true;
	const SceneGraphNode *d_node = dynamic_cast<const SceneGraphNode *>(node); // cast to the internal representation
	if(this == d_node)
		return false;
	bool is_infinite = false;
	Vector3D v = this->point.getPosition(&is_infinite);
	if( is_infinite )
		return true;
	Vector3D v2 = d_node->point.getPosition(&is_infinite);
	if( is_infinite )
		return true;

	/*Vector3D w_pos(this->mvmatrix[12],this->mvmatrix[13],this->mvmatrix[14]);
	Vector3D w_pos2(d_node->mvmatrix[12],d_node->mvmatrix[13],d_node->mvmatrix[14]);*/
	/*ASSERT( this->matrix != NULL );
	ASSERT( d_node->matrix != NULL );
	Vector3D e_pos = this->matrix->getPos();
	Vector3D e_pos2 = d_node->matrix->getPos();*/
	Vector3D e_pos = this->getLastRenderedEyePos();
	Vector3D e_pos2 = d_node->getLastRenderedEyePos();

	e_pos.subtract(e_pos2);
	float dist_sq = e_pos.square();
	//float b = this->getBoundingRadius() + d_node->getBoundingRadius();
	float b = this->getBoundingSphere().radius + d_node->getBoundingSphere().radius;
	if(dist_sq <= b*b + V_TOL_LINEAR)
		return true;
	else
		return false;
}

bool SceneGraphNode::collisionLine(float *intersection, const Vector3D &pos, const Vector3D &dir, float line_thickness) const {
	// dir must be unit vector!
	if( intersection != NULL )
		*intersection = 0.0f;
	if( obj != NULL && ( obj->getClass() == V_CLASS_VOBJECT || obj->getClass() == V_CLASS_MESH ) ) {
		bool is_infinite = false;
		Vector3D v = this->point.getPosition(&is_infinite);
		ASSERT( !is_infinite );

		Sphere sphere = this->getBoundingSphere();
		Vector3D w_pos = this->localToWorldPos(sphere.centre);

		//float r_sq = sphere.radiusSquared;
		float radius = ( sphere.radius + line_thickness );
		float r_sq = radius * radius;
		Vector3D c = w_pos - pos;
		float dir_dot_c = dir % c;
		float val = dir_dot_c * dir_dot_c - c.square() + r_sq;
		//LOG("%f, %f, %f : %f, %f, %f : %f\n", c.x, c.y, c.z, dir->x, dir->y, dir->z, val);
		if( val >= - V_TOL_LINEAR ) {
			// intersects - try bounding box
			Box aabb;
			if( !obj->getBoundingAABB(&aabb) ) {
			//if( true ) {
				// no box info, assume intersect
				if( intersection != NULL )
					*intersection = dir_dot_c;
				return true;
			}
			if( line_thickness > 0.0f ) {
				aabb.expand(line_thickness);
			}
			// transform to world space
			for(int i=0;i<8;i++) {
				aabb.setCorner(i, this->localToWorldPos(aabb.getCorner(i)));
			}

			if( aabb.collisionLine(intersection, pos, dir) ) {
				return true;
			}
		}
	}

	if( intersection == NULL ) {
		// can optimise
		for(size_t i=0;i<this->children.size();i++) {
			const SceneGraphNode *child = this->children.at(i);
			if( child->collisionLine(NULL, pos, dir, line_thickness) ) {
				return true;
			}
		}
	}
	else {
		// need to find closest
		bool any_intersected = false;
		*intersection = 0.0f;
		for(size_t i=0;i<this->children.size();i++) {
			const SceneGraphNode *child = this->children.at(i);
			float child_intersection = 0.0f;
			if( child->collisionLine(&child_intersection, pos, dir, line_thickness) ) {
				if( !any_intersected || child_intersection < *intersection ) {
					*intersection = child_intersection;
				}
				any_intersected = true;
			}
		}
		if( any_intersected ) {
			return true;
		}
	}
	return false;
}

bool SceneGraphNode::collisionLineSeg(float *intersection, const Vector3D &p0, const Vector3D &p1, float line_thickness) const {
	float intersection_local = 0.0f;
	if( intersection == NULL )
		intersection = &intersection_local;
	Vector3D dir = p1 - p0;
	float len = dir.magnitude();
	if( len < V_TOL_LINEAR ) {
		dir.set(1.0f, 0.0f, 0.0f); // set to arbitrary vector
	}
	else {
		dir /= len;
	}
	bool res = this->collisionLine(intersection, p0, dir, line_thickness);
	if( *intersection < 0.0f || *intersection > len ) {
		res = false;
		*intersection = 0.0f;
	}
	return res;
}

void SceneGraphNode::setHAnimationTime(float animationtime, bool do_children) {
	this->animationtime = animationtime;

	if( do_children ) {
		for(size_t i=0;i<children.size();i++) {
			children.at(i)->setHAnimationTime(animationtime, true);
		}
	}
}

void SceneGraphNode::adoptHAnimation(bool do_children) {
	if( c_animationinfo != NULL ) {
		//LOG("%s : %s\n", this->getName(), c_animationinfo->getName());
		if( c_animationinfo->position_keys.size() > 0 ) {
			Vector3D pos = c_animationinfo->getPosition(animationtime);
			this->point.moveTo(pos);
			//this->changedPosition(); // now done in Point3D class
		}
		if( c_animationinfo->rotation_keys.size() > 0 ) {
			Rotation3D rot = c_animationinfo->getRotation(animationtime);
			this->point.rotateTo(rot);
		}
	}
	else if( this->has_default_hanimation ) {
		// revert to default point
		/*this->point.moveTo(0.0f, 0.0f, 0.0f);
		this->point.rotateToIdentity();*/
		this->point = this->hanimation_def_point;
		//this->changedPosition(); // now done in Point3D class
	}

	if( do_children ) {
		for(size_t i=0;i<children.size();i++) {
			children.at(i)->adoptHAnimation(true);
		}
	}
}

void SceneGraphNode::setDefaultHAnimationPosition(bool do_children) {
	this->has_default_hanimation = true;
	this->hanimation_def_point = this->point;

	if( do_children ) {
		for(size_t i=0;i<children.size();i++) {
			children.at(i)->setDefaultHAnimationPosition(true);
		}
	}
}

void SceneGraphNode::update(int time_ms, bool do_children) {
	V__ENTER;

	if( this->vertexanim_frame_speed != 0 && this->vertexanim_end_frame != this->vertexanim_start_frame ) { // n.b., vertexanim_end_frame may be -1, indicating the last frame!
		float frames = (time_ms * this->vertexanim_frame_speed) / 1000.0f;
		int frames_inc = (int)frames;
		frames -= frames_inc;
		this->vertexanim_frame_fraction += frames;
		this->vertexanim_c_frame += frames_inc;
		while(this->vertexanim_frame_fraction >= 1.0) {
			this->vertexanim_c_frame++;
			this->vertexanim_frame_fraction -= 1.0;
		}
		int eframe = this->vertexanim_end_frame;
		if( eframe == -1 ) {
			eframe = this->obj->getNFrames() - 1;
		}
		while(this->vertexanim_c_frame > eframe) {
			if(this->loop_frames)
				this->vertexanim_c_frame -= (eframe - this->vertexanim_start_frame + 1);
			else
				this->vertexanim_c_frame = eframe;
		}
	}

	if( particles != NULL ) {
		for(int i=0;i<n_particles;i++) {
			/*particles[i].velocity = particles[i].velocity + particles[i].acceleration * (float)time_ms / 1000.0f;
			particles[i].pos = particles[i].pos + particles[i].velocity * (float)time_ms / 1000.0f;*/

			// Midpoint method
			// v( t(n+1) ) = v( t(n) ) + a * t
			// p( t(n+1) ) = p( t(n) ) + v( t(n+0.5) ) * t
			//             = p( t(n) ) + v( t(n) ) * t + 0.5 * t * t * a
			Vector3D a = (particles[i].acceleration * (float)time_ms)/1000.0f;
			Vector3D a2 = (a * (0.5f * time_ms))/1000.0f;
			Vector3D v = (particles[i].velocity * (float)time_ms)/1000.0f;
			particles[i].pos = particles[i].pos + v + a2;
			particles[i].velocity = particles[i].velocity + a;
		}
		(static_cast<ParticleSystem *>(this->obj))->updateParticles(this, time_ms);
	}

	this->time_existed_ms += time_ms;

	this->texture_x += ( this->texture_x_sp * time_ms ) / 1000.0f;
	this->texture_y += ( this->texture_y_sp * time_ms ) / 1000.0f;

	if(updateFunc!=NULL) {
		updateFunc(this, updateData);
	}

	// H animation
	//if( this->animationinfos != NULL ) {
	if( this->animationinfos.size() > 0 ) {
		adoptHAnimation(false); // call even if c_animationinfo == NULL, to revert to default position
	}
	if( c_animationinfo != NULL ) {
		this->animationtime += (this->animationspeed * time_ms) / 1000.0f;
		//LOG("# %d : %f , %f\n", this, this->animationtime, c_animationinfo->end);
		if( this->animationtime >= c_animationinfo->end - 0.001f ) {
			//printf("%d %f , %f\n",this->loop_frames,this->animationtime, c_animationinfo->end);
		}

		if( this->loop_frames ) {
			while( this->animationtime >= c_animationinfo->end ) {
				float length = c_animationinfo->end - c_animationinfo->start;
				this->animationtime -= length;
				//LOG("%s time - %f to %f\n", this->getName(), length, animationtime);
			}
		}
		else if( this->animationtime > c_animationinfo->end ) {
			this->animationtime = c_animationinfo->end;
		}
	}

	if( moving )
	{
		this->old_point = this->point;

		bool is_infinite = false;
		Vector3D acceleration = this->force.getPosition(&is_infinite) / this->mass;
		// Euler
		// v( t(n+1) ) = v( t(n) ) + a * t
		// p( t(n+1) ) = p( t(n) ) + v( t(n) ) * t
		/*Vector3D v(&velocity.getPosition(&is_infinite));
		v.scale(time);
		point.move(&v);
		Vector3D a(&acceleration);
		a.scale(time);
		velocity.move(&a);*/

		// Midpoint method
		// v( t(n+1) ) = v( t(n) ) + a * t
		// p( t(n+1) ) = p( t(n) ) + v( t(n+0.5) ) * t
		//             = p( t(n) ) + v( t(n) ) * t + 0.5 * t * t * a
		Vector3D a = (acceleration * (float)time_ms)/1000.0f;
		Vector3D a2 = (a * (0.5f * time_ms))/1000.0f;
		Vector3D v = (velocity.getPosition(&is_infinite) * (float)time_ms)/1000.0f;
		point.move(&v);
		point.move(&a2);
		velocity.move(&a);
		//VI_log("%d : %d : %f : %f\n", this->getTag(), time, point.getPosition(&is_infinite).y, velocity.getPosition(&is_infinite).y);

		// Runge-Kutta
		// v( t(n+1) ) = v( t(n) ) + a * t
		// k1 = v( t(n) )
		// k2 = v( t(n+0.5) ) = v( t(n) ) + 0.5 * t * a
		// k4 = v( t(n+1) ) = v( t(n) ) + t * a
		// slope = (k1 + 4*k2 + k4)/6
		//       = v( t(n) ) + 0.5 * t * a
		// p( t(n+1) ) = p( t(n) ) + t * slope
		// so equivalent to Midpoint method, since velocity is independent of position
		/*Vector3D a = (acceleration * (float)time_ms)/1000.0f;
		Vector3D k1 = velocity.getPosition(&is_infinite);
		Vector3D k2 = */
	}
	// TODO: rotational velocity

	if( this->expire_when_ps_done ) {
		if( !this->hasActiveParticleSystem() ) {
			this->kill();
		}
	}

	if( do_children ) {
		// update children
		for(size_t i=0;i<children.size();i++) {
			children.at(i)->update(time_ms, true);
		}
	}
	V__EXIT;
}

bool SceneGraphNode::doKills() {
	V__ENTER;
	for(size_t i=0;i<children.size();) {
		if( !children.at(i)->doKills() ) {
			// we didn't kill the child, so okay to increment index
			i++;
		}
	}
	if(this->kill_later) {
		/*if(parent != NULL) {
			//parent->removeChildNode(this);
			this->detachFromParentNode();
		}*/
		delete this;
		V__EXIT;
		return true;
	}
	V__EXIT;
	return false;
}

Vector3D SceneGraphNode::getCurrentWorldPos() const {
	// should be equivalent to localToWorldPos() called with zero vector, but we can optimise out the first rotation
	V__ENTER;
	bool is_infinite = false;
	Vector3D pos = this->point.getPosition(&is_infinite);
	SceneGraphNode *parent = dynamic_cast<SceneGraphNode *>(this->getParentNode()); // cast to internal representation
	while(parent != NULL) {
		Matrix mat;
		mat.setRotation(parent->point.getRotation());
		mat.transformVector(&pos);

		if( !is_infinite ) {
			// if it's an infinite object, only care about rotations
			pos.add(parent->point.getPosition(&is_infinite));
		}

		//parent = parent->getParent();
		parent = dynamic_cast<SceneGraphNode *>(parent->getParentNode()); // cast to internal representation
	}
	V__EXIT;
	return pos;
}

Sphere SceneGraphNode::calculateWorldSuperBoundingSphere() const {
	//Sphere world_sphere = this->superboundingsphere;
	Sphere world_sphere = this->calculateSuperBoundingSphere();
	world_sphere.centre = this->localToWorldPos(world_sphere.centre);
	return world_sphere;
}

void SceneGraphNode::localToWorldMatrix(Matrix4d *out_matrix) const {
	bool is_infinite = false;
	out_matrix->setZero();
	out_matrix->setRotation(this->point.getRotation());
	out_matrix->setTranslation(this->point.getPosition(&is_infinite));

	SceneGraphNode *parent = dynamic_cast<SceneGraphNode *>(this->getParentNode()); // cast to internal representation
	while(parent != NULL) {
		Matrix4d mat;
		mat.setRotation(parent->point.getRotation());
		Vector3D pos = parent->point.getPosition(&is_infinite);
		if( !is_infinite ) {
			// if it's an infinite object, only care about rotations
			mat.setTranslation(pos);
		}
		// out_matrix = mat * out_matrix
		out_matrix->premultiply(mat);

		parent = dynamic_cast<SceneGraphNode *>(parent->getParentNode()); // cast to internal representation
	}
}

Vector3D SceneGraphNode::localToWorldPos(const Vector3D &pos) const {
	V__ENTER;
	bool is_infinite = false;
	Vector3D tpos = pos;
	Matrix mat;
	mat.setRotation(this->point.getRotation());
	mat.transformVector(&tpos);
	tpos.add(this->point.getPosition(&is_infinite));

	SceneGraphNode *parent = dynamic_cast<SceneGraphNode *>(this->getParentNode()); // cast to internal representation
	while(parent != NULL) {
		mat.setRotation(parent->point.getRotation());
		mat.transformVector(&tpos);

		if( !is_infinite ) {
			// if it's an infinite object, only care about rotations
			tpos.add(parent->point.getPosition(&is_infinite));
		}

		//parent = parent->getParent();
		parent = dynamic_cast<SceneGraphNode *>(parent->getParentNode()); // cast to internal representation
	}
	V__EXIT;
	return tpos;
}

/*Vector3D SceneGraphNode::worldToLocalPos(Vector3D pos) const {
}*/

Vector3D SceneGraphNode::worldToLocalDir(const Vector3D &dir) const {
	// note, although this is worldToLocal, since we are concerned only with rotations, we can still traverse
	// up the hierarchy, rather than having to traverse downwards.
	Vector3D tdir = dir;
	bool is_infinite = false;
	Matrix mat;
	mat.setRotationInverse(this->point.getRotation());
	mat.transformVector(&tdir);

	SceneGraphNode *parent = dynamic_cast<SceneGraphNode *>(this->getParentNode()); // cast to internal representation
	while(parent != NULL) {
		mat.setRotationInverse(parent->point.getRotation());
		mat.transformVector(&tdir);
		parent = dynamic_cast<SceneGraphNode *>(parent->getParentNode()); // cast to internal representation
	}
	return tdir;
}

Vector3D SceneGraphNode::getLastRenderedEyePos() const {
	//Vector3D pos( mvmatrix[12], mvmatrix[13], mvmatrix[14] );
	ASSERT( this->matrix != NULL );
	Vector3D pos = this->matrix->getPos();
	//LOG("%f , %f, %f\n", pos.x, pos.y, pos.z);
	return pos;
}

Vector3D SceneGraphNode::localToEyePos(const Vector3D &pos) const {
	V__ENTER;
	ASSERT( this->matrix != NULL );
	//TransformationMatrix *new_matrix = g3->getRenderer()->createTransformationMatrix();
	/*TransformationMatrix *new_matrix = g3->getRenderer()->getStaticTransformationMatrix();
	g3->getRenderer()->pushMatrix();
	this->matrix->load();
	g3->getRenderer()->translate(pos.x, pos.y, pos.z);
	new_matrix->save();
	g3->getRenderer()->popMatrix();
	Vector3D new_pos = new_matrix->getPos();*/
	//delete new_matrix;
	Vector3D new_pos = this->matrix->transformVec(pos);
	V__EXIT;
	return new_pos;
}

void SceneGraphNode::setAllToIdentity(bool do_this) {
	if( do_this ) {
		this->point.rotateToIdentity();
	}
	for(size_t i=0;i<children.size();i++) {
		children.at(i)->point.rotateToIdentity();
	}
}


void SceneGraphNode::halt() {
	// o3p
	this->velocity.moveTo(0.0,0.0,0.0);
	this->velocity.rotateToIdentity();
}

//void SceneGraphNode::getShadows(Graphics3D *g3d,Entity **shadows,int *n_shadows,int max_shadows,Entity *skip) {
void SceneGraphNode::getShadows(RenderQueue *queue, GraphicsEnvironment *genv, SceneGraphNode *skip) {
	//V__ENTER;

	ObjectData *d_obj = dynamic_cast<ObjectData *>(this->getObject()); // cast to the internal representation
	ShadowPlane *shadow = d_obj == NULL ? NULL : d_obj->getShadowPlane();
	/*if( *n_shadows == max_shadows ) {
	return;
	}*/

	if( this != skip && shadow != NULL ) {
		genv->getRenderer()->pushMatrix();
		bool is_infinite = false;
		Vector3D pos = this->point.getPosition(&is_infinite);
		genv->getRenderer()->translate(pos.x,pos.y,pos.z);
		genv->getRenderer()->transform(this->point.getRotation().getQuaternion());
		if( this->renderRequired(genv) ) {
			// only add if visible
			queue->renderQueue_shadowplanes->insert(this);
		}
		genv->getRenderer()->popMatrix();
	}

	for(size_t i=0;i<children.size();i++) {
		SceneGraphNode *child = this->children.at(i);
		//child->getShadows(g3d,shadows,n_shadows,max_shadows,skip);
		child->getShadows(queue, genv, skip);
	}
	//V__EXIT;
}

bool SceneGraphNode::setHAnimation(const char *animname, bool forcereset) {
	// returns true if this node or any of its descendants have this animation
	//LOG(">> %d\n", this);
	V__ENTER;
	bool ok = false;
	//if( animationinfos != NULL ) {
	if( this->animationinfos.size() > 0 ) {
		for(size_t i=0;i<animationinfos.size() && !ok && animname != NULL;i++) {
			//AnimationInfo *animationinfo = static_cast<AnimationInfo *>(animationinfos->get(i));
			AnimationInfo *animationinfo = this->animationinfos.at(i);
			if( animationinfo->equals(animname) ) {
				ok = true;
				if( this->c_animationinfo != animationinfo || forcereset ) {
					this->c_animationinfo = animationinfo;
					this->animationtime = c_animationinfo->start;
					//printf(">>> %f\n", animationtime);
					//LOG("### SET %d , %s\n", this->getTag(), c_animationinfo->getName());
				}
			}
		}
		if( !ok ) {
			// set to no animation
			this->c_animationinfo = NULL;
		}
		this->calculateUpdateRequired();
	}

	for(size_t i=0;i<children.size();i++) {
		SceneGraphNode *child = this->children.at(i);
		if( child->setHAnimation(animname, forcereset) ) {
			ok = true;
		}
	}

	V__EXIT;
	return ok;
}

void SceneGraphNode::setHAnimationSpeed(float animationspeed) {
	this->animationspeed = animationspeed;
	this->calculateUpdateRequired();
	for(size_t i=0;i<children.size();i++) {
		SceneGraphNode *child = this->children.at(i);
		child->setHAnimationSpeed(animationspeed);
		child->calculateUpdateRequired();
	}
}

void SceneGraphNode::addHAnimationInfo(VI_HAnimationInfo *animationinfo) {
	AnimationInfo *d_animationinfo = static_cast<AnimationInfo *>(animationinfo); // cast to the internal representation
	/*if( this->animationinfos == NULL ) {
		this->initAnimationInfos();
	}*/
	this->animationinfos.push_back(d_animationinfo);
}

bool SceneGraphNode::isAnimationFinished() const {
	if( this->getCAnimationInfo() != NULL ) {
		if( this->getHAnimationTime() >= this->getCAnimationInfo()->end - 0.001f ) {
			return true;
		}
		return false;
	}
	else {
		if( this->getCVertexAnimFrame() == this->getVertexAnimEndFrame() ) {
			return true;
		}
	}
	return false;

	/*if( this->getCAnimationInfo() != NULL ) {
		if( this->getHAnimationTime() >= this->getCAnimationInfo()->end - 0.001f ) {
			return true;
		}
	}
	return false;*/
}

void SceneGraphNode::setVertexFrameSpeed(float frame_speed, bool do_children) {
	this->vertexanim_frame_speed = frame_speed;
	calculateUpdateRequired();

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->setVertexFrameSpeed(frame_speed, true);
		}
	}
}

void SceneGraphNode::setVertexAnimState(int state, bool do_children) {
	this->vertexanim_state = state;
	this->setVertexAnimFrames();

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->setVertexAnimState(state, true);
		}
	}
}

void SceneGraphNode::setVertexAnimFrames() {
	if(obj != NULL)
		obj->getFrames((V_ANIMSTATE_t)this->vertexanim_state,&this->vertexanim_start_frame,&this->vertexanim_end_frame);
	if(this->vertexanim_c_frame < this->vertexanim_start_frame ||
		( this->vertexanim_end_frame != -1 && this->vertexanim_c_frame > this->vertexanim_end_frame ) ) {
			this->vertexanim_c_frame = this->vertexanim_start_frame;
			this->vertexanim_frame_fraction = 0.0;
	}
}

void SceneGraphNode::setVertexAnimFrames(int start_frame, int end_frame, bool do_children) {
	this->vertexanim_start_frame = start_frame;
	this->vertexanim_end_frame = end_frame;
	if(this->vertexanim_c_frame < this->vertexanim_start_frame ||
		( this->vertexanim_end_frame != -1 && this->vertexanim_c_frame > this->vertexanim_end_frame ) ) {
			this->vertexanim_c_frame = this->vertexanim_start_frame;
			this->vertexanim_frame_fraction = 0.0;
	}

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->setVertexAnimFrames(start_frame, end_frame, true);
		}
	}
}

void SceneGraphNode::setAnimLoop(bool loop, bool do_children) {
	this->loop_frames = loop;

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->setAnimLoop(loop, true);
		}
	}
}

void SceneGraphNode::setSpeculari(unsigned char r, unsigned char g, unsigned char b, bool do_children) {
	if( this->obj != NULL ) {
		obj->setSpeculari(r, g, b);
	}

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->setSpeculari(r, g, b, true);
		}
	}
}

void SceneGraphNode::setShadowCaster(bool enabled,bool do_children) {
	if( this->obj != NULL ) {
		this->obj->setShadowCaster(enabled);
	}

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->setShadowCaster(enabled, true);
		}
	}
}

void SceneGraphNode::enableParticlesystem(bool enabled,bool do_children) {
	this->emit_new_particles = enabled;

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->enableParticlesystem(enabled, true);
		}
	}
}

int SceneGraphNode::emitParticle(int n) {
	if( particles == NULL ) {
		VisionException *ve = new VisionException(this, VisionException::V_GENERAL_ERROR, "Particles array is NULL");
		Vision::setError(ve);
	}
	if( !emit_new_particles ) {
		return 0;
	}
	ParticleSystem *ps = static_cast<ParticleSystem *>(this->obj);
	while( n > 0 && n_particles < ps->getMaxParticles() ) {
		ps->initialiseParticle(particles, n_particles++);
		n--;
	}
	//LOG("emitted particles, total %d\n", n_particles);
	return n;
}

void SceneGraphNode::scale(float sx,float sy,float sz, bool do_children) {
	if( this->getObject() != NULL ) {
		this->getObject()->scale(sx, sy, sz);
	}
	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->scale(sx, sy, sz, true);
		}
	}
	//this->calculateSuperBoundingSphere();
}

void SceneGraphNode::forceColori(unsigned char r, unsigned char g, unsigned char b, unsigned char alpha, bool do_children) {
	Color color;
	color.seti(r,g,b);
	this->material.force(alpha, color);

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->forceColori(r, g, b, alpha, true);
		}
	}
}

void SceneGraphNode::unforce(bool do_children) {
	this->material.unforce();

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->unforce(true);
		}
	}
}

void SceneGraphNode::setBlendtype(V_BLENDTYPE_t blendtype, bool do_children) {
	this->material.setBlendtype(blendtype);

	if( do_children ) {
		for(size_t i=0;i<this->getNChildNodes();i++) {
			VI_SceneGraphNode *child = this->getChildNode(i);
			child->setBlendtype(blendtype, true);
		}
	}
}

void SceneGraphNode::kill() {
	this->kill_later = true;
	Vision::registerKill();
}


Particle::Particle() {
	reset();
}

void Particle::reset() {
	this->birthTime_ms = Vision::getGameTimeMS();
	//this->energy = 0;
	this->size = 0;
	//this->delta_size = 0;
	//this->weight = 0;
	//this->delta_weight = 0;
	this->alpha = 255;
}


/*void Entity::init() {
}*/

/*Entity::Entity() {
	//init();
}

//Entity::Entity(ObjectData *obj) {
Entity::Entity(VI_Object *obj) {
	//init();
	//this->obj = obj;
	this->obj = dynamic_cast<ObjectData *>(obj); // cast to the internal representation
	this->calculateSuperBoundingRadius();
	// o3p
	this->setVertexAnimFrames();
	if( this->obj != NULL ) {
		this->obj->initSceneGraphNode(this);
	}
}*/

/*Entity::Entity(Entity *from) : SceneGraphNode(from) {
printf("!\n");
}*/

/*Entity::~Entity() {
}*/

/*float SceneGraphNode::getBoundingRadius() const {
	return obj==NULL ? 0.0f : obj->getBoundingSphere().radius;
}

float SceneGraphNode::getBoundingRadiusSq() const {
	return obj==NULL ? 0.0f : obj->getBoundingSphere().radiusSquared;
}*/

Sphere SceneGraphNode::getBoundingSphere() const {
	return obj==NULL ? Sphere() : obj->getBoundingSphere();
}

ShadowPlane *SceneGraphNode::getShadowPlane() {
	return obj != NULL ? obj->getShadowPlane() : NULL;
};

void SceneGraphNode::render(GraphicsEnvironment *genv, ObjectData::RenderPhase phase, bool lighting_pass, bool use_shadowmesh, RenderData *renderData) /*const*/ {
	V__ENTER;
	_DEBUG_CHECK_ERROR_
	/*if( strcmp(this->getName(), "Terrain Chunk") == 0 ) {
		VI_log("terrain: %d\n", this);
	}*/
	if(this->visible && this->obj != NULL) {
		bool texture_matrix = false;
		if( this->texture_x != 0.0 || this->texture_y != 0.0 ) {
			texture_matrix = true;
			genv->getRenderer()->switchToTexture();
			genv->getRenderer()->loadIdentity();
			genv->getRenderer()->translate(this->texture_x, this->texture_y, 0.0);
			genv->getRenderer()->switchToModelview();
		}

		const ShaderEffect *c_shader = genv->getRenderer()->getActiveShader();
		if( c_shader != NULL ) {
			//c_shader->setMatrices();
			//c_shader->setMatrices("mx.ModelViewProj", "mx.ModelViewIT", "mx.ModelView");

			if( material.isColorForced() ) {
				if( phase == ObjectData::RENDER_NONOPAQUE && material.getAlpha() < 255 ) {
					// still need test on 'phase', as although we shouldn't ever receive these objects in the opaque renderqueue,
					// they will be called in RENDER_STENCIL mode
					// TODO: avoid conflict with RenderData::blending flag being true?
					genv->getGraphics3D()->setBlend(material.getBlendtype(), lighting_pass);
				}
			}
			/*
			float material_color[4] = {1.0f, 1.0f, 1.0f, 1.0f};
			if( material.isColorForced() ) {
				material_color[0] = material.getColor()->getRf();
				material_color[1] = material.getColor()->getGf();
				material_color[2] = material.getColor()->getBf();
				material_color[3] = ((float)material.getAlpha())/(float)255.0f;
				if( phase == ObjectData::RENDER_NONOPAQUE && material.getAlpha() < 255 ) {
					// still need test on 'phase', as although we shouldn't ever receive these objects in the opaque renderqueue,
					// they will be called in RENDER_STENCIL mode
					// TODO: avoid conflict with RenderData::blending flag being true?
					genv->getGraphics3D()->setBlend(material.getBlendtype(), lighting_pass);
				}
			}
			genv->getGraphics3D()->setShaderMatColor(material_color);
			*/
			genv->getGraphics3D()->setShaderConstants(this);
		}
		else {
			genv->getGraphics3D()->setFixedFunctionConstants(this);
		}

		/*if( c_shader != NULL && c_shader->getCallback() != NULL ) {
			(*c_shader->getCallback())(c_shader, genv);
		}*/
		if( c_shader != NULL && shader_callback != NULL ) {
			(*shader_callback)(genv, c_shader, this);
		}

		int eframe = vertexanim_end_frame;
		if( eframe == -1 )
			eframe = this->obj->getNFrames() - 1;
		int n_frame = vertexanim_c_frame == eframe ? vertexanim_start_frame : vertexanim_c_frame+1;
		//GraphicsEnvironment::checkGLError("Entity::render about to render");
		_DEBUG_CHECK_ERROR_

		this->obj->render(genv->getRenderer(), genv, phase, vertexanim_c_frame, n_frame, vertexanim_frame_fraction, this, lighting_pass, use_shadowmesh, renderData);
		
		_DEBUG_CHECK_ERROR_
		//GraphicsEnvironment::checkGLError("Entity::render done");
		if( texture_matrix ) {
			/*glMatrixMode(GL_TEXTURE);
			glLoadIdentity();
			glMatrixMode(GL_MODELVIEW);*/
			genv->getRenderer()->switchToTexture();
			genv->getRenderer()->loadIdentity();
			genv->getRenderer()->switchToModelview();
		}
	}
	//}
	V__EXIT;
	//return true;
}

bool SceneGraphNode::hasLight() const {
	if( this->getObject() == NULL )
		return false;
	//return this->getObject()->hasLight();
	ObjectData *d_obj = dynamic_cast<ObjectData *>(this->getObject()); // cast to the internal representation
	return d_obj->hasLight();
}

void SceneGraphNode::renderQueue(RenderQueue *queue, bool outofview) /*const*/ {
	if( this->getObject() == NULL )
		return;
	if( !this->visible )
		return;
	ObjectData *d_obj = dynamic_cast<ObjectData *>(this->getObject()); // cast to the internal representation
	d_obj->renderQueue(queue, this, outofview);
}

void SceneGraphNode::castShadow(Renderer *renderer, GraphicsEnvironment *genv, const Vector3D &lightpos, bool infinite) const {
	if( this->getObject() == NULL )
		return;
	if( !this->visible )
		return;
	V__ENTER;
	_DEBUG_CHECK_ERROR_;
	const ShaderEffect *c_shader = renderer->getActiveShader();
	/*if( c_shader != NULL ) {
		c_shader->setMatrices("mx.ModelViewProj", "mx.ModelViewIT", "mx.ModelView");
		// currently we don't ever call this function with shaders - if we do, it would be more consistent to set the lightpos in Graphics3D (using setShaders(), as done for the normal rendering pass)
		//genv->getGraphics3D()->setShaderLightPos(lightpos->x, lightpos->y, lightpos->z, -lightpos->x, -lightpos->y, -lightpos->z);
	}*/
	if( c_shader != NULL ) {
		genv->getGraphics3D()->setShaderConstants(this);
	}
	else {
		genv->getGraphics3D()->setFixedFunctionConstants(this);
	}

	if( c_shader != NULL && shader_callback != NULL ) {
		(*shader_callback)(genv, c_shader, this);
	}

	_DEBUG_CHECK_ERROR_;

	int eframe = vertexanim_end_frame;
	if( eframe == -1 )
		eframe = this->obj->getNFrames() - 1;
	int n_frame = vertexanim_c_frame == eframe ? vertexanim_start_frame : vertexanim_c_frame+1;
	//this->getObject()->castShadow(g, c_frame, n_frame, frame_fraction, lightpos, infinite, g->vertex_shader_shadow_point != NULL);
	ObjectData *d_obj = dynamic_cast<ObjectData *>(this->getObject()); // cast to the internal representation
	d_obj->castShadow(renderer, genv, vertexanim_c_frame, n_frame, vertexanim_frame_fraction, lightpos, infinite, false);
	V__EXIT;
}

/*bool Entity::collision(Entity *obj) {
if((void *)this == (void *)obj)
return false;
bool is_infinite = false;
Vector3D v = this->point.getPosition(&is_infinite);
if( is_infinite )
return false;
Vector3D v2 = obj->point.getPosition(&is_infinite);
if( is_infinite )
return false;
v.subtract(&v2);
float dist_sq = v.square();
float b = this->getBoundingRadius() + obj->getBoundingRadius();
if(dist_sq <= b*b)
return true;
else
return false;
}

bool Entity::collisionPoint(Vector3D *point) {
//Vector3D v = this->point.pos;
bool is_infinite = false;
Vector3D v = this->point.getPosition(&is_infinite);
if( is_infinite )
return false;
v.subtract(point);
float dist_sq = v.square();
if(dist_sq <= this->getBoundingRadiusSq())
return true;
else
return false;
}

bool Entity::collisionLine(Vector3D *intersection,Vector3D *point,Vector3D *n) {
//Vector3D *pos = &this->point.pos;
bool is_infinite = false;
Vector3D pos = this->point.getPosition(&is_infinite);
if( is_infinite )
return false;
float dist_sq = pos.distFromLineSq(point,n);
if( dist_sq < this->getBoundingRadiusSq() ) {
*intersection = pos;
intersection->dropOnLine(point,n);
return true;
}
else
return false;
}*/

/*float Entity::collisionPolygon(Vector3D *plane_pt,Vector3D *poly_pt,Vector3D *popup_pt,bool *popup_set,Vector3D *points,int n_points,Vector3D *normal,float radius,bool shift,bool popup,Vector3D *popdir) {
*popup_set = false;

Vector3D intersection;
float maj_radius;
if(!this->collisionPlane(&intersection,&maj_radius,&points[0],normal,radius,false))
return -1.0;

bool in_poly = intersection.pointInPolygon(points,n_points);
Vector3D *closest_pt;
Vector3D pt;
if(in_poly)
closest_pt = &intersection;
else {
pt = intersection.closestPointOnPolygon(points,n_points);
closest_pt = &pt;
}

*plane_pt = intersection;
*poly_pt = *closest_pt;

const float radius_sq = radius * radius;
// closest_pt is now the point on or in the polygon that is closest to intersection
bool old_is_infinite = false;
bool is_infinite = false;
//Vector3D line = this->old_point.pos - this->point.pos;
Vector3D line = this->old_point.getPosition(&old_is_infinite) - this->point.getPosition(&is_infinite);
if( old_is_infinite || is_infinite ) {
return -1.0;
}
float l_sq = line.square();
if(l_sq < V_TOL_LINEAR) {
//Vector3D Q = this->old_point.pos - closest_pt;
Vector3D Q = this->old_point.getPosition(&old_is_infinite) - closest_pt;
float c_sq = Q.square();
if(c_sq < radius_sq + V_TOL_LINEAR)
return 0.0;
return -1.0;
}
line.normalise();
//Vector3D c = this->old_point.pos - closest_pt;
//float dist_sq = c.square();
//c.parallelComp(&line);
//dist_sq -= c.square();
//bool collision = false;
//if(dist_sq < radius_sq + TOL) {
//	collision = true;
//	if(shift) {
//		// lazy!
//		this->collisionPlane(NULL,NULL,&points[0],normal,radius,true);
//        }
//}
//return collision;
//Vector3D Q = this->old_point.pos - closest_pt;
Vector3D Q = this->old_point.getPosition(&old_is_infinite) - closest_pt;
float c_sq = Q.square();
//Q /= sqrt(c_sq);
float v = Q.dot(&line);
float d = radius_sq - (c_sq - v*v);
if(d < -V_TOL_LINEAR)
return -1.0;
if(d < V_TOL_LINEAR)
return v;
float dist = v - sqrt(d);
//if(dist < - TOL)
//	return -1.0;
if(dist * dist > l_sq)
return -1.0;
return dist;
}

bool Entity::collisionPlane(Vector3D *intersection,float *proj_radius,Vector3D *p_on_plane,Vector3D *normal,float radius,bool shift) {
float plane_d = p_on_plane->dot(normal);
bool is_infinite = false;
bool old_is_infinite = false;
float this_d = this->point.getPosition(&is_infinite).dot(normal);
float old_this_d = this->old_point.getPosition(&old_is_infinite).dot(normal);
if( is_infinite || old_is_infinite )
return false;
float distance = this_d - plane_d;
float old_distance = old_this_d - plane_d;
if(old_distance < 0.0) {
return false;
//distance = -distance;
//old_distance = -old_distance;
}
if(old_distance < distance - V_TOL_LINEAR) {
// moving away from polygon
return false;
}
if(old_distance < radius - V_TOL_LINEAR) {
// already embedded case
Vector3D temp = (*normal) * ( old_distance + V_TOL_LINEAR );
//*intersection = this->old_point.pos - temp;
*intersection = this->old_point.getPosition(&old_is_infinite) - temp;
return true;
}

if(distance < radius - V_TOL_LINEAR) {
if(intersection != NULL) {
// proj_radius must be non NULL too
//*intersection = this->point.pos;
*intersection = this->point.getPosition(&is_infinite);
*proj_radius = radius;
//if(old_distance > distance) {
//	if(distance < TOL) {
//                float f = old_distance / ( old_distance - distance );
//                intersection->subtract(&this->old_point.pos);
//	        intersection->scale(f);
//		intersection->add(&this->old_point.pos);
//        }
//        else {
//	        Vector3D n = *normal;
//                if(old_this_d < plane_d)
//	                n.scale(distance - radius);
//                else
//	        	n.scale(radius - distance);
//                intersection->add(&n);
//        }
//}
//float f = old_distance / ( old_distance - distance );
float f = ( old_distance - radius ) / ( old_distance - distance );
//intersection->subtract(&this->old_point.pos);
intersection->subtract(&this->old_point.getPosition(&old_is_infinite));
intersection->scale(f);
//intersection->add(&this->old_point.pos);
intersection->add(&this->old_point.getPosition(&old_is_infinite));
//float ox = intersection->dot(normal) - plane_d;
*intersection -= ( (*normal) * radius );
float tx = intersection->dot(normal) - plane_d;
//if(tx > 0.0001)
//	int i=0;
//Vector3D obj_intersection_pt = this->old_point.pos - (*normal) * radius;
//Vector3D line = this->point.pos - this->old_point.pos;
//line.normalise();
// intersect the line with the plane

}
if(shift) {
//Vector3D *p = &this->point.pos;
Vector3D *p = &this->point.getPosition(&is_infinite);
Vector3D n = *normal;
if(old_this_d < plane_d)
n.scale(distance - radius);
else
n.scale(radius - distance);
p->add(&n);
}
return true;
}
return false;

}*/

/*Entity *Entity::getCollisionObject(Entity *obj) {
if(collision(obj))
return this;

for(int i=0;i<getNChildren();i++) {
SceneGraphNode *child = this->getChildAt(i);
if( child->getClass() == V_CLASS_ENTITY ) {
Entity *child_e = static_cast<Entity *>(child);
Entity *c = child_e->getCollisionObject(obj);
if( c != NULL )
return c;
}
}
return NULL;
}*/

void SceneGraphNode::collidePlane(const Vector3D &normal,float mew,float cor) {
	// o3p
	bool is_infinite = false;
	//Vector3D R(&this->force.pos);
	Vector3D R(this->force.getPosition(&is_infinite));
	float dot = R.dot(normal);
	if(dot < 0.0)
	{
		R.parallelComp(normal); // (minus)reaction of plane
		//this->force.pos.subtract(&R); // resultant force only in plane
		this->force.move(-R.x,-R.y,-R.z);
	}

	//Vector3D bounce = this->velocity.pos;
	Vector3D bounce = this->velocity.getPosition(&is_infinite);
	bounce.parallelComp(normal);
	//this->velocity.pos.subtract(&bounce);
	this->velocity.move(-bounce.x,-bounce.y,-bounce.z);
	bounce.scale(-cor);

	//Vector3D F(&this->velocity.pos);
	//F.normalise();
	//Vector3D F = this->velocity.pos.unit();
	Vector3D F = this->velocity.getPosition(&is_infinite).unit();
	F.scale(mew * R.magnitude());
	//if(F.square() >= this->velocity.pos.square())
	if(F.square() >= this->velocity.getPosition(&is_infinite).square()) {
		//this->velocity.pos.set(0.0,0.0,0.0);
		this->velocity.moveTo(0.0,0.0,0.0);
	}
	else {
		//this->velocity.pos.subtract(&F);
		this->velocity.move(-F.x,-F.y,-F.z);
	}
	const float MIN = 0.01f;
	if(bounce.square() > MIN * MIN) {
		//this->velocity.pos.add(&bounce);
		this->velocity.move(&bounce);
	}
}

/*Object3D *Entity::convertAllToObject() {
	if( this->obj->getClass() != V_CLASS_VOBJECT )
		return NULL;

	Object3D *this_obj = static_cast<Object3D *>(this->obj);
	Object3D *n_obj = this_obj->clone();

	for(int i=0;i<getNChildren();i++) {
		SceneGraphNode *node = this->getChildAt(i);
		if( node->getClass() != V_CLASS_ENTITY ) {
			return NULL;
		}
		Entity *child = static_cast<Entity *>(node);
		Object3D *child_obj = child->convertAllToObject();
		if( child_obj == NULL ) {
			delete n_obj;
			return NULL;
		}
		//printf("B >>> %d %d\n",n_obj,n_obj->poly_normals_frame[0]);
		//child_obj->rotate(&child->point.rot, false);
		child_obj->rotate(&child->point.getRotation(), false);
		//printf("C >>> %d %d\n",n_obj,n_obj->poly_normals_frame[0]);
		bool is_infinite = false;
		Vector3D pos = child->point.getPosition(&is_infinite);
		//child_obj->translate(child->point.pos.x, child->point.pos.y, child->point.pos.z, false);
		child_obj->translate(pos.x, pos.y, pos.z, false);
		//printf("D >>> %d %d\n",n_obj,n_obj->poly_normals_frame[0]);
		n_obj->combineObject(child_obj, false);
	}
	if( !Vision::isLocked() ) {
		n_obj->prepare();
	}
	return n_obj;
}*/

void SceneGraphNode::getForceColori(V_BLENDTYPE_t *blendtype,unsigned char *r,unsigned char *g,unsigned char *b,unsigned char *alpha) const {
	*blendtype = this->material.getBlendtype();
	const Color *col = this->material.getColor();
	*r = col->getR();
	*g = col->getG();
	*b = col->getB();
	*alpha = this->material.getAlpha();
}

/*VI_Entity *Entity::copyEntity() const {
	VI_SceneGraphNode *copy_node = this->copy();
	VI_Entity *cp = dynamic_cast<VI_Entity *>(copy_node);
	return cp;
}*/

struct SortChildrenData {
	SceneGraphNode *node;
	float distance;
};

int sortChildren(const void *a, const void *b) {
	SortChildrenData *node1 = (SortChildrenData *)a;
	SortChildrenData *node2 = (SortChildrenData *)b;
	if( node1->distance > node2->distance )
		return 1;
	else if( node1->distance < node2->distance )
		return -1;
	return 0;
}

/*void SpatialIndex::renderVector(Vector *vector,Graphics3D *g,Graphics3D::RenderPhase phase,bool lighting_pass) {
#ifdef FALSE
const int STORE_SIZE = 4096;
SortChildrenData store[STORE_SIZE];
if( vector->size() > 0 && vector->size() <= STORE_SIZE ) {
for(int i=0;i<vector->size();i++) {
SceneGraphNode *node = static_cast<SceneGraphNode *>(vector->get(i));
GLfloat mvmatrix[16];
glPushMatrix();
node->multGLMatrix();
glGetFloatv(GL_MODELVIEW_MATRIX,mvmatrix);
glPopMatrix();
float near_dist = (-mvmatrix[14]) - node->getBoundingRadius();
store[i].node = node;
store[i].distance = near_dist;
}
qsort(store, vector->size(), sizeof(store[0]), sortChildren);
for(int i=0;i<vector->size();i++) {
SceneGraphNode *node = store[i].node;
//LOG("%d : %f\n", i, store[i].distance);
g->doObject(node, phase);
}
return;
}
#endif

for(int i=0;i<vector->size();i++) {
SceneGraphNode *node = static_cast<SceneGraphNode *>(vector->get(i));
g->doObject(node, phase,lighting_pass);
}
}*/

//void SpatialIndex::buildRenderQueueVector(Vector *vector,Graphics3D *g) {
void SpatialIndex::buildRenderQueueVector(vector<SceneGraphNode *> *vector, GraphicsEnvironment *genv) {
	for(size_t i=0;i<vector->size();i++) {
		//SceneGraphNode *node = static_cast<SceneGraphNode *>(vector->get(i));
		SceneGraphNode *node = vector->at(i);
		genv->getGraphics3D()->buildRenderQueue(node);
	}
}

void SpatialIndex::updateNode(SceneGraphNode *node) {
	removeNode(node);
	addNode(node);
}

SpatialGrid2D::SpatialGrid2D(float x, float y, float z, float width, float height, float depth, int n_x, int n_z) {
	this->x = x;
	this->y = y;
	this->z = z;
	this->width = width;
	this->height = height;
	this->depth = depth;

	float tw = width / n_x;
	float td = depth / n_z;
	/*this->radius = tw > td ? tw : td;
	this->radius = radius > height ? radius : height;
	//radius *= 0.70710678119;
	radius *= 0.86602540378443864676372317075294;*/
	this->radius = (float)(0.5 * sqrt( tw*tw + td*td + height*height ));

	this->n_x = n_x;
	this->n_z = n_z;

	this->node_other = new vector<SceneGraphNode *>();
	this->nodes = new vector<SceneGraphNode *> *[n_x * n_z];
	this->visible = new bool[n_x * n_z];
	for(int i=0;i<n_x*n_z;i++) {
		this->nodes[i] = new vector<SceneGraphNode *>();
		this->visible[i] = false;
	}
}

SpatialGrid2D::~SpatialGrid2D() {
	delete this->node_other;
	/*for(int i=0;i<n_x*n_z;i++) {
	delete this->nodes[i];
	}*/
	delete [] this->nodes;
}

void SpatialGrid2D::addNode(SceneGraphNode *node) {
	node->setSpatialIndex(this);

	if( node->hasLight() ) {
		//this->node_other->add(node);
		this->node_other->push_back(node);
		return;
	}

	Vector3D pos = node->getPosition();
	float tx = pos.x - this->x;
	float ty = pos.y - this->y;
	float tz = pos.z - this->z;
	//float br = node->getSuperBoundingRadius();
	float br = node->calculateSuperBoundingSphere().radius;
	if( tx - br < 0 || ty - br < 0 || tz - br < 0 || tx + br >= width || ty + br >= height || tz + br >= depth ) {
		//if( tx < 0 || ty < 0 || tz < 0 || tx >= width || ty >= height || tz >= depth ) {
		//this->node_other->add(node);
		this->node_other->push_back(node);
	}
	else {
		float tw = width / n_x;
		float td = depth / n_z;
		int i_x = (int)(tx / tw);
		int i_z = (int)(tz / td);
		float frac_x = tx - tw*i_x;
		float frac_z = tz - td*i_z;
		//float br = node->getSuperBoundingRadius();
		float br = node->calculateSuperBoundingSphere().radius;
		if( frac_x - br < 0 || frac_z - br < 0 || frac_x + br >= tw || frac_z + br >= td ) {
			//this->node_other->add(node);
			this->node_other->push_back(node);
		}
		else
		{
			int index = i_z * n_x + i_x;
			//this->nodes[index]->add(node);
			this->nodes[index]->push_back(node);
		}
	}
}

void SpatialGrid2D::removeNode(SceneGraphNode *node) {
	// TODO: improve performance
	//bool done = this->node_other->remove(node);
	bool done = remove_vec(this->node_other, node);

	for(int i=0;i<n_x*n_z && !done;i++) {
		//done = this->nodes[i]->remove(node);
		done = remove_vec(this->nodes[i], node);
	}
	if( done ) {
		node->setSpatialIndex(NULL);

	}

}

/*void SpatialGrid2D::render(Graphics3D *g,Graphics3D::RenderPhase phase,bool lighting_pass) {
float tw = width / n_x;
float td = depth / n_z;

glDisable(GL_CULL_FACE);
glDisable(GL_LIGHTING);
glDisable(GL_TEXTURE_2D);
glColor3ub(255, 0, 0);
//glDisable(GL_DEPTH_TEST);
//glGenOcclusionQueriesNV(n_x * n_z, occlusion_queries);

for(int cz=0,index=0;cz<n_z;cz++) {
for(int cx=0;cx<n_x;cx++, index++) {
this->visible[index] = false;
GLfloat mvmatrix[16];
glPushMatrix();
//glTranslatef((x+0.5)*tw,0.5*height,(z+0.5)*td);
glTranslatef(x+(cx+0.5)*tw,y+0.5*height,z+(cz+0.5)*td);
glGetFloatv(GL_MODELVIEW_MATRIX,mvmatrix);
glPopMatrix();

if( g->SphereInFrustum(mvmatrix[12],mvmatrix[13],mvmatrix[14],radius)!=0 )
{

{
this->visible[index] = true;
}
}
}
}

glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
glEnable(GL_CULL_FACE);
glEnable(GL_LIGHTING);
glEnable(GL_COLOR);
glEnable(GL_COLOR_MATERIAL);
glEnable(GL_TEXTURE_2D);
glEnable(GL_DEPTH_TEST);

for(int cz=0,index=0;cz<n_z;cz++) {
for(int cx=0;cx<n_x;cx++, index++) {
if( this->visible[index] ) {
{
this->renderVector(this->nodes[index], g, phase, lighting_pass);
}
}
}
}
this->renderVector(this->node_other, g, phase, lighting_pass);
}*/

void SpatialGrid2D::buildRenderQueue(GraphicsEnvironment *genv) {
	float tw = width / n_x;
	float td = depth / n_z;

	for(int cz=0,index=0;cz<n_z;cz++) {
		for(int cx=0;cx<n_x;cx++, index++) {
			this->visible[index] = false;
			//TransformationMatrix *mvmatrix = g->getRenderer()->createTransformationMatrix();
			TransformationMatrix *mvmatrix = genv->getRenderer()->getStaticTransformationMatrix();
			genv->getRenderer()->pushMatrix();
			genv->getRenderer()->translate(x+(cx+0.5f)*tw,y+0.5f*height,z+(cz+0.5f)*td);
			mvmatrix->save();
			genv->getRenderer()->popMatrix();

			if( genv->getFrustum()->SphereInFrustum((*mvmatrix)[12],(*mvmatrix)[13],(*mvmatrix)[14],radius)!=0 )
			{
				/*
				Vector3D pts[8];
				pts[0].set(x+cx*tw,y,z+cz*td);
				pts[1].set(x+(cx+1)*tw,y,z+cz*td);
				pts[2].set(x+cx*tw,y,z+(cz+1)*td);
				pts[3].set(x+(cx+1)*tw,y,z+(cz+1)*td);
				pts[4].set(x+cx*tw,y+height,z+cz*td);
				pts[5].set(x+(cx+1)*tw,y+height,z+cz*td);
				pts[6].set(x+cx*tw,y+height,z+(cz+1)*td);
				pts[7].set(x+(cx+1)*tw,y+height,z+(cz+1)*td);
				for(int i=0;i<8;i++) {
				glPushMatrix();
				glTranslatef(pts[i].x,pts[i].y,pts[i].z);
				glGetFloatv(GL_MODELVIEW_MATRIX,mvmatrix);
				glPopMatrix();
				pts[i].x = mvmatrix[12];
				pts[i].y = mvmatrix[13];
				pts[i].z = mvmatrix[14];
				}
				if( g->BoxInFrustum(pts) )*/

				{
					this->visible[index] = true;
				}
				/*else {
				printf("");
				}*/
			}
			/*else {
			printf("");
			}*/
			//delete mvmatrix;
		}
	}

	for(int cz=0,index=0;cz<n_z;cz++) {
		for(int cx=0;cx<n_x;cx++, index++) {
			if( this->visible[index] ) {
				this->buildRenderQueueVector(this->nodes[index], genv);
			}
		}
	}
	this->buildRenderQueueVector(this->node_other, genv);
}

size_t SpatialGrid2D::getNodeUnoptCount() const {
	return node_other->size();
}


Quadtreenode::Quadtreenode(Quadtree *quadtree,int level,bool right,bool bottom,Quadtreenode *parent) {
	this->quadtree = quadtree;
	this->parent = parent;
	this->level = level;
	this->index_x = right ? 1 : 0;
	if( parent != NULL )
		this->index_x += parent->index_x * 2;
	this->index_z = bottom ? 1 : 0;
	if( parent != NULL )
		this->index_z += parent->index_z * 2;
	this->nodes = new vector<SceneGraphNode *>();
	for(int i=0;i<4;i++)
		this->children[i] = NULL;
	this->x = 0;
	this->y = 0;
	this->z = 0;
	this->width = 0;
	this->height = 0;
	this->depth = 0;
	this->dont_recurse_lower = false;
}

bool Quadtreenode::addNode(SceneGraphNode *node) {
	Vector3D pos = node->getPosition();
	float tx = pos.x - this->x;
	float ty = pos.y - this->y;
	float tz = pos.z - this->z;
	//float br = node->getSuperBoundingRadius();
	float br = node->calculateSuperBoundingSphere().radius;
	if( tx - br < 0 || ty - br < 0 || tz - br < 0 || tx + br >= width || ty + br >= height || tz + br >= depth ) {
		return false;
	}
	else {
		// try children?
		bool done = false;
		for(int i=0;i<4 && !done;i++) {
			if( children[i] != NULL ) {
				done = children[i]->addNode(node);
			}
		}
		if( !done ) {
			//this->nodes->add(node);
			this->nodes->push_back(node);
		}
		return true;
	}
}

bool Quadtreenode::removeNode(SceneGraphNode *node) {
	//bool done = this->nodes->remove(node);
	bool done = remove_vec(this->nodes, node);
	// try children?
	for(int i=0;i<4 && !done;i++) {
		if( children[i] != NULL ) {
			done = children[i]->removeNode(node);
		}
	}
	return done;
}

Quadtree::Quadtree(float x, float y, float z, float width, float height, float depth, int levels) {
	this->x = x;
	this->y = y;
	this->z = z;
	this->width = width;
	this->height = height;
	this->depth = depth;

	this->levels = levels;

	this->node_other = new vector<SceneGraphNode *>();
	this->quadtreenodes = new Quadtreenode(this, 0, false, false, NULL);
	this->quadtreenodes->set(x, y, z, width, height, depth);
	this->quadtreenodes->createChildren(levels);
}

Quadtree::~Quadtree() {
	delete this->node_other;
	/*int n = 1;
	for(int i=0;i<levels;i++) {
	this->nodes[i] = new Vector *[n*n];
	for(int j=0;j<n*n;j++) {
	delete (this->nodes[i])[j];
	}
	delete [] this->nodes[i];
	}
	delete [] this->nodes;*/
	delete this->quadtreenodes;
}

/*bool Quadtree::addNodeLevel(SceneGraphNode *node,int level,int n) {
Vector3D pos = node->getPosition();
float tx = pos.x - this->x;
float ty = pos.y;
float tz = pos.z - this->z;
{
float tw = width / n;
float td = depth / n;
int i_x = tx / tw;
int i_z = tz / td;
float frac_x = tx - tw*i_x;
float frac_z = tz - td*i_z;
float br = node->getSuperBoundingRadius();
if( frac_x - br < 0 || frac_z - br < 0 || frac_x + br >= tw || frac_z + br >= td ) {
return false; // try at a higher level
}
else
{
int index = i_z * n + i_x;
(this->nodes[level])[index]->add(node);
}
}
return true;
}*/

void Quadtree::addNode(SceneGraphNode *node) {
	node->setSpatialIndex(this);

	if( node->hasLight() ) {
		//this->node_other->add(node);
		this->node_other->push_back(node);
		return;
	}

	Vector3D pos = node->getPosition();
	float tx = pos.x - this->x;
	float ty = pos.y - this->y;
	float tz = pos.z - this->z;
	//float br = node->getSuperBoundingRadius();
	float br = node->calculateSuperBoundingSphere().radius;
	if( tx - br < 0 || ty - br < 0 || tz - br < 0 || tx + br >= width || ty + br >= height || tz + br >= depth ) {
		//this->node_other->add(node);
		this->node_other->push_back(node);
	}
	else {
		/*int n=1;
		for(int i=0;i<levels-1;i++) {
		n *= 2;
		}
		bool done = false;
		for(int i=levels-1;i>=0 && !done;i--) {
		done = this->addNodeLevel(node, i, n);
		n /= 2;
		}*/
		bool done = this->quadtreenodes->addNode(node);
		if( !done ) {
			Vision::setError(new VisionException(this, VisionException::V_SPATIALINDEX_ERROR, "Can't add node to quadtree"));
		}
	}
}

void Quadtree::removeNode(SceneGraphNode *node) {
	// TODO: improve performance
	//bool done = this->node_other->remove(node);
	bool done = remove_vec(this->node_other, node);
	/*int n = 1;
	for(int i=0;i<levels && !done;i++) {
	for(int j=0;j<n*n && !done;j++) {
	done = (this->nodes[i])[j]->remove(node);
	}
	n *= 2;
	}*/
	done = quadtreenodes->removeNode(node);
	if( done ) {
		node->setSpatialIndex(NULL);

	}
}

/*void Quadtreenode::render(Graphics3D *g,Graphics3D::RenderPhase phase,bool lighting_pass) {
float radius = width > depth ? width : depth;
radius = radius > height ? radius : height;
radius *= 0.70710678119;
GLfloat mvmatrix[16];
glPushMatrix();
glTranslatef(x+0.5*width,y+0.5*height,z+0.5*depth);
glGetFloatv(GL_MODELVIEW_MATRIX,mvmatrix);
glPopMatrix();

if( g->SphereInFrustum(mvmatrix[12],mvmatrix[13],mvmatrix[14],radius)!=0 ) {
//LOG(">>> %d\n", level);
//LOG("    size %d\n",this->nodes->size());
this->quadtree->renderVector(this->nodes, g, phase, lighting_pass);
for(int i=0;i<4;i++) {
if( children[i] != NULL ) {
this->children[i]->render(g, phase, lighting_pass);
}
}
}
}*/

void Quadtreenode::buildRenderQueue(GraphicsEnvironment *genv) {
	/*float radius = width > depth ? width : depth;
	radius = radius > height ? radius : height;
	//radius *= 0.70710678119;
	radius *= 0.86602540378443864676372317075294;*/
	float radius = (float)(0.5 * sqrt( width*width + depth*depth + height*height ));
	//TransformationMatrix *mvmatrix = g->getRenderer()->createTransformationMatrix();
	TransformationMatrix *mvmatrix = genv->getRenderer()->getStaticTransformationMatrix();
	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->translate(x+0.5f*width,y+0.5f*height,z+0.5f*depth);
	mvmatrix->save();
	genv->getRenderer()->popMatrix();

	if( genv->getFrustum()->SphereInFrustum((*mvmatrix)[12],(*mvmatrix)[13],(*mvmatrix)[14],radius)!=0 ) {
		//LOG(">>> %d\n", level);
		//LOG("    size %d\n",this->nodes->size());
		this->quadtree->buildRenderQueueVector(this->nodes, genv);
		for(int i=0;i<4;i++) {
			if( children[i] != NULL ) {
				this->children[i]->buildRenderQueue(genv);
			}
		}
	}
	//delete mvmatrix;
}

bool Quadtreenode::isVisible(GraphicsEnvironment *genv) {
	//return true;
	/*float radius = width > depth ? width : depth;
	radius = radius > height ? radius : height;
	//radius *= 0.70710678119;
	radius *= 0.86602540378443864676372317075294;*/
	//return true;
	float radius = (float)(0.5 * sqrt( width*width + depth*depth + height*height ));
	/*GLfloat mvmatrix[16];
	glPushMatrix();
	glTranslatef(x+0.5f*width,y+0.5f*height,z+0.5f*depth);
	glGetFloatv(GL_MODELVIEW_MATRIX,mvmatrix);
	glPopMatrix();*/
	//TransformationMatrix *mvmatrix = g->getRenderer()->createTransformationMatrix();
	TransformationMatrix *mvmatrix = genv->getRenderer()->getStaticTransformationMatrix();
	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->translate(x+0.5f*width,y+0.5f*height,z+0.5f*depth);
	mvmatrix->save();
	genv->getRenderer()->popMatrix();

	//LOG("sphere centre %f , %f, %f\n", (*mvmatrix)[12],(*mvmatrix)[13],(*mvmatrix)[14]);
	if( genv->getFrustum()->SphereInFrustum((*mvmatrix)[12],(*mvmatrix)[13],(*mvmatrix)[14],radius)!=0 ) {
		//LOG("    pass\n");
		// more expensive box test
		Vector3D pts[8];
		pts[0].set(x,y,z);
		pts[1].set(x+width,y,z);
		pts[2].set(x+width,y,z+depth);
		pts[3].set(x,y,z+depth);
		pts[4].set(x,y+height,z);
		pts[5].set(x+width,y+height,z);
		pts[6].set(x+width,y+height,z+depth);
		pts[7].set(x,y+height,z+depth);
		/*pts[0].set(x,y-radius,z-radius);
		pts[1].set(x+radius,y-radius,z-radius);
		pts[2].set(x+radius,y+radius,z-radius);
		pts[3].set(x-radius,y+radius,z-radius);
		pts[4].set(x-radius,y-radius,z+radius);
		pts[5].set(x+radius,y-radius,z+radius);
		pts[6].set(x+radius,y+radius,z+radius);
		pts[7].set(x-radius,y+radius,z+radius);*/
		for(int i=0;i<8;i++) {
			/*glPushMatrix();
			glTranslatef(pts[i].x,pts[i].y,pts[i].z);
			glGetFloatv(GL_MODELVIEW_MATRIX,mvmatrix);
			glPopMatrix();
			pts[i].x = mvmatrix[12];
			pts[i].y = mvmatrix[13];
			pts[i].z = mvmatrix[14];*/
			genv->getRenderer()->pushMatrix();
			genv->getRenderer()->translate(pts[i].x,pts[i].y,pts[i].z);
			mvmatrix->save();
			genv->getRenderer()->popMatrix();
			pts[i] = mvmatrix->getPos();
		}
		/*for(int i=0;i<8;i++) {
			LOG("pts[%d]: %f , %f, %f\n", i, pts[i].x, pts[i].y, pts[i].z);
		}*/
		if( genv->getFrustum()->BoxInFrustum(pts) )
		{
			/*if( x == 80 && y == 0 && z == 336 && children[0] == NULL ) {
			LOG("### %f , %f , %f\n", width, height, depth);
			for(int i=0;i<8;i++)
			LOG("--> %d : %f , %f , %f\n", i, pts[i].x, pts[i].y, pts[i].z);
			}*/
			//delete mvmatrix;
			return true;
		}
	}

	//delete mvmatrix;
	return false;
}

/*void Quadtree::render(Graphics3D *g,Graphics3D::RenderPhase phase,bool lighting_pass) {
this->quadtreenodes->render(g, phase, lighting_pass);
this->renderVector(this->node_other, g, phase, lighting_pass);
}*/

void Quadtree::buildRenderQueue(GraphicsEnvironment *genv) {
	this->quadtreenodes->buildRenderQueue(genv);
	this->buildRenderQueueVector(this->node_other, genv);
}

size_t Quadtree::getNodeUnoptCount() const {
	return node_other->size();
}
