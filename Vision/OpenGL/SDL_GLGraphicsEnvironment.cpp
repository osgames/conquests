//---------------------------------------------------------------------------

#include <cassert>

#include "SDL_GLGraphicsEnvironment.h"
#include "GLRenderer.h"
#include "../GraphicsEngine.h"
#include "../Texture.h"
#include "../InputHandler.h"

#ifdef _WIN32
#include <SDL.h>
#include <SDL_syswm.h>
#endif

#ifdef __linux
#include <SDL/SDL.h>
#include <SDL/SDL_syswm.h>
#endif

#include <sstream>
using std::stringstream;

SDL_GLGraphicsEnvironment::SDL_GLGraphicsEnvironment(VisionPrefs visionPrefs) : SDL_GraphicsEnvironment(visionPrefs) {
	LOG("startup SDL OpenGL GraphicsEnvironment\n");
	this->hasGLContext = false;

	this->dummy_texture = NULL;

	if( !visionPrefs.fullscreen ) {
		//putenv("SDL_VIDEO_WINDOW_POS=0,0");
		putenv("SDL_VIDEO_CENTERED=0,0");
	}
	if( SDL_Init(SDL_INIT_VIDEO) != 0 ) {
		free();
		Vision::setError(new VisionException(this,VisionException::V_FAILED_TO_OPEN_WINDOW,"Could not initialise SDL video"));
		return;
	}
	if( SDL_GetVideoInfo() == NULL ) {
		free();
		Vision::setError(new VisionException(this,VisionException::V_FAILED_TO_OPEN_WINDOW,"Could not retreive SDL video info"));
		return;
	}

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	if( visionPrefs.multisample > 0 ) {
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, visionPrefs.multisample);
	}

	Uint32 sdl_flags = SDL_OPENGL | SDL_HWSURFACE;
	if( visionPrefs.fullscreen )
		sdl_flags = sdl_flags | SDL_FULLSCREEN;
	LOG("open window %d X %d\n", visionPrefs.resolution_width, visionPrefs.resolution_height);
	LOG("fullscreen? %d\n", visionPrefs.fullscreen);
	LOG("multisample? %d\n", visionPrefs.multisample);
	if( SDL_SetVideoMode(visionPrefs.resolution_width, visionPrefs.resolution_height, 32,  sdl_flags) == NULL ) {
		free();
		Vision::setError(new VisionException(this,VisionException::V_FAILED_TO_OPEN_WINDOW,"Failed to set SDL video mode"));
		return;
	}
	LOG("SDL window created okay\n");
	this->hasGLContext = true;
	checkError("SDL window opened");

	//SDL_WM_SetCaption("Vision 3 window", "");
	stringstream str;
	str << Vision::getApplicationName() << " - Initialising";
	SDL_WM_SetCaption(str.str().c_str(), "");
#ifdef __linux
	// hack, to make up for not being able to grab mouse coords outside of the window in Linux
	// disabled, as it disables alt-tab on Linux (Ubuntu), meaning we have to force-reset the computer!
	//SDL_WM_GrabInput(SDL_GRAB_ON);
#endif

	this->multisample_ok = false;
	if( visionPrefs.multisample > 0 ) {
		int buffers = 0, samples = 0;
		SDL_GL_GetAttribute(SDL_GL_MULTISAMPLEBUFFERS, &buffers);
		SDL_GL_GetAttribute(SDL_GL_MULTISAMPLESAMPLES, &samples);
		if( buffers == 1 && samples > 0 ) {
			this->multisample_ok = true;
			LOG("multisampling is available: %d\n", samples);
		}
		else {
			LOG("failed to create multisampling: %d, %d\n", buffers, samples);
		}
	}

#ifdef _WIN32
	//this->hWnd = GetActiveWindow();
	SDL_SysWMinfo wmInfo;
	SDL_VERSION(&wmInfo.version);
	SDL_GetWMInfo(&wmInfo);
	this->hWnd = wmInfo.window;
	LOG("hWnd: %d\n", hWnd);
#endif
#ifdef __linux
	SDL_SysWMinfo wmInfo;
	SDL_VERSION(&wmInfo.version);
	SDL_GetWMInfo(&wmInfo);
	this->x_display = wmInfo.info.x11.display;
	this->x_window = wmInfo.info.x11.window;
	LOG("x_display: %d\n", x_display);
#endif

	this->g = new GLGraphics2D(this);
	//this->g->owned = true; // owned by this GraphicsEnvironment class
	this->addChild(this->g);
	this->g3 = new Graphics3D(this, visionPrefs.hdr);
	this->renderer = new GLRenderer(this);
	this->inputSystem = new InputSystem(new MouseInputHandler(), new KeyboardInputHandler());

	LOG("created subsystems okay OK\n");

	char *gl_vendor = (char *)glGetString(GL_VENDOR);
	LOG("GL Vendor: %s\n", gl_vendor);
	char *gl_renderer = (char *)glGetString(GL_RENDERER);
	LOG("GL Renderer: %s\n", gl_renderer);
	char *gl_version = (char *)glGetString(GL_VERSION);
	LOG("GL Version: %s\n", gl_version);

	// initialise extensions

	RendererInfo::init();
	GLRendererInfo::init();

	// parse GL version
	string gl_version_str(gl_version);
	size_t index1 = gl_version_str.find(".");
	size_t index2 = gl_version_str.find(".", index1+1);
	string gl_version_major_str = gl_version_str.substr(0, index1);
	LOG("    gl_version_major_str: %s\n", gl_version_major_str.c_str());
	string gl_version_minor_str = gl_version_str.substr(index1+1, index2-index1-1);
	LOG("    gl_version_minor_str: %s\n", gl_version_minor_str.c_str());
	GLRendererInfo::versionMajor = atoi(gl_version_major_str.c_str());
	LOG("    GL Version Major: %d\n", GLRendererInfo::versionMajor);
	GLRendererInfo::versionMinor = atoi(gl_version_minor_str.c_str());
	LOG("    GL Version Minor: %d\n", GLRendererInfo::versionMinor);

	char *extension = (char *)glGetString(GL_EXTENSIONS);
	if(extension == NULL) {
		return;
	}
	LOG("Extensions: %s\n", extension);

	{
		int maxLights = 0;
		glGetIntegerv(GL_MAX_LIGHTS, &maxLights);
		LOG("Max Lights? %d\n", maxLights);
		if( maxLights <= 0 ) {
			LOG("negative or zero max lights?! set to 1\n");
			maxLights = 0;
		}
		RendererInfo::maxLights = static_cast<unsigned int>(maxLights);
	}

#ifdef _WIN32
	// already defined in Linux?
	if(strstr(extension, "GL_ARB_multitexture")) {
		glMultiTexCoord2fARB = (PFNGLMULTITEXCOORD2FARBPROC)SDL_GL_GetProcAddress("glMultiTexCoord2fARB");
		glActiveTextureARB = (PFNGLACTIVETEXTUREARBPROC)SDL_GL_GetProcAddress("glActiveTextureARB");
		glClientActiveTextureARB = (PFNGLCLIENTACTIVETEXTUREARBPROC)SDL_GL_GetProcAddress("glClientActiveTextureARB");
	}
#endif
	//LOG("Max Texture Units? %d\n", RendererInfo::maxTextureUnits);

	if(strstr(extension, "GL_NV_occlusion_query")) {
		RendererInfo::hardwareOcclusion = true;
		glGenOcclusionQueriesNV = (PFNGLGENOCCLUSIONQUERIESNVPROC)SDL_GL_GetProcAddress("glGenOcclusionQueriesNV");
		glDeleteOcclusionQueriesNV = (PFNGLDELETEOCCLUSIONQUERIESNVPROC)SDL_GL_GetProcAddress("glDeleteOcclusionQueriesNV");
		glGetOcclusionQueryuivNV = (PFNGLGETOCCLUSIONQUERYUIVNVPROC)SDL_GL_GetProcAddress("glGetOcclusionQueryuivNV");
		glBeginOcclusionQueryNV = (PFNGLBEGINOCCLUSIONQUERYNVPROC)SDL_GL_GetProcAddress("glBeginOcclusionQueryNV");
		glEndOcclusionQueryNV = (PFNGLENDOCCLUSIONQUERYNVPROC)SDL_GL_GetProcAddress("glEndOcclusionQueryNV");
	}
	LOG("Hardware Occlusion? %s\n", RendererInfo::hardwareOcclusion?"T":"F");

	if(strstr(extension, "GL_EXT_compiled_vertex_array")) {
		GLRendererInfo::compiledVAs = true;
		glLockArraysEXT = (PFNGLLOCKARRAYSEXTPROC)SDL_GL_GetProcAddress("glLockArraysEXT");
		glUnlockArraysEXT = (PFNGLUNLOCKARRAYSEXTPROC)SDL_GL_GetProcAddress("glUnlockArraysEXT");
	}
	LOG("Compiled Vertex Array? %s\n", GLRendererInfo::compiledVAs?"T":"F");

	//if(false) {
	if(strstr(extension, "GL_ARB_vertex_buffer_object")) {
		GLRendererInfo::vertexBufferObjects = true;
		glGenBuffersARB = (PFNGLGENBUFFERSARBPROC) SDL_GL_GetProcAddress("glGenBuffersARB");
		glBindBufferARB = (PFNGLBINDBUFFERARBPROC) SDL_GL_GetProcAddress("glBindBufferARB");
		glBufferDataARB = (PFNGLBUFFERDATAARBPROC) SDL_GL_GetProcAddress("glBufferDataARB");
		glBufferSubDataARB = (PFNGLBUFFERSUBDATAARBPROC) SDL_GL_GetProcAddress("glBufferSubDataARB");
		glDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC) SDL_GL_GetProcAddress("glDeleteBuffersARB");
	}
	LOG("VBOs? %s\n", GLRendererInfo::vertexBufferObjects?"T":"F");
	if( GLRendererInfo::vertexBufferObjects && !visionPrefs.vbo ) {
		GLRendererInfo::vertexBufferObjects = false;
		LOG(">>> Disabling VBOs\n");
	}

	//if(false) {
	if(strstr(extension, "GL_EXT_draw_range_elements")) {
		GLRendererInfo::drawRangeElements = true;
		glDrawRangeElementsEXT = (PFNGLDRAWRANGEELEMENTSPROC) SDL_GL_GetProcAddress("glDrawRangeElementsEXT");
	}
	LOG("Draw Range Elements? %s\n", GLRendererInfo::drawRangeElements?"T":"F");

	// ATI two-sided stencil disabled - on Radeon HD 6570, the line glEnable(GL_STENCIL_TEST_TWO_SIDE_EXT); generates an OpenGL error! (Without shaders, and sometimes with.)
	/*if(strstr(extension, "GL_ATI_separate_stencil") && strstr(extension, "GL_EXT_stencil_wrap") ) {
		GLRendererInfo::separateStencilATI = true;
		glStencilOpSeparateATI = (PFNGLSTENCILOPSEPARATEATIPROC) SDL_GL_GetProcAddress("glStencilOpSeparateATI");
		glStencilFuncSeparateATI = (PFNGLSTENCILFUNCSEPARATEATIPROC) SDL_GL_GetProcAddress("glStencilFuncSeparateATI");
	}
	LOG("ATI two sided stencil? %s\n", GLRendererInfo::separateStencilATI?"T":"F");
	if( GLRendererInfo::separateStencilATI && !visionPrefs.twosidedstencil ) {
		GLRendererInfo::separateStencilATI = false;
		LOG(">>> Disabling ATI two sided stencil\n");
	}*/

	if(strstr(extension, "GL_EXT_stencil_two_side") && strstr(extension, "GL_EXT_stencil_wrap") ) {
		GLRendererInfo::separateStencil = true;
		glActiveStencilFaceEXT = (PFNGLACTIVESTENCILFACEEXTPROC) SDL_GL_GetProcAddress("glActiveStencilFaceEXT");
	}
	LOG("Two sided stencil? %s\n", GLRendererInfo::separateStencil?"T":"F");
	if( GLRendererInfo::separateStencil && !visionPrefs.twosidedstencil ) {
		GLRendererInfo::separateStencil = false;
		LOG(">>> Disabling two sided stencil\n");
	}

	if(strstr(extension, "GL_EXT_framebuffer_object")) {
		RendererInfo::framebufferObject = true;
		glBindRenderbufferEXT = (PFNGLBINDRENDERBUFFEREXTPROC) SDL_GL_GetProcAddress("glBindRenderbufferEXT");
		glDeleteRenderbuffersEXT = (PFNGLDELETERENDERBUFFERSEXTPROC) SDL_GL_GetProcAddress("glDeleteRenderbuffersEXT");
		glGenRenderbuffersEXT = (PFNGLGENRENDERBUFFERSEXTPROC) SDL_GL_GetProcAddress("glGenRenderbuffersEXT");
		glRenderbufferStorageEXT = (PFNGLRENDERBUFFERSTORAGEEXTPROC) SDL_GL_GetProcAddress("glRenderbufferStorageEXT");
		glBindFramebufferEXT = (PFNGLBINDFRAMEBUFFEREXTPROC) SDL_GL_GetProcAddress("glBindFramebufferEXT");
		glDeleteFramebuffersEXT = (PFNGLDELETEFRAMEBUFFERSEXTPROC) SDL_GL_GetProcAddress("glDeleteFramebuffersEXT");
		glGenFramebuffersEXT = (PFNGLGENFRAMEBUFFERSEXTPROC) SDL_GL_GetProcAddress("glGenFramebuffersEXT");

		glCheckFramebufferStatusEXT = (PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC) SDL_GL_GetProcAddress("glCheckFramebufferStatusEXT");
		glFramebufferTexture2DEXT = (PFNGLFRAMEBUFFERTEXTURE2DEXTPROC) SDL_GL_GetProcAddress("glFramebufferTexture2DEXT");
		glFramebufferRenderbufferEXT = (PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC) SDL_GL_GetProcAddress("glFramebufferRenderbufferEXT");
	}
	LOG("FBOs? %s\n", RendererInfo::framebufferObject?"T":"F");

	/*if( visionPrefs.shaders ) {
		this->initCG();
	}*/
	try {
		g3->init(); // also inits shaders
		// need try/catch block to avoid memory leak (e.g., if CG error, an exception is throw!
	}
	catch(...) {
		LOG("exception thrown initialising graphics engine\n");
		free();
		throw;
	}

	// we do this, as the code expects there to be a projection matrix (for the 3D viewing mode) on the stack
	this->renderer->switchToProjection();
	this->renderer->pushMatrix();
	this->renderer->switchToModelview();

	g3->ReSizeGLScene(renderer->getZNear(), renderer->getZFar(), visionPrefs.resolution_width, visionPrefs.resolution_height, renderer->getPersp(), fovy); // need to reset viewport/perspective info

	this->initSDL();

	//this->created = true;

	{
		/* Hack - OpenGL throws an error if rendering without a texture, when no texture has
		 * previously been bound, when using shaders.
		 * Really needs to be fixed by using a dedicated shader for no textures, but for now, this
		 * is a workaround...
		 * Affects VisionTest(3, 1)
		 */
		Image2D *image = new Image2D(64, 64, 255, 255, 255, true, 255);
		this->dummy_texture = this->renderer->createTexture(image);
		//this->dummy_texture->owned = true;
		this->addChild(this->dummy_texture);
		this->dummy_texture->enable();
	}

	checkError("SDL_GLGraphicsEnvironment constructor complete");
	LOG("all done!\n");

}

SDL_GLGraphicsEnvironment::~SDL_GLGraphicsEnvironment() {
	free();
}

void SDL_GLGraphicsEnvironment::free() {
	if( this->hasContext() ) {
		checkError("SDL_GLGraphicsEnvironment::~SDL_GLGraphicsEnvironment enter");
	}

	/*if( dummy_texture != NULL ) {
		delete dummy_texture;
		dummy_texture = NULL;
	}*/
	// g is dummy_texture automatically as its owned by this class

	/*if( g != NULL ) {
		delete g;
		g = NULL;
	}*/
	// g is deleted automatically as its owned by this class
	if( g3 != NULL ) {
		delete g3;
		g3 = NULL;
	}
	if( renderer != NULL ) {
		delete renderer;
		renderer = NULL;
	}
	if( inputSystem != NULL ) {
		delete inputSystem;
		inputSystem = NULL;
	}

	SDL_Quit();
}

void SDL_GLGraphicsEnvironment::checkError(const char *label) {
	//return;
	if( !existsOrHasContext() ) {
		LOG("Tried to check for GL errors when no available GL context!\n");
		Vision::setError(new VisionException(NULL,VisionException::V_GL_ERROR,"Tried to check for GL errors when no available GL context"));
		return;
	}
	//return;
	// OpenGL error handling
	GLenum gl_err;
	while( gl_err = glGetError(), gl_err != GL_NO_ERROR ) {
		LOG("GL Error (%s): %d , %s\n", label, gl_err, gluErrorString(gl_err));
		Vision::setError(new VisionException(NULL,VisionException::V_GL_ERROR,"OpenGL reported an error"));
	}
}

void SDL_GLGraphicsEnvironment::swapBuffers() {
	this->renderer->endScene();
	SDL_GL_SwapBuffers();
}

bool SDL_GLGraphicsEnvironment::saveScreen(const char *filename,const char *type) {
	unsigned char *data = new unsigned char[visionPrefs.resolution_width*visionPrefs.resolution_height*3];
	memset(data,0,visionPrefs.resolution_width*visionPrefs.resolution_height*3);
	//glReadBuffer(GL_FRONT);
	glReadPixels(0,0,visionPrefs.resolution_width,visionPrefs.resolution_height,GL_RGB,GL_UNSIGNED_BYTE,data);
	/*Image2D *image = new Image2D();
	image->rgbdata = (rgb_struct *)data;
	image->w = width;
	image->h = height;*/
	Image2D *image = new Image2D(data, visionPrefs.resolution_width, visionPrefs.resolution_height);
	bool ok = image->save(filename,type);

	delete image;
	return ok;
}

/*bool SDL_GLGraphicsEnvironment::hasShaders() const {
	//return cgContext != NULL;
	//return this->renderer->hasShaders();
	const GLRenderer *d_renderer = static_cast<const GLRenderer *>(this->renderer);
	return d_renderer->hasShaders();
}*/

/*Shader *SDL_GLGraphicsEnvironment::createShader(const char *file, const char *func, bool vertex_shader) {
	if( !this->hasShaders() )
		return NULL;
	//CGGLShader *shader = new CGGLShader(g3, static_cast<GLRenderer *>(g3->getRenderer()), vertex_shader);
	CGGLShader *shader = new CGGLShader(static_cast<GLRenderer *>(renderer), vertex_shader);
	LOG("Loading %s shader %d : %s / %s\n", vertex_shader?"vertex":"pixel", shader, file, func);
	//shader->cgProfile = vertex_shader ? this->cgVertexProfile : this->cgPixelProfile;
	shader->cgProfile = vertex_shader ? static_cast<GLRenderer *>(renderer)->cgVertexProfile : static_cast<GLRenderer *>(renderer)->cgPixelProfile;

	shader->cgProgram = cgCreateProgramFromFile(static_cast<GLRenderer *>(renderer)->cgContext, CG_SOURCE, file, shader->cgProfile, func, 0);
	if( shader->cgProgram == NULL ) {
		LOG("### Failed to load %s shader:\n", vertex_shader?"vertex":"pixel");
		CGerror Error = cgGetError();
		LOG(cgGetErrorString(Error));
		LOG("\n");
		delete shader;
		//this->cgContext = NULL;
		return NULL;
	}
	cgGLLoadProgram(shader->cgProgram);
	shader->init();
	//g3->addShader(shader);

	return shader;
}*/
