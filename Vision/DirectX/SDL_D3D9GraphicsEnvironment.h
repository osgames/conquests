#pragma once

#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "d3d9.lib")

#include <d3dx9.h>

#include "../SDL_GraphicsEnvironment.h"

class SDL_D3D9GraphicsEnvironment : public SDL_GraphicsEnvironment {
	LPDIRECT3D9       m_pD3D;
	LPDIRECT3DDEVICE9 m_pDevice;
	bool m_bDeviceLost;
	D3DPRESENT_PARAMETERS d3dpp; 

	void closeScreen();

	//static LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );

	bool handlePresentRetVal(HRESULT hResult);
	bool resetDevice();
	// Called when the device is lost or just reset
	void onDeviceLost();
	void onDeviceReset();

	virtual bool drawFrameStart();
	void free();

public:
	SDL_D3D9GraphicsEnvironment(VisionPrefs visionPrefs);
	virtual ~SDL_D3D9GraphicsEnvironment();

	virtual bool hasContext() const {
		return false;
	}
	//virtual Shader *createShader(const char *file, const char *func, bool vertex_shader);

	// interface
	/*virtual HWND getHWND() const {
		return this->hWnd;
	}*/
	virtual void swapBuffers();
	virtual bool saveScreen(const char *filename,const char *type);
	virtual void checkError(const char *label) {
	}
	//virtual bool hasShaders() const;
};
