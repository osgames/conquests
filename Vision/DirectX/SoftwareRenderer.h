#pragma once

#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include <cmath>

#define WIN32_MEAN_AND_LEAN
#include <windows.h>
#include <d3dx9.h>

#include "../Renderer.h"

class SoftwareRenderer;
class Image2D;

/*class D3D9RendererInfo {
public:
	static bool twoSidedStencil;

	static void init() {
		twoSidedStencil = false;
	}
};*/

class SoftwareVertexArray : public VertexArray {
	SoftwareRenderer *renderer;
	friend class SoftwareRenderer;

	SoftwareVertexArray(SoftwareRenderer *renderer, VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim);
public:
	virtual ~SoftwareVertexArray();

	static SoftwareVertexArray *create(SoftwareRenderer *renderer, VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim);

	virtual bool hasVBO() const {
		return false;
		//return data != NULL; // effectively always available, as long as there is data?
	}

	virtual void renderVertices(bool new_data, int offset) const;
	virtual void renderNormals(bool new_data, int offset) const;
	virtual void renderTexCoords(bool new_data, int offset) const;
	virtual void renderTexCoords(bool new_data, int offset, int unit) const;
	virtual void renderColors(bool new_data, int offset) const;
	virtual void update() const;
	virtual void updateIndices() const;
	virtual void renderElements(DrawingMode mode, int n_this_renderlength, int n_this_vertices) const;
};

class SoftwareTransformationMatrix : public TransformationMatrix {
	SoftwareRenderer *renderer;
	D3DXMATRIX mat;
public:
	SoftwareTransformationMatrix(SoftwareRenderer *renderer) : renderer(renderer) {
	}
	virtual void load() const;
	virtual void save();
	virtual void mult() const;

	virtual void set(int indx, float value) {
		//(*this)[indx] = value; // <--- may not work in Release mode? (see comment below for get())
		int row = indx/4;
		int col = indx%4;
		this->mat(row, col) = value;
	}
	virtual float &operator[](const int i) {
		int row = i/4;
		int col = i%4;
		return mat(row, col);
	}
	virtual float get(int indx) const {
		//return (*this)[indx]; // <--- doesn't work in Release mode!
		int row = indx/4;
		int col = indx%4;
		return mat(row, col);
	}
	virtual float get(int row, int col) const {
		return mat(row, col);
	}
	virtual Vector3D getPos() const {
		Vector3D pos(this->mat(3, 0), this->mat(3, 1), this->mat(3, 2));
		return pos;
	}
	virtual Vector3D transformVec(const Vector3D &in) const {
		D3DXVECTOR3 din(in.x, in.y, in.z);
		D3DXVECTOR4 dout;
		D3DXVec3Transform(&dout, &din, &mat);
		Vector3D out(dout.x, dout.y, dout.z);
		return out;
	}
};

class SoftwareTexture : public Texture {
	SoftwareRenderer *renderer;
	Image2D *image;

public:
	SoftwareTexture(SoftwareRenderer *renderer, Image2D *image);
	virtual ~SoftwareTexture();

	virtual Texture *copy();
	virtual void copyTo(int width, int height);
	const Image2D *getImage() const {
		return image;
	}

	static Texture *create(SoftwareRenderer *renderer, int width, int height, bool alpha);
	static Texture *createFromImage(SoftwareRenderer *renderer, VI_Image *image, bool mask, unsigned char maskcol[3]);
	static Texture *loadTexture(SoftwareRenderer *renderer, const char *filename, bool mask, unsigned char maskcol[3]);

	// interface
	virtual int getWidth() const;
	virtual int getHeight() const;
	virtual void draw(int x, int y);
	virtual void draw(int x, int y, int w, int h);
	virtual void enable();
	/*virtual void clampToEdge();
	virtual void clampMirror();*/
};

/*class D3D9FramebufferObject : public FramebufferObject {
	const D3D9Renderer *renderer;
	LPDIRECT3DTEXTURE9 texture;
	PDIRECT3DSURFACE9 depth_stencil;

public:

	D3D9FramebufferObject(const D3D9Renderer *renderer) : FramebufferObject(), renderer(renderer), texture(NULL), depth_stencil(NULL) {
	}
	virtual ~D3D9FramebufferObject() {
		free();
	}

	virtual bool generate(int width,int height,bool want_depth_stencil);
	virtual void free();

	virtual void bind() const;
	virtual void enableTexture() const;
};*/

/*class D3D9QuadricObject : public QuadricObject {
	LPD3DXMESH mesh;

	D3D9QuadricObject(LPD3DXMESH mesh) : QuadricObject(), mesh(mesh) {
	}
public:
	virtual ~D3D9QuadricObject() {
		mesh->Release();
	}

	virtual void draw() const {
		mesh->DrawSubset(0);
	}

	static D3D9QuadricObject *createSphere(LPDIRECT3DDEVICE9 device, float radius, int slices, int stacks) {
		LPD3DXMESH mesh;
		D3DXCreateSphere(device, radius, slices, stacks, &mesh, NULL);
		D3D9QuadricObject *qobj = new D3D9QuadricObject(mesh);
		return qobj;
	}
};*/

class SoftwareFontBuffer : public FontBuffer {
	SoftwareRenderer *renderer;

	Texture **textures;
	int descent;
	int *advance_x;
	int *offset_x;
	int *offset_y;
	int *width; // bitmap widths
	int *height; // bitmap heights
	int font_height;

	int getIndex(char letter) const;
public:
	SoftwareFontBuffer(SoftwareRenderer *renderer,const char *family,int pt,int style);
	virtual ~SoftwareFontBuffer();

	virtual size_t memUsage() {
		// TODO:
		return 0;
	}

	// TODO:
	virtual int writeText(int x,int y,const char *text) const;
	virtual int writeText(int x,int y,const char *text,size_t length) const;
	virtual int getWidth(char letter) const;
	virtual int getWidth(const char *text) const {
		int w = 0;
		while(*text != NULL)
			w += getWidth(*text++);
		return w;
	}
	virtual int getHeight() const {
		return font_height;
	}
};

class SoftwareGraphics2D : public Graphics2D {
	friend class D3D9GraphicsEnvironment;
	friend class SDL_D3D9GraphicsEnvironment;

	SoftwareRenderer *renderer;

	SoftwareGraphics2D(GraphicsEnvironment *genv, SoftwareRenderer *renderer);

public:
	virtual void setColor3i(unsigned char r, unsigned char g, unsigned char b);
	virtual void setColor4i(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	virtual void plot(short x,short y) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareGraphics2D:: not supported\n")); // TODO:
	}
	virtual void draw(short x,short y) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareGraphics2D:: not supported\n")); // TODO:
	}
	virtual void draw(short x1,short y1,short x2,short y2) const {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareGraphics2D:: not supported\n"));
		// TODO:
	}
	virtual void drawRect(short x,short y,short width,short height) const {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareGraphics2D:: not supported\n"));
		// TODO:
	}
	virtual void fillRect(short x,short y,short width,short height,const float tu[4], const float tv[4]) const;
	virtual void fillTriangle(const short *x,const short *y) const {
		// TODO:
	}
};

const unsigned int MAX_SOFTWARE_LIGHTS = 8;

class SoftwareRenderer : public Renderer {
	friend class D3D9GraphicsEnvironment;
	friend class SDL_D3D9GraphicsEnvironment;
	friend class SoftwareGraphics2D;
	friend class SoftwareVertexArray;
	//friend class CGD3D9Shader;
	friend class SoftwareTransformationMatrix;
	friend class SoftwareTexture;
	//friend class D3D9FramebufferObject;
	friend class SoftwareFontBuffer;

	LPDIRECT3D9       m_pD3D;
	LPDIRECT3DDEVICE9 m_pDevice;

	int pitch;
	DWORD *color_buffer;
	DWORD *depth_stencil_buffer;

	enum TransformType {
		TRANSFORMTYPE_MODEL = 0,
		TRANSFORMTYPE_PROJECTION = 1,
		TRANSFORMTYPE_TEXTURE = 2,
		TRANSFORMTYPE_UNKNOWN
	};
	TransformType c_transform;
	D3DXMATRIX transform_model;
	D3DXMATRIX transform_projection;
	D3DXMATRIX transform_texture;

	LPD3DXMATRIXSTACK stack_model;
	LPD3DXMATRIXSTACK stack_projection;
	LPD3DXMATRIXSTACK stack_texture;
	LPD3DXMATRIXSTACK c_stack;

	D3DCOLOR clear_color; // here we can use D3DCOLOR directly, as no processing is required
	D3DXVECTOR4 color_current;
	D3DXVECTOR4 color_ambient;
	D3DXVECTOR4 color_specular;

	bool cull_face, front_face;
	bool lighting;
	bool texturing;
	SoftwareTexture *c_texture;
	//const Light *c_light[MAX_SOFTWARE_LIGHTS];
	bool c_lighton[MAX_SOFTWARE_LIGHTS];
	float c_lightambient[MAX_SOFTWARE_LIGHTS][4];
	float c_lightspecular[MAX_SOFTWARE_LIGHTS][4];
	float c_lightdiffuse[MAX_SOFTWARE_LIGHTS][4];
	float c_lightpos[MAX_SOFTWARE_LIGHTS][3];
	bool c_lightdirectional[MAX_SOFTWARE_LIGHTS];
	RenderState state_depth_buffer_mode;
	RenderState state_depth_buffer_func;
	RenderState state_blend_mode;

	bool render_normals;
	bool render_texcoords;
	bool render_colors;
	const float *render_array_vertices;
	int render_array_vertices_size;
	const float *render_array_normals;
	const float *render_array_texcoords;
	const unsigned char *render_array_colors;
	bool render_array_colors_alpha;

	void write(int x, int y, DWORD color) {
		color_buffer[y*pitch+x] = color;
	}
	DWORD read(int x, int y) const {
		return color_buffer[y*pitch+x];
	}
	void writeZ(int x, int y, int z) {
		depth_stencil_buffer[y*pitch+x] = (DWORD)z;
	}
	int readZ(int x, int y) const {
		return (int)depth_stencil_buffer[y*pitch+x];
	}

	void setTransform(const D3DXMATRIX *transform);
	void multiplyTransform(const D3DXMATRIX *transform);
	void getTransform(D3DXMATRIX *transform) const;

	// rendering operations
	struct SoftwareVertexOut {
		float x, y, z;
		int sx, sy;
		//DWORD color;
		D3DXVECTOR4 color;
		//float u, v;
		D3DXVECTOR2 texcoord;
	};

	void transformVertex(SoftwareVertexOut *vertOut, const D3DXVECTOR3 *vertIn, const D3DXVECTOR3 *normIn, const D3DXVECTOR4 *colorIn, const D3DXVECTOR2 *texcoordIn, const D3DXMATRIX *transform_model, const D3DXMATRIX *transform_projection) const;
	void flatScan32(DWORD *line, DWORD *zbuffer, int x1, int x2, float z1, float z2, D3DXVECTOR4 color1, const D3DXVECTOR4 *color2, D3DXVECTOR2 texcoord1, const D3DXVECTOR2 *texcoord2);
	void drawPoly(const SoftwareVertexOut *p1, const SoftwareVertexOut *p2, const SoftwareVertexOut *p3);
	void renderElements(VertexArray::DrawingMode mode, unsigned short *indices, int n_this_renderlength, int n_this_vertices);
	void render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, const float *normal_data, const unsigned short *indices, int count, const unsigned char *color_data, bool alpha, int color_repeat);

public:

	SoftwareRenderer(GraphicsEnvironment *genv, LPDIRECT3D9 pD3D, LPDIRECT3DDEVICE9 pDevice);
	virtual ~SoftwareRenderer();

	// interface

	virtual void beginScene() const;
	virtual void endScene() const;
	virtual void drawFrameTest() const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}

	// factories
	virtual TransformationMatrix *createTransformationMatrix() {
		return new SoftwareTransformationMatrix(this);
	}
	virtual Texture *createTexture(int w,int h,bool alpha,bool floating_point) {
		// floating_point not supported
		return SoftwareTexture::create(this, w, h, alpha);
	}
	virtual Texture *createTexture(VI_Image *image) {
		return SoftwareTexture::createFromImage(this, image, false, NULL);
	}
	virtual Texture *createTextureWithMask(VI_Image *image, unsigned char maskcol[3]) {
		return SoftwareTexture::createFromImage(this, image, true, maskcol);
	}
	virtual Texture *loadTexture(const char *filename) {
		return SoftwareTexture::loadTexture(this, filename, false, NULL);
	}
	virtual Texture *loadTextureWithMask(const char *filename, unsigned char maskcol[3]) {
		return SoftwareTexture::loadTexture(this, filename, true, maskcol);
	}
	virtual FramebufferObject *createFramebufferObject() {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		return NULL;
		//return new D3D9FramebufferObject(this);
	}
	virtual QuadricObject *createSphereObject(float radius, int slices, int stacks) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		return NULL;
		//return D3D9QuadricObject::createSphere(this->m_pDevice, radius, slices, stacks);
	}
	virtual VertexArray *createVertexArray(VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim) {
		SoftwareVertexArray *va = SoftwareVertexArray::create(this, data_type, tex_unit, stream, data, size, dim);
		return va;
	}
	virtual FontBuffer *createFont(const char *family,int pt) {
		return createFont(family, pt, 0);
	}
	virtual FontBuffer *createFont(const char *family,int pt,int style) {
		return new SoftwareFontBuffer(this, family, pt, style);
	}

	// general commands
	virtual void setViewport(int width, int height) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void resizeScene(float frustum[6][4], int width, int height, bool persp, float z_near, float z_far, float fovy);
	virtual void setClearColor(float r, float g, float b, float a) {
		this->clear_color = D3DCOLOR_COLORVALUE(r, g, b, a);
	}
	virtual void clear(bool color, bool depth, bool stencil) const;

	// render states
	virtual void setToDefaults() {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}

	virtual void setDepthBufferMode(RenderState state) {
		this->state_depth_buffer_mode = state;
	}
	virtual void setDepthBufferFunc(RenderState state) {
		this->state_depth_buffer_func = state;
	}
	virtual void setBlendMode(RenderState state) {
		this->state_blend_mode = state;
	}
	virtual void setAlphaTestMode(RenderState state) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void setStencilMode(RenderState state) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void setFogMode(RenderState state, float start, float end, float density) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		// TODO:
	}
	virtual void setFogColor(const float color[4]) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		// TODO:
	}

	virtual int nShadowPasses() const {
		//return D3D9RendererInfo::twoSidedStencil ? 1 : 2;
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		return NULL;
	}

	virtual void enableCullFace(bool enable) {
		this->cull_face = enable;
	}
	virtual void setFrontFace(bool ccw) {
		this->front_face = ccw;
	}
	virtual void enableTexturing(bool enable) {
		this->texturing = enable;
		if( !enable ) {
			this->c_texture = NULL;
		}
	}
	virtual void enableMultisample(bool enable) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		// TODO:
	}
	virtual void enableColorMask(bool enable) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		//this->m_pDevice->SetRenderState( D3DRS_COLORWRITEENABLE, enable ? (D3DCOLORWRITEENABLE_ALPHA | D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE) : NULL );
	}
	virtual void enableClipPlane(const double pl[4]) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void reenableClipPlane() {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void disableClipPlane() {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void enableScissor(int x,int y,int w,int h) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void reenableScissor() {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void disableScissor() {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void pushAttrib(bool viewport) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void popAttrib() const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}

	// fixed function only functions
	virtual void setFixedFunctionMatSpecular(const float specular[4]) {
		//this->color_specular = D3DCOLOR_COLORVALUE(specular[0], specular[1], specular[2], specular[3]);
		this->color_specular.x = 255.0f*specular[0];
		this->color_specular.y = 255.0f*specular[1];
		this->color_specular.z = 255.0f*specular[2];
		this->color_specular.w = 255.0f*specular[3];
	}
	/*virtual void setFixedFunctionMatColor(const float color[4]) {
		this->color_current.x = 255.0f*color[0];
		this->color_current.y = 255.0f*color[1];
		this->color_current.z = 255.0f*color[2];
		this->color_current.w = 255.0f*color[3];
	}*/
	virtual void enableFixedFunctionLighting(bool enable) {
		this->lighting = enable;
	}
	virtual void setFixedFunctionAmbientLighting(const float col[4]) {
		//this->color_ambient = D3DCOLOR_COLORVALUE(col[0], col[1], col[2], col[3]);
		this->color_ambient.x = 255.0f*col[0];
		this->color_ambient.y = 255.0f*col[1];
		this->color_ambient.z = 255.0f*col[2];
		this->color_ambient.w = 255.0f*col[3];
	}
	virtual void enableFixedFunctionLight(unsigned int index, const float ambient[4], const float diffuse[4], const float specular[4], const float attenuation[3], const float position[3], bool directional) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		//this->c_light[index] = light;
		this->c_lighton[index] = true;
		for(int i=0;i<4;i++) {
			this->c_lightambient[index][i] = ambient[i];
			this->c_lightdiffuse[index][i] = diffuse[i];
			this->c_lightspecular[index][i] = specular[i];
		}
		for(int i=0;i<3;i++) {
			this->c_lightpos[index][i] = position[i];
		}
		this->c_lightdirectional[index] = directional;
	}
	virtual void disableFixedFunctionLights() {
		for(int unsigned i=0;i<RendererInfo::maxLights;i++) {
			disableFixedFunctionLight(i);
		}
	}
	virtual void disableFixedFunctionLight(unsigned int index) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		//this->c_light[index] = NULL;
		this->c_lighton[index] = false;
	}

	// drawing
	virtual void draw(const Vector3D *p0, const Vector3D *p1, const unsigned char rgba[4]);
	virtual void setColor4(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
		//this->color_current = D3DCOLOR_RGBA(r, g, b, a);
		this->color_current.x = (float)r;
		this->color_current.y = (float)g;
		this->color_current.z = (float)b;
		this->color_current.w = (float)a;
	}
	virtual void render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count) {
		render(mode, vertex_data, texcoord_data, count, NULL, false, 0);
	}
	virtual void render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count, const unsigned char *color_data, bool alpha, int color_repeat);
	/*virtual void renderBillboards(int count, Vector3D *offsets, Vector3D tdiagm, Vector3D tdiagp, Vector3D out, unsigned char *color_data, bool alpha, float *sizes, Vector3D normal) const {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		// TODO:
	}*/
	/*virtual void renderBegin(VertexArray::DrawingMode mode) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void renderIntermediate(float *vertex_data, float *texcoord_data, int count, unsigned char *color_data, bool alpha, int color_repeat) const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}
	virtual void renderEnd() const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}*/
	virtual bool supportsQuads() const {
		return false;
	}

	//virtual void startArrays(VertexFormat vertexFormat, bool material_color, bool have_texcoords0, bool have_texcoords1) {
	virtual void startArrays(VertexFormat vertexFormat) {
		/*if( have_texcoords1 ) {
			Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer::startArrays can't support multitexturing unit 1\n"));
		}*/

		if( vertexFormat == VERTEXFORMAT_P ) {
			this->render_normals = false;
			this->render_texcoords = false;
			this->render_colors = false;
		}
		else if( vertexFormat == VERTEXFORMAT_PT0T1C ) {
			/*this->render_normals = false;
			this->render_texcoords = have_texcoords0;
			this->render_colors = !material_color;*/
			Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer::startArrays can't support multitexturing unit 1\n"));
		}
		else if( vertexFormat == VERTEXFORMAT_PNT0T1C ) {
			/*this->render_normals = true;
			this->render_texcoords = have_texcoords0;
			this->render_colors = !material_color;*/
			Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer::startArrays can't support multitexturing unit 1\n"));
		}
		else if( vertexFormat == VERTEXFORMAT_PNT0T1T2C ) {
			Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer::startArrays can't support multitexturing unit 2\n"));
		}
	}
	virtual void endArrays()  {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
		this->render_normals = false;
		this->render_texcoords = false;
		this->render_colors = false;
	}
	/*virtual void endArraysMulti(bool vbos)  {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}*/

	// texture unit handling
	virtual void activeTextureUnit(int unit) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}

	// transformation matrices
	virtual void switchToModelview() {
		this->c_transform = TRANSFORMTYPE_MODEL;
		this->c_stack = this->stack_model;
	}
	virtual void switchToProjection() {
		this->c_transform = TRANSFORMTYPE_PROJECTION;
		this->c_stack = this->stack_projection;
	}
	virtual void switchToTexture() {
		// TODO: make transform and stack work for multiple texture units?
		this->c_transform = TRANSFORMTYPE_TEXTURE;
		this->c_stack = this->stack_texture;
	}
	virtual void ortho2D(int width, int height);
	virtual void pushMatrix() {
		D3DXMATRIX matrix;
		this->getTransform(&matrix);
		this->c_stack->Push();
		this->c_stack->LoadMatrix(&matrix);
	}
	virtual void popMatrix() {
		D3DXMATRIX *matrix = this->c_stack->GetTop();
		this->setTransform(matrix);
		this->c_stack->Pop();
	}
	virtual void loadIdentity() {
		D3DXMATRIX matrix;
		D3DXMatrixIdentity(&matrix);
		this->setTransform(&matrix);
	}
	virtual void translate(float x,float y,float z) {
		D3DXMATRIX matrix;
		D3DXMatrixTranslation(&matrix, x, y, z);
		this->multiplyTransform(&matrix);
	}
	virtual void rotate(float angle,float x,float y,float z) {
		D3DXVECTOR3 axis(x, y, z);
		D3DXMATRIX matrix;
		const float factor = - (float)M_PI / 180.0f;
		angle *= factor; // DirectX requires angles in radians; rotation is in opposite direction to glRotatef
		D3DXMatrixRotationAxis(&matrix, &axis, angle);
		this->multiplyTransform(&matrix);
	}
	virtual void scale(float x,float y,float z) {
		D3DXMATRIX matrix;
		D3DXMatrixScaling(&matrix, x, y, z);
		this->multiplyTransform(&matrix);
	}
	virtual void transform(const Vector3D *pos, const Quaternion *rot, bool infinite) {
		// TODO: improve performance, by copying to D3DXMATRIX directly?
		float matrix[16];
		rot->convertToGLMatrix(matrix);
		matrix[12] = pos->x;
		matrix[13] = pos->y;
		matrix[14] = pos->z;
		if( infinite ) {
			matrix[15] = 0.0;
		}
		D3DXMATRIX dxmatrix(matrix);
		this->multiplyTransform(&dxmatrix);
	}
	virtual void transform(const Quaternion *rot) {
		// TODO: improve performance, by copying to D3DXMATRIX directly?
		float matrix[16];
		rot->convertToGLMatrix(matrix);
		D3DXMATRIX dxmatrix(matrix);
		this->multiplyTransform(&dxmatrix);
	}
	virtual void transformInverse(const Quaternion *rot) {
		// TODO: improve performance, by copying to D3DXMATRIX directly?
		float matrix[16];
		rot->convertInverseToGLMatrix(matrix);
		D3DXMATRIX dxmatrix(matrix);
		this->multiplyTransform(&dxmatrix);
	}
	virtual void getViewOrientation(Vector3D *right,Vector3D *up);
	virtual void getViewDirection(Vector3D *dir);

	// frame buffers
	virtual void unbindFramebuffer() const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	}

	virtual bool hasShaders() const {
		return false;
	}
	virtual Shader *createShaderCG(const char *file, const char *func, bool vertex_shader) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer doesn't support shaders"));
		return NULL;
	}
	virtual ShaderEffect *createShaderEffectCGFX(const char *file,const char **args) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer doesn't support shaders"));
		return NULL;
	}
	virtual void disableShaders() {
	}
};
