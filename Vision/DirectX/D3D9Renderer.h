#pragma once

#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include <cmath>

#include <set>
using std::set;

#define WIN32_MEAN_AND_LEAN
#include <windows.h>
#include <d3dx9.h>

#include "../Renderer.h"
#include "../Cg/CGShader.h"

class D3D9Renderer;

class D3D9RendererInfo {
public:
	static bool twoSidedStencil;

	static void init() {
		twoSidedStencil = false;
	}
};

class D3D9Item {
public:
	virtual void onDeviceLost()=0;
	virtual void onDeviceReset()=0;
};

class CGD3D9Shader : public CGShader {
	D3D9Renderer *renderer;

	// Matrices
	/*CGparameter cgparam_ModelViewProj;
	CGparameter cgparam_ModelViewIT;
	CGparameter cgparam_ModelView;*/

protected:
	virtual void enable() const;
	//virtual void disable() const;

public:
	CGD3D9Shader(D3D9Renderer *renderer,bool vertex_shader);

	//void init();

	virtual void setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const;
	virtual void SetModelViewProjMatrix(const char *parameter) const;

	/*virtual void SetUniformParameter1f(const char *pszParameter, float p1) const;
	virtual void SetUniformParameter2f(const char *pszParameter, float p1, float p2) const;
	virtual void SetUniformParameter3f(const char *pszParameter, float p1, float p2, float p3) const;
	virtual void SetUniformParameter4f(const char *pszParameter, float p1, float p2, float p3, float p4) const;
	virtual void SetUniformParameter2fv(const char *pszParameter, const float v[2]) const;
	virtual void SetUniformParameter3fv(const char *pszParameter, const float v[3]) const;
	virtual void SetUniformParameter4fv(const char *pszParameter, const float v[4]) const;*/
};

class CGFXD3D9ShaderEffect : public CGFXShaderEffect {
	D3D9Renderer *renderer;
public:
	CGFXD3D9ShaderEffect(D3D9Renderer *renderer, CGcontext cg_context, const char *file, const char **args);

	virtual void setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const;
	virtual void SetModelViewProjMatrix(const char *parameter) const;
};

class D3D9VertexArray : public VertexArray, public virtual D3D9Item {
	D3D9Renderer *renderer;
	LPDIRECT3DVERTEXBUFFER9 vb;
	LPDIRECT3DINDEXBUFFER9 ib;

	bool init();

	//virtual char *render(bool new_data, int offset, int size) const;
	D3D9VertexArray(D3D9Renderer *renderer, VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim);
public:
	virtual ~D3D9VertexArray();

	static D3D9VertexArray *create(D3D9Renderer *renderer, VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim);

	//virtual void free();
	virtual bool hasVBO() const {
		//return false;
		return data != NULL; // assume always available, as long as there is data?
	}

	virtual void renderVertices(bool new_data, int offset) const;
	virtual void renderNormals(bool new_data, int offset) const;
	virtual void renderTexCoords(bool new_data, int offset) const;
	virtual void renderTexCoords(bool new_data, int offset, int unit) const;
	virtual void renderColors(bool new_data, int offset) const;
	virtual void update() const;
	virtual void updateIndices() const;
	virtual void renderElements(DrawingMode mode, int n_this_renderlength, int n_this_vertices) const;

	virtual void onDeviceLost();
	virtual void onDeviceReset();

	LPDIRECT3DVERTEXBUFFER9 getVB() const {
		return vb;
	}
	LPDIRECT3DINDEXBUFFER9 getIB() const {
		return ib;
	}
};

class D3D9TransformationMatrix : public TransformationMatrix {
	const D3D9Renderer *renderer;
	D3DXMATRIX mat;
public:
	D3D9TransformationMatrix(const D3D9Renderer *renderer) : renderer(renderer) {
	}
	virtual void load() const;
	virtual void save();
	virtual void mult() const;

	virtual void set(int indx, float value) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9TransformationMatrix:: not supported\n")); // need to transpose
		//this->mat[indx] = value;
		//(*this)[indx] = value; // <--- may not work in Release mode? (see comment below for get())
		int row = indx/4;
		int col = indx%4;
		this->mat(row, col) = value;
	}
	virtual float &operator[](const int i) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9TransformationMatrix:: not supported\n")); // need to transpose
		//return mat[i];
		int row = i/4;
		int col = i%4;
		return mat(row, col);
	}
	/*virtual const float &operator[](const int i) const {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9TransformationMatrix:: not supported\n")); // need to transpose
		//return mat[i];
		int row = i/4;
		int col = i%4;
		//LOG("matrix access %d -> %d , %d = %f\n", i, row, col, mat(row, col));
		return mat(row, col);
	}*/
	virtual float get(int indx) const {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9TransformationMatrix:: not supported\n")); // need to transpose
		//return this->mat[indx];
		//return (*this)[indx]; // <--- doesn't work in Release mode!
		int row = indx/4;
		int col = indx%4;
		//LOG("matrix access %d -> %d , %d = %f\n", indx, row, col, mat(row, col));
		return mat(row, col);
	}
	virtual float get(int row, int col) const {
		return mat(row, col);
	}
	virtual Vector3D getPos() const {
		//Vector3D pos(this->mat[3],this->mat[7],this->mat[11]);
		//Vector3D pos(this->mat[12],this->mat[13],this->mat[14]);
		//LOG("%f , %f , %f\n", this->mat(3, 0), this->mat(3, 1), this->mat(3, 2));
		Vector3D pos(this->mat(3, 0), this->mat(3, 1), this->mat(3, 2));
		//LOG("%f , %f, %f\n", pos.x, pos.y, pos.z);
		return pos;
	}
	virtual Vector3D transformVec(const Vector3D &in) const {
		D3DXVECTOR3 din(in.x, in.y, in.z);
		D3DXVECTOR4 dout;
		D3DXVec3Transform(&dout, &din, &mat);
		Vector3D out(dout.x, dout.y, dout.z);
		return out;
	}
};

class D3D9Texture : public Texture, public virtual D3D9Item {
	//LPDIRECT3DDEVICE9 m_pDevice;
	D3D9Renderer *renderer;
	LPDIRECT3DTEXTURE9 m_pTexture;
	bool alpha;
	bool floating_point; // dynamic only
	bool dynamic;
	int dynamic_width, dynamic_height;

	D3D9Texture(D3D9Renderer *renderer, LPDIRECT3DTEXTURE9 pTexture);
public:
	virtual ~D3D9Texture();

	virtual Texture *copy();
	virtual void copyTo(int width, int height);
	LPDIRECT3DTEXTURE9 getD3DTexture() const {
		return m_pTexture;
	}

	virtual void onDeviceLost();
	virtual void onDeviceReset();

	static Texture *create(D3D9Renderer *renderer, int width, int height, bool alpha, bool floating_point);
	static Texture *createFromImage(D3D9Renderer *renderer, VI_Image *image, bool mask, unsigned char maskcol[3]);
	static Texture *loadTexture(D3D9Renderer *renderer, const char *filename, bool mask, unsigned char maskcol[3]);

	// interface
	virtual int getWidth() const;
	virtual int getHeight() const;
	virtual void draw(int x, int y);
	virtual void draw(int x, int y, int w, int h);
	virtual void enable();
	/*virtual void clampToEdge();
	virtual void clampMirror();*/
};

class D3D9FramebufferObject : public FramebufferObject, public virtual D3D9Item {
	D3D9Renderer *renderer;
	LPDIRECT3DSURFACE9 surface;
	LPDIRECT3DTEXTURE9 texture;
	PDIRECT3DSURFACE9 depth_stencil;
	int width, height;
	bool want_depth_stencil;

	bool regenerate();
public:

	D3D9FramebufferObject(D3D9Renderer *renderer);
	virtual ~D3D9FramebufferObject();

	virtual bool generate(int width,int height,bool want_depth_stencil);
	virtual void free();

	virtual void bind() const;
	virtual void enableTexture() const;

	virtual void onDeviceLost();
	virtual void onDeviceReset();
};

class D3D9QuadricObject : public QuadricObject {
	LPD3DXMESH mesh;

	D3D9QuadricObject(LPD3DXMESH mesh) : QuadricObject(), mesh(mesh) {
	}
public:
	virtual ~D3D9QuadricObject() {
		mesh->Release();
	}

	virtual void draw() const {
		mesh->DrawSubset(0);
	}

	static D3D9QuadricObject *createSphere(LPDIRECT3DDEVICE9 device, float radius, int slices, int stacks) {
		LPD3DXMESH mesh;
		D3DXCreateSphere(device, radius, slices, stacks, &mesh, NULL);
		D3D9QuadricObject *qobj = new D3D9QuadricObject(mesh);
		return qobj;
	}
};

class D3D9FontBuffer : public FontBuffer, public virtual D3D9Item {
	LPD3DXFONT font;
	D3D9Renderer *renderer;
public:
	D3D9FontBuffer(D3D9Renderer *renderer,const char *family,int pt,int style);
	virtual ~D3D9FontBuffer();

	virtual size_t memUsage() {
		// TODO:
		return 0;
	}

	virtual void onDeviceLost();
	virtual void onDeviceReset();

	virtual int writeText(int x,int y,const char *text) const;
	virtual int writeText(int x,int y,const char *text,size_t length) const;
	virtual int getWidth(char letter) const;
	virtual int getWidth(const char *text) const;
	virtual int getHeight() const;
};

class D3D9Graphics2D : public Graphics2D {
	D3D9Renderer *renderer;

public:
	D3D9Graphics2D(GraphicsEnvironment *genv, D3D9Renderer *renderer);

	virtual void setColor3i(unsigned char r, unsigned char g, unsigned char b);
	virtual void setColor4i(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	//virtual void setColor(float r,float g,float b) const;
	virtual void plot(short x,short y);
	virtual void draw(short x,short y);
	virtual void draw(short x1,short y1,short x2,short y2) const;
	virtual void drawRect(short x,short y,short width,short height) const;
	virtual void fillRect(short x,short y,short width,short height,const float tu[4], const float tv[4]) const;
	virtual void fillTriangle(const short *x,const short *y) const;
};

class D3D9Renderer : public Renderer {
	friend class D3D9Graphics2D;
	friend class D3D9VertexArray;
	friend class CGD3D9Shader;
	friend class CGFXD3D9ShaderEffect;
	friend class D3D9TransformationMatrix;
	friend class D3D9Texture;
	friend class D3D9FramebufferObject;
	friend class D3D9FontBuffer;

	//set<D3D9FontBuffer *> store_fonts;
	set<D3D9Item *> store;

	LPDIRECT3D9       m_pD3D;
	LPDIRECT3DDEVICE9 m_pDevice;

	//LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PNTC;
	//LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PNT;
	//LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PNC;
	//LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PN;
	//LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PTC;
	//LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PT;
	//LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PC;
	LPDIRECT3DVERTEXDECLARATION9 m_pDecl_P;
	LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PT0C;
	LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PNT0C;
	// multitexturing declarations:
	LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PT0T1C;
	LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PNT0T1C;
	//LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PNT0T2C;
	//LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PNT1C;
	LPDIRECT3DVERTEXDECLARATION9 m_pDecl_PNT0T1T2C;

	LPDIRECT3DVERTEXDECLARATION9 c_pDecl;

	D3DTRANSFORMSTATETYPE c_transform;
	LPD3DXMATRIXSTACK stack_model;
	LPD3DXMATRIXSTACK stack_projection;
	LPD3DXMATRIXSTACK stack_texture[MAX_TEXUNITS];
	LPD3DXMATRIXSTACK c_stack;
	DWORD c_textureunit;
	VertexArray::DrawingMode c_drawing_mode;
	D3DCOLOR clear_color;
	D3DCOLOR c_color;
	LPD3DXSPRITE sprite;
	/*D3D9VertexArray *uniformColorArray;
	bool using_uniform_color;
	D3D9VertexArray *uniformTexcoordArray;
	bool using_uniform_texcoord0;
	bool using_uniform_texcoord1;*/
	D3DVIEWPORT9 saved_viewport;

	bool cull_face, front_face;

	bool shader_clipping;
	float shader_clipping_plane[4];

	enum DataType {
		STREAM_VERTICES = 0,
		STREAM_NORMALS = 1,
		STREAM_COLORS = 2,
		STREAM_TEXCOORDS0 = 3,
		STREAM_TEXCOORDS1 = 4,
		STREAM_TEXCOORDS2 = 5,
		STREAM_TEXCOORDS3 = 6,
		N_STREAMS         = 7
		//N_STREAMS = STREAM_TEXCOORDS0 + MAX_TEXUNITS
	};
	int stream[N_STREAMS];

	void setCulling() {
		//return;
		//cull_face = false;
		if( !cull_face ) {
			this->m_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		}
		else if( front_face ) {
			this->m_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
		}
		else {
			this->m_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
		}
	}

	CGcontext cgContext;
	CGprofile cgVertexProfile;
	CGprofile cgPixelProfile;

	void initCG();
	static void cgErrorCallback(void);

public:

	D3D9Renderer(GraphicsEnvironment *genv, LPDIRECT3D9 pD3D, LPDIRECT3DDEVICE9 pDevice);
	virtual ~D3D9Renderer();

	D3DFORMAT getFormat() const {
		LPDIRECT3DSURFACE9 sourceSurface = NULL;
		this->m_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &sourceSurface);
		D3DSURFACE_DESC desc;
		sourceSurface->GetDesc(&desc);
		sourceSurface->Release();
		return desc.Format;
	}

	virtual void beginScene() const {
		m_pDevice->BeginScene();
	}
	virtual void endScene() const {
		m_pDevice->EndScene();
	}
	virtual void drawFrameTest() const;
	// Called when the device is lost or just reset
	void onDeviceLost();
	void onDeviceReset();

	// factories
	virtual TransformationMatrix *createTransformationMatrix() {
		return new D3D9TransformationMatrix(this);
	}
	//virtual Shader *createShaderCG(const char *file, const char *func, bool vertex_shader);
	virtual Texture *createTexture(int w,int h,bool alpha,bool floating_point) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return NULL;
		return D3D9Texture::create(this, w, h, alpha, floating_point);
	}
	virtual Texture *createTexture(VI_Image *image) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return NULL;
		return D3D9Texture::createFromImage(this, image, false, NULL);
	}
	virtual Texture *createTextureWithMask(VI_Image *image, unsigned char maskcol[3]) {
		return D3D9Texture::createFromImage(this, image, true, maskcol);
	}
	virtual Texture *loadTexture(const char *filename) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return NULL;
		return D3D9Texture::loadTexture(this, filename, false, NULL);
	}
	virtual Texture *loadTextureWithMask(const char *filename, unsigned char maskcol[3]) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return NULL;
		return D3D9Texture::loadTexture(this, filename, true, maskcol);
	}
	virtual FramebufferObject *createFramebufferObject() {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return NULL;
		return new D3D9FramebufferObject(this);
	}
	/*virtual QuadricObject *createQuadricObject() const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		return NULL;
	}*/
	virtual QuadricObject *createSphereObject(float radius, int slices, int stacks) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return NULL;
		return D3D9QuadricObject::createSphere(this->m_pDevice, radius, slices, stacks);
	}
	virtual VertexArray *createVertexArray(VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim) {
		D3D9VertexArray *va = D3D9VertexArray::create(this, data_type, tex_unit, stream, data, size, dim);
		return va;
	}
	virtual FontBuffer *createFont(const char *family,int pt) {
		return createFont(family, pt, 0);
	}
	virtual FontBuffer *createFont(const char *family,int pt,int style) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return NULL;
		return new D3D9FontBuffer(this, family, pt, style);
	}

	// general commands
	virtual void setViewport(int width, int height);
	virtual void resizeScene(float frustum[6][4], int width, int height, bool persp, float z_near, float z_far, float fovy);
	virtual void setClearColor(float r, float g, float b, float a) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		this->clear_color = D3DCOLOR_COLORVALUE(r, g, b, a);
		//this->clear_color = D3DCOLOR_COLORVALUE(r, g, 1.0, a);
	}
	virtual void clear(bool color, bool depth, bool stencil) const;

	// render states
	virtual void setToDefaults();

	virtual void setDepthBufferMode(RenderState state);
	virtual void setDepthBufferFunc(RenderState state);
	virtual void setBlendMode(RenderState state);
	virtual void setAlphaTestMode(RenderState state);
	virtual void setStencilMode(RenderState state);
	virtual void setFogMode(RenderState state, float start, float end, float density) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		// TODO:
	}
	virtual void setFogColor(const float color[4]) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	}

	virtual int nShadowPasses() const {
		return D3D9RendererInfo::twoSidedStencil ? 1 : 2;
	}

	virtual void enableCullFace(bool enable) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return;
		this->cull_face = enable;
		setCulling();
	}
	virtual void setFrontFace(bool ccw) {
		this->front_face = ccw;
		setCulling();
	}
	virtual void enableTexturing(bool enable) {
		if( !enable ) {
			this->m_pDevice->SetTexture(c_textureunit, NULL);
			/*if( this->getActivePixelShader() != NULL ) {
				const CGShader *shader = static_cast<const CGShader *>(this->getActivePixelShader());
				shader->setTexture(NULL);
			}*/
			/*if( this->getActiveShader() != NULL ) {
				this->getActiveShader()->SetUniformParameter1f("textureID", 0.0f);
			}*/
		}
	}
	virtual void enableMultisample(bool enable) {
		this->m_pDevice->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, enable ? TRUE : FALSE);
	}
	virtual void enableColorMask(bool enable) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		/*if( enable ) {
			this->m_pDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
			this->m_pDevice->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_ONE );
			this->m_pDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
		}
		else {
			this->m_pDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
			this->m_pDevice->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_ZERO );
			this->m_pDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
		}*/
		this->m_pDevice->SetRenderState( D3DRS_COLORWRITEENABLE, enable ? (D3DCOLORWRITEENABLE_ALPHA | D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE) : NULL );
	}
	virtual void enableClipPlane(const double pl[4]);
	virtual void reenableClipPlane();
	virtual void disableClipPlane() {
		// not supported for fixed function
		this->shader_clipping = false;
	}
	virtual void enableScissor(int x,int y,int w,int h);
	virtual void reenableScissor();
	virtual void disableScissor();
	//virtual void disableFog() const=0;
	virtual void pushAttrib(bool viewport);
	virtual void popAttrib() const;

	// fixed function only functions
	virtual void setFixedFunctionMatSpecular(const float specular[4]);
	//virtual void setFixedFunctionMatColor(const float color[4]);
	virtual void enableFixedFunctionLighting(bool enable) {
		this->m_pDevice->SetRenderState(D3DRS_LIGHTING, enable ? TRUE : FALSE);
	}
	virtual void setFixedFunctionAmbientLighting(const float col[4]) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		D3DCOLOR d3dcol = D3DCOLOR_COLORVALUE(col[0], col[1], col[2], col[3]);
		this->m_pDevice->SetRenderState(D3DRS_AMBIENT, d3dcol);
		//this->m_pDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(255,0,0));
	}
	virtual void enableFixedFunctionLight(unsigned int index, const float ambient[4], const float diffuse[4], const float specular[4], const float attenuation[3], const float position[3], bool directional);
	virtual void disableFixedFunctionLights() {
		for(unsigned int i=0;i<RendererInfo::maxLights;i++) {
			disableFixedFunctionLight(i);
		}
	}
	virtual void disableFixedFunctionLight(unsigned int index) {
		//LOG("disable light %d\n", index);
		ASSERT( index >= 0 && index < RendererInfo::maxLights );
		this->m_pDevice->LightEnable(index, FALSE);
	}

	//virtual void enableVertexArrays(bool enable, bool vertices, bool normals, bool texcoords, bool colors) const=0;

	// drawing
	/*virtual void setColor3(unsigned char r, unsigned char g, unsigned char b) const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	}*/
	virtual void draw(const Vector3D *p0, const Vector3D *p1, const unsigned char rgba[4]);
	/*virtual void setNormal(float x, float y, float z) const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	}*/
	virtual void render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count) {
		render(mode, vertex_data, texcoord_data, count, NULL, false, 0);
	}
	virtual void render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count, const unsigned char *color_data, bool alpha, int color_repeat);
	//virtual void renderBillboards(int count, Vector3D *offsets, Vector3D tdiagm, Vector3D tdiagp, Vector3D out, unsigned char *color_data, bool alpha, float *sizes, Vector3D normal) const;
	/*virtual void renderBegin(VertexArray::DrawingMode mode) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		this->c_drawing_mode = mode;
	}
	virtual void renderIntermediate(float *vertex_data, float *texcoord_data, int count, unsigned char *color_data, bool alpha, int color_repeat) const {
		// TODO: optimise, or preferably get rid of, as this is very slow on Direct3D!
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		this->render(c_drawing_mode, vertex_data, texcoord_data, count, color_data, alpha, color_repeat);
	}
	virtual void renderEnd() const {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	}*/
	virtual bool supportsQuads() const {
		return false;
	}
	/*virtual void drawPixels(int x, int y,int w, int h, void *data) const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	}*/

	//virtual void startArrays(VertexFormat vertexFormat, bool material_color, bool have_texcoords0, bool have_texcoords1);
	virtual void startArrays(VertexFormat vertexFormat);
	virtual void endArrays() {
		for(int i=0;i<N_STREAMS;i++)
			this->m_pDevice->SetStreamSource(i, NULL, 0, 0);
	}
	/*virtual void endArraysMulti(bool vbos) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		for(int i=0;i<N_STREAMS;i++)
			this->m_pDevice->SetStreamSource(i, NULL, 0, 0);
	}*/

	// texture unit handling
	virtual void activeTextureUnit(int unit) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		this->c_textureunit = unit;
	}
	/*virtual void activeClientTextureUnit(int uint) const {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	}*/

	// transformation matrices
	virtual void switchToModelview() {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		this->c_transform = D3DTS_WORLD;
		this->c_stack = this->stack_model;
	}
	virtual void switchToProjection() {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		this->c_transform = D3DTS_PROJECTION;
		this->c_stack = this->stack_projection;
	}
	virtual void switchToTexture() {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		// TODO: make stack work for multiple texture units?
		this->c_transform = (D3DTRANSFORMSTATETYPE)((int)D3DTS_TEXTURE0 + this->c_textureunit); // assumes enums are sequential!
		this->c_stack = this->stack_texture[this->c_textureunit];
	}
	virtual void ortho2D(int width, int height);
	virtual void pushMatrix() {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return;
		D3DXMATRIX matrix;
		this->m_pDevice->GetTransform(this->c_transform, &matrix);
		this->c_stack->Push();
		this->c_stack->LoadMatrix(&matrix);
	}
	virtual void popMatrix() {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return;
		//D3DXMATRIX cmatrix;
		//this->m_pDevice->GetTransform(this->c_transform, &cmatrix);
		D3DXMATRIX *matrix = this->c_stack->GetTop();
		this->m_pDevice->SetTransform(this->c_transform, matrix);
		//this->m_pDevice->GetTransform(this->c_transform, &cmatrix);
		this->c_stack->Pop();
	}
	virtual void loadIdentity() {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return;
		D3DXMATRIX matrix;
		D3DXMatrixIdentity(&matrix);
		this->m_pDevice->SetTransform(this->c_transform, &matrix);
	}
	virtual void translate(float x,float y,float z) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return;
		D3DXMATRIX matrix;
		D3DXMatrixTranslation(&matrix, x, y, z);
		this->m_pDevice->MultiplyTransform(this->c_transform, &matrix);
	}
	virtual void rotate(float angle,float x,float y,float z) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
		//return;
		D3DXVECTOR3 axis(x, y, z);
		D3DXMATRIX matrix;
		const float factor = - (float)M_PI / 180.0f;
		angle *= factor; // DirectX requires angles in radians; rotation is in opposite direction to glRotatef
		D3DXMatrixRotationAxis(&matrix, &axis, angle);
		this->m_pDevice->MultiplyTransform(this->c_transform, &matrix);
	}
	virtual void scale(float x,float y,float z) {
		D3DXMATRIX matrix;
		D3DXMatrixScaling(&matrix, x, y, z);
		this->m_pDevice->MultiplyTransform(this->c_transform, &matrix);
	}
	virtual void transform(const Vector3D *pos, const Quaternion *rot, bool infinite);
	virtual void transform(const Quaternion *rot);
	virtual void transformInverse(const Quaternion *rot);
	virtual void getViewOrientation(Vector3D *right,Vector3D *up);
	virtual void getViewDirection(Vector3D *dir);

	// frame buffers
	virtual void unbindFramebuffer() const;

	virtual bool isYTextureInverted() const {
		return true;
	}

	// shaders
	virtual bool hasShaders() const {
		return cgContext != NULL;
	}
	virtual Shader *createShaderCG(const char *file, const char *func, bool vertex_shader);
	virtual ShaderEffect *createShaderEffectCGFX(const char *file,const char **args);
	virtual void disableShaders();
};
