//---------------------------------------------------------------------------

#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include "SDL_D3D9GraphicsEnvironment.h"
#include "D3D9Renderer.h"
#include "SoftwareRenderer.h"
#include "../GraphicsEngine.h"
#include "../Texture.h"
#include "../InputHandler.h"

#include <windows.h>
#include <SDL.h>
#include <SDL_syswm.h>

#include <dxerr.h>

#include <sstream>
using std::stringstream;

#pragma comment(lib, "dxerr.lib")

//---------------------------------------------------------------------------

SDL_D3D9GraphicsEnvironment::SDL_D3D9GraphicsEnvironment(VisionPrefs visionPrefs) : SDL_GraphicsEnvironment(visionPrefs) {
	this->m_pD3D = NULL;
	this->m_pDevice = NULL;
	this->m_bDeviceLost = false;
	ZeroMemory( &d3dpp, sizeof(d3dpp) );

	bool software = visionPrefs.api == VISIONAPI_SOFTWARE;
	bool want_shaders = visionPrefs.shaders;
	LOG("startup SDL Direct3D 9 GraphicsEnvironment\n");
	RendererInfo::init();

	if( !visionPrefs.fullscreen ) {
		//putenv("SDL_VIDEO_WINDOW_POS=0,0");
		putenv("SDL_VIDEO_CENTERED=0,0");
	}
	if( SDL_Init(SDL_INIT_VIDEO) != 0 ) {
		free();
		Vision::setError(new VisionException(this,VisionException::V_FAILED_TO_OPEN_WINDOW,"Could not initialise SDL video"));
		return;
	}
	if( SDL_GetVideoInfo() == NULL ) {
		free();
		Vision::setError(new VisionException(this,VisionException::V_FAILED_TO_OPEN_WINDOW,"Could not retreive SDL video info"));
		return;
	}

	Uint32 sdl_flags = SDL_HWSURFACE;
	if( visionPrefs.fullscreen )
		sdl_flags = sdl_flags | SDL_FULLSCREEN;
	LOG("open window %d X %d\n", visionPrefs.resolution_width, visionPrefs.resolution_height);
	LOG("fullscreen? %d\n", visionPrefs.fullscreen);
	LOG("multisample? %d\n", visionPrefs.multisample);
	if( SDL_SetVideoMode(visionPrefs.resolution_width, visionPrefs.resolution_height, 32, sdl_flags) == NULL ) {
	//if( SDL_SetVideoMode( 800, 600, SDL_GetVideoInfo()->vfmt->BitsPerPixel, SDL_RESIZABLE ) == NULL ) {
		free();
		Vision::setError(new VisionException(this,VisionException::V_FAILED_TO_OPEN_WINDOW,"Failed to set SDL video mode"));
		return;
	}
	/*{
		// test
		free();
		Vision::setError(new VisionException(this,VisionException::V_FAILED_TO_OPEN_WINDOW,"Failed to set SDL video mode"));
		return;
	}*/
	LOG("SDL window created okay\n");

	//SDL_EventState(SDL_SYSWMEVENT, SDL_ENABLE);

	//SDL_WM_SetCaption("Vision 3 - Initialising", "");
	stringstream str;
	str << Vision::getApplicationName() << " - Initialising";
	SDL_WM_SetCaption(str.str().c_str(), "");

	m_pD3D = Direct3DCreate9( D3D_SDK_VERSION );
	LOG("D3D %x\n", m_pD3D);
	if( m_pD3D == NULL ) {
		free();
		Vision::setError(new VisionException(this,VisionException::V_FAILED_TO_INIT_3D,"Could not create Direct3D 9 object"));
		return;
	}
	LOG("initialised Direct3D 9 OK\n");
	D3DADAPTER_IDENTIFIER9 adapterIdentifier;
	m_pD3D->GetAdapterIdentifier(D3DADAPTER_DEFAULT, 0, &adapterIdentifier);
	LOG("D3D Driver: %s\n", adapterIdentifier.Driver);
	LOG("D3D Description: %s\n", adapterIdentifier.Description);
	LOG("D3D Device Name: %s\n", adapterIdentifier.DeviceName);
	LOG("D3D Vendor ID: %d\n", adapterIdentifier.VendorId);
	LOG("D3D Device ID: %d\n", adapterIdentifier.DeviceId);
	LOG("D3D SubSys ID: %d\n", adapterIdentifier.SubSysId);
	LOG("D3D Revision: %d\n", adapterIdentifier.Revision);
	LOG("D3D WHQLLevel: %d\n", adapterIdentifier.WHQLLevel);
	LOG("D3D Product: %d\n", HIWORD(adapterIdentifier.DriverVersion.HighPart));
	LOG("D3D Version: %d\n", LOWORD(adapterIdentifier.DriverVersion.HighPart));
	LOG("D3D SubVersion: %d\n", HIWORD(adapterIdentifier.DriverVersion.LowPart));
	LOG("D3D Build: %d\n", LOWORD(adapterIdentifier.DriverVersion.LowPart));

	//D3DFORMAT backBufferFormat = visionPrefs.fullscreen ? D3DFMT_A8R8G8B8 : D3DFMT_UNKNOWN;
	D3DFORMAT backBufferFormat = D3DFMT_A8R8G8B8;
	D3DFORMAT depthstencilFormat = D3DFMT_D24S8;
	bool multisample_available = false;
	D3DMULTISAMPLE_TYPE multisample_type = D3DMULTISAMPLE_NONE;
	if( visionPrefs.multisample == 0 ) {
		// no multisampling
	}
	else if( visionPrefs.multisample == 2 )
		multisample_type = D3DMULTISAMPLE_2_SAMPLES;
	else if( visionPrefs.multisample == 4 )
		multisample_type = D3DMULTISAMPLE_4_SAMPLES;
	else if( visionPrefs.multisample == 8 )
		multisample_type = D3DMULTISAMPLE_8_SAMPLES;
	else {
		free();
		Vision::setError(new VisionException(this, VisionException::V_PREFS_ERROR, "prefs: unrecognised multisample value requested"));
	}
	DWORD multisample_levels = 0;

	if( visionPrefs.multisample == 0 ) {
		LOG("multisampling not requested\n");
	}
	else if( SUCCEEDED( m_pD3D->CheckDeviceMultiSampleType( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, backBufferFormat, !visionPrefs.fullscreen, multisample_type, &multisample_levels ) ) ) {
		if( SUCCEEDED( m_pD3D->CheckDeviceMultiSampleType( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, depthstencilFormat, !visionPrefs.fullscreen, multisample_type, &multisample_levels ) ) ) {
			multisample_available = true;
			LOG("multisampling available: %d levels\n", multisample_levels);
		}
		else {
			LOG("no multisampling - incompatible depthstencil buffer:(\n");
		}
	}
	else {
		LOG("no multisampling - incompatible back buffer:(\n");
	}

	D3DCAPS9 caps;
	m_pD3D->GetDeviceCaps( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps );
	LOG("obtained device caps OK\n");
	if( caps.MaxActiveLights <= 0 ) {
		// this apparently implies no limit (due to being done in software)
		// set an arbitrary limit that's good enough
		RendererInfo::maxLights = 8;
		LOG("No limit on max Lights (software mode) - set max equal to %d\n", RendererInfo::maxLights);
	}
	else {
		RendererInfo::maxLights = static_cast<unsigned int>(caps.MaxActiveLights);
		LOG("Max Lights? %d\n", RendererInfo::maxLights);
	}

	if( caps.StencilCaps & D3DSTENCILCAPS_TWOSIDED ) {
		D3D9RendererInfo::twoSidedStencil = true;
	}
	LOG("Two sided stencil? %s\n", D3D9RendererInfo::twoSidedStencil?"T":"F");
	if( D3D9RendererInfo::twoSidedStencil && !visionPrefs.twosidedstencil ) {
		D3D9RendererInfo::twoSidedStencil = false;
		LOG(">>> Disabling two sided stencil\n");
	}

	if( caps.Caps2 & D3DCAPS2_CANAUTOGENMIPMAP ) {
		LOG("can autogenerate mipmaps\n");
	}
	else {
		LOG("can't autogenerate mipmaps\n");
	}

	RendererInfo::framebufferObject = true; // TODO: assume true for now?

#ifdef _WIN32
	//this->hWnd = GetActiveWindow();
	SDL_SysWMinfo wmInfo;
	SDL_VERSION(&wmInfo.version);
	SDL_GetWMInfo(&wmInfo);
	this->hWnd = wmInfo.window;
	LOG("hWnd: %d\n", hWnd);
#endif

	d3dpp.Windowed = !visionPrefs.fullscreen;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.EnableAutoDepthStencil = true;
	d3dpp.AutoDepthStencilFormat = depthstencilFormat;
	//d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.hDeviceWindow = hWnd;
	d3dpp.BackBufferFormat = backBufferFormat;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE; // no vsync
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferWidth = visionPrefs.resolution_width;
	d3dpp.BackBufferHeight = visionPrefs.resolution_height;
	if( software ) {
		d3dpp.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER; // needed for saveScreen() too? but can't use with multisampling!
	}
	if( multisample_available ) {
		d3dpp.MultiSampleType = multisample_type;
	}
	/*d3dpp.BackBufferWidth = WindowRect.right-WindowRect.left;
	d3dpp.BackBufferHeight = WindowRect.bottom-WindowRect.top;*/
	/*if( fullscreen ) {
		d3dpp.BackBufferWidth = WindowRect.right-WindowRect.left;
		d3dpp.BackBufferHeight = WindowRect.bottom-WindowRect.top;
	}
	else {
		// wrong!
		d3dpp.BackBufferWidth = GetSystemMetrics(SM_CXSCREEN);
		d3dpp.BackBufferHeight = GetSystemMetrics(SM_CYSCREEN);
	}*/

	DWORD behaviour;
	if ( caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT ) {
		LOG("hardware transform and lighting available\n");
		behaviour = D3DCREATE_HARDWARE_VERTEXPROCESSING;
		if( !visionPrefs.hardwaretl ) {
			behaviour = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
			LOG(">>> Disabling hardware transform and lighting\n");
		}
	}
	else {
		LOG("hardware transform and lighting not available :( using software mode\n");
		behaviour = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}

	this->m_pDevice = NULL;
	if( FAILED( m_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		behaviour,
		&d3dpp, &m_pDevice ) ) )
	{
		free();
		Vision::setError(new VisionException(this,VisionException::V_FAILED_TO_INIT_3D,"Could not create Direct3D 9 Device"));
		return;
	}
	LOG("D3D Device %x\n", m_pDevice);
	LOG("created device OK\n");

	this->g3 = new Graphics3D(this, visionPrefs.hdr);
	if( software ) {
		this->renderer = new SoftwareRenderer(this, m_pD3D, m_pDevice);
		this->g = new SoftwareGraphics2D(this, static_cast<SoftwareRenderer *>(renderer));
	}
	else {
		this->renderer = new D3D9Renderer(this, m_pD3D, m_pDevice);
		this->g = new D3D9Graphics2D(this, static_cast<D3D9Renderer *>(renderer));
	}
	//this->g->owned = true; // owned by this GraphicsEnvironment class
	this->addChild(this->g);
	this->inputSystem = new InputSystem(new MouseInputHandler(), new KeyboardInputHandler());
	//this->inputSystem = new InputSystem(new Win32MouseInputHandler(), new KeyboardInputHandler());
	LOG("created subsystems okay OK\n");

	/*if( want_shaders && !software ) {
		this->initCG();
	}*/
	try {
		g3->init(); // also inits shaders
		// need try/catch block to avoid memory leak (e.g., if CG error, an exception is throw!)
	}
	catch(...) {
		LOG("exception thrown initialising graphics engine\n");
		free();
		throw;
	}

	// we do this, as the code expects there to be a projection matrix (for the 3D viewing mode) on the stack
	this->renderer->switchToProjection();
	this->renderer->pushMatrix();
	this->renderer->switchToModelview();

	g3->ReSizeGLScene(renderer->getZNear(), renderer->getZFar(), visionPrefs.resolution_width, visionPrefs.resolution_height, renderer->getPersp(), fovy); // need to reset viewport/perspective info

	this->initSDL();

	this->multisample_ok = visionPrefs.multisample > 0; // if we're here and it was requested, it must have been successful

	//this->created = true;

	LOG("all done!\n");
}

SDL_D3D9GraphicsEnvironment::~SDL_D3D9GraphicsEnvironment() {
	free();
}

void SDL_D3D9GraphicsEnvironment::free() {
	closeScreen();

	/*if( g != NULL ) {
		delete g;
		g = NULL;
	}*/
	// g is deleted automatically as its owned by this class
	if( g3 != NULL ) {
		delete g3;
		g3 = NULL;
	}
	if( renderer != NULL ) {
		delete renderer;
		renderer = NULL;
	}
	if( inputSystem != NULL ) {
		delete inputSystem;
		inputSystem = NULL;
	}

	/*if( cgContext != NULL ) {
		cgD3D9SetDevice(NULL); // needed to avoid hang on exit, on Intel GMA 950
		cgDestroyContext(cgContext);
		cgContext = NULL;
	}*/
}

void SDL_D3D9GraphicsEnvironment::closeScreen() {
	this->sound = NULL; // clear reference to block calls to it, as it may have been deleted by this stage

	if( m_pDevice != NULL ) {
		m_pDevice->Release();
		m_pDevice = NULL;
	}

	if( m_pD3D != NULL ) {
		m_pD3D->Release();
		m_pD3D = NULL;
	}

	/*if(visionPrefs.fullscreen) {
		ChangeDisplaySettings(NULL,0);
		//ShowCursor(TRUE);
	}*/

	SDL_Quit();
}

/*void SDL_D3D9GraphicsEnvironment::cgErrorCallback(void) {
	CGerror error = cgGetError();
	LOG("CG ERROR %d : %s\n", error, cgGetErrorString(error));
	const char *lastError = cgGetLastErrorString(NULL);
	if( lastError != NULL ) {
		LOG("Last Error String: %s\n", lastError);
	}
	SDL_D3D9GraphicsEnvironment *genv = static_cast<SDL_D3D9GraphicsEnvironment *>(GraphicsEnvironment::getSingleton());
	const char *lastListing = cgGetLastListing(genv->cgContext);
	if( lastListing != NULL ) {
		LOG("Last Listing String: %s\n", lastListing);
	}
	Vision::setError(new VisionException(NULL,VisionException::V_CG_ERROR,"CG reported an error"));
}

void SDL_D3D9GraphicsEnvironment::initCG() {
	// setup CG
	checkError("SDL_D3D9GraphicsEnvironment::initCG enter");

	cgSetErrorCallback(cgErrorCallback);

	cgContext = cgCreateContext();
	if( cgContext != NULL ) {
		LOG("Successfully initialised CG Context\n");

		cgD3D9SetDevice(m_pDevice);
		cgVertexProfile = cgD3D9GetLatestVertexProfile();
		if( cgVertexProfile == CG_PROFILE_UNKNOWN ) {
			LOG("### CG ERROR: Invalid vertex profile type\n");
			cgDestroyContext(cgContext);
			cgContext = NULL;
		}
		else {
			LOG("Successfully obtained vertex profile\n");
			//cgGLSetOptimalOptions(cgVertexProfile);

			cgPixelProfile = cgD3D9GetLatestPixelProfile();
			if( cgPixelProfile == CG_PROFILE_UNKNOWN ) {
				LOG("### CG ERROR: Invalid pixel profile type\n");
				cgDestroyContext(cgContext);
				cgContext = NULL;
			}
			else {
				LOG("Successfully obtained pixel profile\n");
				//cgGLSetOptimalOptions(cgPixelProfile);
			}
		}
	}
	else {
		LOG("### Failed to initialise CG Context\n");
	}

	//cgGLSetDebugMode(CG_FALSE);
	checkError("SDL_D3D9GraphicsEnvironment::initCG exit");
}*/

/*bool SDL_D3D9GraphicsEnvironment::hasShaders() const {
	//return cgContext != NULL;
	//return this->renderer->hasShaders();
	const D3D9Renderer *d_renderer = static_cast<const D3D9Renderer *>(this->renderer);
	return d_renderer->hasShaders();
}*/

/*Shader *SDL_D3D9GraphicsEnvironment::createShader(const char *file, const char *func, bool vertex_shader) {
	if( !this->hasShaders() )
		return NULL;
	CGD3D9Shader *shader = new CGD3D9Shader(static_cast<D3D9Renderer *>(renderer), vertex_shader);
	LOG("Loading %s shader %d : %s / %s\n", vertex_shader?"vertex":"pixel", shader, file, func);
	shader->cgProfile = vertex_shader ? static_cast<D3D9Renderer *>(renderer)->cgVertexProfile : static_cast<D3D9Renderer *>(renderer)->cgPixelProfile;

	const char **profileOpts = cgD3D9GetOptimalOptions(shader->cgProfile);

	shader->cgProgram = cgCreateProgramFromFile(static_cast<D3D9Renderer *>(renderer)->cgContext, CG_SOURCE, file, shader->cgProfile, func, profileOpts);
	if( shader->cgProgram == NULL ) {
		LOG("### Failed to load %s shader:\n", vertex_shader?"vertex":"pixel");
		CGerror Error = cgGetError();
		LOG(cgGetErrorString(Error));
		LOG("\n");
		delete shader;
		//this->cgContext = NULL;
		return NULL;
	}
	cgD3D9LoadProgram(shader->cgProgram, FALSE, 0);
	shader->init();
	//g3->addShader(shader);

	return shader;
}*/

void SDL_D3D9GraphicsEnvironment::swapBuffers() {
	this->renderer->endScene();
	HRESULT hResult = this->m_pDevice->Present(NULL, NULL, NULL, NULL);
	if( FAILED( hResult ) ) {
		LOG("swapBuffers lost device\n");
		if( !handlePresentRetVal(hResult) ) {
			LOG("failed to recover!\n");
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_LOSTDEVICE, "swapBuffers lost device"));
		}
		else {
			LOG("...recovered okay\n");
		}
	}
}

bool SDL_D3D9GraphicsEnvironment::saveScreen(const char *filename,const char *type) {
	// we can't lock a surface unless we set the flag D3DPRESENTFLAG_LOCKABLE_BACKBUFFER - but that can't be used with multisampling
	// so we use D3DXSaveTextureToFile instead
	LPDIRECT3DSURFACE9 sourceSurface = NULL;
	if( FAILED( this->m_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &sourceSurface) ) ) {
		LOG("failed to get back buffer\n");
		return false;
	}

	bool ok = false;
	D3DXIMAGE_FILEFORMAT format = D3DXIFF_PNG;
	if( stricmp(type, "bmp") )
		format = D3DXIFF_BMP;
	else if( stricmp(type, "jpg") )
		format = D3DXIFF_JPG;
	else if( stricmp(type, "tga") )
		format = D3DXIFF_TGA;
	else if( stricmp(type, "png") )
		format = D3DXIFF_PNG;
	else if( stricmp(type, "dds") )
		format = D3DXIFF_DDS;
	else if( stricmp(type, "ppm") )
		format = D3DXIFF_PPM;
	else if( stricmp(type, "dib") )
		format = D3DXIFF_DIB;
	else if( stricmp(type, "hdr") )
		format = D3DXIFF_HDR;
	else if( stricmp(type, "pfm") )
		format = D3DXIFF_PFM;
	else {
		Vision::setError(new VisionException(this, VisionException::V_IMAGE_UNSUPPORTED_FILEFORMAT, "saveScreen: unrecognised or unsupported file format requested\n"));
	}
	if( FAILED( D3DXSaveSurfaceToFileA(filename, format, sourceSurface, NULL, NULL) ) ) {
		LOG("failed to save surface\n");
	}
	else {
		ok = true;
	}
	sourceSurface->Release();
	return ok;
}

bool SDL_D3D9GraphicsEnvironment::drawFrameStart() {
	HRESULT hResult = m_pDevice->TestCooperativeLevel();
	if( FAILED(hResult) ) {
		/*if(!HandlePresentRetVal(hResult))
			m_bShouldExit = true;
		return;*/
		LOG("drawFrameStart lost device\n");
		if( !handlePresentRetVal(hResult) ) {
			LOG("failed to recover!\n");
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_LOSTDEVICE, "drawFrameStart lost device"));
		}
		else {
			LOG("...recovered okay\n");
		}
		return false;
	}
	return true;
}

bool SDL_D3D9GraphicsEnvironment::handlePresentRetVal(HRESULT hResult) {
	if( hResult == D3DERR_DEVICELOST ) {
		LOG("D3DERR_DEVICELOST\n");
		// we now leave calls to onDeviceLost() to resetDevice() - otherwise we have the problem where the application may have created D3D9 objects after this point, but before we receive D3DERR_DEVICENOTRESET!
		/*if( !m_bDeviceLost ) {
			onDeviceLost();
			m_bDeviceLost = true;
		}*/
	}
	else if( hResult == D3DERR_DEVICENOTRESET ) {
		LOG("D3DERR_DEVICENOTRESET\n");
		if( !resetDevice() )
			return false;
		m_bDeviceLost = false;
	}
	else if( hResult == D3DERR_DRIVERINTERNALERROR ) {
		LOG("D3DERR_DRIVERINTERNALERROR\n");
		// DX Docs state that we should try resetting
		/*if( !resetDevice() )
			return false;*/
		// TODO:
		return false;
	}
	else if( FAILED(hResult) ) {
		LOG("Failed: %s\n", DXGetErrorString(hResult));
		return false;
	}

	return true;
}

bool SDL_D3D9GraphicsEnvironment::resetDevice() {
	LOG("SDL_D3D9GraphicsEnvironment::resetDevice()\n");
	// Make sure we call OnDeviceLost to free anything before device is reset
	if( !m_bDeviceLost ) {
		onDeviceLost();
	}

	// Reset device
	LOG("about to reset device...\n");
	HRESULT hResult = m_pDevice->Reset(&d3dpp);
	if( FAILED( hResult ) )	{
		LOG("Failed to reset! Error: %s\n", DXGetErrorString(hResult));
		return false;
	}
	LOG("done\n");

	// TODO:
	// Update window style in case we changed between windowed and fullscreen
	/*if(m_thePresentParams.Windowed)
	{
		SetWindowLong(m_hWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);
		SetWindowPos(m_hWnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	}
	else
	{
		SetWindowLong(m_hWnd, GWL_STYLE, WS_POPUPWINDOW);
		SetWindowPos(m_hWnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	}*/

	// Device is no longer lost
	onDeviceReset();
	// TODO:
	//SetupState();
	m_bDeviceLost = false;
	return true;
}

void SDL_D3D9GraphicsEnvironment::onDeviceLost() {
	LOG("SDL_D3D9GraphicsEnvironment::onDeviceLost()\n");
	// The device is lost, or is about to be reset. This is the time to Release() any resources
	// in the default pool, and call OnLostDevice() for any D3DX objects that need it.
	if( visionPrefs.api == VISIONAPI_SOFTWARE ) {
		return;
	}
	D3D9Renderer *d_renderer = static_cast<D3D9Renderer *>(renderer);
	d_renderer->onDeviceLost();
	LOG("SDL_D3D9GraphicsEnvironment::onDeviceLost() done\n");
}

void SDL_D3D9GraphicsEnvironment::onDeviceReset() {
	LOG("SDL_D3D9GraphicsEnvironment::onDeviceReset()\n");
	// The device has just been reset. This is the time to recreate() any resources in the default
	// pool, and call OnResetDevice() for any D3DX objects that need it.
	if( visionPrefs.api == VISIONAPI_SOFTWARE ) {
		return;
	}
	D3D9Renderer *d_renderer = static_cast<D3D9Renderer *>(renderer);
	//Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "about to reset!"));
	d_renderer->onDeviceReset();
	LOG("SDL_D3D9GraphicsEnvironment::onDeviceReset() done\n");
}
