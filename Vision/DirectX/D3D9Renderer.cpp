//---------------------------------------------------------------------------

#include <cassert>
#include <tchar.h>

#include "D3D9Renderer.h"
#include "../GraphicsEnvironment.h"
#include "../Texture.h"
#include "../Misc.h"

#include <cg/cgD3D9.h>

//---------------------------------------------------------------------------

bool D3D9RendererInfo::twoSidedStencil = false;

#if 0
// for test rendering:
/*struct CUSTOMVERTEX
{
	FLOAT x, y, z, rhw; // The transformed position for the vertex.
	DWORD color;        // The vertex color.
};
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW|D3DFVF_DIFFUSE)

CUSTOMVERTEX g_test_vertices[] =
{
	{ 150.0f,  50.0f, 0.5f, 1.0f, 0xffff0000, }, // x, y, z, rhw, color
	{ 250.0f, 250.0f, 0.5f, 1.0f, 0xff00ff00, },
	{  50.0f, 250.0f, 0.5f, 1.0f, 0xff00ffff, },
};*/
//

struct CUSTOMVERTEX
{
	FLOAT x, y, z; // The transformed position for the vertex.
	DWORD color;        // The vertex color.
};
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)
//#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ)

CUSTOMVERTEX g_test_vertices[] =
{
	{ 150.0f,  50.0f, 0.5f, 0xffff0000, }, // x, y, z, color
	//{ 0.0f,  0.0f, 0.5f, 0xffff0000, }, // x, y, z, color
	{ 250.0f, 250.0f, 0.5f, 0xff00ff00, },
	{  50.0f, 250.0f, 0.5f, 0xff00ffff, },
};
/*{
    { -1.0f,-1.0f, 10.0f, 0xffff0000, },
    {  1.0f,-1.0f, 10.0f, 0xff0000ff, },
    {  0.0f, 1.0f, 100.0f, 0xffffffff, },
};*/
/*{
    { -1.0f,-1.0f, 0.0f, },
    {  3.0f,-1.0f, 0.0f, },
    {  10.0f, 1.0f, 0.0f },
};*/
/*{
    { -1.0f,-1.0f, 0.0f, 0xffff0000, },
    {  1.0f,-1.0f, 0.0f, 0xff0000ff, },
    {  0.0f, 1.0f, 0.0f, 0xffffffff, },
};*/
unsigned short g_test_indices[] =
{
	0, 1, 2
};

#endif

struct VERTEX2D
{
	float x, y, z;
	DWORD color;
};
#define D3DFVF_VERTEX2D (D3DFVF_XYZ|D3DFVF_DIFFUSE)

struct VERTEX2D_TEXTURED
{
	float x, y, z;
	DWORD color;
	float u, v;
};
#define D3DFVF_VERTEX2D_TEXTURED (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1)

struct VERTEX
{
	float x, y, z;
	DWORD color;
};
#define D3DFVF_VERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)

struct VERTEX_TEXTURED
{
	float x, y, z;
	DWORD color;
	float u, v;
};
//#define D3DFVF_VERTEX (D3DFVF_XYZ)
//#define D3DFVF_VERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)
#define D3DFVF_VERTEX_TEXTURED (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1)

D3D9VertexArray::D3D9VertexArray(D3D9Renderer *renderer, VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim) {
	//LOG("D3D9VertexArray()\n");
	this->renderer = renderer;
	this->data_type = data_type;
	this->tex_unit = tex_unit;
	this->stream = stream;
	this->data = data;
	this->size = size;
	this->dim = dim;
	this->ib = NULL;
	this->vb = NULL;
	renderer->store.insert(this);
}

D3D9VertexArray::~D3D9VertexArray() {
	if( ib != NULL )
		ib->Release();
	if( vb != NULL )
		vb->Release();
	renderer->store.erase(this);
}

bool D3D9VertexArray::init() {
	// creates the vertex/index buffer, and copies over the data
	if( data != NULL ) {
		ASSERT( size > 0 );
	}
	bool ok = true;
	D3D9VertexArray *va = this;
	//DWORD usage = 0;
	DWORD usage = D3DUSAGE_WRITEONLY; // needed to avoid performance penalty - "Vertexbuffer created with POOL_DEFAULT but WRITEONLY not set. Performance penalty could be severe."
	D3DPOOL pool = D3DPOOL_DEFAULT; // original
	//D3DPOOL pool = D3DPOOL_MANAGED;
	if( stream ) {
		usage = usage | D3DUSAGE_DYNAMIC; // original
	}
	int local_size = size;
	//usage = 0;
	if( data == NULL ) {
		// no data
	}
	else if( data_type == DATATYPE_INDICES ) {
		if( FAILED( renderer->m_pDevice->CreateIndexBuffer( size, usage, D3DFMT_INDEX16, pool, &va->ib, NULL ) ) ) {
			ok = false;
		}
	}
	/*else if( data_type == DATATYPE_VERTICES ) {
		// test
		if( FAILED( renderer->m_pDevice->xBuffer( 3*sizeof(CUSTOMVERTEX),
			0, D3DFVF_CUSTOMVERTEX, pool, &va->vb, NULL ) ) )
			ok = false;
	}*/
	else {
		/*int n_verts = size / (3*sizeof(float));
		float *vert_data = (float *)data;
		for(int i=0,c=0;i<n_verts;i++) {
			for(int j=0;j<3;j++) {
				LOG("%f ", vert_data[c++]);
			}
			LOG("\n");
		}*/
		//DWORD fvf = 0;
		if( data_type == DATATYPE_VERTICES ) {
			//fvf = D3DFVF_XYZ;
		}
		else if( data_type == DATATYPE_NORMALS ) {
			//fvf = D3DFVF_NORMAL;
		}
		else if( data_type == DATATYPE_COLORS ) {
			//LOG("colour va %d : dim %d, size %d, mod %d\n", this, dim, size, size % dim);
			//fvf = D3DFVF_DIFFUSE;
			if( dim != 3 && dim != 4 ) {
				Vision::setError(new VisionException(NULL, VisionException::V_RENDER_ERROR, "DATATYPECOLORS must have dim 3 or 4"));
			}
			ASSERT( local_size % dim == 0 );
			// always convert to 4 byte format in D3D9
			local_size /= dim;
			local_size *= 4;
		}
		else if( data_type == DATATYPE_TEXCOORDS ) {
			//fvf = D3DFVF_TEX1; // TODO: does texture unit matter?
		}
		else {
			Vision::setError(new VisionException(NULL, VisionException::V_RENDERER_INVALID_STATE, "unknown data type\n"));
		}
		//if( FAILED( renderer->m_pDevice->CreateVertexBuffer( local_size, usage, fvf, pool, &va->vb, NULL ) ) ) {
		if( FAILED( renderer->m_pDevice->CreateVertexBuffer( local_size, usage, 0, pool, &va->vb, NULL ) ) ) {
			ok = false;
		}
	}

	/*if( ok && data != NULL && data_type == DATATYPE_VERTICES ) {
		// test
		VOID *ptr = NULL;
		if( FAILED( va->vb->Lock( 0, sizeof(g_test_vertices), (void**)&ptr, 0 ) ) ) {
			ok = false;
		}
		else {
			memcpy( ptr, g_test_vertices, sizeof(g_test_vertices) );
			va->vb->Unlock();
		}
	}
	else*/
	if( ok && data != NULL ) {
		VOID *ptr = NULL;
		if( data_type == DATATYPE_INDICES ) {
			if( FAILED( va->ib->Lock( 0, size, (void**)&ptr, 0 ) ) ) {
				ok = false;
			}
		}
		else {
			if( FAILED( va->vb->Lock( 0, local_size, (void**)&ptr, 0 ) ) ) {
				ok = false;
			}
		}
		if( ok ) {
			if( data_type == DATATYPE_COLORS ) {
				// now local_size is already in 4 byte format
				DWORD *d3ddata = new DWORD[local_size/4];
				unsigned char *srcdata = (unsigned char *)data;
				for(int i=0;i<local_size/4;i++) {
					unsigned char alpha = (dim == 4) ? srcdata[3] : 255;
					d3ddata[i] = D3DCOLOR_RGBA(srcdata[0],srcdata[1],srcdata[2],alpha);
					//d3ddata[i] = D3DCOLOR_RGBA(127, 127, 127, 255);
					//d3ddata[i] = 0xffff0000;
					//d3ddata[i] = 0xffffffff;
					srcdata += dim;
				}
				memcpy( ptr, d3ddata, local_size );
				delete [] d3ddata;
			}
			else {
				memcpy( ptr, data, local_size );
			}
			if( data_type == DATATYPE_INDICES ) {
				va->ib->Unlock();
			}
			else {
				va->vb->Unlock();
			}
		}
	}
	return ok;
}

D3D9VertexArray *D3D9VertexArray::create(D3D9Renderer *renderer, VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim) {
	//LOG("D3D9VertexArray::create()\n");
	D3D9VertexArray *va = new D3D9VertexArray(renderer, data_type, tex_unit, stream, data, size, dim);

	bool ok = va->init();

	if( !ok ) {
		delete va;
		va = NULL;
		Vision::setError(new VisionException(NULL, VisionException::V_DIRECT3D_ERROR, "D3D9VertexArray::create() failed\n"));
	}
	//LOG("    done\n");
	return va;
}

void D3D9VertexArray::renderVertices(bool new_data, int offset) const {
	if( new_data ) {
		ASSERT( stream );
		VOID *ptr = NULL;
		if( FAILED( this->vb->Lock( 0, size, (void**)&ptr, D3DLOCK_DISCARD ) ) ) {
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9VertexArray::renderVertices failed to lock buffer for new data\n"));
		}
		else {
			/*{
				Vector3D *vxs = (Vector3D *)data;
				for(int i=0;i<size/sizeof(Vector3D);i++) {
					LOG("%d : %f, %f, %f\n", i, vxs[i].x, vxs[i].y, vxs[i].z);
					//vxs[i].y -= 10.0;
				}
			}*/
			memcpy( ptr, data, size );
			/*float *dst = (float *)ptr;
			float *src = (float *)data;
			for(int i=0;i<size/sizeof(float);i++) {
				dst[i] = src[i];
				//dst[i] = 0.0f;
			}*/
			this->vb->Unlock();
		}
	}
	if( FAILED( this->renderer->m_pDevice->SetStreamSource( this->renderer->stream[D3D9Renderer::STREAM_VERTICES], this->vb, offset, sizeof(D3DXVECTOR3) ) ) ) {
		Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "SetStreamSource failed"));
	}
	// test
	//this->renderer->m_pDevice->SetStreamSource( 0, this->vb, 0, sizeof(CUSTOMVERTEX) );

	// test code
	/*const D3DXVECTOR3 *vertices = (const D3DXVECTOR3 *)this->data;
	for(int i=0;i<4;i++) {
		D3DXMATRIX transform_model, transform_projection;
		this->renderer->m_pDevice->GetTransform(D3DTS_WORLD, &transform_model);
		this->renderer->m_pDevice->GetTransform(D3DTS_PROJECTION, &transform_projection);
		D3DXVECTOR4 vert, dummy;
		D3DXVec3Transform(&dummy, &vertices[i], &transform_model);
		D3DXVec4Transform(&vert, &dummy, &transform_projection);
		LOG("");
	}*/
}

void D3D9VertexArray::renderNormals(bool new_data, int offset) const {
	//return;
	// TODO: new data
	if( new_data ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderNormals new_data not supported\n"));
	}
	if( this->renderer->stream[D3D9Renderer::STREAM_NORMALS] >= 0 ) {
		if( FAILED( this->renderer->m_pDevice->SetStreamSource( this->renderer->stream[D3D9Renderer::STREAM_NORMALS], this->vb, offset, sizeof(D3DXVECTOR3) ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "SetStreamSource failed"));
		}
	}
}

void D3D9VertexArray::renderTexCoords(bool new_data, int offset) const {
	if( new_data ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderTexCoords new_data not supported\n"));
	}
	if( this->renderer->stream[D3D9Renderer::STREAM_TEXCOORDS0] >= 0 ) {
		//this->renderer->m_pDevice->SetStreamSource( this->renderer->stream[D3D9Renderer::STREAM_TEXCOORDS0], this->vb, offset, sizeof(D3DXVECTOR2) );
		/*if( this->vb == this->renderer->uniformTexcoordArray->vb ) {
			Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderTexCoords shouldn't call on uniform texcoord array\n"));
		}
		else if( this->renderer->using_uniform_texcoord0 ) {
			Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderTexCoords shouldn't call when using uniform texcoord array\n"));
		}
		else*/ if( FAILED( this->renderer->m_pDevice->SetStreamSource( this->renderer->stream[D3D9Renderer::STREAM_TEXCOORDS0], this->vb, offset, dim * sizeof(float) ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "SetStreamSource failed"));
		}
	}
}

void D3D9VertexArray::renderTexCoords(bool new_data, int offset, int unit) const {
	if( new_data ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderTexCoords new_data not supported\n"));
	}
	if( D3D9Renderer::STREAM_TEXCOORDS0+unit >= D3D9Renderer::N_STREAMS ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderTexCoords texture unit not supported\n"));
	}
	if( this->renderer->stream[D3D9Renderer::STREAM_TEXCOORDS0+unit] >= 0 ) {
		//this->renderer->m_pDevice->SetStreamSource( this->renderer->stream[D3D9Renderer::STREAM_TEXCOORDS0+unit], this->vb, offset, sizeof(D3DXVECTOR2) );
		/*if( this->vb == this->renderer->uniformTexcoordArray->vb ) {
			Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderTexCoords shouldn't call on uniform texcoord array\n"));
		}
		else*/ if( FAILED( this->renderer->m_pDevice->SetStreamSource( this->renderer->stream[D3D9Renderer::STREAM_TEXCOORDS0+unit], this->vb, offset, dim * sizeof(float) ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "SetStreamSource failed"));
		}
	}
}

void D3D9VertexArray::renderColors(bool new_data, int offset) const {
	//return;
	if( new_data ) {
		//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderColors new_data not supported\n"));
		this->update();
	}
	// NB - if alpha is false, note that Direct3D9 will have already converted the array into a version with alphas set to 255
	if( this->renderer->stream[D3D9Renderer::STREAM_COLORS] >= 0 ) {
		/*if( this->vb == this->renderer->uniformColorArray->vb ) {
			Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderColors shouldn't call on uniform color array\n"));
		}
		else if( this->renderer->using_uniform_color ) {
			Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderColors shouldn't call when using uniform color array\n"));
		}*/
		if( FAILED( this->renderer->m_pDevice->SetStreamSource( this->renderer->stream[D3D9Renderer::STREAM_COLORS], this->vb, offset, sizeof(DWORD) ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "SetStreamSource failed"));
		}
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderColors shouldn't call when no colors set\n"));
	}
}

/*static D3DPRIMITIVETYPE D3D9Modes[] = {
	D3DPT_POINTLIST,
    D3DPT_LINELIST,
	0, // line loop?
    D3DPT_LINESTRIP,
    D3DPT_TRIANGLELIST,
    D3DPT_TRIANGLESTRIP,
    D3DPT_TRIANGLEFAN,
	0, // quads
	0, // quad strip
	0 // polygon
};*/

/*static int VertsPerPoly[] = {
};*/

void D3D9VertexArray::update() const {
	ASSERT( stream );
	ASSERT( data != NULL );
	ASSERT( size > 0 );
	ASSERT( vb != NULL );
	VOID *ptr = NULL;
	if( FAILED( this->vb->Lock( 0, size, (void**)&ptr, D3DLOCK_DISCARD ) ) ) {
		Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9VertexArray::update failed to lock buffer\n"));
	}
	else {
		if( data_type == DATATYPE_COLORS ) {
			int local_size = size;
			ASSERT( dim == 3 || dim == 4 );
			ASSERT( local_size % dim == 0 );
			// always convert to 4 byte format in D3D9
			local_size /= dim;
			local_size *= 4;
			// now local_size is already in 4 byte format
			DWORD *d3ddata = new DWORD[local_size/4];
			unsigned char *srcdata = (unsigned char *)data;
			for(int i=0;i<local_size/4;i++) {
				unsigned char alpha = (dim == 4) ? srcdata[3] : 255;
				d3ddata[i] = D3DCOLOR_RGBA(srcdata[0],srcdata[1],srcdata[2],alpha);
				srcdata += dim;
			}
			memcpy( ptr, d3ddata, local_size );
			delete [] d3ddata;
		}
		else {
			memcpy( ptr, data, size );
		}
		this->vb->Unlock();
	}
}

void D3D9VertexArray::updateIndices() const {
	//ASSERT( stream );
	ASSERT( data != NULL );
	ASSERT( ib != NULL );
	VOID *ptr = NULL;
	if( FAILED( this->ib->Lock( 0, size, (void**)&ptr, D3DLOCK_DISCARD ) ) ) {
		Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9VertexArray::updateIndices failed to lock buffer for new data\n"));
	}
	else {
		memcpy( ptr, data, size );
		this->ib->Unlock();
	}
}

void D3D9VertexArray::renderElements(DrawingMode mode, int n_this_renderlength, int n_this_vertices) const {
	//D3DXMATRIX matrix;
	//renderer->m_pDevice->GetTransform(renderer->c_transform, &matrix);
	//D3DXMatrixIdentity(&matrix);
	/*D3DXMatrixTranslation(&matrix, 0, 0, 10);
	renderer->m_pDevice->SetTransform(renderer->c_transform, &matrix);*/
	//renderer->drawFrameTest();
	//return;

	// test
	//renderer->m_pDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
	//renderer->m_pDevice->DrawPrimitive( D3DPT_TRIANGLELIST, 0, 1 );
	//renderer->m_pDevice->SetFVF( D3DFVF_XYZ );
	//renderer->m_pDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 1, g_test_vertices, sizeof(CUSTOMVERTEX));
	//renderer->m_pDevice->DrawIndexedPrimitiveUP(D3DPT_TRIANGLELIST, 0, 3, 1, g_test_indices, D3DFMT_INDEX16, g_test_vertices, sizeof(CUSTOMVERTEX));
	//return;

	D3DPRIMITIVETYPE d3d9mode;
	int n_polys = 0;
	if( mode == DRAWINGMODE_TRIANGLES ) {
		d3d9mode = D3DPT_TRIANGLELIST;
		n_polys = n_this_renderlength / 3;
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9VertexArray::renderElements render mode not supported\n"));
	}
	if( this->ib == NULL ) {
		if( FAILED( this->renderer->m_pDevice->SetVertexDeclaration( this->renderer->c_pDecl ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "SetVertexDeclaration failed"));
		}
		if( FAILED( this->renderer->m_pDevice->DrawPrimitive( d3d9mode, 0, n_polys ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "DrawPrimitive failed"));
		}
	}
	else {
		if( FAILED( this->renderer->m_pDevice->SetIndices( this->ib ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "SetStreamSource failed"));
		}
		if( FAILED( this->renderer->m_pDevice->SetVertexDeclaration( this->renderer->c_pDecl ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "SetVertexDeclaration failed"));
		}
		if( FAILED( this->renderer->m_pDevice->DrawIndexedPrimitive( d3d9mode, 0, 0, n_this_vertices, 0, n_polys ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "DrawIndexedPrimitive failed"));
		}
	}
}

void D3D9VertexArray::onDeviceLost() {
	if( ib != NULL ) {
		ib->Release();
		ib = NULL;
	}
	if( vb != NULL ) {
		vb->Release();
		vb = NULL;
	}
}
void D3D9VertexArray::onDeviceReset() {
	if( !init() ) {
		Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9VertexArray::inDeviceReset() failed to reinitialise\n"));
	}
}

CGD3D9Shader::CGD3D9Shader(D3D9Renderer *renderer,bool vertex_shader) : CGShader(vertex_shader) {
	this->renderer = renderer;
}

/*void CGD3D9Shader::init() {
	//if( this->vertex_shader )
	{
		// Matrices
		cgparam_ModelViewProj = cgGetNamedParameter(cgProgram, "mx.ModelViewProj");
		cgparam_ModelViewIT = cgGetNamedParameter(cgProgram, "mx.ModelViewIT");
		cgparam_ModelView = cgGetNamedParameter(cgProgram, "mx.ModelView");

	}
	//this->setDefaults();
}*/

#if 0
void CGD3D9Shader::setTexture(const Texture *texture) const {
	this->SetUniformParameter1f("textureID", texture != NULL ? 1.0f : 0.0f);

	// don't need to set these?
	/*const D3D9Texture *d3d_texture = static_cast<const D3D9Texture *>(texture);
	CGparameter p = cgGetNamedParameter(cgProgram, "texture");
	if( d3d_texture != NULL ) {
		cgD3D9SetTexture(p, d3d_texture->getD3DTexture());
	}
	else {
		cgD3D9SetTexture(p, NULL);
	}*/
}
#endif

/*void CGD3D9Shader::setSecondaryTexture(const Texture *texture) const {
	this->SetUniformParameter1f("secondary_textureID", texture != NULL ? 1.0f : 0.0f);
}*/

void CGD3D9Shader::setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const {
	D3DXMATRIX modelview, proj, modelviewI, modelviewIT, modelview_proj;
	this->renderer->m_pDevice->GetTransform(D3DTS_WORLD, &modelview);
	this->renderer->m_pDevice->GetTransform(D3DTS_PROJECTION, &proj);
	D3DXMatrixTranspose(&modelviewI, &modelview);
	D3DXMatrixInverse(&modelviewIT, NULL, &modelviewI);
	modelview_proj = modelview * proj;
	D3DXMATRIX modelview_T, modelviewIT_T, modelview_proj_T;
	D3DXMatrixTranspose(&modelview_T, &modelview);
	D3DXMatrixTranspose(&modelviewIT_T, &modelviewIT);
	D3DXMatrixTranspose(&modelview_proj_T, &modelview_proj);
	/*if( cgparam_ModelViewProj != NULL )
		cgD3D9SetUniformMatrix(cgparam_ModelViewProj, &modelview_proj_T);
	if( cgparam_ModelViewIT != NULL )
		cgD3D9SetUniformMatrix(cgparam_ModelViewIT, &modelviewIT_T);
	if( cgparam_ModelView != NULL )
		cgD3D9SetUniformMatrix(cgparam_ModelView, &modelview_T);*/
	CGparameter p = NULL;
	p = cgGetNamedParameter(cgProgram, parameter_modelviewproj);
	if( p != NULL ) {
		cgD3D9SetUniformMatrix(p, &modelview_proj_T);
	}
	p = cgGetNamedParameter(cgProgram, parameter_modelviewit);
	if( p != NULL ) {
		cgD3D9SetUniformMatrix(p, &modelviewIT_T);
	}
	p = cgGetNamedParameter(cgProgram, parameter_modelview);
	if( p != NULL ) {
		cgD3D9SetUniformMatrix(p, &modelview_T);
	}
}

void CGD3D9Shader::SetModelViewProjMatrix(const char *parameter) const {
	CGparameter pMatrix = cgGetNamedParameter(cgProgram, parameter);
	if( pMatrix != NULL ) {
		D3DXMATRIX modelview, proj, modelview_proj;
		this->renderer->m_pDevice->GetTransform(D3DTS_WORLD, &modelview);
		this->renderer->m_pDevice->GetTransform(D3DTS_PROJECTION, &proj);
		modelview_proj = modelview * proj;
		D3DXMATRIX modelview_proj_T;
		D3DXMatrixTranspose(&modelview_proj_T, &modelview_proj);
		cgD3D9SetUniformMatrix(pMatrix, &modelview_proj_T);
	}
}

CGFXD3D9ShaderEffect::CGFXD3D9ShaderEffect(D3D9Renderer *renderer, CGcontext cg_context, const char *file, const char **args) : CGFXShaderEffect(cg_context, file, args), renderer(renderer) {
}

void CGFXD3D9ShaderEffect::setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const {
	D3DXMATRIX modelview, proj, modelviewI, modelviewIT, modelview_proj;
	this->renderer->m_pDevice->GetTransform(D3DTS_WORLD, &modelview);
	this->renderer->m_pDevice->GetTransform(D3DTS_PROJECTION, &proj);
	D3DXMatrixTranspose(&modelviewI, &modelview);
	D3DXMatrixInverse(&modelviewIT, NULL, &modelviewI);
	modelview_proj = modelview * proj;
	D3DXMATRIX modelview_T, modelviewIT_T, modelview_proj_T;
	D3DXMatrixTranspose(&modelview_T, &modelview);
	D3DXMatrixTranspose(&modelviewIT_T, &modelviewIT);
	D3DXMatrixTranspose(&modelview_proj_T, &modelview_proj);
	if( cgparam_ModelViewProj != NULL ) {
		//cgD3D9SetUniformMatrix(cgparam_ModelViewProj, &modelview_proj_T); // gives CG error!
		float f[16];
		for(int y=0,c=0;y<4;y++) {
			for(int x=0;x<4;x++,c++) {
				f[c] = modelview_proj_T.m[x][y];
			}
		}
		cgSetMatrixParameterfc(cgparam_ModelViewProj, f);
	}
}

void CGFXD3D9ShaderEffect::SetModelViewProjMatrix(const char *parameter) const {
	if( cgparam_ModelViewProj != NULL ) {
		D3DXMATRIX modelview, proj, modelview_proj;
		this->renderer->m_pDevice->GetTransform(D3DTS_WORLD, &modelview);
		this->renderer->m_pDevice->GetTransform(D3DTS_PROJECTION, &proj);
		modelview_proj = modelview * proj;
		D3DXMATRIX modelview_proj_T;
		D3DXMatrixTranspose(&modelview_proj_T, &modelview_proj);
		//cgD3D9SetUniformMatrix(cgparam_ModelViewProj, &modelview_proj_T);
		float f[16];
		for(int y=0,c=0;y<4;y++) {
			for(int x=0;x<4;x++,c++) {
				f[c] = modelview_proj_T.m[x][y];
			}
		}
		cgSetMatrixParameterfc(cgparam_ModelViewProj, f);
	}
}

/*void CGD3D9Shader::SetUniformParameter1f(const char *pszParameter, float p1) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p ) {
		cgD3D9SetUniform(p, &p1);
		//cgSetParameter1f(p, p1);
	}
}

void CGD3D9Shader::SetUniformParameter2f(const char *pszParameter, float p1, float p2) const {
	float v[2] = {p1, p2};
	this->SetUniformParameter2fv(pszParameter, v);
}


void CGD3D9Shader::SetUniformParameter3f(const char *pszParameter, float p1, float p2, float p3) const {
	float v[3] = {p1, p2, p3};
	this->SetUniformParameter3fv(pszParameter, v);
}

void CGD3D9Shader::SetUniformParameter4f(const char *pszParameter, float p1, float p2, float p3, float p4) const {
	float v[4] = {p1, p2, p3, p4};
	this->SetUniformParameter4fv(pszParameter, v);
}

void CGD3D9Shader::SetUniformParameter2fv(const char *pszParameter, const float v[2]) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgD3D9SetUniform(p, v);
}

void CGD3D9Shader::SetUniformParameter3fv(const char *pszParameter, const float v[3]) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgD3D9SetUniform(p, v);
}

void CGD3D9Shader::SetUniformParameter4fv(const char *pszParameter, const float v[4]) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgD3D9SetUniform(p, v);
}*/

void CGD3D9Shader::enable() const {
	// set renderer specific uniforms

	if( this->vertex_shader ) {
		if( renderer->shader_clipping ) {
			this->SetUniformParameter4fv("clipping_plane", renderer->shader_clipping_plane);
		}
		else {
			this->SetUniformParameter4f("clipping_plane", 0.0, 0.0, 0.0, -1.0);
		}
	}

	// TODO: fog disabled in shaders for now

	// set rest to some defaults
	/*if( this->vertex_shader ) {
		this->setHardwareAnimationAlpha(0.0);
	}*/

	cgD3D9BindProgram(cgProgram);
	/*if( this->vertex_shader )
		renderer->setActiveVertexShader(this);
	else
		renderer->setActivePixelShader(this);*/
}

/*void CGD3D9Shader::disable() const {
	//cgGLDisableProfile(cgProfile);
	//cgD3D9BindProgram(NULL); // is this how to do it?
	if( this->vertex_shader ) {
		this->renderer->m_pDevice->SetVertexShader(NULL);
		//renderer->setActiveVertexShader(NULL);
	}
	else {
		this->renderer->m_pDevice->SetPixelShader(NULL);
		//renderer->setActivePixelShader(NULL);
	}
}*/

void D3D9TransformationMatrix::load() const {
	renderer->m_pDevice->SetTransform(renderer->c_transform, &mat);
}

void D3D9TransformationMatrix::save() {
	renderer->m_pDevice->GetTransform(renderer->c_transform, &mat);
}

void D3D9TransformationMatrix::mult() const {
	renderer->m_pDevice->MultiplyTransform(renderer->c_transform, &mat);
}

D3D9Texture::D3D9Texture(D3D9Renderer *renderer, LPDIRECT3DTEXTURE9 pTexture) : renderer(renderer), m_pTexture(pTexture), alpha(false), dynamic(false), dynamic_width(0), dynamic_height(0) {
	//LOG("D3D9Texture()\n");
	renderer->store.insert(this);
}

D3D9Texture::~D3D9Texture() {
	this->m_pTexture->Release();
	renderer->store.erase(this);
}

Texture *D3D9Texture::create(D3D9Renderer *renderer, int width, int height, bool alpha, bool floating_point) {
	//LOG("D3D9Texture::create()\n");
	if( !VI_isPower(width, 2) || !VI_isPower(height, 2) ) {
		// see note under D3D9Texture::createFromImage().
		LOG("requested texture has dimensions %d x %d : not power of 2\n", width, height);
		Vision::setError(new VisionException(renderer, VisionException::V_TEXTURE_NON_POWER_2, "Non power of 2 textures not allowed"));
		return NULL;
	}

	LPDIRECT3DTEXTURE9 pTexture = NULL;
	//DWORD usage = 0;
	DWORD usage = D3DUSAGE_RENDERTARGET; // needed for render to texture
	//D3DPOOL pool = D3DPOOL_MANAGED;
	D3DPOOL pool = D3DPOOL_DEFAULT; // needed for render to texture?
	D3DFORMAT renderFormat;
	if( floating_point ) {
		renderFormat = alpha ? D3DFMT_A16B16G16R16F : D3DFMT_A16B16G16R16F;
	}
	else {
		renderFormat = alpha ? D3DFMT_A8R8G8B8 : D3DFMT_R8G8B8;
	}
	if( FAILED( D3DXCreateTexture( renderer->m_pDevice, width, height, 1, usage, renderFormat, pool, &pTexture) ) ) {
		Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "D3DXCreateTexture failed"));
		return NULL;
	}
	D3D9Texture *texture = new D3D9Texture(renderer, pTexture);
	texture->alpha = alpha;
	texture->floating_point = floating_point;
	texture->dynamic = true;
	texture->dynamic_width = width;
	texture->dynamic_height = height;
	//LOG("    done\n");
	return texture;
}

Texture *D3D9Texture::createFromImage(D3D9Renderer *renderer, VI_Image *image, bool mask, unsigned char maskcol[3]) {
	//image->save("test.png", "png");
	//LOG("D3D9Texture::createFromImage()\n");
	if( !VI_isPower(image->getWidth(), 2) || !VI_isPower(image->getHeight(), 2) ) {
		// Some hardware can only cope with non-power-of-2 textures (e.g., calling D3DXCreateTexture with MipLevels set to 0 or D3DX_DEFAULT forces textures to be converted to power-of-2 on Intel GMA 950; using 1 doesn't do this, but means we can't generate mipmaps with D3DXFilterTexture).
		// These are handled by D3D9 by creating a larger texture, with the input image only partially filling the texture. This creates problems (e.g., texture wrapping can't work),
		// so we insist on power-of-2 images. Images can be scaled with VI_Image::scale() if they are not a power-of-2 in width or height.
		LOG("texture has dimensions %d x %d : not power of 2\n", image->getWidth(), image->getHeight());
		Vision::setError(new VisionException(renderer, VisionException::V_TEXTURE_NON_POWER_2, "Non power of 2 textures not allowed"));
		return NULL;
	}

	bool image_alpha = image->getAlpha();
	bool tex_alpha = image_alpha || mask;

	LPDIRECT3DTEXTURE9 pTexture = NULL;
	DWORD usage = 0;
	//DWORD usage = D3DUSAGE_AUTOGENMIPMAP;
	if( FAILED( D3DXCreateTexture( renderer->m_pDevice, image->getWidth(), image->getHeight(), D3DX_DEFAULT, usage, tex_alpha ? D3DFMT_A8R8G8B8 : D3DFMT_R8G8B8, D3DPOOL_MANAGED, &pTexture) ) ) {
	//if( FAILED( D3DXCreateTexture( renderer->m_pDevice, image->getWidth(), image->getHeight(), 0, usage, tex_alpha ? D3DFMT_A8R8G8B8 : D3DFMT_R8G8B8, D3DPOOL_MANAGED, &pTexture) ) ) {
	//if( FAILED( D3DXCreateTexture( renderer->m_pDevice, image->getWidth(), image->getHeight(), 1, usage, tex_alpha ? D3DFMT_A8R8G8B8 : D3DFMT_R8G8B8, D3DPOOL_MANAGED, &pTexture) ) ) {
		Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "D3DXCreateTexture failed"));
		return NULL;
	}
	/*D3DSURFACE_DESC desc;
	pTexture->GetLevelDesc(0, &desc);
	// need to shift in y direction, to cope with possible change in texture size (this can occur when non-power-2 textures not supported)
	int shift = desc.Height - image->getHeight();
	if( shift != 0 ) {
		LOG("texture y shift %d\n", shift);
	}*/
	//int shift = 0;

	D3DLOCKED_RECT d3drect;
	if( FAILED( pTexture->LockRect(0, &d3drect, NULL, 0) ) ) { // no D3DLOCK_DISCARD, as texture is not dynamic
		pTexture->Release();
		Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "Failed to lock texture"));
		return NULL;
	}
	unsigned char *s_ptr = (unsigned char *)image->getData();
	unsigned char *dest = (unsigned char *)d3drect.pBits;
	for(int y=0;y<image->getHeight();y++) {
		//unsigned char *line = &dest[(y+shift)*d3drect.Pitch];
		unsigned char *line = &dest[y*d3drect.Pitch];
		//unsigned char *line = &dest[(image->getHeight()-1-y)*d3drect.Pitch];
		for(int x=0;x<image->getWidth();x++) {
			unsigned char r = s_ptr[0];
			unsigned char g = s_ptr[1];
			unsigned char b = s_ptr[2];
			//r = g = b = 255;
			//b = 255;
			//r = g = 0;
			bool masked = false;
			if( mask ) {
				if( r == maskcol[0] && g == maskcol[1] && b == maskcol[2] ) {
					masked = true;
					r = g = b = 0; // so the mask colour doesn't show up at all, e.g., with filtering
				}
			}
			*line++ = b;
			*line++ = g;
			*line++ = r;
			if( mask ) {
				int alpha = 255;
				//if( rgbdata[i].r == maskcol->r && rgbdata[i].g == maskcol->g && rgbdata[i].b == maskcol->b ) {
				//if( r == maskcol->r && g == maskcol->g && b == maskcol->b ) {
				//if( r == maskcol[0] && g == maskcol[1] && b == maskcol[2] ) {
				if( masked ) {
					alpha = 0;
				}
				*line++ = alpha;
			}
			else if( image_alpha ) {
				*line++ = s_ptr[3];
			}
			else {
				//*line++ = 255;
				line++;
				// texture is only in RGB format, not RGBA, but in Direct3D9, we still have 4 bytes per pixel?!
			}
			s_ptr += image_alpha ? 4 : 3;
		}
	}
	pTexture->UnlockRect(0);

	/*if( desc.Width != image->getWidth() || desc.Height != image->getHeight() ) {
		// simpler to scale image so that it matches power-of-2
		LPDIRECT3DTEXTURE9 pTexture2 = NULL;
		if( FAILED( D3DXCreateTexture( renderer->m_pDevice, desc.Width, desc.Height, D3DX_DEFAULT, usage, tex_alpha ? D3DFMT_A8R8G8B8 : D3DFMT_R8G8B8, D3DPOOL_DEFAULT, &pTexture2) ) ) {
			return NULL;
		}

		LPDIRECT3DSURFACE9 textureSurface = NULL;
		LPDIRECT3DSURFACE9 textureSurface2 = NULL;
		pTexture->GetSurfaceLevel(0, &textureSurface);
		pTexture2->GetSurfaceLevel(0, &textureSurface2);
		RECT sourceRect = {0, 0, image->getWidth(), image->getHeight()};
		RECT destRect = {0, 0, desc.Width, desc.Height};
		if( FAILED( renderer->m_pDevice->StretchRect(textureSurface, &sourceRect, textureSurface2, &destRect, D3DTEXF_LINEAR) ) ) {
			Vision::setError(new VisionException(NULL, VisionException::V_DIRECT3D_ERROR, "D3D9Texture StretchRect failed\n"));
		}
		textureSurface->Release();
		textureSurface2->Release();
		pTexture->Release();
		pTexture = pTexture2;
	}*/

	//pTexture->GenerateMipSubLevels(); // don't need this, as this is only a hint to generate mipmaps when suitable for the application; we'll leave it up to the driver
	if( FAILED( D3DXFilterTexture(pTexture, NULL, D3DX_DEFAULT, D3DX_DEFAULT) ) ) {
		LOG("failed to create mipmaps\n");
	}
	int levels = pTexture->GetLevelCount();
	//LOG("created texture from image, levels: %d\n", levels);
	D3D9Texture *texture = new D3D9Texture(renderer, pTexture);
	texture->alpha = tex_alpha;
	int wid = texture->getWidth();
	int hgt = texture->getHeight();
	delete image;
	//LOG("    done\n");
	return texture;
}

Texture *D3D9Texture::loadTexture(D3D9Renderer *renderer, const char *filename, bool mask, unsigned char maskcol[3]) {
	/*LPDIRECT3DTEXTURE9 pTexture = NULL;
	if( !FAILED( D3DXCreateTextureFromFileA( renderer->m_pDevice, filename, &pTexture ) ) ) { // TODO: need to invert in y direction if loaded via this function, and do masking
		int levels = pTexture->GetLevelCount();
		D3D9Texture *texture = new D3D9Texture(renderer, pTexture);
		return texture;
	}*/

	Image2D *image = Image2D::loadImage(filename);
	if( mask && ( !VI_isPower(image->getWidth(), 2) || !VI_isPower(image->getHeight(), 2) ) ) {
		// We disallow this, as resizing would mess up the mask. Although we could get round this with a simple resize, it's probably better to explicitly disallow, and insist on power-by-2 texture files
		LOG("texture has dimensions %d x %d : not power of 2\n", image->getWidth(), image->getHeight());
		Vision::setError(new VisionException(NULL, VisionException::V_TEXTURE_NON_POWER_2, "Non power of 2 textures not allowed with mask\n"));
		return false;
	}
	if( !image->makePowerOf2() ) {
		delete image;
		Vision::setError(new VisionException(image, VisionException::V_IMAGE_ERROR, "Failed to scale texture image to power-of-2 size\n"));
		return NULL;
	}
	return createFromImage(renderer, image, mask, maskcol);
}

Texture *D3D9Texture::copy() {
	// TODO: use handle/body idiom
	D3D9Texture *texture = new D3D9Texture(renderer, m_pTexture);
	texture->alpha = this->alpha;
	texture->wrap_mode = this->wrap_mode;
	texture->draw_mode = this->draw_mode;
	return texture;
}

void D3D9Texture::enable() {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Texture:: not supported\n"));
	//return;
	int unit = this->renderer->c_textureunit;

	this->renderer->m_pDevice->SetTexture(unit, m_pTexture);

	if( this->draw_mode == DRAWMODE_SMOOTH ) {
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // needed to enable smooth mipmapping
	}
	else {
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MINFILTER, D3DTEXF_POINT);
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	}

	/*this->m_pDevice->SetTextureStageState(unit, D3DTSS_COLOROP,   D3DTOP_MODULATE);
    this->m_pDevice->SetTextureStageState(unit, D3DTSS_COLORARG1, D3DTA_TEXTURE);
    this->m_pDevice->SetTextureStageState(unit, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
    this->m_pDevice->SetTextureStageState(unit, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);*/
	// by default, Direct3D takes alpha values only from the texture map - we want to modulate with the material/vertex diffuse component (as is the case for OpenGL)
	this->renderer->m_pDevice->SetTextureStageState(unit, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
    this->renderer->m_pDevice->SetTextureStageState(unit, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
    this->renderer->m_pDevice->SetTextureStageState(unit, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	if( this->wrap_mode == WRAPMODE_CLAMP ) {
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	}
	else if( this->wrap_mode == WRAPMODE_MIRROR ) {
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR);
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR);
	}
	else {
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	}
	
	/*if( this->renderer->getActivePixelShader() != NULL ) {
		//this->renderer->getActivePixelShader()->setTexture(this);
		const CGShader *shader = static_cast<const CGShader *>(this->renderer->getActivePixelShader());
		shader->setTexture(this);
	}*/
	/*if( this->renderer->getActiveShader() != NULL ) {
		this->renderer->getActiveShader()->SetUniformParameter1f("textureID", 1.0f);
	}*/
}

/*void D3D9Texture::clampToEdge() {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Texture:: not supported\n"));
	int unit = this->renderer->c_textureunit;
	this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
}

void D3D9Texture::clampMirror() {
	int unit = this->renderer->c_textureunit;
	this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR);
	this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR);
}*/

void D3D9Texture::copyTo(int width, int height) {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Texture:: not supported\n"));
	LPDIRECT3DSURFACE9 sourceSurface = NULL;
	LPDIRECT3DSURFACE9 textureSurface = NULL;
	this->renderer->m_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &sourceSurface);
	/*D3DSURFACE_DESC desc;
	sourceSurface->GetDesc(&desc);
	int w = desc.Width;
	int h = desc.Height;*/
	this->m_pTexture->GetSurfaceLevel(0, &textureSurface);
	RECT sourceRect = {0, 0, width, height};
	if( FAILED( this->renderer->m_pDevice->StretchRect(sourceSurface, &sourceRect, textureSurface, &sourceRect, D3DTEXF_NONE) ) ) {
	//if( FAILED( this->renderer->m_pDevice->StretchRect(sourceSurface, NULL, textureSurface, NULL, D3DTEXF_NONE) ) ) {
		Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9Texture::copyTo StretchRect failed\n"));
	}

	/*D3DLOCKED_RECT d3drect;
	if( !FAILED( m_pTexture->LockRect(0, &d3drect, NULL, 0) ) ) {
		unsigned char *dest = (unsigned char *)d3drect.pBits;
		for(int y=0;y<this->getHeight();y++) {
			unsigned char *line = &dest[y*d3drect.Pitch];
			for(int x=0;x<this->getWidth();x++) {
				*line++ = 255;
				*line++ = y < this->getHeight()/2 ? 0 : 255;
				*line++ = 0;
				*line++ = 255;
			}
		}
		m_pTexture->UnlockRect(0);
	}*/
	sourceSurface->Release();
	textureSurface->Release();
}

void D3D9Texture::draw(int x, int y) {
	int width = this->getWidth();
	int height = this->getHeight();
	this->draw(x, y, width, height);
}

void D3D9Texture::draw(int x, int y, int w, int h) {
	// sprite version is actually slower?
	/*D3DXMATRIX matrix;
	// undo the flip-Y that occurs for DirectX textures (which we do because of the texcoords being inverted, compared with OpenGL...)
	//D3DXMatrixScaling(&matrix, 0, -1, 0);
	D3DXPLANE plane(0.0f, -1.0f, 0.0f, y + 0.0f*this->getHeight());
	D3DXMatrixReflect(&matrix, &plane);
	this->renderer->sprite->SetTransform(&matrix);
	this->renderer->sprite->Begin(this->alpha ? D3DXSPRITE_ALPHABLEND : NULL);
	D3DXVECTOR3 pos((float)x, (float)y, 0.0f);
	this->renderer->sprite->Draw(this->m_pTexture, NULL, NULL, &pos, 0xFFFFFFFF);
	this->renderer->sprite->End();

	return;*/

	if( this->alpha ) {
		this->renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
	}

	VERTEX2D_TEXTURED vertices[4];
	vertices[0].x = (float)x;
	vertices[0].y = (float)(y-h);
	vertices[0].z = 0.0f;
	vertices[0].u = 0.0f;
	vertices[0].v = 1.0f;
	vertices[1].x = (float)x;
	vertices[1].y = (float)(y);
	vertices[1].z = 0.0f;
	vertices[1].u = 0.0f;
	vertices[1].v = 0.0;
	vertices[2].x = (float)(x+w);
	vertices[2].y = (float)(y-h);
	vertices[2].z = 0.0f;
	vertices[2].u = 1.0;
	vertices[2].v = 1.0f;
	vertices[3].x = (float)(x+w);
	vertices[3].y = (float)(y);
	vertices[3].z = 0.0f;
	vertices[3].u = 1.0;
	vertices[3].v = 0.0;
	for(int i=0;i<4;i++) {
		vertices[i].color = D3DCOLOR_RGBA(255, 255, 255, 255);
	}
	//renderer->m_pDevice->SetTexture(renderer->c_textureunit, this->m_pTexture);
	this->enable();
	renderer->m_pDevice->SetFVF( D3DFVF_VERTEX2D_TEXTURED );
	renderer->m_pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vertices, sizeof(VERTEX2D_TEXTURED));

	if( this->alpha ) {
		this->renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
	}
	renderer->m_pDevice->SetTexture(renderer->c_textureunit, NULL);
}

int D3D9Texture::getWidth() const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Texture:: not supported\n"));
	D3DSURFACE_DESC desc;
	this->m_pTexture->GetLevelDesc(0, &desc);
	return desc.Width;
}

int D3D9Texture::getHeight() const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Texture:: not supported\n"));
	D3DSURFACE_DESC desc;
	this->m_pTexture->GetLevelDesc(0, &desc);
	return desc.Height;
}

void D3D9Texture::onDeviceLost() {
	//LOG("D3D9Texture::onDeviceLost() %d\n", this);
	if( dynamic ) {
		LOG("dynamic\n");
		this->m_pTexture->Release();
		this->m_pTexture = NULL;
	}
}

void D3D9Texture::onDeviceReset() {
	//LOG("D3D9Texture::onDeviceReset() %d\n", this);
	if( dynamic ) {
		LOG("dynamic\n");
		// only dynamic textures are in default space
		LPDIRECT3DTEXTURE9 pTexture = NULL;
		DWORD usage = D3DUSAGE_RENDERTARGET; // needed for render to texture
		D3DPOOL pool = D3DPOOL_DEFAULT; // needed for render to texture?
		D3DFORMAT renderFormat;
		if( floating_point ) {
			renderFormat = alpha ? D3DFMT_A16B16G16R16F : D3DFMT_A16B16G16R16F;
		}
		else {
			renderFormat = alpha ? D3DFMT_A8R8G8B8 : D3DFMT_R8G8B8;
		}
		if( FAILED( D3DXCreateTexture( renderer->m_pDevice, dynamic_width, dynamic_height, 1, usage, renderFormat, pool, &m_pTexture) ) ) {
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9Texture::inDeviceReset() failed to reinitialise texture\n"));
		}
	}
}

D3D9FramebufferObject::D3D9FramebufferObject(D3D9Renderer *renderer) : FramebufferObject(), renderer(renderer), surface(NULL), texture(NULL), depth_stencil(NULL), width(0), height(0), want_depth_stencil(false) {
	renderer->store.insert(this);
}

D3D9FramebufferObject::~D3D9FramebufferObject() {
	free();
	renderer->store.erase(this);
}

bool D3D9FramebufferObject::generate(int width,int height,bool want_depth_stencil) {
	this->width = width;
	this->height = height;
	this->want_depth_stencil = want_depth_stencil;

	if( !VI_isPower(width, 2) || !VI_isPower(height, 2) ) {
		// warn just in case?
		LOG("requested framebuffer has dimensions %d x %d : not power of 2\n", width, height);
		//Vision::setError(new VisionException(renderer, VisionException::V_TEXTURE_NON_POWER_2, "Non power of 2 textures not allowed\n"));
		//return false;
	}

	return regenerate();
}

bool D3D9FramebufferObject::regenerate() {
	this->free(); // free any existing data

	bool ok = true;
	DWORD usage = D3DUSAGE_RENDERTARGET; // needed for render to texture
	D3DPOOL pool = D3DPOOL_DEFAULT; // needed for render to texture?
	//D3DFORMAT renderFormat = D3DFMT_A32B32G32R32F;
	D3DFORMAT renderFormat = D3DFMT_A16B16G16R16F;
	//D3DFORMAT renderFormat = D3DFMT_A8R8G8B8;
	//D3DFORMAT depthStencilFormat = D3DFMT_D24X8;
	D3DFORMAT depthStencilFormat = D3DFMT_D24S8;
	D3DFORMAT adapterFormat = renderer->getFormat();
	if( FAILED( D3DXCreateTexture( renderer->m_pDevice, width, height, 1, usage, renderFormat, pool, &texture ) ) ) {
		LOG("failed to create texture\n");
		ok = false;
	}
	//else if( FAILED( renderer->m_pD3D->CheckDeviceFormat( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, adapterFormat, D3DUSAGE_RENDERTARGET, D3DRTYPE_SURFACE, renderFormat ) ) ) {
	/*else if( FAILED( renderer->m_pD3D->CheckDeviceFormat( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, adapterFormat, D3DUSAGE_RENDERTARGET | D3DUSAGE_QUERY_POSTPIXELSHADER_BLENDING, D3DRTYPE_TEXTURE, renderFormat ) ) ) {
		LOG("depth render format %d not available on adapter format %d\n", renderFormat, adapterFormat);
		ok = false;
	}
	else if( FAILED( renderer->m_pD3D->CheckDeviceFormat( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, adapterFormat, D3DUSAGE_DEPTHSTENCIL, D3DRTYPE_SURFACE, depthStencilFormat ) ) ) {
		LOG("depth stencil format %d not available on adapter format %d\n", depthStencilFormat, adapterFormat);
		ok = false;
	}
	else if( FAILED( renderer->m_pD3D->CheckDepthStencilMatch( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, adapterFormat, renderFormat, depthStencilFormat ) ) ) {
		LOG("depth stencil formats don't match: %d, %d, %d\n", adapterFormat, renderFormat, depthStencilFormat);
		ok = false;
	}*/
	else if( FAILED( renderer->m_pDevice->CreateDepthStencilSurface( width, height, depthStencilFormat, D3DMULTISAMPLE_NONE, 0, TRUE, &depth_stencil, NULL ) ) ) {
		LOG("failed to create depth stencil surface\n");
		ok = false;
	}
	if( !ok ) {
		this->free();
	}
	else {
		this->texture->GetSurfaceLevel(0, &surface);
	}
	return ok;
}

void D3D9FramebufferObject::free() {
	if( this->surface != NULL ) {
		this->surface->Release();
		this->surface = NULL;
	}
	if( this->texture != NULL ) {
		this->texture->Release();
		this->texture = NULL;
	}
	if( this->depth_stencil != NULL ) {
		this->depth_stencil->Release();
		this->depth_stencil = NULL;
	}
}

void D3D9FramebufferObject::onDeviceLost() {
	this->free();
}

void D3D9FramebufferObject::onDeviceReset() {
	if( !this->regenerate() ) {
		Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9FramebufferObject::inDeviceReset() failed to reinitialise framebuffer\n"));
	}
}

void D3D9FramebufferObject::bind() const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	this->renderer->m_pDevice->SetRenderTarget(0, this->surface);
}

void D3D9FramebufferObject::enableTexture() const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	int unit = 0;
	this->renderer->m_pDevice->SetTexture(unit, texture);

	//this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	//this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	//this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MIPFILTER, D3DTEXF_POINT); // needed to enable simple mipmapping
	//this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // needed to enable smooth mipmapping

	// by default, Direct3D takes alpha values only from the texture map - we want to modulate with the material/vertex diffuse component (as is the case for OpenGL)
	this->renderer->m_pDevice->SetTextureStageState(unit, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
    this->renderer->m_pDevice->SetTextureStageState(unit, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
    this->renderer->m_pDevice->SetTextureStageState(unit, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	this->renderer->m_pDevice->SetSamplerState(unit, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
}

D3D9FontBuffer::D3D9FontBuffer(D3D9Renderer *renderer,const char *family,int pt,int style) : renderer(renderer) {
	//LOG("D3D9FontBuffer()\n");
	/*HDC hDC = GetDC(NULL);
	int width = -MulDiv(pt, GetDeviceCaps(hDC, LOGPIXELSX), 96)/2;
	int height = -MulDiv(pt, GetDeviceCaps(hDC, LOGPIXELSY), 96);
	LOG("scale x = %d\n", GetDeviceCaps(hDC, LOGPIXELSX));
	LOG("scale y = %d\n", GetDeviceCaps(hDC, LOGPIXELSY));
	ReleaseDC(NULL, hDC);*/
	int width = - pt / 2;
	int height = - pt;
	int weight = style;
	bool italic = false;
	if( FAILED( D3DXCreateFontA(renderer->m_pDevice, height, width, weight, 1, italic,
			DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			family, &font) ) ) {
		Vision::setError(new VisionException(this, VisionException::V_FONT_ERROR, "D3DXCreateFont failed\n"));
	}

	TEXTMETRIC metric;
	font->GetTextMetrics(&metric);
	LOG("%s size %d ; descent %d\n", family, pt, metric.tmDescent);

	//renderer->store_fonts.insert(this);
	renderer->store.insert(this);
	//LOG("    done\n");
}

D3D9FontBuffer::~D3D9FontBuffer() {
	this->font->Release();
	/*set<D3D9FontBuffer *>::iterator iter = renderer->store_fonts.find(this);
	renderer->store_fonts.erase(iter);*/
	//renderer->store_fonts.erase(this);
	renderer->store.erase(this);
}

void D3D9FontBuffer::onDeviceLost() {
	this->font->OnLostDevice();
}
void D3D9FontBuffer::onDeviceReset() {
	this->font->OnResetDevice();
}

int D3D9FontBuffer::writeText(int x,int y,const char *text) const {
	return this->writeText(x, y, text, -1);
}

int D3D9FontBuffer::writeText(int x,int y,const char *text,size_t length) const {
	// need to take current transformation into account, as D3DX Fonts don't do this automatically (unlike OpenGL)
	// (we use translation when rendering sub-Panels with non-zero positions)
	D3DXMATRIX matrix;
	this->renderer->m_pDevice->GetTransform(D3DTS_WORLD, &matrix);
	D3DXVECTOR3 vec((float)x, (float)y, 0.0f);
	D3DXVECTOR4 tvec;
	D3DXVec3Transform(&tvec, &vec, &matrix);
	x = (int)tvec.x;
	y = (int)tvec.y;
	TEXTMETRIC metric;
	font->GetTextMetrics(&metric);
	y -= metric.tmDescent;
	RECT rc = {x, y, 0, 0};
	font->DrawTextA(NULL, text, (INT)length, &rc, DT_SINGLELINE | DT_CALCRECT, 0);
	font->DrawTextA(NULL, text, (INT)length, &rc, DT_SINGLELINE, renderer->c_color);
	return rc.right - rc.left;
}

int D3D9FontBuffer::getWidth(char letter) const {
	/*char ab[] = "ab";
	font->DrawTextA(NULL, ab, 2, &rc, DT_SINGLELINE | DT_CALCRECT, 0);
	char a[] = "a";
	font->DrawTextA(NULL, a, 1, &rc, DT_SINGLELINE | DT_CALCRECT, 0);
	char b[] = "b";
	font->DrawTextA(NULL, b, 1, &rc, DT_SINGLELINE | DT_CALCRECT, 0);*/
	SIZE size;
	GetTextExtentPoint32A(font->GetDC(), &letter, 1, &size);
	return size.cx;

	/*RECT rc = {0, 0, 0, 0};
	font->DrawTextA(NULL, &letter, 1, &rc, DT_SINGLELINE | DT_CALCRECT, 0);
	return rc.right;*/
}

int D3D9FontBuffer::getWidth(const char *text) const {
	// n.b. ignores spaces at start/end!
	RECT rc = {0, 0, 0, 0};
	font->DrawTextA(NULL, text, -1, &rc, DT_SINGLELINE | DT_CALCRECT, 0);
	return rc.right;
}

int D3D9FontBuffer::getHeight() const {
	/*TEXTMETRIC metric;
	font->GetTextMetrics(&metric);
	return metric.tmHeight;*/
	//return metric.tmAscent;
	RECT rc = {0, 0, 0, 0};
	font->DrawTextA(NULL, "I", 1, &rc, DT_SINGLELINE | DT_CALCRECT, 0);
	return rc.bottom;
}

D3D9Graphics2D::D3D9Graphics2D(GraphicsEnvironment *genv, D3D9Renderer *renderer) : Graphics2D(genv), renderer(renderer) {
}

void D3D9Graphics2D::setColor3i(unsigned char r, unsigned char g, unsigned char b) {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Graphics2D:: not supported\n"));
	/*D3DMATERIAL9 mtrl;
	renderer->m_pDevice->GetMaterial(&mtrl);
	mtrl.Diffuse.r = mtrl.Ambient.r = ((float)r)/255.0f;
	mtrl.Diffuse.g = mtrl.Ambient.g = ((float)g)/255.0f;
	mtrl.Diffuse.b = mtrl.Ambient.b = ((float)b)/255.0f;
	renderer->m_pDevice->SetMaterial(&mtrl);*/
	renderer->c_color = D3DCOLOR_RGBA(r, g, b, 255);
	//renderer->c_color = D3DCOLOR_RGBA(255, 255, 255, 255);
}

void D3D9Graphics2D::setColor4i(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Graphics2D:: not supported\n"));
	/*D3DMATERIAL9 mtrl;
	renderer->m_pDevice->GetMaterial(&mtrl);
	mtrl.Diffuse.r = mtrl.Ambient.r = ((float)r)/255.0f;
	mtrl.Diffuse.g = mtrl.Ambient.g = ((float)g)/255.0f;
	mtrl.Diffuse.b = mtrl.Ambient.b = ((float)b)/255.0f;
	mtrl.Diffuse.a = mtrl.Ambient.a = ((float)a)/255.0f;
	renderer->m_pDevice->SetMaterial(&mtrl);*/
	renderer->c_color = D3DCOLOR_RGBA(r, g, b, a);
	//renderer->c_color = D3DCOLOR_RGBA(255, 255, 255, 255);
}

/*void D3D9Graphics2D::setColor(float r,float g,float b) const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Graphics2D:: not supported\n"));
	// TODO:
}*/

void D3D9Graphics2D::plot(short x,short y) {
	Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Graphics2D:: not supported\n"));
	curr_x = x;
	curr_y = y;
}

void D3D9Graphics2D::draw(short x,short y) {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Graphics2D:: not supported\n"));
	this->draw(curr_x, curr_y, x, y);
	curr_x = x;
	curr_y = y;
}

void D3D9Graphics2D::draw(short x1,short y1,short x2,short y2) const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Graphics2D:: not supported\n"));
	VERTEX2D vertices[2];
	vertices[0].x = x1;
	vertices[0].y = y1;
	vertices[0].z = 0;
	vertices[1].x = x2;
	vertices[1].y = y2;
	vertices[1].z = 0;
	for(int i=0;i<2;i++) {
		vertices[i].color = renderer->c_color;
	}
	renderer->m_pDevice->SetFVF( D3DFVF_VERTEX2D );
	renderer->m_pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, 1, vertices, sizeof(VERTEX2D));
}

void D3D9Graphics2D::drawRect(short x,short y,short width,short height) const {
	// we -1, so that width of 2 gives us 2 pixels, not 3 pixels
	if( width <= 0 ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "D3D9Graphics2D::drawRect width too small\n"));
	}
	if( height <= 0 ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "D3D9Graphics2D::drawRect height too small\n"));
	}
	/*m_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
	m_pDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 1, g_test_vertices, sizeof(CUSTOMVERTEX));
	return;*/
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Graphics2D:: not supported\n"));
	VERTEX2D vertices[5];
	vertices[0].x = (float)x;
	vertices[0].y = (float)y;
	vertices[0].z = 0.0f;
	vertices[1].x = (float)x;
	vertices[1].y = (float)(y+height-1);
	vertices[1].z = 0.0f;
	vertices[2].x = (float)(x+width-1);
	vertices[2].y = (float)(y+height-1);
	vertices[2].z = 0.0f;
	vertices[3].x = (float)(x+width-1);
	vertices[3].y = (float)y;
	vertices[3].z = 0.0f;
	for(int i=0;i<4;i++) {
		vertices[i].color = renderer->c_color;
	}
	vertices[4] = vertices[0];
	renderer->m_pDevice->SetFVF( D3DFVF_VERTEX2D );
	renderer->m_pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, 4, vertices, sizeof(VERTEX2D));
}

void D3D9Graphics2D::fillRect(short x,short y,short width,short height,const float tu[4], const float tv[4]) const {
	if( this->texture == NULL ) {
		VERTEX2D vertices[4];
		vertices[0].x = (float)x;
		vertices[0].y = (float)y;
		vertices[0].z = 0.0f;
		vertices[1].x = (float)x;
		vertices[1].y = (float)(y+height);
		vertices[1].z = 0.0f;
		vertices[2].x = (float)(x+width);
		vertices[2].y = (float)y;
		vertices[2].z = 0.0f;
		vertices[3].x = (float)(x+width);
		vertices[3].y = (float)(y+height);
		vertices[3].z = 0.0f;
		for(int i=0;i<4;i++) {
			vertices[i].color = renderer->c_color;
		}
		if( FAILED( renderer->m_pDevice->SetFVF( D3DFVF_VERTEX2D ) ) ) {
			LOG("failed to SetFVF\n");
		}
		if( FAILED( renderer->m_pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vertices, sizeof(VERTEX2D)) ) ) {
			LOG("failed to DrawPrimitiveUP\n");
		}
		/*this->renderer->m_pDevice->SetStreamSource( this->renderer->stream[D3D9Renderer::STREAM_NORMALS], this->vb, offset, sizeof(D3DXVECTOR3) ) ) ) {
		if( FAILED( this->renderer->m_pDevice->SetVertexDeclaration( this->renderer->m_pDecl_PC ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "SetVertexDeclaration failed"));
		}
		if( FAILED( this->renderer->m_pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 2 ) ) ) {
			Vision::setError(new VisionException(renderer, VisionException::V_DIRECT3D_ERROR, "DrawPrimitive failed"));
		}*/
	}
	else {
		/*VERTEX2D_TEXTURED vertices[4];
		vertices[0].x = (float)x;
		vertices[0].y = (float)y;
		vertices[0].z = 0.0f;
		vertices[0].u = 0.0f;
		vertices[0].v = 0.0f;
		vertices[1].x = (float)x;
		vertices[1].y = (float)(y+height);
		vertices[1].z = 0.0f;
		vertices[1].u = 0.0f;
		vertices[1].v = 1.0;
		vertices[2].x = (float)(x+width);
		vertices[2].y = (float)y;
		vertices[2].z = 0.0f;
		vertices[2].u = 1.0;
		vertices[2].v = 0.0f;
		vertices[3].x = (float)(x+width);
		vertices[3].y = (float)(y+height);
		vertices[3].z = 0.0f;
		vertices[3].u = 1.0;
		vertices[3].v = 1.0;*/
		VERTEX2D_TEXTURED vertices[4];
		vertices[0].x = (float)x;
		vertices[0].y = (float)y;
		vertices[0].z = 0.0f;
		vertices[1].x = (float)x;
		vertices[1].y = (float)(y+height);
		vertices[1].z = 0.0f;
		vertices[2].x = (float)(x+width);
		vertices[2].y = (float)y;
		vertices[2].z = 0.0f;
		vertices[3].x = (float)(x+width);
		vertices[3].y = (float)(y+height);
		vertices[3].z = 0.0f;
		/*vertices[0].u = 0.0f;
		vertices[0].v = 1.0f;
		vertices[1].u = 0.0f;
		vertices[1].v = 0.0;
		vertices[2].u = 1.0;
		vertices[2].v = 1.0f;
		vertices[3].u = 1.0;
		vertices[3].v = 0.0;*/
		for(int i=0;i<4;i++) {
			vertices[i].u = tu[i];
			vertices[i].v = tv[i];
			vertices[i].color = renderer->c_color;
		}
		/*D3D9Texture *d_texture = static_cast<D3D9Texture *>(texture); // cast to the internal representation
		if( FAILED( renderer->m_pDevice->SetTexture(renderer->c_textureunit, d_texture->getD3DTexture()) ) ) {
			LOG("failed to set texture\n");
		}*/
		this->texture->enable();
		if( FAILED( renderer->m_pDevice->SetFVF( D3DFVF_VERTEX2D_TEXTURED ) ) ) {
			LOG("failed to SetFVF\n");
		}
		if( FAILED( renderer->m_pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vertices, sizeof(VERTEX2D_TEXTURED)) ) ) {
			LOG("failed to DrawPrimitiveUP\n");
		}
		//renderer->m_pDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 1, vertices, sizeof(VERTEX2D_TEXTURED));
		if( FAILED( renderer->m_pDevice->SetTexture(renderer->c_textureunit, NULL) ) ) {
			LOG("failed to disable texture\n");
		}
	}
}

void D3D9Graphics2D::fillTriangle(const short *x,const short *y) const {
	// n.b., doesn't support textures
	VERTEX2D vertices[3];
	for(int i=0;i<3;i++) {
		vertices[i].x = (float)x[i];
		vertices[i].y = (float)y[i];
		vertices[i].z = 0.0f;
		vertices[i].color = renderer->c_color;
	}
	if( FAILED( renderer->m_pDevice->SetFVF( D3DFVF_VERTEX2D ) ) ) {
		LOG("failed to SetFVF\n");
	}
	if( FAILED( renderer->m_pDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 1, vertices, sizeof(VERTEX2D)) ) ) {
		LOG("failed to DrawPrimitiveUP\n");
	}
}

D3D9Renderer::D3D9Renderer(GraphicsEnvironment *genv, LPDIRECT3D9 pD3D, LPDIRECT3DDEVICE9 pDevice) : Renderer(genv), m_pD3D(pD3D), m_pDevice(pDevice) {
	LOG("Initialising Direct3D 9 Renderer...\n");
	//this->m_pDecl_PNTC = NULL;
	//this->m_pDecl_PNT = NULL;
	//this->m_pDecl_PNC = NULL;
	//this->m_pDecl_PN = NULL;
	//this->m_pDecl_PTC = NULL;
	//this->m_pDecl_PT = NULL;
	//this->m_pDecl_PC = NULL;
	this->m_pDecl_P = NULL;
	this->m_pDecl_PT0C = NULL;
	this->m_pDecl_PNT0C = NULL;
	this->m_pDecl_PT0T1C = NULL;
	this->m_pDecl_PNT0T1C = NULL;
	//this->m_pDecl_PNT0T2C = NULL;
	//this->m_pDecl_PNT1C = NULL;
	this->m_pDecl_PNT0T1T2C = NULL;
	this->c_pDecl = NULL;
	this->c_transform = D3DTS_FORCE_DWORD;
	this->c_drawing_mode = VertexArray::DRAWINGMODE_TRIANGLES;
	this->clear_color = 0;
	this->setClearColor(0, 0, 0, 0);
	this->c_color = D3DCOLOR_RGBA(255, 255, 255, 255);
	this->cull_face = true;
	this->front_face = true;
	this->shader_clipping = false;

	for(int i=0;i<N_STREAMS;i++) {
		stream[i] = 0;
	}

	this->cgContext = NULL;
	this->cgVertexProfile = CG_PROFILE_UNKNOWN;
	this->cgPixelProfile = CG_PROFILE_UNKNOWN;

	D3DXCreateSprite(m_pDevice, &sprite);

	//this->beginScene();
	/*D3DVERTEXELEMENT9 declDesc_PNTC[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
        {2, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        {3, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PNTC, &m_pDecl_PNTC );*/

	/*D3DVERTEXELEMENT9 declDesc_PNT[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
        {2, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PNT, &m_pDecl_PNT );*/

	/*D3DVERTEXELEMENT9 declDesc_PNC[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
        {2, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PNC, &m_pDecl_PNC );*/

	/*D3DVERTEXELEMENT9 declDesc_PN[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PN, &m_pDecl_PN );*/

	/*D3DVERTEXELEMENT9 declDesc_PTC[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        {2, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PTC, &m_pDecl_PTC );*/

	/*D3DVERTEXELEMENT9 declDesc_PT[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PT, &m_pDecl_PT );*/

	/*D3DVERTEXELEMENT9 declDesc_PC[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PC, &m_pDecl_PC );*/

	D3DVERTEXELEMENT9 declDesc_P[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_P, &m_pDecl_P );

	D3DVERTEXELEMENT9 declDesc_PT0C[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        {2, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PT0C, &m_pDecl_PT0C );

	D3DVERTEXELEMENT9 declDesc_PNT0C[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
        {2, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        {3, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PNT0C, &m_pDecl_PNT0C );

	D3DVERTEXELEMENT9 declDesc_PT0T1C[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        {2, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        {3, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PT0T1C, &m_pDecl_PT0T1C );

	D3DVERTEXELEMENT9 declDesc_PNT0T1C[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
        {2, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        {3, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        {4, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PNT0T1C, &m_pDecl_PNT0T1C );

	/*D3DVERTEXELEMENT9 declDesc_PNT0T2C[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
        {2, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        {3, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        {4, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PNT0T2C, &m_pDecl_PNT0T2C );*/

	/*D3DVERTEXELEMENT9 declDesc_PNT1C[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
        {2, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        {3, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PNT1C, &m_pDecl_PNT1C );*/

	D3DVERTEXELEMENT9 declDesc_PNT0T1T2C[] = 
    {
        {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
        {1, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
        {2, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
        {3, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
        {4, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
        {5, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2},
        D3DDECL_END()
    };
    m_pDevice->CreateVertexDeclaration( declDesc_PNT0T1T2C, &m_pDecl_PNT0T1T2C );

	if( FAILED( D3DXCreateMatrixStack(0, &this->stack_model) ) ) {
		Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"D3DXCreateMatrixStack failed"));
	}
	if( FAILED( D3DXCreateMatrixStack(0, &this->stack_projection) ) ) {
		Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"D3DXCreateMatrixStack failed"));
	}
	for(int i=0;i<MAX_TEXUNITS;i++) {
		if( FAILED( D3DXCreateMatrixStack(0, &this->stack_texture[i]) ) ) {
			Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"D3DXCreateMatrixStack failed"));
		}
	}
	c_stack = NULL;

	c_textureunit = 0;

	/*unsigned char uniformColor[4] = {255, 255, 255, 255};
	this->uniformColorArray = static_cast<D3D9VertexArray *>(createVertexArray(VertexArray::DATATYPE_COLORS, 0, true, &uniformColor, 4*sizeof(unsigned char), 4));
	this->using_uniform_color = false;

	float uniformTexcoord[2] = {0.0f, 0.0f};
	this->uniformTexcoordArray = static_cast<D3D9VertexArray *>(createVertexArray(VertexArray::DATATYPE_TEXCOORDS, 0, true, &uniformTexcoord, 2*sizeof(float), 2));
	this->using_uniform_texcoord0 = false;
	this->using_uniform_texcoord1 = false;*/

	setCulling();
	this->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);
	this->setDepthBufferFunc(Renderer::RENDERSTATE_DEPTHFUNC_LESS);
	//DWORD val = 0;
	//this->m_pDevice->GetRenderState(D3DRS_ZENABLE, &val);
	//this->m_pDevice->GetRenderState(D3DRS_CULLMODE, &val);


	if( genv->wantShaders() ) {
		this->initCG();
	}

	LOG("    done\n");
}

D3D9Renderer::~D3D9Renderer() {
/*#ifdef _DEBUG
	ASSERT( _CrtCheckMemory() );
#endif*/
	//m_pDecl_PNTC->Release();
	//m_pDecl_PNT->Release();
	//m_pDecl_PNC->Release();
	//m_pDecl_PN->Release();
	//m_pDecl_PTC->Release();
	//m_pDecl_PT->Release();
	//m_pDecl_PC->Release();
	m_pDecl_P->Release();
	m_pDecl_PT0C->Release();
	m_pDecl_PNT0C->Release();
	m_pDecl_PT0T1C->Release();
	m_pDecl_PNT0T1C->Release();
	//m_pDecl_PNT0T2C->Release();
	//m_pDecl_PNT1C->Release();
	m_pDecl_PNT0T1T2C->Release();
	stack_model->Release();
	stack_projection->Release();
	for(int i=0;i<MAX_TEXUNITS;i++) {
		stack_texture[i]->Release();
	}
	sprite->Release();
	/*delete this->uniformColorArray;
	delete this->uniformTexcoordArray;*/
	/*if( m_pDevice != NULL) {
		//m_pDevice->EndScene();
        m_pDevice->Release();
	}*/

	/*for(vector<Shader *>::iterator iter = shaders.begin(); iter != shaders.end(); ++iter) {
		Shader *shader = *iter;
		delete shader;
	}*/
	for(vector<ShaderEffect *>::iterator iter = shader_effects.begin(); iter != shader_effects.end(); ++iter) {
		ShaderEffect *shader_effect = *iter;
		delete shader_effect;
	}
	if( cgContext != NULL ) {
		cgD3D9SetDevice(NULL); // needed to avoid hang on exit, on Intel GMA 950
		cgDestroyContext(cgContext);
		cgContext = NULL;
	}
}

void D3D9Renderer::setToDefaults() {
	D3DMATERIAL9 mtrl;
	ZeroMemory( &mtrl, sizeof(mtrl) );
	mtrl.Diffuse.r = mtrl.Ambient.r = 1.0f;
	mtrl.Diffuse.g = mtrl.Ambient.g = 1.0f;
	mtrl.Diffuse.b = mtrl.Ambient.b = 1.0f;
	mtrl.Diffuse.a = mtrl.Ambient.a = 1.0f;
	mtrl.Power = 64;
	m_pDevice->SetMaterial( &mtrl );

	m_pDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
    m_pDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

	// we want equivalent of glEnable(GL_COLOR_MATERIAL); glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	m_pDevice->SetRenderState( D3DRS_COLORVERTEX, TRUE ); // should be default
	m_pDevice->SetRenderState( D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1 ); // should be default
	m_pDevice->SetRenderState( D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_COLOR1 ); // *not* the default - by default is taken from material

	m_pDevice->SetRenderState( D3DRS_SPECULARENABLE, TRUE ); // *not* the default - by default, specular is off
	m_pDevice->SetRenderState( D3DRS_SPECULARMATERIALSOURCE , D3DMCS_MATERIAL ); // *not* the default - by default is taken from color2
	//m_pDevice->SetRenderState( D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_MATERIAL );
	//m_pDevice->SetRenderState( D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL );

	//m_pDevice->SetRenderState( D3DRS_SPECULARENABLE, FALSE );
}

void D3D9Renderer::drawFrameTest() const {
#if 0
	m_pDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
	m_pDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 1, g_test_vertices, sizeof(CUSTOMVERTEX));
#endif
}

/*Shader *D3D9Renderer::createShaderCG(const char *file, const char *func, bool vertex_shader) {
	if( !this->genv->hasShaders() )
		return NULL;
	CGD3D9Shader *shader = new CGD3D9Shader(this, vertex_shader);
	LOG("Loading %s shader %d : %s / %s\n", vertex_shader?"vertex":"pixel", shader, file, func);
	const SDL_D3D9GraphicsEnvironment *d_genv = static_cast<const SDL_D3D9GraphicsEnvironment *>(genv);
	shader->cgProfile = vertex_shader ? d_genv->cgVertexProfile : d_genv->cgPixelProfile;

	const char **profileOpts = cgD3D9GetOptimalOptions(shader->cgProfile);

	shader->cgProgram = cgCreateProgramFromFile(d_genv->cgContext, CG_SOURCE, file, shader->cgProfile, func, profileOpts);
	if( shader->cgProgram == NULL ) {
		LOG("### Failed to load %s shader:\n", vertex_shader?"vertex":"pixel");
		CGerror Error = cgGetError();
		LOG(cgGetErrorString(Error));
		LOG("\n");
		delete shader;
		//this->cgContext = NULL;
		return NULL;
	}
	cgD3D9LoadProgram(shader->cgProgram, FALSE, 0);
	shader->init();
	//g3->addShader(shader);

	return shader;
}*/

void D3D9Renderer::setViewport(int width, int height) {
	D3DVIEWPORT9 viewport;
	viewport.X = 0;
	viewport.Y = 0;
	viewport.Width = width;
	viewport.Height = height;
	viewport.MinZ = 0.0f;
	viewport.MaxZ = 1.0f;
	if( FAILED( this->m_pDevice->SetViewport(&viewport) ) ) {
		Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9Renderer::setViewport - failed to set viewport\n"));
	}
}

void D3D9Renderer::ortho2D(int width, int height) {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	D3DXMATRIX matrix;
	//D3DXMatrixOrthoRH(&matrix, width, height, -1.0, 1.0);
	//D3DXMatrixOrthoOffCenterRH(&matrix, 0, width, height, 0, -1.0, 1.0);
	D3DXMatrixOrthoOffCenterRH(&matrix, 0.0f, (float)width, (float)height, 0.0f, 0.0f, 1.0f);
	this->m_pDevice->SetTransform(D3DTS_PROJECTION, &matrix);
}

void D3D9Renderer::resizeScene(float frustum[6][4], int width, int height, bool persp, float z_near, float z_far, float fovy) {
	this->z_near = z_near;
	this->z_far = z_far;
	this->persp = persp;
	//setViewport(width, height); // stops clear from working on Intel?!
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	D3DXMATRIX matrix;
	D3DXMatrixIdentity(&matrix);
	this->m_pDevice->SetTransform(D3DTS_WORLD, &matrix);

	/*D3DXVECTOR3 vEyePt   ( 0.0f, 0.0f,0.0f );
	D3DXVECTOR3 vLookatPt( 1.0f, 0.0f, 0.0f );
	D3DXVECTOR3 vUpVec   ( 0.0f, 1.0f, 0.0f );
	D3DXMatrixLookAtLH( &matrix, &vEyePt, &vLookatPt, &vUpVec );*/
    //D3DXVECTOR3 vEyePt( 0.0f, 3.0f,-5.0f );
    //D3DXVECTOR3 vEyePt( 0.0f, 3.0f, 10.0f );

	/*D3DXVECTOR3 vEyePt( 5.0f, 5.0f, 10.0f );
    D3DXVECTOR3 vLookatPt( 0.0f, 0.0f, 0.0f );
    D3DXVECTOR3 vUpVec( 0.0f, 1.0f, 0.0f );
    D3DXMatrixLookAtLH( &matrix, &vEyePt, &vLookatPt, &vUpVec );*/
	this->m_pDevice->SetTransform(D3DTS_VIEW, &matrix);

	//D3DXMatrixPerspectiveFovLH(&matrix, fovy * M_PI / 180.0f, (float)width/(float)height, z_near, z_far);
	D3DXMatrixPerspectiveFovRH(&matrix, fovy * (float)M_PI / 180.0f, (float)width/(float)height, z_near, z_far);
	//D3DXMatrixOrthoRH(&matrix, width, height, z_near, z_far);
	//D3DXMatrixOrthoOffCenterLH(&matrix, 0, width, 0, height, z_near, z_far);
    //D3DXMatrixPerspectiveFovLH( &matrix, D3DX_PI/4, 1.0f, 1.0f, 100.0f );
	// we do this, as the code expects there to be a projection matrix (for the 3D viewing mode) on the stack
	this->switchToProjection();
	this->popMatrix();
	this->m_pDevice->SetTransform(D3DTS_PROJECTION, &matrix);

	double clip[16];
	float t = 0.0f;
	D3DXMATRIX proj;
	D3DXMATRIX modl;
	this->m_pDevice->GetTransform(D3DTS_PROJECTION, &proj);
	this->m_pDevice->GetTransform(D3DTS_WORLD, &modl);

	/* Combine the two matrices (multiply projection by modelview) */
	clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
	clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
	clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
	clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

	clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
	clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
	clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
	clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

	clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
	clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
	clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
	clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

	clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
	clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
	clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
	clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];

	/* Extract the numbers for the RIGHT plane */
	frustum[0][0] = (float)(clip[ 3] - clip[ 0]);
	frustum[0][1] = (float)(clip[ 7] - clip[ 4]);
	frustum[0][2] = (float)(clip[11] - clip[ 8]);
	frustum[0][3] = (float)(clip[15] - clip[12]);
	/* Normalize the result */
	t = sqrt( frustum[0][0] * frustum[0][0] + frustum[0][1] * frustum[0][1] + frustum[0][2] * frustum[0][2] );
	frustum[0][0] /= t;
	frustum[0][1] /= t;
	frustum[0][2] /= t;
	frustum[0][3] /= t;
	LOG("FRUSTUM RIGHT %f %f %f %f\n", frustum[0][0],  frustum[0][1], frustum[0][2], frustum[0][3]);

	/* Extract the numbers for the LEFT plane */
	frustum[1][0] = (float)(clip[ 3] + clip[ 0]);
	frustum[1][1] = (float)(clip[ 7] + clip[ 4]);
	frustum[1][2] = (float)(clip[11] + clip[ 8]);
	frustum[1][3] = (float)(clip[15] + clip[12]);
	/* Normalize the result */
	t = sqrt( frustum[1][0] * frustum[1][0] + frustum[1][1] * frustum[1][1] + frustum[1][2] * frustum[1][2] );
	frustum[1][0] /= t;
	frustum[1][1] /= t;
	frustum[1][2] /= t;
	frustum[1][3] /= t;
	LOG("FRUSTUM LEFT %f %f %f %f\n", frustum[1][0],  frustum[1][1], frustum[1][2], frustum[1][3]);

	/* Extract the BOTTOM plane */
	frustum[2][0] = (float)(clip[ 3] + clip[ 1]);
	frustum[2][1] = (float)(clip[ 7] + clip[ 5]);
	frustum[2][2] = (float)(clip[11] + clip[ 9]);
	frustum[2][3] = (float)(clip[15] + clip[13]);
	/* Normalize the result */
	t = sqrt( frustum[2][0] * frustum[2][0] + frustum[2][1] * frustum[2][1] + frustum[2][2] * frustum[2][2] );
	frustum[2][0] /= t;
	frustum[2][1] /= t;
	frustum[2][2] /= t;
	frustum[2][3] /= t;
	LOG("FRUSTUM BOTTOM %f %f %f %f\n", frustum[2][0],  frustum[2][1], frustum[2][2], frustum[2][3]);

	/* Extract the TOP plane */
	frustum[3][0] = (float)(clip[ 3] - clip[ 1]);
	frustum[3][1] = (float)(clip[ 7] - clip[ 5]);
	frustum[3][2] = (float)(clip[11] - clip[ 9]);
	frustum[3][3] = (float)(clip[15] - clip[13]);
	/* Normalize the result */
	t = sqrt( frustum[3][0] * frustum[3][0] + frustum[3][1] * frustum[3][1] + frustum[3][2] * frustum[3][2] );
	frustum[3][0] /= t;
	frustum[3][1] /= t;
	frustum[3][2] /= t;
	frustum[3][3] /= t;
	LOG("FRUSTUM TOP %f %f %f %f\n", frustum[3][0],  frustum[3][1], frustum[3][2], frustum[3][3]);

	/* Extract the FAR plane */
	frustum[4][0] = (float)(clip[ 3] - clip[ 2]);
	frustum[4][1] = (float)(clip[ 7] - clip[ 6]);
	frustum[4][2] = (float)(clip[11] - clip[10]);
	frustum[4][3] = (float)(clip[15] - clip[14]);
	/* Normalize the result */
	t = sqrt( frustum[4][0] * frustum[4][0] + frustum[4][1] * frustum[4][1] + frustum[4][2] * frustum[4][2] );
	frustum[4][0] /= t;
	frustum[4][1] /= t;
	frustum[4][2] /= t;
	frustum[4][3] /= t;
	LOG("FRUSTUM FAR %f %f %f %f\n", frustum[4][0],  frustum[4][1], frustum[4][2], frustum[4][3]);

	/* Extract the NEAR plane */
	frustum[5][0] = (float)(clip[ 3] + clip[ 2]);
	frustum[5][1] = (float)(clip[ 7] + clip[ 6]);
	frustum[5][2] = (float)(clip[11] + clip[10]);
	frustum[5][3] = (float)(clip[15] + clip[14]);
	/* Normalize the result */
	t = sqrt( frustum[5][0] * frustum[5][0] + frustum[5][1] * frustum[5][1] + frustum[5][2] * frustum[5][2] );
	frustum[5][0] /= t;
	frustum[5][1] /= t;
	frustum[5][2] /= t;
	frustum[5][3] /= t;
	LOG("FRUSTUM NEAR %f %f %f %f\n", frustum[5][0],  frustum[5][1], frustum[5][2], frustum[5][3]);

	this->pushMatrix();
	this->ortho2D(width, height);

	this->switchToModelview(); // set to model view by default

	//setViewport(width, height); // stops clear from working on Intel?!

}

void D3D9Renderer::clear(bool color, bool depth, bool stencil) const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	if( color || depth || stencil ) {
		DWORD dw = 0;
		if( color )
			dw |= D3DCLEAR_TARGET;
		if( depth )
			dw |= D3DCLEAR_ZBUFFER;
		if( stencil ) // TODO: also check if stencil buffer available?
			dw |= D3DCLEAR_STENCIL;
		m_pDevice->Clear(0, NULL, dw, this->clear_color, 1.0f, 0);
		//m_pDevice->Clear( 0, NULL, dw, D3DCOLOR_XRGB(0,255,0), 1.0f, 0 );
	}
	//m_pDevice->Clear(0, NULL, D3DCLEAR_TARGET, this->clear_color, 1.0f, 0);
    //m_pDevice->Clear( 0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0,255,0), 1.0f, 0 );
}

void D3D9Renderer::setDepthBufferMode(RenderState state) {
	if( state == RENDERSTATE_DEPTH_READWRITE ) {
		this->m_pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
		this->m_pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	}
	else if( state == RENDERSTATE_DEPTH_READONLY ) {
		this->m_pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
		this->m_pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	}
	else if( state == RENDERSTATE_DEPTH_NONE ) {
		this->m_pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
		this->m_pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setDepthBufferMode"));
	}
}

void D3D9Renderer::setDepthBufferFunc(RenderState state) {
	if( state == RENDERSTATE_DEPTHFUNC_EQUAL ) {
		this->m_pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_EQUAL);
	}
	else if( state == RENDERSTATE_DEPTHFUNC_LESS ) {
		this->m_pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESS);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setDepthBufferFunc"));
	}
}

void D3D9Renderer::setBlendMode(RenderState state) {
	if( state == RENDERSTATE_BLEND_BOTH ) {
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
	}
	else if( state == RENDERSTATE_BLEND_ADDITIVE ) {
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
	}
	else if( state == RENDERSTATE_BLEND_TRANSPARENCY ) {
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
	}
	else if( state == RENDERSTATE_BLEND_REVERSE_TRANSPARENCY ) {
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_INVDESTALPHA) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_DESTALPHA) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
	}
	else if( state == RENDERSTATE_BLEND_ONE_BY_SRCALPHA ) {
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCALPHA) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
	}
	else if( state == RENDERSTATE_BLEND_ONE_BY_ONEMINUSSRCALPHA ) {
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
	}
	else if( state == RENDERSTATE_BLEND_DST_BY_ZERO ) {
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_DESTCOLOR) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
		if( FAILED( this->m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO) ) )
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "Direct3D command returned error"));
	}
	else if( state == RENDERSTATE_BLEND_NONE ) {
		this->m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setBlendMode"));
	}
}

void D3D9Renderer::setAlphaTestMode(RenderState state) {
	if( state == RENDERSTATE_ALPHA_TEST_ON ) {
		this->m_pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE); 
		this->m_pDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);
		this->m_pDevice->SetRenderState(D3DRS_ALPHAREF, 127);
		/*glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, 0.5);*/
	}
	else if( state == RENDERSTATE_ALPHA_TEST_OFF ) {
		this->m_pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE); 
		//glDisable(GL_ALPHA_TEST);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setAlphaTestMode"));
	}
}

void D3D9Renderer::setStencilMode(RenderState state) {
	if( state == RENDERSTATE_STENCIL_REPLACE ) {
		this->m_pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);
		this->m_pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
		this->m_pDevice->SetRenderState(D3DRS_STENCILREF, 1);
		this->m_pDevice->SetRenderState(D3DRS_STENCILMASK, 0xFFFFFFFF);
		this->m_pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_REPLACE);
		this->m_pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_REPLACE);
		this->m_pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_REPLACE);
	}
	else if( state == RENDERSTATE_STENCIL_DRAWATSTENCIL_OVERDRAW ) {
		this->m_pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);
		this->m_pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_EQUAL);
		this->m_pDevice->SetRenderState(D3DRS_STENCILREF, 1);
		this->m_pDevice->SetRenderState(D3DRS_STENCILMASK, 0xFFFFFFFF);
		this->m_pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
		this->m_pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
		this->m_pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
	}
	else if( state == RENDERSTATE_STENCIL_DRAWATNOSTENCIL ) {
		this->m_pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);
		this->m_pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_EQUAL);
		this->m_pDevice->SetRenderState(D3DRS_STENCILREF, 0);
		this->m_pDevice->SetRenderState(D3DRS_STENCILMASK, 0xFFFFFFFF);
		this->m_pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
		this->m_pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_DECRSAT);
		this->m_pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_DECRSAT);
		//this->m_pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_DECR);
		//this->m_pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_DECR);
	}
	else if( state == RENDERSTATE_STENCIL_SHADOWPASS_ONE ) {
		this->m_pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);
		this->m_pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
		this->m_pDevice->SetRenderState(D3DRS_STENCILREF, 1);
		this->m_pDevice->SetRenderState(D3DRS_STENCILMASK, 0xFFFFFFFF);
		if( D3D9RendererInfo::twoSidedStencil ) {
			this->m_pDevice->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, TRUE);
			this->enableCullFace(false);

			this->m_pDevice->SetRenderState(D3DRS_CCW_STENCILFAIL, D3DSTENCILOP_KEEP);
			this->m_pDevice->SetRenderState(D3DRS_CCW_STENCILZFAIL, D3DSTENCILOP_KEEP);
			this->m_pDevice->SetRenderState(D3DRS_CCW_STENCILPASS, D3DSTENCILOP_INCR);

			this->m_pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
			this->m_pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
			this->m_pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_DECR);
		}
		else {
			this->m_pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
			this->m_pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
			this->m_pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_INCRSAT);
		}
		/*glEnable(GL_STENCIL_TEST);
		glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
		if( GLRendererInfo::separateStencilATI ) {
			glEnable(GL_STENCIL_TEST_TWO_SIDE_EXT);
			glDisable(GL_CULL_FACE);
			glStencilOpSeparateATI(GL_FRONT, GL_KEEP, GL_KEEP, GL_INCR_WRAP_EXT);
			glStencilOpSeparateATI(GL_BACK, GL_KEEP, GL_KEEP, GL_DECR_WRAP_EXT);
		}
		else if( GLRendererInfo::separateStencil ) {
			glEnable(GL_STENCIL_TEST_TWO_SIDE_EXT);
			glDisable(GL_CULL_FACE);
			glActiveStencilFaceEXT(GL_BACK);
			glStencilOp(GL_KEEP, GL_KEEP, GL_DECR_WRAP_EXT);
			glActiveStencilFaceEXT(GL_FRONT);
			glStencilOp(GL_KEEP, GL_KEEP, GL_INCR_WRAP_EXT);
		}
		else {
			glStencilOp( GL_KEEP, GL_KEEP, GL_INCR );
		}*/
	}
	else if( state == RENDERSTATE_STENCIL_SHADOWPASS_TWO ) {
		// assumes stencil already enabled and with func set
		//glStencilOp( GL_KEEP, GL_KEEP, GL_DECR );
		this->m_pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_DECRSAT);
	}
	else if( state == RENDERSTATE_STENCIL_SHADOWPASS_OFF ) {
		/*glDisable(GL_STENCIL_TEST_TWO_SIDE_EXT);
		glEnable(GL_CULL_FACE);*/
		this->m_pDevice->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
		this->enableCullFace(true);
	}
	else if( state == RENDERSTATE_STENCIL_NONE ) {
		this->m_pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setStencilMode"));
	}
}

void D3D9Renderer::enableClipPlane(const double pl[4]) {
	// not supported for fixed function

	if( !genv->hasShaders() ) {
		// no shaders
		return;
	}

	// user clip planes seem to be supported for fixed pipeline, but not always
	// when using shaders (e.g., NVIDIA 8600GT, Intel GMA950).
	// we need to set with _both_ methods, because a scene can mix shaders and non-shaders!

	TransformationMatrix *model_matrix = this->createTransformationMatrix();

	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->translate((float)(pl[3]*pl[0]), (float)(pl[3]*pl[1]), (float)(pl[3]*pl[2]));
	model_matrix->save();
	genv->getRenderer()->popMatrix();
	Vector3D new_p = model_matrix->getPos();

	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->translate((float)pl[0], (float)pl[1], (float)pl[2]);
	model_matrix->save();
	genv->getRenderer()->popMatrix();
	Vector3D new_n = model_matrix->getPos();
	model_matrix->save();
	new_n.x -= (*model_matrix)[12];
	new_n.y -= (*model_matrix)[13];
	new_n.z -= (*model_matrix)[14];
	ASSERT( new_n.magnitude() > 0 );
	new_n.normalise();
	delete model_matrix;

	// shader clipping
	this->shader_clipping = true;
	this->shader_clipping_plane[0] = new_n.x;
	this->shader_clipping_plane[1] = new_n.y;
	this->shader_clipping_plane[2] = new_n.z;
	this->shader_clipping_plane[3] = new_n % new_p;
}

void D3D9Renderer::reenableClipPlane() {
	// not supported for fixed function
	if( genv->hasShaders() ) {
		this->shader_clipping = true;
	}
}

void D3D9Renderer::enableScissor(int x,int y,int w,int h) {
    m_pDevice->SetRenderState( D3DRS_SCISSORTESTENABLE, TRUE );
	RECT rect = { x, y, x+w, y+h };
	m_pDevice->SetScissorRect(&rect);
}

void D3D9Renderer::reenableScissor() {
    m_pDevice->SetRenderState( D3DRS_SCISSORTESTENABLE, TRUE );
}

void D3D9Renderer::disableScissor() {
    m_pDevice->SetRenderState( D3DRS_SCISSORTESTENABLE, FALSE );
}

void D3D9Renderer::pushAttrib(bool viewport) {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	if( FAILED( this->m_pDevice->GetViewport(&saved_viewport) ) ) {
		Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9Renderer::pushAttrib - failed to get viewport\n"));
	}
}

void D3D9Renderer::popAttrib() const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	if( FAILED( this->m_pDevice->SetViewport(&saved_viewport) ) ) {
		Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9Renderer::popAttrib - failed to set viewport\n"));
	}
}

void D3D9Renderer::setFixedFunctionMatSpecular(const float specular[4]) {
	D3DMATERIAL9 mtrl;
	this->m_pDevice->GetMaterial(&mtrl);
	mtrl.Specular.r = specular[0];
	mtrl.Specular.g = specular[1];
	mtrl.Specular.b = specular[2];
	mtrl.Specular.a = specular[3];
	/*mtrl.Specular.r = 1;
	mtrl.Specular.g = 0;
	mtrl.Specular.b = 0;
	mtrl.Specular.a = 1;*/
	this->m_pDevice->SetMaterial(&mtrl);
	// shaders
	/*if( c_vertex_shader != NULL )
		c_vertex_shader->setMatSpecular(specular);
	if( c_pixel_shader != NULL )
		c_pixel_shader->setMatSpecular(specular);*/
}

/*void D3D9Renderer::setFixedFunctionMatColor(const float color[4]) {
	D3DMATERIAL9 mtrl;
	this->m_pDevice->GetMaterial(&mtrl);
	mtrl.Diffuse.r = mtrl.Ambient.r = color[0];
	mtrl.Diffuse.g = mtrl.Ambient.g = color[1];
	mtrl.Diffuse.b = mtrl.Ambient.b = color[2];
	mtrl.Diffuse.a = mtrl.Ambient.a = color[3];
	this->m_pDevice->SetMaterial(&mtrl);
}*/

void D3D9Renderer::enableFixedFunctionLight(unsigned int index, const float ambient[4], const float diffuse[4], const float specular[4], const float attenuation[3], const float position[3], bool directional) {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	ASSERT( index >= 0 && index < RendererInfo::maxLights );
	D3DLIGHT9 d3dlight;
	ZeroMemory( &d3dlight, sizeof(d3dlight) );
	if( directional ) {
		d3dlight.Type = D3DLIGHT_DIRECTIONAL;
		//d3dlight.Direction = D3DXVECTOR3(position[0], position[1], position[2]);
		d3dlight.Direction = D3DXVECTOR3(-position[0], -position[1], -position[2]);
	}
	else {
		d3dlight.Type = D3DLIGHT_POINT;
		d3dlight.Position = D3DXVECTOR3(position[0], position[1], position[2]);
		d3dlight.Range = 1000.0; // TODO: fix?
		/*d3dlight.Attenuation0 = light->getLightAttenuation(0);
		d3dlight.Attenuation1 = light->getLightAttenuation(1);
		d3dlight.Attenuation2 = light->getLightAttenuation(2);*/
		d3dlight.Attenuation0 = attenuation[0];
		d3dlight.Attenuation1 = attenuation[1];
		d3dlight.Attenuation2 = attenuation[2];
	}
	/*float f[4];
	light->getLightAmbient().floatArray(f);
	d3dlight.Ambient.r = f[0];
	d3dlight.Ambient.g = f[1];
	d3dlight.Ambient.b = f[2];
	d3dlight.Ambient.a = f[3];
	light->getLightDiffuse().floatArray(f);
	d3dlight.Diffuse.r = f[0];
	d3dlight.Diffuse.g = f[1];
	d3dlight.Diffuse.b = f[2];
	d3dlight.Diffuse.a = f[3];
	light->getLightSpecular().floatArray(f);
	d3dlight.Specular.r = f[0];
	d3dlight.Specular.g = f[1];
	d3dlight.Specular.b = f[2];
	d3dlight.Specular.a = f[3];*/
	d3dlight.Ambient.r = ambient[0];
	d3dlight.Ambient.g = ambient[1];
	d3dlight.Ambient.b = ambient[2];
	d3dlight.Ambient.a = ambient[3];

	d3dlight.Diffuse.r = diffuse[0];
	d3dlight.Diffuse.g = diffuse[1];
	d3dlight.Diffuse.b = diffuse[2];
	d3dlight.Diffuse.a = diffuse[3];

	d3dlight.Specular.r = specular[0];
	d3dlight.Specular.g = specular[1];
	d3dlight.Specular.b = specular[2];
	d3dlight.Specular.a = specular[3];

	this->m_pDevice->SetLight(index, &d3dlight);
	this->m_pDevice->LightEnable(index, TRUE);
	//LOG("enable light %d\n", index);
}

void D3D9Renderer::draw(const Vector3D *p0, const Vector3D *p1, const unsigned char rgba[4]) {
	VERTEX vertices[2];
	vertices[0].x = p0->x;
	vertices[0].y = p0->y;
	vertices[0].z = p0->z;
	vertices[1].x = p1->x;
	vertices[1].y = p1->y;
	vertices[1].z = p1->z;
	for(int i=0;i<2;i++) {
		vertices[i].color = D3DCOLOR_RGBA(rgba[0], rgba[1], rgba[2], rgba[3]);
	}
	this->m_pDevice->SetFVF( D3DFVF_VERTEX );
	this->m_pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, 1, vertices, sizeof(VERTEX));
}

void D3D9Renderer::render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count, const unsigned char *color_data, bool alpha, int color_repeat) {
	// TODO: optimise
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	int n_vertices = count;
	if( mode == VertexArray::DRAWINGMODE_LINE_LOOP ) {
		n_vertices++; // need to add extra vertex at end
	}
	VERTEX_TEXTURED *vertices = new VERTEX_TEXTURED[n_vertices];
	DWORD col = D3DCOLOR_RGBA(255, 255, 255, 255);
	for(int i=0,v=0,t=0,c=0;i<count;i++) {
		vertices[i].x = vertex_data[v++];
		vertices[i].y = vertex_data[v++];
		vertices[i].z = vertex_data[v++];
		if( texcoord_data != NULL ) {
			vertices[i].u = texcoord_data[t++];
			vertices[i].v = texcoord_data[t++];
		}
		else {
			vertices[i].u = 0;
			vertices[i].v = 0;
		}
		if( color_data != NULL && i % color_repeat == 0 ) {
			unsigned char r = color_data[c++];
			unsigned char g = color_data[c++];
			unsigned char b = color_data[c++];
			unsigned char a = 255;
			if( alpha ) {
				a = color_data[c++];
			}
			col = D3DCOLOR_RGBA(r, g, b, a);
		}
		vertices[i].color = col;
	}
	if( mode == VertexArray::DRAWINGMODE_LINE_LOOP ) {
		vertices[n_vertices-1] = vertices[0];
	}

	D3DPRIMITIVETYPE d3d9mode;
	int n_polys = 0;
	unsigned short *indices = NULL;
	if( mode == VertexArray::DRAWINGMODE_TRIANGLES ) {
		d3d9mode = D3DPT_TRIANGLELIST;
		n_polys = count / 3;
	}
	else if( mode == VertexArray::DRAWINGMODE_QUADS ) {
		// have to draw with triangles instead!
		d3d9mode = D3DPT_TRIANGLELIST;
		n_polys = count / 2;
		indices = new unsigned short[n_polys*3];
		for(int i=0,index=0,v=0;i<n_polys/2;i++) {
			indices[index++] = v;
			indices[index++] = v+1;
			indices[index++] = v+2;
			indices[index++] = v;
			indices[index++] = v+2;
			indices[index++] = v+3;
			v += 4;
		}
	}
	else if( mode == VertexArray::DRAWINGMODE_LINE_LOOP || mode == VertexArray::DRAWINGMODE_LINE_STRIP ) {
		d3d9mode = D3DPT_LINESTRIP;
		n_polys = n_vertices - 1;
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer::render render mode not supported\n"));
	}

	this->m_pDevice->SetFVF( D3DFVF_VERTEX_TEXTURED );
	if( indices == NULL ) {
		//this->m_pDevice->DrawPrimitiveUP(d3d9mode, n_polys, vertices, sizeof(VERTEX));
		if( FAILED( this->m_pDevice->DrawPrimitiveUP(d3d9mode, n_polys, vertices, sizeof(VERTEX_TEXTURED)) ) ) {
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9Renderer::render DrawIndexedPrimitiveUP failed"));
		}
	}
	else {
		//this->m_pDevice->DrawIndexedPrimitiveUP(d3d9mode, 0, count, n_polys, indices, D3DFMT_INDEX16, vertices, sizeof(VERTEX));
		if( FAILED( this->m_pDevice->DrawIndexedPrimitiveUP(d3d9mode, 0, count, n_polys, indices, D3DFMT_INDEX16, vertices, sizeof(VERTEX_TEXTURED)) ) ) {
			Vision::setError(new VisionException(this, VisionException::V_DIRECT3D_ERROR, "D3D9Renderer::render DrawIndexedPrimitiveUP failed"));
		}
	}
	delete [] vertices;
	delete [] indices;
}

//void D3D9Renderer::startArrays(VertexFormat vertexFormat, bool material_color, bool have_texcoords0, bool have_texcoords1) {
void D3D9Renderer::startArrays(VertexFormat vertexFormat) {
#if 0
	/*
	// In OpenGL, shaders will pick up the material colour, as it's been set by glColor*(). This isn't supported
	// in D3D9 (colours must be per-vertex, and can't be set to a default). This is handled by manually setting up
	// a special array with a length of 1, and a stride of 0. However, there's no point specifying colours if they
	// aren't used by the shader - this happens when rendering shadows, for example.
	// Note that this code isn't needed for fixed function pipeline (since there we do pick of the material
	// colour), however, we enable it always for consistency.
	using_uniform_color = material_color;

	// the shaders we use expect TEXCOORD0 and TEXCOORD1 to be enabled; so if not present, we use the uniform texcoord array
	using_uniform_texcoord0 = !have_texcoords0;
	using_uniform_texcoord1 = !have_texcoords1;
	*/
#endif

	if( vertexFormat == VERTEXFORMAT_P ) {
		this->c_pDecl = this->m_pDecl_P;
		stream[STREAM_VERTICES] = 0;
		stream[STREAM_NORMALS] = -1;
		stream[STREAM_TEXCOORDS0] = -1;
		stream[STREAM_COLORS] = -1;
		stream[STREAM_TEXCOORDS1] = -1;
		stream[STREAM_TEXCOORDS2] = -1;
		/*
		// don't expect any data for colors or texcoords for this vertex format
		using_uniform_color = false;
		using_uniform_texcoord0 = false;
		using_uniform_texcoord1 = false;
		*/
	}
	else if( vertexFormat == VERTEXFORMAT_PT0C ) {
		this->c_pDecl = this->m_pDecl_PT0C;
		stream[STREAM_VERTICES] = 0;
		stream[STREAM_NORMALS] = -1;
		stream[STREAM_TEXCOORDS0] = 1;
		stream[STREAM_COLORS] = 2;
		stream[STREAM_TEXCOORDS1] = -1;
		stream[STREAM_TEXCOORDS2] = -1;
	}
	else if( vertexFormat == VERTEXFORMAT_PNT0C ) {
		this->c_pDecl = this->m_pDecl_PNT0C;
		stream[STREAM_VERTICES] = 0;
		stream[STREAM_NORMALS] = 1;
		stream[STREAM_TEXCOORDS0] = 2;
		stream[STREAM_COLORS] = 3;
		stream[STREAM_TEXCOORDS1] = -1;
		stream[STREAM_TEXCOORDS2] = -1;
	}
	else if( vertexFormat == VERTEXFORMAT_PT0T1C ) {
		this->c_pDecl = this->m_pDecl_PT0T1C;
		stream[STREAM_VERTICES] = 0;
		stream[STREAM_NORMALS] = -1;
		stream[STREAM_TEXCOORDS0] = 1;
		stream[STREAM_COLORS] = 2;
		stream[STREAM_TEXCOORDS1] = 3;
		stream[STREAM_TEXCOORDS2] = -1;
	}
	else if( vertexFormat == VERTEXFORMAT_PNT0T1C ) {
		this->c_pDecl = this->m_pDecl_PNT0T1C;
		stream[STREAM_VERTICES] = 0;
		stream[STREAM_NORMALS] = 1;
		stream[STREAM_TEXCOORDS0] = 2;
		stream[STREAM_COLORS] = 3;
		stream[STREAM_TEXCOORDS1] = 4;
		stream[STREAM_TEXCOORDS2] = -1;
	}
	else if( vertexFormat == VERTEXFORMAT_PNT0T1T2C ) {
		this->c_pDecl = this->m_pDecl_PNT0T1T2C;
		stream[STREAM_VERTICES] = 0;
		stream[STREAM_NORMALS] = 1;
		stream[STREAM_TEXCOORDS0] = 2;
		stream[STREAM_COLORS] = 3;
		stream[STREAM_TEXCOORDS1] = 4;
		stream[STREAM_TEXCOORDS2] = 5;
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer::startArrays options not supported\n"));
	}

	/*if( using_uniform_color ) {
		//this->uniformColorArray->renderColors(true, false, 0, );
		// set the stream manually, rather than using renderColors, to set stride of zero
		this->m_pDevice->SetStreamSource( stream[STREAM_COLORS], this->uniformColorArray->getVB(), 0, 0 );
	}
	if( using_uniform_texcoord0 ) {
		this->m_pDevice->SetStreamSource( stream[STREAM_TEXCOORDS0], this->uniformTexcoordArray->getVB(), 0, 0 );
	}
	if( using_uniform_texcoord1 ) {
		this->m_pDevice->SetStreamSource( stream[STREAM_TEXCOORDS1], this->uniformTexcoordArray->getVB(), 0, 0 );
	}*/
}

void D3D9Renderer::transform(const Vector3D *pos, const Quaternion *rot, bool infinite) {
	// TODO: improve performance, by copying to D3DXMATRIX directly?
	float matrix[16];
	/*rot->convertInverseToGLMatrix(matrix); // Direct3D has matrices in transposed order to OpenGL
	matrix[3] = pos->x;
	matrix[7] = pos->y;
	matrix[11] = pos->z;
	if( infinite ) {
		matrix[15] = 0.0;
	}*/
	rot->convertToGLMatrix(matrix);
	matrix[12] = pos->x;
	matrix[13] = pos->y;
	matrix[14] = pos->z;
	if( infinite ) {
		matrix[15] = 0.0;
	}
	D3DXMATRIX dxmatrix(matrix);
	//D3DXMatrixTranslation(&dxmatrix, pos->x, pos->y, pos->z);
	this->m_pDevice->MultiplyTransform(this->c_transform, &dxmatrix);
}

void D3D9Renderer::transform(const Quaternion *rot) {
	// TODO: improve performance, by copying to D3DXMATRIX directly?
	float matrix[16];
	rot->convertToGLMatrix(matrix);
	D3DXMATRIX dxmatrix(matrix);
	this->m_pDevice->MultiplyTransform(this->c_transform, &dxmatrix);
}

void D3D9Renderer::transformInverse(const Quaternion *rot) {
	// TODO: improve performance, by copying to D3DXMATRIX directly?
	float matrix[16];
	rot->convertInverseToGLMatrix(matrix);
	D3DXMATRIX dxmatrix(matrix);
	this->m_pDevice->MultiplyTransform(this->c_transform, &dxmatrix);
}

void D3D9Renderer::getViewOrientation(Vector3D *right,Vector3D *up) {
	// TODO: optimise so we don't have to allocate a matrix? Just call the matrix->save D3D code directly
	TransformationMatrix *mat = this->createTransformationMatrix();
	mat->save();
	right->set((*mat)[0],(*mat)[4],(*mat)[8]);
	up->set((*mat)[1],(*mat)[5],(*mat)[9]);
	delete mat;
}

void D3D9Renderer::getViewDirection(Vector3D *dir) {
	// TODO: optimise so we don't have to allocate a matrix? Just call the matrix->save D3D code directly
	TransformationMatrix *mat = this->createTransformationMatrix();
	mat->save();
	dir->set((*mat)[2],(*mat)[6],(*mat)[10]);
	delete mat;
}

void D3D9Renderer::unbindFramebuffer() const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "D3D9Renderer:: not supported\n"));
	LPDIRECT3DSURFACE9 sourceSurface = NULL;
	this->m_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &sourceSurface);
	this->m_pDevice->SetRenderTarget(0, sourceSurface);
	sourceSurface->Release();
}

void D3D9Renderer::cgErrorCallback(void) {
	CGerror error = cgGetError();
	LOG("CG ERROR %d : %s\n", error, cgGetErrorString(error));
	const char *lastError = cgGetLastErrorString(NULL);
	if( lastError != NULL ) {
		LOG("Last Error String: %s\n", lastError);
	}
	//SDL_D3D9GraphicsEnvironment *genv = static_cast<SDL_D3D9GraphicsEnvironment *>(GraphicsEnvironment::getSingleton());
	const D3D9Renderer *renderer = static_cast<const D3D9Renderer *>(GraphicsEnvironment::getSingleton()->getRenderer());
	const char *lastListing = cgGetLastListing(renderer->cgContext);
	if( lastListing != NULL ) {
		LOG("Last Listing String: %s\n", lastListing);
	}
	Vision::setError(new VisionException(NULL,VisionException::V_CG_ERROR,"CG reported an error"));
}

void D3D9Renderer::initCG() {
	// setup CG
	//genv->checkError("D3D9Renderer::initCG enter");

	cgSetErrorCallback(cgErrorCallback);

	cgContext = cgCreateContext();
	if( cgContext != NULL ) {
		LOG("Successfully initialised CG Context\n");
		cgD3D9RegisterStates(cgContext); // needed for CGFX files

		cgD3D9SetDevice(m_pDevice);
		cgVertexProfile = cgD3D9GetLatestVertexProfile();
		if( cgVertexProfile == CG_PROFILE_UNKNOWN ) {
			LOG("### CG ERROR: Invalid vertex profile type\n");
			cgDestroyContext(cgContext);
			cgContext = NULL;
		}
		else {
			LOG("Successfully obtained vertex profile\n");
			//cgGLSetOptimalOptions(cgVertexProfile);

			cgPixelProfile = cgD3D9GetLatestPixelProfile();
			if( cgPixelProfile == CG_PROFILE_UNKNOWN ) {
				LOG("### CG ERROR: Invalid pixel profile type\n");
				cgDestroyContext(cgContext);
				cgContext = NULL;
			}
			else {
				LOG("Successfully obtained pixel profile\n");
				//cgGLSetOptimalOptions(cgPixelProfile);
			}
		}
	}
	else {
		LOG("### Failed to initialise CG Context\n");
	}

	//cgGLSetDebugMode(CG_FALSE);
	//genv->checkError("D3D9Renderer::initCG exit");
}

Shader *D3D9Renderer::createShaderCG(const char *file, const char *func, bool vertex_shader) {
	if( !this->hasShaders() ) {
		Vision::setError(new VisionException(this,VisionException::V_CG_ERROR, "Shaders not available"));
		return NULL;
	}
	CGD3D9Shader *shader = new CGD3D9Shader(this, vertex_shader);
	LOG("Loading %s CG shader %d : %s / %s\n", vertex_shader?"vertex":"pixel", shader, file, func);
	shader->cgProfile = vertex_shader ? this->cgVertexProfile : this->cgPixelProfile;

	const char **profileOpts = cgD3D9GetOptimalOptions(shader->cgProfile);

	shader->cgProgram = cgCreateProgramFromFile(this->cgContext, CG_SOURCE, file, shader->cgProfile, func, profileOpts);
	if( shader->cgProgram == NULL ) {
		LOG("### Failed to load %s CG shader:\n", vertex_shader?"vertex":"pixel");
		CGerror Error = cgGetError();
		LOG(cgGetErrorString(Error));
		LOG("\n");
		delete shader;
		Vision::setError(new VisionException(this,VisionException::V_CG_ERROR, "Failed to load CG program"));
		return NULL;
	}
	cgD3D9LoadProgram(shader->cgProgram, FALSE, 0);
	//shader->init();
	return shader;
}

ShaderEffect *D3D9Renderer::createShaderEffectCGFX(const char *file,const char **args) {
	if( !this->hasShaders() ) {
		Vision::setError(new VisionException(this,VisionException::V_CG_ERROR, "Shaders not available"));
		return NULL;
	}
	CGFXShaderEffect *shader_effect = new CGFXD3D9ShaderEffect(this, this->cgContext, file, args);
	LOG("Loading CGFX shader %d : %s\n", shader_effect, file);
	this->addShaderEffect(shader_effect);
	return shader_effect;
}

void D3D9Renderer::onDeviceLost() {
	// The device is lost, or is about to be reset. This is the time to Release() any resources
	// in the default pool, and call OnLostDevice() for any D3DX objects that need it.

	/*for(set<D3D9FontBuffer *>::iterator iter = this->store_fonts.begin(); iter != this->store_fonts.end();++iter) {
		D3D9FontBuffer *fb = *iter;
		fb->onDeviceLost();
	}*/
	for(set<D3D9Item *>::iterator iter = this->store.begin(); iter != this->store.end();++iter) {
		D3D9Item *item = *iter;
		//LOG("device lost: %d\n", item);
		item->onDeviceLost();
	}

	//LOG("device lost sprite: %d\n", sprite);
	sprite->OnLostDevice();

	this->m_pDevice->SetTexture(c_textureunit, NULL);

	//m_pDecl_PNTC->Release();
	/*m_pDecl_PNT->Release();
	m_pDecl_PNC->Release();
	m_pDecl_PN->Release();
	m_pDecl_PTC->Release();
	m_pDecl_PT->Release();
	m_pDecl_PC->Release();
	m_pDecl_P->Release();
	m_pDecl_PNT0T1C->Release();
	m_pDecl_PNT0T2C->Release();*/
	/*stack_model->Release();
	stack_projection->Release();
	for(int i=0;i<MAX_TEXUNITS;i++) {
		stack_texture[i]->Release();
	}
	//sprite->Release();
	delete this->uniformColorArray;
	*/
}

void D3D9Renderer::onDeviceReset() {
	// The device has just been reset. This is the time to recreate() any resources in the default
	// pool, and call OnResetDevice() for any D3DX objects that need it.

	/*for(set<D3D9FontBuffer *>::iterator iter = this->store_fonts.begin(); iter != this->store_fonts.end();++iter) {
		D3D9FontBuffer *fb = *iter;
		fb->onDeviceReset();
	}*/
	for(set<D3D9Item *>::iterator iter = this->store.begin(); iter != this->store.end();++iter) {
		D3D9Item *item = *iter;
		item->onDeviceReset();
	}
	sprite->OnResetDevice();
}

void D3D9Renderer::disableShaders() {
	this->m_pDevice->SetVertexShader(NULL);
	this->m_pDevice->SetPixelShader(NULL);
	this->setActiveShader(NULL);
}
