#pragma once

#include "VisionIface.h"
#include "Resource.h"

const int max_sounds_c = 1024;

class Sound : public VisionObject, public virtual VI_Sound {
/*protected:
	bool initialised;*/
public:

	Sound();
	virtual ~Sound() {}
	virtual V_CLASS_t getClass() const { return V_CLASS_SOUND; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_SOUND );
	}
	virtual size_t memUsage() {
		return 0;
	}

	/*bool isInitialised() const {
		return this->initialised;
	}*/
};

#ifdef _WIN32
#include <SDL_mixer.h>
#elif __linux
#include <SDL/SDL_mixer.h>
#endif

class SDL_Sound : public Sound {
	Mix_Music *music;
	Mix_Chunk *chunks[max_sounds_c];
	int channels[max_sounds_c];
public:
	SDL_Sound();
	virtual ~SDL_Sound();

	// interface
	virtual void LoadSound(int s,const char *file);
	virtual bool PlaySound(int s,bool loop,int delay);
	virtual void FreeSound(int s);
	virtual bool IsSoundPlaying(int s);
	virtual void StopSound(int s,bool bResetPosition);

	virtual bool playMedia(const char *filename);
	virtual void stopMedia();
	virtual bool isMediaPlaying();

/*#ifdef _WIN32
	virtual void processEvents(UINT message, WPARAM wParam, LPARAM lParam) {
	}
	virtual bool initHWND(HWND hWnd) {
		return false;
	}
#endif*/
};

//#endif
