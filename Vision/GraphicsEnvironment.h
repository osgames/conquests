#pragma once

/*
#ifdef _WIN32
#define WIN32_MEAN_AND_LEAN
#include <windows.h>
#endif
*/

#include "Resource.h"
#include "VisionIface.h"
#include "VisionPrefs.h"

class Panel;
class Texture;
class Graphics2D;
class Graphics3D;
class Renderer;
class Shader;
class InputSystem;
class World;

/*enum InputMode {
INPUTMODE_INTERNAL = 0,
INPUTMODE_EXTERNAL = 1
};*/

//const float fovy = 58.0;
const float fovy = 45.0;

#ifdef _DEBUG
#define _DEBUG_CHECK_ERROR_ GraphicsEnvironment::getSingleton()->checkError("_DEBUG_CHECK_ERROR_");
//#define _DEBUG_CHECK_ERROR_
#else
#define _DEBUG_CHECK_ERROR_
#endif

class Frustum {
public:
	float frustum[6][4];

	Frustum() {
		for(int i=0;i<6;i++)
			for(int j=0;j<4;j++)
				frustum[i][j] = 0;
	}

	bool PointInFrustum( float x, float y, float z ) const;
	//bool WorldPointInFrustum(Renderer *renderer, float x, float y, float z) const;
	float SphereInFrustum( float x, float y, float z, float radius ) const;
	bool infiniteShadowVolumeInFrustum(float px,float py,float pz,float radius,float dx,float dy,float dz) const;
	bool BoxInFrustum(Vector3D pts[8]) const;
};

class GraphicsEnvironment : public VisionObject, public VI_GraphicsEnvironment {
protected:

	static GraphicsEnvironment *singleton;

	//HWND hWnd;
	bool request_quit;

	/*int width,height,multisample;
	//int bits;
	bool fullscreen;*/
	VisionPrefs visionPrefs;
	bool multisample_ok; // did we manage to create a screen with multisampling?
	//InputMode inputMode;
	Panel *panel;
	Panel *modal_panel;
	Graphics2D *g;
	Graphics3D *g3;
	//Image2D *mouse_image;
	Texture *mouse_texture;
	Panel *popup;

	Renderer *renderer;
	InputSystem *inputSystem;
	//bool created; // successfully created?
	//float zoom; // from 0 to 1

	Frustum frustum;

	VI_Sound *sound; // optional sound reference, for passing messages (needed for DirectShow)

	int stats_poly_count;
	int stats_vertex_count;
	int stats_distinct_vertex_count;
	int stats_render_calls_count;

	bool is_fading;
	int fade_start, fade_end; // 255 == no fade, 0 == black
	int fade_timestart_ms, fade_timeend_ms;

	bool init();
	void resize(int width,int height);
	//static GraphicsEnvironment *getGenv(HWND hWnd);

	virtual bool drawFrameStart() {
		return true;
	}
public:
	VI_GraphicsEnvironment_Render *renderFunc;
	VI_GraphicsEnvironment_MouseMotionEvent *mouseMotionEventFunc;
	void *mouseMotionEventData;

	//GraphicsEnvironment(int width,int height,int multisample,bool fullscreen);
	GraphicsEnvironment(VisionPrefs visionPrefs);
	virtual ~GraphicsEnvironment();
	virtual V_CLASS_t getClass() const { return V_CLASS_GRAPHICSENV; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_GRAPHICSENV );
	}
	virtual size_t memUsage();

	static GraphicsEnvironment *getSingleton();
	static bool exists() {
		return singleton != NULL;
	}
	virtual bool hasContext() const=0;
	static bool existsOrHasContext() {
		return singleton != NULL && singleton->hasContext();
	}
	/*bool isCreated() const {
		return this->created;
	}*/
	InputSystem *getInputSystem() const {
		return this->inputSystem;
	}
	void setPopup(Panel *popup) {
		this->popup = popup;
	}
	const Panel *getPopup() const {
		return this->popup;
	}

	void setWorld(World *world);
	virtual void drawFrameTest();
	virtual void checkError(const char *label)=0;
	Graphics2D *getGraphics2D() {
		return g;
	}
	Graphics3D *getGraphics3D() {
		return g3;
	}
	const Graphics3D *getGraphics3D() const {
		return g3;
	}
	/*bool inWindow(int x,int y) {
	return (x >= 0 && x < width && y >= 0 && y < height);
	}*/
	int getMultisample() const {
		return visionPrefs.multisample;
	}
	bool wantStdShaders() const {
		return visionPrefs.want_std_shaders;
	}
	bool wantShaders() const {
		return visionPrefs.shaders;
	}
	/*InputMode getInputMode() {
	return inputMode;
	}*/
	//bool setFullScreen(bool fullscreen);
	/*void setMouseImage(Image2D *mouse_image) {
		this->mouse_image = mouse_image;*/
	//virtual Shader *createShader(const char *file, const char *func, bool vertex_shader)=0;
	const Frustum *getFrustum() const {
		return &frustum;
	};
	Frustum *getFrustum() {
		return &frustum;
	};
	Vector2D getScreenSizeAtDist(float z) const;
	Vector3D getDirectionOfPoint(int x,int y) const;
	Vector3D getDirectionOfRotatedPoint(const Rotation3D &rot,int x,int y) const;
	void eyeToScreen(int *x,int *y,const Vector3D &eye) const;

	void updateStats(int poly_count, int vertex_count, int distinct_vertex_count, int render_calls_count) {
		stats_poly_count += poly_count;
		stats_vertex_count += vertex_count;
		stats_distinct_vertex_count += distinct_vertex_count;
		stats_render_calls_count += render_calls_count;
	}

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/

	virtual VI_Font *createFont(const char *family,int pt,int style) const;

	virtual Renderer *getRenderer() {
		return this->renderer;
	}
	virtual const Renderer *getRenderer() const {
		return this->renderer;
	}
	/*virtual HWND getHWND() const {
		return this->hWnd;
	}*/
	virtual int getWidth() const {
		return visionPrefs.resolution_width;
	}
	virtual int getHeight() const {
		return visionPrefs.resolution_height;
	}
	virtual int getAA() const {
		return (visionPrefs.multisample>0) ? visionPrefs.multisample : 1;
	}
	virtual bool isFullScreen() const {
		return visionPrefs.fullscreen;
	}
	virtual const VisionPrefs *getVisionPrefs() const {
		return &visionPrefs;
	}
	virtual void setSMP(bool smp) {
		visionPrefs.smp = smp;
	}
	virtual bool hasShaders() const;
	virtual bool failedToLoadShaders() const;
	//virtual bool isActive() const=0;
	virtual void getFrameStats(int *poly_count,int *vertex_count, int *distinct_vertex_count, int *render_calls_count) const {
		*poly_count = this->stats_poly_count;
		*vertex_count = this->stats_vertex_count;
		*distinct_vertex_count = this->stats_distinct_vertex_count;
		*render_calls_count = this->stats_render_calls_count;
	}
	virtual float getFPS() const;
	virtual void getMouseInfo(int *x,int *y,bool buttons[3]) const;
	/*virtual void getMouseClick(bool buttons[3]) const;
	virtual bool readMouseWheelUp() const;
	virtual bool readMouseWheelDown() const;*/

	virtual VI_Panel *getPanel() const;

	virtual void setZ(float z_near,float z_far);
	virtual void setMouseTexture(VI_Texture *mouse_texture);
	virtual void setPanel(VI_Panel *panel);
	virtual void setModalPanel(VI_Panel *panel);
	virtual VI_Panel *getModalPanel() const;
	virtual void setUseShadowVolumes(bool use_shadow_volumes);
	virtual void linkWorld(VI_World *world);
	virtual VI_World *getWorld();
	virtual const VI_World *getWorld() const;
	virtual void setSound(VI_Sound *sound) {
		this->sound = sound;
	}
	virtual VI_Sound *getSound() const {
		return this->sound;
	}

	virtual Vector3D getDirectionOfPoint(const VI_SceneGraphNode *node, int sx, int sy) const;
	virtual void localToScreen(int *x, int *y, const VI_SceneGraphNode *node, const Vector3D &local) const;
	virtual Vector3D worldToEye(const Vector3D &world) const;
	virtual void worldToScreen(int *x,int *y,const Vector3D &world) const;

	//virtual void setVisible(bool visible);
	virtual void drawFrame();
	virtual void swapBuffers()=0;
	virtual void handleInput();
	virtual void handleInput(int mouseX,int mouseY,bool mouseL,bool mouseM,bool mouseR);
	virtual void flushInput();
	virtual bool shouldQuit() {
		return request_quit;
	}
	virtual void clearQuitRequest() {
		this->request_quit = false;
	}
	virtual bool saveScreen(const char *filename,const char *type)=0;
	virtual void render(VI_SceneGraphNode *node, float x, float y, float z);
	virtual void setRenderFunc(VI_GraphicsEnvironment_Render *renderFunc) {
		this->renderFunc = renderFunc;
	}
	virtual void setMouseMotionEventFunc(VI_GraphicsEnvironment_MouseMotionEvent *mouseMotionEventFunc,void *mouseMotionEventData) {
		this->mouseMotionEventFunc = mouseMotionEventFunc;
		this->mouseMotionEventData = mouseMotionEventData;
	}
	virtual void setFade(float duration, unsigned char fade_start, unsigned char fade_end);
	virtual void stopFade() {
		this->is_fading = false;
		this->fade_start = 0;
		this->fade_end = 0;
		this->fade_timestart_ms = 0;
		this->fade_timeend_ms = 0;
	}
};
