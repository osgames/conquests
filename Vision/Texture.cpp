//---------------------------------------------------------------------------
#include <cstdio>
#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include <cmath>
#include <cassert>

#include <algorithm>
using std::min;
using std::max;

#ifdef _WIN32
#define WIN32_MEAN_AND_LEAN
#include <windows.h>
#endif

#include <FreeImage.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "Texture.h"
#include "Misc.h"

//---------------------------------------------------------------------------

Image2D::Image2D() : VisionObject() {
	this->w = 0;
	this->h = 0;
	this->alpha = false;
	this->rgbdata = NULL;
	//this->mask = NULL;
}

Image2D::Image2D(int w,int h) : VisionObject() {
	this->w = w;
	this->h = h;
	this->alpha = false;
	rgbdata = new rgb_struct[w * h];
	rgb_struct *rgb = (rgb_struct *)rgbdata;
	for(int i=0;i<w*h;i++) {
		rgb[i].r = 0;
		rgb[i].g = 0;
		rgb[i].b = 0;
	}
	//this->mask = NULL;
}

Image2D::Image2D(int w,int h,bool alpha) : VisionObject() {
	this->w = w;
	this->h = h;
	this->alpha = alpha;
	if( alpha ) {
		rgbdata = new rgba_struct[w * h];
		rgba_struct *rgba = (rgba_struct *)rgbdata;
		for(int i=0;i<w*h;i++) {
			rgba[i].r = 0;
			rgba[i].g = 0;
			rgba[i].b = 0;
			rgba[i].a = 0;
		}
	}
	else {
		rgbdata = new rgb_struct[w * h];
		rgb_struct *rgb = (rgb_struct *)rgbdata;
		for(int i=0;i<w*h;i++) {
			rgb[i].r = 0;
			rgb[i].g = 0;
			rgb[i].b = 0;
		}
	}
	//this->mask = NULL;
}

Image2D::Image2D(int w, int h, unsigned char r, unsigned char g, unsigned char b, bool alpha, unsigned char a) : VisionObject() {
	this->w = w;
	this->h = h;
	this->alpha = alpha;
	if( alpha ) {
		rgbdata = new rgba_struct[w * h];
		rgba_struct *rgba = (rgba_struct *)rgbdata;
		for(int i=0;i<w*h;i++) {
			rgba[i].r = r;
			rgba[i].g = g;
			rgba[i].b = b;
			rgba[i].a = a;
		}
	}
	else {
		rgbdata = new rgb_struct[w * h];
		rgb_struct *rgb = (rgb_struct *)rgbdata;
		for(int i=0;i<w*h;i++) {
			rgb[i].r = r;
			rgb[i].g = g;
			rgb[i].b = b;
		}
	}
}

Image2D::Image2D(void *data,int w,int h) {
	this->rgbdata = data;
	this->w = w;
	this->h = h;
	this->alpha = false;
	//this->mask = NULL;
}

/*Image2D::Image2D(char *filename,char *ext) : VisionObject() {
rgbdata = NULL;
mask = NULL;

this->load(filename,ext);
}*/

Image2D::~Image2D() {
	if(rgbdata!=NULL)
		delete [] rgbdata;
	/*if(mask!=NULL)
		delete [] mask;*/
}

size_t Image2D::memUsage() {
	size_t size = sizeof(this);
	size_t datasize = alpha ? w*h*sizeof(rgba_struct) : w*h*sizeof(rgb_struct);
	if( rgbdata != NULL )
		size += datasize;
	/*if( mask != NULL )
		size += datasize;*/
	return size;
}

Image2D *Image2D::loadImage(const char *filename) {
	Image2D *image = new Image2D();
	if(!image->load(filename)) {
		delete image;
		image = NULL;
		LOG("failed to load image: %s\n", filename);
		Vision::setError(new VisionException(NULL, VisionException::V_FILE_ERROR, "Failed to load image"));
	}
	return image;
}

int Image2D::loadFont(Image2D ***images, GlobalFontInfo *globalFontInfo, FontInfo **fontInfo, const char *font, int size, int char_start, int n_chars) {
	int n_images = 0;
	*images = NULL;
	*fontInfo = NULL;

	FT_Library library;
	int error = 0;

	error = FT_Init_FreeType(&library);
	if( error ) {
		Vision::setError(new VisionException(NULL, VisionException::V_FONT_ERROR, "Failed to initialise Freetype library"));
		return 0;
	}

	FT_Face face;
	error = FT_New_Face(library, font, 0, &face);
	if( error == FT_Err_Unknown_File_Format ) {
		LOG("failed to load %s\n", font);
		FT_Done_FreeType(library);
		Vision::setError(new VisionException(NULL, VisionException::V_FONT_ERROR, "Font file format is unsupported by Freetype"));
		return 0;
	}
	else if( error ) {
		LOG("failed to load %s\n", font);
		FT_Done_FreeType(library);
		Vision::setError(new VisionException(NULL, VisionException::V_FONT_ERROR, "Failed to open font"));
		return 0;
	}

	error = FT_Set_Char_Size(face, 0, size*64, 0, 72); // 72 ensures we get size == pixels, which is consistent with previous non-Freetype behaviour
	if( error ) {
		LOG("failed to load %s\n", font);
		FT_Done_Face(face);
		FT_Done_FreeType(library);
		Vision::setError(new VisionException(NULL, VisionException::V_FONT_ERROR, "Failed to set font size"));
		return 0;
	}

	// set global font info
	//globalFontInfo->descent = face->ascender >> 6;
	//globalFontInfo->descent = face->descender >> 6;
	//globalFontInfo->descent = face->size->metrics.ascender >> 6;
	globalFontInfo->descent = ( face->size->metrics.ascender >> 6 ) + ( face->size->metrics.descender >> 6 );
	//globalFontInfo->descent = - face->size->metrics.descender >> 6;
	globalFontInfo->height = face->size->metrics.height >> 6;
	//globalFontInfo->descent = face->size->metrics.height >> 6;
	//globalFontInfo->descent = 3;

	n_images = n_chars - char_start;
	*images = new Image2D *[n_images];
	for(int i=0;i<n_images;i++)
		(*images)[i] = NULL;
	*fontInfo = new FontInfo[n_images];

	FT_GlyphSlot slot = face->glyph;
	bool mono = size <= 14; // hack, probably best to have it controlled by the user?
	//bool mono = false;
	for(int i=0;i<n_images;i++) {
		int ch = char_start + i;
		if( mono )
			error = FT_Load_Char(face, ch, FT_LOAD_RENDER|FT_LOAD_MONOCHROME);
		else
		error = FT_Load_Char(face, ch, FT_LOAD_RENDER);
		if( error ) {
			VI_log("failed to load glyph for char %d\n", i);
			continue;
		}
		/*if( ch == 'I' ) {
			LOG("");
		}*/
		ASSERT( slot->format == FT_GLYPH_FORMAT_BITMAP );
		//int height = slot->metrics.height >> 6;
		int advance_x = slot->advance.x >> 6;
		(*fontInfo)[i].advance_x = advance_x;

		FT_Bitmap *bitmap = &slot->bitmap;
		(*fontInfo)[i].offset_x = slot->bitmap_left;
		(*fontInfo)[i].offset_y = slot->bitmap_top;
		//ASSERT( bitmap->width <= advance_x );
		if( bitmap->width == 0 || bitmap->rows == 0 ) {
			continue; // some characters have no bitmap, e.g., space
		}
		//Image2D *image = new Image2D(bitmap->width, bitmap->rows, true);
		int image_width = VI_getNextPower(bitmap->width, 2);
		int image_height = VI_getNextPower(bitmap->rows, 2);
		Image2D *image = new Image2D(image_width, image_height, true);
		for(int y=0;y<bitmap->rows;y++) {
			for(int x=0;x<bitmap->width;x++) {
				if( mono ) {
					unsigned char byte = bitmap->buffer[y*bitmap->pitch + x/8];
					int mask = 128 >> (x%8);
					unsigned char ch = byte & mask;
					if( ch != 0 ) {
						image->putRGBA(x, y, 255, 255, 255, 255);
					}
					else {
						image->putRGBA(x, y, 0, 0, 0, 0);
					}
				}
				else {
					unsigned char ch = bitmap->buffer[y*bitmap->pitch + x];
					//unsigned char alpha = ch == 0 ? 0 : 255;
					unsigned char alpha = ch;
					image->putRGBA(x, y, ch, ch, ch, alpha);
				}
			}
		}
		(*images)[i] = image;
	}

	FT_Done_Face(face);
	FT_Done_FreeType(library);

	return n_images;
}

bool Image2D::scale(int newWidth, int newHeight) {
	if( this->w == newWidth && this->h == newHeight ) {
		return true;
	}

	FIBITMAP *dib = this->toFreeImage();
	if( dib == NULL )
		return false;

	FIBITMAP *new_dib = FreeImage_Rescale(dib, newWidth, newHeight, FILTER_BILINEAR);
	//FIBITMAP *new_dib = FreeImage_Rescale(dib, newWidth, newHeight, FILTER_BOX);
	if( new_dib == NULL ) {
		return false;
	}

	this->fromFreeImage(new_dib);
	FreeImage_Unload(dib);
	FreeImage_Unload(new_dib);
	return true;
}

void Image2D::flip(bool flipX, bool flipY) {
	if( flipY ) {
		for(int y=0;y<this->getHeight()/2;y++) {
			int y1 = this->getHeight()-1-y;
			for(int x=0;x<this->getWidth();x++) {
				if( this->alpha ) {
					unsigned char r0 = 0, g0 = 0, b0 = 0, a0 = 0;
					unsigned char r1 = 0, g1 = 0, b1 = 0, a1 = 0;
					this->getRGBA(&r0, &g0, &b0, &a0, x, y);
					this->getRGBA(&r1, &g1, &b1, &a1, x, y1);
					this->putRGBA(x, y, r1, g1, b1, a1);
					this->putRGBA(x, y1, r0, g0, b0, a0);
				}
				else {
					unsigned char r0 = 0, g0 = 0, b0 = 0;
					unsigned char r1 = 0, g1 = 0, b1 = 0;
					this->getRGB(&r0, &g0, &b0, x, y);
					this->getRGB(&r1, &g1, &b1, x, y1);
					this->putRGB(x, y, r1, g1, b1);
					this->putRGB(x, y1, r0, g0, b0);
				}
			}
		}
	}
}


bool Image2D::makePowerOf2() {
	if( !VI_isPower(this->getWidth(), 2) || !VI_isPower(this->getHeight(), 2) ) {
		//LOG("image has dimensions %d x %d : not power of 2\n", this->getWidth(), this->getHeight());
		int newWidth = VI_getNextPower(this->getWidth(), 2);
		int newHeight = VI_getNextPower(this->getHeight(), 2);
		//LOG("    scale to %d x %d\n", newWidth, newHeight);
		if( !this->scale(newWidth, newHeight) ) {
			return false;
		}
	}
	return true;
}

bool Image2D::makeSamePowerOf2() {
	if( this->getWidth() != this->getHeight() || !VI_isPower(this->getWidth(), 2) || !VI_isPower(this->getHeight(), 2) ) {
		LOG("image has dimensions %d x %d : not same power of 2\n", this->getWidth(), this->getHeight());
		int newWidth = VI_getNextPower(this->getWidth(), 2);
		int newHeight = VI_getNextPower(this->getHeight(), 2);
		while( newWidth > newHeight ) {
			newHeight *= 2;
		}
		while( newWidth < newHeight ) {
			newWidth *= 2;
		}
		LOG("    scale to %d x %d\n", newWidth, newHeight);
		ASSERT( newWidth == newHeight );
		if( !this->scale(newWidth, newHeight) ) {
			return false;
		}
	}
	return true;
}

bool Image2D::load(const char *filename) {
	LOG("Loading Image: %s\n",filename);
	bool ok = false;

	FREE_IMAGE_FORMAT type = FreeImage_GetFileType(filename, 0);
	FIBITMAP *dib = FreeImage_Load(type, filename);
	if( dib == NULL ) {
		LOG("failed to load image\n");
	}

	//if( dib != NULL && FreeImage_GetBPP(dib) != 24 ) {
	if( dib != NULL && FreeImage_GetBPP(dib) != 24 && FreeImage_GetBPP(dib) != 32 ) {
		dib = FreeImage_ConvertTo24Bits(dib);
		if( dib == NULL ) {
			LOG("failed to convert image to 24 bit\n");
		}
	}

	if( dib != NULL ) {
		/*this->w = FreeImage_GetWidth(dib);
		this->h = FreeImage_GetHeight(dib);
		unsigned char *data = (unsigned char *)FreeImage_GetBits(dib);
		int bpp = FreeImage_GetBPP(dib);
		//RGBQUAD *palette = FreeImage_GetPalette(dib);
		//this->rgbdata = new rgb_struct[this->w * this->h];
		unsigned char *ptr = data;
		switch(bpp) {
					case 24:
						{
							rgb_struct *rgb = new rgb_struct[this->w * this->h];
							for(int y=0,i=0;y<this->h;y++) {
								for(int x=0;x<this->w;x++,i++) {
									//int i = (this->h - 1 - y) * this->w + x;
									rgb[i].b = *ptr++;
									rgb[i].g = *ptr++;
									rgb[i].r = *ptr++;
								}
							}
							this->rgbdata = (void *)rgb;
							ok = true;
							break;
						}
					case 32:
						{
							rgba_struct *rgba = new rgba_struct[this->w * this->h];
							this->alpha = true;
							for(int y=0,i=0;y<this->h;y++) {
								for(int x=0;x<this->w;x++,i++) {
									//int i = (this->h - 1 - y) * this->w + x;
									rgba[i].b = *ptr++;
									rgba[i].g = *ptr++;
									rgba[i].r = *ptr++;
									rgba[i].a = *ptr++;
								}
							}
							this->rgbdata = (void *)rgba;
							ok = true;
							break;
						}
		}*/
		this->fromFreeImage(dib);
		ok = true;
		FreeImage_Unload(dib);
		dib = NULL;
	}

	if(!ok) {
		// error
		//Vision::setError(new VisionException(this,VisionException::V_FILE_ERROR,"Failed to load Texture"));
		this->w = 0;
		this->h = 0;
		if(this->rgbdata != NULL) {
			delete [] this->rgbdata;
			this->rgbdata = NULL;
		}
	}
	return ok;
}


void Image2D::fromFreeImage(FIBITMAP *dib) {
	if( rgbdata != NULL ) {
		delete [] rgbdata;
		rgbdata = NULL;
	}

	this->w = FreeImage_GetWidth(dib);
	this->h = FreeImage_GetHeight(dib);
	unsigned char *data = (unsigned char *)FreeImage_GetBits(dib);
	int bpp = FreeImage_GetBPP(dib);
	//RGBQUAD *palette = FreeImage_GetPalette(dib);
	//this->rgbdata = new rgb_struct[this->w * this->h];
	//unsigned char *ptr = data;
	int pitch = FreeImage_GetPitch(dib);
	switch(bpp) {
		case 24:
			{
				rgb_struct *rgb = new rgb_struct[this->w * this->h];
				this->alpha = false;
				for(int y=0,i=0;y<this->h;y++) {
					unsigned char *ptr = &data[y*pitch];
					for(int x=0;x<this->w;x++,i++) {
						//int i = (this->h - 1 - y) * this->w + x;
						rgb[i].b = *ptr++;
						rgb[i].g = *ptr++;
						rgb[i].r = *ptr++;
					}
				}
				this->rgbdata = (void *)rgb;
				break;
			}
		case 32:
			{
				rgba_struct *rgba = new rgba_struct[this->w * this->h];
				this->alpha = true;
				for(int y=0,i=0;y<this->h;y++) {
					unsigned char *ptr = &data[y*pitch];
					for(int x=0;x<this->w;x++,i++) {
						//int i = (this->h - 1 - y) * this->w + x;
						rgba[i].b = *ptr++;
						rgba[i].g = *ptr++;
						rgba[i].r = *ptr++;
						rgba[i].a = *ptr++;
					}
				}
				this->rgbdata = (void *)rgba;
				break;
			}
		default:
			ASSERT(false);
			break;
	}
}

FIBITMAP *Image2D::toFreeImage() const {
	FIBITMAP *dib = FreeImage_Allocate(w, h, alpha ? 32 : 24);
	if(dib == NULL)
		return NULL;

	unsigned char *data = FreeImage_GetBits(dib);
	if(data == NULL) {
		FreeImage_Unload(dib);
		return NULL;
	}

	//unsigned char *ptr = data;
	int pitch = FreeImage_GetPitch(dib);
	if( this->alpha ) {
		rgba_struct *rgb = (rgba_struct *)rgbdata;
		for(int y=0,i=0;y<h;y++) {
			unsigned char *ptr = &data[y*pitch];
			for(int x=0;x<w;x++,i++) {
				*ptr++ = rgb[i].b;
				*ptr++ = rgb[i].g;
				*ptr++ = rgb[i].r;
				*ptr++ = rgb[i].a;
			}
		}
	}
	else {
		rgb_struct *rgb = (rgb_struct *)rgbdata;
		for(int y=0,i=0;y<h;y++) {
			unsigned char *ptr = &data[y*pitch];
			for(int x=0;x<w;x++,i++) {
				*ptr++ = rgb[i].b;
				*ptr++ = rgb[i].g;
				*ptr++ = rgb[i].r;
			}
		}
	}
	//ASSERT( !this->alpha );

	return dib;
}

bool Image2D::save(const char *filename,const char *ext) const {
	bool ok = false;
	FIBITMAP *dib = this->toFreeImage();
	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(ext);
	if( fif == FIF_UNKNOWN ) {
		LOG("warning, freeimage can't determine type for extension: %s\n", ext);
		LOG("default to PNG\n");
		fif = FIF_PNG;
	}
	FreeImage_Save(fif, dib, filename);
	/*switch(FreeImage_GetFileTypeFromExt(ext)) {
	case FIF_BMP :
	FreeImage_SaveBMP(dib,filename);
	break;

	case FIF_JPEG :
	FreeImage_SaveJPEG(dib,filename,JPEG_ACCURATE);
	break;

	case FIF_PNG :
	FreeImage_SavePNG(dib,filename);
	break;

	case FIF_PBM :
	case FIF_PGM :
	case FIF_PPM :
	FreeImage_SavePNM(dib,filename);
	break;

	case FIF_TIFF :
	FreeImage_SaveTIFF(dib,filename);
	break;

	case FIF_WBMP :
	FreeImage_SaveWBMP(dib,filename);
	break;

	default :
	ok = false;
	break;
	};*/

	FreeImage_Unload(dib);
	return ok;
}

/*void Image2D::draw(int x,int y) {
GraphicsEnvironment::getSingleton()->renderer->drawPixels(x, y, w, h, rgbdata);
}

void Image2D::drawWithMask(int x,int y) {
if(mask == NULL) {
this->draw(x, y);
return;
}

GraphicsEnvironment::getSingleton()->renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_DST_BY_ZERO);
GraphicsEnvironment::getSingleton()->renderer->drawPixels(x, y, w, h, mask);
GraphicsEnvironment::getSingleton()->renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_BOTH);
GraphicsEnvironment::getSingleton()->renderer->drawPixels(x, y, w, h, rgbdata);
GraphicsEnvironment::getSingleton()->renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
}*/

/*void Image2D::createMask(rgb_struct maskcol) {
	this->mask = new rgb_struct[w * h];
	rgb_struct black = {0,0,0};
	rgb_struct white = {255,255,255};
	for(int i=0;i<w*h;i++) {
		// TODO: fix for 32 bit images!
		rgb_struct *rgb_p = (rgb_struct *)this->rgbdata;
		rgb_struct rgb = rgb_p[i];
		if( rgb.r == maskcol.r && rgb.g == maskcol.g && rgb.b == maskcol.b ) {
			// mask
			this->mask[i] = white;
			rgb_p[i] = black;
		}
		else
			this->mask[i] = black;
	}
}

void Image2D::createMask() {
	rgb_struct black = {0,0,0};
	createMask(black);
}*/

/*Image2D *Image2D::createNoise(int w,int h) {
	const unsigned char filter_max[3] = {255, 0, 0};
	const unsigned char filter_min[3] = {0, 0, 0};
	return createNoise(w, h, 3.2f, filter_max, filter_min, V_NOISEMODE_PERLIN, 1);
}*/

Image2D *Image2D::createNoise(int w,int h,float scale_u,float scale_v,const unsigned char filter_max[3],const unsigned char filter_min[3],V_NOISEMODE_t noisemode,int n_iterations,bool maximise) {
	LOG("Image2D::createNoise(%d, %d, %f, %f,...)\n", w, h, scale_u, scale_v);
	float fvec[2] = {0.0f, 0.0f};
	float *values = new float[w*h];
	for(int y=0,i=0;y<h;y++) {
		fvec[0] = scale_v * ((float)y) / ((float)h - 1.0f);
		for(int x=0;x<w;x++,i++) {
			fvec[1] = scale_u * ((float)x) / ((float)w - 1.0f);
			/*float h = perlin_noise2(fvec);
			if( true ) {
			//if( noisemode == V_NOISEMODE_SMOKE || noisemode == V_NOISEMODE_PATCHY || noisemode == V_NOISEMODE_MARBLE ) {
			//if( noisemode == V_NOISEMODE_SMOKE || noisemode == V_NOISEMODE_PATCHY || noisemode == V_NOISEMODE_MARBLE || noisemode == V_NOISEMODE_CLOUDS ) {
			if( noisemode == V_NOISEMODE_PATCHY || noisemode == V_NOISEMODE_MARBLE )
			h = abs(h);
			float max_val = 1.0f;
			float mult = 2.0f;
			for(int i=1;i<n_iterations;i++,mult*=2.0f) {
			float this_fvec[2];
			this_fvec[0] = fvec[0] * mult;
			this_fvec[1] = fvec[1] * mult;
			float this_h = perlin_noise2(this_fvec) / mult;
			if( noisemode == V_NOISEMODE_PATCHY || noisemode == V_NOISEMODE_MARBLE )
			this_h = abs(this_h);
			h += this_h;
			max_val += 1.0f / mult;
			}
			if( noisemode == V_NOISEMODE_PATCHY ) {
			h /= max_val;
			}
			else if( noisemode == V_NOISEMODE_MARBLE ) {
			h = sin(scale * ((float)x) / ((float)w - 1.0f) + h);
			h = 0.5f + 0.5f * h;
			}
			else {
			h /= max_val;
			h = 0.5f + 0.5f * h;
			}
			}
			else {
			h = 0.5f + 0.5f * h;
			}*/

			float h = 0.0f;
			float max_val = 0.0f;
			float mult = 1.0f;
			for(int j=0;j<n_iterations;j++,mult*=2.0f) {
				float this_fvec[2];
				this_fvec[0] = fvec[0] * mult;
				this_fvec[1] = fvec[1] * mult;
				float this_h = VI_perlin_noise2(this_fvec) / mult;
				if( noisemode == V_NOISEMODE_PATCHY || noisemode == V_NOISEMODE_MARBLE )
					this_h = abs(this_h);
				h += this_h;
				max_val += 1.0f / mult;
			}
			if( noisemode == V_NOISEMODE_PATCHY ) {
				h /= max_val;
			}
			else if( noisemode == V_NOISEMODE_MARBLE ) {
				h = sin(scale_u * ((float)x) / ((float)w - 1.0f) + h);
				h = 0.5f + 0.5f * h;
			}
			else {
				h /= max_val;
				h = 0.5f + 0.5f * h;
			}

			if( noisemode == V_NOISEMODE_CLOUDS ) {
				//const float offset = 0.4f;
				//const float offset = 0.3f;
				const float offset = 0.2f;
				h = offset - h * h;
				h = max(h, 0.0f);
				h /= offset;
			}
			// h is now in range [0, 1]
			if( h < 0.0 || h > 1.0 ) {
				Vision::setError(new VisionException(NULL, VisionException::V_IMAGE_NOISEFUNC_ERROR, "h value is out of bounds"));
			}
			if( noisemode == V_NOISEMODE_WOOD ) {
				h = 20 * h;
				h = h - floor(h);
			}
			values[i] = h;
		}
	}


	if( maximise ) {
		float min_value = 1.0f, max_value = 0.0f;
		for(int y=0,i=0;y<h;y++) {
			for(int x=0;x<w;x++,i++) {
				if( values[i] > max_value ) {
					max_value = values[i];
				}
				if( values[i] < min_value ) {
					min_value = values[i];
				}
			}
		}
		LOG("stretch noise: %f, %f\n", min_value, max_value);
		if( max_value > min_value ) {
			for(int y=0,i=0;y<h;y++) {
				for(int x=0;x<w;x++,i++) {
					float frac = (values[i] - min_value) / (max_value - min_value);
					values[i] = frac;
				}
			}
		}
	}

	Image2D *image = new Image2D(w, h);
	rgb_struct *rgb = (rgb_struct *)image->rgbdata;
	for(int y=0,i=0;y<h;y++) {
		for(int x=0;x<w;x++,i++) {
			rgb[i].r = (unsigned char)((filter_max[0] - filter_min[0]) * values[i] + filter_min[0]);
			rgb[i].g = (unsigned char)((filter_max[1] - filter_min[1]) * values[i] + filter_min[1]);
			rgb[i].b = (unsigned char)((filter_max[2] - filter_min[2]) * values[i] + filter_min[2]);
		}
	}

	delete [] values;

	return image;
}

Image2D *Image2D::createRadial(int w,int h,const unsigned char filter_max[3],const unsigned char filter_min[3],bool squared,bool alpha) {
	Image2D *image = new Image2D(w, h, alpha);
	//rgb_struct *rgb = (rgb_struct *)image->rgbdata;
	float cx = (((float)w)-1.0f)/2.0f;
	float cy = (((float)h)-1.0f)/2.0f;
	//float max_dist = sqrt(cx*cx+cy*cy);
	float max_dist = min(cx, cy);
	for(int y=0,i=0;y<h;y++) {
		float dy = ((float)y) - cy;
		float dy2 = dy*dy;
		for(int x=0;x<w;x++,i++) {
			float dx = ((float)x) - cx;
			float dx2 = dx*dx;
			float dist = sqrt(dx2+dy2);
			float d = max_dist == 0 ? 0.0f : dist / max_dist;
			d = max(d, 0.0f);
			d = min(d, 1.0f);
			if( squared ) {
				//d *= d;
				//d = 1.0f - (1.0f-d)*(1.0f-d);
				d *= (2.0f - d);
			}
			float r = d * (float)filter_min[0] + (1.0f - d) * (float)filter_max[0];
			float g = d * (float)filter_min[1] + (1.0f - d) * (float)filter_max[1];
			float b = d * (float)filter_min[2] + (1.0f - d) * (float)filter_max[2];
			//float a = d * (float)filter_min[3] + (1.0f - d) * (float)filter_max[3];
			float a = (1.0f - d)*255;
			if( alpha ) {
				rgba_struct *rgba = (rgba_struct *)image->rgbdata;
				rgba[i].r = (unsigned char)r;
				rgba[i].g = (unsigned char)g;
				rgba[i].b = (unsigned char)b;
				rgba[i].a = (unsigned char)a;
			}
			else {
				rgb_struct *rgb = (rgb_struct *)image->rgbdata;
				rgb[i].r = (unsigned char)r;
				rgb[i].g = (unsigned char)g;
				rgb[i].b = (unsigned char)b;
			}
		}
	}
	return image;
}

VI_Image *Image2D::copy() const {
	// TODO: 32 bit images!
	ASSERT( !this->alpha );
	Image2D *image = new Image2D(w, h);
	rgb_struct *this_rgb = (rgb_struct *)this->rgbdata;
	rgb_struct *new_rgb = (rgb_struct *)image->rgbdata;
	for(int i=0;i<w*h;i++) {
		new_rgb[i] = this_rgb[i];
	}
	return image;
}

VI_Image *Image2D::createDerivative(float scale) const {
	// TODO: 32 bit images!
	ASSERT( !this->alpha );
	Image2D *image = new Image2D(w, h);
	rgb_struct *this_rgb = (rgb_struct *)this->rgbdata;
	rgb_struct *new_rgb = (rgb_struct *)image->rgbdata;
	float z = 255.0f / scale;
	for(int y=0;y<h;y++) {
		int y0 = (y==0) ? y : y-1;
		int y1 = (y==h-1) ? y : y+1;
		int yw = y*w;
		int y0w = y0*w;
		int y1w = y1*w;
		for(int x=0;x<w;x++) {
			int x0 = (x==0) ? 0 : x-1;
			int x1 = (x==w-1) ? x : x+1;
			float px0 = (float)this_rgb[yw+x0].r;
			float px1 = (float)this_rgb[yw+x1].r;
			float py0 = (float)this_rgb[y0w+x].r;
			float py1 = (float)this_rgb[y1w+x].r;
			Vector3D normal(px1 - px0, py1 - py0, z);
			normal.normalise();
			int dx = (int)(127.5f + 127.5f * normal.x);
			int dy = (int)(127.5f + 127.5f * normal.y);
			int dz = (int)(127.5f + 127.5f * normal.z);
			new_rgb[y*w+x].r = dx;
			new_rgb[y*w+x].g = dy;
			new_rgb[y*w+x].b = dz;
		}
		/*int x0 = 0;
		int x1 = (0==w-1) ? 0 : 1;
		float px0 = (float)this_rgb[yw+x0].r;
		float px1 = (float)this_rgb[yw+x1].r;
		float py0 = (float)this_rgb[y0w].r;
		float py1 = (float)this_rgb[y1w].r;
		Vector3D normal(px1 - px0, py1 - py0, z);
		normal.normalise();
		int dx = (int)(127.5f + 127.5f * normal.x);
		int dy = (int)(127.5f + 127.5f * normal.y);
		int dz = (int)(127.5f + 127.5f * normal.z);
		new_rgb[y*w].r = dx;
		new_rgb[y*w].g = dy;
		new_rgb[y*w].b = dz;
		for(int x=1;x<w-1;x++) {
			x0 = x-1;
			x1 = x+1;
			px0 = (float)this_rgb[yw+x0].r;
			px1 = (float)this_rgb[yw+x1].r;
			py0 = (float)this_rgb[y0w+x].r;
			py1 = (float)this_rgb[y1w+x].r;
			normal.set(px1 - px0, py1 - py0, z);
			normal.normalise();
			dx = (int)(127.5f + 127.5f * normal.x);
			dy = (int)(127.5f + 127.5f * normal.y);
			dz = (int)(127.5f + 127.5f * normal.z);
			new_rgb[y*w+x].r = dx;
			new_rgb[y*w+x].g = dy;
			new_rgb[y*w+x].b = dz;
		}
		x0 = (w-1==0) ? 0 : w-2;
		x1 = w-1;
		px0 = (float)this_rgb[yw+x0].r;
		px1 = (float)this_rgb[yw+x1].r;
		py0 = (float)this_rgb[y0w+w-1].r;
		py1 = (float)this_rgb[y1w+w-1].r;
		normal.set(px1 - px0, py1 - py0, z);
		normal.normalise();
		dx = (int)(127.5f + 127.5f * normal.x);
		dy = (int)(127.5f + 127.5f * normal.y);
		dz = (int)(127.5f + 127.5f * normal.z);
		new_rgb[y*w+w-1].r = dx;
		new_rgb[y*w+w-1].g = dy;
		new_rgb[y*w+w-1].b = dz;*/
	}
	return image;
}

void Image2D::adjustBrightness1D(unsigned char bright, bool xdir) {
	// TODO: 32 bit images!
	ASSERT( !this->alpha );
	int n1 = xdir ? w : h;
	int n2 = xdir ? h : w;
	for(int i=0;i<n1;i++) {
		int total_bright = 0;
		for(int j=0;j<n2;j++) {
			int x = xdir ? i : j;
			int y = xdir ? j : i;
			unsigned char r, g, b;
			getRGB(&r, &g, &b, x, y);
			int this_bright = ((int)r) + ((int)g) + ((int)b);
			total_bright += this_bright;
		}
		int average_bright = total_bright / n2;
		if( total_bright != 0 ) {
			float scale = (3.0f*(float)bright) / ((float)average_bright);
			for(int j=0;j<n2;j++) {
				int x = xdir ? i : j;
				int y = xdir ? j : i;
				unsigned char r, g, b;
				getRGB(&r, &g, &b, x, y);
				float fr = (float)r;
				float fg = (float)g;
				float fb = (float)b;
				fr *= scale;
				fg *= scale;
				fb *= scale;
				int ir = (int)fr;
				int ig = (int)fg;
				int ib = (int)fb;
				ir = min(ir, 255);
				ig = min(ig, 255);
				ib = min(ib, 255);
				putRGB(x, y, ir, ig, ib);
			}
		}
	}
}

void Image2D::setAllToi(unsigned char r, unsigned char g, unsigned char b) {
	for(int y=0;y<h;y++) {
		for(int x=0;x<w;x++) {
			putRGB(x, y, r, g, b);
		}
	}
}

void Image2D::multiply(float scaleR, float scaleG, float scaleB) {
	for(int y=0;y<h;y++) {
		for(int x=0;x<w;x++) {
			unsigned char r, g, b;
			getRGB(&r, &g, &b, x, y);
			int newR = (int)(scaleR * (float)r);
			int newG = (int)(scaleG * (float)g);
			int newB = (int)(scaleB * (float)b);
			newR = min(newR, 255);
			newG = min(newG, 255);
			newB = min(newB, 255);
			/*newR = 0;
			newG = 255;
			newB = 0;*/
			putRGB(x, y, (unsigned char)newR, (unsigned char)newG, (unsigned char)newB);
		}
	}
}

void Image2D::adjustBrightness(unsigned char bright) {
	// TODO: 32 bit images!
	ASSERT( !this->alpha );
	adjustBrightness1D(bright, true);
	adjustBrightness1D(bright, false);
}

void Image2D::merge(VI_Image *image) {
	Image2D *d_image = static_cast<Image2D *>(image);
	ASSERT( this->getWidth() == image->getWidth() );
	ASSERT( this->getHeight() == d_image->getHeight() );
	// TODO: other combinations for alpha
	ASSERT( !this->alpha );
	ASSERT( d_image->alpha );

	for(int y=0;y<h;y++) {
		for(int x=0;x<w;x++) {
			/*unsigned char r, g, b;
			getRGB(&r, &g, &b, x, y);
			unsigned char r2, g2, b2, a2;
			d_image->getRGBA(&r2, &g2, &b2, &a2, x, y);
			float scale_r = (((float)r2) * ((float)a2)) / 255.0f;
			float scale_g = (((float)g2) * ((float)a2)) / 255.0f;
			float scale_b = (((float)b2) * ((float)a2)) / 255.0f;*/
			unsigned char r, g, b, a;
			d_image->getRGBA(&r, &g, &b, &a, x, y);
			if( a > 0 ) {
				putRGB(x, y, r, g, b);
			}
		}
	}
}

VI_Image *Image2D::createReflection2D() const {
	// TODO: 32 bit images!
	ASSERT( !this->alpha );
	Image2D *image = new Image2D(2*w, 2*h);
	rgb_struct *this_rgb = (rgb_struct *)this->rgbdata;
	rgb_struct *new_rgb = (rgb_struct *)image->rgbdata;
	for(int y=0;y<h;y++) {
		for(int x=0;x<w;x++) {
			rgb_struct rgb = this_rgb[y*w+x];
			int x2 = 2*w-1-x;
			int y2 = 2*h-1-y;
			new_rgb[y*2*w+x] = rgb;
			new_rgb[y*2*w+x2] = rgb;
			new_rgb[y2*2*w+x] = rgb;
			new_rgb[y2*2*w+x2] = rgb;
		}
	}
	return image;
}

/*void Image2D::put(int x, int y, rgb_struct *c) {
	ASSERT( !this->alpha );
	rgb_struct *rgb = (rgb_struct *)this->rgbdata;
	rgb[y * w + x] = *c;
}*/

void Image2D::putRGB(int x, int y, unsigned char r, unsigned char g, unsigned char b) {
	//ASSERT( !this->alpha );
	if( this->alpha ) {
		rgba_struct *rgb = (rgba_struct *)this->rgbdata;
		rgb[y * w + x].r = r;
		rgb[y * w + x].g = g;
		rgb[y * w + x].b = b;
	}
	else {
		rgb_struct *rgb = (rgb_struct *)this->rgbdata;
		rgb[y * w + x].r = r;
		rgb[y * w + x].g = g;
		rgb[y * w + x].b = b;
	}
}

void Image2D::putRGBA(int x, int y, unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	ASSERT( this->alpha );
	rgba_struct *rgba = (rgba_struct *)this->rgbdata;
	rgba[y * w + x].r = r;
	rgba[y * w + x].g = g;
	rgba[y * w + x].b = b;
	rgba[y * w + x].a = a;
}

void Image2D::getRGB(unsigned char *r, unsigned char *g, unsigned char *b, int x, int y) const {
	//ASSERT( !this->alpha );
	if( this->alpha ) {
		rgba_struct *rgb = (rgba_struct *)this->rgbdata;
		*r = rgb[y * w + x].r;
		*g = rgb[y * w + x].g;
		*b = rgb[y * w + x].b;
	}
	else {
		rgb_struct *rgb = (rgb_struct *)this->rgbdata;
		*r = rgb[y * w + x].r;
		*g = rgb[y * w + x].g;
		*b = rgb[y * w + x].b;
	}
}

void Image2D::getRGBA(unsigned char *r, unsigned char *g, unsigned char *b, unsigned char *a, int x, int y) const {
	ASSERT( this->alpha );
	rgba_struct *rgba = (rgba_struct *)this->rgbdata;
	*r = rgba[y * w + x].r;
	*g = rgba[y * w + x].g;
	*b = rgba[y * w + x].b;
	*a = rgba[y * w + x].a;
}

/*rgb_struct *Image2D::get(int x,int y) {
	ASSERT( !this->alpha );
	rgb_struct *rgb = (rgb_struct *)this->rgbdata;
	return &rgb[y * w + x];
}*/

const void *Image2D::get(int x,int y) const {
	if( alpha ) {
		const rgba_struct *rgba = (const rgba_struct *)this->rgbdata;
		return &rgba[y * w + x];
	}
	else {
		const rgb_struct *rgb = (const rgb_struct *)this->rgbdata;
		return &rgb[y * w + x];
	}
}
