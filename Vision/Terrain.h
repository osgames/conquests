#pragma once

#include "Resource.h"
#include "VisionIface.h"
#include "VisionUtils.h"

// Depth of variance tree: should be near SQRT(PATCH_SIZE) + 1
//const int VARIANCE_DEPTH = 9;

//const int POOL_SIZE = 25000;

class Quadtree;
class Quadtreenode;
class Color;
class Graphics3D;
class QuadricObject;
class SceneGraphNode;
class GraphicsEnvironment;
class RenderQueue;
class Texture;

class Atmosphere : public VisionObject, public VI_Atmosphere {
	QuadricObject *pSphere;
	SceneGraphNode *light_ent;
	float fWavelength[3];
	float fWavelength4[3];
public:
	Atmosphere(SceneGraphNode *light_ent);
	~Atmosphere();
	virtual V_CLASS_t getClass() const { return V_CLASS_ATMOSPHERE; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_ATMOSPHERE );
	}
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}
	void render(Graphics3D *g,SceneGraphNode *portal);

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/

};

class SkyBox : public VisionObject, public VI_Sky {
	unsigned char col[3];
	Texture *textures[6];
	V_SKYMODE_t mode;
	bool distinct_textures;

	void free();

public:

	SkyBox();
	~SkyBox();
	virtual V_CLASS_t getClass() const { return V_CLASS_SKY; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_SKY );
	}
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}

	void init(const char *texture,const char *ext);
	void init1(const char *texture);
	void init1(Texture *texture);

	void render(Graphics3D *g);

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/

	virtual void setMode(V_SKYMODE_t mode);
	virtual void setColori(unsigned char r,unsigned char g,unsigned char b) {
		this->col[0] = r;
		this->col[1] = g;
		this->col[2] = b;
	}

	virtual void render(Renderer *renderer);
};

class Clouds : public VisionObject, public VI_Clouds {
	unsigned char col[3];
	Vector3D dir0, dir1;
	float min_brightness, density;

	Texture *cloud_texture0;
	Texture *cloud_texture1;
public:

	Clouds();
	virtual ~Clouds();
	virtual V_CLASS_t getClass() const { return V_CLASS_CLOUDS; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_CLOUDS );
	}
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}

	void render(Graphics3D *g);

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/

	virtual void setColori(unsigned char r,unsigned char g,unsigned char b) {
		this->col[0] = r;
		this->col[1] = g;
		this->col[2] = b;
	}
	virtual void setParameters(float min_brightness, float density) {
		this->min_brightness = min_brightness;
		this->density = density;
	}
	virtual void setDir0(float speed_x, float speed_z) {
		this->dir0.x = speed_x;
		this->dir0.z = speed_z;
	}
	virtual void setDir1(float speed_x, float speed_z) {
		this->dir1.x = speed_x;
		this->dir1.z = speed_z;
	}
};

const int N_TERRAIN_TEXTURES = 256;

class TerrainEngine : public VisionObject, public VI_Terrain {
protected:
	//vector<VisionObject *> garbage_objects; // garbage collection
	bool recalc; // do we need to reprepare the object?
	Graphics3D *g3;
	bool lod;
	unsigned char *data;
	unsigned char *texture_map;
	Color *lightmap[N_TERRAIN_TEXTURES];
	bool *visible;
	int width;
	int depth;
	bool sea, sea_effects;
	float unscaled_sea_height, sea_extent;
	Texture *textures[N_TERRAIN_TEXTURES];
	Texture *sea_texture;
	int n_chunks_x;
	int n_chunks_z;
	//Entity **entities;
	//Mesh **meshes;
	//Mesh *mesh_sea;
	Quadtree *quadtree;
	float xscale;
	float zscale;
	float hscale;
	/*enum LightingMode {
	LIGHTING_NONE = 0,
	LIGHTING_PRECALCULATED,
	LIGHTING_PERVERTEX
	};
	LightingMode lighting;*/
	bool lighting_enabled;
	//bool color_enabled;
	V_COLORMODE_t colormode;
	bool splat_sort_textures;
	//Shader *pixel_shader;
	int nodes_rendered;

	Vector3D offset;
	//Entity *entity_sea;
	SceneGraphNode *entity_sea_dummy;
	unsigned char pretend_reflection_min_rgb[3];
	unsigned char pretend_reflection_max_rgb[3];
	Texture *pretend_reflection_texture; // used as an imposter for the reflection texture, when not rendering reflections for real
	Texture *waves_bump_texture;

	//Shader *pixel_shader_ambient;
	const ShaderEffect *shader_ambient;

	void buildQuadtreenodeMesh(Quadtreenode *quadtreenode);
	void renderQueueQuadtreenode(GraphicsEnvironment *genv, Quadtreenode *quadtreenode);
	//void renderQuadtreenode(Graphics3D *g, Quadtreenode *quadtreenode);

	unsigned char getTextureMap(int x,int z) const;
	Color getLightmap(int index,int x,int z) const;
	void setLightmap(int index,int x,int z,Color color);
	void multLightmap(int index,int x,int z,Color color);
	void free();
	void createRandomSubdivide(int off_x,int off_z,int size_x,int size_z,int min_height,int max_height,float roughness);
public:

	TerrainEngine(Graphics3D *g3,float xscale,float zscale,float hscale);
	~TerrainEngine();
	virtual V_CLASS_t getClass() const { return V_CLASS_TERRAIN; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_TERRAIN );
	}
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		size += width * depth * sizeof(unsigned char);
		//size += width * height * sizeof(Color);
		return size;
	}

	virtual void prepare();
	bool loadMap(const char *filename);
	void create(int width,int depth);
	void createRandom(int width,int depth,const int base_heights[4],int min_height,int max_height,float roughness,int seed);
	//void setColor(bool color_enabled);
	//void setLightingNone();
	void setColorLightmap(int indx,Color color_amb,Color color_diff,Vector3D dir);
	//void setLightingPerVertex();
	virtual void renderQueue(RenderQueue *queue, GraphicsEnvironment *genv);
	//virtual void render(Graphics3D *g);
	virtual void update(int time_ms);
	SceneGraphNode *getSeaReflection() const {
		return this->entity_sea_dummy;
	}
	static void shaderCallback(const GraphicsEnvironment *genv, const ShaderEffect *shader, const SceneGraphNode *node);

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/

	virtual int getWidth() const {
		return width;
	}
	virtual int getDepth() const {
		return depth;
	}
	virtual float getXScale() const {
		return xscale;
	}
	virtual float getZScale() const {
		return zscale;
	}
	virtual float getHScale() const {
		return hscale;
	}
	virtual float getWidthDist() const {
		// -1, as we care about the physical distance (width/depth refer to the array size of the height vertices)
		return (width-1) * xscale;
	}
	virtual float getDepthDist() const {
		// -1, as we care about the physical distance (width/depth refer to the array size of the height vertices)
		return (depth-1) * zscale;
	}
	virtual unsigned char get(int x,int z) const;
	virtual void set(int x,int z,unsigned char v);
	virtual void setTextureMap(int x,int z,unsigned char v);
	virtual float getHeightAt(float x,float z) const;
	virtual Vector3D getNormalAt(float x,float z) const;
	virtual bool rayfire(Vector3D *hit, Vector3D start, Vector3D dir, float range) const;

	virtual void setOffset(Vector3D offset) {
		this->offset = offset;
		this->recalc = true;
		if( !Vision::isLocked() ) {
			prepare();
		}
	}
	virtual void setLOD(bool lod);
	virtual void setTexture(int index,VI_Texture *texture);
	virtual void setSea(bool sea,float unscaled_sea_height,float sea_extent,bool sea_effects);
	virtual bool hasSea() const {
		return this->sea;
	}
	virtual float getUnscaledSeaHeight() const {
		return this->sea ? this->unscaled_sea_height : 0.0f;
	}
	virtual float getSeaHeight() const {
		return this->getUnscaledSeaHeight() * this->hscale;
	}
	virtual void setSeaTexture(VI_Texture *sea_texture);
	virtual void setLighting(bool lighting_enabled);
	virtual void toggleSeaReflections();
	virtual void setSplatSortTextures(bool splat_sort_textures) {
		this->splat_sort_textures = splat_sort_textures;
	}
	virtual void setPretendReflectionColor(unsigned char min_rgb[3], unsigned char max_rgb[3]) {
		for(int i=0;i<3;i++) {
			this->pretend_reflection_min_rgb[i] = min_rgb[i];
			this->pretend_reflection_max_rgb[i] = max_rgb[i];
		}
		this->recalc = true;
		if( !Vision::isLocked() ) {
			prepare();
		}
	}

	virtual void setColorMode(V_COLORMODE_t colormode);
	//virtual void setColorNoise(int index,float min_r,float min_g,float min_b,float max_r,float max_g,float max_b,float noise_scale);
	virtual void setColorNoisei(int index,unsigned char min_r,unsigned char min_g,unsigned char min_b,unsigned char max_r,unsigned char max_g,unsigned char max_b,float noise_scale);
	virtual void setColorAmbientOcclusion();
	virtual void initColor(int index, unsigned char r, unsigned char g, unsigned char b);
	virtual void setColorAt(int index, int x, int z, unsigned char r, unsigned char g, unsigned char b);

	virtual bool isVisible(int x,int z) const;
	virtual void setVisible(int x,int z,bool v);

	virtual int getNodesRendered() const {
		return nodes_rendered;
	}

	/*virtual void setAmbientPixelShader(Shader *shader) {
		this->pixel_shader_ambient = shader;
		this->recalc = true;
		if( !Vision::isLocked() ) {
			prepare();
		}
	}*/
	virtual void setAmbientShader(const ShaderEffect *shader) {
		this->shader_ambient = shader;
		this->recalc = true;
		if( !Vision::isLocked() ) {
			prepare();
		}
	}
};
