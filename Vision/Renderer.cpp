//---------------------------------------------------------------------------
#include <cassert>
#include <cstdarg>

#include "Renderer.h"
#include "GraphicsEnvironment.h"

//---------------------------------------------------------------------------

bool RendererInfo::multiSample = false;
unsigned int RendererInfo::maxLights = 0;
//int RendererInfo::maxTextureUnits = 0;
bool RendererInfo::hardwareOcclusion = false;
bool RendererInfo::framebufferObject = false;

#if 0
void RendererInfo::initGLExtensions(GraphicsEnvironment *genv) {
	maxLights = 0;
	maxTextureUnits = 0;
	hardwareOcclusion = false;
	compiledVAs = false;
	vertexBufferObjects = false;
	drawRangeElements = false;

	// now we can grab the extension info
	PFNWGLGETEXTENSIONSSTRINGARBPROC glGetExtensionsStringARB =
		(PFNWGLGETEXTENSIONSSTRINGARBPROC)wglGetProcAddress("wglGetExtensionsStringARB");
	const char *wgl_extension = NULL;
	if( glGetExtensionsStringARB ) {
		wgl_extension = glGetExtensionsStringARB(genv->getHDC());
	}

	char *extension = (char *)glGetString(GL_EXTENSIONS);
	if(extension == NULL) {
		/*DestroyWindow(hwnd);
		UnregisterClass("Vision3_dummy", hInstance);*/
		return;
	}
	LOG("Extensions: %s\n", extension);
	if( wgl_extension != NULL ) {
		LOG("WGL Extensions: %s\n", wgl_extension);
	}

	if(wgl_extension != NULL && strstr(wgl_extension, "WGL_ARB_pixel_format")) {
		pixelFormat = true;
		wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
		//__wglewChoosePixelFormatARB(hdc, iattr, 0, 256, formats, &count);
	}
	LOG("WGL Pixel Format? %s\n", pixelFormat?"T":"F");

	if(wgl_extension != NULL && strstr(wgl_extension, "WGL_ARB_multisample")) {
		multiSample = true;
	}
	LOG("Multisample? %s\n", multiSample?"T":"F");

	/*if(wgl_extension != NULL && strstr(wgl_extension, "WGL_ARB_pbuffer") && strstr(wgl_extension, "WGL_ARB_render_texture")) {
	pbuffer = true;
	wglCreatePbufferARB    = (PFNWGLCREATEPBUFFERARBPROC)wglGetProcAddress("wglCreatePbufferARB");
	wglGetPbufferDCARB     = (PFNWGLGETPBUFFERDCARBPROC)wglGetProcAddress("wglGetPbufferDCARB");
	wglReleasePbufferDCARB = (PFNWGLRELEASEPBUFFERDCARBPROC)wglGetProcAddress("wglReleasePbufferDCARB");
	wglDestroyPbufferARB   = (PFNWGLDESTROYPBUFFERARBPROC)wglGetProcAddress("wglDestroyPbufferARB");
	wglQueryPbufferARB     = (PFNWGLQUERYPBUFFERARBPROC)wglGetProcAddress("wglQueryPbufferARB");

	wglBindTexImageARB     = (PFNWGLBINDTEXIMAGEARBPROC)wglGetProcAddress("wglBindTexImageARB");
	wglReleaseTexImageARB  = (PFNWGLRELEASETEXIMAGEARBPROC)wglGetProcAddress("wglReleaseTexImageARB");
	wglSetPbufferAttribARB = (PFNWGLSETPBUFFERATTRIBARBPROC)wglGetProcAddress("wglSetPbufferAttribARB");
	}
	LOG("P Buffer? %s\n", pbuffer?"T":"F");*/

	glGetIntegerv(GL_MAX_LIGHTS, &maxLights);
	LOG("Max Lights? %d\n", maxLights);

	if(strstr(extension, "GL_ARB_multitexture")) {
		glGetIntegerv(GL_MAX_TEXTURE_UNITS_ARB, &maxTextureUnits);

		glMultiTexCoord2fARB = (PFNGLMULTITEXCOORD2FARBPROC)wglGetProcAddress("glMultiTexCoord2fARB");
		glActiveTextureARB = (PFNGLACTIVETEXTUREARBPROC)wglGetProcAddress("glActiveTextureARB");
		glClientActiveTextureARB = (PFNGLCLIENTACTIVETEXTUREARBPROC)wglGetProcAddress("glClientActiveTextureARB");
	}
	LOG("Max Texture Units? %d\n", maxTextureUnits);

	if(strstr(extension, "GL_NV_occlusion_query")) {
		hardwareOcclusion = true;
		glGenOcclusionQueriesNV = (PFNGLGENOCCLUSIONQUERIESNVPROC)wglGetProcAddress("glGenOcclusionQueriesNV");
		glDeleteOcclusionQueriesNV = (PFNGLDELETEOCCLUSIONQUERIESNVPROC)wglGetProcAddress("glDeleteOcclusionQueriesNV");
		glGetOcclusionQueryuivNV = (PFNGLGETOCCLUSIONQUERYUIVNVPROC)wglGetProcAddress("glGetOcclusionQueryuivNV");
		glBeginOcclusionQueryNV = (PFNGLBEGINOCCLUSIONQUERYNVPROC)wglGetProcAddress("glBeginOcclusionQueryNV");
		glEndOcclusionQueryNV = (PFNGLENDOCCLUSIONQUERYNVPROC)wglGetProcAddress("glEndOcclusionQueryNV");
	}
	LOG("Hardware Occlusion? %s\n", hardwareOcclusion?"T":"F");

	if(strstr(extension, "GL_EXT_compiled_vertex_array")) {
		compiledVAs = true;
		glLockArraysEXT = (PFNGLLOCKARRAYSEXTPROC)wglGetProcAddress("glLockArraysEXT");
		glUnlockArraysEXT = (PFNGLUNLOCKARRAYSEXTPROC)wglGetProcAddress("glUnlockArraysEXT");
	}
	LOG("Compiled Vertex Array? %s\n", compiledVAs?"T":"F");

	//if(false) {
	if(strstr(extension, "GL_ARB_vertex_buffer_object")) {
		vertexBufferObjects = true;
		glGenBuffersARB = (PFNGLGENBUFFERSARBPROC) wglGetProcAddress("glGenBuffersARB");
		glBindBufferARB = (PFNGLBINDBUFFERARBPROC) wglGetProcAddress("glBindBufferARB");
		glBufferDataARB = (PFNGLBUFFERDATAARBPROC) wglGetProcAddress("glBufferDataARB");
		glBufferSubDataARB = (PFNGLBUFFERSUBDATAARBPROC) wglGetProcAddress("glBufferSubDataARB");
		glDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC) wglGetProcAddress("glDeleteBuffersARB");
	}
	LOG("VBOs? %s\n", vertexBufferObjects?"T":"F");

	//if(false) {
	if(strstr(extension, "GL_EXT_draw_range_elements")) {
		drawRangeElements = true;
		glDrawRangeElementsEXT = (PFNGLDRAWRANGEELEMENTSPROC) wglGetProcAddress("glDrawRangeElementsEXT");
	}
	LOG("Draw Range Elements? %s\n", drawRangeElements?"T":"F");

	if(strstr(extension, "GL_ATI_separate_stencil") && strstr(extension, "GL_EXT_stencil_wrap") ) {
		separateStencilATI = true;
		glStencilOpSeparateATI = (PFNGLSTENCILOPSEPARATEATIPROC) wglGetProcAddress("glStencilOpSeparateATI");
		glStencilFuncSeparateATI = (PFNGLSTENCILFUNCSEPARATEATIPROC) wglGetProcAddress("glStencilFuncSeparateATI");
	}
	LOG("ATI Two Sided Stencil? %s\n", separateStencilATI?"T":"F");

	if(strstr(extension, "GL_EXT_stencil_two_side") && strstr(extension, "GL_EXT_stencil_wrap") ) {
		separateStencil = true;
		glActiveStencilFaceEXT = (PFNGLACTIVESTENCILFACEEXTPROC) wglGetProcAddress("glActiveStencilFaceEXT");
	}
	LOG("Two Sided Stencil? %s\n", separateStencil?"T":"F");

	if(strstr(extension, "GL_EXT_framebuffer_object")) {
		framebufferObject = true;
		glBindRenderbufferEXT = (PFNGLBINDRENDERBUFFEREXTPROC) wglGetProcAddress("glBindRenderbufferEXT");
		glDeleteRenderbuffersEXT = (PFNGLDELETERENDERBUFFERSEXTPROC) wglGetProcAddress("glDeleteRenderbuffersEXT");
		glGenRenderbuffersEXT = (PFNGLGENRENDERBUFFERSEXTPROC) wglGetProcAddress("glGenRenderbuffersEXT");
		glRenderbufferStorageEXT = (PFNGLRENDERBUFFERSTORAGEEXTPROC) wglGetProcAddress("glRenderbufferStorageEXT");
		glBindFramebufferEXT = (PFNGLBINDFRAMEBUFFEREXTPROC) wglGetProcAddress("glBindFramebufferEXT");
		glDeleteFramebuffersEXT = (PFNGLDELETEFRAMEBUFFERSEXTPROC) wglGetProcAddress("glDeleteFramebuffersEXT");
		glGenFramebuffersEXT = (PFNGLGENFRAMEBUFFERSEXTPROC) wglGetProcAddress("glGenFramebuffersEXT");

		glCheckFramebufferStatusEXT = (PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC) wglGetProcAddress("glCheckFramebufferStatusEXT");
		glFramebufferTexture2DEXT = (PFNGLFRAMEBUFFERTEXTURE2DEXTPROC) wglGetProcAddress("glFramebufferTexture2DEXT");
		glFramebufferRenderbufferEXT = (PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC) wglGetProcAddress("glFramebufferRenderbufferEXT");
	}
	LOG("FBOs? %s\n", framebufferObject?"T":"F");

	/*DestroyWindow(hwnd);
	UnregisterClass("Vision3_dummy", hInstance);*/
}
#endif

Shader::~Shader() {
	LOG("deleting %s shader %d\n", vertex_shader?"vertex":"pixel", this);
}

/*Shader *Shader::createShader(VI_GraphicsEnvironment *genv, const char *file, const char *func,bool vertex_shader) {
	//GraphicsEnvironment *d_genv = static_cast<GraphicsEnvironment *>(genv); // cast to internal representation
	Shader *shader = NULL;
	// if we fail to load a shader, not a problem - just return NULL (allows us to continue without this shader - handled at the Graphics3D level)
	try {
		//shader = d_genv->createShader(file, func, vertex_shader);
		shader = genv->getRenderer()->createShaderCG(file, func, vertex_shader);
	}
	catch(VisionException *) {
		LOG("failed to load shader\n");
		shader = NULL;
	}
	//if( shader != NULL ) {
	//	genv->getRenderer()->addShader(shader);
	//}
	return shader;
}*/

/*void Shader::setDefaults() const {
	// sets some sensible defaults
	const float zero[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	this->setMatSpecular(zero);
}*/

/*Shader *Shader::createShader(Graphics3D *g3, char *file, char *func,bool vertex_shader) {
	if( !g3->getGraphicsEnvironment()->hasShaders() )
		return NULL;
	Shader *shader = new Shader(g3, vertex_shader);
	shader->cgProgram = cgCreateProgramFromFile(g3->getGraphicsEnvironment()->cgContext, CG_SOURCE, file, shader->cgProfile, func, 0);
	if( shader->cgProgram == NULL ) {
		LOG("### Failed to load vertex shader:\n");
		CGerror Error = cgGetError();
		LOG(cgGetErrorString(Error));
		LOG("\n");
		delete shader;
		g3->getGraphicsEnvironment()->cgContext = NULL;
		return NULL;
	}
	LOG("Successfully loaded %s shader %d\n",vertex_shader?"vertex":"pixel",shader);
	cgGLLoadProgram(shader->cgProgram);
	shader->init();
	g3->addShader(shader);

	return shader;
}*/

ShaderEffectSimple::ShaderEffectSimple(const Shader *vertex_shader, const Shader *pixel_shader) : vertex_shader(vertex_shader), pixel_shader(pixel_shader) {
	if( vertex_shader == NULL ) {
		Vision::setError(new VisionException(NULL, VisionException::V_RENDER_ERROR, "No vertex shader supplied"));
	}
	if( pixel_shader == NULL ) {
		Vision::setError(new VisionException(NULL, VisionException::V_RENDER_ERROR, "No pixel shader supplied"));
	}
}

ShaderEffectSimple::~ShaderEffectSimple() {
	if( this->vertex_shader != NULL ) {
		delete this->vertex_shader;
	}
	if( this->pixel_shader != NULL ) {
		delete this->pixel_shader;
	}
}

void ShaderEffectSimple::setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const {
	vertex_shader->setMatrices(parameter_modelviewproj, parameter_modelviewit, parameter_modelview);
	pixel_shader->setMatrices(parameter_modelviewproj, parameter_modelviewit, parameter_modelview);
}

void ShaderEffectSimple::SetUniformParameter1f(const char *parameter, float p1) const {
	vertex_shader->SetUniformParameter1f(parameter, p1);
	pixel_shader->SetUniformParameter1f(parameter, p1);
}

void ShaderEffectSimple::SetUniformParameter2f(const char *parameter, float p1, float p2) const {
	vertex_shader->SetUniformParameter2f(parameter, p1, p2);
	pixel_shader->SetUniformParameter2f(parameter, p1, p2);
}

void ShaderEffectSimple::SetUniformParameter3f(const char *parameter, float p1, float p2, float p3) const {
	vertex_shader->SetUniformParameter3f(parameter, p1, p2, p3);
	pixel_shader->SetUniformParameter3f(parameter, p1, p2, p3);
}

void ShaderEffectSimple::SetUniformParameter4f(const char *parameter, float p1, float p2, float p3, float p4) const {
	vertex_shader->SetUniformParameter4f(parameter, p1, p2, p3, p4);
	pixel_shader->SetUniformParameter4f(parameter, p1, p2, p3, p4);
}

void ShaderEffectSimple::SetUniformParameter4fv(const char *parameter, const float v[4]) const {
	vertex_shader->SetUniformParameter4fv(parameter, v);
	pixel_shader->SetUniformParameter4fv(parameter, v);
}

void ShaderEffectSimple::SetModelViewProjMatrix(const char *parameter) const {
	vertex_shader->SetModelViewProjMatrix(parameter);
	pixel_shader->SetModelViewProjMatrix(parameter);
}

void ShaderEffectSimple::enable(Renderer *renderer) const {
	ASSERT(vertex_shader != NULL);
	ASSERT(pixel_shader != NULL);
	this->vertex_shader->enable();
	this->pixel_shader->enable();
	renderer->setActiveShader(this);
}

Texture::Texture() : wrap_mode(WRAPMODE_REPEAT), draw_mode(DRAWMODE_SMOOTH) {
}

/*Texture::Texture(bool mipmap) : Image2D() {
this->mipmap = mipmap;
this->textureID = 0;
}*/

Texture::~Texture() {
	//GraphicsEnvironment::checkGLError("Texture::~Texture enter");
	/*if(filename != NULL)
		delete [] filename;*/
	//GraphicsEnvironment::checkGLError("Texture::~Texture exit");
}

FontBuffer::FontBuffer() : VisionObject() {
}

int FontBuffer::writeTextExt(int x,int y,const char *text,...) const {
	va_list vlist;
	const int BUFSIZE = 65535;
	char buff[BUFSIZE+1];
	va_start(vlist, text);
	int length = vsnprintf(buff, BUFSIZE, text, vlist);
	va_end(vlist);
	ASSERT( length >= 0 );
	return this->writeText(x,y,buff, length);
}

Graphics2D::Graphics2D(GraphicsEnvironment *genv) : VisionObject(), genv(genv), curr_x(0), curr_y(0), texture(NULL) {
}

Renderer *Graphics2D::getRenderer() const {
	return this->genv->getRenderer();
}

void Graphics2D::move(short x,short y) {
	curr_x = x;
	curr_y = y;
}

//Renderer::Renderer(Graphics3D *g3) : g3(g3), c_pixel_shader(NULL), c_vertex_shader(NULL) {
Renderer::Renderer(GraphicsEnvironment *genv) : genv(genv), static_transformationMatrix(NULL), /*c_pixel_shader(NULL), c_vertex_shader(NULL), */c_shader(NULL), z_near(0.1f), z_far(1000.0f), persp(true) {
//Renderer::Renderer() : c_pixel_shader(NULL), c_vertex_shader(NULL) {
	//this->hdr_fbo = NULL;
	//this->hdr_texture = NULL;
	//this->hdr_depth_stencil_texture = NULL;
	//this->static_transformationMatrix = this->createTransformationMatrix();
	// NB - we create static_transformationMatrix lazily rather than here, as when in a constructor, we can't call the derived classes virtual functions
}

Renderer::~Renderer() {
	delete this->static_transformationMatrix;
}

/*void Renderer::disableShaders() {
	if( this->c_shader != NULL ) {
		this->c_shader->disable(this);
	}
}*/

ShaderEffect *Renderer::createShaderEffectSimpleCG(const char *file, const char *func_vs,const char *func_ps) {
	// n.b., these functions will throw exceptions if there's a problem
	// but we need to catch the exception, so that we can delete the vertex_shader if there was a problem with loading the pixel shader
	Shader *vertex_shader = NULL;
	Shader *pixel_shader = NULL;
	try {
		vertex_shader = this->createShaderCG(file, func_vs, true);
		pixel_shader = this->createShaderCG(file, func_ps, false);
	}
	catch(...) {
		LOG("failed to load at least one of either vertex or pixel shader\n");
		if( vertex_shader != NULL ) {
			delete vertex_shader;
			vertex_shader = NULL;
		}
		if( pixel_shader != NULL ) { // shouldn't happen here, but just to be consistent
			delete pixel_shader;
			pixel_shader = NULL;
		}
		throw; // rethrow the error, to indicate failure to the caller
	}
	ShaderEffect *shader_effect = new ShaderEffectSimple(vertex_shader, pixel_shader);
	this->addShaderEffect(shader_effect);
	return shader_effect;
}
