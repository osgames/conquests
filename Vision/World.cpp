//---------------------------------------------------------------------------
//#include <ctime>

#include "World.h"
#include "Entity.h"
#include "Terrain.h"

//---------------------------------------------------------------------------

World::World() : VisionObject() {
	genv = NULL;
	terrain = NULL;
	skybox = NULL;
	clouds = NULL;
	atmosphere = NULL;
	sector.rootObject = new SceneGraphNode();
	sector.rootObject->setWorld(this);
	lightAmbient.seti(204,204,204);
	backColor.seti(0,0,0);
	fog.enable(false);

	defViewPoint = new SceneGraphNode();
	viewPoint = defViewPoint;
	sector.rootObject->addChildNode(viewPoint);

	//sector.spatialindex = new SpatialGrid2D(-5.0, -10.0, -5.0, 330.0, 20.0, 330.0, 8, 8);
	//sector.spatialindex = new Quadtree(-5.0, -10.0, -5.0, 330.0, 20.0, 330.0, 5);

	//timer = this->getRealTimeMS();
	//Vision::addWorld(this);
}

World::~World() {
	if( sector.rootObject != NULL )
		delete sector.rootObject;
	if( sector.spatialindex != NULL )
		delete sector.spatialindex;
}

size_t World::memUsage() {
	size_t size = sizeof(this);
	return size;
}


void World::setGraphicsEnvironment(GraphicsEnvironment *genv) {
	this->genv = genv;
}

bool World::addNode(VI_SceneGraphNode *node) {
	if(getRootNode() != NULL) {
		//SceneGraphNode *d_node = static_cast<SceneGraphNode *>(node); // cast to the internal representation
		SceneGraphNode *d_node = dynamic_cast<SceneGraphNode *>(node); // cast to the internal representation
		getRootNode()->addChildNode(d_node);
		if( this->sector.spatialindex != NULL ) {
			this->sector.spatialindex->addNode(d_node);
		}
		return true;
	}
	return false;
}

void World::setSky(VI_Sky *sky) {
	SkyBox *d_sky = static_cast<SkyBox *>(sky); // cast to the internal representation
	this->skybox = d_sky;
}

void World::setClouds(VI_Clouds *clouds) {
	Clouds *d_clouds = static_cast<Clouds *>(clouds); // cast to the internal representation
	this->clouds = d_clouds;
}

void World::setAtmosphere(VI_Atmosphere *atmosphere) {
	Atmosphere *d_atmosphere = static_cast<Atmosphere *>(atmosphere); // cast to the internal representation
	this->atmosphere = d_atmosphere;
}

void World::setTerrain(VI_Terrain *terrain) {
	TerrainEngine *d_terrain = static_cast<TerrainEngine *>(terrain); // cast to the internal representation
	this->terrain = d_terrain;
}

void World::removeAllObjects() {
	SceneGraphNode *root = this->getRootNode();
	for(size_t i=0;i<root->getNChildNodes();i++) {
		SceneGraphNode *child = dynamic_cast<SceneGraphNode *>(root->getChildNode(i));
		if( child != this->viewPoint )
			child->kill();
	}
}

void World::update(int time_ms) {
	if(getRootNode() != NULL) {
		getRootNode()->update(time_ms, true);
	}

	if(viewPoint->getParentNode() == NULL) {
		// viewpoint can't be in the object hierarchy
		viewPoint->update(time_ms, true);
	}
	if(terrain != NULL)
		terrain->update(time_ms);
}

void World::init() {
}

void World::getAmbience(float *r,float *g,float *b) {
	*r = this->lightAmbient.getRf();
	*g = this->lightAmbient.getGf();
	*b = this->lightAmbient.getBf();
}

void World::setAmbiencei(unsigned char r, unsigned char g, unsigned char b) {
	this->lightAmbient.seti(r, g, b);
}

void World::setViewpoint(VI_SceneGraphNode *viewpoint) {
	SceneGraphNode *d_viewpoint = dynamic_cast<SceneGraphNode *>(viewpoint); // cast to the internal representation
	this->viewPoint = d_viewpoint;
}
