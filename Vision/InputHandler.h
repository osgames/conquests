#pragma once

class GraphicsEnvironment;
class Point3D;

#include "VisionUtils.h"
#include "VisionIface.h"

/*#define KEY_DOWN(x) InputHandler::keys[x]
#define KEY_UP(x) !InputHandler::keys[x]*/
/*#define KEY_DOWN_ASYNC(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define KEY_UP_ASYNC(vk_code)   ((GetAsyncKeyState(vk_code) & 0x8000) ? 0 : 1)*/

/*#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define KEY_UP(vk_code)   ((GetAsyncKeyState(vk_code) & 0x8000) ? 0 : 1)
#define KEY_DOWN_ASYNC KEY_DOWN
#define KEY_UP_ASYNC KEY_UP*/

const int N_KEYS = 256;

/*class InputHandler {
	bool mouseL,mouseM,mouseR;
	bool mouseClickL,mouseClickM,mouseClickR;
	bool q_mouseClickL,q_mouseClickM,q_mouseClickR;
public:
	bool handled_event;
	bool keys[N_KEYS];
	int mouseX,mouseY;

	InputHandler() {
		handled_event = false;
		mouseL = false;
		mouseM = false;
		mouseR = false;
		q_mouseClickL = false;
		q_mouseClickM = false;
		q_mouseClickR = false;
		flush();
	}
	virtual ~InputHandler() {
	}
	void setMouse(GraphicsEnvironment *genv);
	void setMouse(int mouseX,int mouseY,bool mouseL,bool mouseM,bool mouseR);
	bool isLMousePressed() {
		return !handled_event && mouseL;
		//return mouseL;
	}
	bool isMMousePressed() {
		return !handled_event && mouseM;
		//return mouseM;
	}
	bool isRMousePressed() {
		return !handled_event && mouseR;
		//return mouseR;
	}
	bool isLMouseClicked();
	bool isMMouseClicked();
	bool isRMouseClicked();
	void mouseLChange(bool down);
	void mouseMChange(bool down);
	void mouseRChange(bool down);
	bool keyAny();
	void flushMouse();
	void flushKeyboard() {
		for(int i=0;i<N_KEYS;i++) {
			keys[i] = false;
		}
	}
	void flush() {
		flushMouse();
		flushKeyboard();
	}
	void readQueue() {
		mouseClickL = q_mouseClickL;
		mouseClickM = q_mouseClickM;
		mouseClickR = q_mouseClickR;
		q_mouseClickL = false;
		q_mouseClickM = false;
		q_mouseClickR = false;
	}
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}
};*/

class InputHandler {
public:
	bool handled_event;

	InputHandler() {
		handled_event = false;
	}
	virtual ~InputHandler() {
	}
	virtual void flush()=0;
	virtual void readQueue(GraphicsEnvironment *genv)=0; // called before checking input
	virtual void update()=0; // called after checking input
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}
};

class MouseInputHandler : public InputHandler {
	bool mouseL, mouseM, mouseR;
	//bool mouseClickL, mouseClickM, mouseClickR;
	//bool q_mouseClickL, q_mouseClickM, q_mouseClickR;
	bool mouseWheelUp, mouseWheelDown;
public:
	int mouseX,mouseY;

	MouseInputHandler() : InputHandler() {
		mouseL = false;
		mouseM = false;
		mouseR = false;
		/*mouseClickL = false;
		mouseClickM = false;
		mouseClickR = false;
		q_mouseClickL = false;
		q_mouseClickM = false;
		q_mouseClickR = false;*/
		mouseWheelUp = false;
		mouseWheelDown = false;
	}
	//virtual void setMouse(GraphicsEnvironment *genv)=0;
	void setMouse(int mouseX,int mouseY/*,bool mouseL,bool mouseM,bool mouseR*/);
	bool isLMousePressed() {
		return !handled_event && mouseL;
		//return mouseL;
	}
	bool isMMousePressed() {
		return !handled_event && mouseM;
		//return mouseM;
	}
	bool isRMousePressed() {
		return !handled_event && mouseR;
		//return mouseR;
	}
	/*bool isLMouseClicked();
	bool isMMouseClicked();
	bool isRMouseClicked();*/
	void mouseLChange(bool down);
	void mouseMChange(bool down);
	void mouseRChange(bool down);
	/*bool readMouseWheelUp() {
		return mouseWheelUp;
	}
	bool readMouseWheelDown() {
		return mouseWheelDown;
	}*/
	void setMouseWheelUp() {
		this->mouseWheelUp = true;
	}
	void setMouseWheelDown() {
		this->mouseWheelDown = true;
	}
	virtual void flush();
	virtual void readQueue(GraphicsEnvironment *genv) {
		/*mouseClickL = q_mouseClickL;
		mouseClickM = q_mouseClickM;
		mouseClickR = q_mouseClickR;
		q_mouseClickL = false;
		q_mouseClickM = false;
		q_mouseClickR = false;*/
	}
	virtual void update();
};

class KeyboardInputHandler : public InputHandler {
	//bool keys_old[N_KEYS];
	bool keys[N_KEYS];
	/*bool keysClicked[N_KEYS];
	bool q_keysClicked[N_KEYS];*/
public:

	KeyboardInputHandler() : InputHandler() {
		for(int i=0;i<N_KEYS;i++) {
			//keys_old[i] = false;
			keys[i] = false;
			/*keysClicked[i] = false;
			q_keysClicked[i] = false;*/
		}
	}
	bool keyAny();
	virtual void flush();
	/*void flush(int key) {
		//keys_old[key] = false;
		keys[key] = false;
		keysClicked[key] = false;
		//keys_old[key] = keys[key];
	}*/
	virtual void readQueue(GraphicsEnvironment *genv);
	virtual void update();
	void setKey(int key, bool down) {
		this->keys[key] = down;
		/*if( down )
			this->q_keysClicked[key] = true;*/
	}
	bool isPressed(int key) const {
		return keys[key];
	}
	/*bool wasPressed(int key) const {
		// returns whether pressed since the last frame
		//return keys[key] && !keys_old[key];
		return keysClicked[key];
	}*/
	/*bool wasPressed(int key, bool repeat) const {
		// returns whether pressed since the last frame, unless repeat is true
		//return keys[key] && ( repeat || !keys_old[key] );
		if( repeat )
			return keys[key];
		return keysClicked[key];
	}*/
};

class InputSystem {
	MouseInputHandler *mouseInputHandler;
	KeyboardInputHandler *keyboardInputHandler;
public:
	VI_Panel_Action *pushed_action;
	VI_Panel        *pushed_action_src;
	VI_Panel_Action *pushed_mouse_enter_action;
	VI_Panel        *pushed_mouse_enter_action_src;
	VI_Panel_Action *pushed_mouse_exit_action;
	VI_Panel        *pushed_mouse_exit_action_src;

	InputSystem(MouseInputHandler *mouseInputHandler, KeyboardInputHandler *keyboardInputHandler);
	~InputSystem();

	MouseInputHandler *getMouse() const {
		return this->mouseInputHandler;
	}
	KeyboardInputHandler *getKeyboard() const {
		return this->keyboardInputHandler;
	}
	size_t memUsage() {
		size_t size = sizeof(this);
		size += mouseInputHandler->memUsage();
		size += keyboardInputHandler->memUsage();
		return size;
	}
};

#include "GraphicsEnvironment.h"

inline bool KEY_DOWN(int x) {
	GraphicsEnvironment *ge = GraphicsEnvironment::getSingleton();
	//InputHandler *ih = ge->getInputHandler();
	//KeyboardInputHandler *ih = ge->getKeyboardInputHandler();
	InputSystem *is = ge->getInputSystem();
	KeyboardInputHandler *ih = is->getKeyboard();
	//return ih->keys[x];
	return ih->isPressed(x);
}

inline bool KEY_UP(int x) {
	GraphicsEnvironment *ge = GraphicsEnvironment::getSingleton();
	//InputHandler *ih = ge->getInputHandler();
	//KeyboardInputHandler *ih = ge->getKeyboardInputHandler();
	InputSystem *is = ge->getInputSystem();
	KeyboardInputHandler *ih = is->getKeyboard();
	//return !ih->keys[x];
	return !ih->isPressed(x);
}

class Movement {
public:
	float vsp;
	float rot_sp;
	float sp;
	bool onground;

	float speed;

	Vector3D speed3d;
	int throttle;

	Movement();
	~Movement();

	friend class InputHandler;

	void inputXY(Point3D *point,int time);
	void inputXYHover(Point3D *point,int time);
	void inputFlightSimple(Point3D *point,int time);
	void inputFlight(Point3D *point,int time,float groundLevel);

	void setSpeed(float vsp,float rot_sp,float sp) {
		this->vsp=vsp;
		this->rot_sp=rot_sp;
		this->sp=sp;
	}
};
