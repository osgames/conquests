//---------------------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS

#include <cfloat>
#include <cassert>
#include <ctime>
#include <cstdarg>

#include <map>
using std::map;
using std::pair;
#include <string>
using std::string;
#include <sstream>
using std::stringstream;

#ifdef _WIN32
#include <io.h> // for access
#include <direct.h> // for mkdir
#define access _access
#elif __linux
#include <sys/stat.h>
#endif

#include "Resource.h"
#include "GraphicsEngine.h"
#include "GraphicsEnvironment.h"
#include "World.h"
#include "Entity.h"

//---------------------------------------------------------------------------

//bool x_DEBUG = false;

bool Vision::v_initialised = false;

vector<VisionObject *> Vision::tags;
VSafedeleteCollection Vision::safedeletequeue;
bool Vision::any_kills = false;

queue<void *> Vision::message_queue;

int Vision::default_persistence = 0;

int Vision::lock_level = 0;

size_t Vision::next_creation_id = 0;

int Vision::game_time_ms = 0;
int Vision::game_time_last_frame_ms = 0;

VisionObject::VisionObject() : owner(NULL), /*owned(false),*/ deleteLevel(0), creation_id(0), tag(0) {
	Vision::addTag(this); // also sets tag and creation_id
	this->deleteLevel = Vision::getDefaultPersistence();
	//LOG("Creating VisionObject (Tag %d, Ptr %d)\n",tag,this);
}

VisionObject::~VisionObject() {
	//LOG(">>> about to delete tag %d\n", this->tag);
	Vision::removeTag(this->tag);
	//LOG("removed tag %d\n", this->tag);

	this->detachFromOwner(); // detach from any owner
	this->deleteAllOwnedObjects();
	//LOG("<<< deleted tag %d\n", this->tag);
}

void VisionObject::detachFromOwner() {
	if( this->owner != NULL ) {
		this->owner->removeOwnedObject(this);
	}
}

void VisionObject::removeAllOwnedObjects() {
	/*while( this->owned_vision_objects.size() > 0 ) {
		VisionObject *child = *this->owned_vision_objects.begin();
		this->removeOwnedObject(child);
	}*/
	for(set<VisionObject *>::iterator iter = owned_vision_objects.begin(); iter != owned_vision_objects.end(); ++iter) {
		VisionObject *child = *iter;
		child->owner = NULL;
	}
	owned_vision_objects.clear();
}

void VisionObject::deleteAllOwnedObjects() {
	for(set<VisionObject *>::iterator iter = owned_vision_objects.begin(); iter != owned_vision_objects.end(); ++iter) {
		VisionObject *child = *iter;
		//LOG("### %d\n", child->tag);
		child->owner = NULL; // needed so we don't remove from this object's owned_vision_objects vector when deleting the child
		delete child;
	}
	owned_vision_objects.clear();
}

bool VisionObject::removeOwnedObject(VI_VisionObject *child) {
	VisionObject *d_child = dynamic_cast<VisionObject *>(child); // cast to the internal representation
	size_t n = owned_vision_objects.erase(d_child);
	if( n != 0 ) {
		// removed from list
		ASSERT( d_child->owner == this );
		d_child->owner = NULL;
		return true;
	}
	ASSERT( d_child->owner != this );
	return false;
}

void VisionObject::setName(const char *name) {
	this->name = name;
}

const char *VisionObject::getName() const {
	return this->name.c_str();
}

bool VisionObject::check() const {
	bool ok = true;
	if( ok && ( this->tag <= 0 || this->tag > Vision::getNTags() ) ) {
		LOG("Tag %d outsize of memory space\n",this->tag);
		ok = false;
	}
	return ok;
}

FILE *logfile = NULL;
/*#ifdef _WIN32
char application_path[MAX_PATH] = "";
char logfilename[MAX_PATH] = "";
char assertlogfilename[MAX_PATH] = "";
char oldlogfilename[MAX_PATH] = "";
#else
char application_path[] = "";
char logfilename[] = "log.txt";
char assertlogfilename[] = "log_assertion_failure.txt";
char oldlogfilename[] = "log_old.txt";
#endif*/
string logname = "log.txt";
string assertlogname = "log_assertion_failure.txt";
string oldlogname = "log_old.txt";

string application_path;
string logfilename;
string assertlogfilename;
string oldlogfilename;

string application_name_str;

map<string, bool> checked_application_data_folder;
map<string, string> application_data_folder;

const char *Vision::getApplicationName() {
	return application_name_str.c_str();
}

/* Returns a full path for a filename for where the application's data is stored.
 * Folder must be non-empty, and have the trailing '/'.
 * By default, this returns "folder/name". However, on Linux, it first checks if the "folder/"
 * folder exists. If not, it instead returns "/usr/share/application_name/folder/name".
 * This is to accomodate Linux installations, where typically the application data should be located
 * in a folder such as /usr/share/application_name, and not the same folder as the executable. However,
 * we still want to support running the application from a folder without installation (also necessary
 * when developing the program).
 * UPDATE: It's probably better these days to store Linux data in a subfolder (when installing to
 * /opt) anyway, but this code is kept for backwards compatibility.
 */
string Vision::getApplicationDataFilename(const char *folder, const char *name) {
	if( !checked_application_data_folder[folder] ) {
	    string application_folder = folder;
		LOG("looking for application data folder for: %s\n", application_folder.c_str());
		bool ok = false;
		if( access(application_folder.c_str(), 0) == 0 ) {
			// folder exists
			ok = true;
		}
#ifdef __linux
		if( !ok ) {
			// try alternative location
			application_folder = "/usr/share/" + application_name_str + "/" + application_folder;
			if( access(application_folder.c_str(), 0) == 0 ) {
				// folder exists
				ok = true;
			}
		}
#endif
		if( ok ) {
			LOG("using application folder: %s\n", application_folder.c_str());
		}
		else {
			LOG("can't find application folder! will probably fail to load...\n");
		}
		checked_application_data_folder[folder] = true;
		application_data_folder[folder] = application_folder;
	}
	string full_path = application_data_folder[folder] + name;
	return full_path;
}

/* Returns a full path for a filename in userspace (i.e., where we'll have read/write access).
 * For Windows, this is in %APPDATA%/application_name/
 * For Linux, this is in user's home/.config/application_name/ (note the '.', to make it a hidden folder)
 * If the folder can't be accessed (or running on a new operating system), the program folder is used.
 */
char *Vision::getApplicationFilename(const char *name) {
    // not safe to use LOG here, as logfile may not have been initialised!
/*#ifdef _WIN32
	char *filename = new char[MAX_PATH];
	strcpy(filename, application_path.c_str());
	PathAppendA(filename, name);
#else
	char *filename = new char[strlen(name)+1];
	strcpy(filename, name);
#endif*/
	char *filename = NULL;
	int application_path_len = application_path.length();
	if( application_path_len == 0 || application_path[application_path_len-1] == '/' ) {
		// shouldn't add path separator
		int len = application_path_len + strlen(name);
		filename = new char[len+1];
		sprintf(filename, "%s%s", application_path.c_str(), name);
	}
	else {
		// should add path separator
		int len = application_path_len + 1 + strlen(name);
		filename = new char[len+1];
		sprintf(filename, "%s/%s", application_path.c_str(), name);
	}
	return filename;
}

void Vision::openLogFile(const char *application_name) {
    // not safe to use LOG here, as logfile not initialised!
#ifdef _WIN32
	// first need to establish full path, and create folder if necessary
	bool ok = true;
	char folder_path[MAX_PATH] = "";
    if ( SUCCEEDED( SHGetFolderPathA( NULL, CSIDL_APPDATA,
                                     NULL, 0, folder_path ) ) ) {
        PathAppendA(folder_path, application_name);

		if( access(folder_path, 0) != 0 ) {
			// folder doesn't seem to exist - try creating it
			int res = mkdir(folder_path);
			//int res = 1; // test
			if( res != 0 ) {
				printf("Failed to create folder for application data!\n");
				MessageBoxA(NULL, "Failed to create folder for application data - storing in local folder instead.\n", "Warning", MB_OK|MB_ICONEXCLAMATION);
				ok = false;
			}
		}
    }
	else {
		printf("Failed to obtain path for application folder!\n");
		MessageBoxA(NULL, "Failed to obtain path for application folder - storing in local folder instead.\n", "Warning", MB_OK|MB_ICONEXCLAMATION);
		ok = false;
	}

	if( ok ) {
		application_path = folder_path;
		/*strcpy(assertlogfilename, logfilename);
		strcpy(oldlogfilename, logfilename);
		PathAppendA(logfilename, "log.txt");
		PathAppendA(assertlogfilename, "log_assertion_failure.txt");
		PathAppendA(oldlogfilename, "log_old.txt");*/
	}
	else {
		// just save in local directory and hope for the best!
		application_path = "";
		/*strcpy(application_path, "");
		strcpy(logfilename, "log.txt");
		strcpy(assertlogfilename, "log_assertion_failure.txt");
		strcpy(oldlogfilename, "log_old.txt");*/
	}
#elif __linux
	char *homedir = getenv("HOME");
	/*char *subdir = "/.";
	int len = strlen(homedir) + strlen(subdir) + strlen(application_name);
	application_path = new char[len+1];
	sprintf(application_path, "%s%s%s", homedir, subdir, application_name);*/
	stringstream application_path_ss;
	//application_path_ss << homedir <<  "/." << application_name;
	application_path_ss << homedir <<  "/.config/" << application_name;
	application_path = application_path_ss.str();

	// create the folder if it doesn't already exist
	bool ok = true;
	if( access(application_path.c_str(), 0) != 0 ) {
		int res = mkdir(application_path.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
		if( res != 0 ) {
			ok = false;
		}
	}

	if( !ok ) {
		// just save in local directory and hope for the best!
		application_path = "";
	}
#else
	// save in local directory
	application_path = "";
#endif
	logfilename = getApplicationFilename(logname.c_str());
	assertlogfilename = getApplicationFilename(assertlogname.c_str());
	oldlogfilename = getApplicationFilename(oldlogname.c_str());

	remove(oldlogfilename.c_str());
	rename(logfilename.c_str(), oldlogfilename.c_str());
	remove(logfilename.c_str());
}

bool V_logText(const char *text,...) {
	//return true;
	logfile = fopen(logfilename.c_str(), "a+");
	if( !logfile )
		return false;
	/*{
		fopen_s(&logfile, "c:\\temp\\test.txt", "a+");
		if( logfile != NULL )
			fclose(logfile);
	}*/
	/*if( fopen_s(&logfile, "log.txt", "a+") != 0 )
		return false;*/
	va_list vlist;
	const int BUFSIZE = 65535;
	char buffer[BUFSIZE+1];
	va_start(vlist, text);
	//vprintf(text,vlist);
	//vsprintf(buffer,text,vlist);
	vsnprintf(buffer, BUFSIZE, text, vlist);
	fprintf(logfile,buffer);
	printf(buffer);
	va_end(vlist);
	fclose(logfile);
	return true;
}

void V_assert_failed() {
#ifdef _WIN32
	// TODO for other platforms
	//CopyFileA("log.txt", "log_assertion_failure.txt", false);
	CopyFileA(logfilename.c_str(), assertlogfilename.c_str(), false);
#endif
}

#ifdef _DEBUG

class Timeinfo {
	int totaltime;
	int calls;
public:
	Timeinfo(int time) : totaltime(time), calls(1) {
	}
	void update(int time) {
		this->totaltime += time;
		this->calls++;
	}
	int getTotaltime() const {
		return totaltime;
	}
	int getCalls() const {
		return calls;
	}
};
map<string, Timeinfo> timesMap;
int entiretime = 0;
int V__tracelevel = 0;

void V__logtime(const char *file, const char *func, int time) {
	return;
	//LOG("%d : file %s, func %s, time: %d\n", V__tracelevel, file, func, time);
	if( V__tracelevel == 0 ) {
		//LOG("TOP LEVEL %s\n", func);
		entiretime += time;
	}
	else if( V__tracelevel < 0 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_TRACETIMING_ERROR, "Negative trace level"));
	}
	/*if( V__tracelevel != 0 ) {
		return;
	}*/
	std::stringstream out;
	out << func << " " << V__tracelevel;
	string name = out.str();
	map<string, Timeinfo>::iterator iter = timesMap.find(name);
	if( iter == timesMap.end() ) {
		Timeinfo timeinfo(time);
		/*char buffer[65536] = "";
		sprintf(buffer, "%s %d", func, V__tracelevel);
		string name = buffer;*/
		//string name = func;
		timesMap.insert( pair<string, Timeinfo>(name, timeinfo) );
		//LOG("    new\n");
	}
	else {
		//iter->second += time;
		iter->second.update(time);
		//LOG("    time is now %d\n", iter->second);
	}
}

void traceTimes() {
	LOG(">>> TIMING\n");
	LOG("Total time %d\n", entiretime);
	for(map<string, Timeinfo>::const_iterator iter = timesMap.begin(); iter != timesMap.end(); ++iter) {
		int time = iter->second.getTotaltime();
		int calls = iter->second.getCalls();
		float percent = (100.0f * ((float)time)) / (float)entiretime;
		LOG("    %f %d : %d : %s\n", percent, time, calls, iter->first.c_str());
	}
	LOG("<<<\n");
	timesMap.clear();
}

#endif

VisionException::VisionException(const void *source,VisionException::ExceptionType type,const char *message) {
	this->source=source;
	this->type=type;
	/*strncpy(this->message,message,VE_MAX_TEXT);
	this->message[VE_MAX_TEXT] = '\0';*/
	this->message = message;
}

void Vision::initialise(const char *application_name) {
	if( v_initialised ) {
		Vision::setError(new VisionException(NULL, VisionException::V_ALREADY_INITIALISED, "Vision is already initialised"));
	}
	::application_name_str = application_name;

	lock_level = 0;
	game_time_ms = 0;
	game_time_last_frame_ms = 0;
/*#ifdef _WIN32
	_control87(MCW_EM, MCW_EM); // required to use OpenGL with Borland - turns off floating point error trapping!
#endif*/
/*	if( want_debug_window ) {
#ifdef _WIN32
		AllocConsole();
		FILE *dummy = NULL;
		freopen_s(&dummy, "con", "w", stdout);
		SetConsoleTitleA("DEBUG:");
#endif
	}*/
	openLogFile(application_name);
	LOG("Vision3 Initialised\n");
#ifdef _DEBUG
	LOG("Running in Debug mode\n");
#else
	LOG("Running in Release mode\n");
#endif

#ifdef _WIN32
	LOG("Platform: Windows\n");
#elif __linux
	LOG("Platform: Linux\n");
#else
	LOG("Platform: UNKNOWN\n");
#endif
	v_initialised = true;

	/*int tmpDbgFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	tmpDbgFlag |= _CRTDBG_CHECK_ALWAYS_DF;
	//tmpDbgFlag |= _CRTDBG_DELAY_FREE_MEM_DF;
	//tmpDbgFlag |= _CRTDBG_LEAK_CHECK_DF;
	_CrtSetDbgFlag(tmpDbgFlag);*/
}

void Vision::flushAll() {
	LOG("Vision::flushAll()\n");
	if( GraphicsEnvironment::existsOrHasContext() ) {
		GraphicsEnvironment::getSingleton()->checkError("Vision::flushAll enter");
	}
	for(size_t i=0;i<tags.size();i++) {
		VisionObject *vo = tags.at(i);
		/*if( vo != NULL ) {
			V_CLASS_t cl = vo->getClass();
			LOG("Deleting tag %d , class %d\n", i, cl);
			if( i == 498 && cl == V_CLASS_TEXTURE ) {
				Texture *texture = reinterpret_cast<Texture *>(vo);
				LOG("Texture with id %d\n", texture->textureID);
			}
		}*/
		//if( vo != NULL && !vo->owned ) {
		//if( vo != NULL && !vo->owned && vo->getClass() != V_CLASS_GRAPHICSENV ) {
		if( vo != NULL && vo->getClass() != V_CLASS_GRAPHICSENV ) {
			//ASSERT( _CrtCheckMemory() );
			delete vo;
			//tags.getData()[i] = NULL;
			//tags[i] = NULL;
		}
		//LOG("    done\n");
	}
	// we leave the GraphicsEnvironment until last, as this deletes the GL context, which should be done after freeing textures etc
	if( GraphicsEnvironment::exists() ) {
		LOG("free GraphicsEnvironment\n");
		if( GraphicsEnvironment::existsOrHasContext() ) {
			GraphicsEnvironment::getSingleton()->checkError("Vision::flushAll about to flush singleton");
		}
		delete GraphicsEnvironment::getSingleton();
	}
	T_ASSERT(tags.size() == 0);
}

void Vision::flush(int deleteLevel) {
	LOG("Vision::flush(%d)\n", deleteLevel);
	if( GraphicsEnvironment::existsOrHasContext() ) {
		GraphicsEnvironment::getSingleton()->checkError("Vision::flush enter");
	}
	for(size_t i=0;i<tags.size();i++) {
		VisionObject *vo = tags.at(i);
		//if( vo != NULL && !vo->owned && vo->deleteLevel >= deleteLevel ) {
		//if( vo != NULL && !vo->owned && vo->getClass() != V_CLASS_GRAPHICSENV && vo->deleteLevel >= deleteLevel ) {
		if( vo != NULL && vo->getClass() != V_CLASS_GRAPHICSENV && vo->deleteLevel >= deleteLevel ) {
			delete vo;
			//tags.getData()[i] = NULL;
			//tags[i] = NULL;
		}
	}
	// we leave the GraphicsEnvironment until last, as this deletes the GL context, which should be done after freeing textures etc
	if( GraphicsEnvironment::exists() && GraphicsEnvironment::getSingleton()->deleteLevel >= deleteLevel ) {
		LOG("free GraphicsEnvironment\n");
		if( GraphicsEnvironment::existsOrHasContext() ) {
			GraphicsEnvironment::getSingleton()->checkError("Vision::flush about to flush singleton");
		}
		delete GraphicsEnvironment::getSingleton();
	}
}

void Vision::flushSinceCreationId(size_t creation_id) {
	LOG("Vision::flushSinceCreationId(%d)\n", creation_id);
	if( GraphicsEnvironment::existsOrHasContext() ) {
		GraphicsEnvironment::getSingleton()->checkError("Vision::flush enter");
	}
	for(size_t i=0;i<tags.size();i++) {
		VisionObject *vo = tags.at(i);
		//if( vo != NULL && !vo->owned && vo->getClass() != V_CLASS_GRAPHICSENV && vo->creation_id >= creation_id ) {
		if( vo != NULL && vo->getClass() != V_CLASS_GRAPHICSENV && vo->creation_id >= creation_id ) {
			delete vo;
		}
	}
	// we leave the GraphicsEnvironment until last, as this deletes the GL context, which should be done after freeing textures etc
	if( GraphicsEnvironment::exists() && GraphicsEnvironment::getSingleton()->creation_id >= creation_id ) {
		LOG("free GraphicsEnvironment\n");
		if( GraphicsEnvironment::existsOrHasContext() ) {
			GraphicsEnvironment::getSingleton()->checkError("Vision::flush about to flush singleton");
		}
		delete GraphicsEnvironment::getSingleton();
	}
}

/*void Vision::doKills() {
for(int i=0;i<tags.size();i++) {
VisionObject *vo = static_cast<VisionObject *>(tags.getData()[i]);
if(vo != NULL) {
if( vo->getClass() == V_CLASS_ENTITY ) {
// do nothing - these are handled separately by Entity::doKills() for now
}
else if(vo->kill_later) {
delete vo;
}
}
}
}*/

void Vision::lock() {
	//locked = true;
	lock_level++;
	// n.b., don't log higher levels, for performance
	/*if( lock_level == 1 ) {
		LOG("Vision::lock() : lock level is now %d\n", lock_level);
	}*/
	//LOG("Vision::lock() : lock level is now %d\n", lock_level);
}

void Vision::unlock(bool reprepare) {
	/* The reprepare flag is used for internal locking - if false, it means that we don't prepare objects. The
	 * idea is that we trust ourselves to know which objects need unlocking, and manually prepare() them. This
	 * option replaces the "reprepare" flags that used to be handed into various operations to disable the
	 * prepare() call - it's simpler to only have a reprepare option for only this function.
	 */
	// n.b., don't log higher levels, for performance
	/*if( lock_level <= 1 ) {
		LOG("Vision::unlock(%d) : lock level was %d\n", reprepare, lock_level);
	}*/
	//LOG("Vision::unlock(%d) : lock level was %d\n", reprepare, lock_level);
	lock_level--;
	if( lock_level < 0 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_NOT_LOCKED, "Attempted to unlock when not locked"));
	}
	else if( lock_level == 0 && reprepare ) {
		GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
		int time_s = genv->getRealTimeMS();
		for(size_t i=0;i<tags.size();i++) {
			VisionObject *vo = tags.at(i);
			if(vo != NULL) {
				int time_s2 = genv->getRealTimeMS();
				vo->prepare();
			}
		}
		int total_time = genv->getRealTimeMS() - time_s;
#ifdef _DEBUG
		if( total_time > 10 ) {
			V_log("Total Prepare Time = %d\n", total_time);
		}
#endif
	}
}

bool Vision::isLocked() {
	return lock_level > 0;
}

void closeThreads();

void Vision::cleanup() {
	LOG("Vision::cleanup()\n");
	if( !v_initialised ) {
		Vision::setError(new VisionException(NULL, VisionException::V_NOT_INITIALISED, "Vision is not initialised"));
	}

	flushAll();

	closeThreads();
	lock_level = 0;
	/*#ifdef _DEBUG
	dumpAllocs();
	#endif*/
#ifdef _DEBUG
	traceTimes();
#endif

	v_initialised = false;
#ifdef _DEBUG
	ASSERT( _CrtCheckMemory() != 0 );
#endif
}

void Vision::addTag(VisionObject *ptr) {
	V_TAG_t tag = tags.size() + 1;
	// set tag
	ptr->tag = tag;
	// set creation_id
	ptr->creation_id = next_creation_id;
	next_creation_id++;
	// insert into tags list
	tags.push_back(ptr);
	//LOG("add tag %d, size now %d\n", tag, tags.size());
}

VisionObject *Vision::ptrFromTag(V_TAG_t tag) {
	VisionObject *ptr = NULL;
	if( tag <= 0 || tag > (int)tags.size() ) {
		// error
		ptr = NULL;
	}
	else {
		ptr = tags.at(tag-1);
	}
	return ptr;
}

size_t Vision::getNTags() {
	return tags.size();
}

size_t Vision::getNTagsInUse() {
	size_t n = 0;
	for(vector<VisionObject *>::const_iterator iter = tags.begin(); iter != tags.end(); ++iter) {
		const VisionObject *obj = *iter;
		if( obj != NULL ) {
			n++;
		}
	}
	return n;
}

void Vision::removeTag(V_TAG_t tag) {
	tags[tag-1] = NULL;
	//LOG("remove tag %d\n", tag);
	//LOG("ntags = %d\n", tags.size());
	if( tag == (int)tags.size() ) {
		do {
			tag--;
		} while( tag > 0 && tags[tag - 1] == NULL );
#ifdef _DEBUG
		LOG("    shrinking tag space from %d to size %d\n",tags.size(),tag);
#endif
		tags.resize( tag );
	}
}

void Vision::setError(VisionException *ve) {
	LOG("EXCEPTION FOUND\n");
	if( ve == NULL )
		LOG("    NULL Exception!!!\n");
	else {
		LOG("    type: %d\n",ve->type);
		LOG("    source: %d\n",ve->source);
		LOG("    message: %s\n",ve->message.c_str());
	}
	throw ve;
}

void Vision::update(int time) {
	// time received in ticks
	const int max_time_c = 1000;
	if(time > max_time_c)
		time = max_time_c;
	game_time_last_frame_ms = time;
	game_time_ms += game_time_last_frame_ms;

	GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
	World *world = genv->getGraphics3D()->getWorld();
	if( world != NULL ) {
		world->update(game_time_last_frame_ms);
		// we handle safe deletion of SceneGraphNodes here rather than by safedeletequeue, to cope with the possibility of both a parent and a child (or some node further down the hierarchy) both being deleted
		if( any_kills )
		{
			world->getRootNode()->doKills();
			any_kills = false;
		}
	}

	for(VSafedeleteIter iter = safedeletequeue.begin();iter != safedeletequeue.end();++iter) {
		VisionObject *obj = *iter;
		delete obj;
	}
	safedeletequeue.clear();
}

void Vision::check() {
	GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
	int time_s = genv->getRealTimeMS();
	LOG("Vision::check()\n");
	if( !v_initialised ) {
		Vision::setError(new VisionException(NULL, VisionException::V_NOT_INITIALISED, "Vision is not initialised"));
	}

	size_t n_tags = Vision::getNTags();
	LOG("Size of tag space is %d\n",n_tags);
	size_t n_actual_tags = 0;
	for(size_t i=1;i<=n_tags;i++) {
		VisionObject *vo = Vision::ptrFromTag((V_TAG_t)i);
		if( vo != NULL ) {
			n_actual_tags++;
			//LOG("Checking tag %d (Ptr %d, Class %d)\n",i,vo,vo->getClass());
			//GraphicsEnvironment::checkGLError("1");
			if( !vo->check() ) {
				LOG("FAILED to check!\n");
				LOG("    tag %d (Ptr %d, Class %d)\n",i,vo,vo->getClass());
				Vision::setError(new VisionException(vo, VisionException::V_INVALID_VISIONOBJECT, "VisionObject failed to check"));
			}
			//GraphicsEnvironment::checkGLError("2");
		}
	}
	LOG("Found %d actual tags\n",n_actual_tags);
	LOG("Time taken: %d\n", genv->getRealTimeMS() - time_s);
}

void Vision::setPersistenceAll(int level) {
	LOG("Vision::setPersistenceAll(%d)\n", level);
	size_t n_tags = Vision::getNTags();
	for(size_t i=1;i<=n_tags;i++) {
		VisionObject *vo = Vision::ptrFromTag((V_TAG_t)i);
		if( vo != NULL ) {
			vo->deleteLevel = level;
		}
	}
}

void Vision::debugMemUsage() {
	GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
	int time_s = genv->getRealTimeMS();
	LOG("Vision::debug_mem_usage()\n");
	size_t n_tags = Vision::getNTags();
	size_t total_size = 0;
	for(size_t i=1;i<=n_tags;i++) {
		VisionObject *vo = Vision::ptrFromTag((V_TAG_t)i);
		if( vo != NULL ) {
			size_t size = vo->memUsage();
			total_size += size;
			//LOG("TAG %d : Ptr %d , Class %d , Size %d\n",i,vo,vo->getClass(),size);
		}
	}
	LOG("TOTAL SIZE = %d\n", total_size);
	LOG("Time taken: %d\n", genv->getRealTimeMS() - time_s);
}

#ifdef _WIN32
// SMP stuff - Windows only for now
#include <process.h>

//const bool multithreading_enabled = false;
//const bool multithreading_enabled = true;
bool threads_initialised = false;
bool threads_running = false;

const int max_threads_c = 4;
HANDLE threads[max_threads_c];
HANDLE thread_main[max_threads_c];
HANDLE thread_sub[max_threads_c];

struct ThreadData {
	int index;
	VI_ThreadFunction *function;
	void *data;
	//int stage;
};

ThreadData threadData[max_threads_c];

unsigned __stdcall thread_func(void *ptr) {
	ThreadData *data = static_cast<ThreadData *>(ptr);
	//data->stage = 1;
	//LOG("new thread initialised...\n");
	//LOG("    thread index %d\n", data->index);

	WaitForSingleObject(thread_sub[data->index], INFINITE);
	//for(;;) {
	while( data->function != NULL ) {
		//data->stage = 2;
		//if( data->function != NULL )
		{
			//data->stage = 3;
			//LOG("calling thread function %d\n", data->index);
			//printf("calling thread function %d\n", data->index);
			(data->function)(data->data);
			//LOG("    thread function %d complete\n", data->index);
			//printf("    thread function %d complete\n", data->index);
		}
		//data->stage = 4;
		data->function = NULL;
		//data->stage = 5;
		data->data = NULL;
		//data->stage = 6;
		ResetEvent(thread_sub[data->index]);
		//data->stage = 7;
		SetEvent(thread_main[data->index]);
		//data->stage = 8;
		//printf("    thread function %d waiting\n", data->index);
		WaitForSingleObject(thread_sub[data->index], INFINITE);
		//printf("    thread function %d woken up\n", data->index);
		//data->stage = 9;
	}

	//LOG("thread %d finishing...\n", data->index);
	return 1;
}
#endif

void createThreads(int n_threads, VI_ThreadFunction **function, void **data) {
	V__ENTER;
	//printf(">>> creating %d threads\n", n_threads);
#ifdef _WIN32
	bool multithreading_enabled = GraphicsEnvironment::getSingleton()->getVisionPrefs()->smp;
	if( multithreading_enabled && !threads_initialised ) {
		LOG("initialising threads...\n");
		bool ok = true;
		for(int i=0;i<max_threads_c;i++) {
			threads[i] = NULL;
			thread_main[i] = NULL;
			thread_sub[i] = NULL;
		}
		for(int i=0;i<max_threads_c && ok;i++) {
			//thread_main[i] = CreateEvent(NULL, false, false, "Vision3 Main-Thread Event");
			//thread_sub[i] = CreateEvent(NULL, false, false, "Vision3 Sub-Thread Event");
			thread_main[i] = CreateEvent(NULL, true, false, NULL);
			thread_sub[i] = CreateEvent(NULL, true, false, NULL);
			if( thread_main[i] == NULL ) {
				LOG("failed to create main thread event!!!\n");
				ok = false;
			}
			if( thread_sub[i] == NULL ) {
				LOG("failed to create sub thread event!!!\n");
				ok = false;
			}
		}
		if( ok ) {
			LOG("done\n");
		}
		threads_initialised = ok;
	}

	if( multithreading_enabled && threads_initialised && !threads_running ) {
		// multithreaded
		//LOG("set up threads...\n");
		threads_running = true;
		for(int i=0;i<n_threads;i++) {
			ThreadData *thr_data = &threadData[i];
			thr_data->function = function[i];
			//LOG("    %d : %d , %d\n", i, &(threadData[i].function), threadData[i].function);
			thr_data->data = data[i];
			//thr_data->stage = 0;
			if( threads[i] == NULL ) {
				LOG("create new thread %d\n", i);
				thr_data->index = i;
				threads[i] = (HANDLE)_beginthreadex(NULL, 0, thread_func, thr_data, 0, NULL);
				if( threads[i] == NULL ) {
					LOG("failed to create thread %d\n", i);
				}
				else {
					LOG("done\n", i);
				}
			}
			ResetEvent(thread_main[i]);
			SetEvent(thread_sub[i]);
		}
		//LOG("waiting...\n");
		WaitForMultipleObjects(n_threads, thread_main, TRUE, INFINITE);
		for(int i=0;i<n_threads;i++) {
			//int stage = threadData[i].stage;
			VI_ThreadFunction *func = threadData[i].function;
			if( func != NULL ) {
				LOG(">>> thread %d still running!!!\n", i);
				//LOG("    stage %d\n", stage);
				LOG("    %d , %d\n", &(threadData[i].function), func);
			}
		}
		//LOG("    done!\n");
		threads_running = false;
	}
	else {
		// single threaded
		for(int i=0;i<n_threads;i++) {
			(function[i])(data[i]);
		}
	}

#else
	// non-Windows platforms don't support SMP yet
	// single threaded
	for(int i=0;i<n_threads;i++) {
		(function[i])(data[i]);
	}
#endif
	//printf("<<< done threads\n");
	V__EXIT;
}

void closeThreads() {
#ifdef _WIN32
	if( threads_initialised ) {
		LOG("closing threads...\n");
		for(int i=0;i<max_threads_c;i++) {
			if( threads[i] != NULL ) {
				// signal thread to end
				LOG("signal thread %d to end\n", i);
				SetEvent(thread_sub[i]);
			}
		}
		for(int i=0;i<max_threads_c;i++) {
			if( threads[i] != NULL ) {
				LOG("wait for thread %d to end\n", i);
				WaitForSingleObject(threads[i], INFINITE);
				LOG("    thread ended\n");
				CloseHandle(threads[i]);
				threads[i] = NULL;
			}
		}
		LOG("closing events\n");
		for(int i=0;i<max_threads_c;i++) {
			if( thread_main[i] != NULL ) {
				CloseHandle(thread_main[i]);
				thread_main[i] = NULL;
			}
			if( thread_sub[i] != NULL ) {
				CloseHandle(thread_sub[i]);
				thread_sub[i] = NULL;
			}
		}
		threads_initialised = false;
		LOG("threads closed\n");
	}
#endif
}
