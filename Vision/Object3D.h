#pragma once

#include <vector>
using std::vector;
#include <set>
using std::set;
#include <string>
using std::string;

#include "Resource.h"
#include "VisionIface.h"
#include "VisionShared.h"
#include "Misc.h"

class Renderer;
class GraphicsEnvironment;
class ShadowPlane;
class RenderData;
class SceneGraphNode;
class RenderQueue;
class Texture;
class Entity;
//class Shader;
class ShaderEffect;
class VertexArray;
class Particle;

/** Interface that describes to Shader callbacks the individual item being rendered - either an
  * ObjectData subclass, or one of its sub-meshes (a RenderData).
  */
class RenderableInfo {
public:
	virtual Texture *getFirstTexture() const=0;
	virtual const float *getMaterialSpecular() const=0;
};

class Vertex3D : public Vector3D/*, public virtual VI_Vertex3D*/ {
public:
	Vector3D normal;
	Vertex3D() : Vector3D() {
	}
	Vertex3D(float x,float y,float z) : Vector3D(x,y,z) {
	}
};

//class Polygon3D /*: public VisionObject*/ {
class Polygon3D : public VI_Face {
	friend class Object3D;

	string name;
protected:
	void init();
	LightingStyle shading;

	vector<size_t> vlist;
	vector<float> tx, ty;
	vector<float> secondary_tx, secondary_ty;

	Texture *texture;
	Texture *secondary_texture; // nb - only supported in shaders atm

public:

	Polygon3D();
	Polygon3D(size_t v0,size_t v1,size_t v2);
	Polygon3D(size_t v0,size_t v1,size_t v2,size_t v3);
	Polygon3D(const size_t *vlist, size_t n_vertices);
	//Polygon3D(const Polygon3D * poly);
	//virtual V_CLASS_t getClass() const { return V_CLASS_FACE; };
	LightingStyle getShading() const {
		return shading;
	}
	const Texture *getTexture() const {
		return texture;
	}
	void setNVertices(size_t n_vertices);
	void setVertex(size_t index, size_t vertex) {
		vlist[index] = vertex;
	}
	/* setTextureWrap sets textures coordinate. text_uvs in an array to pointers of type TextureUV, which
	*  represent texture coordinates *for the entire object*. I.e., the i-th element is the texture coordinate
	* on the i-th vertex in the body (and *not* in general the i-th polygon).
	*/
	void setTextureWrap(VI_Texture *texture, const TextureUV **text_uvs);
	/*void setTexture(Texture *texture,const Vertex3D *vertices) {
		setTexture(texture,vertices,false);
	}
	void setTexture(Texture *texture,const Vertex3D *vertices,bool mirror);
	void setTexture(Texture *texture,const Vertex3D *vertices,bool mirror,int skip);*/
	//void setTexture(Texture *texture,const Vertex3D *vertices,bool mirror,int skip,float scale_u,float scale_v);
	void setTextureCoords(size_t index, float u, float v) {
		tx[index] = u;
		ty[index] = v;
	}
	void setSecondaryTexture(Texture *secondary_texture);

	void invert();

	virtual void setName(const char *name) {
		this->name = name;
	}
	virtual const char *getName() const {
		return this->name.c_str();
	}
	/*int getNameLength() const {
		return this->name.length();
	}*/

	virtual void setTexture(VI_Texture *texture,const Vertex3D *vertices,bool mirror,int skip,bool have_size,float size_u,float size_v);
	virtual void setTexture(VI_Texture *texture,const Vertex3D *vertices,const Vector3D *origin, const Vector3D *u_dir, const Vector3D *v_dir);

	// interface
	virtual size_t getNVertices() const {
		return vlist.size();
	}
	virtual size_t getVertexIndex(size_t i) const {
		return vlist.at(i);
	}

	virtual void setTexture(VI_Texture *texture);
};

class ObjectDataShaders {
public:
	/*const Shader *vertex_shader_ambient;
	const Shader *pixel_shader_ambient;
	const Shader *vertex_shader_directional;
	const Shader *pixel_shader_directional;
	const Shader *vertex_shader_point;
	const Shader *pixel_shader_point;*/
	const ShaderEffect *shader_ambient;
	const ShaderEffect *shader_directional;
	const ShaderEffect *shader_point;

	ObjectDataShaders() {
		clear();
	}
	/*ObjectDataShaders(const Shader *vertex_shader_ambient, const Shader *pixel_shader_ambient,
		const Shader *vertex_shader_directional, const Shader *pixel_shader_directional,
		const Shader *vertex_shader_point, const Shader *pixel_shader_point) :
	vertex_shader_ambient(vertex_shader_ambient),
	pixel_shader_ambient(pixel_shader_ambient),
	vertex_shader_directional(vertex_shader_directional),
	pixel_shader_directional(pixel_shader_directional),
	vertex_shader_point(vertex_shader_point),
	pixel_shader_point(pixel_shader_point)
	{
	}*/
	ObjectDataShaders(const ShaderEffect *shader_ambient, const ShaderEffect *shader_directional, const ShaderEffect *shader_point) :
	shader_ambient(shader_ambient), shader_directional(shader_directional), shader_point(shader_point)
	{
	}
	void clear() {
		/*vertex_shader_ambient = NULL;
		pixel_shader_ambient = NULL;
		vertex_shader_directional = NULL;
		pixel_shader_directional = NULL;
		vertex_shader_point = NULL;
		pixel_shader_point = NULL;*/
		shader_ambient = NULL;
		shader_directional = NULL;
		shader_point = NULL;
	}
};

class Frameinfo {
public:
	int frame_starts[V_ANIMSTATE_MD2_NSTATES];
	int frame_ends[V_ANIMSTATE_MD2_NSTATES];

	Frameinfo() {
		clear();
	}
	void clear() {
		for(int i=0;i<V_ANIMSTATE_MD2_NSTATES;i++) {
			frame_starts[i] = 0;
			frame_ends[i] = 0;
		}
	}
};

class ObjectData : public VisionObject, public virtual VI_Object {
	friend class Object3D; // so we can access protected members of subclasses other than Object3D, from within Object3D

public:
	enum RenderPhase {
		RENDER_NONOPAQUE,
		RENDER_OPAQUE,
		RENDER_STENCIL
	};

protected:
	bool calc_bounds;
	bool calc_renderdata;
	bool calc_shadows;

	Sphere boundingSphere;
	bool has_boundingBox;
	Box boundingBox;

	ShadowPlane *shadowplane;

	vector<RenderData *> renderDatas;

	vector<RenderData *> shadow_renderDatas;
	size_t getNShadowRenderData() const {
		return this->shadow_renderDatas.size();
	}
	bool shadow_caster;

	ObjectDataShaders shaders;
	int n_frames;
	Frameinfo frameinfo;

	virtual void prepareRenderDatas();
public:
	ObjectData();
	virtual ~ObjectData();

	// inherited from VisionObject
	virtual V_CLASS_t getClass() const { return V_CLASS_OBJECTDATA; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_OBJECTDATA );
	}
	virtual size_t memUsage();

	// simple set/get functions
	virtual bool hasLight() { return false; };
	virtual bool renderWithLighting() { return true; }
	//virtual void setBaseColor(Color col,bool reprepare) {};
	virtual void setSpecular(Color *specular)=0;
	virtual void setSecondaryTexture(Texture *secondary_texture) {};
	//virtual void setShadowPlane(Graphics3D *g3,ShadowPlane *shadowplane);
	void setShadowPlane(Renderer *renderer, ShadowPlane *shadowplane, const ObjectDataShaders *shaders, int texture_size);
	virtual ShadowPlane *getShadowPlane() const {
		return this->shadowplane;
	};
	void setShaders(const ObjectDataShaders *shaders) {
		this->shaders = *shaders;
	}
	virtual void setBounds() {
		this->calc_bounds = true;
	}
	size_t getNRenderData() const {
		return renderDatas.size();
	}
	const RenderData *getRenderData(size_t i) const;
	RenderData *getRenderData(size_t i);
	/*const Shader *getVertexShaderAmbient() const {
		return this->shaders.vertex_shader_ambient;
	}
	const Shader *getPixelShaderAmbient() const {
		return this->shaders.pixel_shader_ambient;
	}
	const Shader *getVertexShaderDirectional() const {
		return this->shaders.vertex_shader_directional;
	}
	const Shader *getPixelShaderDirectional() const {
		return this->shaders.pixel_shader_directional;
	}
	const Shader *getVertexShaderPoint() const {
		return this->shaders.vertex_shader_point;
	}
	const Shader *getPixelShaderPoint() const {
		return this->shaders.pixel_shader_point;
	}*/
	const ShaderEffect *getShaderAmbient() const {
		return this->shaders.shader_ambient;
	}
	const ShaderEffect *getShaderDirectional() const {
		return this->shaders.shader_directional;
	}
	const ShaderEffect *getShaderPoint() const {
		return this->shaders.shader_point;
	}

	// rendering
	virtual void render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *node, bool lighting_pass, bool use_shadowmesh, RenderData *renderData);
	virtual void renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview);
	void castShadow(Renderer *renderer, GraphicsEnvironment *genv, int frame, int frame2, float frac, const Vector3D &lightpos, bool infinite, bool shader);

	// modification
	virtual void prepare();
	//virtual void initEntity(Entity *entity) {}
	virtual void initSceneGraphNode(SceneGraphNode *node) {}
	virtual void optimiseVertices();
	//virtual void scale(float sx,float sy,float sz,bool reprepare)=0;
	//virtual void translate(float sx,float sy,float sz,bool reprepare)=0;
	//virtual void rotate(Rotation3D *rot,bool reprepare)=0;
	//virtual void rotateEuler(float x,float y,float z,bool reprepare);
	/*virtual void transform4d(Matrix4d * m,bool reprepare) {};
	virtual void transform4d(Matrix4d *m) {
		transform4d(m, true);
	}*/

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/
	virtual void setName(const char *name) {
		VisionObject::setName(name);
	}
	virtual bool check() const {
		return VisionObject::check();
	}

	virtual Sphere getBoundingSphere() {
		if( !calc_bounds ) {
			//Vision::setError(new VisionException(this, VisionException::V_OBJ_NOT_PREPARED_BOUNDS, "getBoundingSphere: bounds not available, didn't prepare this object?"));
			setBounds();
		}
		return boundingSphere;
	}
	virtual bool getBoundingAABB(Box *box) {
		if( !calc_bounds ) {
			//Vision::setError(new VisionException(this, VisionException::V_OBJ_NOT_PREPARED_BOUNDS, "getBoundingAABB: bounds not available, didn't prepare this object?"));
			setBounds();
		}
		if( has_boundingBox ) {
			*box = boundingBox;
		}
		return has_boundingBox;
	}
	virtual void getExtent(float *low,float *high,const Vector3D &n,bool firstFrameOnly) const {
		*low = 0;
		*high = 0;
	}
	virtual void getDirectionRadiusSq(float *radius_sq,Vector3D *n,bool firstFrameOnly) const {
		*radius_sq = 0;
	}
	//virtual Vector3D findCentre() const=0; // now declared by the interface, but defined in subclasses only

	virtual bool isShadowCaster() const {
		return this->shadow_caster;
	}

	/*virtual void scale(float sx,float sy,float sz)=0;
	virtual void translate(float sx,float sy,float sz)=0;
	virtual void rotate(Rotation3D *rot)=0;
	virtual void transform4d(Matrix4d *m)=0;*/
	virtual void rotateEuler(float x,float y,float z);
	/*virtual void scale(float sx,float sy,float sz) {
		this->scale(sx, sy, sz, true);
	}
	virtual void translate(float sx,float sy,float sz) {
		this->translate(sx, sy, sz, true);
	}
	virtual void rotate(Rotation3D *rot) {
		rotate(rot, true);
	}
	virtual void rotateEuler(float x,float y,float z) {
		this->rotateEuler(x, y, z, true);
	}*/
	virtual void setHardwareAnimation(const VI_GraphicsEnvironment *genv, bool enabled) {}
	virtual void setSpeculari(unsigned char r, unsigned char g, unsigned char b);
	virtual void setShadowPlane(VI_GraphicsEnvironment *genv, VI_ShadowPlane *shadowPlane, unsigned char alpha);
	virtual void setShadowCaster(bool shadow_caster);
	/*virtual void setShadowCaster(bool shadow_caster) {
		this->setShadowCaster(shadow_caster, true);
	}*/

	virtual int getNFrames() const {
		return this->n_frames;
	}
	virtual void setAnimFrames(V_ANIMSTATE_t state,int start,int end) {
		frameinfo.frame_starts[state] = start;
		frameinfo.frame_ends[state] = end;
	}
	virtual void getFrames(V_ANIMSTATE_t state,int *start,int *end) const;
};

class Mesh : public ObjectData, public virtual VI_Mesh {
	friend class TerrainEngine;
protected:
	//set<Texture *> texture_list;

public:
	Mesh();
	Mesh(int n_frames);
	virtual ~Mesh();

	virtual V_CLASS_t getClass() const { return V_CLASS_MESH; };
	/*virtual bool isSubClass(V_CLASS_t v_class) {
	return ( this->getClass() == v_class || ObjectData::isSubClass(v_class) );
	}*/
	//virtual size_t memUsage();
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_MESH || ObjectData::isSubClass(v_class) );
	}

	//virtual void setTexture(Texture *texture,bool reprepare); // commented out as currently unused - we probably should allow this function though?
	virtual void setSecondaryTexture(Texture *secondary_texture);
	virtual void setSpecular(Color *specular);
	virtual void setBounds();
	virtual Vector3D findCentre() const;
	virtual void getExtent(float *low,float *high,const Vector3D &n,bool firstFrameOnly) const;
	virtual void getDirectionRadiusSq(float *radius_sq,Vector3D *n,bool firstFrameOnly) const;
	virtual void scale(float sx,float sy,float sz);
	virtual void translate(float sx,float sy,float sz);
	virtual void rotate(const Rotation3D &rot);
	virtual void transform4d(const Matrix4d &m);
	virtual void deform(VI_Deform *deformFunc, void *deformData);
	virtual void addRenderData(RenderData *rd) {
		this->renderDatas.push_back(rd);
	}

	// interface
	//virtual void addTexture(VI_Texture *texture);
	virtual void setAlphai(unsigned char alpha);
	virtual void setBaseColori(unsigned char r, unsigned char g, unsigned char b);
	virtual void setBlendType(V_BLENDTYPE_t blend_type);
	virtual void setHardwareAnimation(const VI_GraphicsEnvironment *genv, bool enabled);

	virtual VI_Mesh *clone();
	virtual void combineObject(VI_Mesh *mesh);
};

class Object3DAttribs {
public:
	bool solid;
	unsigned char alpha;
	Color specular;
	//Texture *bump_texture;
	bool need_tangents;
	bool uses_hardware_animation;
	V_BLENDTYPE_t blend_type;

	Object3DAttribs() {
		this->solid = true;
		this->alpha = 255;
		this->specular.seti(0, 0, 0, 0);
		//this->bump_texture = NULL;
		this->need_tangents = false;
		this->uses_hardware_animation = false;
		this->blend_type = V_BLENDTYPE_FADE_SRC;
	}
};

class Object3D : public ObjectData, public virtual VI_VObject {
protected:
	bool calc_vn,calc_pn;

	vector<Vertex3D> *frames; // array of size n_frames
	vector<Vector3D> *poly_normals_frame; // array of size n_frames
	vector<Color> colors;
	vector<Polygon3D> polys;

	Object3DAttribs attribs;

	virtual void init(int n_frames);
	void initRenderData(RenderData *renderData, Texture *this_texture) const;
	virtual void prepareRenderDatas();
public:

	//Face **faces;

	Object3D();
	Object3D(int n_frames);
	virtual ~Object3D();
	virtual V_CLASS_t getClass() const { return V_CLASS_VOBJECT; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_VOBJECT || ObjectData::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = ObjectData::memUsage();
		for(int i=0;i<n_frames;i++) {
			size += sizeof(frames[i]);
			size += sizeof(poly_normals_frame[i]);
		}
		return size;
	}

	virtual void set_n_vertices(size_t n_vertices);
	virtual void compact();
	virtual size_t addVertex(int frame, Vertex3D vertex);
	//virtual int addVertex(Vertex3D *vertex);
	virtual Vertex3D *getVertices(int frame) { // needs to be public, due to confusion with overloaded const function!
		return &*frames[frame].begin();
	}
	virtual const Vertex3D *getVertices(int frame) const {
		return &*frames[frame].begin();
	}
	virtual Vertex3D getVertex(int frame, size_t index) const {
		return frames[frame].at(index);
	}
	virtual void setVertex(int frame, int index, const Vector3D *pos) {
		frames[frame][index].x = pos->x;
		frames[frame][index].y = pos->y;
		frames[frame][index].z = pos->z;
	}
	virtual VI_Face *addPolygon(Polygon3D polygon);
	//virtual void addPolygon(Polygon3D *polygon);
	//virtual void addPolygon(Polygon3D *polygon, const char *name);
	virtual const Color *getColors() const {
		return &*colors.begin();
	}
	virtual const Vector3D *getPolyNormals(int frame) const {
		return &*poly_normals_frame[frame].begin();
	}

	virtual void calculatePolygonNormals();
	virtual void calculateVertexNormals();
	virtual void setBounds();
	virtual Vector3D findCentre() const;
	virtual void getExtent(float *low,float *high,const Vector3D &n,bool firstFrameOnly) const;
	virtual void getDirectionRadiusSq(float *radius_sq,Vector3D *n,bool firstFrameOnly) const;
	/*virtual void setTextureWrap(Texture *texture,TextureUV **text_uvs,bool reprepare);
	virtual void setTextureWrap(Texture *texture,TextureUV **text_uvs) {
		setTextureWrap(texture, text_uvs, true);
	}*/
	virtual void setSecondaryTexture(Texture *secondary_texture);
	/*virtual void setShading(Polygon3D::LightingStyle shading,bool reprepare);
	virtual void setShading(Polygon3D::LightingStyle shading) {
		setShading(shading, true);
	}*/
	/*virtual void setBaseColor(Color col,bool reprepare);
	virtual void setBaseColor(Color col) {
		setBaseColor(col, true);
	}*/
	//virtual void setPolygonBaseColor(Polygon3D *poly,Color col);
	//virtual void setPolygonTexture(Polygon3D *poly,Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v);
	virtual void setSpecular(Color *specular) {
		this->attribs.specular = *specular;
		this->calc_renderdata = false;
		if( !Vision::isLocked() ) {
			this->prepare();
		}
	}
	//virtual void combineObject(VI_VObject *o, bool reprepare);

	virtual void prepare();
	virtual void freeData();
	virtual void scale(float sx,float sy,float sz);
	virtual void translate(float sx,float sy,float sz);
	virtual void rotate(const Rotation3D &rot);
	virtual void transform4d(const Matrix4d &m);
	virtual void deform(VI_Deform *deformFunc, void *deformData);

	// interface
	virtual bool check() const;

	virtual size_t get_n_vertices() const {
		return frames[0].size();
	}
	virtual size_t get_n_polys() const {
		return polys.size();
	}

	virtual size_t addVertex(float x, float y, float z);
	virtual size_t addVertex(int frame, float x, float y, float z);
	virtual void setVertexColori(size_t index, unsigned char r, unsigned char g, unsigned char b);

	/*virtual VI_Face *addPolygon(size_t v0, size_t v1, size_t v2);
	virtual VI_Face *addPolygon(size_t v0, size_t v1, size_t v2, size_t v3);*/
	virtual VI_Face *addPolygon(size_t v0, size_t v1, size_t v2, const char *name);
	virtual VI_Face *addPolygon(size_t v0, size_t v1, size_t v2, size_t v3, const char *name);
	virtual VI_Face *addPolygon(const size_t *vlist, size_t n_vertices, const char *name);
	virtual void reservePolygons(size_t n_polys) {
		this->polys.reserve(n_polys);
	}
	virtual void clearPolygons() {
		polys.clear();
		if( poly_normals_frame != NULL ) {
			delete [] poly_normals_frame;
			poly_normals_frame = NULL;
		}
	}

	/*virtual void scale(float sx,float sy,float sz) {
		scale(sx, sy, sz, true);
	}
	virtual void translate(float sx,float sy,float sz) {
		translate(sx, sy, sz, true);
	}
	virtual void rotate(Rotation3D *rot) {
		rotate(rot, true);
	}*/
	virtual void setAlphai(unsigned char alpha) {
		this->attribs.alpha = alpha;
		this->calc_renderdata = false;
		if( !Vision::isLocked() ) {
			//this->doDisplayLists();
			this->prepare();
		}
	}
	virtual void setBlendType(V_BLENDTYPE_t blend_type) {
		this->attribs.blend_type = blend_type;
		this->calc_renderdata = false;
		if( !Vision::isLocked() ) {
			this->prepare();
		}
	}
	virtual void setTexture(VI_Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v);
	/*virtual void setTexture(VI_Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v,bool reprepare);
	virtual void setTexture(VI_Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v) {
		setTexture(texture, mirror, skip, have_size, size_u, size_v, true);
	}*/
	/* setTextureWrap and setPolygonTextureWrap except the TextureUV array to be an array of texture coordinates
	*  for the *entire object*, not just one polygon.
	*/
	virtual void setTextureWrap(VI_Texture *texture, const TextureUV **text_uvs);
	virtual void setPolygonTextureWrap(VI_Face *face,VI_Texture *texture,const TextureUV **text_uvs);
	/* setPolygonTextureCoords on the other hand only takes an array of texture coords for the individual face.
	*/
	virtual void setPolygonTextureCoords(VI_Face *face,VI_Texture *texture,const float *u, const float *v);
	virtual void setBumpTexture(const VI_GraphicsEnvironment *genv, VI_Texture *bump_texture);
	virtual void setHardwareAnimation(const VI_GraphicsEnvironment *genv, bool enabled);

	virtual void combineObject(VI_VObject *o);
	/*virtual void combineObject(VI_VObject *o) {
		combineObject(o, true);
	}*/
	virtual VI_VObject *clone() const;

	virtual Vector3D getVertexPos(size_t index) const {
		// version of getVertex that returns as a Vector3D instead of Vertex3D
		return getVertex(0, index);
	}
	virtual size_t getFaces(VI_Face ***faces);
	//virtual Polygon3D *getPolygon(int index) {
	virtual const VI_Face *getFace(size_t index) const {
		return &polys[index];
	}
	virtual VI_Face *getFace(size_t index) {
		return &polys[index];
	}
	virtual VI_Face *findFace(const char *name);
	virtual void setShading(VI_Face::LightingStyle shading);
	virtual void setBaseColori(unsigned char r, unsigned char g, unsigned char b);
	virtual void setPolygonBaseColori(VI_Face *face, unsigned char r, unsigned char g, unsigned char b);
	virtual void setPolygonTexture(VI_Face *face,VI_Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v);
	virtual void setPolygonTexture(VI_Face *face,VI_Texture *texture,const Vector3D *origin, const Vector3D *u_dir, const Vector3D *v_dir);
	virtual void invertPolygon(VI_Face *face);
	virtual void setSolid(bool solid);
	virtual bool isSolid() const;

	virtual Mesh *createMesh();
	//virtual void subdivide(VI_Face *face);
	virtual void subdivide();
};

class Light : public ObjectData, public virtual VI_Light {
protected:
	Color ambient;
	Color diffuse;
	Color specular;
	float attenuation[3]; // constant, linear, quadratic

	//virtual void doDisplayLists();
public:
	Light() {
		/*ambient.seti(255,255,255);
		diffuse.seti(255,255,255);
		specular.seti(255,255,255);*/ // already set by Color constructor
		attenuation[0] = 1.0;
		attenuation[1] = 0.0;
		attenuation[2] = 0.0;
	}
	Light(float R,float G,float B,float dR,float dG,float dB) {
		ambient.setf(R,G,B);
		diffuse.setf(dR,dG,dB);
		specular.setf(dR,dG,dB); // specular defaults to being same as diffuse
		attenuation[0] = 1.0;
		attenuation[1] = 0.0;
		attenuation[2] = 0.0;
	}

	virtual V_CLASS_t getClass() const { return V_CLASS_LIGHT; };
	/*virtual bool isSubClass(V_CLASS_t v_class) {
	return ( this->getClass() == v_class || ObjectData::isSubClass(v_class) );
	}*/
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_LIGHT || ObjectData::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = ObjectData::memUsage();
		return size;
	}

	virtual void setLightAmbient(Color *col);
	virtual void setLightDiffuse(Color *col);
	virtual void setLightSpecular(Color *col);
	virtual void setLightAttenuation(float attenuation[3]);
	virtual Color getLightAmbient() const {
		return this->ambient;
	}
	virtual Color getLightDiffuse() const {
		return this->diffuse;
	}
	virtual Color getLightSpecular() const {
		return this->specular;
	}
	virtual float getLightAttenuation(int i) const {
		return this->attenuation[i];
	}
	virtual const float *getLightAttenuation() const {
		return this->attenuation;
	}
	virtual void setSpecular(Color *specular) {}; // this refers to an object receiving light, so is not relevant here
	virtual void scale(float sx,float sy,float sz) {};
	virtual void translate(float sx,float sy,float sz) {};
	virtual void rotate(const Rotation3D &rot) {};
	virtual void transform4d(const Matrix4d &m) {};
	virtual void deform(VI_Deform *deformFunc, void *deformData) {};
	virtual void setBounds();
	virtual void render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *node, bool lighting_pass, bool use_shadowmesh, RenderData *renderData);
	virtual bool hasLight() { return true; };
	virtual void renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview);
	virtual Vector3D findCentre() const {
		return Vector3D(0.0, 0.0, 0.0);
	}

	// interface
	virtual void setAlphai(unsigned char alpha) {};
	virtual void setBaseColori(unsigned char r, unsigned char g, unsigned char b) {}
	virtual void setBlendType(V_BLENDTYPE_t blend_type) {}
	virtual void setLightAmbienti(unsigned char r, unsigned char g, unsigned char b);
	virtual void setLightDiffusei(unsigned char r, unsigned char g, unsigned char b);
	virtual void setLightSpeculari(unsigned char r, unsigned char g, unsigned char b);
	virtual void setLightAttenuation(float quadratic,float linear,float constant) {
		float attenuation[3] = {constant, linear, quadratic};
		this->setLightAttenuation(attenuation);
	}
};

class Explosion : public Light, public RenderableInfo, public virtual VI_Explosion {
	Color scaled_ambient;
	Color scaled_diffuse;
	Texture *texture;
	float duration;
	float max_size;
	bool lit;

public:
	Explosion(Texture *texture) : Light(0,0,0,1,1,0) {
		this->texture = texture;
		this->duration = 1.0;
		this->max_size = 25.0;
		this->lit = true;
		this->scaled_ambient = this->ambient;
		this->scaled_diffuse = this->diffuse;
	}
	Explosion(Texture *texture,float R,float G,float B,float dR,float dG,float dB) : Light(R,G,B,dR,dG,dB) {
		this->texture = texture;
		this->duration = 1.0;
		this->max_size = 25.0;
		this->lit = true;
		this->scaled_ambient = this->ambient;
		this->scaled_diffuse = this->diffuse;
	}

	virtual V_CLASS_t getClass() const { return V_CLASS_EXPLOSION; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_EXPLOSION || Light::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = Light::memUsage();
		return size;
	}

	virtual bool renderWithLighting() { return false; }

	virtual Color getLightAmbient() {
		return this->scaled_ambient;
	}
	virtual Color getLightDiffuse() {
		return this->scaled_diffuse;
	}
	virtual void render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *node, bool lighting_pass, bool use_shadowmesh, RenderData *renderData);
	virtual void renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview);

	// RenderableInfo interface
	virtual Texture *getFirstTexture() const {
		return texture;
	}
	virtual const float *getMaterialSpecular() const {
		static const float material_specular[] = {0.0f, 0.0f, 0.0f, 0.0f};
		return material_specular;
	}

};

class BillBoard : public ObjectData, public RenderableInfo, public virtual VI_Billboard {
	VertexArray *vao_vertices;
	float *va_vertices;
	VertexArray *vao_normals;
	float *va_normals;
	VertexArray *vao_texcoords;
	float *va_texcoords;
	VertexArray *vao_colors;
	unsigned char *va_colors;
	VertexArray *vao_indices;
	unsigned short *va_indices;
	V_BLENDTYPE_t blend_type;

	Vector3D *offsets;
	unsigned char *cols;
	int n_billboards;
	Texture *texture;
	float width, height;
	bool blend; // if true, use blending, otherwise, use alpha test
	bool lock;
	float wave_amp;
	float wave_sp;

	void init();
public:

	BillBoard(Texture *texture, float width, float height,bool blend);
	BillBoard(Texture *texture, float width, float height,bool blend,int n_billboards);
	virtual ~BillBoard();

	virtual V_CLASS_t getClass() const { return V_CLASS_BILLBOARD; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_BILLBOARD || ObjectData::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = ObjectData::memUsage();
		return size;
	}

	virtual bool renderWithLighting() { return !blend; }
	virtual void render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *node, bool lighting_pass, bool use_shadowmesh, RenderData *renderData);
	virtual void renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview);
	virtual Vector3D findCentre() const;
	virtual void setBounds();
	void setColor(const Color *color);
	void setOneColor(int i, const Color *color);
	virtual void setSpecular(Color *specular) {};
	virtual void scale(float sx,float sy,float sz) {};
	virtual void translate(float sx,float sy,float sz) {};
	virtual void rotate(const Rotation3D &rot) {};
	virtual void transform4d(const Matrix4d &m) {};
	virtual void deform(VI_Deform *deformFunc, void *deformData) {};

	// RenderableInfo interface
	virtual Texture *getFirstTexture() const {
		return texture;
	}
	virtual const float *getMaterialSpecular() const {
		static const float material_specular[] = {0.0f, 0.0f, 0.0f, 0.0f};
		return material_specular;
	}

	// interface
	virtual void setOffset(Vector3D v, int i) {
		offsets[i] = v;
	}
	virtual void setLock(bool lock) {
		this->lock = lock;
	}
	virtual void setWave(float wave_amp, float wave_sp) {
		this->wave_amp = wave_amp;
		this->wave_sp = wave_sp;
	}
	virtual void setColori(unsigned char r, unsigned char g, unsigned char b) {
		Color col;
		col.seti(r, g, b);
		this->setColor(&col);
	}
	virtual void setOneColori(int i, unsigned char r, unsigned char g, unsigned char b) {
		Color col;
		col.seti(r, g, b);
		this->setOneColor(i, &col);
	}
	virtual void setAlphai(unsigned char alpha) {};
	virtual void setBaseColori(unsigned char r, unsigned char g, unsigned char b) {}
	virtual void setBlendType(V_BLENDTYPE_t blend_type) {
		this->blend_type = blend_type;
		// doesn't need re-prepare
	}
};

class ParticleSystem : public ObjectData,  public RenderableInfo, public virtual VI_Particlesystem {
protected:
	int accumulated_time_ms;
	Texture *texture;
	bool lock;

	VertexArray *vao_vertices;
	float *va_vertices;
	VertexArray *vao_texcoords;
	float *va_texcoords;
	VertexArray *vao_colors;
	unsigned char *va_colors;
	VertexArray *vao_indices;
	unsigned short *va_indices;

	int max_particles;

public:
	ParticleSystem(int max_particles);
	~ParticleSystem();

	virtual V_CLASS_t getClass() const { return V_CLASS_PARTICLESYSTEM; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_PARTICLESYSTEM || ObjectData::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = ObjectData::memUsage();
		//size += n_particles * sizeof(Particle);
		return size;
	}

	virtual bool renderWithLighting() { return false; }
	//virtual void initEntity(Entity *entity);
	virtual void initSceneGraphNode(SceneGraphNode *node);
	virtual void initialiseParticle(Particle *particles,int index)=0;
	virtual int getMaxParticles() const {
		return this->max_particles;
	}
	virtual Vector3D findCentre() const {
		return Vector3D(0,0,0);
	}
	/*virtual void setBoundingRadius() {
	this->boundingRadius = V_TOL_LARGE;
	this->boundingRadiusSquared = boundingRadius*boundingRadius;
	ObjectData::setBoundingRadius();
	};*/
	virtual void setBounds() {
		this->boundingSphere.setRadius( V_TOL_LARGE );
		//this->boundingAABB.available = false;
		this->has_boundingBox = false;
		ObjectData::setBounds();
	};
	virtual void setSpecular(Color *specular) {};
	virtual void scale(float sx,float sy,float sz) {};
	virtual void translate(float sx,float sy,float sz) {};
	virtual void rotate(const Rotation3D &rot) {};
	virtual void transform4d(const Matrix4d &m) {};
	virtual void deform(VI_Deform *deformFunc, void *deformData) {};
	virtual void renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview);
	virtual void render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *node, bool lighting_pass, bool use_shadowmesh, RenderData *renderData);
	virtual void updateParticles(SceneGraphNode *node,int time_ms)=0;

	// RenderableInfo interface
	virtual Texture *getFirstTexture() const {
		return texture;
	}
	virtual const float *getMaterialSpecular() const {
		static const float material_specular[] = {0.0f, 0.0f, 0.0f, 0.0f};
		return material_specular;
	}

	// interface
	virtual void setTexture(VI_Texture *texture);
	virtual void setAlphai(unsigned char alpha) {};
	virtual void setBaseColori(unsigned char r, unsigned char g, unsigned char b) {}
	virtual void setBlendType(V_BLENDTYPE_t blend_type) {}
};


class Snowstorm : public ParticleSystem {
protected:
	float width;
	float height;
	float depth;
	Color color;
	float size;
	float rate;
	Vector3D velocity;
	Vector3D variation;
public:
	Snowstorm(int max_particles,Texture *texture,float width,float height,float depth);

	virtual V_CLASS_t getClass() const { return V_CLASS_SNOWSTORM; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_SNOWSTORM || ParticleSystem::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = ParticleSystem::memUsage();
		return size;
	}
	virtual void initialiseParticle(Particle *particles,int index);

	virtual void updateParticles(SceneGraphNode *node,int time_ms);
};

class NWNParticleSystem : public ParticleSystem, public virtual VI_GeneralParticlesystem {
protected:
	Color color_st;
	Color color_nd;
	unsigned char alpha_st, alpha_nd;
	float size_st, size_nd;
	int birthRate;
	float lifeExp;
	float mass;
	float initVel;

public:
	NWNParticleSystem(int max_particles);
	NWNParticleSystem(int max_particles,Texture *texture,Color color_st,Color color_nd,unsigned char alpha_st,unsigned char alpha_nd,float size_st,float size_nd,int birthRate,float lifeExp,float mass,float initVel);

	virtual V_CLASS_t getClass() const { return V_CLASS_NWNPARTICLESYSTEM; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_NWNPARTICLESYSTEM || ParticleSystem::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = ParticleSystem::memUsage();
		return size;
	}
	virtual void initialiseParticle(Particle *particles,int index);
	virtual void updateParticles(SceneGraphNode *node,int time_ms);

	// interface
	virtual void setColoriStart(unsigned char r, unsigned char g, unsigned char b);
	virtual void setColoriEnd(unsigned char r, unsigned char g, unsigned char b);
	virtual void setAlphaiStart(unsigned char a) {
		this->alpha_st = a;
	}
	virtual void setAlphaiEnd(unsigned char a) {
		this->alpha_nd = a;
	}
	virtual void setSizeStart(float size);
	virtual void setSizeEnd(float size);
	virtual void setBirthRate(int value);
	virtual void setLife(float value);
	virtual void setMass(float value);
	virtual void setInitialVelocity(float value);
};

/*class Face : public VisionObject {
public:
	ObjectData *owner;
	int index;

	Face();
	virtual ~Face();
	virtual V_CLASS_t getClass() const { return V_CLASS_FACE; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_FACE );
	}
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}
};*/

class ShadowPlane : public VisionObject, public VI_ShadowPlane {
	Plane plane;
	bool shadow;
	bool reflection;
	bool render_to_texture; // true - use render to texture; false - use stencil
	int next_update;
	int frames_per_update;
	Texture *render_texture;

public:
	ShadowPlane(const Plane *plane, bool shadow, bool reflection, bool render_to_texture);
	~ShadowPlane();

	virtual V_CLASS_t getClass() const { return V_CLASS_SHADOWPLANE; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_SHADOWPLANE );
	}
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}

	const Plane *getPlane() const {
		return &this->plane;
	}
	bool isShadow() const {
		return this->shadow;
	}
	bool isReflection() const {
		return this->reflection;
	}
	bool isRenderToTexture() const {
		return this->render_to_texture;
	}
	const Texture *getRenderTexture() const {
		return this->render_texture;
	}
	Texture *getRenderTexture() {
		return this->render_texture;
	}
	void setRenderTexture(Texture *render_texture);
	bool needUpdate() {
		if( this->next_update == 0 ) {
			this->next_update = frames_per_update-1;
			return true;
		}
		else {
			this->next_update--;
			return false;
		}
	}

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/
};
