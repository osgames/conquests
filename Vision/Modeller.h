#pragma once

#include "VisionUtils.h"

class VI_Texture;
class VI_Object;
class VI_VObject;
class VI_SceneGraphNode;

class LevelSquareInfo;

#include <vector>
using std::vector;

class VI_Modeller {
	static VI_VObject *createBranch(float r0, float r1, float len, int sides, int n_recurse, VI_Texture *textures[2], const float size_u[2], const float size_v[2]);
	static void setPositionKeys(VI_SceneGraphNode *entity, const char *name, const Vector3D &pos, const Vector3D &norm, float length);
public:
	// create primitives
	static VI_VObject *createTriangle(float size);
	static VI_VObject *createRectangle(float w,float d);
	static VI_VObject *createGrid(float w,float d,unsigned short nx,unsigned short ny,bool mirror,bool share);
	static VI_VObject *createPlate();
	static VI_VObject *createCuboid(float w,float h,float d);
	static VI_VObject *createOpenCuboid(float w,float h,float d);
	static VI_VObject *createCone(float h,float r,unsigned short n);
	static VI_VObject *createDisc(float h,float r,unsigned short n,bool two_sided);
	static VI_VObject *createCylinder(float h,float r1,float r2,unsigned short n);
	static VI_VObject *createSphere(float r,unsigned short d);
	static VI_VObject *createHemisphere(float r,unsigned short d);
	static VI_VObject *createPrism(float r,float d,unsigned short n);
	static VI_VObject *createParallelPlates(float xd,float yd,float zd);

	// create complex objects
	static VI_VObject *createTable(float xd,float yd,float zd);
	//static Texture **getTexturesRoom();
	static VI_VObject *createRoom(float w,float h,float d,unsigned short nx,unsigned short ny,const bool *doors,VI_Texture *textures[2],bool share,bool invert);
	static VI_VObject *createLevel(int width,int depth,const LevelSquareInfo *level,VI_Texture *textures[2],float scale_w,float scale_d,float scale_h);
	/*static Texture **getTexturesPerson();
	static VI_VObject *createPerson(float s,int phase,Texture ** textures);*/
	static VI_VObject *sweep(VI_VObject *profile, Vector3D dir, bool capped, bool mirror);
	static VI_VObject *sweep(VI_VObject *profile, const vector<Vector3D> *points, bool capped, bool mirror);
	static VI_VObject *loft(vector<VI_VObject *> profiles, bool capped, bool mirror);
	static VI_VObject *createTree(float r0, float r1, float height, int sides, VI_Texture *textures[2], const float size_u[2], const float size_v[2]);
	static VI_VObject *createBuilding(float w, float d, float ceiling, float top);

	class HumanoidFormat {
	public:
		float height;
		Vector3D body_size;
		Vector3D head_size;
		VI_Texture *armour_texture;
		VI_Texture *skin_texture;
		VI_Texture *face_texture;
		VI_Texture *hair_texture;
		VI_Texture *foot_texture;
		float u_leg_r, l_leg_r;

		HumanoidFormat(float height, Vector3D body_size, Vector3D head_size, float u_leg_r, VI_Texture *armour_texture, VI_Texture *skin_texture, VI_Texture *face_texture, VI_Texture *hair_texture, VI_Texture *foot_texture) : height(height), body_size(body_size), head_size(head_size), u_leg_r(u_leg_r), armour_texture(armour_texture), skin_texture(skin_texture), face_texture(face_texture), hair_texture(hair_texture), foot_texture(foot_texture) {
			l_leg_r = 0.8f*u_leg_r;
		}
	};
#if 0
	static VI_SceneGraphNode *createHumanoid(const HumanoidFormat *format);
#endif

	// perform operations on an object - whilst these might seem more suited to being member functions of ObjectData,
	// we keep them here to reduce complexity of that class
	static void deformAttractor(VI_Object *obj, Vector3D centre, float radius, float scale);
	static void deformPoke(VI_Object *obj, Vector3D pos, Vector3D dir, float radius, float dist);
	static void deformRound(VI_VObject *obj, float frac, float scale);
};
