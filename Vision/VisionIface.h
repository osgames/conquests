#pragma once

#ifdef _WIN32

// disable warning C4250: "xxx inherits yyy via dominance"
#pragma warning(disable:4250)

#define WIN32_MEAN_AND_LEAN
#include <windows.h>

#endif

#ifdef __linux
#include<X11/X.h>
#include<X11/Xlib.h>
#endif

#include "VisionShared.h"
#include "VisionUtils.h"

#include <string>

class VisionPrefs;

class VI_GraphicsEnvironment;
class VI_Graphics2D;
class VI_Font;
class VI_Image;
class VI_Texture;

class VI_World;
class VI_Object;
class VI_VObject;
class VI_SceneGraphNode;
//class VI_Entity;
class VI_Face;
class VI_Terrain;
class VI_Sky;

class VI_Panel;

class Renderer;
class ShaderEffect;

//void VI_addtag();

void VI_initialise(const char *application_name);
string VI_getApplicationDataFilename(const char *folder, const char *name);
char *VI_getApplicationFilename(const char *name);
void VI_cleanup();
void VI_check();
void VI_set_persistence_all(int level);
void VI_set_default_persistence(int level);
void VI_debug_mem_usage_all();
void VI_lock();
void VI_unlock();
bool VI_islocked();
void VI_flush(int level);
void VI_flush_all();
size_t VI_getNextCreationId();
void VI_flushSinceCreationId(size_t creation_id);
int VI_getNTags();
int VI_getNTagsInUse();
void VI_update(int time);
int VI_getGameTimeMS();
int VI_getGameTimeLastFrameMS();
bool VI_keydown(int v_keycode);
bool VI_keyany();
//bool VI_keypressed(int v_keycode);
void VI_createThreads(int n_threads, VI_ThreadFunction **function, void **data);
void VI_pushMessage(void *message);
bool VI_hasMessages();
void *VI_peekMessage();
void *VI_popMessage();

#ifndef VI_log
#define VI_log V_log
#endif

class VI_Sound {
public:
	virtual ~VI_Sound() {}

	virtual void LoadSound(int s,const char *file)=0;
	virtual bool PlaySound(int s,bool loop,int delay)=0;
	virtual void FreeSound(int s)=0;
	virtual bool IsSoundPlaying(int s)=0;
	virtual void StopSound(int s,bool bResetPosition)=0;

	virtual bool playMedia(const char *filename)=0;
	virtual void stopMedia()=0;
	virtual bool isMediaPlaying()=0;

/*#ifdef _WIN32
	virtual void processEvents(UINT message, WPARAM wParam, LPARAM lParam)=0;
	virtual bool initHWND(HWND hWnd)=0;
	//static VI_Sound *create(HWND hwnd);
#endif*/
	static VI_Sound *create();

};

class VI_Graphics2D {
public:
	virtual ~VI_Graphics2D() {}

	virtual void setColor3i(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setColor4i(unsigned char r, unsigned char g, unsigned char b, unsigned char a)=0;
	virtual void setTexture(VI_Texture *texture)=0;
	virtual void move(short x,short y)=0;
	virtual void plot(short x,short y)=0;
	virtual void draw(short x,short y)=0;
	virtual void draw(short x1,short y1,short x2,short y2) const=0;
	virtual void drawRect(short x,short y,short width,short height) const=0;
	virtual void fillRect(short x,short y,short width,short height) const {
		float tu[4] = {0.0f, 0.0f, 1.0f, 1.0f};
		float tv[4] = {1.0f, 0.0f, 1.0f, 0.0f};
		this->fillRect(x, y, width, height, tu, tv);
	}
	virtual void fillRect(short x,short y,short width,short height,const float tu[4], const float tv[4]) const=0;
	virtual void fillTriangle(const short *x,const short *y) const=0;
};

class VI_MouseMotionEvent {
public:
	int x;
	int y;
	int rel_x;
	int rel_y;

	VI_MouseMotionEvent() {
		x = 0;
		y = 0;
		rel_x = 0;
		rel_y = 0;
	}
};

typedef void (VI_GraphicsEnvironment_MouseMotionEvent) (VI_MouseMotionEvent *mouse_motion_event, void *data);
typedef bool (VI_GraphicsEnvironment_Update) (int time);
typedef void (VI_GraphicsEnvironment_Render) ();

class VI_VisionObject {
public:
	virtual ~VI_VisionObject() {}

	//virtual V_TAG_t getTag() const=0;
	virtual void setName(const char *name)=0;
	virtual const char *getName() const=0;
	virtual void addChild(VI_VisionObject *child)=0;
	virtual int getNChildObjects() const=0;
	virtual void detachFromOwner()=0;
	virtual bool removeOwnedObject(VI_VisionObject *child)=0;
	virtual void removeAllOwnedObjects()=0;
	virtual void deleteAllOwnedObjects()=0;
};

class VI_GraphicsEnvironment : public virtual VI_VisionObject {
public:
	virtual ~VI_GraphicsEnvironment() {}

	virtual VI_Font *createFont(const char *family,int pt,int style) const=0;

	virtual Renderer *getRenderer()=0;
	virtual const Renderer *getRenderer() const=0;
	virtual int getWidth() const=0;
	virtual int getHeight() const=0;
	virtual int getAA() const=0;
	virtual bool isFullScreen() const=0;
	virtual const VisionPrefs *getVisionPrefs() const=0;
	virtual void setSMP(bool smp)=0;
	virtual bool hasShaders() const=0;
	virtual bool failedToLoadShaders() const=0;
	virtual bool isActive() const=0;
	virtual void getFrameStats(int *poly_count,int *vertex_count, int *distinct_vertex_count, int *render_calls_count) const=0;
	virtual float getFPS() const=0;
	virtual void getMouseInfo(int *x,int *y,bool buttons[3]) const=0;
	/*virtual void getMouseClick(bool buttons[3]) const=0;
	virtual bool readMouseWheelUp() const=0;
	virtual bool readMouseWheelDown() const=0;*/

	virtual VI_Panel *getPanel() const=0;

	virtual void setZ(float z_near,float z_far)=0;
	virtual void setMouseTexture(VI_Texture *mouse_texture)=0;
	virtual void setPanel(VI_Panel *panel)=0;
	virtual void setModalPanel(VI_Panel *panel)=0;
	virtual VI_Panel *getModalPanel() const=0;
	virtual void setUseShadowVolumes(bool use_shadow_volumes)=0;
	virtual void linkWorld(VI_World *world)=0;
	virtual VI_World *getWorld()=0;
	virtual const VI_World *getWorld() const=0;
	virtual void setSound(VI_Sound *sound)=0;
	virtual VI_Sound *getSound() const=0;

	virtual Vector3D getDirectionOfPoint(const VI_SceneGraphNode *node, int sx, int sy) const=0;
	virtual void localToScreen(int *x, int *y, const VI_SceneGraphNode *node, const Vector3D &local) const=0;
	virtual Vector3D worldToEye(const Vector3D &world) const=0;
	virtual void worldToScreen(int *x,int *y,const Vector3D &world) const=0;

	virtual void setVisible(bool visible)=0;
	virtual void setWindowTitle(const char *title)=0;
	virtual void grabInput(bool grab)=0;
	virtual void showCursor(bool show)=0;
	virtual void setCursor(unsigned char *data, unsigned char *mask, int w, int h, int hot_x, int hot_y)=0;

	virtual void drawFrame()=0;
	virtual void drawFrameTest()=0; // debugging use only
	virtual void swapBuffers()=0;
	virtual void handleInput()=0;
	virtual void handleInput(int mouseX,int mouseY,bool mouseL,bool mouseM,bool mouseR)=0;
	virtual void flushInput()=0;
	virtual void processEvents()=0;
	virtual void waitForEvents()=0;
	virtual void sleep(int time)=0; // sleep, time in milliseconds
	virtual int getRealTimeMS()=0;
	virtual bool shouldQuit()=0;
	virtual void clearQuitRequest()=0;
	virtual bool saveScreen(const char *filename,const char *type)=0;
	//virtual void render(VI_Entity *entity, float x, float y, float z)=0;
	virtual void render(VI_SceneGraphNode *node, float x, float y, float z)=0;
	virtual void setRenderFunc(VI_GraphicsEnvironment_Render *renderFunc)=0;
	virtual void setMouseMotionEventFunc(VI_GraphicsEnvironment_MouseMotionEvent *mouseMotionEventFunc,void *mouseMotionEventData)=0;
	virtual void setFade(float duration, unsigned char fade_start, unsigned char fade_end)=0;
	virtual void stopFade()=0;

	void render(VI_GraphicsEnvironment_Update update);

	// OS specific functions
#ifdef _WIN32
	virtual HWND getHWND() const=0;
	virtual bool isTrueFullscreen() const=0; // hack for Windows, whether the full screen has been implemented as a "true" fullscreen mode in Windows
#endif
#ifdef __linux
	virtual Display *getXDisplay() const=0;
	virtual Window getXWindow() const=0;
#endif
};

class VI_Texture : public virtual VI_VisionObject {
public:
	virtual ~VI_Texture() {}

	virtual int getWidth() const=0;
	virtual int getHeight() const=0;

	virtual void draw(int x, int y)=0;
	virtual void draw(int x, int y, int w, int h)=0;
	virtual void enable()=0;

	// if desired, clamping functions should be called after enabling, but before the texture is used in a rendering call
	/*virtual void clampToEdge()=0;
	virtual void clampMirror()=0;*/
	enum WrapMode {
		WRAPMODE_REPEAT = 0,
		WRAPMODE_CLAMP = 1,
		WRAPMODE_MIRROR = 2
	};
	virtual void setWrapMode(WrapMode wrap_mode)=0;
	enum DrawMode {
		DRAWMODE_SMOOTH = 0,
		DRAWMODE_BLOCKY = 1
	};
	virtual void setDrawMode(DrawMode draw_mode)=0;
};

class VI_Font : public virtual VI_VisionObject {
public:
	virtual ~VI_Font() {}

	virtual int writeText(int x,int y,const char *text) const=0;
	virtual int writeText(int x,int y,const char *text,size_t length) const=0;
	virtual int writeTextExt(int x,int y,const char *text,...) const=0;
	virtual int getWidth(char letter) const=0;
	virtual int getWidth(const char *text) const=0;
	virtual int getHeight() const=0;
};

class VI_Image : public virtual VI_VisionObject {
public:
	virtual ~VI_Image() {}

	virtual int getWidth() const=0;
	virtual int getHeight() const=0;
	virtual bool getAlpha() const=0;
	virtual bool makePowerOf2()=0;
	virtual bool makeSamePowerOf2()=0;
	virtual unsigned char *getData()=0;
	virtual void putRGB(int x, int y, unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void putRGBA(int x, int y, unsigned char r, unsigned char g, unsigned char b, unsigned char a)=0;
	virtual void getRGB(unsigned char *r, unsigned char *g, unsigned char *b, int x, int y) const=0;
	virtual void getRGBA(unsigned char *r, unsigned char *g, unsigned char *b, unsigned char *a, int x, int y) const=0;
	virtual const void *get(int x,int y) const=0;
	virtual const unsigned char *getData() const=0;

	virtual void setAllToi(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void multiply(float scaleR, float scaleG, float scaleB)=0;
	virtual void adjustBrightness(unsigned char bright)=0;
	virtual void merge(VI_Image *image)=0;
	virtual bool scale(int newWidth, int newHeight)=0;
	virtual void flip(bool flipX, bool flipY)=0;

	virtual VI_Image *copy() const=0;
	virtual VI_Image *createDerivative(float scale) const=0;
	virtual VI_Image *createReflection2D() const=0;
	virtual bool save(const char *filename,const char *ext) const=0;

	static VI_Image *load(const char *filename);
	static VI_Image *createBlank(int w, int h, unsigned char r, unsigned char g, unsigned char b, bool alpha, unsigned char a);
	static VI_Image *createNoise(int w,int h);
	static VI_Image *createNoise(int w,int h,float scale_u,float scale_v,const unsigned char filter_max[3],const unsigned char filter_min[3],V_NOISEMODE_t noisemode,int n_iterations,bool maximise);
	static VI_Image *createRadial(int w,int h,const unsigned char filter_max[3],const unsigned char filter_min[3],bool squared,bool alpha);
};

class VI_Sky : public virtual VI_VisionObject {
public:
	virtual ~VI_Sky() {}

	virtual void setMode(V_SKYMODE_t mode)=0;
	virtual void setColori(unsigned char r,unsigned char g,unsigned char b)=0;

	virtual void render(Renderer *renderer)=0;
};

class VI_Clouds : public virtual VI_VisionObject {
public:
	virtual ~VI_Clouds() {}

	virtual void setColori(unsigned char r,unsigned char g,unsigned char b)=0;
	virtual void setParameters(float min_noise, float max_noise)=0;
	virtual void setDir0(float speed_x, float speed_z)=0;
	virtual void setDir1(float speed_x, float speed_z)=0;
};

class VI_Atmosphere : public virtual VI_VisionObject {
public:
	virtual ~VI_Atmosphere() {}
};

class VI_Terrain : public virtual VI_VisionObject {
public:
	virtual ~VI_Terrain() {}

	virtual int getWidth() const=0;
	virtual int getDepth() const=0;
	virtual float getXScale() const=0;
	virtual float getZScale() const=0;
	virtual float getHScale() const=0;
	virtual float getWidthDist() const=0;
	virtual float getDepthDist() const=0;
	virtual unsigned char get(int x,int z) const=0;
	virtual void set(int x,int z,unsigned char v)=0;
	virtual void setTextureMap(int x,int z,unsigned char v)=0;
	virtual float getHeightAt(float x,float z) const=0;
	virtual Vector3D getNormalAt(float x,float z) const=0;
	virtual bool rayfire(Vector3D *hit, Vector3D start, Vector3D dir, float range) const=0;

	virtual void setOffset(Vector3D offset)=0;
	virtual void setLOD(bool lod)=0;
	virtual void setTexture(int index,VI_Texture *texture)=0;
	virtual void setSea(bool sea,float unscaled_sea_height,float sea_extent,bool sea_effects)=0;
	virtual bool hasSea() const=0;
	virtual float getUnscaledSeaHeight() const=0;
	virtual float getSeaHeight() const=0;
	virtual void setSeaTexture(VI_Texture *sea_texture)=0;
	virtual void setLighting(bool lighting_enabled)=0;
	virtual void toggleSeaReflections()=0;
	virtual void setSplatSortTextures(bool splat_sort_textures)=0;
	virtual void setPretendReflectionColor(unsigned char min_rgb[3], unsigned char max_rgb[3])=0;

	virtual void setColorMode(V_COLORMODE_t colormode)=0;
	//virtual void setColorNoise(int index,float min_r,float min_g,float min_b,float max_r,float max_g,float max_b,float noise_scale)=0;
	virtual void setColorNoisei(int index,unsigned char min_r,unsigned char min_g,unsigned char min_b,unsigned char max_r,unsigned char max_g,unsigned char max_b,float noise_scale)=0;
	virtual void setColorAmbientOcclusion()=0;
	virtual void initColor(int index, unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setColorAt(int index, int x, int z, unsigned char r, unsigned char g, unsigned char b)=0;

	virtual bool isVisible(int x,int z) const=0;
	virtual void setVisible(int x,int z,bool v)=0;

	virtual int getNodesRendered() const=0;

	//virtual void setAmbientPixelShader(Shader *shader)=0;
	virtual void setAmbientShader(const ShaderEffect *shader)=0;
};

class VI_World : public virtual VI_VisionObject {
public:
	virtual ~VI_World() {}

	virtual void getAmbience(float *r,float *g,float *b)=0;
	virtual VI_SceneGraphNode *getViewpoint()=0;
	virtual const VI_SceneGraphNode *getViewpoint() const=0;

	virtual bool addNode(VI_SceneGraphNode *node)=0;
	virtual void setSky(VI_Sky *sky)=0;
	virtual VI_Sky *getSky()=0;
	virtual const VI_Sky *getSky() const=0;
	virtual void setClouds(VI_Clouds *clouds)=0;
	virtual VI_Clouds *getClouds()=0;
	virtual const VI_Clouds *getClouds() const=0;
	virtual void setAtmosphere(VI_Atmosphere *atmosphere)=0;
	virtual VI_Atmosphere *getAtmosphere()=0;
	virtual const VI_Atmosphere *getAtmosphere() const=0;
	//virtual void setHorizon(bool enabled)=0;
	virtual void setBackColori(unsigned char r, unsigned char g, unsigned char b)=0;
	//virtual void setGroundColori(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setTerrain(VI_Terrain *terrain)=0;
	virtual VI_Terrain *getTerrain()=0;
	virtual const VI_Terrain *getTerrain() const=0;
	virtual void setAmbiencei(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setViewpoint(VI_SceneGraphNode *viewpoint)=0;
	virtual void enableFog(bool enabled)=0;
	virtual void setFogParmsExp(float density)=0;
};

class VI_HAnimationInfo {
	// note, not a VisionObject!
public:
	virtual ~VI_HAnimationInfo() {}

	virtual void setName(const char *name)=0;
	virtual const char *getName() const=0;
	virtual void addPositionKey(float time,Vector3D pos)=0;
	virtual void addRotationKey(float time,Rotation3D rot)=0;

	static VI_HAnimationInfo *create(const char *name, float start, float end);
};

//typedef void (VI_Entity_Update) (VI_Entity *source, int time);
typedef void (VI_SceneGraphNode_Update) (VI_SceneGraphNode *source, void *data);

class VI_SceneGraphNode : public virtual VI_VisionObject {
public:
	virtual ~VI_SceneGraphNode() {}

	virtual VI_SceneGraphNode *copy() const=0;
	virtual void setObject(VI_Object *v_obj)=0;
	virtual VI_Object *getObject() const=0;
	virtual Vector3D getPosition() const=0;
	virtual Vector3D getOldPosition() const=0;
	virtual Vector3D getCurrentWorldPos() const=0;
	virtual Vector3D getLastRenderedEyePos() const=0;
	virtual void localToWorldMatrix(Matrix4d *out_matrix) const=0;
	virtual Vector3D localToWorldPos(const Vector3D &pos) const=0;
	virtual Vector3D worldToLocalDir(const Vector3D &dir) const=0;
	virtual Rotation3D getRotation() const=0;
	virtual bool getExtent(float *low,float *high,const Vector3D &p,const Vector3D &n,bool firstFrameOnly,bool do_children) const=0;
	virtual bool isVisible() const=0;
	virtual V_SOURCETYPE_t getSourcetype() const=0;
	virtual void setSourcetype(V_SOURCETYPE_t sourcetype)=0;

	virtual void setPosition(float x,float y,float z)=0;
	virtual void setPosition(const Vector3D &v)=0;
	virtual void setInfinite(bool infinite)=0;
	virtual void move(float x,float y,float z)=0;
	virtual void move(Vector3D *v)=0;
	virtual void moveDirection(Vector3D v,float d)=0;
	virtual void moveDirectionY(Vector3D v,float d)=0;
	virtual void setRotation(const Rotation3D &rotation)=0;
	virtual void rotate(float x,float y,float z)=0;
	virtual void rotateLocal(float x,float y,float z)=0;
	virtual void rotateToEuler(float x,float y,float z)=0;
	virtual void rotateToDirection(const Vector3D &dir)=0;
	virtual void rotateToDirection(const Vector3D &axis, const Vector3D &dir)=0;
	virtual void setVisible(bool visible)=0;

	virtual void addChildNode(VI_SceneGraphNode *node)=0;
	virtual size_t getNChildNodes() const=0;
	virtual VI_SceneGraphNode *getChildNode(size_t i)=0;
	virtual const VI_SceneGraphNode *getChildNode(size_t i) const=0;
	virtual VI_SceneGraphNode *findChildNode(const char *name)=0;
	virtual bool removeChildNode(VI_SceneGraphNode *node)=0;
	virtual void removeAllChildNodes()=0;
	virtual VI_SceneGraphNode *getParentNode() const=0;
	virtual void detachFromParentNode()=0;

	virtual void kill()=0;
	virtual void setVertexFrameSpeed(float frame_speed, bool do_children)=0;
	virtual void setVertexAnimState(int state, bool do_children)=0;
	virtual void setVertexAnimFrames(int start_frame, int end_frame, bool do_children)=0;
	virtual int getVertexAnimState() const=0;
	virtual void setAnimLoop(bool loop, bool do_children)=0;
	virtual void setSpeculari(unsigned char r, unsigned char g, unsigned char b, bool do_children)=0;
	virtual void setShadowCaster(bool enabled,bool do_children)=0;
	virtual void enableParticlesystem(bool enabled,bool do_children)=0;
	virtual void expireWhenPsDone(bool enabled)=0;
	virtual void scale(float sx,float sy,float sz, bool do_children)=0;
	virtual void forceColori(unsigned char r, unsigned char g, unsigned char b, unsigned char alpha, bool do_children)=0;
	virtual void unforce(bool do_children)=0;
	virtual void getForceColori(V_BLENDTYPE_t *blendtype,unsigned char *r,unsigned char *g,unsigned char *b,unsigned char *alpha) const=0;
	virtual void setBlendtype(V_BLENDTYPE_t blendtype, bool do_children)=0;
	virtual void inheritHAnimationFrom(VI_SceneGraphNode *from)=0;
	virtual bool setHAnimation(const char *animname, bool forcereset)=0;
	virtual void setHAnimationSpeed(float animationspeed)=0; // in frames per second
	virtual void addHAnimationInfo(VI_HAnimationInfo *animationinfo)=0;
	virtual float getHAnimationTime() const=0;
	virtual void setHAnimationTime(float animationtime, bool do_children)=0;
	virtual void adoptHAnimation(bool do_children)=0;
	virtual void setDefaultHAnimationPosition(bool do_children)=0;
	virtual void setUpdateFunc(VI_SceneGraphNode_Update *updateFunc, void *updateData)=0;
	//virtual void *getUpdateData()=0;

	virtual bool isAnimationFinished() const=0;

	virtual void setAllToIdentity(bool do_this)=0;
	virtual void setVelocity(float x,float y,float z)=0;
	virtual void setForce(float x,float y,float z)=0; // n.b., only supported in Shaders!
	/*virtual void getVelocity(float *x,float *y,float *z) const=0;
	virtual void getForce(float *x,float *y,float *z) const=0;*/
	virtual Vector3D getVelocity() const=0;
	virtual Vector3D getForce() const=0;
	virtual void halt()=0;

	virtual bool collision(const VI_SceneGraphNode *node) const=0;
	virtual bool collisionLine(float *intersection, const Vector3D &pos, const Vector3D &dir, float line_thickness) const=0;
	virtual bool collisionLineSeg(float *intersection, const Vector3D &p0, const Vector3D &p1, float line_thickness) const=0;

	virtual void update(int time, bool do_children)=0;

	/*virtual float getBoundingRadius() const=0;
	virtual float getBoundingRadiusSq() const=0;*/
	virtual Sphere getBoundingSphere() const=0;
	virtual Sphere calculateSuperBoundingSphere() const=0;
	virtual Sphere calculateWorldSuperBoundingSphere() const=0;

	virtual void setPropertyFloat(const char *name, float value)=0;
	virtual float getPropertyFloat(const char *name) const=0;

	virtual void setTextureSpeed(float texture_x_sp, float texture_y_sp)=0; // in frames per second
};

/*class VI_Entity : public virtual VI_SceneGraphNode {
public:
	virtual ~VI_Entity() {}

	//virtual VI_Entity *copyEntity() const=0; // version of copy that safely returns as a VI_Entity
};*/

class VI_ShadowPlane : public virtual VI_VisionObject {
public:
	virtual ~VI_ShadowPlane() {}
};

class VI_Face {
	// note, not a VisionObject!
public:
	enum LightingStyle {
		FLAT = 0,
		CURVED = 1
	};

	virtual ~VI_Face() {}

	virtual void setName(const char *name)=0;
	virtual const char *getName() const=0;

	virtual size_t getNVertices() const=0;
	virtual size_t getVertexIndex(size_t i) const=0;

	virtual void setTexture(VI_Texture *texture)=0;
};

typedef Vector3D (VI_Deform) (Vector3D,void *data);

class VI_Object : public virtual VI_VisionObject {
public:
	virtual ~VI_Object() {}
	virtual void setName(const char *name)=0;
	virtual bool check() const=0;

	virtual Sphere getBoundingSphere()=0;
	virtual bool getBoundingAABB(Box *box)=0;
	virtual void getExtent(float *low,float *high,const Vector3D &n,bool firstFrameOnly) const=0;
	virtual void getDirectionRadiusSq(float *radius_sq,Vector3D *n,bool firstFrameOnly) const=0;
	virtual Vector3D findCentre() const=0;
	virtual bool isShadowCaster() const=0;

	virtual void scale(float sx,float sy,float sz)=0;
	virtual void translate(float sx,float sy,float sz)=0;
	virtual void rotate(const Rotation3D &rot)=0;
	virtual void rotateEuler(float x,float y,float z)=0;
	virtual void transform4d(const Matrix4d &m)=0;
	virtual void deform(VI_Deform *deformFunc, void *deformData)=0;
	virtual void setHardwareAnimation(const VI_GraphicsEnvironment *genv, bool enabled)=0;
	virtual void setAlphai(unsigned char alpha)=0;
	virtual void setBaseColori(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setSpeculari(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setBlendType(V_BLENDTYPE_t blend_type)=0;
	virtual void setShadowPlane(VI_GraphicsEnvironment *genv, VI_ShadowPlane *shadowPlane, unsigned char alpha)=0;
	virtual void setShadowCaster(bool shadow_caster)=0;
	virtual int getNFrames() const=0;
	virtual void setAnimFrames(V_ANIMSTATE_t state,int start,int end)=0;
	virtual void getFrames(V_ANIMSTATE_t state,int *start,int *end) const=0;
};

class TextureUV {
public:
	//int vx_indx;
	float u;
	float v;
	TextureUV(/*int vx_indx,*/float u, float v) {
		//this->vx_indx = vx_indx;
		this->u = u;
		this->v = v;
	}
	void set(float u, float v) {
		this->u = u;
		this->v = v;
	}
};

class VI_Mesh : public virtual VI_Object {
public:
	virtual ~VI_Mesh() {}

	//virtual void addTexture(VI_Texture *texture)=0;
	virtual VI_Mesh *clone()=0; // non-const, as may cause textures to become unowned, due to sharing
	virtual void combineObject(VI_Mesh *mesh)=0;

	static VI_Mesh *create(const char *name);
	static VI_Mesh *create(const char *name, int n_frames);
};

class VI_VObject : public virtual VI_Object {
public:
	virtual ~VI_VObject() {}

	virtual size_t get_n_vertices() const=0;
	virtual size_t get_n_polys() const=0;

	virtual size_t addVertex(float x, float y, float z)=0;
	virtual size_t addVertex(int frame, float x, float y, float z)=0;
	virtual void setVertexColori(size_t index, unsigned char r, unsigned char g, unsigned char b)=0;

	/*virtual VI_Face *addPolygon(size_t v0, size_t v1, size_t v2)=0;
	virtual VI_Face *addPolygon(size_t v0, size_t v1, size_t v2, size_t v3)=0;*/
	virtual VI_Face *addPolygon(size_t v0, size_t v1, size_t v2, const char *name)=0;
	virtual VI_Face *addPolygon(size_t v0, size_t v1, size_t v2, size_t v3, const char *name)=0;
	virtual VI_Face *addPolygon(const size_t *vlist, size_t n_vertices, const char *name)=0;
	virtual void reservePolygons(size_t n_polys)=0;
	virtual void clearPolygons()=0;

	virtual void setTexture(VI_Texture *texture) {
		this->setTexture(texture, false, 0, false, 0.0f, 0.0f);
	}
	virtual void setTexture(VI_Texture *texture, float size_u, float size_v) {
		this->setTexture(texture, false, 0, true, size_u, size_v);
	}
	virtual void setTexture(VI_Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v)=0;
	virtual void setTextureWrap(VI_Texture *texture, const TextureUV **text_uvs)=0;
	virtual void setPolygonTextureWrap(VI_Face *face,VI_Texture *texture,const TextureUV **text_uvs)=0;
	virtual void setPolygonTextureCoords(VI_Face *face,VI_Texture *texture,const float *u, const float *v)=0;
	virtual void setBumpTexture(const VI_GraphicsEnvironment *genv, VI_Texture *bump_texture)=0;

	virtual void combineObject(VI_VObject *o)=0;
	virtual VI_VObject *clone() const=0;

	virtual Vector3D getVertexPos(size_t index) const=0;
	virtual size_t getFaces(VI_Face ***faces)=0;
	virtual const VI_Face *getFace(size_t index) const=0;
	virtual VI_Face *getFace(size_t index)=0;
	virtual VI_Face *findFace(const char *name)=0;
	virtual void setShading(VI_Face::LightingStyle shading)=0;
	virtual void setPolygonBaseColori(VI_Face *face, unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setPolygonTexture(VI_Face *face,VI_Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v)=0;
	virtual void setPolygonTexture(VI_Face *face,VI_Texture *texture,const Vector3D *origin, const Vector3D *u_dir, const Vector3D *v_dir)=0;
	virtual void invertPolygon(VI_Face *face)=0;
	virtual void setSolid(bool solid)=0;
	virtual bool isSolid() const=0;

	virtual VI_Mesh *createMesh()=0;
	virtual void subdivide()=0;

	//static VI_VObject *create();
	static VI_VObject *create(const char *name);
	static VI_VObject *create(const char *name, int n_frames);
};

class VI_Billboard : public virtual VI_Object {
public:
	virtual ~VI_Billboard() {}

	virtual void setOffset(Vector3D v, int i)=0;
	virtual void setLock(bool lock)=0;
	virtual void setWave(float wave_amp, float wave_sp)=0;
	virtual void setColori(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setOneColori(int i, unsigned char r, unsigned char g, unsigned char b)=0;
};

class VI_Light : public virtual VI_Object {
public:
	virtual ~VI_Light() {}

	virtual void setLightAmbienti(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setLightDiffusei(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setLightSpeculari(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setLightAttenuation(float quadratic,float linear,float constant)=0;
};

class VI_Explosion : public virtual VI_Light {
public:
	virtual ~VI_Explosion() {}
};

class VI_Particlesystem : public virtual VI_Object {
public:
	virtual ~VI_Particlesystem() {}

	virtual void setTexture(VI_Texture *texture)=0;

	static VI_Particlesystem *createSnowstorm(int max_particles,VI_Texture *texture,float width,float height,float depth);
};

class VI_GeneralParticlesystem : public virtual VI_Particlesystem {
public:
	virtual ~VI_GeneralParticlesystem() {}

	virtual void setColoriStart(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setColoriEnd(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setAlphaiStart(unsigned char a)=0;
	virtual void setAlphaiEnd(unsigned char a)=0;
	virtual void setSizeStart(float size)=0;
	virtual void setSizeEnd(float size)=0;
	virtual void setBirthRate(int value)=0;
	virtual void setLife(float value)=0;
	virtual void setMass(float value)=0;
	virtual void setInitialVelocity(float value)=0;

	static VI_GeneralParticlesystem *create(int max_particles);
};

/*class VI_Modeller {
public:
	static VI_VObject *createGrid(float w,float d,unsigned short nx,unsigned short ny,bool mirror,bool share);
	static VI_VObject *createCuboid(float w,float h,float d);
	static VI_VObject *createSphere(float r,unsigned short d);

	static void deformAttractor(VI_Object *obj, Vector3D centre, float radius, float scale);
};*/

typedef void (VI_Panel_Action) (VI_Panel *source);
//typedef void (VI_Panel_Action) (VI_Panel *source, void *data);

class VI_MouseClickEvent {
public:
	bool handled;
	bool left;
	bool middle;
	bool right;
	bool wheel_up;
	bool wheel_down;
	int mouse_x;
	int mouse_y;
	VI_Panel_Action *pushed_action;
	VI_Panel        *pushed_action_src;

	VI_MouseClickEvent() {
		handled = false;
		left = false;
		middle = false;
		right = false;
		wheel_up = false;
		wheel_down = false;
		mouse_x = 0;
		mouse_y = 0;
		pushed_action = NULL;
		pushed_action_src = NULL;
	}
};

class VI_MouseReleaseEvent {
public:
	bool handled;
	bool left;
	bool middle;
	bool right;
	int mouse_x;
	int mouse_y;

	VI_MouseReleaseEvent() {
		handled = false;
		left = false;
		middle = false;
		right = false;
		mouse_x = 0;
		mouse_y = 0;
	}
};

class VI_KeyEvent {
public:
	int              code;
	unsigned char    ascii;
	VI_Panel_Action *pushed_action;
	VI_Panel        *pushed_action_src;

	VI_KeyEvent() {
		code = 0;
		ascii = '\0';
		pushed_action = NULL;
		pushed_action_src = NULL;
	}
};

typedef void (VI_Panel_MouseClickEvent) (VI_MouseClickEvent *mouse_click_event, void *data);
typedef void (VI_Panel_MouseReleaseEvent) (VI_MouseReleaseEvent *mouse_release_event, void *data);
typedef void (VI_Panel_KeyDownEvent) (VI_KeyEvent *key_event, void *data);
typedef void (VI_Panel_Paint) (VI_Graphics2D *g2d, void *data);

class VI_Panel : public virtual VI_VisionObject {
public:
	virtual ~VI_Panel() {}

	/*virtual void setX(int x)=0;
	virtual void setY(int y)=0;*/
	virtual void setPos(int fixed_x, int fixed_y)=0;
	virtual void setPosCentred(int fixed_x, int fixed_y)=0;
	virtual int getX() const=0;
	virtual int getY() const=0;

	virtual void setEnabled(bool enabled,bool do_children)=0;
	virtual bool isEnabled() const=0;
	virtual void setVisible(bool visible)=0;
	virtual bool isVisible() const=0;
	virtual void setHasInputFocus(bool has_input_focus)=0;

	virtual void setPopupText(const char *popup_text, const VI_Font *font)=0;
	virtual void addChildPanel(VI_Panel *panel,int x,int y)=0;
	virtual int getNChildPanels() const=0;
	virtual VI_Panel *getChildPanel(size_t i)=0;
	virtual const VI_Panel *getChildPanel(size_t i) const=0;
	virtual bool removeChildPanel(VI_Panel *panel)=0;
	virtual void removeAllChildPanels()=0;
	virtual void detachFromParentPanel()=0;
	//virtual void setAction(V_Action *action)=0;
	virtual void setMeasureFrom(bool from_left, bool from_top)=0;
	virtual void setAction(VI_Panel_Action *action)=0;
	virtual void setMouseEnterAction(VI_Panel_Action *mouse_enter_action)=0;
	virtual void setMouseExitAction(VI_Panel_Action *mouse_exit_action)=0;
	virtual void setMouseClickEventFunc(VI_Panel_MouseClickEvent *mouseClickEventFunc,void *mouseClickEventData)=0;
	virtual void setMouseReleaseEventFunc(VI_Panel_MouseReleaseEvent *mouseReleaseEventFunc,void *mouseReleaseEventData)=0;
	virtual void setKeyDownEventFunc(VI_Panel_KeyDownEvent *keyDownEventFunc,void *keyDownEventData)=0;
	virtual void setPaintFunc(VI_Panel_Paint *paintPanel,void *paintData)=0;
	virtual void setKeyShortcut(int shortcut)=0;
	virtual void disableKeyShortcut()=0;
	virtual void setForeground(float r,float g,float b)=0;
	virtual void setForeground(float r,float g,float b,float a)=0;
	virtual void setBackground(float r,float g,float b)=0;
	virtual void setBackground(float r,float g,float b,float a)=0;
	virtual void setBackgroundAlphai(int alpha)=0;
	virtual void setHighlightColor(unsigned char r, unsigned char g, unsigned char b)=0;
	virtual void setTexture(VI_Texture *texture)=0;
	virtual void setSize(int width, int height)=0;
	virtual void getSize(int *width, int *height) const=0;
	virtual int getWidth() const=0;
	virtual int getHeight() const=0;
	virtual void setUserData(void *userData)=0;
	virtual void *getUserData() const=0;
	virtual VI_Panel *find(int fx, int fy)=0;
};

class VI_Button : public virtual VI_Panel {
public:
	virtual ~VI_Button() {}

	virtual void setText(const char *text)=0;
	virtual void setBorder(bool border)=0;
};

class VI_ImageButton : public virtual VI_Panel {
public:
	virtual ~VI_ImageButton() {}

	virtual void setTexCoords(const float tu[4], const float tv[4])=0;
};

class VI_Cycle : public virtual VI_Panel {
public:
	virtual ~VI_Cycle() {}

	virtual size_t getActive() const=0;
	virtual size_t getNEntries() const=0;
	virtual void addEntry(const char *entry)=0;

	virtual void setActive(size_t active)=0;
};

class VI_Listbox : public virtual VI_Panel {
public:
	virtual ~VI_Listbox() {}

	virtual int getNEntries() const=0;
	virtual int getActive() const=0;
	virtual void setActive(int active)=0;
	virtual void setAllowSelect(bool allow_select)=0;
	virtual bool isAllowSelect() const=0;
	virtual void reset(const std::string *entries, int n_entries)=0;
	virtual void reset(char **entries)=0;
};

class VI_Stringgadget : public virtual VI_Panel {
public:
	virtual ~VI_Stringgadget() {}

	virtual void setReadOnly(bool read_only)=0;
	virtual void setMaxLen(size_t max_len)=0;
	virtual void addText(const char *str)=0;
	virtual void clear()=0;
	virtual void getText(char **text,size_t *length) const=0;
	virtual const char *getText() const=0;
	virtual void setEnterAction(VI_Panel_Action *action)=0;
	virtual void setBlockedCharacters(const char *blocked_characters)=0;
};

class VI_Textfield : public virtual VI_Stringgadget {
public:
	virtual ~VI_Textfield() {}

	virtual void addText(const char *str)=0;
	virtual void setCPos(size_t cpos)=0;
};

VI_GraphicsEnvironment *VI_createGraphicsEnvironment(int width,int height,int multisample,bool fullscreen,bool hdr);
VI_GraphicsEnvironment *VI_createGraphicsEnvironment(VisionPrefs *visionPrefs);

VI_World *VI_createWorld();

VI_Sky *VI_createSky(const char *filename,const char *ext);
VI_Sky *VI_createSky1(const char *filename);
VI_Sky *VI_createSky1(VI_Texture *texture);

VI_Clouds *VI_createClouds();

VI_Atmosphere *VI_createAtmosphere(VI_SceneGraphNode *light_entity); // n.b., returns NULL rather than throwing error, if required shaders not available

VI_Terrain *VI_createTerrain(VI_GraphicsEnvironment *genv,int width,int depth,float xscale,float zscale,float hscale);
VI_Terrain *VI_createTerrainRandom(VI_GraphicsEnvironment *genv,int width,int depth,float xscale,float zscale,float hscale,const int base_heights[4],int min_height,int max_height,float roughness,int seed);

VI_Image *VI_createImage(const char *filename);
VI_Image *VI_createImage(int w,int h);
/*VI_Image *VI_createImageNoise(int w,int h);
VI_Image *VI_createImageNoise(int w,int h,float scale,const unsigned char filter_max[3],const unsigned char filter_min[3],V_NOISEMODE_t noisemode,int n_iterations);
VI_Image *VI_createImageNoise(int w,int h,float scale_u,float scale_v,const unsigned char filter_max[3],const unsigned char filter_min[3],V_NOISEMODE_t noisemode,int n_iterations);
VI_Image *VI_createImageRadial(int w,int h,const unsigned char filter_max[3],const unsigned char filter_min[3],bool squared,bool alpha);*/

VI_Texture *VI_createTexture(const char *filename);
VI_Texture *VI_createTextureWithMask(const char *filename,unsigned char r,unsigned char g,unsigned char b);
VI_Texture *VI_createTexture(VI_Image *image);
VI_Texture *VI_createTextureWithMask(VI_Image *image,unsigned char r,unsigned char g,unsigned char b);

/*VI_VObject *VI_createCuboid(float w,float h,float d);
VI_VObject *VI_createGrid(float w,float d,unsigned short nx,unsigned short ny,bool mirror,bool share);*/

VI_Billboard *VI_createBillboard(float width,float height,VI_Texture *texture,bool blend,int n_billboards);
VI_Billboard *VI_loadBillboardWithMask(const char *filename,float width,float height,unsigned char r,unsigned char g,unsigned char b,int n_billboards);

VI_SceneGraphNode *VI_createLight(VI_Light **light,float aR,float aG,float aB,float dR,float dG,float dB,float x,float y,float z);

VI_SceneGraphNode *VI_createExplosion(VI_Explosion **explosion,VI_Texture *texture,float aR,float aG,float aB,float dR,float dG,float dB,float x,float y,float z);

//VI_Entity *VI_createEntity();
VI_SceneGraphNode *VI_createSceneGraphNode();
//VI_Entity *VI_createEntity(VI_Object *obj);
VI_SceneGraphNode *VI_createSceneGraphNode(VI_Object *obj);

VI_ShadowPlane *VI_createShadowPlane(Vector3D normal,float D,bool shadow,bool reflection);

VI_Panel *VI_createPanel();
VI_Button *VI_createButton(const char *text,const VI_Font *font);
VI_ImageButton *VI_createImageButton(VI_Texture *texture);
VI_ImageButton *VI_createImageButton(VI_Texture *texture, int width, int height);
VI_Cycle *VI_createCycle(char **entries,const VI_Font *font);
VI_Cycle *VI_createCycle(const VI_Font *font);
VI_Listbox *VI_createListbox(int width,int height,char **entries,const VI_Font *font);
VI_Listbox *VI_createListbox(int width,int height,const std::string *entries,int n_entries,const VI_Font *font);
VI_Stringgadget *VI_createStringgadget(const VI_Font *font,int width);
VI_Textfield *VI_createTextfield(const VI_Font *font,int width,int height);

void VI_movementSimpleXY(VI_SceneGraphNode *node,int time);
void VI_movementSimpleXYhover(VI_SceneGraphNode *node,int time);

//#include "VisionTest.h"

/** This class allows locking and unlocking of Vision (via VI_lock() and VI_unlock) in a RAII
  * manner. If an object of this type is created, Vision is locked; Vision is then unlocked
  * automatically when the object goes out of scope, and its destructor is called.
  * Automatic deletion of vision objects can also be enabled if auto_flush is set to true. In
  * this case, under normal operation, the setSucceeded() method should be called before the
  * object goes out of scope, to indicate success. If an exception is thrown, then this means the
  * setSucceeded() is not called. In this case, VI_flush() is also called, with the delete_level
  * passed to the destructor. This means that the VI_unlock() call doesn't waste time preparing
  * objects, if we're only going to end up deleting those objects anyway.
 */
class VisionLocker {
	bool success;
	bool auto_flush;
	int delete_level;
public:
	VisionLocker(bool auto_flush, int delete_level) : success(false), auto_flush(auto_flush), delete_level(delete_level) {
		VI_log("VisionLocker constructor locking\n");
		VI_lock();
	}
	~VisionLocker() {
		VI_log("VisionLocker destructor\n");
		if( auto_flush && !success ) {
			VI_log("    error occurred, so flush, level %d\n", delete_level);
			VI_flush(delete_level);
		}
		VI_log("    VisionLocker destructor now unlocking\n");
		VI_unlock();
	}
	void setSucceeded() {
		this->success = true;
	}
};

// RAII class to ensure that (non-owned) vision objects are deleted if we fail
class VisionObjectGarbageCollector {
	bool success;
	size_t creation_id;
	//vector<VI_VisionObject *> objects;
public:
	VisionObjectGarbageCollector() : success(false) {
		creation_id = VI_getNextCreationId();
		VI_log("VisionObjectGarbageCollector constructor, set mark at creation_id %d\n", creation_id);
	}
	~VisionObjectGarbageCollector() {
		VI_log("VisionObjectGarbageCollector destructor\n");
		/*if( !success ) {
			LOG("garbage collecting: %d objects\n", objects.size());
			for(vector<VI_VisionObject *>::iterator iter = objects.begin(); iter != objects.end(); ++iter) {
				VI_VisionObject *object = *iter;
				VisionObject *d_object = dynamic_cast<VisionObject *>(object); // cast to internal representation
				if( !d_object->owned ) {
					delete object;
				}
			}
		}*/
		if( !success ) {
			VI_log("    error occurred, so garbage collecting objects created since: %d\n", creation_id);
			VI_flushSinceCreationId(creation_id);
		}
	}

	/*void add(VI_VisionObject *object) {
		objects.push_back(object);
	}*/
	void setSucceeded() {
		this->success = true;
	}
};
