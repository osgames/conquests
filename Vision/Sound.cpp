//---------------------------------------------------------------------------
#include "Sound.h"
#include "Resource.h"

//---------------------------------------------------------------------------

Sound::Sound() {
	//this->initialised = false;
}

#ifdef _WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#elif __linux
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#endif

SDL_Sound::SDL_Sound() : Sound(), music(NULL) {
	LOG("SDL_Sound::SDL_Sound()\n");
	for(int i=0;i<max_sounds_c;i++) {
		chunks[i] = NULL;
		channels[i] = -1;
	}

	if( SDL_InitSubSystem(SDL_INIT_AUDIO) != 0 ) {
		LOG("failed to init SDL audio subsystem");
		Vision::setError(new VisionException(NULL, VisionException::V_SOUND_ERROR, "failed to init SDL audio subsystem"));
		return;
	}
	if( Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1 ) {
		LOG("Mix_OpenAudio failed: %s\n", Mix_GetError());
		SDL_QuitSubSystem(SDL_INIT_AUDIO);
		Vision::setError(new VisionException(NULL, VisionException::V_SOUND_ERROR, "Mix_OpenAudio failed"));
		return;
	}
#ifdef _WIN32
    // not yet available on Linux!
	Mix_Init(0);
#endif
	//this->initialised = true;
}

SDL_Sound::~SDL_Sound() {
	for(int i=0;i<max_sounds_c;i++) {
		if( chunks[i] != NULL )
			Mix_FreeChunk(chunks[i]);
	}
	if( music != NULL ) {
		Mix_FreeMusic(music);
	}
	//if( this->initialised )
	{
		Mix_CloseAudio();
#ifdef _WIN32
        // not yet available on Linux!
		Mix_Quit();
#endif
		SDL_QuitSubSystem(SDL_INIT_AUDIO);
	}
}

void SDL_Sound::LoadSound(int s,const char *file) {
	//ASSERT( s >= 0 && s < max_sounds_c );
	if( s < 0 || s >= max_sounds_c ) {
		Vision::setError(new VisionException(NULL, VisionException::V_SOUND_ERROR, "invalid sound index slot"));
	}
	/*if( !initialised )
		return false;*/
	chunks[s] = Mix_LoadWAV(file);
	if( chunks[s] == NULL ) {
		LOG("failed to load sound: %s\n", file);
		LOG("SDL_Sound: Mix_LoadWAV failed: %s\n", Mix_GetError());
		Vision::setError(new VisionException(NULL, VisionException::V_SOUND_ERROR, "fail to load sound"));
		return;
	}
	return;
}

bool SDL_Sound::PlaySound(int s,bool loop,int delay) {
	ASSERT( s >= 0 && s < max_sounds_c );
	/*if( !initialised )
		return false;*/
	if( chunks[s] == NULL )
		return false;
	if( channels[s] != -1 ) {
		if( Mix_Paused(channels[s]) ) {
			// sound was paused, so let's just resume
			Mix_Resume(channels[s]);
			return true;
		}
		// otherwise, let's stop the currently playing sound
		StopSound(s, true);
	}
	channels[s] = Mix_PlayChannel(-1, chunks[s], loop ? -1 : 0);
	if( channels[s] == -1 ) {
		LOG("Failed to play sound %d: %s\n", s, Mix_GetError());
		return false;
	}
	return true;
}

void SDL_Sound::FreeSound(int s) {
	ASSERT( s >= 0 && s < max_sounds_c );
	/*if( !initialised )
		return;*/
	if( chunks[s] == NULL )
		return;

	if( channels[s] != -1 )
		StopSound(s, true);
	Mix_FreeChunk(chunks[s]);
}

bool SDL_Sound::IsSoundPlaying(int s) {
	ASSERT( s >= 0 && s < max_sounds_c );
	/*if( !initialised )
		return false;*/
	if( chunks[s] == NULL )
		return false;
	if( channels[s] == -1 )
		return false;
	if( Mix_Paused(channels[s]) != 0 )
		return false; // paused, so count as not playing
	return true;
}

void SDL_Sound::StopSound(int s,bool bResetPosition) {
	ASSERT( s >= 0 && s < max_sounds_c );
	/*if( !initialised )
		return;*/
	if( chunks[s] == NULL )
		return;
	if( channels[s] == -1 )
		return;
	if( bResetPosition ) {
		Mix_HaltChannel(channels[s]);
		channels[s] = -1;
	}
	else {
		Mix_Pause(channels[s]);
		// n.b., don't reset channels[s], so we can still unpause it
	}
}

bool SDL_Sound::playMedia(const char *filename) {
	LOG("SDL_Sound::playMedia(%s\n", filename);
	if( music != NULL ) {
		Mix_FreeMusic(music);
	}
	music = Mix_LoadMUS(filename);
	if( music == NULL ) {
		LOG("SDL_Sound: Mix_LoadMUS failed: %s\n", Mix_GetError());
		return false;
	}
	//if( Mix_PlayMusic(music, -1) == -1 ) {
	if( Mix_FadeInMusic(music, -1, 2000) == -1 ) {
		LOG("SDL_Sound: Mix_PlayMusic failed: %s\n", Mix_GetError());
		return false;
	}
	Mix_VolumeMusic(MIX_MAX_VOLUME);
	return true;
}

void SDL_Sound::stopMedia() {
	if( music != NULL ) {
		// we fade the music out rather than stopping straight away
		// don't bother to delete the music - this will happen when a new music is played, or
		// when the SDL_Sound is destroyed
		Mix_FadeOutMusic(2000);
	}
}

bool SDL_Sound::isMediaPlaying() {
	return music != NULL;
}

//#endif
