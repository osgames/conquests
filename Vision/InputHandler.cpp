//---------------------------------------------------------------------------
#include "InputHandler.h"
#include "Misc.h"

#include <cstring>

//---------------------------------------------------------------------------

//bool InputHandler::keys[N_KEYS];

bool KeyboardInputHandler::keyAny() {
	for(int i=0;i<N_KEYS;i++) {
		if( keys[i] )
			return true;
	}
	return false;
}

void KeyboardInputHandler::flush() {
	//LOG("FLUSH\n");
	for(int i=0;i<N_KEYS;i++) {
		//keys_old[i] = false;
		keys[i] = false;
		//keysClicked[i] = false;
	}
}

void KeyboardInputHandler::readQueue(GraphicsEnvironment *genv) {
	//LOG("KeyboardInputHandler::readQueue()\n");
}

void KeyboardInputHandler::update() {
}

void MouseInputHandler::setMouse(int mouseX,int mouseY/*,bool mouseL,bool mouseM,bool mouseR*/) {
	this->mouseX = mouseX;
	this->mouseY = mouseY;
	/*this->mouseL = mouseL;
	this->mouseM = mouseM;
	this->mouseR = mouseR;

	if(!mouseBlockL)
	mouseClickL = mouseL;
	else {
	mouseBlockL = mouseL;
	mouseClickL = false;
	}*/
}

/*bool MouseInputHandler::isLMouseClicked() {
	return !handled_event && mouseClickL;
	//return mouseClickL;
}

bool MouseInputHandler::isMMouseClicked() {
	return !handled_event && mouseClickM;
	//return mouseClickM;
}

bool MouseInputHandler::isRMouseClicked() {
	return !handled_event && mouseClickR;
	//return mouseClickR;
}*/

void MouseInputHandler::mouseLChange(bool down) {
	this->mouseL = down;
	/*if( down ) {
		this->q_mouseClickL = true;
		//LOG("CLICK ON\n");
	}*/
}

void MouseInputHandler::mouseMChange(bool down) {
	this->mouseM = down;
	/*if( down )
		this->q_mouseClickM = true;*/
}

void MouseInputHandler::mouseRChange(bool down) {
	this->mouseR = down;
	/*if( down )
		this->q_mouseClickR = true;*/
}

void MouseInputHandler::flush() {
	//LOG("FLUSH\n");
	//mouseX = 0;
	//mouseY = 0;
	/*mouseClickL = mouseL = false;
	mouseClickM = mouseM = false;
	mouseClickR = mouseR = false;*/
	/*mouseClickL = false;
	mouseClickM = false;
	mouseClickR = false;*/
	mouseL = false;
	mouseM = false;
	mouseR = false;
	mouseWheelUp = false;
	mouseWheelDown = false;
}


void MouseInputHandler::update() {
	mouseWheelUp = false;
	mouseWheelDown = false;
}

InputSystem::InputSystem(MouseInputHandler *mouseInputHandler, KeyboardInputHandler *keyboardInputHandler) :
mouseInputHandler(mouseInputHandler), keyboardInputHandler(keyboardInputHandler),
pushed_action(NULL), pushed_mouse_enter_action(NULL), pushed_mouse_exit_action(NULL),
pushed_action_src(NULL), pushed_mouse_enter_action_src(NULL), pushed_mouse_exit_action_src(NULL) {
}

InputSystem::~InputSystem() {
	delete mouseInputHandler;
	delete keyboardInputHandler;
}


Movement::Movement() {
	vsp = 0.024f;
	rot_sp = 0.375;
	sp = 0.032f;

	onground = true;
	speed = 0;

	speed3d.set(0,0,0);
	throttle = 0;
}

Movement::~Movement() {
}

void Movement::inputXY(Point3D *point,int time) {
	if(KEY_DOWN(V_LCONTROL) || KEY_DOWN(V_RCONTROL)) {
		if(KEY_DOWN(V_LEFT)) {
			point->moveDirection(Vector3D::left,sp*time/2.0f);
		}
		else if(KEY_DOWN(V_RIGHT))
			point->moveDirection(Vector3D::right,sp*time/2.0f);
	}
	else {
		if(KEY_DOWN(V_LEFT)) {
			point->rotate(0,rot_sp*time,0);
			//point->resetDir();
		}
		else if(KEY_DOWN(V_RIGHT)) {
			point->rotate(0,-rot_sp*time,0);
			//point->resetDir();
		}
	}
	if(KEY_DOWN(V_UP))
		point->moveDirectionY(Vector3D::forwards,sp*time);
	//point->moveForwards(sp);
	else if(KEY_DOWN(V_DOWN))
		point->moveDirectionY(Vector3D::backwards,sp*time);
	//point->moveForwards(-sp);
	if(KEY_DOWN(V_NUMPAD9) || KEY_DOWN(V_W)) {
		point->rotateLocal(rot_sp*time/3.0f,0.0f,0.0f);
		//point->resetDir();
	}
	else if(KEY_DOWN(V_NUMPAD3) || KEY_DOWN(V_S)) {
		point->rotateLocal(-rot_sp*time/3.0f,0.0f,0.0f);
		//point->resetDir();
	}
}

void Movement::inputXYHover(Point3D *point,int time) {
	inputXY(point, time);
	/*if(KEY_DOWN(V_LSHIFT) || KEY_DOWN(V_RSHIFT)) {
	point->moveDirectionY(Vector3D::forwards,sp);
	point->move(0, -vsp, 0);
	//point->pos.y -= vsp;
	}*/
	if(KEY_DOWN(V_NUMPAD8) || KEY_DOWN(V_Q)) {
		//point->moveDirection(Vector3D::upwards,vsp);
		//point->pos.y += vsp;
		point->move(0, +vsp*time, 0);
	}
	else if(KEY_DOWN(V_NUMPAD2) || KEY_DOWN(V_A)) {
		//point->moveDirection(Vector3D::downwards,vsp);
		//point->pos.y -= vsp;
		point->move(0, -vsp*time, 0);
	}
}

void Movement::inputFlightSimple(Point3D *point,int time) {
	onground = false;
	if(onground) {
		//if(world->onground) {
		if(KEY_DOWN(V_LEFT)) {
			point->rotate(0,rot_sp*time,0);
		}
		else if(KEY_DOWN(V_RIGHT)) {
			point->rotate(0,-rot_sp*time,0);
		}
	}
	else {
		if(KEY_DOWN(V_LEFT))
			point->rotateLocal(0,0,rot_sp*time);
		else if(KEY_DOWN(V_RIGHT))
			point->rotateLocal(0,0,-rot_sp*time);
	}
	if(KEY_DOWN(V_EQUALS))
		speed += 0.01f;
	else if(KEY_DOWN(V_SUBTRACT)) {
		speed -= 0.01f;
		if(speed < 0)
			speed = 0;
	}
	if(KEY_DOWN(V_DELETE)) // rudder left
		point->rotateLocal(0,rot_sp*time,0);
	//point->rot.rotateLocalY(-rot_sp);
	else if(KEY_DOWN(V_PGDOWN)) // rudder right
		point->rotateLocal(0,-rot_sp*time,0);
	//point->rot.rotateLocalY(rot_sp);
	if(KEY_DOWN(V_DOWN)) // pitch up
		point->rotateLocal(rot_sp*time,0,0);
	//point->rot.rotateLocalX(rot_sp);
	else if(KEY_DOWN(V_UP)) // pitch down
		point->rotateLocal(-rot_sp*time,0,0);
	//point->rot.rotateLocalX(-rot_sp);
	// move
	point->moveDirection(Vector3D::forwards,speed);
	//point->check();
	// land
	/*if(point->pos.y <= world->groundLevel) {
	onground=true;
	//world->onground=true;
	point->pos.y = world->groundLevel;
	//int t =((int)viewPoint.rot.x) % 360;
	float tx = point->rot.x;
	if(tx>180 && tx<360) {
	//point->rot.x=0;
	point->rot.x += 4;
	if(point->rot.x >= 360)
	point->rot.x = 0;
	}
	else if(tx>10)
	point->rot.x=10;
	point->rot.z=0;
	}
	else
	onground=false;*/
	//world->onground=false;
}

void Movement::inputFlight(Point3D *point,int time,float groundLevel) {
	if(onground) {
		//if(world->onground) {
		if(KEY_DOWN(V_LEFT))
			point->rotate(0,rot_sp*time,0);
		else if(KEY_DOWN(V_RIGHT))
			point->rotate(0,-rot_sp*time,0);
	}
	else {
		if(KEY_DOWN(V_LEFT))
			point->rotateLocal(0,0,rot_sp*time);
		else if(KEY_DOWN(V_RIGHT))
			point->rotateLocal(0,0,-rot_sp*time);
	}
	if(KEY_DOWN(V_EQUALS) && throttle < 100)
		throttle ++;
	else if(KEY_DOWN(V_SUBTRACT) && throttle > 0)
		throttle --;

	if(KEY_DOWN(V_DELETE)) // rudder left
		point->rotateLocal(0,rot_sp*time,0);
	//point->rot.rotateLocalY(-rot_sp);
	else if(KEY_DOWN(V_PGDOWN)) // rudder right
		point->rotateLocal(0,-rot_sp*time,0);
	//point->rot.rotateLocalY(rot_sp);
	if(KEY_DOWN(V_DOWN)) // pitch up
		point->rotateLocal(rot_sp*time,0,0);
	//point->rot.rotateLocalX(rot_sp);
	else if(KEY_DOWN(V_UP)) // pitch down
		point->rotateLocal(-rot_sp*time,0,0);
	//point->rot.rotateLocalX(-rot_sp);

	// move
	//point->moveDirection(Vector3D::forwards,speed);
	//speed3d->moveDirection(point->rot
	//point->check();
	// land
	bool is_infinite = false;
	if(point->getPosition(&is_infinite).y <= groundLevel) {
		onground=true;
		//world->onground=true;
		Vector3D pos = point->getPosition(&is_infinite);
		//point->pos.y = world->groundLevel;
		pos.y = groundLevel;
		point->moveTo(pos.x, pos.y, pos.z);
		//int t =((int)viewPoint.rot.x) % 360;
		float tx, ty, tz;
		point->getRotation().getEuler(&tx, &ty, &tz);
		point->rotate(tx, ty, 0);
		if(tx>180 && tx<360) {
			//point->rot.x=0;
			point->rotate(4, 0, 0);
			if(tx + 4 >= 360)
				point->rotateToEuler(0, ty, 0);
		}
		else if(tx>10)
			point->rotate(10, ty, 0);
	}
	else
		onground=false;
	//world->onground=false;
}

