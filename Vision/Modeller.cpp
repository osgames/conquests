//---------------------------------------------------------------------------
#include <algorithm>
using std::swap;

#include "Modeller.h"
#include "VisionIface.h"
#include "Resource.h"

//---------------------------------------------------------------------------

VI_VObject * VI_Modeller::createTriangle(float size) {
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		object = VI_VObject::create("Triangle");

		/*Vertex3D vertex;
		vertex.set(0.0f,1.0f*size,0.0f);
		object->addVertex(vertex);
		vertex.set(0.87f*size,-0.5f*size,0.0f);
		object->addVertex(vertex);
		vertex.set(-0.87f*size,-0.5f*size,0.0f);
		object->addVertex(vertex);*/

		object->addVertex(0.0f,1.0f*size,0.0f);
		object->addVertex(0.87f*size,-0.5f*size,0.0f);
		object->addVertex(-0.87f*size,-0.5f*size,0.0f);
		/*Polygon3D poly(0,1,2);
		object->addPolygon(poly);*/
		object->addPolygon(0, 1, 2, "");

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}

	return object;
}

VI_VObject * VI_Modeller::createRectangle(float w,float d)
{
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		float hw=w/2,hd=d/2;
		object = VI_VObject::create("Rectangle");

		object->addVertex(hw,0.0f,hd);
		object->addVertex(-hw,0.0f,hd);
		object->addVertex(-hw,0.0f,-hd);
		object->addVertex(hw,0.0f,-hd);
		object->addPolygon(3, 2, 1, 0, "");

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createGrid(float w,float d,unsigned short nx,unsigned short ny,bool mirror,bool share) {
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		float sw = w/nx;
		float sd = d/ny;
		object = VI_VObject::create("Grid");
		if( share ) {
			for(unsigned short y=0;y<=ny;y++) {
				for(unsigned short x=0;x<=nx;x++) {
					//vertices[i++].set(sw*x,0,sd*y);
					object->addVertex(sw*x, 0.0f, sd*y);
				}
			}
		}
		else {
			for(unsigned short y=0;y<ny;y++) {
				for(unsigned short x=0;x<nx;x++) {
					/*vertices[i++].set(sw*x,0,sd*y);
					vertices[i++].set(sw*(x+1),0,sd*y);
					vertices[i++].set(sw*x,0,sd*(y+1));
					vertices[i++].set(sw*(x+1),0,sd*(y+1));*/
					object->addVertex(sw*x, 0.0f, sd*y);
					object->addVertex(sw*(x+1), 0.0f, sd*y);
					object->addVertex(sw*x, 0.0f, sd*(y+1));
					object->addVertex(sw*(x+1), 0.0f, sd*(y+1));
				}
			}
		}
		//object->polys.reserve(nx*ny);
		object->reservePolygons(nx*ny);
		int i = 0;
		for(unsigned short y=0;y<ny;y++) {
			for(unsigned short x=0;x<nx;x++) {
				int vlist[4];
				if( share ) {
					int n = y*(nx+1) + x;
					vlist[0] = n+1;
					vlist[1] = n+nx+2;
					vlist[2] = n+nx+1;
					vlist[3] = n;
				}
				else {
					vlist[0] = i+1;
					vlist[1] = i+3;
					vlist[2] = i+2;
					vlist[3] = i;
					i += 4;
				}
				if(mirror) {
					object->addPolygon(vlist[0],vlist[1],vlist[2],vlist[3], "");
				}
				else {
					object->addPolygon(vlist[3],vlist[2],vlist[1],vlist[0], "");
				}
			}
		}

		object->setShading(VI_Face::FLAT);
		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createPlate()
{
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		// one sided only!
		const int points=8;
		const int o = 2*points;

		object = VI_VObject::create("Plate");
		/*object->set_n_vertices(2*points+1);

		Vertex3D *vertices = object->getVertices(0);
		vertices[o].set(0,0,0);
		for(int k=0;k<points;k++) {
			float angle = (6.2832f*k)/points;
			vertices[k].set(cos(angle),0,sin(angle));
			vertices[k+points].set(1.5f*cos(angle),0.375f,1.5f*sin(angle));
		}
		*/
		for(int k=0;k<points;k++) {
			float angle = (6.2832f*k)/points;
			object->addVertex(cos(angle),0.0f,sin(angle));
		}
		for(int k=0;k<points;k++) {
			float angle = (6.2832f*k)/points;
			object->addVertex(1.5f*cos(angle),0.375f,1.5f*sin(angle));
		}
		object->addVertex(0.0f, 0.0f, 0.0f);

		//object->polygons=2*points;
		for(int k=0;k<points;k++) {
			int v = k+1;
			if(v==points)
				v=0;
			object->addPolygon(o,k,v, "");
		}
		for(int k=0;k<points;k++) {
			int v = k+1;
			if(v==points)
				v=0;
			object->addPolygon(k,k+points,v+points,v, "");
		}

		//object->isShade=1;

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createCuboid(float w,float h,float d)
{
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		float hw=w/2,hh=h/2,hd=d/2;
		object = VI_VObject::create("Cuboid");
		/*object->set_n_vertices(8);

		//Vertex3D *vertices = object->frames[0];
		Vertex3D *vertices = object->getVertices(0);
		vertices[0].set(hw,-hh,hd);
		vertices[1].set(-hw,-hh,hd);
		vertices[2].set(-hw,-hh,-hd);
		vertices[3].set(hw,-hh,-hd);
		vertices[4].set(hw,hh,hd);
		vertices[5].set(-hw,hh,hd);
		vertices[6].set(-hw,hh,-hd);
		vertices[7].set(hw,hh,-hd);
		*/
		object->addVertex(hw, -hh, hd);
		object->addVertex(-hw, -hh, hd);
		object->addVertex(-hw, -hh, -hd);
		object->addVertex(hw, -hh, -hd);
		object->addVertex(hw, hh, hd);
		object->addVertex(-hw, hh, hd);
		object->addVertex(-hw, hh, -hd);
		object->addVertex(hw, hh, -hd);

		object->addPolygon(0,1,2,3, "BOTTOM");
		object->addPolygon(5,4,7,6, "TOP");
		object->addPolygon(6,7,3,2, "BACK");
		object->addPolygon(7,4,0,3, "RIGHT");
		object->addPolygon(4,5,1,0, "FRONT");
		object->addPolygon(5,6,2,1, "LEFT");

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createOpenCuboid(float w,float h,float d)
{
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		// the top must be covered by another polygon in the object!
		float hw=w/2,hh=h/2,hd=d/2;
		object = VI_VObject::create("Open Cuboid");
		/*object->set_n_vertices(8);

		//Vertex3D *vertices = object->frames[0];
		Vertex3D *vertices = object->getVertices(0);
		vertices[0].set(hw,-hh,hd);
		vertices[1].set(-hw,-hh,hd);
		vertices[2].set(-hw,-hh,-hd);
		vertices[3].set(hw,-hh,-hd);
		vertices[4].set(hw,hh,hd);
		vertices[5].set(-hw,hh,hd);
		vertices[6].set(-hw,hh,-hd);
		vertices[7].set(hw,hh,-hd);
		*/
		object->addVertex(hw, -hh, hd);
		object->addVertex(-hw, -hh, hd);
		object->addVertex(-hw, -hh, -hd);
		object->addVertex(hw, -hh, -hd);
		object->addVertex(hw, hh, hd);
		object->addVertex(-hw, hh, hd);
		object->addVertex(-hw, hh, -hd);
		object->addVertex(hw, hh, -hd);

		object->addPolygon(0,1,2,3, "BOTTOM");
		object->addPolygon(6,7,3,2, "BACK");
		object->addPolygon(7,4,0,3, "RIGHT");
		object->addPolygon(4,5,1,0, "FRONT");
		object->addPolygon(5,6,2,1, "LEFT");

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createCone(float h,float r,unsigned short n) {
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		object = VI_VObject::create("Cone");
		/*object->set_n_vertices(n+1);

		//Vertex3D *vertices = object->frames[0];
		Vertex3D *vertices = object->getVertices(0);
		int i;
		for(i=0;i<n;i++) {
			float x = r * V_cosd( (i*360.0f)/n );
			float y = r * V_sind( (i*360.0f)/n );
			vertices[i].set(x,0,y);
		}
		vertices[n].set(0,h,0);*/
		for(int i=0;i<n;i++) {
			float x = r * V_cosd( (i*360.0f)/n );
			float y = r * V_sind( (i*360.0f)/n );
			object->addVertex(x, 0.0f, y);
		}
		object->addVertex(0.0f, h, 0.0f);

		/*Polygon3D bottom;
		bottom.setNVertices(n);
		for(i=0;i<n;i++) {
			//bottom->vlist[bottom->n_vertices++] = (unsigned short)n-1-i;
			bottom.vlist[i] = (unsigned short)i;
		}
		object->addPolygon(bottom);*/
		//size_t *vlist = new size_t[n];
		vector<size_t> vlist(n);
		for(int i=0;i<n;i++) {
			vlist[i] = i;
		}
		object->addPolygon(&vlist[0], n, "");
		//delete [] vlist;
		for(int i=0;i<n;i++) {
			/*if(i==n-1)
			object->addPolygon(new Polygon3D(i,0,n));
			else
			object->addPolygon(new Polygon3D(i,i+1,n));*/
			if(i==n-1)
				object->addPolygon(0,i,n, "");
			else
				object->addPolygon(i+1,i,n, "");
		}

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createDisc(float h,float r,unsigned short n,bool two_sided)
{
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		object = VI_VObject::create("Disc");
		/*object->set_n_vertices(n);

		//Vertex3D *vertices = object->frames[0];
		Vertex3D *vertices = object->getVertices(0);
		int i;
		for(i=0;i<n;i++) {
			float x = r * V_cosd( (i*360.0f)/n );
			float y = r * V_sind( (i*360.0f)/n );
			vertices[i].set(x,h,y);
		}*/
		for(int i=0;i<n;i++) {
			float x = r * V_cosd( (i*360.0f)/n );
			float y = r * V_sind( (i*360.0f)/n );
			object->addVertex(x,h,y);
		}

		vector<size_t> auto_bottom_vlist;
		size_t *bottom_vlist = NULL;
		//Polygon3D bottom;
		if( two_sided ) {
			//bottom.setNVertices(n);
			//bottom_vlist = new size_t[n];
			auto_bottom_vlist.resize(n);
			bottom_vlist = &auto_bottom_vlist[0];
		}
		/*Polygon3D top;
		top.setNVertices(n);*/
		//size_t *top_vlist = new size_t[n];
		vector<size_t> auto_top_vlist(n);
		size_t *top_vlist = &auto_top_vlist[0];
		for(int i=0;i<n;i++) {
			if( two_sided ) {
				//bottom.vlist[i] = n-1-i;
				bottom_vlist[i] = n-1-i;
			}
			//top.vlist[i] = i;
			top_vlist[i] = i;
		}
		if( two_sided ) {
			//object->addPolygon(bottom);
			object->addPolygon(bottom_vlist, n, "");
			//delete [] bottom_vlist;
		}
		//object->addPolygon(top);
		object->addPolygon(top_vlist, n, "");
		//delete [] top_vlist;

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createCylinder(float h,float r1,float r2,unsigned short n) {
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		object = VI_VObject::create("Cylinder");
		/*object->set_n_vertices(2*n);

		//Vertex3D *vertices = object->frames[0];
		Vertex3D *vertices = object->getVertices(0);
		int i;
		for(i=0;i<n;i++) {
			float x1 = r1 * V_cosd( (i*360.0f)/n );
			float y1 = r1 * V_sind( (i*360.0f)/n );
			float x2 = r2 * V_cosd( (i*360.0f)/n );
			float y2 = r2 * V_sind( (i*360.0f)/n );
			vertices[i].set(x1,0,y1);
			vertices[n+i].set(x2,h,y2);
		}*/
		for(int i=0;i<n;i++) {
			float x1 = r1 * V_cosd( (i*360.0f)/n );
			float y1 = r1 * V_sind( (i*360.0f)/n );
			object->addVertex(x1,0,y1);
		}
		for(int i=0;i<n;i++) {
			float x2 = r2 * V_cosd( (i*360.0f)/n );
			float y2 = r2 * V_sind( (i*360.0f)/n );
			object->addVertex(x2,h,y2);
		}

		/*Polygon3D top, bottom;
		top.setNVertices(n);
		bottom.setNVertices(n);*/
		/*size_t *bottom_vlist = new size_t[n];
		size_t *top_vlist = new size_t[n];*/
		vector<size_t> bottom_vlist(n);
		vector<size_t> top_vlist(n);
		for(int i=0;i<n;i++) {
			bottom_vlist[i] = i;
			top_vlist[i] = 2*n-1-i;
		}
		object->addPolygon(&bottom_vlist[0], n, "");
		object->addPolygon(&top_vlist[0], n, "");
		/*delete [] bottom_vlist;
		delete [] top_vlist;*/
		for(int i=0;i<n;i++)
		{
			if(i==n-1)
				object->addPolygon(n+i,n,0,i, "");
			else
				object->addPolygon(n+i,n+i+1,i+1,i, "");
		}

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createSphere(float r,unsigned short d) {
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		//d += 3;
		int di = 1;
		if(d>0) {
			for(int i=0;i<d;i++)
				di *= 2;
		}
		int dy = di*2;
		int dn = di*4;
		object = VI_VObject::create("Shere");
		//object->expandCapacity(1000,1000);
		/*object->set_n_vertices(dy*dn+2);

		//Vertex3D *vertices = object->frames[0];
		Vertex3D *vertices = object->getVertices(0);*/
		for(int i=0;i<dy;i++) {
			float ah = ( (2*r*(i+1)) / (dy+1) );
			float h = r - ah;
			float dr = sqrt(r*r - h*h);
			for(int j=0;j<dn;j++) {
				float x = dr * V_cosd( (j*360.0f)/dn );
				float y = dr * V_sind( (j*360.0f)/dn );
				//vertices[v++].set(x,ah,y);
				object->addVertex(x, ah, y);
			}
		}
		/*vertices[v++].set(0,0,0);
		vertices[v++].set(0,2*r,0);*/
		object->addVertex(0.0f, 0.0f, 0.0f);
		object->addVertex(0.0f, 2.0f*r, 0.0f);

		/*for(i=0;i<dn-2;i++) {
		int v0=0;
		int v1=i+1;
		int v2=i+2;
		Polygon3D *top=new Polygon3D(v-1-v0,v-1-v1,v-1-v2);
		Polygon3D *bottom=new Polygon3D(v0,v1,v2);
		object->addPolygon(bottom);
		object->addPolygon(top);
		}*/
		size_t v = object->get_n_vertices();
		for(int i=0;i<dn;i++) {
			int v0=i;
			int v1=i+1;
			if(v1 == dn)
				v1 = 0;
			object->addPolygon(v-1,v-3-v0,v-3-v1, "");
			object->addPolygon(v-2,v0,v1, "");
		}
		int bv = 0;
		for(int i=0;i<dy-1;i++) {
			for(int j=0;j<dn;j++) {
				if(j==dn-1)
					object->addPolygon(bv+dn+j,bv+dn,bv+0,bv+j, "");
				else
					object->addPolygon(bv+dn+j,bv+dn+j+1,bv+j+1,bv+j, "");
			}
			bv += dn;
		}
		object->translate(0,-r,0);
		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createHemisphere(float r,unsigned short d) {
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		int di = 1;
		if(d>0) {
			for(int i=0;i<d;i++)
				di *= 2;
		}
		int dy = di*2;
		int dn = di*4;
		object = VI_VObject::create("Hemisphere");
		/*object->set_n_vertices(dy*dn+2);

		//Vertex3D *vertices = object->frames[0];
		Vertex3D *vertices = object->getVertices(0);*/
		for(int i=0;i<dy;i++) {
			float h = ( (r*i) / dy );
			float dr = sqrt(r*r - h*h);
			for(int j=0;j<dn;j++) {
				float x = dr * V_cosd( (j*360.0f)/dn );
				float y = dr * V_sind( (j*360.0f)/dn );
				//vertices[v++].set(x,h,y);
				object->addVertex(x, h, y);
			}
		}
		/*vertices[v++].set(0,0,0);
		vertices[v++].set(0,r,0);*/
		object->addVertex(0.0f, 0.0f, 0.0f);
		object->addVertex(0.0f, r, 0.0f);

		/*for(i=0;i<dn-2;i++) {
		int v0=0;
		int v1=i+1;
		int v2=i+2;
		Polygon3D *top=new Polygon3D(v-1-v0,v-1-v1,v-1-v2),*bottom=new Polygon3D(v0,v1,v2);
		object->addPolygon(bottom);
		object->addPolygon(top);
		}*/
		size_t v = object->get_n_vertices();
		for(int i=0;i<dn;i++) {
			/*unsigned short v0=(unsigned short)i;
			unsigned short v1=(unsigned short)i+1;*/
			int v0 = i;
			int v1 = i+1;
			if(v1 == dn)
				v1 = 0;
			object->addPolygon(v-1,v-3-v0,v-3-v1, "");
			object->addPolygon(v-2,v0,v1, "");
		}
		int bv = 0;
		for(int i=0;i<dy-1;i++) {
			for(int j=0;j<dn;j++) {
				if(j==dn-1)
					object->addPolygon(bv+dn+j,bv+dn,bv+0,bv+j, "");
				else
					object->addPolygon(bv+dn+j,bv+dn+j+1,bv+j+1,bv+j, "");
			}
			bv += dn;
		}

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject * VI_Modeller::createPrism(float r,float d,unsigned short n) {
	VI_VObject * object = createCylinder(d,r,r,n);
	//object->isShade=1;

	return object;
}

VI_VObject * VI_Modeller::createParallelPlates(float xd,float yd,float zd)
{
	VI_VObject *top = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		//float lx=(xd*8)/20;
		//float ly=(yd*8)/20;
		top = createCuboid(xd,yd,zd/10);

		top->translate(0,0,20);
		VI_VObject * bottom = createCuboid(xd,yd,zd/10);
		bottom->translate(0,0,-20);
		//top->combineObject(bottom, false);
		top->combineObject(bottom);
		delete bottom;
		bottom = NULL;

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(top);
		vo->prepare();
	}
	return top;
}

VI_VObject * VI_Modeller::createTable(float xd,float yd,float zd)
{
	VI_VObject *top = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		float lx=(xd*8)/20;
		float lz=(zd*8)/20;
		top = createCuboid(xd,yd/10,zd);

		top->translate(0,(yd*95)/100,0);
		VI_VObject * pole = createOpenCuboid(xd/10,(9*yd)/10,zd/10);
		pole->translate(lx,(yd*9)/20,lz);
		top->combineObject(pole);
		delete pole;
		pole = createOpenCuboid(xd/10,(9*yd)/10,zd/10);
		pole->translate(lx,(yd*9)/20,-lz);
		top->combineObject(pole);
		delete pole;
		pole = createOpenCuboid(xd/10,(9*yd)/10,zd/10);
		pole->translate(-lx,(yd*9)/20,-lz);
		top->combineObject(pole);
		delete pole;
		pole = createOpenCuboid(xd/10,(9*yd)/10,zd/10);
		pole->translate(-lx,(yd*9)/20,lz);
		top->combineObject(pole);
		delete pole;

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(top);
		vo->prepare();
	}
	return top;
}

/*Texture ** Modeller::getTexturesRoom() {
Texture ** textures = new Texture *[3];
//textures[0] = new Texture("textures\\relief13.pcx","pcx");
//textures[1] = new Texture("textures\\natural_wall.pcx","pcx");
textures[0] = Texture::loadTexture("textures\\relief13.pcx");
textures[1] = Texture::loadTexture("textures\\natural_wall.pcx");
textures[2] = NULL;
return textures;
}*/

VI_VObject * VI_Modeller::createRoom(float w,float h,float d,unsigned short nx,unsigned short ny,const bool *doors,VI_Texture *textures[2],bool share,bool invert) {
	VI_VObject *floor = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		floor = createGrid(w,d,nx,ny,invert,share);
		if( textures != NULL )
			floor->setTexture(textures[0],false,0,true,w/nx,d/ny);
		VI_VObject * ceiling = createGrid(w,d,nx,ny,!invert,share);
		if( textures != NULL ) {
			//ceiling->setTexture(textures[0],false);
			ceiling->setTexture(textures[0],false,0,true,w/nx,d/ny);
		}
		ceiling->translate(0,h,0);
		floor->combineObject(ceiling);
		delete ceiling;
		int n = (nx+1)*(ny+1);
		int door=0;
		//Object3D *d_floor = dynamic_cast<Object3D *>(floor); // cast to the internal representation
		for(int i=0;i<nx;i++) {
			if(doors == NULL || !doors[door++]) {
				/*Polygon3D poly(i,i+1,n+i+1,n+i);
				if( textures != NULL ) {
					//poly->setTexture(textures[1], floor->getVertices(0));
					poly.setTexture(textures[1], d_floor->getVertices(0), false, 0, true, w/nx, d/ny);
				}
				poly.invert();
				d_floor->addPolygon(poly);*/
				VI_Face *face = floor->addPolygon(i,i+1,n+i+1,n+i, "");
				if( textures != NULL )
					floor->setPolygonTexture(face, textures[1], false, 0, true, w/nx, d/ny);
				floor->invertPolygon(face);
			}
			if(doors == NULL || !doors[door++]) {
				int m = ny*(nx+1);
				/*Polygon3D poly(m+n+i,m+n+i+1,m+i+1,m+i);
				if( textures != NULL ) {
					//poly->setTexture(textures[1], floor->getVertices(0));
					poly.setTexture(textures[1], d_floor->getVertices(0), false, 0, true, w/nx, d/ny);
				}
				poly.invert();
				d_floor->addPolygon(poly);*/
				VI_Face *face = floor->addPolygon(m+n+i,m+n+i+1,m+i+1,m+i, "");
				if( textures != NULL )
					floor->setPolygonTexture(face, textures[1], false, 0, true, w/nx, d/ny);
				floor->invertPolygon(face);
			}
		}
		for(int i=0;i<ny;i++) {
			if(doors == NULL || !doors[door++]) {
				/*Polygon3D poly(n+i*(nx+1),n+(i+1)*(nx+1),(i+1)*(nx+1),i*(nx+1));
				if( textures != NULL ) {
					//poly->setTexture(textures[1], floor->getVertices(0));
					poly.setTexture(textures[1], d_floor->getVertices(0), false, 0, true, w/nx, d/ny);
				}
				poly.invert();
				d_floor->addPolygon(poly);*/
				VI_Face *face = floor->addPolygon(n+i*(nx+1),n+(i+1)*(nx+1),(i+1)*(nx+1),i*(nx+1), "");
				if( textures != NULL )
					floor->setPolygonTexture(face, textures[1], false, 0, true, w/nx, d/ny);
				floor->invertPolygon(face);
			}
			if(doors == NULL || !doors[door++]) {
				/*Polygon3D poly(i*(nx+1)+nx,(i+1)*(nx+1)+nx,n+(i+1)*(nx+1)+nx,n+i*(nx+1)+nx);
				if( textures != NULL ) {
					//poly->setTexture(textures[1], floor->getVertices(0));
					poly.setTexture(textures[1], d_floor->getVertices(0), false, 0, true, w/nx, d/ny);
				}
				poly.invert();
				d_floor->addPolygon(poly);*/
				VI_Face *face = floor->addPolygon(i*(nx+1)+nx,(i+1)*(nx+1)+nx,n+(i+1)*(nx+1)+nx,n+i*(nx+1)+nx, "");
				if( textures != NULL )
					floor->setPolygonTexture(face, textures[1], false, 0, true, w/nx, d/ny);
				floor->invertPolygon(face);
			}
		}
		floor->setShading(VI_Face::FLAT);

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(floor);
		vo->prepare();
	}
	return floor;
}

VI_VObject * VI_Modeller::createLevel(int width,int depth,const LevelSquareInfo *level,VI_Texture *textures[2],float scale_w,float scale_d,float scale_h) {
	VI_VObject *level_obj = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		level_obj = createGrid(width*scale_w,depth*scale_d,width,depth,false,true);
		if( textures != NULL ) {
			//level_obj->setTexture(textures[0],false);
			level_obj->setTexture(textures[0],false,0,true,scale_w,scale_d);
		}
		//Vertex3D *vx = NULL;
		//Object3D *d_level_obj = dynamic_cast<Object3D *>(level_obj); // cast to the internal representation
		for(int z=0;z<depth;z++) {
			for(int x=0;x<width;x++) {
				const LevelSquareInfo *square = &level[z*width+x];
				if( !square->solid ) {
					bool solid_n = z==0 || level[(z-1)*width+x].solid;
					bool solid_s = z==depth-1 || level[(z+1)*width+x].solid;
					bool solid_w = x==0 || level[z*width+x-1].solid;
					bool solid_e = x==width-1 || level[z*width+x+1].solid;
					if( solid_n ) {
						size_t vx0 = level_obj->addVertex(x*scale_w,0,z*scale_d);
						size_t vx1 = level_obj->addVertex((x+1)*scale_w,0,z*scale_d);
						size_t vx2 = level_obj->addVertex((x+1)*scale_w,scale_h,z*scale_d);
						size_t vx3 = level_obj->addVertex(x*scale_w,scale_h,z*scale_d);
						/*Polygon3D poly(vx0,vx1,vx2,vx3);
						if( textures != NULL ) {
							//poly->setTexture(textures[1], level_obj->getVertices(0));
							poly.setTexture(textures[1], d_level_obj->getVertices(0), false, 0, true, scale_w, scale_h);
						}
						d_level_obj->addPolygon(poly);*/
						VI_Face *face = level_obj->addPolygon(vx0,vx1,vx2,vx3, "");
						if( textures != NULL )
							level_obj->setPolygonTexture(face, textures[1], false, 0, true, scale_w, scale_h);
					}
					if( solid_s ) {
						size_t vx0 = level_obj->addVertex(x*scale_w,0,(z+1)*scale_d);
						size_t vx1 = level_obj->addVertex((x+1)*scale_w,0,(z+1)*scale_d);
						size_t vx2 = level_obj->addVertex((x+1)*scale_w,scale_h,(z+1)*scale_d);
						size_t vx3 = level_obj->addVertex(x*scale_w,scale_h,(z+1)*scale_d);
						/*Polygon3D poly(vx3,vx2,vx1,vx0);
						if( textures != NULL ) {
							//poly->setTexture(textures[1], level_obj->getVertices(0));
							poly.setTexture(textures[1], d_level_obj->getVertices(0), false, 0, true, scale_w, scale_h);
						}
						d_level_obj->addPolygon(poly);*/
						VI_Face *face = level_obj->addPolygon(vx3,vx2,vx1,vx0, "");
						if( textures != NULL )
							level_obj->setPolygonTexture(face, textures[1], false, 0, true, scale_w, scale_h);
					}
					if( solid_w ) {
						size_t vx0 = level_obj->addVertex(x*scale_w,0,z*scale_d);
						size_t vx1 = level_obj->addVertex(x*scale_w,0,(z+1)*scale_d);
						size_t vx2 = level_obj->addVertex(x*scale_w,scale_h,(z+1)*scale_d);
						size_t vx3 = level_obj->addVertex(x*scale_w,scale_h,z*scale_d);
						/*Polygon3D poly(vx3,vx2,vx1,vx0);
						if( textures != NULL ) {
							//poly->setTexture(textures[1], level_obj->getVertices(0));
							poly.setTexture(textures[1], d_level_obj->getVertices(0), false, 0, true, scale_d, scale_h);
						}
						d_level_obj->addPolygon(poly);*/
						VI_Face *face = level_obj->addPolygon(vx3,vx2,vx1,vx0, "");
						if( textures != NULL )
							level_obj->setPolygonTexture(face, textures[1], false, 0, true, scale_d, scale_h);
					}
					if( solid_e ) {
						size_t vx0 = level_obj->addVertex((x+1)*scale_w,0,z*scale_d);
						size_t vx1 = level_obj->addVertex((x+1)*scale_w,0,(z+1)*scale_d);
						size_t vx2 = level_obj->addVertex((x+1)*scale_w,scale_h,(z+1)*scale_d);
						size_t vx3 = level_obj->addVertex((x+1)*scale_w,scale_h,z*scale_d);
						/*Polygon3D poly(vx0,vx1,vx2,vx3);
						if( textures != NULL ) {
							//poly->setTexture(textures[1], level_obj->getVertices(0));
							poly.setTexture(textures[1], d_level_obj->getVertices(0), false, 0, true, scale_d, scale_h);
						}
						d_level_obj->addPolygon(poly);*/
						VI_Face *face = level_obj->addPolygon(vx0,vx1,vx2,vx3, "");
						if( textures != NULL )
							level_obj->setPolygonTexture(face, textures[1], false, 0, true, scale_d, scale_h);
					}
				}
			}
		}
		level_obj->setShading(VI_Face::FLAT);

		//LOG("createLevel Object %d ( %d ) has %d vertices, %d polys\n", level_obj->getTag(), level_obj, level_obj->get_n_vertices(), level_obj->get_n_polys());
		LOG("createLevel Object %d has %d vertices, %d polys\n", level_obj, level_obj->get_n_vertices(), level_obj->get_n_polys());

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(level_obj);
		vo->prepare();
	}
	return level_obj;
}

/*Texture ** Modeller::getTexturesPerson() {
	Texture ** textures = new Texture *[4];
	//textures[1] = GraphicsEnvironment::getSingleton()->renderer->loadTexture("textures\\fish.bmp");
	textures[1] = GraphicsEnvironment::getSingleton()->renderer->loadTexture("textures\\squares.bmp");
	textures[2] = GraphicsEnvironment::getSingleton()->renderer->loadTexture("textures\\upperarm.bmp");
	textures[3] = NULL;
	return textures;
}

VI_VObject * Modeller::createPerson(float s,int phase,Texture ** textures)
{
	Object3D * torso = Modeller::createCuboid(s/2,s,s/8);
	//torso->setTexture(textures[0]);
	torso->translate(0.0f,s*1.5f,0.0f,false);

	Object3D *l,*l2;
	float angLeg=5.0f,angArm=10.0f;
	float bendLArm=5.0f,bendRArm=5.0f;
	if(phase < 0)
		bendLArm += -10.0f*phase;
	else if(phase > 0)
		bendRArm += 10.0f*phase;
	// phase:
	// 1,2,3, or -ve for reverse, or 0

	// legs
	//   left
	l = Modeller::createOpenCuboid(s/8,s/2,s/8);
	l2 = Modeller::createOpenCuboid(s/8,s/2,s/8);
	l2->translate(0,-s/4,0,false);
	//l2->rotate(QV_convertDegrees((phase-3)*2),0,0);
	l2->rotateEuler((phase-3.0f)*2.0f,0.0f,0.0f,false);
	l2->translate(0,-s/4.0f,0.0f,false);
	l->combineObject(l2, false);
	delete l2;
	l->translate(0,-s/4.0f,0.0f,false);
	//l->rotate(QV_convertDegrees(phase*6),0,QV_convertDegrees(angLeg));
	l->rotateEuler(phase*6.0f,0.0f,angLeg,false);
	l->translate(s*0.15f,s,0.0f,false);
	l->setTexture(textures[1],false);
	torso->combineObject(l, false);
	delete l;
	//   right
	l = Modeller::createOpenCuboid(s/8.0f,s/2.0f,s/8.0f);
	l2 = Modeller::createOpenCuboid(s/8.0f,s/2.0f,s/8.0f);
	l2->translate(0,-s/4,0,false);
	//l2->rotate(QV_convertDegrees((-phase-3.0f)*2.0f),0.0f,0.0f);
	l2->rotateEuler((-phase-3)*2.0f,0.0f,0.0f,false);
	l2->translate(0,-s/4.0f,0.0f,false);
	l->combineObject(l2, false);
	delete l2;
	l->translate(0,-s/4.0f,0.0f,false);
	//l->rotate(QV_convertDegrees(-phase*6),0,QV_convertDegrees(-angLeg));
	l->rotateEuler(-phase*6.0f,0.0f,-angLeg,false);
	l->translate(-s*0.15f,s,0.0f,false);
	l->setTexture(textures[1],false);
	torso->combineObject(l, false);
	delete l;
	// arms
	//   left
	l = Modeller::createCuboid(s/10.0f,s/2.0f,s/10.0f);
	l->setTexture(textures[2],false);
	l2 = Modeller::createOpenCuboid(s/10.0f,s/2.0f,s/10.0f);
	l2->translate(0.0f,-s/4.0f,0.0f,false);
	//l2->rotate(QV_convertDegrees(bendLArm),0,0);
	l2->rotateEuler(bendLArm,0.0f,0.0f,false);
	l2->translate(0.0f,-s/4.0f,0.0f,false);
	l->combineObject(l2, false);
	delete l2;
	l->translate(0.0f,-s/4.0f,0.0f,false);
	//l->rotate(QV_convertDegrees(-phase*5),0,QV_convertDegrees(angArm));
	l->rotateEuler(-phase*5.0f,0.0f,angArm,false);
	l->translate(s*0.3f,s*2.0f,0.0f,false);
	torso->combineObject(l, false);
	delete l;
	//   right
	l = Modeller::createCuboid(s/10.0f,s/2.0f,s/10.0f);
	l->setTexture(textures[2],false);
	l2 = Modeller::createOpenCuboid(s/10.0f,s/2.0f,s/10.0f);
	l2->translate(0,-s/4.0f,0.0f,false);
	//l2->rotate(QV_convertDegrees(bendRArm),0,0);
	l2->rotateEuler(bendRArm,0.0f,0.0f,false);
	l2->translate(0,-s/4.0f,0.0f,false);
	l->combineObject(l2, false);
	delete l2;
	l->translate(0.0f,-s/4.0f,0.0f,false);
	//l->rotate(QV_convertDegrees(phase*5),0,QV_convertDegrees(-angArm));
	l->rotateEuler(phase*5.0f,0.0f,-angArm,false);
	l->translate(-s*0.3f,s*2.0f,0.0f,false);
	torso->combineObject(l, false);
	delete l;
	// head
	l = Modeller::createCuboid(s/4.0f,s/4.0f,s/4.0f);
	l->translate(0.0f,s/8.0f,0.0f,false);
	l->translate(0.0f,s*2.125f,0.0f,false);
	torso->combineObject(l, false);
	delete l;

	//torso->setShading(0);
	//cout << top->boundingRadius << endl;
	return torso;
}*/

VI_VObject *VI_Modeller::sweep(VI_VObject *profile, Vector3D dir, bool capped, bool mirror) {
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		VI_VObject *end_profile = profile->clone();
		end_profile->translate(dir.x, dir.y, dir.z);
		vector<VI_VObject *> profiles;
		profiles.push_back(profile);
		profiles.push_back(end_profile);
		object = loft(profiles, capped, mirror);
		delete end_profile;
		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject *VI_Modeller::sweep(VI_VObject *profile, const vector<Vector3D> *points, bool capped, bool mirror) {
	if( points->size() < 2 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_MODELLER_ERROR, "VI_Modeller::sweep: insufficient points supplied"));
	}
	VI_VObject *object = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		vector<VI_VObject *> profiles;

		profiles.push_back(profile);
		Vector3D first_point = points->at(0);
		for(size_t i=1;i<points->size();i++) {
			Vector3D this_point = points->at(i);
			Vector3D dir = this_point - first_point;
			VI_VObject *n_profile = profile->clone();
			n_profile->translate(dir.x, dir.y, dir.z);
			profiles.push_back(n_profile);
		}
		object = loft(profiles, capped, mirror);
		for(size_t i=1;i<profiles.size();i++) {
			VI_VObject *p = profiles.at(i);
			delete p;
		}
		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(object);
		vo->prepare();
	}
	return object;
}

VI_VObject *VI_Modeller::loft(vector<VI_VObject *> profiles, bool capped, bool mirror) {
	if( profiles.size() < 2 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_MODELLER_ERROR, "VI_Modeller::loft: insufficient profiles supplied"));
	}
	//int n_profile_vertices = profiles[0]->frames[0].size();
	size_t n_profile_vertices = profiles[0]->get_n_vertices();
	for(size_t i=0;i<profiles.size();i++) {
		//Object3D *d_profile = dynamic_cast<Object3D *>(profiles[i]); // cast to the internal representation
		if( profiles[i]->get_n_vertices() != n_profile_vertices ) {
			Vision::setError(new VisionException(NULL, VisionException::V_MODELLER_ERROR, "VI_Modeller::loft: unequal number of profile vertices"));
		}
		if( profiles[i]->get_n_polys() != 1 ) {
			Vision::setError(new VisionException(NULL, VisionException::V_MODELLER_ERROR, "VI_Modeller::loft: number of polygons not equal to 1"));
		}
		//Polygon3D *poly = d_profile->getPolygon(0);
		const VI_Face *poly = profiles[i]->getFace(0);
		if( poly->getNVertices() != n_profile_vertices ) {
			Vision::setError(new VisionException(NULL, VisionException::V_MODELLER_ERROR, "VI_Modeller::loft: unequal number of profile polygon vertices"));
		}
	}

	VI_VObject *obj = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		obj = VI_VObject::create("Lofted Object");
		//obj->set_n_vertices(profiles.size() * n_profile_vertices);
		for(size_t i=0/*,v=0*/;i<profiles.size();i++) {
			/*Object3D *d_profile = dynamic_cast<Object3D *>(profiles[i]); // cast to the internal representation
			Polygon3D *poly = static_cast<Polygon3D *>(profiles[i]->getFace(0)); // cast to the internal representation*/
			VI_Face *face = profiles[i]->getFace(0);
			for(size_t j=0;j<n_profile_vertices;j++/*,v++*/) {
				//int indx = poly->vlist[j];
				size_t indx = face->getVertexIndex(j);
				//Vertex3D vx = d_profile->getVertex(0, indx);
				Vector3D vx = profiles[i]->getVertexPos(indx);
				//obj->setVertex(0, v, &vx);
				obj->addVertex(vx.x, vx.y, vx.z);
			}
		}
		for(size_t i=0;i<profiles.size()-1;i++) {
			for(size_t j=0;j<n_profile_vertices;j++) {
				size_t indx = i*n_profile_vertices+j;
				size_t indx2 = i*n_profile_vertices+(j==n_profile_vertices-1 ? 0 : j+1);
				if( !mirror ) {
					swap(indx, indx2);
				}
				obj->addPolygon(indx, indx+n_profile_vertices, indx2+n_profile_vertices, indx2, "");
			}
		}
		if( capped ) {
			VI_Face *front_face = profiles[0]->getFace(0);
			VI_Face *back_face = profiles[profiles.size()-1]->getFace(0);
			// non-triangulated version
			/*Polygon3D front, back;
			front.setNVertices(n_profile_vertices);
			back.setNVertices(n_profile_vertices);
			for(int i=0;i<n_profile_vertices;i++) {
				int front_indx = i;
				int back_indx = n_profile_vertices-1-i;
				if( !mirror ) {
					swap(front_indx, back_indx);
				}
				front.setVertex(front_indx, i);
				back.setVertex(back_indx, (profiles.size()-1)*n_profile_vertices+i);
			}
			obj->addPolygon(front);
			obj->addPolygon(back);*/
			vector<size_t> front_vlist(n_profile_vertices);
			vector<size_t> back_vlist(n_profile_vertices);
			//front_list.resize(n_profile_vertices);
			//back_list.resize(n_profile_vertices);
			for(size_t i=0;i<n_profile_vertices;i++) {
				size_t front_indx = i;
				size_t back_indx = n_profile_vertices-1-i;
				if( !mirror ) {
					swap(front_indx, back_indx);
				}
				front_vlist.at(front_indx) = i;
				back_vlist.at(back_indx) = (profiles.size()-1)*n_profile_vertices+i;
			}
			//obj->addPolygon(&*front_vlist.begin(), front_vlist.size(), front_face->getName());
			//obj->addPolygon(&*back_vlist.begin(), back_vlist.size(), back_face->getName());
			obj->addPolygon(&front_vlist[0], front_vlist.size(), front_face->getName());
			obj->addPolygon(&back_vlist[0], back_vlist.size(), back_face->getName());
			// triangulated version
			/*for(int tr=0;tr<n_profile_vertices-2;tr++) {
				int back_offset = (profiles.size()-1)*n_profile_vertices;
				const int n_verts = 3;
				int front_vlist[n_verts];
				int back_vlist[n_verts];
				int fwd_indices[n_verts] = {0, 1, 2};
				int bwd_indices[n_verts] = {1, 0, 2};
				int *front_indices = mirror ? fwd_indices : bwd_indices;
				int *back_indices = mirror ? bwd_indices : fwd_indices;
				front_vlist[ front_indices[0] ] = 0;
				back_vlist[ back_indices[0] ] = back_offset;
				for(int i=1;i<3;i++) {
					front_vlist[ front_indices[i] ] = tr+i;
					back_vlist[ back_indices[i] ] = back_offset+tr+i;
				}
				obj->addPolygon(front_vlist, n_verts, front_face->getName());
				obj->addPolygon(back_vlist, n_verts, back_face->getName());
			}*/
		}

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(obj);
		vo->prepare();
	}
	return obj;
}

VI_VObject *VI_Modeller::createBranch(float r0, float r1, float len, int sides, int n_recurse, VI_Texture *textures[2], const float size_u[2], const float size_v[2]) {
	VI_VObject *branch = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		const int n_profiles = 4;
		float disp[n_profiles] = {1.0f, 1.2f, 1.1f, 1.0f};
		vector<VI_VObject *> profiles;
		for(int i=0;i<n_profiles;i++) {
			float frac = ((float)i) / ((float)n_profiles - 1.0f);
			float rad = (1.0f-frac) * r0 + frac * r1;
			rad *= disp[i];
			float h = frac * len;
			VI_VObject *profile = createDisc(h, rad, sides, false);
			profiles.push_back(profile);
		}
		const bool capped = true;
		branch = loft(profiles, capped, true);
		for(size_t i=0;i<profiles.size();i++) {
			delete profiles[i];
		}
		branch->setTexture(textures[0], false, 0, true, size_u[0], size_v[0]);

		if( n_recurse > 0 ) {
			const int n_sub = 4;
			float sub_dists[n_sub] = {0.5f, 0.65f, 0.75f, 0.8f};
			for(int i=0;i<n_sub;i++) {
				float x = (rand() % RAND_MAX) / ((float)RAND_MAX - 1.0f);
				x = (x - 0.5f) * 2.0f;
				float z = (rand() % RAND_MAX) / ((float)RAND_MAX - 1.0f);
				z = (z - 0.5f) * 2.0f;
				Vector3D vec(x, 0.0f, z);
				if( vec.x == 0 && vec.z == 0 )
					vec.set(1.0f, 0.0f, 0.0f);
				else
					vec.normalise();
				const float angle = 0.523599f; // 30 degrees
				const Vector3D up(0.0f, 1.0f, 0.0f);
				//Vector3D dir = cos(angle) * vec + sin(angle) * up;
				Vector3D vec_perp = up ^ vec;
				vec_perp.normalise();
				Rotation3D rot;
				rot.getQuaternion()->setAngleAxis(vec_perp.x, vec_perp.y, vec_perp.z, angle);
				//const float scale = 0.1f;
				const float scale = 0.5f;
				VI_VObject *sub_branch = createBranch(scale * r0, scale * r1, scale * len, sides, n_recurse-1, textures, size_u, size_v);
				sub_branch->rotate(rot);
				sub_branch->translate(0.0f, sub_dists[i] * len, 0.0f);
				branch->combineObject(sub_branch);
				delete sub_branch;
			}
		}
		else {
			// leaves
			VI_VObject *leaves = createSphere(4.0f*r1, 1);
			leaves->translate(0.0f, 0.8f*len, 0.0f);
			leaves->setTexture(textures[1], false, 0, true, size_u[1], size_v[1]);
			branch->combineObject(leaves);
			delete leaves;
		}

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(branch);
		vo->prepare();
	}
	return branch;
}

VI_VObject *VI_Modeller::createTree(float r0, float r1, float height, int sides, VI_Texture *textures[2], const float size_u[2], const float size_v[2]) {
	VI_VObject *tree = createBranch(r0, r1, height, sides, 2, textures, size_u, size_v);
	/*Object3D *d_tree = dynamic_cast<Object3D *>(tree); // cast to the internal representation
	d_tree->setName("TREE");*/
	tree->setName("TREE");
	return tree;
}

VI_VObject *VI_Modeller::createBuilding(float w, float d, float ceiling, float top) {
	VI_VObject *building = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		building = createCuboid(w, ceiling, d);
		building->translate(0.0f, ceiling*0.5f, 0.0f);

		{
			//Object3D *roof_profile = new Object3D();
			VI_VObject *roof_profile = VI_VObject::create("");
			roof_profile->addVertex(-w/2, ceiling, -d/2-d*0.02f);
			roof_profile->addVertex(-w/2, ceiling, d/2+d*0.02f);
			roof_profile->addVertex(-w/2, top, 0.0f);
			roof_profile->addPolygon(0, 1, 2, "");

			/*Object3D *roof_profile2 = new Object3D();
			vertex.set(w/2, ceiling, d/2);
			roof_profile2->addVertex(vertex);
			vertex.set(w/2, ceiling, -d/2);
			roof_profile2->addVertex(vertex);
			vertex.set(w/2, top, 0.0f);
			roof_profile2->addVertex(vertex);
			Polygon3D poly2(0,1,2);
			roof_profile2->addPolygon(poly2);

			vector<Object3D *> profiles;
			profiles.push_back(roof_profile2);
			profiles.push_back(roof_profile);
			Object3D *roof = loft(profiles, true, true);*/

			const Vector3D dir(w, 0.0f, 0.0f);
			VI_VObject *roof = sweep(roof_profile, dir, true, true);
			building->combineObject(roof);
			//building = roof;

			//delete roof;
			delete roof_profile;
		}

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(building);
		vo->prepare();
	}
	return building;
}

void VI_Modeller::setPositionKeys(VI_SceneGraphNode *node, const char *name, const Vector3D &pos, const Vector3D &norm, float length) {
	VI_HAnimationInfo *animinfo = VI_HAnimationInfo::create(name, 0.0f, length);
	const int n_steps_c = 4;
	node->setHAnimation(name, true);
	for(int i=0;i<=n_steps_c;i++) {
		float time = length * ((float)i)/((float)n_steps_c);
		node->setHAnimationTime(time, true);
		node->adoptHAnimation(true);
		float low = 0.0f, high = 0.0f;
		if( node->getExtent(&low, &high, pos, norm, true, true) ) {
			Vector3D new_pos = pos - norm * low;
			new_pos += node->getPosition(); // need to also include the current position, as node->getExtent() takes this into account
			animinfo->addPositionKey(time, new_pos);
		}
	}
	node->addHAnimationInfo(animinfo);
}

#if 0
VI_SceneGraphNode *VI_Modeller::createHumanoid(const HumanoidFormat *format) {
	VI_SceneGraphNode *root = NULL;
	vector<VI_VObject *> objects;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		VI_Texture *hair_texture = format->hair_texture;
		//VI_Texture *hair_texture = format->armour_texture;
		//VI_Texture *hair_texture = format->skin_texture;
		//VI_Texture *hair_texture = VI_createTexture("textures/pstextures_blonde-hair-texture.jpg");
		//VI_Texture *hair_texture = VI_createTexture("textures/pstextures_brown-hair-texture.jpg");

		/*VI_Texture *front_texture = VI_createTexture("textures/o/p.gif");
		VI_Texture *back_texture = VI_createTexture("textures/o/p_back.jpg");
		VI_Texture *mat_texture = VI_createTexture("textures/o/p_mat.jpg");*/

		/*VI_Texture *front_texture = VI_createTexture("textures/o/p2.jpg");
		VI_Texture *back_texture = VI_createTexture("textures/o/p2_mat.jpg");
		VI_Texture *mat_texture = VI_createTexture("textures/o/p2_mat.jpg");*/

		/*VI_Texture *front_texture = VI_createTexture("textures/o/cam.jpg");
		//VI_Texture *front_texture = VI_createTexture("textures/o/cam2.jpg");
		VI_Texture *back_texture = VI_createTexture("textures/o/cam_mat.jpg");
		VI_Texture *mat_texture = VI_createTexture("textures/o/cam_mat.jpg");*/

		/*VI_Texture *front_texture = VI_createTexture("textures/o/b_front.jpg");
		VI_Texture *back_texture = VI_createTexture("textures/o/b_back.jpg");
		VI_Texture *mat_texture = VI_createTexture("textures/o/b_mat.jpg");*/

		/*VI_Texture *front_texture = VI_createTexture("textures/o/g_front.jpg");
		VI_Texture *back_texture = VI_createTexture("textures/o/g_back.jpg");
		VI_Texture *mat_texture = VI_createTexture("textures/o/g_mat.jpg");*/

		/*VI_Texture *front_texture = VI_createTexture("textures/o/g2_front.jpg");
		//VI_Texture *front_texture = VI_createTexture("textures/o/g3_front.jpg");
		VI_Texture *back_texture = VI_createTexture("textures/o/g2_back.jpg");
		VI_Texture *mat_texture = VI_createTexture("textures/o/g2_mat.jpg");*/

		VI_Texture *mat_texture = format->armour_texture;

		root = VI_createSceneGraphNode();
		root->setName("Humanoid");

		VI_SceneGraphNode *dummy = VI_createSceneGraphNode();
		root->addChildNode(dummy);

		//const float height = 1.55f;
		//const float thin = 0.001f;
		//const float body_w = 0.3f, body_h = 0.5f, body_d = 0.12f;
		const float neck_w = 0.04f, neck_h = 0.02f;
		const float chin_w = 0.03f;
		//const float head_w = 0.14f, head_d = 0.1f, head_h = 0.15f;
		float head_w = format->head_size.x, head_h = format->head_size.y, head_d = format->head_size.z;
		const float foot_w = 0.1f, foot_h = 0.1f, foot_d = 0.24f;
		const float hand_w = 0.08f, hand_h = 0.1f, hand_d = 0.1f;
		const float hip_overlap_h = 0.1f * format->body_size.y;
		const float leg_length = format->height - ( format->body_size.y - hip_overlap_h ) - head_h - neck_h - foot_h;
		/*const float u_leg_r = 0.05f, u_leg_length = 0.4f*leg_length;
		const float l_leg_r = 0.04f, l_leg_length = 0.5f*leg_length;*/
		const float u_arm_r = format->l_leg_r, u_arm_length = 0.24f;
		const float l_arm_r = 0.75f*u_arm_r, l_arm_length = 0.26f;
		const float u_leg_length = 0.45f*leg_length;
		const float l_leg_length = 0.55f*leg_length;

		//const unsigned char skin_r = 224, skin_g = 180, skin_b = 118;
		//const unsigned char skin_r = 200, skin_g = 190, skin_b = 188;
		//const unsigned char skin_r = 190, skin_g = 180, skin_b = 175;
		/*const unsigned char skin_r = 224, skin_g = 180, skin_b = 150;
		const unsigned char skin_min[] = {0.9f*skin_r, 0.9f*skin_g, 0.9f*skin_b};
		const unsigned char skin_max[] = {1.1f*skin_r, 1.1f*skin_g, 1.1f*skin_b};
		VI_Image *skin_image = VI_createImageNoise(256, 256, 4.0f, skin_max, skin_min, V_NOISEMODE_PATCHY, 1);
		VI_Texture *skin_texture = VI_createTexture(skin_image);*/
		const float skin_size_u = 0.25f, skin_size_v = 0.25f;

		//VI_Texture *chain_texture = VI_createTexture("objects/knight/chain.jpg");
		//const float chain_size_u = 0.1f, chain_size_v = 0.1f;
		const float chain_size_u = 0.25f, chain_size_v = 0.25f;

		const float foot_size_u = 0.25f, foot_size_v = 0.25f;

		/*const unsigned char hair_min[] = {200, 166, 127};
		const unsigned char hair_max[] = {240, 230, 170};
		VI_Image *hair_image = VI_createImageNoise(256, 256, 4.0f, hair_max, hair_min, V_NOISEMODE_SMOKE, 1);
		VI_Texture *hair_texture = VI_createTexture(hair_image);*/

		const float arm_z_angle_c = 10.0f;
		const float leg_z_angle_c = 5.0f;
		const float leg_x_angle_c = 40.0f;
		const float leg_x_angle_bwd_c = 20.0f;
		const float lleg_x_angle1_c = 10.0f;
		const float lleg_x_angle2_c = 25.0f;

		// torso
		VI_Entity *entity_torso = NULL;
		float torso_height = 0;
		{
			const int n_verts_c = 8;
			size_t vlist[n_verts_c] = {0, 1, 2, 3, 4, 5, 6, 7};

			VI_VObject *p0 = VI_VObject::create("");

			p0->addVertex(0.0f, 0.0f, 0.0f);
			p0->addVertex(0.3f*format->body_size.x, 0.0f, 0.0f);
			p0->addVertex(0.4f*format->body_size.x, 0.0f, 0.5f*format->body_size.z);
			p0->addVertex(0.3f*format->body_size.x, 0.0f, format->body_size.z);

			p0->addVertex(0.0f, 0.0f, format->body_size.z);
			p0->addVertex(-0.3f*format->body_size.x, 0.0f, format->body_size.z);
			p0->addVertex(-0.4f*format->body_size.x, 0.0f, 0.5f*format->body_size.z);
			p0->addVertex(-0.3f*format->body_size.x, 0.0f, 0.0f);

			p0->addPolygon(vlist, n_verts_c, "");

			VI_VObject *p1 = VI_VObject::create("");
			const float chest_h = 0.7f*format->body_size.y;

			p1->addVertex(0.0f, chest_h, -0.2f*format->body_size.z);
			p1->addVertex(0.35f*format->body_size.x, chest_h, 0.0f);
			p1->addVertex(0.45f*format->body_size.x, chest_h, 0.5f*format->body_size.z);
			p1->addVertex(0.35f*format->body_size.x, chest_h, format->body_size.z);

			p1->addVertex(0.0f, chest_h, 1.1f*format->body_size.z);
			p1->addVertex(-0.35f*format->body_size.x, chest_h, format->body_size.z);
			p1->addVertex(-0.45f*format->body_size.x, chest_h, 0.5f*format->body_size.z);
			p1->addVertex(-0.35f*format->body_size.x, chest_h, 0.0f);

			p1->addPolygon(vlist, n_verts_c, "");

			VI_VObject *p2 = VI_VObject::create("");
			const float top_h = 0.9f*format->body_size.y;

			p2->addVertex(0.0f, top_h, 0.1f*format->body_size.z);
			p2->addVertex(0.4f*format->body_size.x, top_h, 0.1f*format->body_size.z);
			p2->addVertex(0.5f*format->body_size.x, top_h, 0.5f*format->body_size.z);
			p2->addVertex(0.4f*format->body_size.x, top_h, 0.9f*format->body_size.z);

			p2->addVertex(0.0f, top_h, 0.9f*format->body_size.z);
			p2->addVertex(-0.4f*format->body_size.x, top_h, 0.9f*format->body_size.z);
			p2->addVertex(-0.5f*format->body_size.x, top_h, 0.5f*format->body_size.z);
			p2->addVertex(-0.4f*format->body_size.x, top_h, 0.1f*format->body_size.z);

			p2->addPolygon(vlist, n_verts_c, "");

			VI_VObject *p3 = VI_VObject::create("");

			const float coll = 0.01f;
			p3->addVertex(0.0f, format->body_size.y, 0.1f*format->body_size.z);
			p3->addVertex(0.2f*format->body_size.x, format->body_size.y+coll, 0.1f*format->body_size.z);
			p3->addVertex(0.3f*format->body_size.x, format->body_size.y, 0.5f*format->body_size.z);
			p3->addVertex(0.2f*format->body_size.x, format->body_size.y+coll, 0.9f*format->body_size.z);

			p3->addVertex(0.0f, format->body_size.y, 0.9f*format->body_size.z);
			p3->addVertex(-0.2f*format->body_size.x, format->body_size.y+coll, 0.9f*format->body_size.z);
			p3->addVertex(-0.3f*format->body_size.x, format->body_size.y, 0.5f*format->body_size.z);
			p3->addVertex(-0.2f*format->body_size.x, format->body_size.y+coll, 0.1f*format->body_size.z);

			p3->addPolygon(vlist, n_verts_c, "");

			vector<VI_VObject *> profiles;
			profiles.push_back(p0);
			profiles.push_back(p1);
			profiles.push_back(p2);
			profiles.push_back(p3);
			VI_VObject *obj = loft(profiles, true, true);
			delete p0;
			delete p1;
			delete p2;
			delete p3;

			obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			//obj->setShading(VI_Face::FLAT);
			/*VI_Face **faces = NULL;
			int n_faces = obj->getFaces(&faces);
			float tx_h = format->body_size.y+coll+10.0f*V_TOL_LINEAR;
			Vector3D t_o(-0.5f*format->body_size.x, tx_h, 0.0f);
			Vector3D t_u(format->body_size.x, 0.0f, 0.0f);
			Vector3D t_v(0.0f, tx_h, 0.0f);
			{
				obj->setPolygonTexture(faces[0], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[1], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[6], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[7], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[8], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[9], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[14], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[15], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[16], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[17], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[22], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[23], front_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[25], front_texture, &t_o, &t_u, &t_v);
				//obj->setPolygonTexture(faces[25], format->skin_texture, &t_o, &t_u, &t_v);
			}
			//obj->setTexture(format->skin_texture, skin_size_u, skin_size_v);
			{
				obj->setPolygonTexture(faces[2], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[3], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[4], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[5], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[10], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[11], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[12], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[13], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[18], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[19], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[20], back_texture, &t_o, &t_u, &t_v);
				obj->setPolygonTexture(faces[21], back_texture, &t_o, &t_u, &t_v);
			}

			delete [] faces;*/

			objects.push_back(obj);
			entity_torso = VI_createEntity(obj);
			entity_torso->setName("TORSO");
			torso_height = foot_h + l_leg_length + u_leg_length;
			//entity_torso->setPosition(0.0f, torso_height, 0.5f*head_d - 0.5f*body_d);
			//entity_torso->setPosition(0.0f, 0.0f, -2.0f*format->body_size.z);
			entity_torso->setPosition(0.0f, 0.0f, -0.5f*format->body_size.z);
		
			dummy->addChildNode(entity_torso);
		}

		// neck
		VI_Entity *entity_neck = NULL;
		{
			//VI_VObject *obj = VI_Modeller::createCuboid(neck_w, neck_h, neck_w);
			VI_VObject *obj = VI_Modeller::createCylinder(neck_h, neck_w, 0.8f*neck_w, 8);
			obj->translate(0.0f, -0.5f*neck_h, 0.0f);
			//obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			obj->setTexture(format->skin_texture, skin_size_u, skin_size_v);
			objects.push_back(obj);
			entity_neck = VI_createEntity(obj);
			entity_neck->setName("NECK");
			entity_neck->setPosition(0.0f, format->body_size.y + 0.5f*neck_h, 0.5f*format->body_size.z);
			//entity_neck->setPosition(0.0f, format->body_size.y, 0.5f*format->body_size.z);
			entity_torso->addChildNode(entity_neck);
		}

		// head
		VI_Entity *entity_head = NULL;
		{
			/*VI_VObject *p0 = VI_VObject::create("");
			p0->addVertex(0.5*chin_w, 0.0f, 0.0f);
			p0->addVertex(0.5*head_w, head_d, 0.1f*head_h);
			p0->addVertex(-0.5*head_w, head_d, 0.1f*head_h);
			p0->addVertex(-0.5*chin_w, 0.0f, 0.0f);
			p0->addPolygon(0, 1, 2, 3, "");

			VI_VObject *p1 = VI_VObject::create("");
			p1->addVertex(0.5*head_w, 0.0f, head_h);
			p1->addVertex(0.5*head_w, head_d, head_h);
			p1->addVertex(-0.5*head_w, head_d, head_h);
			p1->addVertex(-0.5*head_w, 0.0f, head_h);
			p1->addPolygon(0, 1, 2, 3, "");*/
			const int n_verts_c = 10;
			size_t vlist[n_verts_c] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

			VI_VObject *p0 = VI_VObject::create("");

			p0->addVertex(-0.5f*chin_w, 0.0f, 0.0f);
			p0->addVertex(-chin_w, 0.1f*head_h, 0.0f);
			p0->addVertex(-0.4f*head_w, 0.2f*head_h, 0.0f);
			p0->addVertex(-0.5f*head_w, 0.5f*head_h, 0.0f);
			p0->addVertex(-0.4f*head_w, head_h, 0.0f);

			p0->addVertex(0.4f*head_w, head_h, 0.0f);
			p0->addVertex(0.5f*head_w, 0.5f*head_h, 0.0f);
			p0->addVertex(0.4f*head_w, 0.2f*head_h, 0.0f);
			p0->addVertex(chin_w, 0.1f*head_h, 0.0f);
			p0->addVertex(0.5f*chin_w, 0.0f, 0.0f);

			p0->addPolygon(vlist, n_verts_c, "Face");

			VI_VObject *p1 = VI_VObject::create("");

			p1->addVertex(-0.3f*head_w, 0.1f*head_h, 0.5f*head_d);
			p1->addVertex(-0.4f*head_w, 0.2f*head_h, 0.5f*head_d);
			p1->addVertex(-0.6f*head_w, 0.6f*head_h, 0.5f*head_d);
			p1->addVertex(-0.5f*head_w, 0.9f*head_h, 0.5f*head_d);
			p1->addVertex(-0.4f*head_w, 1.2f*head_h, 0.5f*head_d);

			p1->addVertex(0.4f*head_w, 1.2f*head_h, 0.5f*head_d);
			p1->addVertex(0.5f*head_w, 0.9f*head_h, 0.5f*head_d);
			p1->addVertex(0.6f*head_w, 0.6f*head_h, 0.5f*head_d);
			p1->addVertex(0.4f*head_w, 0.2f*head_h, 0.5f*head_d);
			p1->addVertex(0.3f*head_w, 0.1f*head_h, 0.5f*head_d);

			p1->addPolygon(vlist, n_verts_c, "");

			VI_VObject *p2 = VI_VObject::create("");

			p2->addVertex(-0.3f*head_w, 0.1f*head_h, head_d);
			p2->addVertex(-0.4f*head_w, 0.2f*head_h, head_d);
			p2->addVertex(-0.5f*head_w, 0.6f*head_h, head_d);
			p2->addVertex(-0.45f*head_w, 0.9f*head_h, head_d);
			p2->addVertex(-0.4f*head_w, head_h, head_d);

			p2->addVertex(0.4f*head_w, head_h, head_d);
			p2->addVertex(0.45f*head_w, 0.9f*head_h, head_d);
			p2->addVertex(0.5f*head_w, 0.6f*head_h, head_d);
			p2->addVertex(0.4f*head_w, 0.2f*head_h, head_d);
			p2->addVertex(0.3f*head_w, 0.1f*head_h, head_d);

			p2->addPolygon(vlist, n_verts_c, "");

			vector<VI_VObject *> profiles;
			profiles.push_back(p0);
			profiles.push_back(p1);
			profiles.push_back(p2);
			VI_VObject *obj = loft(profiles, true, true);
			delete p0;
			delete p1;
			delete p2;

			// nose
			/*const float nose_h = 0.025f, nose_r = 0.01f;
			VI_VObject *nose = createCone(nose_h, nose_r, 3);
			nose->rotateEuler(0.0f, 90.0f, 0.0f);
			nose->translate(0.0f, 0.5f*head_h, 0.0f);
			obj->combineObject(nose);
			delete nose;*/

			//obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			obj->setTexture(hair_texture, chain_size_u, chain_size_v);

			VI_Face **faces = NULL;
			//int n_faces = obj->getFaces(&faces);
			obj->getFaces(&faces);
			/*for(int i=0;i<10;i++) {
				obj->setPolygonTexture(faces[i], format->skin_texture, false, 0, true, chain_size_u, chain_size_v);
			}*/
			obj->setPolygonTexture(faces[0], format->skin_texture, false, 0, true, chain_size_u, chain_size_v);
			obj->setPolygonTexture(faces[8], format->skin_texture, false, 0, true, chain_size_u, chain_size_v);
			obj->setPolygonTexture(faces[9], format->skin_texture, false, 0, true, chain_size_u, chain_size_v);
			delete [] faces;

			// face texture
			VI_Face *face_face = obj->findFace("Face");
			ASSERT( face_face != NULL );
			/*
			//VI_Texture *face_texture = VI_createTexture("dungeon/data/textures/head_elf.png");
			VI_Texture *face_texture = VI_createTexture("dungeon/data/textures/head_elf2.png");
			//obj->setPolygonTexture(face_face, face_texture, false, 4, true, head_w, -head_h);
			*/
			Vector3D t_o(-0.5f*head_w, head_h, 0.0f);
			Vector3D t_u(head_w, 0.0f, 0.0f);
			Vector3D t_v(0.0f, head_h, 0.0f);
			obj->setPolygonTexture(face_face, format->face_texture, &t_o, &t_u, &t_v);

			// hair
			/*const float hair_h = 0.06f;
			//VI_VObject *hair_top = createCuboid(0.8f*head_w, hair_h, head_d);
			//hair_top->translate(0.0f, head_h+0.5f*hair_h, 0.5f*head_d);
			VI_VObject *hair_top = NULL;
			{
				VI_VObject *p0 = VI_VObject::create("");
				p0->addVertex(0.5f*head_w, head_h, 0.0f);
				p0->addVertex(0.5f*head_w, head_h+hair_h, 0.3f*head_d);
				p0->addVertex(-0.5f*head_w, head_h+hair_h, 0.3f*head_d);
				p0->addVertex(-0.5f*head_w, head_h, 0.0f);
				p0->addPolygon(0, 1, 2, 3, "");

				VI_VObject *p1 = VI_VObject::create("");
				p1->addVertex(1.0f*head_w, 0.3f*head_h, 1.2f*head_d);
				p1->addVertex(1.0f*head_w, head_h+hair_h, 1.2f*head_d);
				p1->addVertex(-1.0f*head_w, head_h+hair_h, 1.2f*head_d);
				p1->addVertex(-1.0f*head_w, 0.3f*head_h, 1.2f*head_d);
				p1->addPolygon(0, 1, 2, 3, "");

				VI_VObject *p2 = VI_VObject::create("");
				p2->addVertex(1.0f*head_w, 0.3f*head_h, 1.2f*head_d+0.1f);
				p2->addVertex(1.0f*head_w, 0.3f*head_h, 1.2f*head_d+0.2f);
				p2->addVertex(-1.0f*head_w, 0.3f*head_h, 1.2f*head_d+0.2f);
				p2->addVertex(-1.0f*head_w, 0.3f*head_h, 1.2f*head_d+0.1f);
				p2->addPolygon(0, 1, 2, 3, "");

				vector<VI_VObject *> profiles;
				profiles.push_back(p0);
				profiles.push_back(p1);
				profiles.push_back(p2);
				hair_top = loft(profiles, true, false);
				delete p0;
				delete p1;
			}
			hair_top->setTexture(hair_texture, 0.1f, 0.1f);
			obj->combineObject(hair_top);
			delete hair_top;*/

			obj->subdivide();
			// round
			deformRound(obj, 0.7f, 1.0f);

			//obj->scale(1.0f, 1.2f, 1.0f);

			// eyes
			/*VI_Texture *eye_texture = VI_createTexture("textures/eye_texture.jpg");
			//const float eye_w = 0.02f, eye_h = 0.01f, eye_off_x = 0.015f;
			//const float eye_w = 0.04f, eye_h = 0.02f, eye_off_x = 0.015f;
			const float eye_w = 0.03f, eye_h = eye_w * 0.67358f, eye_off_x = 0.015f;

			VI_VObject *right_eye = createCuboid(eye_w, eye_h, thin);
			right_eye->translate(0.5f*eye_w + eye_off_x, 0.5f*head_h+nose_h, 0.45f*thin);
			//right_eye->setBaseColori(0.1f, 0.1f, 0.1f);
			//right_eye->setTexture(eye_texture, eye_w, eye_h);
			right_eye->setTexture(eye_texture, false, 2, true, eye_w, eye_h);
			obj->combineObject(right_eye);
			delete right_eye;

			VI_VObject *left_eye = createCuboid(eye_w, eye_h, thin);
			left_eye->translate(-0.5f*eye_w - eye_off_x, 0.5f*head_h+nose_h, 0.45f*thin);
			//left_eye->setBaseColori(0.1f, 0.1f, 0.1f);
			left_eye->setTexture(eye_texture, true, 3, true, eye_w, eye_h);
			obj->combineObject(left_eye);
			delete left_eye;*/

			objects.push_back(obj);
			entity_head = VI_createEntity(obj);
			entity_head->setName("HEAD");
			entity_head->setPosition(0.0f, 0.5f*neck_h - 0.1f*head_h, -0.5f*head_d);
			entity_neck->addChildNode(entity_head);
		}

		//  upper arms
		VI_Entity *entity_u_l_arm = NULL, *entity_u_r_arm = NULL;
		{
			VI_VObject *obj = createPrism(u_arm_r, u_arm_length, 5);
			VI_VObject *shoulder = createSphere(1.1f*u_arm_r, 1);
			shoulder->translate(0.0f, u_arm_length, 0.0f);
			obj->combineObject(shoulder);
			delete shoulder;
			VI_VObject *l_obj = obj;
			VI_VObject *r_obj = obj->clone();

			//l_obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			//r_obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			l_obj->setTexture(mat_texture, chain_size_u, chain_size_v);
			r_obj->setTexture(mat_texture, chain_size_u, chain_size_v);

			l_obj->translate(0.0f, -u_arm_length, 0.0f);
			r_obj->translate(0.0f, -u_arm_length, 0.0f);

			objects.push_back(l_obj);
			objects.push_back(r_obj);

			entity_u_l_arm = VI_createEntity(l_obj);
			entity_u_l_arm->setName("UPPER LEFT ARM");
			entity_u_l_arm->setPosition(-0.45f*format->body_size.x, 0.9f*format->body_size.y, 0.5f*format->body_size.z);
			entity_u_l_arm->rotateLocal(0.0f, 0.0f, -arm_z_angle_c);
			entity_torso->addChildNode(entity_u_l_arm);

			entity_u_r_arm = VI_createEntity(r_obj);
			entity_u_r_arm->setName("UPPER RIGHT ARM");
			entity_u_r_arm->setPosition(0.45f*format->body_size.x, 0.9f*format->body_size.y, 0.5f*format->body_size.z);
			//entity_u_r_arm->rotateLocal(0.0f, 0.0f, 10.0f);
			entity_u_r_arm->rotateLocal(0.0f, 0.0f, arm_z_angle_c);
			entity_torso->addChildNode(entity_u_r_arm);
		}

		//  lower arms
		VI_Entity *entity_l_l_arm = NULL, *entity_l_r_arm = NULL;
		{
			VI_VObject *obj = createPrism(l_arm_r, l_arm_length, 5);
			VI_VObject *l_obj = obj;
			VI_VObject *r_obj = obj->clone();

			//l_obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			//r_obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			l_obj->setTexture(mat_texture, chain_size_u, chain_size_v);
			r_obj->setTexture(mat_texture, chain_size_u, chain_size_v);

			l_obj->translate(0.0f, -l_arm_length, 0.0f);
			r_obj->translate(0.0f, -l_arm_length, 0.0f);

			objects.push_back(l_obj);
			objects.push_back(r_obj);

			entity_l_l_arm = VI_createEntity(l_obj);
			entity_l_l_arm->setName("LOWER LEFT ARM");
			entity_l_l_arm->setPosition(0.0f, -1.0f*u_arm_length, 0.0f);
			entity_u_l_arm->addChildNode(entity_l_l_arm);

			entity_l_r_arm = VI_createEntity(r_obj);
			entity_l_r_arm->setName("LOWER RIGHT ARM");
			entity_l_r_arm->setPosition(0.0f, -1.0f*u_arm_length, 0.0f);
			entity_u_r_arm->addChildNode(entity_l_r_arm);
		}

		// hands
		{
			VI_VObject *p0 = VI_VObject::create("");

			p0->addVertex(0.5f*hand_w, 0.0f, -0.5f*hand_d);
			p0->addVertex(0.0f, -0.5f*hand_h, -0.5f*hand_d);
			p0->addVertex(-0.5f*hand_w, 0.0f, -0.5f*hand_d);
			p0->addVertex(0.0f, 0.5f*hand_h, -0.5f*hand_d);

			p0->addPolygon(0, 1, 2, 3, "");

			VI_VObject *p1 = VI_VObject::create("");

			p1->addVertex(0.5f*hand_w, 0.0f, 0.5f*hand_d);
			p1->addVertex(0.0f, -0.5f*hand_h, 0.5f*hand_d);
			p1->addVertex(-0.5f*hand_w, 0.0f, 0.5f*hand_d);
			p1->addVertex(0.0f, 0.5f*hand_h, 0.5f*hand_d);

			p1->addPolygon(0, 1, 2, 3, "");

			vector<VI_VObject *> profiles;
			profiles.push_back(p0);
			profiles.push_back(p1);
			VI_VObject *obj = loft(profiles, true, true);
			delete p0;
			delete p1;

			obj->setTexture(format->skin_texture, skin_size_u, skin_size_v);

			VI_VObject *l_obj = obj;
			VI_VObject *r_obj = obj->clone();

			objects.push_back(l_obj);
			objects.push_back(r_obj);

			VI_Entity *l_entity = VI_createEntity(l_obj);
			l_entity->setName("lhand");
			l_entity->setPosition(0.0f, -1.0f*l_arm_length, 0.0f);
			entity_l_l_arm->addChildNode(l_entity);

			VI_Entity *r_entity = VI_createEntity(r_obj);
			r_entity->setName("rhand");
			r_entity->setPosition(0.0f, -1.0f*l_arm_length, 0.0f);
			entity_l_r_arm->addChildNode(r_entity);
		}

		//  upper legs
		VI_Entity *entity_u_l_leg = NULL, *entity_u_r_leg = NULL;
		{
			//VI_VObject *obj = createPrism(u_leg_r, u_leg_length, 5);
			VI_VObject *obj = createPrism(format->u_leg_r, u_leg_length, 5);
			VI_VObject *l_obj = obj;
			VI_VObject *r_obj = obj->clone();

			//l_obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			//r_obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			l_obj->setTexture(mat_texture, chain_size_u, chain_size_v);
			r_obj->setTexture(mat_texture, chain_size_u, chain_size_v);

			l_obj->translate(0.0f, -u_leg_length, 0.0f);
			r_obj->translate(0.0f, -u_leg_length, 0.0f);

			objects.push_back(l_obj);
			objects.push_back(r_obj);

			entity_u_l_leg = VI_createEntity(l_obj);
			entity_u_l_leg->setName("UPPER LEFT LEG");
			entity_u_l_leg->setPosition(-0.3f*format->body_size.x, hip_overlap_h, 0.5f*format->body_size.z);
			entity_u_l_leg->rotateLocal(0.0f, 0.0f, -leg_z_angle_c);
			entity_torso->addChildNode(entity_u_l_leg);

			entity_u_r_leg = VI_createEntity(r_obj);
			entity_u_r_leg->setName("UPPER RIGHT LEG");
			entity_u_r_leg->setPosition(0.3f*format->body_size.x, hip_overlap_h, 0.5f*format->body_size.z);
			entity_u_r_leg->rotateLocal(0.0f, 0.0f, leg_z_angle_c);
			entity_torso->addChildNode(entity_u_r_leg);
		}

		//  lower legs
		VI_Entity *entity_l_l_leg = NULL, *entity_l_r_leg = NULL;
		{
			//VI_VObject *obj = createPrism(l_leg_r, l_leg_length, 5);
			VI_VObject *obj = createPrism(format->l_leg_r, l_leg_length, 5);
			VI_VObject *l_obj = obj;
			VI_VObject *r_obj = obj->clone();

			//l_obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			//r_obj->setTexture(format->armour_texture, chain_size_u, chain_size_v);
			l_obj->setTexture(mat_texture, chain_size_u, chain_size_v);
			r_obj->setTexture(mat_texture, chain_size_u, chain_size_v);

			l_obj->translate(0.0f, -l_leg_length, 0.0f);
			r_obj->translate(0.0f, -l_leg_length, 0.0f);

			objects.push_back(l_obj);
			objects.push_back(r_obj);

			entity_l_l_leg = VI_createEntity(l_obj);
			entity_l_l_leg->setName("LOWER LEFT LEG");
			entity_l_l_leg->setPosition(0.0f, -1.0f*u_leg_length, 0.0f);
			entity_l_l_leg->rotateLocal(-lleg_x_angle1_c, 0.0f, 0.0f);
			entity_u_l_leg->addChildNode(entity_l_l_leg);

			entity_l_r_leg = VI_createEntity(r_obj);
			entity_l_r_leg->setName("LOWER RIGHT LEG");
			entity_l_r_leg->setPosition(0.0f, -1.0f*u_leg_length, 0.0f);
			entity_l_r_leg->rotateLocal(-lleg_x_angle1_c, 0.0f, 0.0f);
			entity_u_r_leg->addChildNode(entity_l_r_leg);
		}

		// feet
		{
			VI_VObject *p0 = VI_VObject::create("");

			p0->addVertex(0.5f*foot_w, 0.0f, -foot_d);
			p0->addVertex(-0.5f*foot_w, 0.0f, -foot_d);
			p0->addVertex(-0.5f*foot_w, 0.5f*foot_h, -foot_d);
			p0->addVertex(0.5f*foot_w, 0.5f*foot_h, -foot_d);

			p0->addPolygon(0, 1, 2, 3, "");

			VI_VObject *p1 = VI_VObject::create("");

			p1->addVertex(0.5f*foot_w, 0.0f, 0.0f);
			p1->addVertex(-0.5f*foot_w, 0.0f, 0.0f);
			p1->addVertex(-0.5f*foot_w, foot_h, 0.0f);
			p1->addVertex(0.5f*foot_w, foot_h, 0.0f);

			p1->addPolygon(0, 1, 2, 3, "");

			vector<VI_VObject *> profiles;
			profiles.push_back(p0);
			profiles.push_back(p1);
			VI_VObject *obj = loft(profiles, true, true);
			delete p0;
			delete p1;

			//obj->translate(0.0f, -foot_h, 0.5f*l_leg_r);
			obj->translate(0.0f, -foot_h, 0.5f*format->l_leg_r);
			//obj->setBaseColori(32, 32, 32);
			obj->setTexture(format->foot_texture, foot_size_u, foot_size_v);

			VI_VObject *l_obj = obj;
			VI_VObject *r_obj = obj->clone();

			objects.push_back(l_obj);
			objects.push_back(r_obj);

			VI_Entity *l_entity = VI_createEntity(l_obj);
			l_entity->setName("LEFT FOOT");
			l_entity->setPosition(0.0f, -1.0f*l_leg_length, 0.0f);
			entity_l_l_leg->addChildNode(l_entity);

			VI_Entity *r_entity = VI_createEntity(r_obj);
			r_entity->setName("RIGHT FOOT");
			r_entity->setPosition(0.0f, -1.0f*l_leg_length, 0.0f);
			entity_l_r_leg->addChildNode(r_entity);
		}

		size_t n_total_polys = 0, n_total_verts = 0;
		for(vector<VI_VObject *>::iterator iter = objects.begin(); iter != objects.end(); ++iter) {
			VI_VObject *obj = *iter;
			//obj->setShading(VI_Face::FLAT);
			n_total_polys += obj->get_n_polys();
			n_total_verts += obj->get_n_vertices();
		}
		LOG("Humanoid: %d objects, %d polys, %d verts\n", objects.size(), n_total_polys, n_total_verts);

		VI_HAnimationInfo *animinfo = NULL;
		Vector3D pos(0.0f, 0.0f, 0.0f);
		Vector3D norm(0.0f, 1.0f, 0.0f);

		// so that we are at the right height, even for no animation (needed for Golem Scenery item)
		float low = 0.0f, high = 0.0f;
		if( dummy->getExtent(&low, &high, pos, norm, true, true) ) {
			Vector3D new_pos = pos - norm * low;
			dummy->setPosition(new_pos);
		}
		dummy->setDefaultHAnimationPosition(true); // not to root, as there we don't want to override the character's position/orientation!

		{
			// idle animation
			char name[] = "pause2";
			//char name[] = "pause2 T";

			const float head_y_angle_c = 5.0f;
			const float arm_x_angle_c = 0.5f;

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 2.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(0.5f, Rotation3D(0.0f, -head_y_angle_c, 0.0f));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(1.5f, Rotation3D(0.0f, head_y_angle_c, 0.0f));
			animinfo->addRotationKey(2.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			entity_head->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 2.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, -arm_z_angle_c));
			animinfo->addRotationKey(0.5f, Rotation3D(-arm_x_angle_c, 0.0f, -arm_z_angle_c));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, -arm_z_angle_c));
			animinfo->addRotationKey(1.5f, Rotation3D(arm_x_angle_c, 0.0f, -arm_z_angle_c));
			animinfo->addRotationKey(2.0f, Rotation3D(0.0f, 0.0f, -arm_z_angle_c));
			entity_u_l_arm->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 2.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			animinfo->addRotationKey(0.5f, Rotation3D(-arm_x_angle_c, 0.0f, arm_z_angle_c));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			animinfo->addRotationKey(1.5f, Rotation3D(arm_x_angle_c, 0.0f, arm_z_angle_c));
			animinfo->addRotationKey(2.0f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			entity_u_r_arm->addHAnimationInfo(animinfo);

			setPositionKeys(dummy, name, pos, norm, 2.0f);
		}

		{
			// walk animation
			char name[] = "walk";
			const float leg_z_angle2_c = 0.5f*leg_z_angle_c;

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, -arm_z_angle_c));
			animinfo->addRotationKey(0.25f, Rotation3D(-10.0f, 0.0f, -arm_z_angle_c));
			animinfo->addRotationKey(0.5f, Rotation3D(0.0f, 0.0f, -arm_z_angle_c));
			animinfo->addRotationKey(0.75f, Rotation3D(10.0f, 0.0f, -arm_z_angle_c));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, -arm_z_angle_c));
			entity_u_l_arm->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			animinfo->addRotationKey(0.25f, Rotation3D(10.0f, 0.0f, arm_z_angle_c));
			animinfo->addRotationKey(0.5f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			animinfo->addRotationKey(0.75f, Rotation3D(-10.0f, 0.0f, arm_z_angle_c));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			entity_u_r_arm->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, -leg_z_angle2_c));
			animinfo->addRotationKey(0.25f, Rotation3D(leg_x_angle_c, 0.0f, -leg_z_angle2_c)); // forwards
			animinfo->addRotationKey(0.5f, Rotation3D(0.0f, 0.0f, -leg_z_angle2_c));
			animinfo->addRotationKey(0.75f, Rotation3D(-leg_x_angle_bwd_c, 0.0f, -leg_z_angle2_c)); // backwards
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, -leg_z_angle2_c));
			entity_u_l_leg->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, leg_z_angle2_c));
			animinfo->addRotationKey(0.25f, Rotation3D(-leg_x_angle_bwd_c, 0.0f, leg_z_angle2_c)); // backwards
			animinfo->addRotationKey(0.5f, Rotation3D(0.0f, 0.0f, leg_z_angle2_c));
			animinfo->addRotationKey(0.75f, Rotation3D(leg_x_angle_c, 0.0f, leg_z_angle2_c)); // forwards
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, leg_z_angle2_c));
			entity_u_r_leg->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(-lleg_x_angle1_c, 0.0f, 0.0f));
			animinfo->addRotationKey(0.25f, Rotation3D(-lleg_x_angle2_c, 0.0f, 0.0f));
			animinfo->addRotationKey(0.5f, Rotation3D(-lleg_x_angle1_c, 0.0f, 0.0f));
			animinfo->addRotationKey(0.75f, Rotation3D(-lleg_x_angle1_c, 0.0f, 0.0f));
			animinfo->addRotationKey(1.0f, Rotation3D(-lleg_x_angle1_c, 0.0f, 0.0f));
			entity_l_l_leg->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(-lleg_x_angle1_c, 0.0f, 0.0f));
			animinfo->addRotationKey(0.25f, Rotation3D(-lleg_x_angle1_c, 0.0f, 0.0f));
			animinfo->addRotationKey(0.5f, Rotation3D(-lleg_x_angle1_c, 0.0f, 0.0f));
			animinfo->addRotationKey(0.75f, Rotation3D(-lleg_x_angle2_c, 0.0f, 0.0f));
			animinfo->addRotationKey(1.0f, Rotation3D(-lleg_x_angle1_c, 0.0f, 0.0f));
			entity_l_r_leg->addHAnimationInfo(animinfo);

			setPositionKeys(dummy, name, pos, norm, 1.0f);
		}

		{
			// attack animation
			char name[] = "1hslashl";
			//char name[] = "pause2";

			const float arm_y_angle_c = 60.0f;
			const float arm_z_angle2_c = 5.0f*arm_z_angle_c;
			const float half_time_c = 0.3f;

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			animinfo->addRotationKey(half_time_c, Rotation3D(60.0f, arm_y_angle_c, arm_z_angle2_c));
			animinfo->addRotationKey(0.5f*(half_time_c + 1.0f), Rotation3D(0.0f, arm_y_angle_c, arm_z_angle2_c));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			entity_u_r_arm->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(half_time_c, Rotation3D(90.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(0.5f*(half_time_c + 1.0f), Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			entity_l_r_arm->addHAnimationInfo(animinfo);

			setPositionKeys(dummy, name, pos, norm, 1.0f);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			root->addHAnimationInfo(animinfo); // so we know when the effect has finished
		}

		{
			// bow animation
			char name[] = "bowshot";

			const float half_time_c = 0.3f;
			const float hold_time_c = 0.1f;
			const float back_angle_c = 85.0f;

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, -arm_z_angle_c));
			animinfo->addRotationKey(half_time_c, Rotation3D(90.0f, -30.0f, 0.0f));
			animinfo->addRotationKey(half_time_c+hold_time_c, Rotation3D(90.0f, -30.0f, 0.0f));
			//animinfo->addRotationKey(0.5f*(half_time_c + 1.0f), Rotation3D(0.0f, arm_y_angle_c, arm_z_angle_c));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, -arm_z_angle_c));
			entity_u_l_arm->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			//animinfo->addRotationKey(half_time_c, Rotation3D(-60.0f, 0.0f, 2.0f*arm_z_angle_c));
			animinfo->addRotationKey(half_time_c, Rotation3D(0.0f, -back_angle_c, 90.0f));
			animinfo->addRotationKey(half_time_c+hold_time_c, Rotation3D(0.0f, -back_angle_c, 90.0f));
			//animinfo->addRotationKey(0.5f*(half_time_c + 1.0f), Rotation3D(0.0f, arm_y_angle_c, arm_z_angle_c));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, arm_z_angle_c));
			entity_u_r_arm->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(half_time_c, Rotation3D(2.0f*back_angle_c, 0.0f, 0.0f));
			animinfo->addRotationKey(half_time_c+hold_time_c, Rotation3D(2.0f*back_angle_c, 0.0f, 0.0f));
			//animinfo->addRotationKey(0.5f*(half_time_c + 1.0f), Rotation3D(0.0f, arm_y_angle_c, arm_z_angle_c));
			animinfo->addRotationKey(1.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			entity_l_r_arm->addHAnimationInfo(animinfo);
			setPositionKeys(dummy, name, pos, norm, 1.0f);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, 1.0f);
			root->addHAnimationInfo(animinfo); // so we know when the effect has finished
		}

		{
			// death animation
			char name[] = "kdbck";

			const float leg_bend_c = 60.0f;
			const float length_c = 0.5f;

			animinfo = VI_HAnimationInfo::create(name, 0.0f, length_c);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(length_c, Rotation3D(90.0f, 0.0f, 0.0f));
			entity_torso->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, length_c);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(length_c, Rotation3D(leg_bend_c, 0.0f, 0.0f));
			entity_u_l_leg->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, length_c);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(length_c, Rotation3D(-2.0f*leg_bend_c, 0.0f, 0.0f));
			entity_l_l_leg->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, length_c);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(length_c, Rotation3D(leg_bend_c, 0.0f, 0.0f));
			entity_u_r_leg->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, length_c);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(length_c, Rotation3D(-2.0f*leg_bend_c, 0.0f, 0.0f));
			entity_l_r_leg->addHAnimationInfo(animinfo);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, length_c);
			animinfo->addRotationKey(0.0f, Rotation3D(0.0f, 0.0f, 0.0f));
			animinfo->addRotationKey(length_c, Rotation3D(0.0f, 30.0f, 0.0f));
			entity_head->addHAnimationInfo(animinfo);

			setPositionKeys(dummy, name, pos, norm, length_c);

			animinfo = VI_HAnimationInfo::create(name, 0.0f, length_c);
			root->addHAnimationInfo(animinfo); // so we know when the effect has finished
		}

		root->setHAnimation(NULL, true); // reset to no animation

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		for(vector<VI_VObject *>::iterator iter = objects.begin(); iter != objects.end(); ++iter) {
			VI_VObject *obj = *iter;
			VisionObject *vo = dynamic_cast<VisionObject *>(obj);
			vo->prepare();
		}
	}

	return root;
}
#endif

struct deformAttractorData {
	Vector3D centre;
	float radius;
	float scale;
};

Vector3D deformAttractorFunc(Vector3D pos, void *ext_data) {
	deformAttractorData *data = (deformAttractorData *)ext_data;
	Vector3D diff = pos - data->centre;
	if( diff.magnitude() <= data->radius ) {
		diff *= data->scale;
		pos = data->centre + diff;
	}
	return pos;
}

void VI_Modeller::deformAttractor(VI_Object *obj, Vector3D centre, float radius, float scale) {
	deformAttractorData data;
	data.centre = centre;
	data.radius = radius;
	data.scale = scale;
	obj->deform(deformAttractorFunc, &data);
}

struct deformPokeData {
	Vector3D pos;
	Vector3D dir;
	float radius;
	float dist;
};

Vector3D deformPokeFunc(Vector3D pos, void *ext_data) {
	deformPokeData *data = (deformPokeData *)ext_data;
	Vector3D diff = pos - data->pos;
	diff.perpComp(data->dir);

	if( diff.magnitude() <= data->radius ) {
		pos += data->dir * data->dist;
	}
	return pos;
}

void VI_Modeller::deformPoke(VI_Object *obj, Vector3D pos, Vector3D dir, float radius, float dist) {
	dir.normalise();
	deformPokeData data;
	data.pos = pos;
	data.dir = dir;
	data.radius = radius;
	data.dist = dist;
	obj->deform(deformPokeFunc, &data);
}

struct deformRoundData {
	Vector3D centre;
	float radius;
	float frac;
};

Vector3D deformRoundFunc(Vector3D pos, void *ext_data) {
	deformRoundData *data = (deformRoundData *)ext_data;
	Vector3D radial = pos - data->centre;
	ASSERT( radial.magnitude() > 0 );
	Vector3D norm = radial;
	norm.normalise();
	Vector3D radius = norm * data->radius;
	Vector3D diff = radius - radial;
	radial += diff * data->frac;
	pos = data->centre + radial;
	return pos;
}

void VI_Modeller::deformRound(VI_VObject *obj, float frac, float scale) {
	Vector3D centre(0.0f, 0.0f, 0.0f);
	for(size_t i=0;i<obj->get_n_vertices();i++) {
		centre += obj->getVertexPos(i);
	}
	centre /= (float)obj->get_n_vertices();
	float radius = 0.0f;
	for(size_t i=0;i<obj->get_n_vertices();i++) {
		Vector3D diff = obj->getVertexPos(i) - centre;
		radius += diff.magnitude();
	}
	radius /= obj->get_n_vertices();
	radius *= scale;

	deformRoundData data;
	data.centre = centre;
	data.radius = radius;
	data.frac = frac;

	//deformRoundFunc(centre + Vector3D(0.0f, 2*radius, 0.0f), &data);
	obj->deform(deformRoundFunc, &data);
}
