#pragma once

#include <vector>
using std::vector;
#include <set>
using std::set;
#include <string>
using std::string;
#include <queue>
using std::queue;

#ifndef _WIN32
	#define stricmp strcasecmp
	#define strnicmp strncasecmp
#endif

#include "VisionIface.h"
#include "VisionShared.h"

#ifndef LOG
#define LOG V_log
#endif
/*inline void V_log(const char *text, ...) {
	logText(text, ...);
}*/

#ifdef _DEBUG

#include <ctime>

extern int V__tracelevel;

#define V__ENTER                                         \
	V__tracelevel++;                                     \
	int __time_start = clock();

#define V__EXIT                                          \
	V__tracelevel--;                                     \
	int __time_diff = clock() - __time_start;            \
	{                                                    \
		V__logtime(__FILE__, __FUNCTION__, __time_diff); \
	}
/*	if( __time_diff > 0 )                                \*/

#else

#define V__ENTER
#define V__EXIT

#endif

void V__logtime(const char *file, const char *func, int time);

class VisionObject : public virtual VI_VisionObject {
protected:
	string name;
	VisionObject *owner;
	set<VisionObject *> owned_vision_objects;
public:
	//bool kill_later;
	V_TAG_t tag;
	size_t creation_id; // guaranteed to never be repeated during lifetime of Vision engine; ordered by time of creation
	int deleteLevel;
	//bool owned; // if true, this object shouldn't be freed when flushing memory, as it's owned by another VisionObject

	VisionObject();
	virtual ~VisionObject();
	//virtual void update(float time) {}
	virtual V_CLASS_t getClass() const=0;
	virtual bool isSubClass(V_CLASS_t v_class)=0;
	/*unsigned int getNameLength() const {
		return this->name.length();
	}*/
	virtual size_t memUsage()=0;
	virtual bool check() const;
	virtual void prepare() {} // used by the ObjectData class and its subclasses
	/*virtual void kill() {
	this->kill_later = true;
	}*/

	// interface
	virtual V_TAG_t getTag() const {
		return tag;
	}
	virtual void setName(const char *name);
	virtual const char *getName() const;
	virtual void addChild(VI_VisionObject *child) {
		VisionObject *d_child = dynamic_cast<VisionObject *>(child); // cast to the internal representation
		if( d_child->owner != NULL ) {
			d_child->detachFromOwner();
		}
		owned_vision_objects.insert(d_child);
		d_child->owner = this;
	}
	virtual int getNChildObjects() const {
		return owned_vision_objects.size();
	}
	virtual void detachFromOwner();
	virtual bool removeOwnedObject(VI_VisionObject *child);
	virtual void removeAllOwnedObjects();
	virtual void deleteAllOwnedObjects();
};

//typedef vector<World *> VWorldsCollection;
//typedef set<World *> VWorldsCollection;
//typedef VWorldsCollection::iterator VWorldsIter;

typedef vector<VisionObject *> VSafedeleteCollection;
typedef VSafedeleteCollection::iterator VSafedeleteIter;

class Vision {
	static bool v_initialised;

	static int lock_level;

	static vector<VisionObject *> tags;
	static size_t next_creation_id;
	static VSafedeleteCollection safedeletequeue;
	static bool any_kills;

	static queue<void *> message_queue;

	static int default_persistence;

	// game_time_ms is the accumulated time in ms, as passed to Vision::update() calls. Is should
	// be used instead of calls to clock(), to keep everything consistent, and to allow the
	// application to do things such as pausing, or speeding up or slowing down.
	static int game_time_ms;
	// this is the time passed to the last call to Vision::update(), converted to milliseconds.
	static int game_time_last_frame_ms;

	static void openLogFile(const char *application_name);
public:
	//static VisionException *error;

	static void initialise(const char *application_name);
	static const char *getApplicationName();
	static string getApplicationDataFilename(const char *folder, const char *name);
	static char *getApplicationFilename(const char *name);
	static void flushAll();
	static void flush(int deleteLevel);
	static size_t getNextCreationId() {
		return next_creation_id;
	}
	static void flushSinceCreationId(size_t creation_id);
	static void lock();
	static void unlock(bool reprepare);
	static void unlock() {
		unlock(true);
	}
	static bool isLocked();
	static void cleanup();
	static void addTag(VisionObject *ptr);
	static VisionObject *ptrFromTag(V_TAG_t tag);
	static size_t getNTags();
	static size_t getNTagsInUse();
	static void removeTag(V_TAG_t tag);
	static void setError(VisionException *ve);
	static void update(int time);
	static int getGameTimeMS() {
		return game_time_ms;
	}
	static int VI_getGameTimeLastFrameMS() {
		return game_time_last_frame_ms;
	}
	static void safeDelete(VisionObject *vo) {
		safedeletequeue.push_back(vo);
	}
	static void registerKill() {
		any_kills = true;
	}
	static void pushMessage(void *message) {
		message_queue.push(message);
	}
	static bool hasMessages() {
		return !message_queue.empty();
	}
	static void *peekMessage() {
		if( message_queue.empty() ) {
			Vision::setError(new VisionException(NULL, VisionException::V_NO_MESSAGES, "Message queue is empty"));
		}
		void *message = message_queue.front();
		return message;
	}
	static void *popMessage() {
		if( message_queue.empty() ) {
			Vision::setError(new VisionException(NULL, VisionException::V_NO_MESSAGES, "Message queue is empty"));
		}
		void *message = message_queue.front();
		message_queue.pop();
		return message;
	}
	static void setDefaultPersistence(int level) {
		LOG("Vision::setDefaultPersistence(%d)\n", level);
		default_persistence = level;
	}
	static int getDefaultPersistence() {
		return default_persistence;
	}
	static void check();
	static void setPersistenceAll(int level);
	static void debugMemUsage();
};

void createThreads(int n_threads, VI_ThreadFunction **function, void **data);

/** A cut down version of VisionLocker. This doesn't do any flushing, and calls unlock with
  * the argument reprepare set to false, to indicate not to do any preparing of objects when
  * unlocking. See VisionLocker, and Vision::unlock(), for more details.
  * This class shouldn't be available to external callers - only internal callers should be
  * trusted to call unlock with reprepare set to false.
 */
class SimpleVisionLocker {
public:
	SimpleVisionLocker() {
		VI_log("SimpleVisionLocker constructor locking\n");
		Vision::lock();
	}
	~SimpleVisionLocker() {
		VI_log("SimpleVisionLocker destructor now unlocking\n");
		Vision::unlock(false);
	}
};
