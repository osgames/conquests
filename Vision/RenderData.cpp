//---------------------------------------------------------------------------
#include <cstring>
#include <ctime>

#include <algorithm>
using std::find;

#include "RenderData.h"
#include "Misc.h"
#include "GraphicsEnvironment.h"
#include "GraphicsEngine.h"
#include "Renderer.h"

//---------------------------------------------------------------------------

#define SHADOW_ALGORITHM_NEW

//#define TEXTURE_UNIT_SECONDARY (1) // used for various effects
//static int TEXTURE_UNIT_SPLATS[MAX_SPLATS] = { 1, 2, 3, 4 };

// n.b., we can use both of these on unit 1, as in the shaders, bumpmaps use the texture coords of
// unit 0, whilst TEXTURE_UNIT_POS1 only uses coordinates, and don't involve passing a texture map
//#define TEXTURE_UNIT_BUMPMAP (1)
//#define TEXTURE_UNIT_POS1 (1)

//#define TEXTURE_UNIT_TANGENTS (1)

//#define TEXTURE_UNIT_0 (0)

RenderData::RenderData() {
	this->n_textures = 0;
	for(int i=0;i<MAX_TEXTURES;i++) {
		this->textures[i] = NULL;
	}
	this->n_frames = 0;
	//this->texture = NULL;
	//this->secondary_texture = NULL;
	this->need_tangents = false;
	this->tangent_uv_channel = -1;
	this->uses_hardware_animation = false;
	this->hardware_animation_channel = -1;
	//this->bump_texture = NULL;
	/*this->n_splats = 0;
	for(int i=0;i<MAX_SPLATS;i++)
		this->splat_textures[i] = NULL;*/
	//this->splat_detail_scale = 1.0f;
	this->n_vertices = 0;
	//this->using_vbos = false;
	this->need_shadows = false;
	//this->vbo_shadow_vertices = 0;
	this->vao_shadow_vertices = NULL;
	/*this->va_shadow_vertices = NULL;
	this->va_shadow_normals = NULL;*/
	this->vao_shadow_indices = NULL;
	//this->va_shadow_indices = NULL;
	//this->vbo_vertices = 0;
	this->vao_vertices = NULL;
	//this->vbo_vertices_stream = 0;
	this->vao_vertices_stream = NULL;
	//this->vbo_normals = 0;
	this->vao_normals = NULL;
	this->vao_tangents = NULL;
	//this->vbo_texcoords = 0;
	this->vao_texcoords = NULL;
	//this->vbo_texcoords_secondary = 0;
	//this->vbo_colors = 0;
	this->vao_colors = NULL;
	//this->vbo_indices = 0;
	this->vao_indices = NULL;
	/*this->va_colors = NULL;
	this->va_normals = NULL;
	this->va_tangents = NULL;
	this->va_vertices = NULL;
	this->va_vertices_stream = NULL;
	this->va_texcoords = NULL;*/
	//this->va_texcoords_secondary = NULL;
	//this->vx_index = NULL;
	//this->va_indices = NULL;
	//this->n_indices = 0;
	this->n_renderlength = 0;
	this->last_frame = -1;
	this->last_frac = -1.0;
	this->solid = true;
	this->blending = false;
	//this->blend_glow = true;
	this->blend_type = V_BLENDTYPE_FADE_SRC;
	//this->alpha_channel = false;
	//this->splatting = false;
	/*this->cols[0] = 255;
	this->cols[1] = 255;
	this->cols[2] = 255;
	this->cols[3] = 255;*/
	this->material_specular[0] = 0;
	this->material_specular[1] = 0;
	this->material_specular[2] = 0;
	this->material_specular[3] = 0;
	/*this->faces_planes = NULL;
	this->faces_adj_faces = NULL;
	this->faces_boundary = NULL;
	this->edges_skip = NULL;
	this->visible_polys = NULL;
	this->shadow_edges = NULL;
	this->shadow_vertices_used = NULL;*/
	this->shadow_closed = false;
}

void RenderData::setRenderlength(bool allocate_indices, int n_renderlength) {
	if( n_renderlength < 0 ) {
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Renderlength is negative"));
	}
	else if( n_renderlength >= 65536 ) {
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Renderlength too large"));
	}
	this->n_renderlength = static_cast<unsigned short>(n_renderlength);
	if( allocate_indices || this->va_indices.size() > 0 ) {
		this->va_indices.resize(n_renderlength);
	}
}

bool RenderData::compatible(const RenderData *that) const {
	bool ok = //this->alpha_channel == that->alpha_channel &&
		this->blend_type == that->blend_type &&
		this->blending == that->blending &&
		this->need_tangents == that->need_tangents &&
		this->tangent_uv_channel == that->tangent_uv_channel &&
		this->uses_hardware_animation == that->uses_hardware_animation &&
		this->hardware_animation_channel == that->hardware_animation_channel &&
		//this->bump_texture == that->bump_texture &&
		this->n_textures == that->n_textures &&
		this->n_frames == that->n_frames &&
		//this->n_splats == that->n_splats &&
		this->need_shadows == that->need_shadows &&
		//this->texture == that->texture &&
		//this->secondary_texture == that->secondary_texture &&
		this->solid == that->solid;
	/*for(int i=0;i<4 && ok;i++)
		ok = this->cols[i] == that->cols[i];*/
	for(int i=0;i<4 && ok;i++)
		ok = this->material_specular[i] == that->material_specular[i];
	/*for(int i=0;i<MAX_SPLATS && ok;i++)
		ok = this->splat_textures[i] == that->splat_textures[i];*/
	for(int i=0;i<n_textures && ok;i++)
		ok = this->textures[i] == that->textures[i];
	/*if( ok && this->n_splats > 0 ) {
		ok = V_TOL_equal(this->splat_detail_scale, that->splat_detail_scale, V_TOL_LINEAR);
	}*/
	return ok;
}

void RenderData::freeVBOs() {
	delete vao_vertices;
	vao_vertices = NULL;
	delete vao_vertices_stream;
	vao_vertices_stream = NULL;
	delete vao_normals;
	vao_normals = NULL;
	delete vao_tangents;
	vao_tangents = NULL;
	delete vao_texcoords;
	vao_texcoords = NULL;
	delete vao_colors;
	vao_colors = NULL;
	delete vao_indices;
	vao_indices = NULL;
	/*if( RendererInfo::vertexBufferObjects && using_vbos ) {
		using_vbos = false;
	}*/
}

void RenderData::free() {
	freeVBOs();
	freeShadowData();
	/*if(va_colors != NULL )
		delete [] va_colors;
	va_colors = NULL;
	if(va_normals != NULL )
		delete [] va_normals;
	va_normals = NULL;
	if(va_tangents != NULL )
		delete [] va_tangents;
	va_tangents = NULL;
	if(va_vertices != NULL )
		delete [] va_vertices;
	va_vertices = NULL;
	if(va_vertices_stream != NULL )
		delete [] va_vertices_stream;
	va_vertices_stream = NULL;
	if(va_texcoords != NULL )
		delete [] va_texcoords;
	va_texcoords = NULL;*/
	/*if(va_texcoords_secondary != NULL )
	delete [] va_texcoords_secondary;
	va_texcoords_secondary = NULL;*/
	/*if(vx_index != NULL )
	delete [] vx_index;
	vx_index = NULL;*/
	/*if(va_indices != NULL )
		delete [] va_indices;
	va_indices = NULL;*/
	va_vertices.clear();
	va_vertices_stream.clear();
	va_normals.clear();
	va_tangents.clear();
	va_colors.clear();
	va_indices.clear();
	this->last_frame = -1;
	this->last_frac = -1.0;
	clearHelpData();
}

size_t RenderData::memUsage() {
	size_t size = sizeof(this);
	// TODO: shadow vertex arrays
	/*if( va_colors != NULL )
		size += (alpha_channel ? 4 : 3) * n_vertices * sizeof(unsigned char);
	if( va_normals != NULL )
		size += 3 * n_frames * n_vertices * sizeof(float);
	if( va_tangents != NULL )
		size += 3 * n_frames * n_vertices * sizeof(float);
	if( va_vertices != NULL )
		size += 3 * n_frames * n_vertices * sizeof(float);
	if( va_vertices_stream != NULL )
		size += 3 * n_vertices * sizeof(float);
	if( va_texcoords != NULL )
		size += 2 * n_vertices * sizeof(float);*/
	/*if( va_texcoords_secondary != NULL )
	size += 2 * n_vertices * sizeof(float);*/
	size += va_vertices.size() * sizeof(float);
	size += va_vertices_stream.size() * sizeof(float);
	size += va_normals.size() * sizeof(float);
	size += va_tangents.size() * sizeof(float);
	size += va_colors.size() * sizeof(unsigned char);
	size += va_indices.size() * sizeof(unsigned short);
	/*if( vx_index != NULL ) {
	// ???
	}*/
	/*if( va_indices != NULL ) {
		// ???
	}*/
	return size;
}

void RenderData::initVertexArrays(int n_frames,size_t n_vertices,/*bool colors,bool alpha_channel,*/bool normals/*,bool texcoords*//*,bool texcoords_secondary = false*/) {
	if( n_frames == 0 ) {
		LOG("RenderData::initVertexArrays: n_frames %d not greater than zero\n", n_frames);
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "invalid n_frames"));
	}
	if( n_vertices == 0 ) {
		LOG("RenderData::initVertexArrays: n_vertices %d not greater than zero\n", n_vertices);
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "invalid n_vertices"));
	}
	free();
	//alpha_channel = true; // force to true - tests show that on modern hardware we actually get better performance with alpha_channels
	this->n_frames = n_frames;
	this->n_vertices = n_vertices;
	size_t n_elements = n_frames * n_vertices;
	if( normals ) {
		this->va_normals.resize(3 * n_elements);
		// always initialise, just to be safe
		for(size_t i=0;i<3*n_elements;i++) {
			this->va_normals[i] = 0.0f;
		}
	}
	this->va_vertices.resize(3 * n_elements);
	// always initialise, just to be safe
	for(size_t i=0;i<3*n_elements;i++) {
		this->va_vertices[i] = 0.0f;
	}
	this->va_vertices_stream.resize(3 * n_vertices);
	// always initialise, just to be safe
	for(size_t i=0;i<3*n_vertices;i++) {
		this->va_vertices_stream[i] = 0.0f;
	}
	//this->alpha_channel = alpha_channel;
	this->va_colors.resize(getDim() * n_vertices);
	// always create a color array, so that the data is present for the shaders (otherwise causes problems depending on graphics card type and/or API)
	// always initialise, just to be safe
	for(size_t i=0;i<getDim() * n_vertices;i++) {
		this->va_colors[i] = 255;
	}
	// always create a texture coord array, so that the data is present for the shaders (otherwise causes problems depending on graphics card type and/or API)
	this->va_texcoords.resize(2 * n_vertices);
	// always initialise, just to be safe
	for(size_t i=0;i<2*n_vertices;i++) {
		this->va_texcoords[i] = 0.0f;
	}
}


struct RD_Edge {
	size_t f0, f1;
	size_t v0, v1, v2;
	bool merge;
	RD_Edge() {
		f0 = f1 = -1;
		v0 = v1 = v2 = -1;
		merge = false;
	}
};

void RenderData::simplify() {
	return; // not yet implemented
	unsigned short n_polys = this->n_renderlength / 3;
	bool need_va_indices = true;
	//if( this->va_indices == NULL ) {
	if( this->va_indices.size() == 0 ) {
		// TODO: fix needing to do this

		if( n_renderlength >= 65536 ) {
			Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Renderlength overflow"));
		}
		need_va_indices = false;
		//this->va_indices = new unsigned short[n_renderlength];
		this->va_indices.resize(n_renderlength);
		for(unsigned short i=0;i<n_renderlength;i++)
			this->va_indices[i] = i;
	}

#if 0
	if( false ) {
		//{
		//if( this->va_indices != NULL && this->mode == GL_TRIANGLES ) {
		LOG("attempt LOD geomipmapping\n");
		//Vector *edges = new Vector();
		//vector<RD_Edge *> *edges = new vector<RD_Edge *>();
		vector<RD_Edge> edges;
		for(size_t i=0;i<n_polys;i++) {
			unsigned short f0_vxs[3] = {0, 0, 0};
			for(int j=0;j<3;j++)
				f0_vxs[j] = this->va_indices[i*3 + j];
			for(size_t i1=0;i1<i;i1++) {
				unsigned short f1_vxs[3] = {0, 0, 0};
				for(int j1=0;j1<3;j1++)
					f1_vxs[j1] = this->va_indices[i1*3 + j1];
				int common[3] = {0, 0, 0}, common1[3] = {0, 0, 0};
				int n_common = 0;
				for(int j=0;j<3;j++) {
					for(int j1=0;j1<3;j1++) {
						if( f0_vxs[j] == f1_vxs[j1] ) {
							common[n_common] = j;
							common1[n_common] = j1;
							n_common++;
						}
					}
				}
				if( n_common == 2 ) {
					// TODO: check winding order
					// found a common edge!
					//RD_Edge *edge = new RD_Edge();
					RD_Edge edge;
					edge.f0 = i;
					edge.f1 = i1;
					edge.v0 = f0_vxs[ common[0] ];
					edge.v1 = f0_vxs[ common[1] ];
					edges.push_back(edge);
				}
			}
		}

		bool *tweaked_vx = new bool[n_vertices];
		for(size_t i=0;i<n_vertices;i++)
			tweaked_vx[i] = false;
		unsigned short new_n_polys = n_polys;
		bool *del_face = new bool[n_polys];
		for(size_t i=0;i<n_polys;i++)
			del_face[i] = false;
		//Vector *merged_faces = new Vector();
		vector<size_t> *merged_faces = new vector<size_t>();
		for(size_t i=0;i<n_polys;i++) {
			//if( merged_faces->contains((void *)i) ) {
			if( contains_vec(merged_faces, i) ) {
				// can't merge
				continue;
			}
			bool tweaked = false;
			for(int j=0;j<3 && !tweaked;j++) {
				int vx = this->va_indices[i*3+j];
				if( tweaked_vx[vx] )
					tweaked = true;
			}
			if( tweaked ) {
				// can't merge
				continue;
			}
			LOG("try to merge face %d\n", i);
			const RD_Edge *eds[3] = {NULL, NULL, NULL};
			size_t adj_faces[3] = {0, 0, };
			int n_edges = 0;
			for(size_t j=0;j<edges.size();j++) {
				//RD_Edge *edge = static_cast<RD_Edge *>(edges->get(j));
				const RD_Edge *edge = &edges.at(i);
				if( edge->f0 == i ) {
					LOG("    edge %d between %d and %d\n", j, edge->f0, edge->f1);
					eds[n_edges] = edge;
					adj_faces[n_edges] = edge->f1;
					n_edges++;
				}
				else if( edge->f1 == i ) {
					LOG("    edge %d between %d and %d\n", j, edge->f0, edge->f1);
					eds[n_edges] = edge;
					adj_faces[n_edges] = edge->f0;
					n_edges++;
				}
			}
			bool ok = n_edges == 3;
			for(int j=0;j<3 && ok;j++) {
				//if( merged_faces->contains((void *)adj_faces[j]) )
				if( contains_vec(merged_faces, adj_faces[j]) )
					ok = false; // can't merge
			}
			if( ok ) {
				// compare normals of faces
				Vector3D pvecs[3];
				for(int k=0;k<3;k++) {
					int vx = this->va_indices[i*3+k];
					pvecs[k].x = this->va_vertices[3*n_vertices*0 + 3*vx + 0];
					pvecs[k].y = this->va_vertices[3*n_vertices*0 + 3*vx + 1];
					pvecs[k].z = this->va_vertices[3*n_vertices*0 + 3*vx + 2];
				}
				pvecs[1] = pvecs[1] - pvecs[0];
				pvecs[2] = pvecs[2] - pvecs[0];
				Vector3D fa_norm = pvecs[1] ^ pvecs[2];
				fa_norm.normaliseSafe();
				for(int j=0;j<3 && ok;j++) {
					for(int k=0;k<3;k++) {
						int vx = this->va_indices[adj_faces[j]*3+k];
						pvecs[k].x = this->va_vertices[3*n_vertices*0 + 3*vx + 0];
						pvecs[k].y = this->va_vertices[3*n_vertices*0 + 3*vx + 1];
						pvecs[k].z = this->va_vertices[3*n_vertices*0 + 3*vx + 2];
					}
					pvecs[1] = pvecs[1] - pvecs[0];
					pvecs[2] = pvecs[2] - pvecs[0];
					Vector3D adj_fa_norm = pvecs[1] ^ pvecs[2];
					adj_fa_norm.normaliseSafe();
					float cosang = fa_norm % adj_fa_norm;
					LOG("    cos angle %f with adj face %d\n", cosang, adj_faces[j]);
					if( cosang < 0.5 ) {
						ok = false;
					}
				}
			}
			if( ok ) {
				{
					// sort adj_faces for correct winding order
					bool has_sort_face[3] = {false, false, false};
					size_t sort_faces[3] = {0, 0, 0};
					for(int j=0;j<3;j++) {
						int vx = this->va_indices[i*3+j];
						// find opposite adj face - the one without this vx
						//sort_faces[j] = -1;
						has_sort_face[j] = false;
						for(int k=0;k<3 && !has_sort_face[j];k++) {
							if( vx != this->va_indices[adj_faces[k]*3+0] &&
								vx != this->va_indices[adj_faces[k]*3+1] &&
								vx != this->va_indices[adj_faces[k]*3+2] ) {
									has_sort_face[j] = true;
									sort_faces[j] = adj_faces[k];
							}
						}
					}
					for(int j=0;j<3;j++) {
						adj_faces[j] = sort_faces[j];
					}
				}
				for(int j=0;j<3 && ok;j++) {
					LOG("    %d has adj face %d\n", i, adj_faces[j]);
				}
				int new_vxs[3];
				int n_new_vxs = 0;
				for(int j=0;j<3 && n_new_vxs<3;j++) {
					for(int k=0;k<3 && n_new_vxs<3;k++) {
						int vx = this->va_indices[adj_faces[j]*3+k];
						LOG("    face %d has vx %d\n", adj_faces[j], vx);
						if( vx != this->va_indices[i*3+0] &&
							vx != this->va_indices[i*3+1] &&
							vx != this->va_indices[i*3+2] ) {
								new_vxs[n_new_vxs++] = vx;
						}
					}
				}
				if( n_new_vxs != 3 ) {
					Vision::setError(new VisionException(this,VisionException::V_OBJ_LOD_ERROR,"LOD Failure: Couldn't find 3 new vxs"));
				}
				for(int j=0;j<3;j++) {
					int vx = this->va_indices[i*3+j];
					LOG("    replace middle face %d vx %d with vx %d\n", i, vx, new_vxs[j]);
				}
				bool merge = true;
				//Vector3D newpos[3];
				Vector3D line_pos[3];
				Vector3D line_dir[3];
				for(int j=0;j<3;j++) {
					// need to find vertices adjacent to this vx
					unsigned int vx = static_cast<unsigned int>(this->va_indices[i*3+j]); // cast to unsigned int, to avoid overflow when multiplying
					Vector3D vecs[2];
					for(int k=0;k<2;k++) {
						int adj_vx = new_vxs[(j+k+1)%3];
						vecs[k].x = this->va_vertices[3*n_vertices*0 + 3*adj_vx + 0];
						vecs[k].y = this->va_vertices[3*n_vertices*0 + 3*adj_vx + 1];
						vecs[k].z = this->va_vertices[3*n_vertices*0 + 3*adj_vx + 2];
					}
					Vector3D mid;
					mid.x = this->va_vertices[3*n_vertices*0 + 3*vx + 0];
					mid.y = this->va_vertices[3*n_vertices*0 + 3*vx + 1];
					mid.z = this->va_vertices[3*n_vertices*0 + 3*vx + 2];
					Vector3D dir = vecs[1] - vecs[0];
					if( dir.x == 0 && dir.y == 0 && dir.z == 0 ) {
						// coi vertices (or shared vx)
						merge = false;
						break;
					}
					dir.normaliseSafe();
					float dist = mid.distFromLine(&vecs[0], &dir);
					if( dist > 1.0 ) {
						merge = false;
						break;
					}
					LOG("    %d : dist from line is %f\n", j, dist);
					line_pos[j] = vecs[0];
					line_dir[j] = dir;
					/*newpos[j] = mid;
					newpos[j].dropOnLine(&vecs[0], &dir);
					LOG("    %d : move from %f,%f,%f to %f,%f,%f\n", j, mid.x, mid.y, mid.z, newpos[j].x, newpos[j].y, newpos[j].z);*/
				}

				if( merge ) {
					new_n_polys -= 3;
					for(int j=0;j<3;j++) {
						for(int f=0;f<n_frames;f++) {
							// shift position of old vertices
							unsigned int vx = static_cast<unsigned int>(this->va_indices[i*3+j]); // cast to unsigned int, to avoid overflow when multiplying
							Vector3D pos;
							pos.x = this->va_vertices[3*n_vertices*f + 3*vx + 0];
							pos.y = this->va_vertices[3*n_vertices*f + 3*vx + 1];
							pos.z = this->va_vertices[3*n_vertices*f + 3*vx + 2];
							float dist = pos.distFromLine(&line_pos[j], &line_dir[j]);
							if( dist > 0.0 )
								tweaked_vx[vx] = true;
							Vector3D newpos = pos;
							newpos.dropOnLine(&line_pos[j], &line_dir[j]);
							this->va_vertices[3*n_vertices*f + 3*vx + 0] = newpos.x;
							this->va_vertices[3*n_vertices*f + 3*vx + 1] = newpos.y;
							this->va_vertices[3*n_vertices*f + 3*vx + 2] = newpos.z;
							// check for other coi vertices
							for(size_t k=0;k<n_vertices;k++) {
								if( k == vx )
									continue;
								Vector3D o_pos;
								o_pos.x = this->va_vertices[3*n_vertices*f + 3*k + 0];
								o_pos.y = this->va_vertices[3*n_vertices*f + 3*k + 1];
								o_pos.z = this->va_vertices[3*n_vertices*f + 3*k + 2];
								if( o_pos == pos ) {
									this->va_vertices[3*n_vertices*f + 3*k + 0] = newpos.x;
									this->va_vertices[3*n_vertices*f + 3*k + 1] = newpos.y;
									this->va_vertices[3*n_vertices*f + 3*k + 2] = newpos.z;
									if( dist > 0.0 )
										tweaked_vx[k] = true;
								}
							}
						}
					}
					for(int j=0;j<3;j++) {
						del_face[adj_faces[j]] = true;
						//merged_faces->add((void *)adj_faces[j]);
						merged_faces->push_back(adj_faces[j]);
					}
					//merged_faces->add((void *)i);
					merged_faces->push_back(i);
					for(int j=0;j<3;j++) {
						// set new vertices
						this->va_indices[i*3+j] = new_vxs[j];
					}
				}
			}
		}
		/*
		for(int i=0;i<edges->size();i++) {
		RD_Edge *edge = static_cast<RD_Edge *>(edges->get(i));
		if( merged_faces->contains((void *)edge->f0) || merged_faces->contains((void *)edge->f1) ) {
		// can't merge anymore
		}
		else {
		LOG("merge face %d and %d\n", edge->f0, edge->f1);
		for(int j=0;j<3;j++) {
		LOG("    face %d : vx %d\n", edge->f0, this->va_indices[edge->f0*3 + j]);
		}
		for(int j=0;j<3;j++) {
		LOG("    face %d : vx %d\n", edge->f1, this->va_indices[edge->f1*3 + j]);
		}
		int del_vx = edge->v0;
		int new_vxs[3];
		int o_vx[2] = {-1,-1};
		for(int j=0;j<3;j++) {
		int vx = this->va_indices[edge->f0*3 + j];
		if( vx != edge->v0 && vx != edge->v1 )
		o_vx[0] = vx;
		vx = this->va_indices[edge->f1*3 + j];
		if( vx != edge->v0 && vx != edge->v1 )
		o_vx[1] = vx;
		}
		LOG("    del_vx %d , o_vx %d , %d\n", del_vx, o_vx[0], o_vx[1]);
		if( o_vx[0] == -1 || o_vx[1] == -1 ) {
		Vision::setError(new VisionException(this,VisionException::V_OBJ_LOD_ERROR,"LOD Failure: Couldn't find o_vxs"));
		break;
		}
		Vector3D p0, p1, mid;
		p0.x = this->va_vertices[3*n_vertices*0 + 3*o_vx[0] + 0];
		p0.y = this->va_vertices[3*n_vertices*0 + 3*o_vx[0] + 1];
		p0.z = this->va_vertices[3*n_vertices*0 + 3*o_vx[0] + 2];
		p1.x = this->va_vertices[3*n_vertices*0 + 3*o_vx[1] + 0];
		p1.y = this->va_vertices[3*n_vertices*0 + 3*o_vx[1] + 1];
		p1.z = this->va_vertices[3*n_vertices*0 + 3*o_vx[1] + 2];
		mid.x = this->va_vertices[3*n_vertices*0 + 3*del_vx + 0];
		mid.y = this->va_vertices[3*n_vertices*0 + 3*del_vx + 1];
		mid.z = this->va_vertices[3*n_vertices*0 + 3*del_vx + 2];
		Vector3D dir = p1 - p0;
		dir.normaliseSafe();
		float dist = mid.distFromLine(&p0, &dir);
		LOG("    dist from line is %f\n", dist);
		// keeping f0, so we use o_vx[1]
		for(int j=0;j<3;j++) {
		int vx = this->va_indices[edge->f0*3 + j];
		if( vx == del_vx )
		this->va_indices[edge->f0*3 + j] = o_vx[1];
		}
		for(int j=0;j<3;j++) {
		LOG("    new order : vx %d\n", this->va_indices[edge->f0*3 + j]);
		}
		del_face[edge->f1] = true;
		new_n_polys--;
		merged_faces->add((void *)edge->f0);
		merged_faces->add((void *)edge->f1);
		}
		delete edge;
		}
		*/
		/*for(size_t i=0;i<edges.size();i++) {
			//RD_Edge *edge = static_cast<RD_Edge *>(edges->get(i));
			RD_Edge *edge = edges.at(i);
			delete edge;
		}*/
		delete merged_faces;
		//delete edges;
		if( new_n_polys < n_polys ) {
			LOG("reduced from %d polys to %d\n", n_polys, new_n_polys);
			//unsigned short *new_va_indices = new unsigned short[3*new_n_polys];
			vector<unsigned short> new_va_indices(3*new_n_polys);
			for(size_t i=0,c=0;i<n_polys;i++) {
				if( !del_face[i] ) {
					for(int j=0;j<3;j++)
						new_va_indices[c*3+j] = this->va_indices[i*3+j];
					c++;
				}
			}
			this->n_renderlength = 3*new_n_polys;
			//delete [] this->va_indices;
			this->va_indices = new_va_indices;
			// TODO: remove redundant vertices
		}
	}
#endif
#if 0
	// geomipmapping
	bool done = false;
	//if( this->va_indices != NULL && this->mode == GL_TRIANGLES ) {
	//while( this->va_indices != NULL && this->mode == GL_TRIANGLES && !done ) {
	//while( !done ) {
	if( false ) {
		done = true;
		LOG("attempt LOD geomipmapping\n");
		size_t n_polys = this->n_renderlength / 3;
		LOG("n_polys = %d\n", n_polys);
		bool *remove = new bool[this->n_vertices];
		bool *remove_poly = new bool[n_polys];
		for(size_t i=0;i<this->n_vertices;i++) {
			remove[i] = false;
		}
		for(size_t i=0;i<n_polys;i++) {
			remove_poly[i] = false;
		}
		for(size_t i=0;i<this->n_vertices;i++) {
			// try to remove vertex
			unsigned short outer_vxs[3] = {0, 0, 0};
			size_t polys[3] = {0, 0, 0};
			int count = 0;
			bool ok = true;
			for(size_t j=0;j<n_polys && ok;j++) {
				unsigned short temp_vxs[3] = {0, 0, 0};
				for(int k=0;k<3;k++) {
					temp_vxs[k] = this->va_indices[j*3 + k];
				}
				for(int k=0;k<3 && ok;k++) {
					if( temp_vxs[k] == i ) {
						/*for(int l=0;l<3;l++) {
						LOG("temp >>> %d\n", temp_vxs[l]);
						}*/
						polys[count] = j;
						if( count == 0 ) {
							outer_vxs[0] = temp_vxs[(k+1)%3];
							outer_vxs[1] = temp_vxs[(k+2)%3];
						}
						else if( count == 1 ) {
							// TODO: check winding order is the same
							bool found = false;
							for(int l=0;l<3 && ok;l++) {
								if( temp_vxs[l] != i && temp_vxs[l] != outer_vxs[0] && temp_vxs[l] != outer_vxs[1] ) {
									if( found )
										ok = false;
									else {
										found = true;
										outer_vxs[2] = temp_vxs[l];
									}
								}
							}
						}
						else if( count == 2 ) {
							for(int l=0;l<3 && ok;l++) {
								if( temp_vxs[l] != i && temp_vxs[l] != outer_vxs[0] && temp_vxs[l] != outer_vxs[1] && temp_vxs[l] != outer_vxs[2] ) {
									ok = false;
								}
							}
						}
						else if( count == 3 ) {
							ok = false;
						}
						count++;
						break;
					}
				}
			}
			if( count != 3 )
				ok = false;
			/*for(int l=0;l<3 && ok;l++) {
			LOG("outer %d\n", outer_vxs[l]);
			}*/
			for(int j=0;j<3 && ok;j++) {
				ok = !remove[ outer_vxs[j] ];
			}
			if( ok ) {
				// force removal
				LOG("    remove vx %d\n", i);
				done = false;
				remove[i] = true;
				for(int j=0;j<3;j++) {
					this->va_indices[ polys[0] * 3 + j ] = outer_vxs[j];
				}
				remove_poly[ polys[1] ] = true;
				remove_poly[ polys[2] ] = true;
			}
			//break;
		}

		int n_new_polys = 0;
		for(size_t i=0;i<n_polys;i++) {
			if( !remove_poly[i] )
				n_new_polys++;
		}
		LOG("n_new_polys = %d\n", n_new_polys);
		//unsigned short *new_va_indices = new unsigned short[n_new_polys*3];
		vector<unsigned short> new_va_indices;
		for(size_t i=0,dst=0;i<n_polys;i++) {
			if( !remove_poly[i] ) {
				//LOG("%d %d\n", dst, i);
				for(int j=0;j<3;j++) {
					new_va_indices[dst*3+j] = va_indices[i*3+j];
				}
				dst++;
			}
		}
		this->n_renderlength = n_new_polys*3;
		//delete [] va_indices;
		this->va_indices = new_va_indices;

		/*int n_new_vxs = 0;
		for(int i=0;i<this->n_vertices;i++) {
		if( !remove[i] )
		n_new_vertices++;
		}*/

		delete [] remove;
		delete [] remove_poly;
	}
#endif

	if( !need_va_indices ) {
		/*delete [] this->va_indices;
		this->va_indices = NULL;*/
		this->va_indices.clear();
	}

	this->prepare(need_shadows);
}

void RenderData::shareVertices() {
	static int total_time = 0;
	int time_s = clock();
	size_t n_polys = this->n_renderlength / 3;
	LOG("RenderData::shareVertices()\n");
	//return;
	//size_t *vertex_pos = new size_t[n_vertices];
	vector<size_t> vertex_pos(n_vertices);
	for(size_t i=0;i<n_vertices;i++)
		vertex_pos[i] = i;

	//set<Vector3D> vectors;

	// TODO: support colors, normals, texture coords etc
	bool any_coi = false;
	for(size_t i=0;i<n_vertices;i++) {
		//int coi_vx = -1;
		//for(int j=0;j<i && coi_vx == -1;j++) {
		for(size_t j=i+1;j<n_vertices;j++) {
			if( vertex_pos[j] != j ) {
				// vertex j already merged
				continue;
			}
			// is vertex j coi with vertex i?
			bool coi = true;
			//coi = false;
			for(int f=0;f<n_frames && coi;f++) {
				Vector3D vx_i = this->getVertex(f, i);
				Vector3D vx_j = this->getVertex(f, j);
				if( vx_i != vx_j ) {
					Vector3D dist = vx_i - vx_j;
					if( dist.magnitude() <= V_TOL_LINEAR ) {
						//LOG("!!!\n");
					}
					else {
						coi = false;
					}
				}
			}
			/*for(int p=0;p<n_polys && coi;p++) {
			bool found[2] = {false, false};
			for(int k=0;k<3;k++) {
			int vx_index = this->va_indices[3*p+k];
			if( vx_index == i )
			found[0] = true;
			else if( vx_index == j )
			found[1] = true;
			}
			if( found[0] && found[1] ) {
			// poly would have repeated vertex
			coi = false;
			}
			}*/
			if( coi ) {
				any_coi = true;
				//LOG("vertex %d is coi with %d\n", i, j);
				//coi_vx = j;
				vertex_pos[j] = i;
			}
		}
		/*if( coi_vx != -1 ) {
		vertex_pos[i] = coi_vx;
		}*/
	}

	if( any_coi ) {
		// remap vertex indices
		//if( this->va_indices == NULL ) {
		if( this->va_indices.size() == 0 ) {
			if( n_renderlength >= 65536 ) {
				Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Renderlength overflow"));
			}
			//this->va_indices = new unsigned short[this->n_renderlength];
			this->va_indices.resize(this->n_renderlength);
			for(unsigned short i=0;i<this->n_renderlength;i++)
				this->va_indices[i] = i;
		}
		for(size_t i=0;i<this->n_renderlength;i++) {
			if( vertex_pos[ this->va_indices[i] ] >= 65536 ) {
				Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Renderlength overflow"));
			}
			this->va_indices[i] = (unsigned short)vertex_pos[ this->va_indices[i] ];
		}

		//this->compact(); // now called in prepare() method
	}

	this->prepare(need_shadows);
	//delete [] vertex_pos;

	int this_time = clock() - time_s;
	total_time += this_time;
	if( this_time > 0 ) {
		LOG("Time %d\n", this_time);
		LOG("### Total Time %d\n", total_time);
	}
}

void RenderData::compact() {
	//return;
	//bool *used = new bool[this->n_vertices];
	vector<unsigned char> used(this->n_vertices);
	for(size_t i=0;i<n_vertices;i++)
		used[i] = 0;
	for(unsigned short i=0;i<this->n_renderlength;i++) {
		//if( this->va_indices == NULL )
		if( this->va_indices.size() == 0 )
			used[i] = 1;
		else
			used[ this->va_indices[i] ] = 1;
	}
	bool changed = false;
	//int *remap_to = new int[n_vertices];
	vector<int> remap_to(n_vertices);
	int last_used = -1;
	for(size_t i=0;i<n_vertices;i++) {
		if( used[i] != 0 ) {
			last_used++;
			remap_to[i] = last_used;
			if( i != remap_to[i] )
				changed = true;
		}
		else {
			remap_to[i] = -1;
		}
		//LOG("map %d to %d\n", i, remap_to[i]);
	}
	if( changed )
	{
		for(int fr=0;fr<n_frames;fr++) {
			for(size_t i=0;i<n_vertices;i++) {
				if( used[i] == 0 )
					continue;
				if( i == remap_to[i] )
					continue;
				for(int j=0;j<3;j++) {
					this->va_vertices[(fr*n_vertices+remap_to[i])*3+j] = this->va_vertices[(fr*n_vertices+i)*3+j];
					//if( this->va_normals != NULL )
					if( this->va_normals.size() != 0 )
						this->va_normals[(fr*n_vertices+remap_to[i])*3+j] = this->va_normals[(fr*n_vertices+i)*3+j];
				}
				/*for(int j=0;j<2;j++) {
					if( this->va_texcoords != NULL ) {
						this->va_texcoords[(fr*n_vertices+remap_to[i])*2+j] = this->va_texcoords[(fr*n_vertices+i)*2+j];
					}
				}
				for(int j=0;j<(alpha_channel ? 4 : 3);j++) {
					if( this->va_colors != NULL )
						this->va_colors[(fr*n_vertices+remap_to[i])*(alpha_channel ? 4 : 3)+j] = this->va_colors[(fr*n_vertices+i)*(alpha_channel ? 4 : 3)+j];
				}*/
			}
		}
		for(size_t i=0;i<n_vertices;i++) {
			if( used[i] == 0 )
				continue;
			if( i == remap_to[i] )
				continue;
			for(int j=0;j<2;j++) {
				//if( this->va_texcoords != NULL ) {
				if( this->va_texcoords.size() != 0 ) {
					this->va_texcoords[remap_to[i]*2+j] = this->va_texcoords[i*2+j];
				}
			}
			for(int j=0;j<getDim();j++) {
				//if( this->va_colors != NULL )
				if( this->va_colors.size() != 0 )
					this->va_colors[remap_to[i]*getDim()+j] = this->va_colors[i*getDim()+j];
			}
		}
		//for(unsigned short i=0;i<this->n_renderlength && this->va_indices != NULL;i++) {
		for(unsigned short i=0;i<this->n_renderlength && this->va_indices.size() != 0;i++) {
			this->va_indices[i] = remap_to[ this->va_indices[i] ];
		}
	}
	//delete [] used;
	//delete [] remap_to;

	// maybe shrink vertex array
	int max_vert = -1;
	//if( this->va_indices == NULL )
	if( this->va_indices.size() == 0 )
		max_vert = this->n_renderlength-1;
	else {
		for(int i=0;i<this->n_renderlength;i++) {
			if( this->va_indices[i] > max_vert )
				max_vert = this->va_indices[i];
		}
	}
	//max_vert = this->n_vertices-1;
	if( (size_t)(max_vert+1) < this->n_vertices ) {
		int n_new_vertices = max_vert+1;
		// need to remap vertices
		for(int fr=1;fr<n_frames;fr++) {
			for(int i=0;i<n_new_vertices;i++) {
				for(int j=0;j<3;j++) {
					this->va_vertices[(fr*n_new_vertices+i)*3+j] = this->va_vertices[(fr*n_vertices+i)*3+j];
					//if( this->va_normals != NULL )
					if( this->va_normals.size() != 0 )
						this->va_normals[(fr*n_new_vertices+i)*3+j] = this->va_normals[(fr*n_vertices+i)*3+j];
					/*if( this->va_texcoords != NULL )
						this->va_texcoords[(fr*n_new_vertices+i)*2+j] = this->va_texcoords[(fr*n_vertices+i)*2+j];
					if( this->va_colors != NULL )
						this->va_colors[(fr*n_new_vertices+i)*(alpha_channel ? 4 : 3)+j] = this->va_colors[(fr*n_vertices+i)*(alpha_channel ? 4 : 3)+j];*/
				}
			}
		}
		this->n_vertices = n_new_vertices;
	}
}

void RenderData::combine(const RenderData *that) {
	freeVBOs();
	freeShadowData();
	clearHelpData();

	if( !this->compatible(that) ) {
		Vision::setError(new VisionException(this,VisionException::V_MODELLER_ERROR,"RenderData::combine called with incompatible RenderDatas"));
	}

	size_t n_old_vertices = this->n_vertices;
	this->n_vertices += that->n_vertices;
	if( this->n_vertices >= 65536 ) {
		Vision::setError(new VisionException(this,VisionException::V_MODELLER_ERROR,"RenderData::combine too many vertices"));
	}
	size_t n_old_elements = n_frames * n_old_vertices;
	size_t n_elements = n_frames * n_vertices;
	if( this->va_normals.size() != 0 ) {
		vector<float> new_va_normals(3 * n_elements);
		for(size_t i=0;i<3*n_old_elements;i++)
			new_va_normals[i] = this->va_normals[i];
		for(size_t i=3*n_old_elements;i<3*n_elements;i++)
			new_va_normals[i] = that->va_normals[i-3*n_old_elements];
		this->va_normals = new_va_normals;
	}

	vector<float> new_va_vertices(3 * n_elements);
	size_t c = 0;
	for(int i=0;i<n_frames;i++) {
		for(size_t j=0;j<3*n_old_vertices;j++) {
			new_va_vertices[c++] = this->va_vertices[3*n_old_vertices*i+j];
		}
		for(size_t j=0;j<3*that->n_vertices;j++) {
			new_va_vertices[c++] = that->va_vertices[3*that->n_vertices*i+j];
		}
	}
	this->va_vertices = new_va_vertices;

	this->va_vertices_stream.resize(3 * n_vertices);

	if( this->va_colors.size() != 0 ) {
		vector<unsigned char> new_va_colors(getDim() * n_vertices);
		for(size_t i=0;i<getDim() * n_old_vertices;i++)
			new_va_colors[i] = this->va_colors[i];
		for(size_t i=getDim() * n_old_vertices;i<getDim() * n_vertices;i++)
			new_va_colors[i] = that->va_colors[i-getDim()*n_old_vertices];
		this->va_colors = new_va_colors;
	}
	if( this->va_texcoords.size() != 0 ) {
		vector<float> new_va_texcoords(2 * n_vertices);
		for(size_t i=0;i<2*n_old_vertices;i++)
			new_va_texcoords[i] = this->va_texcoords[i];
		for(size_t i=2*n_old_vertices;i<2*n_vertices;i++)
			new_va_texcoords[i] = that->va_texcoords[i-2*n_old_vertices];
		this->va_texcoords = new_va_texcoords;
	}

	size_t n_old_renderlength = this->n_renderlength;
	this->n_renderlength += that->n_renderlength;
	vector<unsigned short> new_va_indices(this->n_renderlength);
	for(size_t i=0;i<n_old_renderlength;i++)
		new_va_indices[i] = this->va_indices[i];
	for(size_t i=n_old_renderlength;i<n_renderlength;i++) {
		if( that->va_indices[i-n_old_renderlength] + n_old_vertices >= 65536 ) {
			Vision::setError(new VisionException(this,VisionException::V_MODELLER_ERROR,"RenderData::combine too many render indices"));
		}
		new_va_indices[i] = (unsigned short)(that->va_indices[i-n_old_renderlength] + n_old_vertices); // need to offset
	}
	this->va_indices = new_va_indices;
}

/*RenderData *RenderData::createShadowMesh(const RenderData *rd, int n_rd) {
	RenderData *rd = new RenderData();
	*rd = *this;

	// needed so we don't clear "this"'s data
	rd->faces_planes = NULL;
	rd->faces_adj_faces = NULL;
	rd->faces_boundary = NULL;
	rd->edges_skip = NULL;
	rd->visible_polys = NULL;
	rd->va_shadow_vertices = NULL;
	rd->va_shadow_normals = NULL;
	rd->va_shadow_indices = NULL;
	rd->va_normals = NULL;
	rd->va_tangents = NULL;
	rd->va_vertices = NULL;
	rd->va_vertices_stream = NULL;
	rd->va_texcoords = NULL;
	//rd->va_texcoords_secondary = NULL;
	rd->va_indices = NULL;

	rd->vao_shadow_vertices = NULL;
	rd->vao_shadow_indices = NULL;
	rd->vao_vertices = NULL;
	rd->vao_vertices_stream = NULL;
	rd->vao_normals = NULL;
	rd->vao_tangents = NULL;
	rd->vao_texcoords = NULL;
	rd->vao_colors = NULL;
	rd->va_colors = 0;
	rd->vao_indices = NULL;

	rd->free();

	rd->initVertexArrays(this->n_frames, this->n_vertices, false, this->alpha_channel, false, false);
	int n_elements = n_frames * n_vertices;
	for(int i=0;i<3*n_elements;i++)
		rd->va_vertices[i] = this->va_vertices[i];
	if( va_indices != NULL ) {
		rd->va_indices = new unsigned short[n_renderlength];
		for(int i=0;i<n_renderlength;i++)
			rd->va_indices[i] = this->va_indices[i];
	}

	rd->prepare(true);
	return rd;
}*/

RenderData *RenderData::copy(bool need_shadows,bool colors,bool normals,bool texcoords) const {
	RenderData *rd = new RenderData();
	*rd = *this;

	if( !texcoords ) {
		// strip textures
		//rd->texture = NULL;
		//rd->secondary_texture = NULL;
		/*for(int i=0;i<MAX_SPLATS;i++) {
			rd->splat_textures[i] = NULL;
		}*/
		//rd->bump_texture = NULL;
		rd->need_tangents = false;
		rd->tangent_uv_channel = -1;
		rd->uses_hardware_animation = false;
		rd->hardware_animation_channel = -1;
		rd->n_textures = 0;

		for(int i=0;i<MAX_TEXTURES;i++) {
			rd->textures[i] = NULL;
		}
	}

	// needed so we don't clear "this"'s data
	/*rd->faces_planes = NULL;
	rd->faces_adj_faces = NULL;
	rd->faces_boundary = NULL;
	rd->edges_skip = NULL;
	rd->visible_polys = NULL;
	rd->shadow_edges = NULL;
	rd->shadow_vertices_used = NULL;*/
	/*rd->va_shadow_vertices = NULL;
	rd->va_shadow_normals = NULL;
	rd->va_shadow_indices = NULL;*/
	/*rd->va_normals = NULL;
	rd->va_tangents = NULL;
	rd->va_vertices = NULL;
	rd->va_vertices_stream = NULL;
	rd->va_texcoords = NULL;
	rd->va_colors = NULL;
	//rd->va_texcoords_secondary = NULL;
	rd->va_indices = NULL;*/
	rd->va_vertices.clear();
	rd->va_vertices_stream.clear();
	rd->va_normals.clear();
	rd->va_tangents.clear();
	rd->va_texcoords.clear();
	rd->va_colors.clear();

	rd->vao_shadow_vertices = NULL;
	rd->vao_shadow_indices = NULL;
	rd->vao_vertices = NULL;
	rd->vao_vertices_stream = NULL;
	rd->vao_normals = NULL;
	rd->vao_tangents = NULL;
	rd->vao_texcoords = NULL;
	rd->vao_colors = NULL;
	rd->vao_indices = NULL;

	/*rd->clearHelpData();
	rd->freeShadowData();
	rd->freeVBOs();*/
	rd->free();

	//rd->initVertexArrays(this->n_frames, this->n_vertices, this->va_colors!=NULL, this->alpha_channel, this->va_normals!=NULL, this->va_texcoords!=NULL);
	//rd->initVertexArrays(this->n_frames, this->n_vertices, colors && this->va_colors!=NULL, this->alpha_channel, normals && this->va_normals!=NULL, texcoords && this->va_texcoords!=NULL);
	//rd->initVertexArrays(this->n_frames, this->n_vertices, colors && this->va_colors.size()!=0, /*this->alpha_channel,*/ normals && this->va_normals.size()!=0, texcoords && this->va_texcoords.size()!=0);
	rd->initVertexArrays(this->n_frames, this->n_vertices, normals && this->va_normals.size()!=0);
	size_t n_elements = n_frames * n_vertices;
	//for(size_t i=0;i<(alpha_channel ? 4 : 3) * n_vertices && colors && this->va_colors!=NULL;i++)
	for(size_t i=0;i<getDim() * n_vertices && colors && this->va_colors.size()!=0;i++)
		rd->va_colors[i] = this->va_colors[i];
	//for(size_t i=0;i<3*n_elements && normals && this->va_normals!=NULL;i++)
	for(size_t i=0;i<3*n_elements && normals && this->va_normals.size()!=0;i++)
		rd->va_normals[i] = this->va_normals[i];
	for(size_t i=0;i<3*n_elements;i++)
		rd->va_vertices[i] = this->va_vertices[i];
	//for(size_t i=0;i<2*n_vertices && texcoords && this->va_texcoords!=NULL;i++)
	for(size_t i=0;i<2*n_vertices && texcoords && this->va_texcoords.size()!=0;i++)
		rd->va_texcoords[i] = this->va_texcoords[i];
	//if( va_indices != NULL ) {
	if( va_indices.size() != 0 ) {
		//rd->va_indices = new unsigned short[n_renderlength];
		rd->va_indices.resize(n_renderlength);
		for(size_t i=0;i<n_renderlength;i++)
			rd->va_indices[i] = this->va_indices[i];
	}

	rd->prepare(need_shadows);
	return rd;
}

void RenderData::clearHelpData() {
	faces_planes.clear();
	faces_adj_faces.clear();
	faces_boundary.clear();
	edges_skip.clear();
	visible_polys.clear();
	shadow_edges.clear();
	shadow_vertices_used.clear();
	/*if( faces_planes != NULL ) {
		delete [] faces_planes;
		faces_planes = NULL;
	}
	if( faces_adj_faces != NULL ) {
		delete [] faces_adj_faces;
		faces_adj_faces = NULL;
	}
	if( faces_boundary != NULL ) {
		delete [] faces_boundary;
		faces_boundary = NULL;
	}
	if( edges_skip != NULL ) {
		delete [] edges_skip;
		edges_skip = NULL;
	}
	if( visible_polys != NULL ) {
		delete [] visible_polys;
		visible_polys = NULL;
	}
	if( shadow_edges != NULL ) {
		delete [] shadow_edges;
		shadow_edges = NULL;
	}
	if( shadow_vertices_used != NULL ) {
		delete [] shadow_vertices_used;
		shadow_vertices_used = NULL;
	}*/
}
void RenderData::freeShadowData() {
	delete vao_shadow_vertices;
	vao_shadow_vertices = NULL;
	delete vao_shadow_indices;
	vao_shadow_indices = NULL;
	/*delete [] va_shadow_vertices;
	va_shadow_vertices = NULL;
	delete [] va_shadow_normals;
	va_shadow_normals = NULL;
	delete [] va_shadow_indices;
	va_shadow_indices = NULL;*/
	va_shadow_vertices.clear();
	va_shadow_normals.clear();
	va_shadow_indices.clear();
	n_shadow_renderlength = 0;
}

void RenderData::setTexture(int index, Texture *texture) {
	if( index < 0 || index >= MAX_TEXTURES ) {
		LOG("invalid texture index %d\n", index);
		Vision::setError(new VisionException(this,VisionException::V_RENDER_ERROR, "invalid texture index"));
	}
	this->textures[index] = texture;
}

Texture *RenderData::getTexture(int index) const {
	if( index < 0 || index >= MAX_TEXTURES ) {
		LOG("invalid texture index %d\n", index);
		Vision::setError(new VisionException(this,VisionException::V_RENDER_ERROR, "invalid texture index"));
	}
	else if( index >= n_textures ) {
		LOG("texture index %d outside of valid range %d\n", index, n_textures);
		Vision::setError(new VisionException(this,VisionException::V_RENDER_ERROR, "texture index out of range"));
	}
	return this->textures[index];
}

void RenderData::prepare(bool need_shadows) {
	//LOG("%d: %d, %d\n", this, this->n_renderlength, this->n_vertices);
	this->need_shadows = need_shadows;
	//return;
	//if( Vision::vertexBufferObjects && n_frames == 1 ) {

	this->last_frame = -1;
	this->last_frac = -1.0;

	int n_polys = this->n_renderlength / 3;
	//LOG("RenderData %d : n_polys = %d\n", this, n_polys);

	this->clearHelpData();
	this->freeShadowData();
	this->freeVBOs();

	this->compact();

	if( this->n_renderlength == 0 ) {
		return; // support for empty meshes
	}

#ifndef SHADOW_ALGORITHM_NEW
	int n_shadow_vertices = 2 * 3 * n_polys * 3; // original
#endif
#ifdef SHADOW_ALGORITHM_NEW
	int n_shadow_vertices = 6 * 3 * n_polys * 3; // new
#endif
	int n_shadow_indices = 2 * 3 * 3 * n_polys;

	if( need_shadows )
	{

		bool need_va_indices = true;
		//if( this->va_indices == NULL ) {
		if( this->va_indices.size() == 0 ) {
			// TODO: fix needing to do this
			need_va_indices = false;
			//this->va_indices = new unsigned short[n_renderlength];
			this->va_indices.resize(n_renderlength);
			for(unsigned short i=0;i<n_renderlength;i++)
				this->va_indices[i] = i;
		}

		//this->va_shadow_vertices = new float[4 * 3 * n_polys * 3 * sizeof(float)];
		//this->va_shadow_indices = new unsigned short[6 * 3 * n_polys * sizeof(unsigned short)];
		/*this->va_shadow_vertices = new float[n_shadow_vertices * sizeof(float)];
		this->va_shadow_normals = new float[n_shadow_vertices * sizeof(float)];*/
		this->va_shadow_vertices.resize(n_shadow_vertices);
		this->va_shadow_normals.resize(n_shadow_vertices);
		// initialise to sensible defaults
		for(int i=0;i<n_shadow_vertices;i++) {
			this->va_shadow_vertices[i] = 0.0f;
			this->va_shadow_normals[i] = 0.0f;
		}
		//this->va_shadow_vertices = new float[2 * n_vertices * 3 * sizeof(float)];
/*#ifndef SHADOW_ALGORITHM_NEW
		int n_shadow_indices = 2 * 3 * 3 * n_polys;
#endif
#ifdef SHADOW_ALGORITHM_NEW
		int n_shadow_indices = 2 * 3 * 3 * n_polys;
#endif*/
		//this->va_shadow_indices = new unsigned short[n_shadow_indices * sizeof(unsigned short)];
		this->va_shadow_indices.resize(n_shadow_indices);
		for(int i=0;i<n_shadow_indices;i++) {
			this->va_shadow_indices[i] = 0;
		}
		//this->va_shadow_indices = new unsigned short[4 * 3 * n_polys * sizeof(unsigned short)];

		//this->visible_polys = new bool[n_polys];
		this->visible_polys.resize(n_polys);
		// get plane equations
		//faces_planes = new float[4 * this->n_frames * n_polys];
		faces_planes.resize(4 * this->n_frames * n_polys);
		for(int fr=0;fr<this->n_frames;fr++) {
			for(int i=0;i<n_polys;i++) {
				unsigned short vxs[3] = {0, 0, 0};
				Vector3D vecs[3];
				for(int j=0;j<3;j++) {
					vxs[j] = this->va_indices[i*3 + j];
					vecs[j] = this->getVertex(fr, vxs[j]);
				}
				// check for dups
				/*for(int j=0;j<3;j++) {
				for(int j1=0;j1<j;j1++) {
				if( vxs[j] == vxs[j1] ) {
				LOG("duplicated vertex %d\n", vxs[j]);
				}
				}
				}*/
				vecs[1] -= vecs[0];
				vecs[2] -= vecs[0];
				Vector3D normal = vecs[1] ^ vecs[2];
				/*if( normal.magnitude() <= 0 ) {
				LOG(">>>ERROR %f %f %f\n", normal.x, normal.y, normal.z);
				}*/
				normal.normaliseSafe();
				faces_planes[4*n_polys*fr + 4*i + 0] = normal.x;
				faces_planes[4*n_polys*fr + 4*i + 1] = normal.y;
				faces_planes[4*n_polys*fr + 4*i + 2] = normal.z;
				float D = - normal % vecs[0];
				faces_planes[4*n_polys*fr + 4*i + 3] = D;
				/*if( fr == 0 ) {
				//LOG("%d : %d , %d , %d\n", i, vxs[0], vxs[1], vxs[2]);
				LOG("%d : %f , %f , %f , %f\n", i, normal.x, normal.y, normal.z, D);
				}*/
			}
		}

		// get adjacent faces
		// TODO: improve performance
		//faces_adj_faces = new int[this->n_renderlength];
		faces_adj_faces.resize(this->n_renderlength);
		for(int i=0;i<this->n_renderlength;i++) {
			faces_adj_faces[i] = -1;
		}
		//if( false )
		for(int i=0;i<n_polys;i++) { // for each face i
			for(int j=0;j<3;j++) { // for each edge j in i
				int vx0 = this->va_indices[i*3 + j];
				int vx1 = this->va_indices[i*3 + ((j+1)%3)];
				bool done = false; // while we haven't found the face adjacent to edge j
				//bool f = false;
				for(int i1=0;i1<i && !done;i1++) { // for each face i1
					//for(int i1=0;i1<n_polys && !done;i1++) { // for each face i1
					for(int j1=0;j1<3 && !done;j1++) { // for each edge j1 in i1
						int o_vx0 = this->va_indices[i1*3 + j1];
						int o_vx1 = this->va_indices[i1*3 + ((j1+1)%3)];
						//int o_vx2 = this->va_indices[i1*3 + ((j1+2)%3)];
						//Vector3D o_vec = this->getVertex(0, o_vx2);
						//if( ( vx0 == o_vx0 && vx1 == o_vx1 ) || ( vx0 == o_vx1 && vx1 == o_vx0 ) ) {
						if( vx0 == o_vx1 && vx1 == o_vx0 ) {
							//if( vx0 == o_vx0 && vx1 == o_vx1 ) {
							faces_adj_faces[3*i+j] = i1;
							faces_adj_faces[3*i1+j1] = i;
							done = true;
							//f = true;
							// check for degenerate faces
							/*bool any_degen = false;
							for(int fr=0;fr<this->n_frames && !any_degen;fr++) {
							float p_x = faces_planes[4*n_polys*fr + 4*i + 0];
							float p_y = faces_planes[4*n_polys*fr + 4*i + 1];
							float p_z = faces_planes[4*n_polys*fr + 4*i + 2];
							if( p_x == 0 && p_y == 0 && p_z == 0 ) {
							any_degen = true;
							}
							}
							if( any_degen ) {
							faces_adj_faces[3*i+j] = -1;
							faces_adj_faces[3*i1+j1] = -1;
							}*/
						}
					}
				}
			}
		}
		this->shadow_closed = true;
		//faces_boundary = new bool[n_polys];
		faces_boundary.resize(n_polys);
		for(int i=0;i<n_polys;i++) { // for each face i
			bool boundary = false;
			for(int j=0;j<3 && !boundary;j++) {
				int adj_face = this->faces_adj_faces[3*i+j];
				if( adj_face == -1 ) {
					boundary = true;
					this->shadow_closed = false;
				}
			}
			faces_boundary[i] = boundary;
		}
		/*if( closed ) {
			LOG("closed\n");
		}*/

		//edges_skip = new bool[3*n_polys];
		edges_skip.resize(3*n_polys);
		for(int i=0;i<3*n_polys;i++)
			edges_skip[i] = false;
		for(int i=0;i<n_polys;i++) { // for each face i
			for(int j=0;j<3;j++) { // for each edge j in i
				int adj_face = faces_adj_faces[3*i+j];
				if( adj_face != -1 && adj_face > i ) {
					int nj = (j+1)%3;
					int vx0 = this->getVaIndex(i*3 + j);
					int vx1 = this->getVaIndex(i*3 + nj);
					int adj_j = -1;
					for(int j1=0;j1<3 && adj_j == -1;j1++) {
						int adj_vx0 = this->getVaIndex(adj_face*3 + j1);
						int adj_vx1 = this->getVaIndex(adj_face*3 + ((j1+1)%3));
						if( vx0 == adj_vx1 && vx1 == adj_vx0 ) {
							adj_j = j1;
						}
					}
					if( adj_j == -1 ) {
						Vision::setError(new VisionException(this,VisionException::V_PROGRAMMING_ERROR, "Failure: couldn't find adjacent vertex index"));
					}

					bool all_parallel = true;
					for(int fr=0;fr<this->n_frames && all_parallel;fr++) {
						int indx = 4*n_polys*fr + 4*i;
						Vector3D normal( faces_planes[indx], faces_planes[indx+1], faces_planes[indx+2] );
						int adj_indx = 4*n_polys*fr + 4*adj_face;
						Vector3D adj_normal( faces_planes[adj_indx], faces_planes[adj_indx+1], faces_planes[adj_indx+2] );
						float dot_norm = normal % adj_normal;
						if( dot_norm + V_TOL_ANGULAR < 1.0 ) {
							all_parallel = false;
						}
					}
					if( all_parallel ) {
						//LOG("skip %d th edge of face %d adj face %d\n", j, i, adj_face);
						edges_skip[3*i+j] = true;
						edges_skip[3*adj_face+adj_j] = true;
					}
				}
			}
		}

		this->n_shadow_renderlength = 0;
#ifndef SHADOW_ALGORITHM_NEW
		for(int i=0;i<n_polys;i++) { // for each face i
			for(int j=0;j<3;j++) { // for each edge j in i
				int adj_face = faces_adj_faces[3*i+j];
				//if( adj_face != -1 && adj_face > i ) {
				if( adj_face != -1 && adj_face > i && !edges_skip[3*i+j] ) {
					int nj = (j+1)%3;
					int vx0 = this->getVaIndex(i*3 + j);
					int vx1 = this->getVaIndex(i*3 + nj);
					int adj_j = -1;
					int adj_nj = -1;
					for(int j1=0;j1<3 && adj_j == -1;j1++) {
						int adj_vx0 = this->getVaIndex(adj_face*3 + j1);
						int adj_vx1 = this->getVaIndex(adj_face*3 + ((j1+1)%3));
						if( vx0 == adj_vx1 && vx1 == adj_vx0 ) {
							adj_j = j1;
							adj_nj = (j1+1)%3;
						}
					}
					if( adj_j == -1 ) {
						Vision::setError(new VisionException(this,VisionException::V_PROGRAMMING_ERROR, "Failure: couldn't find adjacent vertex index"));
					}
					int v0 = 3*i+nj;
					int v1 = 3*i+j;
					int v2 = 3*adj_face+adj_nj;
					int v3 = 3*adj_face+adj_j;
					this->va_shadow_indices[n_shadow_renderlength++] = v0;
					this->va_shadow_indices[n_shadow_renderlength++] = v1;
					this->va_shadow_indices[n_shadow_renderlength++] = v2;
					this->va_shadow_indices[n_shadow_renderlength++] = v0;
					this->va_shadow_indices[n_shadow_renderlength++] = v2;
					this->va_shadow_indices[n_shadow_renderlength++] = v3;
					/*this->va_shadow_indices[n_shadow_renderlength++] = 3*i+nj;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*i+j;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*adj_face+adj_nj;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*adj_face+adj_j;*/

				}
				else if( adj_face == -1 ) {
					int nj = (j+1)%3;
					adj_face = i + n_polys;
					/*if( n_shadow_renderlength/6 == 19 ) {
						LOG("b\n");
					}*/
					this->va_shadow_indices[n_shadow_renderlength++] = 3*i+nj;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*i+j;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*adj_face+j;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*adj_face+nj;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*i+nj;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*adj_face+j;
					/*this->va_shadow_indices[n_shadow_renderlength++] = 3*i+nj;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*i+j;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*adj_face+j;
					this->va_shadow_indices[n_shadow_renderlength++] = 3*adj_face+nj;*/
				}
			}
		}
		LOG("shadow renderlength %d\n", n_shadow_renderlength);
#endif
#ifdef SHADOW_ALGORITHM_NEW
		// n_shadow_renderlength is set during rendering
		for(int i=0,c=0,e=0;i<n_polys;i++) { // for each face i
			for(int j=0;j<3;j++,e++) { // for each edge j in i
				this->va_shadow_indices[c++] = 4*e+0;
				this->va_shadow_indices[c++] = 4*e+2;
				this->va_shadow_indices[c++] = 4*e+1;
				this->va_shadow_indices[c++] = 4*e+1;
				this->va_shadow_indices[c++] = 4*e+2;
				this->va_shadow_indices[c++] = 4*e+3;
			}
		}
#endif
		/*for(int i=0;i<2*n_polys;i++) {
		for(int j=0;j<3;j++) {
		if( i < n_polys ) {
		this->va_shadow_normals[9*i + 3*j + 0] = this->faces_planes[4*n_polys*frame + 4*i + 0];
		this->va_shadow_normals[9*i + 3*j + 1] = this->faces_planes[4*n_polys*frame + 4*i + 1];
		this->va_shadow_normals[9*i + 3*j + 2] = this->faces_planes[4*n_polys*frame + 4*i + 2];
		}
		else {
		this->va_shadow_normals[9*i + 3*j + 0] = 0;
		this->va_shadow_normals[9*i + 3*j + 1] = 0;
		this->va_shadow_normals[9*i + 3*j + 2] = 0;
		}
		}
		}*/

		/*for(int i=0;i<3*3*n_polys;i++) {
		int indx = this->va_shadow_indices[i];
		LOG(">>> %d : %d\n", i, indx);
		}*/

		if( !need_va_indices ) {
			/*delete [] this->va_indices;
			this->va_indices = NULL;*/
			this->va_indices.clear();
		}

#ifdef SHADOW_ALGORITHM_NEW
		//this->shadow_edges = new unsigned short[6 * n_polys];
		//this->shadow_vertices_used = new bool[n_vertices];
		this->shadow_edges.resize(6*n_polys);
		this->shadow_vertices_used.resize(n_vertices);
#endif
	}

	size_t n_elements = n_frames * n_vertices;

	//bool need_tangents = false; // disabled for now
	//bool need_tangents = this->bump_texture != NULL;
	//if( need_tangents && va_normals != NULL && va_texcoords != NULL ) {
	if( need_tangents && va_normals.size() != 0 && va_texcoords.size() != 0 ) {
		//if( this->va_tangents == NULL ) {
		if( this->va_tangents.size() == 0 ) {
			// initialise lazily
			//this->va_tangents = new float[3 * n_elements];
			this->va_tangents.resize(3 * n_elements);
		}
		/*Vector3D *tan1 = new Vector3D[n_vertices * 2];
		Vector3D *tan2 = tan1 + n_vertices;*/
		vector<Vector3D> tan1(n_vertices);
		vector<Vector3D> tan2(n_vertices);
		for(int fr=0;fr<n_frames;fr++) {
			//memset(tan1, 0, n_vertices * sizeof(Vector3D) * 2);
			for(size_t i=0;i<n_vertices;i++) {
				tan1[i].set(0.0f, 0.0f, 0.0f);
				tan2[i].set(0.0f, 0.0f, 0.0f);
			}
			int n_polys = n_renderlength / 3;
			for(int i=0;i<n_polys;i++) {
				int i1 = this->getVaIndex(3*i);
				int i2 = this->getVaIndex(3*i+1);
				int i3 = this->getVaIndex(3*i+2);
				Vector3D vec1 = this->getVertex(fr, i1);
				Vector3D vec2 = this->getVertex(fr, i2);
				Vector3D vec3 = this->getVertex(fr, i3);
				Vector3D tex1 = this->getTexCoord(i1);
				Vector3D tex2 = this->getTexCoord(i2);
				Vector3D tex3 = this->getTexCoord(i3);

				float x1 = vec2.x - vec1.x;
				float x2 = vec3.x - vec1.x;
				float y1 = vec2.y - vec1.y;
				float y2 = vec3.y - vec1.y;
				float z1 = vec2.z - vec1.z;
				float z2 = vec3.z - vec1.z;

				float s1 = tex2.x - tex1.x;
				float s2 = tex3.x - tex1.x;
				float t1 = tex2.y - tex1.y;
				float t2 = tex3.y - tex1.y;

				if( s1 * t2 - s2 * t1 == 0.0 ) {
					Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "Error in calculating tangents"));
				}

				float r = 1.0f / (s1 * t2 - s2 * t1);
				Vector3D sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
					(t2 * z1 - t1 * z2) * r);
				Vector3D tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
					(s1 * z2 - s2 * z1) * r);

				tan1[i1] += sdir;
				tan1[i2] += sdir;
				tan1[i3] += sdir;

				tan2[i1] += tdir;
				tan2[i2] += tdir;
				tan2[i3] += tdir;
			}
			for(size_t i=0;i<n_vertices;i++) {
				Vector3D n = this->getNormal(fr, i);
				Vector3D t = tan1[i];

				// Gram-Schmidt orthogonalize
				Vector3D tangent = (t - n * (n % t));
				tangent.normalise();

				// Calculate handedness
				//tangent[a].w = (((n ^ t) % tan2[i]) < 0.0f) ? -1.0f : 1.0f;
				/*float hand = (((n ^ t) % tan2[i]) < 0.0f) ? -1.0f : 1.0f;
				LOG(">>> %d : %f\n", i, hand);
				if( hand < 0.0 )
					tangent = - tangent;*/
				this->setTangent(fr, i, tangent);
			}
		}
		//delete [] tan1;
	}

	// we upload all the frames: this is needed for hardware animation; even if not using hardware animation, we may use it if the frame fraction equals 0, so no interpolation is needed
	//ASSERT( va_vertices.size() > 0 );
	if( va_vertices.size() == 0 ) {
		LOG("va_vertices size %d not greater than zero\n", va_vertices.size());
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "invalid va_vertices size"));
	}
	vao_vertices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_VERTICES, 0, false, &va_vertices[0], 3*n_elements*sizeof(float), 3);

	if( n_frames > 1 && !this->uses_hardware_animation ) {
		// used for animation (interpolated between frames), when cannot be done in hardware
		//vao_vertices_stream = new VertexArray(true, va_vertices_stream, 3*n_vertices*sizeof(float));
		//vao_vertices_stream = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_VERTICES, 0, true, va_vertices_stream, 3*n_vertices*sizeof(float));
		//vao_vertices_stream = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_TEXCOORDS, 0, true, va_vertices_stream, 3*n_vertices*sizeof(float), 3);
		ASSERT( va_vertices_stream.size() > 0 );
		vao_vertices_stream = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_TEXCOORDS, 0, true, &va_vertices_stream[0], 3*n_vertices*sizeof(float), 3);
	}
	/*vao_normals = new VertexArray(false, va_normals, 3*n_elements*sizeof(float));
	vao_texcoords = new VertexArray(false, va_texcoords, 2*n_vertices*sizeof(float));
	vao_colors = new VertexArray(false, va_colors, (alpha_channel ? 4 : 3) * n_vertices*sizeof(unsigned char));
	vao_indices = new VertexArray(false, va_indices, n_renderlength*sizeof(unsigned short), true);*/
	//if( va_normals != NULL ) {
	if( va_normals.size() != 0 ) {
		/*for(int i=0;i<n_elements;i++) {
			LOG("%d : %f, %f, %f\n", i, va_normals[3*i], va_normals[3*i+1], va_normals[3*i+2]);
		}*/
		//vao_normals = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_NORMALS, 0, false, va_normals, 3*n_elements*sizeof(float), 3);
		vao_normals = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_NORMALS, 0, false, &va_normals[0], 3*n_elements*sizeof(float), 3);
	}
	//if( va_tangents != NULL )
	if( va_tangents.size() != 0 ) {
		//vao_tangents = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_TEXCOORDS, 0, false, va_tangents, 3*n_elements*sizeof(float), 3);
		vao_tangents = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_TEXCOORDS, 0, false, &va_tangents[0], 3*n_elements*sizeof(float), 3);
	}
	//if( va_texcoords != NULL )
	if( va_texcoords.size() != 0 ) {
		//vao_texcoords = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_TEXCOORDS, 0, false, va_texcoords, 2*n_vertices*sizeof(float), 2); // TODO: do we need to set the texture unit?
		vao_texcoords = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_TEXCOORDS, 0, false, &va_texcoords[0], 2*n_vertices*sizeof(float), 2); // TODO: do we need to set the texture unit?
	}
	//if( va_colors != NULL ) {
	if( va_colors.size() != 0 ) {
		//vao_colors = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_COLORS, 0, false, va_colors, (alpha_channel ? 4 : 3) * n_vertices*sizeof(unsigned char), (alpha_channel ? 4 : 3));
		vao_colors = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_COLORS, 0, false, &va_colors[0], getDim() * n_vertices*sizeof(unsigned char), getDim());
	}
	//if( va_indices != NULL )
	//vao_indices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_INDICES, 0, false, va_indices, n_renderlength*sizeof(unsigned short), 2); // we need a vao_indices object even if va_indices is null
	vao_indices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_INDICES, 0, false, va_indices.size() == 0 ? NULL : &va_indices[0], n_renderlength*sizeof(unsigned short), 2); // we need a vao_indices object even if va_indices is null
	// TODO: can we delete some of these va_normals/texcoords/colors/indices, if using VBOs? - but need a using_normals/texcoords/colors/indices flag first!

	if( need_shadows ) {
		/*vao_shadow_vertices = new VertexArray(true, va_shadow_vertices, 2 * 3 * n_polys * 3 * sizeof(float));
		vao_shadow_indices = new VertexArray(false, va_shadow_indices, n_shadow_renderlength*sizeof(unsigned short), true);*/
		ASSERT( va_shadow_vertices.size() > 0 );
		vao_shadow_vertices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_VERTICES, 0, true, &va_shadow_vertices[0], n_shadow_vertices * sizeof(float), 3);
#ifndef SHADOW_ALGORITHM_NEW
		vao_shadow_indices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_INDICES, 0, false, va_shadow_indices, n_shadow_renderlength*sizeof(unsigned short)); // original
#endif
#ifdef SHADOW_ALGORITHM_NEW
		//vao_shadow_indices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_INDICES, 0, false, NULL, 0); // without indices
		//vao_shadow_indices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_INDICES, 0, false, va_shadow_indices, n_shadow_indices*sizeof(unsigned short)); // with indices
		ASSERT( va_shadow_indices.size() > 0 );
		vao_shadow_indices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_INDICES, 0, true, &va_shadow_indices[0], n_shadow_indices*sizeof(unsigned short), 2); // newer - with streamed indices
#endif
	}

	/*if( RendererInfo::vertexBufferObjects ) {
		using_vbos = true;
	}*/
}

void RenderData::render(Renderer *renderer, GraphicsEnvironment *genv, int frame, int frame2, float frac, /*bool force_color, const Color *forced_color, unsigned char forced_alpha,*/ bool lighting_pass) {
	if( this->n_renderlength == 0 ) {
		return; // support for empty meshes
	}
	_DEBUG_CHECK_ERROR_
	{
		_DEBUG_CHECK_ERROR_
		/*if( texture != NULL ) {
			texture->enable();
			_DEBUG_CHECK_ERROR_
		}
		else {
			renderer->enableTexturing(false);
		}*/

		_DEBUG_CHECK_ERROR_
		/*if( secondary_texture != NULL ) {
			renderer->activeTextureUnit(TEXTURE_UNIT_SECONDARY);
			// secondary is disabled by default, unlike primary!
			renderer->enableTexturing(true);
			secondary_texture->enable();
			renderer->activeTextureUnit(TEXTURE_UNIT_0);
		}*/

		_DEBUG_CHECK_ERROR_
		/*if( bump_texture != NULL ) {
			// bump mapping only supported in shaders
			if( renderer->getActiveShader() != NULL ) {
				renderer->activeTextureUnit(TEXTURE_UNIT_BUMPMAP);
				renderer->enableTexturing(true);
				bump_texture->enable();
				renderer->activeTextureUnit(TEXTURE_UNIT_0);
			}
		}*/

		_DEBUG_CHECK_ERROR_
#if 0
		if( n_splats > 0 ) {
			// if n_splats==0, we shouldn't have to disable splats, as the shader shouldn't be expecting them
			const ShaderEffect *c_shader = renderer->getActiveShader();
			if( c_shader != NULL ) {
				//c_shader->SetUniformParameter1f("splat_detail_scale", this->splat_detail_scale);
				for(int i=0;i<n_splats;i++) {
					if( splat_textures[i] != NULL ) {
						renderer->activeTextureUnit(TEXTURE_UNIT_SPLATS[i]);
						renderer->enableTexturing(true);
						/*if( i == 0 ) {
							// alpha map shouldn't wrap!
							splat_textures[i]->setWrapMode(VI_Texture::WRAPMODE_CLAMP);
						}*/
						splat_textures[i]->enable();
						/*if( i == 0 ) {
							// alpha map shouldn't wrap!
							splat_textures[i]->clampToEdge();
						}*/
					}
				}
				renderer->activeTextureUnit(TEXTURE_UNIT_0);
			}
		}
#endif

		for(int i=0;i<n_textures;i++) {
			if( textures[i] != NULL ) {
				renderer->activeTextureUnit(i);
				renderer->enableTexturing(true);
				textures[i]->enable();
			}
		}
		if( n_textures > 0 ) {
			renderer->activeTextureUnit(0);
		}
		if( n_textures == 0 || textures[0] == NULL ) {
			renderer->enableTexturing(false); // 0 is on by default
		}

		_DEBUG_CHECK_ERROR_
		if( !this->solid ) {
			renderer->enableCullFace(false);
			renderer->setAlphaTestMode(Renderer::RENDERSTATE_ALPHA_TEST_ON);
		}
		if( blending /*|| splatting*/ ) {
			/*if( blend_glow || lighting_pass //|| splatting
				) {
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
			}
			else {
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
			}*/
			/*if( blend_type == V_BLENDTYPE_FADE_SRC )
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE); // "glow"
			else if( blend_type == V_BLENDTYPE_FADE_DST )
				renderer->setBlendMode(lighting_pass ? Renderer::RENDERSTATE_BLEND_BOTH : Renderer::RENDERSTATE_BLEND_ONE_BY_ONEMINUSSRCALPHA);
			else
				renderer->setBlendMode(lighting_pass ? Renderer::RENDERSTATE_BLEND_ADDITIVE : Renderer::RENDERSTATE_BLEND_TRANSPARENCY); // fade/normal transparency*/
			genv->getGraphics3D()->setBlend(blend_type, lighting_pass);
		}
		//va_normals = NULL;
		//if( va_normals == NULL ) {
		if( va_normals.size() == 0 ) {
			renderer->enableFixedFunctionLighting(false);
		}

		_DEBUG_CHECK_ERROR_
		//GraphicsEnvironment::checkGLError("RenderData::render about to prepareArrays");
		renderCore(renderer, genv, frame, frame2, frac/*, force_color, forced_color, forced_alpha*/);

		int n_polys = n_renderlength / 3;

		genv->updateStats(n_polys, n_renderlength, n_vertices, 1);

		/*if( texture == NULL ) {
			renderer->enableTexturing(true);
		}*/
		/*if( secondary_texture != NULL ) {
			renderer->activeTextureUnit(TEXTURE_UNIT_SECONDARY);
			renderer->enableTexturing(false);
			renderer->activeTextureUnit(TEXTURE_UNIT_0);
		}*/
		/*if( bump_texture != NULL ) {
			// bump mapping only supported in shaders
			if( renderer->getActiveShader() != NULL ) {
				renderer->activeTextureUnit(TEXTURE_UNIT_BUMPMAP);
				renderer->enableTexturing(false);
				renderer->activeTextureUnit(TEXTURE_UNIT_0);
			}
		}*/
		/*if( n_splats > 0 ) {
			if( renderer->getActiveShader() != NULL ) {
				for(int i=0;i<n_splats;i++) {
					renderer->activeTextureUnit(TEXTURE_UNIT_SPLATS[i]);
					renderer->enableTexturing(false);
				}
				renderer->activeTextureUnit(TEXTURE_UNIT_0);
			}
		}*/
		for(int i=1;i<n_textures;i++) {
			if( textures[i] != NULL ) {
				renderer->activeTextureUnit(i);
				renderer->enableTexturing(false);
			}
		}
		if( n_textures > 0 ) {
			renderer->activeTextureUnit(0);
		}
		if( n_textures == 0 || textures[0] == NULL ) {
			renderer->enableTexturing(true); // 0 is on by default
		}

		/*if( g3->cgProgram_vertex_shader != NULL ) {

		cgGLDisableProfile(g3->getGraphicsEnvironment()->cgVertexProfile);

		}*/


		if(!this->solid) {
			renderer->enableCullFace(true);
			renderer->setAlphaTestMode(Renderer::RENDERSTATE_ALPHA_TEST_OFF);
		}
		//if( va_normals == NULL ) {
		if( va_normals.size() == 0 ) {
			renderer->enableFixedFunctionLighting(true);
		}
	}

	//GraphicsEnvironment::checkGLError("RenderData::render exit");
	_DEBUG_CHECK_ERROR_
}

struct interpolateVertices_data {
	int start, end;
	RenderData *rd;
	float *vxs, *n_vxs;
	float frac;
};

void RenderData::interpolateVertices_thread(void *ptr) {
	interpolateVertices_data *data = (interpolateVertices_data *)ptr;
	int count = data->start * 3;
	for(int i=data->start;i<data->end;i++) {
		for(int j=0;j<3;j++) {
			data->rd->va_vertices_stream[count] = (1.0f - data->frac) * data->vxs[count] + data->frac * data->n_vxs[count];
			count++;
		}
	}
}

void RenderData::renderCore(Renderer *renderer, GraphicsEnvironment *genv, int frame, int frame2, float frac/*, bool force_color, const Color *forced_color, unsigned char forced_alpha*/) {
	float *vxs = &va_vertices[3*this->n_vertices*frame];
	float *n_vxs = NULL;
	//frame = 0;
	//frame2 = 1;
	//frac = 0.0; // test
	if( frac > 0.0 )
		n_vxs = &va_vertices[3*this->n_vertices*frame2];
	bool new_data = last_frame != frame || last_frac != frac;
	//new_data = true;

	//bool hardware_animation = false;
	/*const Shader *c_vertex_shader = renderer->getActiveVertexShader();
	//if( this->using_vbos && vbo_vertices != 0 && vbo_vertices_stream != 0 &&
	//if( this->using_vbos && vao_vertices->hasVBO() && vao_vertices_stream != NULL && vao_vertices_stream->hasVBO() &&
	if( vao_vertices->hasVBO() && vao_vertices_stream != NULL && vao_vertices_stream->hasVBO() &&
		genv->getVisionPrefs()->hardwarevertexanimation &&
		//g3->c_vertex_shader == g3->vertex_shader_ambient
		c_vertex_shader != NULL
		) {
			// can use hardware per-vertex animation
			hardware_animation = true;
			// disabled for now - ATI shader problems!
			// we should use specialised shaders for hardware animation (or something?)
	}*/

	if( n_vxs != NULL && new_data && !uses_hardware_animation ) {
		//for(int DEBUG=0;DEBUG<100;DEBUG++)
		{
			/*for(int i=0,count=0;i<this->n_vertices;i++) {
			for(int j=0;j<3;j++) {
			va_vertices_stream[count] = (1.0 - frac) * vxs[count] + frac * n_vxs[count];
			count++;
			}
			}*/
			const int n_threads_c = 2;
			interpolateVertices_data thread_data[n_threads_c];
			for(int i=0;i<n_threads_c;i++) {
				thread_data[i].rd = this;
				thread_data[i].vxs = vxs;
				thread_data[i].n_vxs = n_vxs;
				thread_data[i].frac = frac;
			}
			thread_data[0].start = 0;
			thread_data[0].end = this->n_vertices/2;
			thread_data[1].start = thread_data[0].end;
			thread_data[1].end = this->n_vertices;

			VI_ThreadFunction *funcs[n_threads_c];
			void *data[n_threads_c];
			for(int i=0;i<n_threads_c;i++) {
				funcs[i] = interpolateVertices_thread;
				data[i] = &thread_data[i];
			}
			createThreads(n_threads_c, funcs, data);

		}
	}

	//bool want_texcoords[MAX_TEXUNITS];
	//bool use_texcoords_array = false;
	// For OpenGL with shaders, we always need to specify values in TEXTURE_UNIT_POS1, even if they aren't being
	// used!
	// For hardware animation, this means when frac==0.0
	// When not using hardware_animation, this will cause problems on OpenGL (we could specify values there too,
	// but this causes problems with D3D9 due to having to support lots of extra vertex declaration combinations).
	// Normally this shouldn't be a problem - any hardware that supports shaders should also support VBOs, hence
	// we'll be using hardware animation anyway. Workaround if this isn't true is to use D3D9.
	// It seems a similar issue to the problem when not using texture maps on OpenGL - ideally we probably need to
	// be using different shaders.
	/*if( hardware_animation || this->va_tangents != NULL ) {
		// currently only support bump mapping without hardware animation (i.e., on objects with only 1 frame)?
		Renderer::VertexFormat vertexFormat = Renderer::VERTEXFORMAT_UNDEFINED;
		ASSERT( this->va_normals != NULL );
		ASSERT( this->va_texcoords != NULL );
		bool material_color = this->va_colors == NULL;
		bool have_texcoords0 = false;
		bool have_texcoords1 = false;
		if( hardware_animation && this->va_tangents != NULL ) {
			LOG("hardware animation and bump mapping not currently supported; will fail!\n");
			ASSERT(false);
		}
		else if( hardware_animation ) {
			vertexFormat = Renderer::VERTEXFORMAT_PNT0T1C;
			have_texcoords0 = true;
			have_texcoords1 = true;
		}
		else if( this->va_tangents != NULL ) {
			vertexFormat = Renderer::VERTEXFORMAT_PNT0T1C;
			have_texcoords0 = true;
			have_texcoords1 = true;
		}
		else {
			ASSERT( false );
		}
		renderer->startArrays(vertexFormat, material_color, have_texcoords0, have_texcoords1);
	}*/
	//if( this->va_tangents != NULL ) {
	if( this->va_tangents.size() != 0 ) {
		// to support bump mapping and hardware animation, need to add vertex formats for T1 and T2 texture coords
		if( uses_hardware_animation ) {
			LOG("invalid tangent_uv_channel %d\n", tangent_uv_channel);
			Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "hardware animation and tangents not yet supported"));
		}
		if( tangent_uv_channel != 1 ) {
			LOG("invalid tangent_uv_channel %d\n", tangent_uv_channel);
			Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "invalid tangent_uv_channel"));
		}
		Renderer::VertexFormat vertexFormat = Renderer::VERTEXFORMAT_PNT0T1C;
		/*ASSERT( this->va_normals != NULL );
		ASSERT( this->va_texcoords != NULL );*/
		ASSERT( this->va_normals.size() != 0 );
		ASSERT( this->va_texcoords.size() != 0 );
		//bool material_color = this->va_colors == NULL;
		/*bool material_color = this->va_colors.size() == 0;
		bool have_texcoords0 = true;
		bool have_texcoords1 = true;
		renderer->startArrays(vertexFormat, material_color, have_texcoords0, have_texcoords1);*/
		renderer->startArrays(vertexFormat);
	}
	else if( uses_hardware_animation ) {
		Renderer::VertexFormat vertexFormat = Renderer::VERTEXFORMAT_UNDEFINED;
		if( hardware_animation_channel != 1 ) {
			LOG("invalid hardware_animation_channel %d\n", hardware_animation_channel);
			Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "invalid hardware_animation_channel"));
		}
		if( this->va_normals.size() != 0 )
			vertexFormat = Renderer::VERTEXFORMAT_PNT0T1C;
		else
			vertexFormat = Renderer::VERTEXFORMAT_PT0T1C;
		renderer->startArrays(vertexFormat);
	}
	else {
		Renderer::VertexFormat vertexFormat = Renderer::VERTEXFORMAT_UNDEFINED;
		//bool material_color = this->va_colors == NULL;
		/*bool material_color = this->va_colors.size() == 0;
		//bool have_texcoords0 = this->va_texcoords != NULL;
		bool have_texcoords0 = this->va_texcoords.size() != 0;
		bool have_texcoords1 = false; // no hardware animation*/
		if( this->va_normals.size() != 0 )
			vertexFormat = Renderer::VERTEXFORMAT_PNT0C;
		else
			vertexFormat = Renderer::VERTEXFORMAT_PT0C;
		//renderer->startArrays(vertexFormat, material_color, have_texcoords0, have_texcoords1);
		renderer->startArrays(vertexFormat);
	}

	//if( this->va_normals != NULL ) {
	if( this->va_normals.size() != 0 ) {
		vao_normals->renderNormals(false, (3*sizeof(float)*n_vertices*frame));
	}

	//if( this->va_tangents != NULL ) {
	if( this->va_tangents.size() != 0 ) {
		//vao_tangents->renderTexCoords(false, (3*sizeof(float)*n_vertices*frame), TEXTURE_UNIT_TANGENTS);
		if( this->tangent_uv_channel < 0 ) {
			LOG("invalid tangent_uv_channel %d\n", tangent_uv_channel);
			Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "invalid tangent_uv_channel"));
		}
		vao_tangents->renderTexCoords(false, (3*sizeof(float)*n_vertices*frame), this->tangent_uv_channel);
	}

	//if( this->va_texcoords != NULL ) {
	if( this->va_texcoords.size() != 0 ) {
		vao_texcoords->renderTexCoords(false, 0);
	}

	/*if( this->va_colors != NULL && !forced_color ) {
		vao_colors->renderColors(this->alpha_channel, false, 0);
	}
	else if( !forced_color ) {
		//renderer->setColor4(cols[0], cols[1], cols[2], cols[3]);
		renderer->setColor4(255, 255, 255, 255);
	}*/
	// forced color currently disabled, until shader problems fixed
	/*if( this->va_colors != NULL ) {
		vao_colors->renderColors(this->alpha_channel, false, 0);
	}*/
	/*else {
		renderer->setColor4(255, 255, 255, 255);
	}*/
	/*float material_color[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	if( force_color ) {
		material_color[0] = forced_color->getRf();
		material_color[1] = forced_color->getGf();
		material_color[2] = forced_color->getBf();
		material_color[3] = ((float)forced_alpha)/(float)255.0f;
	}*/

	if( this->vao_colors != NULL ) {
		vao_colors->renderColors(false, 0);
	}

	//genv->getGraphics3D()->setShaderMatSpecular(material_specular);
	if( renderer->getActiveShader() != NULL ) {
		genv->getGraphics3D()->setShaderConstants(this);
	}
	else {
		genv->getGraphics3D()->setFixedFunctionConstants(this);
	}
	//genv->getGraphics3D()->setShaderMatColor(material_color);

	/*if( c_vertex_shader != NULL ) {
		c_vertex_shader->setHardwareAnimationAlpha( hardware_animation ? frac : 0.0f );
	}*/

	if( uses_hardware_animation ) {
		//genv->getRenderer()->getActiveShader()->SetUniformParameter1f("frame_alpha", frac);
		if( this->hardware_animation_channel < 0 ) {
			LOG("invalid hardware_animation_channel %d\n", hardware_animation_channel);
			Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "invalid hardware_animation_channel"));
		}
		vao_vertices->renderTexCoords(false, (3*sizeof(float)*n_vertices*frame2), hardware_animation_channel);
		vao_vertices->renderVertices(false, (3*sizeof(float)*n_vertices*frame));
	}
	else if( n_vxs != NULL ) {
		// no hardware animation, and need to interpolate
		vao_vertices_stream->renderVertices(new_data, 0);
		if( new_data ) {
			last_frame = frame;
			last_frac = frac;
		}
	}
	else {
		// no hardware animation, but don't need to interpolate
		vao_vertices->renderVertices(false, (3*sizeof(float)*n_vertices*frame));
	}

	// now render
	//LOG("doArrays()\n");
	VertexArray::DrawingMode mode = VertexArray::DRAWINGMODE_TRIANGLES;

	_DEBUG_CHECK_ERROR_
	vao_indices->renderElements(mode, n_renderlength, n_vertices);

	// now cleanup
	/*if( use_texcoords_array ) {
		renderer->endArraysMulti(this->vao_vertices->hasVBO());
	}
	else {
		renderer->endArrays(this->vao_vertices->hasVBO());
	}*/
	renderer->endArrays();

	/*if( Vision::compiledVAs ) // causes lighting problems on Voodoo Banshee!
	glUnlockArraysEXT();*/

	/*_DEBUG_CHECK_ERROR_
	vao_indices->endArrays(true, true, true, true, this->vao_vertices->hasVBO());

	_DEBUG_CHECK_ERROR_
	g3->getRenderer()->activeClientTextureUnit(TEXTURE_UNIT_POS1);
	_DEBUG_CHECK_ERROR_
	vao_indices->endArrays(false, false, true, false, false);
	_DEBUG_CHECK_ERROR_
	g3->getRenderer()->activeClientTextureUnit(TEXTURE_UNIT_TANGENTS);
	_DEBUG_CHECK_ERROR_
	vao_indices->endArrays(false, false, true, false, false);
	_DEBUG_CHECK_ERROR_
	g3->getRenderer()->activeClientTextureUnit(TEXTURE_UNIT_0);
	_DEBUG_CHECK_ERROR_*/
	//LOG("end prepareArrays()\n");
}

const int infinity_c = 100;

void RenderData::castShadowSetVisible(int frame, const Vector3D &lightpos, bool infinite, bool shader) {
	V__ENTER;
	//int n_polys = this->n_renderlength / 3;
	unsigned int n_polys = static_cast<unsigned int>(this->n_renderlength / 3); // cast to int to avoid overflow in calculations below
	//bool *visible = this->visible_polys;
	unsigned char *visible = &this->visible_polys[0];
	for(unsigned int i=0;i<n_polys && !shader;i++) {
		float p_x = this->faces_planes[4*n_polys*frame + 4*i + 0];
		float p_y = this->faces_planes[4*n_polys*frame + 4*i + 1];
		float p_z = this->faces_planes[4*n_polys*frame + 4*i + 2];
		float p_D = this->faces_planes[4*n_polys*frame + 4*i + 3];
		if( p_x == 0 && p_y == 0 && p_z == 0 ) {
			visible[i] = false;
			continue;
		}
		float side = p_x * lightpos.x + p_y * lightpos.y + p_z * lightpos.z;
		//LOG("%d : %f , %f , %f : %f , %f , %f : %f\n", i, p_x, p_y, p_z, lightpos->x, lightpos->y, lightpos->z, side);
		if( !infinite )
			side += p_D;
		visible[i] = side < 0;
		//visible[i] = side < -V_TOL_LINEAR;
		//visible[i] = side > 0;
		//visible[i] = side > -V_TOL_LINEAR;
	}
	/*for(int i=0;i<n_polys && !shader;i++) {
		LOG("visible %d : %d\n", i, visible[i]);
	}*/
	V__EXIT;
}

#ifndef SHADOW_ALGORITHM_NEW

struct castShadow_data {
	bool even;
	int n_shadow_vertices;
	RenderData *rd;
	int frame, frame2;
	float frac;
	bool shader;
	bool infinite;
	Vector3D *lightpos;
};

void castShadow_thread(void *ptr) {
	castShadow_data *data = (castShadow_data *)ptr;
	//printf("THREAD %s\n", data->even?"EVEN":"ODD");
	RenderData *rd = data->rd;
	int n_polys = rd->n_renderlength / 3;
	bool *visible = rd->visible_polys;
	int frame = data->frame;
	int frame2 = data->frame2;
	float frac = data->frac;
	bool shader = data->shader;
	bool infinite = data->infinite;
	Vector3D *lightpos = data->lightpos;

	data->n_shadow_vertices = 0;
	int n_iter = rd->shadow_closed ? n_polys/2 : n_polys;
	for(int ki=0;ki<n_iter;ki++) {
		int i = 2*ki + (data->even ? 0 : 1);
		int poly = i % n_polys;
		if( i >= n_polys && !rd->faces_boundary[poly] ) {
			continue;
		}
		for(int j=0;j<3;j++) {
			int pj = (j+2)%3;
			if( rd->edges_skip[3*poly+j] && rd->edges_skip[3*poly+pj] ) {
				continue;
			}
			int vx = rd->getVaIndex(poly*3 + j);
			Vector3D v = rd->getVertex(frame, vx);
			if( frac > 0.0 ) {
				Vector3D n_v = rd->getVertex(frame2, vx);
				v = v * (1.0f - frac) + n_v * frac;
			}
			if( !shader ) {
				if( i >= n_polys || !visible[poly] ) {
					// project
					if( infinite ) {
						v -= *lightpos * infinity_c;
					}
					else {
						v = ( v - *lightpos ) * infinity_c + v;
					}
				}
			}
			int new_n_verts = 3*i + j + 1;
			if( new_n_verts > data->n_shadow_vertices )
				data->n_shadow_vertices = new_n_verts;
			rd->va_shadow_vertices[9*i + 3*j + 0] = v.x;
			rd->va_shadow_vertices[9*i + 3*j + 1] = v.y;
			rd->va_shadow_vertices[9*i + 3*j + 2] = v.z;
			/*if( poly == 40 && j == 2 ) {
				//LOG("ping\n");
			}
			if( rd->edges_skip[3*poly+j] && rd->edges_skip[3*poly+pj] ) {
				rd->va_shadow_vertices[9*i + 3*j + 0] = -1001;
			}*/
			//LOG("%d, %d : %f, %f, %f\n", i, j, v.x, v.y, v.z);
			//if( i < n_polys && visible[poly] ) {
			if( shader )
			{
				if( i < n_polys ) {
					rd->va_shadow_normals[9*i + 3*j + 0] = rd->faces_planes[4*n_polys*frame + 4*i + 0];
					rd->va_shadow_normals[9*i + 3*j + 1] = rd->faces_planes[4*n_polys*frame + 4*i + 1];
					rd->va_shadow_normals[9*i + 3*j + 2] = rd->faces_planes[4*n_polys*frame + 4*i + 2];
				}
				else {
					rd->va_shadow_normals[9*i + 3*j + 0] = 0;
					rd->va_shadow_normals[9*i + 3*j + 1] = 0;
					rd->va_shadow_normals[9*i + 3*j + 2] = 0;
				}
			}
		}
	}

	//return 1;
}

int RenderData::castShadowConstruct(int frame, int frame2, float frac, Vector3D *lightpos, bool infinite, bool shader) {
	V__ENTER;
	//const bool multithreading = false;
	const bool multithreading = true;
	const int n_threads_c = 2;
	castShadow_data thread_data[n_threads_c];
	for(int i=0;i<n_threads_c;i++) {
		thread_data[i].even = i==0;
		thread_data[i].n_shadow_vertices = 0;
		thread_data[i].rd = this;
		thread_data[i].frame = frame;
		thread_data[i].frame2 = frame2;
		thread_data[i].frac = frac;
		thread_data[i].infinite = infinite;
		thread_data[i].lightpos = lightpos;
		thread_data[i].shader = shader;
	}

	//for(int DEBUG=0;DEBUG<100;DEBUG++)
	{

		if( multithreading ) {
			ThreadFunction *funcs[n_threads_c];
			void *data[n_threads_c];
			for(int i=0;i<n_threads_c;i++) {
				funcs[i] = castShadow_thread;
				data[i] = &thread_data[i];
			}
			createThreads(n_threads_c, funcs, data);
		}
		else {
			for(int i=0;i<n_threads_c;i++)
				castShadow_thread(&thread_data[i]);
		}

	}

	int n_shadow_vertices = thread_data[0].n_shadow_vertices;
	for(int i=1;i<n_threads_c;i++) {
		if( thread_data[i].n_shadow_vertices > n_shadow_vertices )
			n_shadow_vertices = thread_data[i].n_shadow_vertices;
	}

	V__EXIT;
	return n_shadow_vertices;
}
#endif

#ifdef SHADOW_ALGORITHM_NEW
void addShadowEdge(int *n_shadow_edges,unsigned short *shadow_edges, int v0, int v1) {
	for(int i=0;i<*n_shadow_edges;i++) {
		if( ( shadow_edges[2*i] == v0 && shadow_edges[2*i+1] == v1 ) ||
			( shadow_edges[2*i] == v1 && shadow_edges[2*i+1] == v0 ) )
		{
			if( *n_shadow_edges > 0 ) {
				shadow_edges[2*i] = shadow_edges[2*(*n_shadow_edges-1)];
				shadow_edges[2*i+1] = shadow_edges[2*(*n_shadow_edges-1)+1];
			}
			(*n_shadow_edges)--;
			return;
		}
	}
	shadow_edges[2*(*n_shadow_edges)] = v0;
	shadow_edges[2*(*n_shadow_edges)+1] = v1;
	(*n_shadow_edges)++;
}

int RenderData::findShadowEdges() {
	V__ENTER;
	for(size_t i=0;i<n_vertices;i++)
		shadow_vertices_used[i] = false;

	int n_polys = this->n_renderlength / 3;
	int n_shadow_edges = 0;
	/*for(int i=0;i<n_polys;i++) {
		if( this->visible_polys[i] ) {
			int v0 = getVaIndex(3*i);
			int v1 = getVaIndex(3*i+1);
			int v2 = getVaIndex(3*i+2);
			addShadowEdge(&n_shadow_edges, shadow_edges, v0, v1);
			addShadowEdge(&n_shadow_edges, shadow_edges, v1, v2);
			addShadowEdge(&n_shadow_edges, shadow_edges, v2, v0);
		}
	}*/
	for(int i=0;i<n_polys;i++) { // for each face i
		for(int j=0;j<3;j++) { // for each edge j in i
			int adj_face = faces_adj_faces[3*i+j];
			if( adj_face == -1 || adj_face > i )
			{
				//bool adj_visible = adj_face == -1 ? false : visible_polys[adj_face];
				bool adj_visible = adj_face == -1 ? false : (visible_polys[adj_face] != 0);
				//if( visible_polys[i] && !adj_visible ) {
				//if( visible_polys[i] != adj_visible ) {
				if( (visible_polys[i] != 0) != adj_visible ) {
					int nj = (j+1)%3;
					int v0 = getVaIndex(3*i+j);
					int v1 = getVaIndex(3*i+nj);
					if( visible_polys[i] ) {
						shadow_edges[2*n_shadow_edges] = v0;
						shadow_edges[2*n_shadow_edges+1] = v1;
					}
					else {
						shadow_edges[2*n_shadow_edges] = v1;
						shadow_edges[2*n_shadow_edges+1] = v0;
					}
					shadow_vertices_used[v0] = true;
					shadow_vertices_used[v1] = true;
					n_shadow_edges++;
				}
			}
		}
	}
	/*for(int i=0;i<n_shadow_edges;i++) {
		LOG("%d : %d -> %d\n", i, shadow_edges[2*i], shadow_edges[2*i+1]);
	}*/
	V__EXIT;
	return n_shadow_edges;
}

struct shadowExtrudeVertices_data {
	bool even;
	RenderData *rd;
	int frame, frame2;
	float frac;
	bool infinite;
	Vector3D lightpos;

	shadowExtrudeVertices_data() : even(false), rd(NULL), frame(0), frame2(0), frac(0.0f), infinite(false) {
	}
};

void RenderData::shadowExtrudeVertices_thread(void *ptr) {
	shadowExtrudeVertices_data *data = (shadowExtrudeVertices_data *)ptr;
	//printf("THREAD %s\n", data->even?"EVEN":"ODD");
	int n_vertices = data->rd->n_vertices;
	int frame = data->frame;
	bool infinite = data->infinite;
	const Vector3D lightpos = data->lightpos;
	int start = data->even ? 0 : 1;
	for(int i=start;i<n_vertices;i+=2) {
		if( data->rd->shadow_vertices_used[i] ) {
			Vector3D vec;
			vec.x = data->rd->va_vertices[3*n_vertices*frame + 3*i + 0];
			vec.y = data->rd->va_vertices[3*n_vertices*frame + 3*i + 1];
			vec.z = data->rd->va_vertices[3*n_vertices*frame + 3*i + 2];
			if( data->frac > 0.0 ) {
				Vector3D vec2;
				vec2.x = data->rd->va_vertices[3*n_vertices*data->frame2 + 3*i + 0];
				vec2.y = data->rd->va_vertices[3*n_vertices*data->frame2 + 3*i + 1];
				vec2.z = data->rd->va_vertices[3*n_vertices*data->frame2 + 3*i + 2];
				vec = vec * (1.0f - data->frac) + vec2 * data->frac;
			}
			Vector3D vec_x;
			if( infinite ) {
				vec_x = vec - lightpos * infinity_c;
			}
			else {
				vec_x = ( vec - lightpos ) * infinity_c + vec;
			}
			data->rd->va_shadow_vertices[3*i + 0] = vec.x;
			data->rd->va_shadow_vertices[3*i + 1] = vec.y;
			data->rd->va_shadow_vertices[3*i + 2] = vec.z;
			data->rd->va_shadow_vertices[3*(n_vertices+i) + 0] = vec_x.x;
			data->rd->va_shadow_vertices[3*(n_vertices+i) + 1] = vec_x.y;
			data->rd->va_shadow_vertices[3*(n_vertices+i) + 2] = vec_x.z;
		}
	}
}

void RenderData::castShadowConstructVertices(int frame, int frame2, float frac, const Vector3D &lightpos, bool infinite) {
	V__ENTER;
	//const bool multithreading = false;
	const bool multithreading = true;
	const int n_threads_c = 2;
	shadowExtrudeVertices_data thread_data[n_threads_c];
	for(int i=0;i<n_threads_c;i++) {
		thread_data[i].even = i==0;
		thread_data[i].rd = this;
		thread_data[i].frame = frame;
		thread_data[i].frame2 = frame2;
		thread_data[i].frac = frac;
		thread_data[i].infinite = infinite;
		thread_data[i].lightpos = lightpos;
	}

	if( multithreading ) {
		VI_ThreadFunction *funcs[n_threads_c];
		void *data[n_threads_c];
		for(int i=0;i<n_threads_c;i++) {
			funcs[i] = RenderData::shadowExtrudeVertices_thread;
			data[i] = &thread_data[i];
		}
		createThreads(n_threads_c, funcs, data);
	}
	else {
		for(int i=0;i<n_threads_c;i++)
			RenderData::shadowExtrudeVertices_thread(&thread_data[i]);
	}
	V__EXIT;
}

void RenderData::castShadowConstructIndices(int n_shadow_edges) {
	V__ENTER;
	/*for(int i=0;i<2*n_shadow_edges;) {
		int v = shadow_edges[i++];
		this->va_shadow_indices[n_shadow_renderlength] = v;
		this->va_shadow_indices[n_shadow_renderlength+1] = n_vertices+v;
		this->va_shadow_indices[n_shadow_renderlength+4] = n_vertices+v;

		v = shadow_edges[i++];
		this->va_shadow_indices[n_shadow_renderlength+2] = v;
		this->va_shadow_indices[n_shadow_renderlength+3] = v;
		this->va_shadow_indices[n_shadow_renderlength+5] = n_vertices+v;

		n_shadow_renderlength += 6;
	}*/
	for(int i=0;i<n_shadow_edges;i++) {
		int v0 = shadow_edges[2*i];
		int v1 = shadow_edges[2*i+1];

		this->va_shadow_indices[n_shadow_renderlength++] = v0;
		this->va_shadow_indices[n_shadow_renderlength++] = n_vertices+v0;
		this->va_shadow_indices[n_shadow_renderlength++] = v1;

		this->va_shadow_indices[n_shadow_renderlength++] = v1;
		this->va_shadow_indices[n_shadow_renderlength++] = n_vertices+v0;
		this->va_shadow_indices[n_shadow_renderlength++] = n_vertices+v1;
	}
	V__EXIT;
}

int RenderData::castShadowConstruct(int frame, int frame2, float frac, const Vector3D &lightpos, bool infinite, bool shader) {
	V__ENTER;
	int n_shadow_vertices = 0;
	int n_shadow_edges = this->findShadowEdges();

	this->castShadowConstructVertices(frame, frame2, frac, lightpos, infinite);

	/*for(int i=0;i<n_vertices;i++) {
		if( shadow_vertices_used[i] ) {
			// TODO: frame interpolation
			Vector3D vec;
			vec.x = this->va_vertices[3*n_vertices*frame + 3*i + 0];
			vec.y = this->va_vertices[3*n_vertices*frame + 3*i + 1];
			vec.z = this->va_vertices[3*n_vertices*frame + 3*i + 2];
			Vector3D vec_x;
			if( infinite ) {
				vec_x = vec - *lightpos * infinity_c;
			}
			else {
				vec_x = ( vec - *lightpos ) * infinity_c + vec;
			}
			this->va_shadow_vertices[3*i + 0] = vec.x;
			this->va_shadow_vertices[3*i + 1] = vec.y;
			this->va_shadow_vertices[3*i + 2] = vec.z;
			this->va_shadow_vertices[3*(n_vertices+i) + 0] = vec_x.x;
			this->va_shadow_vertices[3*(n_vertices+i) + 1] = vec_x.y;
			this->va_shadow_vertices[3*(n_vertices+i) + 2] = vec_x.z;
		}
	}*/

	n_shadow_vertices = 2 * n_vertices; // newer method
	n_shadow_renderlength = 0; // newer method

	this->castShadowConstructIndices(n_shadow_edges);
#if 0
	for(int i=0;i<n_shadow_edges;i++) {
		int v0 = shadow_edges[2*i];
		int v1 = shadow_edges[2*i+1];

		/*Vector3D vec0;
		vec0.x = this->va_vertices[3*n_vertices*frame + 3*v0 + 0];
		vec0.y = this->va_vertices[3*n_vertices*frame + 3*v0 + 1];
		vec0.z = this->va_vertices[3*n_vertices*frame + 3*v0 + 2];
		Vector3D vec1;
		vec1.x = this->va_vertices[3*n_vertices*frame + 3*v1 + 0];
		vec1.y = this->va_vertices[3*n_vertices*frame + 3*v1 + 1];
		vec1.z = this->va_vertices[3*n_vertices*frame + 3*v1 + 2];
		// TODO: frame interpolation
		Vector3D vec2;
		Vector3D vec3;
		if( infinite ) {
			vec2 = vec0 - *lightpos * infinity_c;
			vec3 = vec1 - *lightpos * infinity_c;
		}
		else {
			vec2 = ( vec0 - *lightpos ) * infinity_c + vec0;
			vec3 = ( vec1 - *lightpos ) * infinity_c + vec1;
		}

		// newer method
		this->va_shadow_vertices[3*v0 + 0] = vec0.x;
		this->va_shadow_vertices[3*v0 + 1] = vec0.y;
		this->va_shadow_vertices[3*v0 + 2] = vec0.z;

		this->va_shadow_vertices[3*v1 + 0] = vec1.x;
		this->va_shadow_vertices[3*v1 + 1] = vec1.y;
		this->va_shadow_vertices[3*v1 + 2] = vec1.z;

		this->va_shadow_vertices[3*(n_vertices+v0) + 0] = vec2.x;
		this->va_shadow_vertices[3*(n_vertices+v0) + 1] = vec2.y;
		this->va_shadow_vertices[3*(n_vertices+v0) + 2] = vec2.z;

		this->va_shadow_vertices[3*(n_vertices+v1) + 0] = vec3.x;
		this->va_shadow_vertices[3*(n_vertices+v1) + 1] = vec3.y;
		this->va_shadow_vertices[3*(n_vertices+v1) + 2] = vec3.z;*/

		this->va_shadow_indices[n_shadow_renderlength++] = v0;
		this->va_shadow_indices[n_shadow_renderlength++] = n_vertices+v0;
		this->va_shadow_indices[n_shadow_renderlength++] = v1;

		this->va_shadow_indices[n_shadow_renderlength++] = v1;
		this->va_shadow_indices[n_shadow_renderlength++] = n_vertices+v0;
		this->va_shadow_indices[n_shadow_renderlength++] = n_vertices+v1;

		// with indices
		/*this->va_shadow_vertices[3*n_shadow_vertices+0] = vec0.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec0.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec0.z;
		n_shadow_vertices++;
		this->va_shadow_vertices[3*n_shadow_vertices+0] = vec1.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec1.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec1.z;
		n_shadow_vertices++;
		this->va_shadow_vertices[3*n_shadow_vertices+0] = vec2.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec2.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec2.z;
		n_shadow_vertices++;
		this->va_shadow_vertices[3*n_shadow_vertices+0] = vec3.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec3.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec3.z;
		n_shadow_vertices++;*/

		/*
		// no indices
		this->va_shadow_vertices[3*n_shadow_vertices+0] = vec0.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec0.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec0.z;
		n_shadow_vertices++;
		this->va_shadow_vertices[3*n_shadow_vertices+0] = vec2.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec2.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec2.z;
		n_shadow_vertices++;
		this->va_shadow_vertices[3*n_shadow_vertices+0] = vec1.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec1.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec1.z;
		n_shadow_vertices++;

		this->va_shadow_vertices[3*n_shadow_vertices+0] = vec1.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec1.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec1.z;
		n_shadow_vertices++;
		this->va_shadow_vertices[3*n_shadow_vertices+0] = vec2.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec2.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec2.z;
		n_shadow_vertices++;
		this->va_shadow_vertices[3*n_shadow_vertices+0] = vec3.x;
		this->va_shadow_vertices[3*n_shadow_vertices+1] = vec3.y;
		this->va_shadow_vertices[3*n_shadow_vertices+2] = vec3.z;
		n_shadow_vertices++;
		*/

	}
#endif

	V__EXIT;
	return n_shadow_vertices;
}
#endif

void RenderData::castShadow(Renderer *renderer, GraphicsEnvironment *genv, int frame, int frame2, float frac, const Vector3D &lightpos, bool infinite, bool shader) {
	V__ENTER;
	_DEBUG_CHECK_ERROR_;
	int n_polys = this->n_renderlength / 3;
	//bool *visible = this->visible_polys;
	unsigned char *visible = &this->visible_polys[0];
	int n_shadow_vertices = 0;

	this->castShadowSetVisible(frame, lightpos, infinite, shader);

	//this->va_shadow_vertices = new float[3 * n_polys * 3 * sizeof(float)];
	//this->va_shadow_vertices = new float[2 * 3 * n_polys * 3 * sizeof(float)];
	/*
	n_shadow_vertices = 0;
	//for(int i=0;i<n_polys;i++) {
	for(int i=0;i<2*n_polys;i++) {
	int poly = i % n_polys;
	if( i >= n_polys && !faces_boundary[poly] ) {
	//LOG("!\n");
	continue;
	}
	for(int j=0;j<3;j++) {
	int vx = this->getVaIndex(poly*3 + j);
	Vector3D v = this->getVertex(frame, vx);
	if( frac > 0.0 ) {
	Vector3D n_v = this->getVertex(frame2, vx);
	v = v * (1.0 - frac) + n_v * frac;
	}
	if( !shader ) {
	if( i >= n_polys || !visible[poly] ) {
	// project
	if( infinite ) {
	v -= *lightpos * infinity_c;
	}
	else {
	v = ( v - *lightpos ) * infinity_c + v;
	}
	}
	}
	int new_n_verts = 3*i + j + 1;
	if( new_n_verts > n_shadow_vertices )
	n_shadow_vertices = new_n_verts;
	this->va_shadow_vertices[9*i + 3*j + 0] = v.x;
	this->va_shadow_vertices[9*i + 3*j + 1] = v.y;
	this->va_shadow_vertices[9*i + 3*j + 2] = v.z;
	//if( i < n_polys && visible[poly] ) {
	if( shader )
	{
	if( i < n_polys ) {
	this->va_shadow_normals[9*i + 3*j + 0] = this->faces_planes[4*n_polys*frame + 4*i + 0];
	this->va_shadow_normals[9*i + 3*j + 1] = this->faces_planes[4*n_polys*frame + 4*i + 1];
	this->va_shadow_normals[9*i + 3*j + 2] = this->faces_planes[4*n_polys*frame + 4*i + 2];
	}
	else {
	this->va_shadow_normals[9*i + 3*j + 0] = 0;
	this->va_shadow_normals[9*i + 3*j + 1] = 0;
	this->va_shadow_normals[9*i + 3*j + 2] = 0;
	}
	}
	}
	}
	*/
	
	_DEBUG_CHECK_ERROR_;
	n_shadow_vertices = castShadowConstruct(frame, frame2, frac, lightpos, infinite, shader);
#ifdef SHADOW_ALGORITHM_NEW
	//n_shadow_renderlength = n_shadow_vertices; // without indices
	//n_shadow_renderlength = 6*(n_shadow_vertices/4); // with indices
#endif

	/*for(int i=0;i<n_shadow_renderlength;i++) {
		int indx = va_shadow_indices[i];
		LOG("%d : %d : %f, %f, %f\n", i, indx, va_shadow_vertices[3*indx+0], va_shadow_vertices[3*indx+1], va_shadow_vertices[3*indx+2]);
	}*/
	this->renderShadow(renderer, genv, n_shadow_vertices);

	V__EXIT;
}

void RenderData::renderShadow(Renderer *renderer, GraphicsEnvironment *genv, int n_shadow_vertices) const {
	V__ENTER;

	_DEBUG_CHECK_ERROR_;
	ASSERT( vao_shadow_vertices != NULL );
	ASSERT( vao_shadow_indices != NULL );
	//renderer->startArrays(Renderer::VERTEXFORMAT_P, false, false, false);
	renderer->startArrays(Renderer::VERTEXFORMAT_P);
	vao_shadow_vertices->renderVertices(true, 0);
	/*float *vx_data = (float *)vao_shadow_vertices->data;
	printf(">>> %f\n", vx_data[3*2185]);
	printf(">>> %f\n", vx_data[3*2185+1]);
	printf(">>> %f\n", vx_data[3*2185+2]);*/

	/*genv->stats_poly_count += n_shadow_renderlength/3;
	genv->stats_vertex_count += n_shadow_renderlength;
	genv->stats_distinct_vertex_count += n_shadow_vertices;*/

	//this->va_shadow_indices = new unsigned short[3 * 3 * n_polys * sizeof(unsigned short)];
	_DEBUG_CHECK_ERROR_;
	vao_shadow_indices->updateIndices(); // newer method
	_DEBUG_CHECK_ERROR_;

	genv->updateStats(n_shadow_renderlength/2, n_shadow_renderlength, n_shadow_vertices, 1);
	vao_shadow_indices->renderElements(VertexArray::DRAWINGMODE_TRIANGLES, n_shadow_renderlength, n_shadow_vertices);
	renderer->endArrays();

	V__EXIT;
	return;

#if 0
	//bool *visible = NULL;
	/*int *vxarray_pos = new int[this->n_vertices];
	for(int i=0;i<n_vertices;i++)
	vxarray_pos[i] = -1;*/
	//int n_shadow_vertices = 0;
	n_shadow_vertices = 0;
	n_shadow_renderlength = 0;
	//glBegin(GL_TRIANGLES);
	//glBegin(GL_QUADS);
	for(int i=0;i<n_polys;i++) {
		if( visible[i] )
		{
			// visible to the light source
			for(int j=0;j<3;j++) { // for each edge
				int adj_face = this->faces_adj_faces[3*i+j];
				if( adj_face == -1 || !visible[adj_face] )
				{
					// this edge casts a shadow
					int vx0 = this->getVaIndex(i*3 + j);
					int vx1 = this->getVaIndex(i*3 + ((j+1)%3));
					Vector3D v0 = this->getVertex(frame, vx0);
					Vector3D v1 = this->getVertex(frame, vx1);
					Vector3D v2, v3;
					if( infinite ) {
						v2 = ( - *lightpos ) * infinity_c + v0;
						v3 = ( - *lightpos ) * infinity_c + v1;
					}
					else {
						v2 = ( v0 - *lightpos ) * infinity_c + v0;
						v3 = ( v1 - *lightpos ) * infinity_c + v1;
					}
					/*glBegin(GL_TRIANGLE_STRIP);
					glVertex3f(v3.x, v3.y, v3.z);
					glVertex3f(v1.x, v1.y, v1.z);
					glVertex3f(v2.x, v2.y, v2.z);
					glVertex3f(v0.x, v0.y, v0.z);
					glEnd();*/

					// quads
					/*glVertex3f(v3.x, v3.y, v3.z);
					glVertex3f(v1.x, v1.y, v1.z);
					glVertex3f(v0.x, v0.y, v0.z);
					glVertex3f(v2.x, v2.y, v2.z);*/

					// triangles
					/*glVertex3f(v3.x, v3.y, v3.z);
					glVertex3f(v1.x, v1.y, v1.z);
					glVertex3f(v2.x, v2.y, v2.z);

					glVertex3f(v2.x, v2.y, v2.z);
					glVertex3f(v1.x, v1.y, v1.z);
					glVertex3f(v0.x, v0.y, v0.z);*/

					/*va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+1;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+2;*/

					/*va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+1;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+3;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+3;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+1;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+2;*/

					/*va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+1;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+2;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+3;*/
					n_shadow_renderlength += 4;

					va_shadow_vertices[3*n_shadow_vertices] = v3.x;
					va_shadow_vertices[3*n_shadow_vertices+1] = v3.y;
					va_shadow_vertices[3*n_shadow_vertices+2] = v3.z;
					n_shadow_vertices++;
					va_shadow_vertices[3*n_shadow_vertices] = v1.x;
					va_shadow_vertices[3*n_shadow_vertices+1] = v1.y;
					va_shadow_vertices[3*n_shadow_vertices+2] = v1.z;
					n_shadow_vertices++;
					va_shadow_vertices[3*n_shadow_vertices] = v0.x;
					va_shadow_vertices[3*n_shadow_vertices+1] = v0.y;
					va_shadow_vertices[3*n_shadow_vertices+2] = v0.z;
					n_shadow_vertices++;
					va_shadow_vertices[3*n_shadow_vertices] = v2.x;
					va_shadow_vertices[3*n_shadow_vertices+1] = v2.y;
					va_shadow_vertices[3*n_shadow_vertices+2] = v2.z;
					n_shadow_vertices++;

					/*int pos0 = vxarray_pos[vx0];
					int pos1 = vxarray_pos[vx1];
					if( pos0 == -1 && pos1 == -1 ) {
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+3;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+2;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+1;
					vxarray_pos[vx0] = n_shadow_vertices;
					vxarray_pos[vx1] = n_shadow_vertices+2;
					}
					else if( pos0 != -1 && pos1 == -1 ) {
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+1;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices;
					va_shadow_indices[n_shadow_renderlength++] = pos0;
					va_shadow_indices[n_shadow_renderlength++] = pos0+1;
					vxarray_pos[vx1] = n_shadow_vertices;
					}
					else if( pos0 == -1 && pos1 != -1 ) {
					va_shadow_indices[n_shadow_renderlength++] = pos1+1;
					va_shadow_indices[n_shadow_renderlength++] = pos1;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices;
					va_shadow_indices[n_shadow_renderlength++] = n_shadow_vertices+1;
					vxarray_pos[vx0] = n_shadow_vertices;
					}
					else {
					va_shadow_indices[n_shadow_renderlength++] = pos1+1;
					va_shadow_indices[n_shadow_renderlength++] = pos1;
					va_shadow_indices[n_shadow_renderlength++] = pos0;
					va_shadow_indices[n_shadow_renderlength++] = pos0+1;
					}

					if( pos0 == -1 ) {
					va_shadow_vertices[3*n_shadow_vertices] = v0.x;
					va_shadow_vertices[3*n_shadow_vertices+1] = v0.y;
					va_shadow_vertices[3*n_shadow_vertices+2] = v0.z;
					n_shadow_vertices++;
					va_shadow_vertices[3*n_shadow_vertices] = v2.x;
					va_shadow_vertices[3*n_shadow_vertices+1] = v2.y;
					va_shadow_vertices[3*n_shadow_vertices+2] = v2.z;
					n_shadow_vertices++;
					}
					if( pos1 == -1 ) {
					va_shadow_vertices[3*n_shadow_vertices] = v1.x;
					va_shadow_vertices[3*n_shadow_vertices+1] = v1.y;
					va_shadow_vertices[3*n_shadow_vertices+2] = v1.z;
					n_shadow_vertices++;
					va_shadow_vertices[3*n_shadow_vertices] = v3.x;
					va_shadow_vertices[3*n_shadow_vertices+1] = v3.y;
					va_shadow_vertices[3*n_shadow_vertices+2] = v3.z;
					n_shadow_vertices++;
					}*/
				}
			}
		}
	}
	//glEnd();

	glEnableClientState(GL_VERTEX_ARRAY);
	/*if( this->using_vbos && vbo_shadow_vertices != 0 ) {
	glBindBufferARB( GL_ARRAY_BUFFER_ARB, vbo_shadow_vertices );
	glBufferSubDataARB( GL_ARRAY_BUFFER_ARB, 0, 3*n_shadow_vertices*sizeof(float), va_shadow_vertices );
	glVertexPointer(3, GL_FLOAT, 0, (char *)NULL);
	glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
	}
	else
	glVertexPointer(3, GL_FLOAT, 0, va_shadow_vertices);*/
	vao_shadow_vertices->renderVertices(true, NULL, 3*n_shadow_vertices*sizeof(float));
	//glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );

	//renderArrays( GL_TRIANGLES, vbo_shadow_indices, va_shadow_indices, n_shadow_renderlength, n_shadow_vertices );
	//renderArrays( GL_QUADS, vbo_shadow_indices, va_shadow_indices, n_shadow_renderlength, n_shadow_vertices );
	//renderArrays( GL_QUADS, 0, NULL, n_shadow_renderlength, n_shadow_vertices );
	glDrawArrays(GL_QUADS, 0, n_shadow_renderlength);
	//glDisableClientState(GL_VERTEX_ARRAY);
	vao_shadow_vertices->endArrays(false, false, false, true);
#endif

	_DEBUG_CHECK_ERROR_;
}
