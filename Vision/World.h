#pragma once

#include "Resource.h"
#include "VisionIface.h"
#include "Misc.h"
#include "Terrain.h"
#include "Entity.h"

class GraphicsEnvironment;
//class BSPTree;
class SpatialIndex;

//const int MAX_EXPLOSIONS = 32;

const int MAX_SECTORS = 1024;

/*class PortalSector {
public:
Vector3D low,high;
AbstractObject *object;

bool pvs[MAX_SECTORS];

PortalSector();
~PortalSector();
};

class Portals {
public:
int n_sectors;
PortalSector *sectors[MAX_SECTORS];
Portals();
~Portals();

void addPortal(AbstractObject *object,Vector3D low,Vector3D high);
void recalculate();
int findPortal(Vector3D *p);
void link(int i,int j);
};*/

class Fog : public Color {
	bool enabled;
public:
	enum FogType {
		FOG_LINEAR = 0,
		FOG_EXP,
		FOG_EXP2
	};
	FogType fogtype;
	float density,start,end;
	Fog() : Color() {
		this->density = 0.35f;
		this->start = 1.0f;
		this->end = 5.0f;
		this->fogtype = FOG_EXP;
		this->enabled = true;
	}
	void enable(bool enabled) {
		this->enabled = enabled;
	}
	bool isEnabled() const {
		return this->enabled;
	}
	void seti(int R,int G,int B) {
		Color::seti(R,G,B);
	}
	void seti(int R,int G,int B,FogType fogtype,float density,float start,float end) {
		Color::seti(R,G,B);
		this->fogtype=fogtype;
		this->density=density;
		this->start=start;
		this->end=end;
	}
	void setf(float R,float G,float B) {
		Color::setf(R,G,B);
	}
	void setf(float R,float G,float B,FogType fogtype,float density,float start,float end) {
		Color::setf(R,G,B);
		this->fogtype=fogtype;
		this->density=density;
		this->start=start;
		this->end=end;
	}
	//void draw() const;
};

class Sector {
public:
	SceneGraphNode *rootObject;
	SpatialIndex *spatialindex;

	Sector() {
		rootObject = NULL;
		spatialindex = NULL;
	}
};

class World : public VisionObject, public VI_World {
	//int timer;
	SceneGraphNode *defViewPoint;
	Sector sector;
	GraphicsEnvironment *genv;
	SceneGraphNode *viewPoint;
	TerrainEngine *terrain;
	SkyBox *skybox;
	Clouds *clouds;
	Atmosphere *atmosphere;
	Color lightAmbient;
	Fog fog;
	Color backColor;
public:

	World();
	virtual ~World();
	virtual V_CLASS_t getClass() const { return V_CLASS_WORLD; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_WORLD );
	}
	virtual size_t memUsage();

	void setGraphicsEnvironment(GraphicsEnvironment *genv);
	void removeAllObjects();
	virtual void update(int time_ms);
	virtual void init();
	virtual Sector *getSector() {
		return &this->sector;
	}
	virtual SceneGraphNode *getRootNode() {
		return this->sector.rootObject;
	}
	/*virtual void addShadowNode(SceneGraphNode *node);
	virtual void removeShadowNode(SceneGraphNode *node);*/
	/*virtual Vector *getShadowNodes() {
	return this->shadowNodes;
	}*/
	/*virtual vector<SceneGraphNode *> *getShadowNodes() {
		return this->shadowNodes;
	}*/
	/*virtual void checkUpdateNode(SceneGraphNode *node);
	virtual void removeUpdateNode(SceneGraphNode *node);*/

	const Color *getBackColor() const {
		return &this->backColor;
	}
	/*const Color *getGroundColor() const {
		return &this->groundColor;
	}*/
	const Fog *getFog() const {
		return &this->fog;
	}
	Fog *getFog() {
		return &this->fog;
	}
	/*bool isHorizonEnabled() const {
		return this->enableHorizon;
	}*/

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/

	virtual void getAmbience(float *r,float *g,float *b);
	virtual VI_SceneGraphNode *getViewpoint() {
		return this->viewPoint;
	}
	virtual const VI_SceneGraphNode *getViewpoint() const {
		return this->viewPoint;
	}

	virtual bool addNode(VI_SceneGraphNode *node);
	virtual void setSky(VI_Sky *sky);
	virtual VI_Sky *getSky() {
		return this->skybox;
	}
	virtual const VI_Sky *getSky() const {
		return this->skybox;
	}
	virtual void setClouds(VI_Clouds *clouds);
	virtual VI_Clouds *getClouds() {
		return this->clouds;
	}
	virtual const VI_Clouds *getClouds() const {
		return this->clouds;
	}
	virtual void setAtmosphere(VI_Atmosphere *atmosphere);
	virtual VI_Atmosphere *getAtmosphere() {
		return this->atmosphere;
	}
	virtual const VI_Atmosphere *getAtmosphere() const {
		return this->atmosphere;
	}
	/*virtual void setHorizon(bool enabled) {
		this->enableHorizon = enabled;
	}*/
	virtual void setBackColori(unsigned char r, unsigned char g, unsigned char b) {
		this->backColor.seti(r, g, b);
	}
	/*virtual void setGroundColori(unsigned char r, unsigned char g, unsigned char b) {
		this->groundColor.setf(r, g, b);
	}*/
	virtual void setTerrain(VI_Terrain *terrain);
	virtual VI_Terrain *getTerrain() {
		return this->terrain;
	}
	virtual const VI_Terrain *getTerrain() const {
		return this->terrain;
	}
	virtual void setAmbiencei(unsigned char r, unsigned char g, unsigned char b);
	virtual void setViewpoint(VI_SceneGraphNode *viewpoint);
	virtual void enableFog(bool enabled) {
		this->fog.enable(enabled);
	}
	virtual void setFogParmsExp(float density) {
		this->fog.density = density;
	}
};
