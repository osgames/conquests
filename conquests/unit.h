#pragma once

/** Contains Unit (an instantiation of a UnitTemplate).
*/

#define USE_LUA_UNIT_AI

#include "utils.h"

#include <vector>
using std::vector;

class MainGamestate;
class Civilization;
class UnitTemplate;
class Distance;

class Unit {
public:
	// n.b., enum Status fails to compile on Linux, when including X11 headers!
	enum UnitStatus {
		STATUS_UNDEFINED = -1,
		STATUS_NORMAL = 0,
		STATUS_FORTIFIED = 1,
		STATUS_BUILDING_ROAD = 2,
		STATUS_BUILDING_RAILWAYS = 3
	};
private:
#ifndef USE_LUA_UNIT_AI
	enum Decision {
		DECISION_UNDECIDED = -1,
		DECISION_BUILDCITY = 0,
		DECISION_WORK = 1,
		DECISION_ATTACKCITY = 2,
		DECISION_ATTACKUNITS = 3,
		DECISION_CONTACTUNITS = 4,
		DECISION_EXPLORE = 5,
		DECISION_CITY = 6
		//DECISION_ = ,
	};
#endif

	MainGamestate *mainGamestate;
	Civilization *civilization;
	const UnitTemplate *unit_template; // saved
	Pos2D pos; // saved
	Pos2D old_pos; // not saved
	//Pos2D last_target; // AI; not saved
	//int moves_used; // saved
	Rational moves_used;
	UnitStatus status; // saved
	bool is_veteran; // saved
	bool is_automated; // saved
	bool has_goto_target; // saved // n.b., if this is true, is_automated must be set to true also
	Pos2D goto_target; // saved
	bool mark; // not saved - temp flag
	bool is_moving; // not saved
	int moving_time_ms; // not saved
	//Pos2D start_pos; // not saved - used for AI to check where we started from
	Pos2D ai_start_pos; // not saved - used for AI to check for infinite loops
	int ai_move_count; // not saved - used for AI to check for infinite loops
	bool ai_has_dest; // not saved - used to cache AI destination
	Pos2D ai_dest;
	int n_bombed; // not saved - number of times bombed, used for testing

	//friend void MainGamestate::runTest(const char *filename, TestID index); // so runTest can access private AI functions
	//friend class MainGamestate; // so runTest can access private AI functions
#ifndef USE_LUA_UNIT_AI
	//bool doAIAir();
	AIInterface::UnitAction doAIAir(Pos2D *target_square);
	//bool doAILand();
	AIInterface::UnitAction doAILand(Pos2D *target_square);
#endif
public:
	Unit(MainGamestate *mainGamestate, Civilization *civilization,const UnitTemplate *unit_template, Pos2D pos);
	Unit(MainGamestate *mainGamestate, Civilization *civilization);
	~Unit();

	static Unit *load(MainGamestate *mainGamestate, Civilization *civilization, FILE *file);
	void save(FILE *file) const;

	int getX() const {
		return pos.x;
	}
	int getY() const {
		return pos.y;
	}
	Pos2D getPos() const {
		return pos;
	}
	void setPos(Pos2D newPos, bool handle_exploration);
	void setPos(Pos2D newPos) {
		setPos(newPos, true);
	}
	void setAIMoveCount() {
		this->ai_move_count = 0;
		this->ai_start_pos = pos;
	}
	void setAIDest(){
		this->ai_has_dest = false;
	}
	void setAIDest(Pos2D ai_dest){
		this->ai_has_dest = true;
		this->ai_dest = ai_dest;
	}
	int getNBombed() const {
		return n_bombed;
	}
	void incNBombed() {
		this->n_bombed++;
	}
	bool isMoving() const {
		return is_moving;
	}
	void setMoving();
	void getMovingPosition(float *mx, float *my);
	Civilization *getCivilization() const {
		// civilization isn't owned by this class, so okay to return non-const in const member function
		return civilization;
	}
	void setCivilization(Civilization *civilization) {
		this->civilization = civilization;
	}
	UnitStatus getStatus() const {
		return status;
	}
	void setStatus(UnitStatus status);
	bool isVeteran() const {
		return is_veteran;
	}
	void setVeteran(bool is_veteran) {
		this->is_veteran = is_veteran;
	}
	bool isAutomated() const {
		return is_automated;
	}
	void setAutomated(bool is_automated) {
		this->is_automated = is_automated;
		this->has_goto_target = false; // use setGotoTarget() if want to set this instead
	}
	bool hasGotoTarget(Pos2D *goto_target) const {
		if( this->has_goto_target ) {
			*goto_target = this->goto_target;
			return true;
		}
		return false;
	}
	void setGotoTarget(Pos2D goto_target) {
		this->is_automated = true;
		this->has_goto_target = true;
		this->goto_target = goto_target;
	}
	void unsetGotoTarget() {
		this->is_automated = false;
		this->has_goto_target = false;
	}
	bool isMarked() const {
		return mark;
	}
	void setMark(bool mark) {
		this->mark = mark;
	}
	//void moveTo(Pos2D pos);
	bool canMoveTo(Pos2D newPos, bool checkEnemies, bool checkTerritory) const;
	/*bool canMoveTo(Pos2D newPos) const {
		return canMoveTo(newPos, true);
	}*/
	bool hasMovesLeft() const;
	/*int movesUsed() const {
		return moves_used;
	}*/
	Rational movesUsed() const {
		return moves_used;
	}
	Rational movesLeft() const;
	void useMove(int n) {
		moves_used += n;
		/*if( moves_used < 0 )
			moves_used = 0;*/
	}
	void useMove(Rational n) {
		moves_used += n;
	}
	void useMoves();
	const UnitTemplate *getTemplate() const {
		return unit_template;
	}
	void reset();
	const UnitTemplate *canUpgrade() const;
	void update();
	bool automate();

	// AI functions
	bool doAI();
	bool automateBuildRoads(int *dist, vector<Pos2D> *candidates, const Distance *dists) const;
	bool moveTowardsAI(int target_x, int target_y) {
		return moveTowardsAI(target_x, target_y, true, true);
	}
	bool moveTowardsAI(int target_x, int target_y, bool allow_travel, bool allow_combat);
};
