#include "conquests_stdafx.h"

#include "optionsgamestate.h"
#include "maingamestate.h"

#include "../Vision/VisionIface.h"

#include <sstream>
using std::stringstream;

OptionsGamestate *OptionsGamestate::optionsGamestate = NULL;

void OptionsGamestate::action(VI_Panel *source) {
	if( source == optionsGamestate->startgameButton ) {
		MainGamestate *mainGamestate = new MainGamestate();
		//if( !mainGamestate->initGameData(true) ) {
		if( !game_g->setupNewGame(mainGamestate) ) {
			VI_log("failed to setup new game\n");
			delete mainGamestate;
		}
		else {
			game_g->pushNewGamestate(mainGamestate);
		}
	}
	else if( source == optionsGamestate->loadgameButton ) {
		MainGamestate *mainGamestate = new MainGamestate();
		if( !mainGamestate->loadGame() ) {
			VI_log("failed to load game (or player cancelled)\n");
			delete mainGamestate;
		}
		else {
			game_g->pushNewGamestate(mainGamestate);
		}
	}
	else if( source == optionsGamestate->quitgameButton ) {
		optionsGamestate->quitGame();
	}
}

bool OptionsGamestate::start() {
	game_g->setCursor(cursor_wait);
	try {
		VI_Font *font = game_g->getFont();

		//game_g->getSound()->playMedia("conquests/data/music/Before the Battle.ogg");
		game_g->getSound()->playMedia(getFullPath("data/music/Before the Battle.ogg").c_str());

		gamePanel = VI_createPanel();
		//gamePanel->setPaintFunc(paintPanel, NULL);
		game_g->getGraphicsEnvironment()->setPanel(gamePanel);

		//imageTexture = VI_createTexture("conquests/data/gfx/intro.jpg");
		imageTexture = VI_createTexture(getFullPath("data/gfx/intro.jpg").c_str());
		/*VI_Image *image = VI_createImage("conquests/data/gfx/intro.jpg");
		if( !image->scale(game_g->getGraphicsEnvironment()->getWidth(), game_g->getGraphicsEnvironment()->getHeight()) ) {
			VI_log("failed to scale background image\n");
			delete image;
			game_g->setCursor(cursor_arrow);
			return false;
		}
		VI_Texture *image_texture = VI_createTexture(image);*/
		imageButton = VI_createImageButton(imageTexture, game_g->getGraphicsEnvironment()->getWidth(), game_g->getGraphicsEnvironment()->getHeight());
		gamePanel->addChildPanel(imageButton, 0, 0);

		VI_Font *largefont = game_g->getLargefont();
		stringstream titletext;
		titletext << "Conquests   (v" << versionMajor << "." << versionMinor << ")";
		titleButton = VI_createButton(titletext.str().c_str(), largefont);
		titleButton->setForeground(1.0f, 1.0f, 0.0f);
		titleButton->setBackground(0.0f, 0.0f, 0.0f);
		titleButton->setBorder(false);
		gamePanel->addChildPanel(titleButton, 48, game_g->getGraphicsEnvironment()->getHeight() - 64);

		int xpos = 48;
		int ypos = 80;
		const int ydiff = 32;

		startgameButton = VI_createButton("New Game", font);
		initButton(startgameButton);
		startgameButton->setAction(action);
		startgameButton->setKeyShortcut(V_N);
		startgameButton->setPopupText("Start playing a new game (N)", font);
		gamePanel->addChildPanel(startgameButton, xpos, ypos);
		ypos += ydiff;

		loadgameButton = VI_createButton("Load Game", font);
		initButton(loadgameButton);
		loadgameButton->setAction(action);
		loadgameButton->setKeyShortcut(V_L);
		loadgameButton->setPopupText("Load a previously saved game (L)", font);
		gamePanel->addChildPanel(loadgameButton, xpos, ypos);
		ypos += ydiff;

		quitgameButton = VI_createButton("Exit", font);
		initButton(quitgameButton);
		quitgameButton->setAction(action);
		quitgameButton->setPopupText("Exit the game", font);
		gamePanel->addChildPanel(quitgameButton, xpos, ypos);
		ypos += ydiff;

		/*VI_World *world = VI_createWorld();

		// setup world here

		game_g->getGraphicsEnvironment()->linkWorld(world);*/
		//game_g->getGraphicsEnvironment()->linkWorld(NULL);

		VI_check();
		VI_debug_mem_usage_all();

		game_g->getGraphicsEnvironment()->setVisible(true);
		game_g->getGraphicsEnvironment()->setFade(fade_duration_c, 0, 255);
	}
	catch(VisionException *ve) {
		VI_log("OptionsGamestate::start failed\n");
		delete ve;
		game_g->setCursor(cursor_arrow);
		return false;
	}

	game_g->setCursor(cursor_arrow);
	return true;
}

void OptionsGamestate::stop() {
	game_g->setCursor(cursor_wait);
	VI_flush(0); // delete all the gamestate objects, but leave the game level objects (which should be set at persistence level -1)
	VI_GraphicsEnvironment *genv = game_g->getGraphicsEnvironment();
	game_g->getGraphicsEnvironment()->setPanel(NULL); // as the main panel is now destroyed
	game_g->getSound()->stopMedia();
	game_g->setCursor(cursor_arrow);
}

void OptionsGamestate::quitGame() {
	VI_log("OptionsGamestate::quitGame()\n");
	quit = true;
}

void OptionsGamestate::update() {
}
