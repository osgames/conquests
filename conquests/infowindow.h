#pragma once

#include <vector>
using std::vector;
#include <string>
using std::string;

const int infowindow_def_x_c = 128;
const int infowindow_def_y_c = 64;
//const int infowindow_def_w_c = 480;
//const int infowindow_def_h_c = 140;
const int infowindow_def_w_c = 440;
//const int infowindow_def_h_c = 440;
//const int infowindow_def_h_c = 400;
const int infowindow_def_h_c = 256;

class InfoWindow;

class VI_GraphicsEnvironment;
class VI_Graphics2D;
class VI_Font;
class VI_Texture;
class VI_Panel;
class VI_Button;
class VI_Textfield;
class VI_ImageButton;

typedef void (InfoWindow_Update) (InfoWindow *infoWindow);
typedef void (InfoWindow_Action) (InfoWindow *infoWindow);

class InfoWindow {
	VI_GraphicsEnvironment *genv;
	VI_Panel *panel;
	/*VI_Button **buttons;
	int n_buttons;*/
	vector<VI_Button *> buttons;
	VI_Panel *hiddenCloseButton;
	VI_Textfield *textfield;
	VI_ImageButton *image_button;
	//static InfoWindow *activeWindow;
	bool is_modal;
	bool close;
	int close_value;
	InfoWindow_Update *update;
	InfoWindow_Action *close_action;
	void *userData;

	static void paintPanel(VI_Graphics2D *g2d, void *data);
	static void action(VI_Panel *source);

	void init(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, VI_Texture *image, const string *button_texts, int n_buttons, int x, int y, int w, int h, bool allow_multiline);
public:
	InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, VI_Texture *image, const string *button_texts, int n_buttons, int x, int y, int w, int h, bool allow_multiline);
	InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, const string *button_texts, int n_buttons, int x, int y, int w, int h, bool allow_multiline);
	InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, const string *button_texts, int n_buttons, int x, int y, int w, int h);
	InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, int x, int y, int w, int h);
	InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture);
	~InfoWindow();

	void createHiddenEscapeClose();
	const VI_Panel *getPanel() const {
		return panel;
	}
	VI_Panel *getPanel() {
		return panel;
	}
	const VI_Button *getButton(size_t index) const;
	VI_Button *getButton(size_t index);
	void setCloseAction(InfoWindow_Action *close_action) {
		this->close_action = close_action;
	}
	virtual void setUserData(void *userData) {
		this->userData = userData;
	}
	virtual void *getUserData() const {
		return userData;
	}
	/*static InfoWindow *getActiveWindow() {
		return activeWindow;
	}
	static void clearActiveWindow() {
		activeWindow = NULL;
	}*/
	void setUpdate(InfoWindow_Update update) {
		this->update = update;
	}

	int doModal();

	static bool confirm(VI_GraphicsEnvironment *genv, VI_Panel *parent, VI_Font *font, VI_Texture *panel_texture, string text, string yes, string no);
	static int select(VI_GraphicsEnvironment *genv, VI_Panel *parent, VI_Font *font, VI_Texture *panel_texture, string text, string yes, string no, int window_h, const vector<string> *list);
};
