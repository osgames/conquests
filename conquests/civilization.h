#pragma once

/** Represents a Civilization, together with AI routines. Also stores Race, which defines particular properties
*   of a Civilization that do not change during the game; and Relationship, which defines the statuses between
*   two civilizations.
*/

#include "common.h"
#include "utils.h"

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <map>
using std::map;

class MainGamestate;
class Civilization;
class City;
class Technology;
class Unit;

class VI_Texture;

class Race {
	string name;
	string name_adj;
	string name_leader;
	string info;
	unsigned char rgb[3];
	VI_Texture *texture;
	vector<string> city_names;
	const Technology *starting_technology;
	bool always_hostile;
	bool dummy;
public:
	Race(const char *name, const char *name_adj, const char *name_leader, unsigned char rgb[3], const char *image_filename);
	virtual ~Race();

	const char *getName() const {
		return this->name.c_str();
	}
	const char *getNameAdjective() const {
		return this->name_adj.c_str();
	}
	const char *getNameLeader() const {
		return this->name_leader.c_str();
	}
	virtual string getHelp(bool html=false) const;
	const unsigned char *getColor() const {
		return this->rgb;
	}
	VI_Texture *getTexture() const {
		// n.b., return non-const needed due to draw functions being non-const
		return this->texture;
	}
	void setStartingTechnology(const Technology *starting_technology) {
		this->starting_technology = starting_technology;
	}
	const Technology *getStartingTechnology() const {
		return this->starting_technology;
	}
	bool isAlwaysHostile() const {
		return always_hostile;
	}
	void setAlwaysHostile(bool always_hostile) {
		this->always_hostile = always_hostile;
	}
	bool isDummy() const {
		return dummy;
	}
	void setDummy(bool dummy) {
		this->dummy = dummy;
	}
	bool equals(const char *name) const {
		bool equal = this->name == name;
		return equal;
	}
	void addCityName(const char *city_name) {
		this->city_names.push_back(city_name);
	}
	string makeCityName(int *looped, size_t *index) const;
	void advanceCityName(int *looped, size_t *index) const;
	void setInfo(const char *info) {
		this->info = info;
	}
};

class Relationship {
	MainGamestate *mainGamestate;
	Civilization *civ1, *civ2; // saved
public:
	// n.b., enum Status fails to compile on Linux, when including X11 headers!
	enum RelationshipStatus {
		STATUS_PEACE = 0,
		STATUS_WAR = 1
	};
protected:
	bool made_contact; // saved
	RelationshipStatus status; // saved
	bool has_right_of_passage; // saved
	int year_declared_war; // saved
	int year_civ1_allows_rop; // earliest year when civ1 considers rop with civ2 (-1 if always okay) // saved
	int year_civ2_allows_rop; // earliest year when civ1 considers rop with civ2 (-1 if always okay) // saved

	Relationship(MainGamestate *mainGamestate);
public:
	Relationship(MainGamestate *mainGamestate, Civilization *civ1, Civilization *civ2);

	static Relationship *load(MainGamestate *mainGamestate, FILE *file);
	void save(FILE *file) const;

	bool equals(const Civilization *civ1, const Civilization *civ2) const {
		if( ( this->civ1 == civ1 && this->civ2 == civ2 ) ||
			( this->civ1 == civ2 && this->civ2 == civ1 ) ) {
				return true;
		}
		return false;
	}
	bool madeContact() const {
		return made_contact;
	}
	void makeContact();
	RelationshipStatus getStatus() const {
		return status;
	}
	void setStatus(RelationshipStatus status);
	bool hasRightOfPassage() const {
		return has_right_of_passage;
	}
	bool canMakeRightOfPassage() const;
	void setRightOfPassage(bool has_right_of_passage);
	int getYearDeclaredWar() const;
	int getYearConsiderROP(const Civilization *civ) const;
};

class History {
public:
	int power;
	int population;
	int production;
	int science;
	int territory;
	int militarypower;

	History() : power(0), population(0), production(0), science(0), territory(0), militarypower(0) {
	}

	int getScore(HistoryType history_type) const;
};

class Civilization {
public:
	// n.b., the values of each enum must not be changed, in order to provide backwards compatibility with save game files
	enum Bonus {
		BONUS_TECH = 0,
		BONUS_MILITARY = 1,
		BONUS_GROWTH = 2,
		BONUS_PRODUCTION = 3,
		BONUS_WORKERS = 4,
		N_BONUSES = 5
	};
private:
	MainGamestate *mainGamestate;
	const Race *race; // race name saved
	vector<City *> cities; // saved
	vector<const Technology *> technologies; // saved
	vector<Unit *> units; // saved
	//bool is_dead; // TODO: save

	int progress_technology; // saved
	const Technology *technology; // saved

	int power; // saved

	map<int, History> history; // saved

	int city_name_looped; // saved
	size_t city_name_index; // saved

	int map_width, map_height; // not saved
    bool *map_visible; // saved
	bool *fog_of_war; // saved

	bool done_ai; // not saved
	bool has_calculated_fow; // calculated fog of war for AI in this turn? - not saved

	int bonus_turns[N_BONUSES]; // saved

	// civilization-wide bonuses (from city improvements that affect entire civilization, not just that city)
	// calculated per turn, and stored for efficiency
	int production_bonus; // not saved
	int defence_bonus; // as percentage bonus, e.g., 50 means +50% // not saved
	bool auto_veteran_bonus; // not saved
	int research_multiplier; // as percentage, e.g., 50 means +50% // not saved

	void checkDead();
	bool setFogOfWar(int x,int y);
	bool setFogOfWar(int x,int y,bool visible);
	void setMapVisible(int x,int y,bool visible);

	Civilization(MainGamestate *mainGamestate);
public:
	Civilization(MainGamestate *mainGamestate, const Race *race);
	~Civilization();

	static Civilization *load(MainGamestate *mainGamestate, FILE *file);
	void save(FILE *file) const;

	bool isDead() const;
	const char *getName() const {
		return this->race->getName();
	}
	const char *getNameAdjective() const {
		return this->race->getNameAdjective();
	}
	const unsigned char *getColor() const {
		return this->race->getColor();
	}
	VI_Texture *getTexture() const {
		// n.b., return non-const needed due to draw functions being non-const
		return this->race->getTexture();
	}
	const Race *getRace() const {
		return this->race;
	}
	string makeCityName();
	void advanceCityName();

	void addCity(City *city);
	void removeCity(const City *city);
	size_t getNCities() const;
	const City *getCity(size_t i) const;
	City *getCity(size_t i);

	void addUnit(Unit *unit);
	void removeUnit(const Unit *unit, bool check_dead);
	void removeUnit(const Unit *unit) {
		removeUnit(unit, true);
	}
	size_t getNUnits() const;
	const Unit *getUnit(size_t i) const;
	Unit *getUnit(size_t i);
	/*const vector<Unit *> *getUnits() const {
		return &units;
	}*/

	bool hasTechnology(const Technology *technology) const;
	bool hasTechnology(const char *technology) const;
	bool hasAllTechnology() const;
	/*const vector<const Technology *> *getTechnologies() const {
		return &technologies;
	}*/
	int getNTechnologies() const {
		return technologies.size();
	}
	const Technology *getTechnology(int i) {
		return technologies.at(i);
	}
	bool canResearch(const Technology *technology, bool terrain_restriction) const;
	const Technology *getTechnology() const {
		return technology;
	}
	void setTechnology(const Technology *technology) {
		this->technology = technology;
	}
	Age getAge() const;
	void removeAllTechnologies();
	void addTechnology(const Technology *tech);
	bool updateProgressTechnology(int update);
	int getProgressTechnology() const {
		return progress_technology;
	}
	int getPower() const {
		return power;
	}
	int calculatePowerPerTurn() const;
	void updatePower(int update);
	bool hasBonus(Bonus bonus) const;

	int getCivBonusProduction() const {
		return production_bonus;
	}
	int getCivBonusDefence() const {
		return defence_bonus;
	}
	bool getCivBonusAutoVeteran() const {
		return auto_veteran_bonus;
	}
	int getCivBonusResearchMultiplier() const {
		return research_multiplier;
	}

	int calculateTotalScience() const;
	int calculateTotalProduction() const;
	int calculateTotalPopulation() const;
	string getTotalPopulationString() const;
	int calculateTerritory() const;
	int calculateMilitary(int *n_units) const;
	int calculateAir(int *n_units) const;
	int calculateSea(int *n_units) const;
	int calculateNuclear() const;

	void resetAI() {
		this->done_ai = false;
		this->has_calculated_fow = false;
	}
	void setDoneAI() {
		this->done_ai = true;
	}
	bool doneAI() const {
		return done_ai;
	}
	void update();
	void calculateCivBonuses();
	void recordHistory();
	const map<int, History> *getHistory() const {
		return &this->history;
	}
	/*bool isDead() const {
		return is_dead;
	}
	void setDead();*/
	void calculateFogOfWar(bool force); // calculates only if has_calculated_fow is false
	void updateFogOfWarCity(const City *city, bool update_terrain = true);
	void updateFogOfWar(Pos2D pos, bool update_terrain = true);
	bool isFogOfWarVisible(int x, int y) const;
	bool isExplored(int x,int y) const;
	bool isExploredBase(int x,int y) const;
	bool isExploredBase(int indx) const;
	bool uncover(int x,int y,int range);
	bool uncoverCity(int x,int y,int range);
	void makeContact(int x,int y,int range);
	void revealMap();
	bool compareMaps(const Civilization *that) const;
	bool canShareMaps(Civilization *that) const;
	void shareMaps(Civilization *that);

	//int getArmyStrength() const;

	static void getDistinctTechnologies(MainGamestate *mainGamestate, vector<const Technology *> *technologies1, vector<const Technology *> *technologies2, const Civilization *civ1, const Civilization *civ2);

	// AI functions
	void chooseTechnologyAI();
	enum AIText {
		AITEXT_OPENING_AT_PEACE = 0,
		AITEXT_OPENING_AT_WAR = 1,
		AITEXT_DECLARED_WAR = 2,
		AITEXT_MAKE_PEACE_YES = 3,
		AITEXT_MAKE_PEACE_NO = 4,
		AITEXT_BREAK_RIGHTOFPASSAGE = 5,
		AITEXT_MAKE_RIGHTOFPASSAGE_YES = 6,
		AITEXT_MAKE_RIGHTOFPASSAGE_NO = 7
	};
	string getAIText(AIText aiText);
};
