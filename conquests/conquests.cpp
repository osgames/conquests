#ifdef _WIN32
#include <Windows.h>
#include <SDL.h>
#elif __linux
#include <SDL/SDL.h>
#endif

#include <cstdio>
#include <cstring>

#ifndef _WIN32
#define stricmp strcasecmp
#define strnicmp strncasecmp
#endif

#include "game.h"

int main(int argc, char *argv[]) {
	/*{
		Game::doDocs();
		return 0;
	}*/
	/*{
		Game::doBitmapToXML("conquests/gfxsrc/earth.png", "conquests/data/maps/Earth.tmx");
		return 0;
	}*/
	/*{
		Game::doTests(TEST_ALL, true);
		//Game::doTests(TEST_ATTACK_CITY);
		//Game::doTests(TEST_REBEL_1);
		//Game::doTests(TEST_REBEL_2);
		return 0;
	}*/
	if( argc <= 1 ) {
#ifdef _WIN32
#ifndef _DEBUG
		// close the console automatically opened on Windows, in Release mode
		FreeConsole();
#endif
/*#ifdef _DEBUG
	{
		AllocConsole();
		FILE *dummy = NULL;
		freopen_s(&dummy, "con", "w", stdout);
		SetConsoleTitleA("DEBUG:");
	}
#endif*/
#endif
		Game::doGame();
	}
	else {
		if( stricmp(argv[1], "test") == 0 ) {
			bool quiet = false;
			if( argc >= 3 && stricmp(argv[2], "quiet") == 0 ) {
				quiet = true;
			}
			Game::doTests(TEST_ALL, quiet);
		}
		else {
			printf("Unknown option: %s\n", argv[0]);
		}
	}
	return 0;
}
