#pragma once

/** Contains the city window class.
*/

class MainGamestate;
class City;
class InfoWindow;

#include <string>
using std::string;

class VI_Panel;
class VI_Button;
class VI_Listbox;
class VI_Graphics2D;

class CityWindow {
	MainGamestate *mainGamestate;
	VI_Panel *panel;
	VI_Button *okayButton;
	VI_Panel *escapeHiddenButton;
	VI_Button *destroyButton;
	VI_Button *statsButton;
	VI_Listbox *buildableListbox;
	VI_Button *changeBuildButton;
	VI_Button *infoBuildButton;
	VI_Button *adviseBuildButton;
	VI_Listbox *improvementsListbox;
	VI_Listbox *unitsListbox;
	VI_Button *activateUnitButton;
	VI_Button *activateAllUnitsButton;
	VI_Button *fortifyAllUnitsButton;
	VI_Listbox *elementsListbox;
	City *city;
	InfoWindow *floatingInfoWindow;
	int cached_production;
	int cached_science;
	int cached_power_per_turn;

	static void paintPanel(VI_Graphics2D *g2d, void *data);
	static void action(VI_Panel *source);
	//static void actionInfoWindow(VI_Panel *source);
	static void actionInfoWindow(InfoWindow *infoWindow);
	void createInfoWindow(string text);
public:
	CityWindow(MainGamestate *mainGamestate, City *city);
	~CityWindow();
};
