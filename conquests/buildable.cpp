#include "conquests_stdafx.h"

#include "buildable.h"
#include "technology.h"
#include "civilization.h"
#include "game.h"
#include "infowindow.h"

#include "../Vision/VisionIface.h"

#include <sstream>
using std::stringstream;

Buildable::Buildable(const char *name, int cost) :
name(name), cost(cost), requires_improvement(NULL), requires_technology(NULL), /*requires_element(NULL), requires_element_amount(0),*/ requires_bonus_resource(NULL), replaced_by(NULL), obsoleted_by(NULL), race_specific(NULL) {
	//VI_log("created Buildable: %s [%d]\n", name, this);
}

Buildable::~Buildable() {
}

void Buildable::setRequires(const Element *requires_element, int requires_element_amount) {
	this->requires_element[requires_element] = requires_element_amount;
	T_ASSERT( requires_element_amount > 0 );
}

int Buildable::getRequiresElementAmount(const Element *element) const {
	map<const Element *, int>::const_iterator iter = requires_element.find(element);
	if( iter == requires_element.end() ) {
		return 0;
	}
	return iter->second;
}

void Buildable::setReplacedBy(const Buildable *replaced_by) {
	ASSERT( this->getType() == replaced_by->getType() );
	this->replaced_by = replaced_by;
}

Improvement::Improvement(/*ImprovementType improvementType,*/const char *name, int cost, int ai_weight) :
Buildable(name, cost), /*improvementType(improvementType),*/
ai_weight(ai_weight), aiHint(AIHINT_NONE), requires_coastal(false),
travel_range(0), travel_by_air(false), air_defence(0),
immune_from_bombing(false), power_per_turn(0), rebellion_bonus(0), growth_rate(0),
production_bonus(0), production_bonus_all_cities(false),
defence_bonus(0), defence_bonus_all_cities(false),
auto_veteran_bonus(false), auto_veteran_bonus_all_cities(false),
research_multiplier_bonus(0), research_multiplier_bonus_all_cities(false)
{
}

Improvement::~Improvement() {
}

string Improvement::getHelp(bool html) const {
	stringstream text;
	string nl = html ? "<br>\n" : "\n";
	string nbsp = html ? "&nbsp;" : " ";
	string bold = html ? "<b>" : "";
	string bold_end = html ? "</b>" : "";

	text << bold << this->getName() << bold_end;
	text << " (improvement):" << nl;
	if( this->race_specific != NULL ) {
		text << "Specific to " << this->race_specific->getName() << nl;
	}
	text << nl;
	text << "Cost : " << this->getCost() << nl;
	if( this->getProductionBonus() > 0 ) {
		text << "+" << this->getProductionBonus() << " to city production";
		if( this->getProductionBonusAllCities() ) {
			text << " (for all cities)";
		}
		text << "." << nl;
	}
	if( this->getDefenceBonus() > 0 ) {
		text << "+" << this->getDefenceBonus() << "% to city defence";
		if( this->getDefenceBonusAllCities() ) {
			text << " (for all cities)";
		}
		text << "." << nl;
	}
	if( this->getAutoVeteranBonus() ) {
		if( this->getProductionBonusAllCities() ) {
			text << "All units produced in any city are automatically veterans (except those generated from Barracks)." << nl;
		}
		else {
			text << "All units produced in the city are automatically veterans (except those generated from Barracks)." << nl;
		}
	}
	if( this->getResearchMultiplierBonus() > 0 ) {
		if( this->getResearchMultiplierBonusAllCities() ) {
			text << "+" << this->getResearchMultiplierBonus() << "% to the entire civilization's rate of base scientific research." << nl;
		}
		else {
			text << "+" << this->getResearchMultiplierBonus() << "% to the city's rate of base scientific research." << nl;
		}
	}
	if( this->getGrowthRate() < 0 ) {
		text << "Increases growth rate by " << - this->getGrowthRate();
		text << "." << nl;
	}
	if( this->getRebellionBonus() > 0 ) {
		//text << "Reduces chance of rebellion (" << this->getRebellionBonus() << ")." << nl;
		text << "Reduces chance of rebellion." << nl;
	}
	if( this->getPowerPerTurn() > 0 ) {
		text << "Power Per Turn : " << this->getPowerPerTurn() << nl;
	}
	if( this->requires_technology != NULL ) {
		text << "Requires technology : ";
		if( html ) {
			text << "<a href=\"#" << this->requires_technology->getName() << "\">";
		}
		text << this->requires_technology->getName();
		if( html ) {
			text << "</a>";
		}
		text << nl;
	}
	if( this->requires_improvement != NULL ) {
		text << "Requires improvement : ";
		if( html ) {
			text << "<a href=\"#" << this->requires_improvement->getName() << "\">";
		}
		text << this->requires_improvement->getName();
		if( html ) {
			text << "</a>";
		}
		text << nl;
	}
	if( this->requires_coastal ) {
		text << "Coastal cities only" << nl;
	}
	if( this->requires_element.size() > 0 ) {
		text << "Requires elements :" << nl;
		for(map<const Element *, int>::const_iterator iter = this->requires_element.begin(); iter != this->requires_element.end(); ++iter) {
			const Element *element = iter->first;
			int amount = iter->second;
			text << nbsp << nbsp << nbsp << nbsp;
			text << element->getName() << " (" << amount << ")" << nl;
		}
	}
	bool any_allows = false;
	for(size_t i=0;i<game_g->getGameData()->getNBuildables();i++) {
		const Buildable *b = game_g->getGameData()->getBuildable(i);
		if( b->getRequiresImprovement() == this ) {
			if( !any_allows ) {
				any_allows = true;
				text << "Allows :" << nl;
			}
			text << nbsp << nbsp << nbsp << nbsp;
			if( html ) {
				text << "<a href=\"#" << b->getName() << "\">";
			}
			text << b->getName();
			if( html ) {
				text << "</a>";
			}
			text << nl;
		}
	}
	bool any_elements = false;
	for(size_t i=0;i<game_g->getGameData()->getNElements();i++) {
		const Element *element = game_g->getGameData()->getElement(i);
		if( element->getRequiresImprovement() == this ) {
			if( !any_elements ) {
				text << nl << "Allows elements:" << nl;
			}
			any_elements = true;
			text << nbsp << nbsp << nbsp << nbsp;
			/*if( html ) {
				text << "<a href=\"#" << tech->getName() << "\">";
			}*/
			text << element->getName();
			/*if( html ) {
				text << "</a>";
			}*/
			text << nl;
		}
	}
	if( this->getReplacedBy() != NULL ) {
		text << "Replaced by : ";
		if( html ) {
			text << "<a href=\"#" << this->getReplacedBy()->getName() << "\">";
		}
		text << this->getReplacedBy()->getName();
		if( html ) {
			text << "</a>";
		}
		text << nl;
	}
	if( this->getObsoletedBy() != NULL ) {
		text << "Made obsolete by : ";
		if( html ) {
			text << "<a href=\"#" << this->getObsoletedBy()->getName() << "\">";
		}
		text << this->getObsoletedBy()->getName();
		if( html ) {
			text << "</a>";
		}
		text << nl;
	}
	for(size_t i=0;i<game_g->getGameData()->getNBonusResources();i++) {
		const BonusResource *bonus_resource = game_g->getGameData()->getBonusResource(i);
		if( bonus_resource->getRequiresImprovement() == this ) {
			text << "Required for : " << bonus_resource->getName() << nl;
		}
	}

	if( *this->getInfo() != '\0' ) {
		text << nl;
		text << this->getInfo();
		text << nl;
	}

	return text.str();
}

UnitTemplate::UnitTemplate(const char *name, int cost, int attack, int defence, int moves) : Buildable(name, cost),
attack(attack), defence(defence), bombard(0), bombard_power(0), moves(moves), is_foot(false),
is_air(false), is_missile(false), nuclear_type(NUCLEARTYPE_NONE), air_attack(0), air_defence(0), air_range(0), visibility_range(1),
is_sea(false), sea_attack(0), sea_defence_range(0),
can_build_city(false), can_build_roads(false), upgrade(false), sound_effect(SOUNDEFFECT_SWORDS) {
	string unit_name = string(name);
	/*for(int i=0;i<unit_name.length();i++) {
		if( unit_name[i] == ' ' ) {
			unit_name[i] = '_';
		}
	}*/
	string texture_name = "data/gfx/unit_" + unit_name + ".png";
	string full_texture_name = getFullPath(texture_name.c_str());
	std::transform(full_texture_name.begin(), full_texture_name.end(), full_texture_name.begin(), tolower);
	this->texture = VI_createTexture(full_texture_name.c_str());
	//VI_log("    loaded texture %d\n", this->texture);
}

UnitTemplate::~UnitTemplate() {
	//delete this->texture;
	// Texture automatically deleted via Vision memory management.
}

string UnitTemplate::getHelp(bool html) const {
	stringstream text;
	string nl = html ? "<br>\n" : "\n";
	string nbsp = html ? "&nbsp;" : " ";
	string bold = html ? "<b>" : "";
	string bold_end = html ? "</b>" : "";

	text << bold << this->getName() << bold_end;
	if( this->isAir() ) {
		text << " (air unit):";
	}
	else if( this->isSea() ) {
		text << " (sea unit):";
	}
	else {
		text << " (unit):";
	}
	text << nl;
	if( html ) {
		string texture_name = "data/gfx/unit_" + name + ".png";
		string full_texture_name = getFullPath(texture_name.c_str());
		std::transform(full_texture_name.begin(), full_texture_name.end(), full_texture_name.begin(), tolower);
		text << "<img src=\"" << full_texture_name << "\">\n";
	}
	text << nl;

	text << "Cost : " << this->getCost() << nl;
	if( this->isAir() ) {
		text << "Attack : " << this->getAirAttack() << nl;
		text << "Defence : " << this->getAirDefence() << nl;
	}
	else if( this->isSea() ) {
		text << "Attack : " << this->getSeaAttack() << nl;
	}
	else {
		text << "Attack : " << this->getAttack() << nl;
		text << "Defence : " << this->getDefence() << nl;
	}
	if( this->getBombard() > 0 ) {
		text << "Bombard : " << this->getBombard() << nl;
		text << "Bombard Power : " << this->getBombardPower() << nl;
	}
	if( this->getAirRange() > 0 ) {
		text << "Air Range : " << this->getAirRange() << nl;
	}
	if( !this->isSea() ) {
		text << "Moves : " << this->getMoves() << nl;
		text << "Visibility Range : " << this->getVisibilityRange() << nl;
	}
	if( this->requires_technology != NULL ) {
		text << "Requires technology : ";
		if( html ) {
			text << "<a href=\"#" << this->requires_technology->getName() << "\">";
		}
		text << this->requires_technology->getName();
		if( html ) {
			text << "</a>";
		}
		text << nl;
	}
	if( this->requires_improvement != NULL ) {
		text << "Requires improvement : ";
		if( html ) {
			text << "<a href=\"#" << this->requires_improvement->getName() << "\">";
		}
		text << this->requires_improvement->getName();
		if( html ) {
			text << "</a>";
		}
		text << nl;
	}
	if( this->requires_element.size() > 0 ) {
		text << "Requires Elements :" << nl;
		for(map<const Element *, int>::const_iterator iter = this->requires_element.begin(); iter != this->requires_element.end(); ++iter) {
			const Element *element = iter->first;
			int amount = iter->second;
			text << nbsp << nbsp << nbsp << nbsp;
			text << element->getName() << " (" << amount << ")" << nl;
		}
	}
	if( this->getReplacedBy() != NULL ) {
		text << "Replaced by : ";
		if( html ) {
			text << "<a href=\"#" << this->getReplacedBy()->getName() << "\">";
		}
		text << this->getReplacedBy()->getName();
		if( html ) {
			text << "</a>";
		}
		text << nl;
	}
	if( this->getObsoletedBy() != NULL ) {
		text << "Made obsolete by : ";
		if( html ) {
			text << "<a href=\"#" << this->getObsoletedBy()->getName() << "\">";
		}
		text << this->getObsoletedBy()->getName();
		if( html ) {
			text << "</a>";
		}
		text << nl;
	}
	bool has_nl = false;
	if( this->isFoot() ) {
		if( !has_nl )
			text << nl;
		has_nl = true;
		text << "Foot unit." << nl;
	}
	if( this->canUpgrade() ) {
		if( !has_nl )
			text << nl;
		has_nl = true;
		text << "This unit can upgrade." << nl;
	}
	if( this->canBuildCity() ) {
		if( !has_nl )
			text << nl;
		has_nl = true;
		text << "This unit can build cities." << nl;
	}
	if( this->canBuildRoads() ) {
		if( !has_nl )
			text << nl;
		has_nl = true;
		text << "This unit can build roads/railways." << nl;
	}

	if( *this->getInfo() != '\0' ) {
		text << nl;
		text << this->getInfo();
		text << nl;
	}

	return text.str();
}
