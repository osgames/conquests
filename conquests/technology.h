#pragma once

/** Contains Technology.
*/

#include <string>
using std::string;

class MainGamestate;

#include "common.h"

class Technology {
public:
	enum AIHint {
		AIHINT_NONE = 0,
		AIHINT_REQUIRES_AIRPORT = 1
	};
protected:
	string name;
	string info;
	int base_cost;
	const Technology *requires1;
	const Technology *requires2;
	Age age;
	int ai_weight;
	AIHint aiHint;
	Type terrain;
	bool prevents_rebellion;
public:
	Technology(const char *name, int base_cost, const Technology *requires1, const Technology *requires2, Age age, int ai_weight);

	const char *getName() const {
		return name.c_str();
	}
	bool equals(const char *name) const {
		bool equal = this->name == name;
		return equal;
	}
	bool equals(const Technology *technology) const {
		// TODO: could just compare pointers, since we should only have 1 of any given Technology?
		return equals(technology->getName());
	}
	void setInfo(const char *info) {
		this->info = info;
	}
	const char *getInfo() const {
		return info.c_str();
	}
	string getHelp(bool html=false) const;
	/*int getBaseCost() const {
		return cost;
	}*/
	int getCost(const MainGamestate *mainGamestate) const; // modifies depending on number of civs that have discovered tech
	Age getAge() const {
		return age;
	}
	static string getAgeName(Age age);
	string getAgeName() const {
		return getAgeName(age);
	}
	int getAIWeight() const {
		return ai_weight;
	}
	void setAIHint(AIHint aiHint) {
		this->aiHint = aiHint;
	}
	AIHint getAIHint() const {
		return aiHint;
	}
	Type getTerrain() const {
		return this->terrain;
	}
	void setTerrain(Type terrain) {
		this->terrain = terrain;
	}
	bool getPreventsRebellion() const {
		return prevents_rebellion;
	}
	void setPreventsRebellion(bool prevents_rebellion) {
		this->prevents_rebellion = prevents_rebellion;
	}

	const Technology *getRequires1() const {
		return requires1;
	}
	const Technology *getRequires2() const {
		return requires2;
	}
};
