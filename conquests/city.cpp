#include "conquests_stdafx.h"

#include "city.h"
#include "citywindow.h"
#include "civilization.h"
#include "buildable.h"
#include "technology.h"
#include "unit.h"
#include "maingamestate.h"
#include "map.h"
#include "infowindow.h"

#include "../Vision/VisionIface.h"

#include <cstring> // needed for Linux at least

#include <sstream>
using std::stringstream;

#include <map>
using std::multimap;
using std::pair;

int populations[] = {
	10000, // 1
	12500, // 2
	16000, // 3
	20000, // 4
	25000, // 5
	31000, // 6
	38000, // 7
	48000, // 8
	60000, // 9
	75000, // 10
	95000, // 11
	120000, // 12
	150000, // 13
	185000, // 14
	225000, // 15
	280000, // 16
	350000, // 17
	440000, // 18
	550000, // 19
	700000, // 20
	860000, // 21
	1100000, // 22
	1350000, // 23
	1700000, // 24
	2100000, // 25
	2650000, // 26
	3300000, // 27
	4000000 // 28
};
/*int populations[] = {
	10000, // 1
	14000, // 2
	20000, // 3
	27000, // 4
	38000, // 5
	54000, // 6
	75000, // 7
	100000  // 8
};*/
const int n_size_c = sizeof(populations)/sizeof(populations[0]);

City::City(MainGamestate *mainGamestate, Civilization *civilization, const char *name, int x, int y, int size) :
mainGamestate(mainGamestate), civilization(civilization), name(name), pos(x, y), size(size),
is_resisting(false), resisting_civilization(NULL), resistance(0),
needs_update(false),
progress_population(0),
progress_buildable(0), buildable(NULL),
counter_barracks(0) {
	//this->name = civilization->makeCityName();
	VI_log("New %s city of %s at %d, %d\n", civilization->getNameAdjective(), this->getName(), pos.x, pos.y);
	ASSERT( mainGamestate->getMap() != NULL );
	MapSquare *square = mainGamestate->getMap()->getSquare(x, y);
	ASSERT( square->getCity() == NULL );
	ASSERT( square->isLand() );
	ASSERT( square->canBuildCity() );

	civilization->addCity(this);
	square->setCity(this, true);
	if( square->getType() == TYPE_FOREST ) {
		square->setType(TYPE_GRASSLAND);
	}
	if( square->getBonusResource() != NULL ) {
		square->setBonusResource(NULL);
	}
	// initial element stocks
	for(size_t i=0;i<game_g->getGameData()->getNElements();i++) {
		const Element *element = game_g->getGameData()->getElement(i);
		this->element_stocks[element] = 0;
		if( element->getRequiresTechnology() != NULL && !this->civilization->hasTechnology(element->getRequiresTechnology()) ) {
			continue;
		}
		if( element->getInitialAmount() > 0 ) {
			this->element_stocks[element] = element->getInitialAmount();
		}
	}
	//this->civilization->uncover(this->pos.x, this->pos.y, 1); // normally shouldn't be needed, but just in case - also makes things easier for test suite
	// -> Now done in addCity(), above
	if( civilization != mainGamestate->getPlayer() ) {
		this->chooseAndSetBuildAI();
	}
	else {
		//const Buildable *buildable = game->findBuildable("Peasants");
		//this->setBuildable(buildable);
		this->setDefaultBuildable();
	}
}

City::City(MainGamestate *mainGamestate, Civilization *civilization) :
mainGamestate(mainGamestate), civilization(civilization), is_resisting(false), resisting_civilization(NULL), resistance(0), pos(-1, -1), size(-1),
needs_update(false),
progress_population(-1),
progress_buildable(-1), buildable(NULL),
counter_barracks(-1) {
	// create dummy city, used by City::load()

	// initialise just to fill up the entries (so don't have to worry about find() failing)
	for(size_t i=0;i<game_g->getGameData()->getNElements();i++) {
		const Element *element = game_g->getGameData()->getElement(i);
		this->element_stocks[element] = 0;
	}
}

City::~City() {
	VI_log("Destroy city of %s\n", this->getName());
	// Don't delete improvements, as they are not copies!
	if( civilization != NULL ) {
		civilization->removeCity(this);
	}
	ASSERT( mainGamestate->getMap() != NULL );
	MapSquare *square = mainGamestate->getMap()->getSquare(pos.x, pos.y);
	ASSERT( square->getCity() != NULL );
	square->setCity(NULL, true);
	/*if( square->getCity() != NULL ) {
		// n.b., square info may already have been cleared
		square->setCity(NULL);
	}*/
}

City *City::load(MainGamestate *mainGamestate, Civilization *civilization, FILE *file) {
	const int max_line_c = 4095;
	char line[max_line_c+1] = "";
	char word[max_line_c+1] = "";
	bool found_end = false;
	bool ok = true;
	const char field_xpos_c[] = "xpos=";
	const char field_ypos_c[] = "ypos=";
	const char field_name_c[] = "name=";
	const char field_size_c[] = "size=";
	const char field_progress_population_c[] = "progress_population=";
	const char field_progress_buildable_c[] = "progress_buildable=";
	const char field_buildable_c[] = "buildable=";
	const char field_improvement_c[] = "improvement=";
	const char field_element_stocks_c[] = "element_stocks=";
	const char field_amount_c[] = "amount=";
	const char field_counter_barracks_c[] = "counter_barracks=";

	City *city = new City(mainGamestate, civilization);

	while( ok && fgets(line, max_line_c, file) != NULL ) {
		//if( strcmp(line, "[/city]\n") == 0 ) {
		if( matchFileLine(line, "[/city]") ) {
			found_end = true;
			break;
		}
		if( line[0] == '[' ) {
			VI_log("City::load: Unexpected '[' start of new section!\n");
			ok = false;
			break;
		}
		if( matchFileLine(line, field_xpos_c) ) {
			parseFileLine(word, line, field_xpos_c);
			city->pos.x = atoi(word);
		}
		else if( matchFileLine(line, field_ypos_c) ) {
			parseFileLine(word, line, field_ypos_c);
			city->pos.y = atoi(word);
		}
		else if( matchFileLine(line, field_name_c) ) {
			parseFileLine(word, line, field_name_c);
			city->name = word;
		}
		else if( matchFileLine(line, field_size_c) ) {
			parseFileLine(word, line, field_size_c);
			city->size = atoi(word);
		}
		else if( matchFileLine(line, field_progress_population_c) ) {
			parseFileLine(word, line, field_progress_population_c);
			city->progress_population = atoi(word);
		}
		else if( matchFileLine(line, field_progress_buildable_c) ) {
			parseFileLine(word, line, field_progress_buildable_c);
			city->progress_buildable = atoi(word);
		}
		else if( matchFileLine(line, field_buildable_c) ) {
			parseFileLine(word, line, field_buildable_c);
			if( strcmp(word, "null") == 0 ) {
				city->buildable = NULL;
			}
			else {
				city->buildable = game_g->findBuildable(word);
				if( city->buildable == NULL ) {
					VI_log("City::load: can't find buildable: %s\n", word);
					ok = false;
				}
			}
		}
		else if( matchFileLine(line, field_improvement_c) ) {
			parseFileLine(word, line, field_improvement_c);
			const Improvement *improvement = game_g->findImprovement(word);
			if( improvement == NULL ) {
				VI_log("City::load: can't find improvement: %s\n", word);
				ok = false;
			}
			else {
				city->improvements.push_back(improvement); // don't copy, just pass a pointer
			}
		}
		else if( matchFileLine(line, field_element_stocks_c) ) {
			parseFileLine(word, line, field_element_stocks_c);
			const Element *element = game_g->getGameData()->findElement(word);
			if( element == NULL ) {
				VI_log("City::load: can't find element: %s\n", word);
				ok = false;
			}
			else {
				if( fgets(line, max_line_c, file) == NULL ) {
					VI_log("unexpected eof after element %s\n", word);
					ok = false;
				}
				else {
					if( matchFileLine(line, field_amount_c) ) {
						parseFileLine(word, line, field_amount_c);
						city->element_stocks[element] = atoi(word);
					}
					else {
						VI_log("failed to find amount for element %s\n", word);
						ok = false;
					}
				}
			}
		}
		else if( matchFileLine(line, field_counter_barracks_c) ) {
			parseFileLine(word, line, field_counter_barracks_c);
			city->counter_barracks = atoi(word);
		}
	}
	if( !found_end ) {
		VI_log("City::load: didn't find end section\n");
		ok = false;
	}
	else if( city->pos.x == -1 || city->pos.y == -1 ) {
		VI_log("City::load: didn't find position\n");
		ok = false;
	}
	// TODO: check all fields have been set

	if( ok ) {
		civilization->addCity(city);
		MapSquare *square = mainGamestate->getMap()->getSquare(city->pos);
		ASSERT( square->getCity() == NULL );
		square->setCity(city, false); // territory is updated at end of loading
	}
	else {
		delete city;
		city = NULL;
	}
	return city;
}

void City::save(FILE *file) const {
	fprintf(file, "[city]\n");
	fprintf(file, "xpos=%d\n", pos.x);
	fprintf(file, "ypos=%d\n", pos.y);
	fprintf(file, "name=%s\n", name.c_str());
	fprintf(file, "size=%d\n", size);
	fprintf(file, "progress_population=%d\n", progress_population);
	fprintf(file, "progress_buildable=%d\n", progress_buildable);
	fprintf(file, "buildable=%s\n", buildable == NULL ? "null" : buildable->getName());
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *improvement = *iter;
		fprintf(file, "improvement=%s\n", improvement->getName());
	}
	fprintf(file, "counter_barracks=%d\n", counter_barracks);
	for(map<const Element *, int>::const_iterator iter = this->element_stocks.begin(); iter != this->element_stocks.end(); ++iter) {
		fprintf(file, "element_stocks=%s\n", iter->first->getName());
		fprintf(file, "amount=%d\n", iter->second);
	}
	fprintf(file, "[/city]\n");
}

void City::modifyForResistance(int *value) const {
	if( this->is_resisting ) {
		ASSERT( resistance > 0 && resistance <= 100 );
		int mult = 100 - resistance;
		int new_value = (mult * (*value)) / 100;
		*value = new_value;
	}
}

void City::modifyForDifficulty(int *value, bool science) const {
	if( this->civilization == mainGamestate->getPlayer() ) {
		return;
	}
	//const int min_value = science ? 3 : 2;
	int min_value = 3;
	/*if( !science && mainGamestate->getDifficulty() != MainGamestate::DIFFICULTY_EASY && mainGamestate->getDifficulty() != MainGamestate::DIFFICULTY_MEDIUM ) {
		min_value = 2;
	}*/
	if( *value < min_value ) {
		// try to hide the obviousness of this, by only introducing the change when civilizations are a non-trivial size
		return;
	}
	int mult = 100;
	switch( mainGamestate->getDifficulty() ) {
		case MainGamestate::DIFFICULTY_EASY:
			mult = 75;
			break;
		case MainGamestate::DIFFICULTY_MEDIUM:
			mult = 100;
			break;
		case MainGamestate::DIFFICULTY_HARD:
			//mult = 150;
			// production needs a higher value, to make the increase comparable
			mult = science ? 150 : 165;
			break;
		case MainGamestate::DIFFICULTY_INSANE:
			mult = 200;
			break;
		default:
			ASSERT( false );
			break;
	}
	int new_value = (mult * (*value)) / 100;
	/*if( *value > 0 && new_value == 0 ) {
		// don't round down to 0
		new_value = 1;
	}*/
	if( *value < min_value ) {
		*value = min_value; // don't go below the min value
	}
	*value = new_value;
}

const int n_city_squares_c = 21;
const int centre_city_square_c = 10;

// remember to check if isValid before using it!
Pos2D City::getCitySquare(const Map *map, int i, Pos2D pos) {
	ASSERT( i >= 0 && i < n_city_squares_c );
	Pos2D npos;
	switch( i ) {
		case 0:
			npos = Pos2D(pos.x - 1, pos.y - 2);
			break;
		case 1:
			npos = Pos2D(pos.x, pos.y - 2);
			break;
		case 2:
			npos = Pos2D(pos.x + 1, pos.y - 2);
			break;
		case 3:
			npos = Pos2D(pos.x - 2, pos.y - 1);
			break;
		case 4:
			npos = Pos2D(pos.x - 1, pos.y - 1);
			break;
		case 5:
			npos = Pos2D(pos.x, pos.y - 1);
			break;
		case 6:
			npos = Pos2D(pos.x + 1, pos.y - 1);
			break;
		case 7:
			npos = Pos2D(pos.x + 2, pos.y - 1);
			break;
		case 8:
			npos = Pos2D(pos.x - 2, pos.y);
			break;
		case 9:
			npos = Pos2D(pos.x - 1, pos.y);
			break;
		case 10:
			npos = Pos2D(pos.x, pos.y);
			break;
		case 11:
			npos = Pos2D(pos.x + 1, pos.y);
			break;
		case 12:
			npos = Pos2D(pos.x + 2, pos.y);
			break;
		case 13:
			npos = Pos2D(pos.x - 2, pos.y + 1);
			break;
		case 14:
			npos = Pos2D(pos.x - 1, pos.y + 1);
			break;
		case 15:
			npos = Pos2D(pos.x, pos.y + 1);
			break;
		case 16:
			npos = Pos2D(pos.x + 1, pos.y + 1);
			break;
		case 17:
			npos = Pos2D(pos.x + 2, pos.y + 1);
			break;
		case 18:
			npos = Pos2D(pos.x - 1, pos.y + 2);
			break;
		case 19:
			npos = Pos2D(pos.x, pos.y + 2);
			break;
		case 20:
			npos = Pos2D(pos.x + 1, pos.y + 2);
			break;
		default:
			ASSERT( false );
			break;
	}
	map->reduceToBase(&npos.x, &npos.y);
	return npos;
}

// remember to check if isValid before using it!
Pos2D City::getCitySquare(int i) const {
	return getCitySquare(mainGamestate->getMap(), i, pos);
}

int City::getCentreCitySquareIndex() {
	return centre_city_square_c;
}

int City::getNCitySquares() {
	return n_city_squares_c;
}

/*Pos2D City::getAdjacentSquare(int i) const {
	ASSERT( i >= 0 && i < 8 );
	switch( i ) {
		case 0:
			return Pos2D(pos.x - 1, pos.y - 1);
			break;
		case 1:
			return Pos2D(pos.x, pos.y - 1);
			break;
		case 2:
			return Pos2D(pos.x + 1, pos.y - 1);
			break;
		case 3:
			return Pos2D(pos.x - 1, pos.y);
			break;
		case 4:
			return Pos2D(pos.x + 1, pos.y);
			break;
		case 5:
			return Pos2D(pos.x - 1, pos.y + 1);
			break;
		case 6:
			return Pos2D(pos.x, pos.y + 1);
			break;
		case 7:
			return Pos2D(pos.x + 1, pos.y + 1);
			break;
	}
	ASSERT( false );
	return Pos2D(-1, -1); // won't actually reach here
}*/

bool City::isOnOrAdjacent(Type mapType) const {
	/*for(int i=0;i<8;i++) {
		MapSquare *square = game->getMap()->getSquare( getAdjacentSquare(i) );
		if( square->getType() == mapType ) {
			return true;
		}
	}
	return false;*/
	return mainGamestate->getMap()->isOnOrAdjacent(pos, mapType);
}

string City::getStatsInfo() const {
	stringstream str;
	const Map *map = mainGamestate->getMap();
	const MapSquare *square = map->getSquare(this->pos);
	if( square->getType() == TYPE_DESERT || square->getType() == TYPE_ARTIC ) {
		str << "Slower growth due to " << square->getName() << ".\n";
	}
	if( map->isWithinCityRadius(this->pos, TYPE_FOREST, false) ) {
		str << "+1 city production due to Forest.\n";
	}
	int road_railways_bonus = calculateRoadRailwaysScienceBonus();
	if( road_railways_bonus > 0 ) {
		str << "+" << road_railways_bonus << " science due to Roads/Railways.\n";
	}
	for(int i=0;i<this->getNCitySquares();i++) {
		if( i == centre_city_square_c ) {
			continue;
		}
		Pos2D sq_pos = this->getCitySquare(i);
		if( !mainGamestate->getMap()->isValid(sq_pos.x, sq_pos.y) ) {
			continue;
		}
		const MapSquare *square = mainGamestate->getMap()->getSquare(sq_pos);
		const BonusResource *bonus_resource = square->getBonusResource();
		if( bonus_resource != NULL && bonus_resource->canUse(this) ) {
			int bonus_production = bonus_resource->getBonusProduction();
			int bonus_growth = bonus_resource->getBonusReduceGrowthTime();
			if( bonus_production > 0 ) {
				str << "+" << bonus_production << " production due to " << bonus_resource->getName() << " production bonus.\n";
			}
			if( bonus_growth > 0 ) {
				str << "Faster growth due to " << bonus_resource->getName() << " trade bonus.\n";
			}
		}
	}
	return str.str();
}

void City::capture(Civilization *new_civilization) {
	VI_log("%s city of %s captured by %s\n", this->civilization->getNameAdjective(), this->getName(), new_civilization->getName());
	const int init_resistance_c = 100;
	if( is_resisting && new_civilization == resisting_civilization ) {
		VI_log("city reclaimed back - stop resisting\n");
		this->is_resisting = false;
		this->resisting_civilization = NULL;
		this->resistance = 0;
	}
	else if( is_resisting ) {
		VI_log("reset resistance\n");
		// captured by someone else - city still identifies as the original civilization, but we reset the year
		this->resistance = init_resistance_c;
	}
	else if( this->size > 1 ) {
		VI_log("start resistance\n");
		this->is_resisting = true;
		this->resisting_civilization = civilization;
		this->resistance = init_resistance_c;
	}
	civilization->removeCity(this);
	// need to update territory as soon as the city is removed, so that it's valid when calling addCity (needed for when we make contact - otherwise we assertion fail if the civ is now wiped out, because their territory still exists
	this->mainGamestate->calculateTerritory();
	civilization = new_civilization;
	new_civilization->addCity(this);
	this->mainGamestate->calculateTerritory(); // now recalculate again, with the city belonging to the new civilization
	this->mainGamestate->refreshMapDisplay();
	this->civilization->uncover(this->pos.x, this->pos.y, 1);
	this->civilization->updateFogOfWarCity(this);
	this->initTravel(true, NULL); // reveal squares from harbour etc
	this->setBuildable(NULL); // always call this, to reset the production progress
	if( civilization == mainGamestate->getPlayer() ) {
		if( this->is_resisting && this->size > 1 && this->size < 10 ) {
			string text = "The citizens are resisting our rule! Shall we use our troops to crush the resistance?";
			if( InfoWindow::confirm(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), game_g->getFont(), game_g->getPanelTexture(), text.c_str(), "Yes", "No") ) {
				this->resistance /= 2;
				this->setSize( size-1 );
			}
		}
		this->setDefaultBuildable();
		new CityWindow(mainGamestate, this);
	}
	else {
		this->chooseAndSetBuildAI();
	}
}

void City::setSize(int size) {
	this->size = size;
	VI_log("%s city of %s: size changed to %d\n", this->civilization->getNameAdjective(), this->getName(), size);
	ASSERT( size > 0 );
}

int City::getPopulation() const {
	/*int pop = 10000;
	for(int i=1;i<size;i++) {
		pop *= 1.1;
	}*/
	ASSERT( size >= 1 && size <= n_size_c );
	int population = populations[size-1];
	return population;
}

string City::getPopulationString() const {
	/*int pop = this->getPopulation();
	if( pop == 0 ) {
		return "0";
	}
	stringstream str;
	bool first = true;
	while( pop > 0 ) {
		stringstream temp;
		int mod_pop = pop % 1000;
		if( pop >= 1000 ) {
			if( mod_pop < 10 )
				temp << "00";
			else if( mod_pop < 100 )
				temp << "0";
		}
		temp <<  mod_pop;
		VI_log("temp: %s\n", temp.str().c_str());
		if( !first ) {
			temp << ",";
		}
		else
			first = false;
		str.str(temp.str() + str.str());
		VI_log("str: %s\n", str.str().c_str());
		pop /= 1000;
	}
	return str.str();*/
	int pop = this->getPopulation();
	T_ASSERT( pop > 0 );
	//stringstream str;
	//str << pop;
	/*int n = 0;
	int temp_pop = pop;
	while( temp_pop > 0 ) {
		temp_pop /= 10;
		n++;
	}*/
	/*stringstream base_str;
	base_str << pop;
	int n = 3 - (base_str.str().length() % 3);
	bool first = true;
	for(int i=0;i<base_str.str().length();i++) {
		if( !first && n % 3 == 0 ) {
			str << ",";
		}
		first = false;
		n++;
		str << base_str.str().at(i);
	}
	return str.str();*/
	return formatNumber(pop);
}

int City::calculateProduction() const {
	// todo: cache the value?
	int production = getPopulation() / 10000;
	// improvements
	production += this->civilization->getCivBonusProduction();
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *improvement = *iter;
		if( !this->improvementHasEffect(improvement) ) {
			continue;
		}
		if( !improvement->getProductionBonusAllCities() ) {
			production += improvement->getProductionBonus();
		}
	}
	// bonuses
	bool done_forest = false;
	for(int i=0;i<this->getNCitySquares();i++) {
		if( i == centre_city_square_c ) {
			continue;
		}
		Pos2D sq_pos = this->getCitySquare(i);
		if( !mainGamestate->getMap()->isValid(sq_pos.x, sq_pos.y) ) {
			continue;
		}
		const MapSquare *square = mainGamestate->getMap()->getSquare(sq_pos);
		if( !done_forest && square->getType() == TYPE_FOREST ) {
			production++;
			done_forest = true;
		}
		const BonusResource *bonus_resource = square->getBonusResource();
		if( bonus_resource != NULL ) {
			/*bool bonus_ok = true;
			if( bonus_ok && bonus_resource->getRequiresImprovement() != NULL ) {
				if( !this->hasImprovement( bonus_resource->getRequiresImprovement() ) ) {
					bonus_ok = false;
				}
			}
			if( bonus_ok && bonus_resource->getRequiresTechnology() != NULL ) {
				if( !this->civilization->hasTechnology( bonus_resource->getRequiresTechnology() ) ) {
					bonus_ok = false;
				}
			}
			if( bonus_ok )*/
			if( bonus_resource->canUse(this) )
				production += bonus_resource->getBonusProduction();
		}
	}
	//if( this->civilization->hasTechnology( game_g->getGameData()->findTechnology("Agricultural Revolution") ) ) {
	if( this->civilization->hasTechnology("Agricultural Revolution") ) {
		production += 2;
	}
	//if( this->civilization->hasTechnology( game_g->getGameData()->findTechnology("Industrial Revolution") ) ) {
	if( this->civilization->hasTechnology("Industrial Revolution") ) {
		production += 4;
	}
	modifyForResistance(&production);
	modifyForDifficulty(&production, false);
	if( this->civilization->hasBonus(Civilization::BONUS_PRODUCTION) ) {
		//production *= 1.5;
		production = (int)(production * 1.5);
	}
	return production;
}

int City::calculateRoadRailwaysScienceBonus() const {
	int road_bonus = 0;
	int rail_bonus = 0;
	for(int i=0;i<this->getNCitySquares();i++) {
		if( i == centre_city_square_c ) {
			continue;
		}
		Pos2D sq_pos = this->getCitySquare(i);
		if( !mainGamestate->getMap()->isValid(sq_pos.x, sq_pos.y) ) {
			continue;
		}
		const MapSquare *square = mainGamestate->getMap()->getSquare(sq_pos);
		/*if( square->getRoad() != ROAD_NONE ) {
			if( square->givesRoadBonus() ) {
				road_bonus++;
				if( road_bonus == max_road_bonus_c ) {
					break;
				}
			}
		}*/
		if( square->givesRoadBonus() ) {
			if( square->getRoad() == ROAD_BASIC ) {
				road_bonus++;
			}
			else if( square->getRoad() == ROAD_RAILWAYS ) {
				rail_bonus++;
				//road_bonus++;
			}
		}
	}
	rail_bonus = min(rail_bonus, max_road_bonus_c);
	road_bonus = min(road_bonus, max_road_bonus_c - rail_bonus);
	ASSERT( rail_bonus >= 0 );
	ASSERT( road_bonus >= 0 );
	int bonus = 2 * rail_bonus + road_bonus;
	return bonus;
}

int City::calculateScience() const {
	// todo: cache the value?
	int science = getPopulation() / 20000;
	if( science == 0 )
		science = 1;
	// bonuses
	/*int road_bonus = 0;
	int rail_bonus = 0;
	for(int i=0;i<this->getNCitySquares();i++) {
		if( i == centre_city_square_c ) {
			continue;
		}
		Pos2D sq_pos = this->getCitySquare(i);
		if( !mainGamestate->getMap()->isValid(sq_pos.x, sq_pos.y) ) {
			continue;
		}
		const MapSquare *square = mainGamestate->getMap()->getSquare(sq_pos);
		if( square->givesRoadBonus() ) {
			if( square->getRoad() == ROAD_BASIC ) {
				road_bonus++;
			}
			else if( square->getRoad() == ROAD_RAILWAYS ) {
				rail_bonus++;
				//road_bonus++;
			}
		}
	}
	rail_bonus = min(rail_bonus, max_road_bonus_c);
	road_bonus = min(road_bonus, max_road_bonus_c - rail_bonus);
	ASSERT( rail_bonus >= 0 );
	ASSERT( road_bonus >= 0 );
	science += 2 * rail_bonus;
	science += road_bonus;*/
	science += calculateRoadRailwaysScienceBonus();

	if( this->hasImprovement("School") ) {
		// increases base, so do before multiplier modifiers
		science += 2;
	}

	int base_science = science;

	// improvements
	science += (base_science*this->civilization->getCivBonusResearchMultiplier())/100;
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *improvement = *iter;
		if( !this->improvementHasEffect(improvement) ) {
			continue;
		}
		if( !improvement->getResearchMultiplierBonusAllCities() ) {
			science += (base_science*improvement->getResearchMultiplierBonus())/100;
		}
	}
	// hardcoded improvements
	/*if( this->hasImprovement("Library") ) {
		science += base_science/2;
	}*/
	if( this->hasImprovement("University") ) {
		// +50% is already handled, but still need to do Scientific Method
		//science += base_science/2;
		//if( this->civilization->hasTechnology( mainGamestate->findTechnology("Scientific Method") ) ) {
		if( this->civilization->hasTechnology("Scientific Method") ) {
			science += base_science/20;
		}
	}

	modifyForResistance(&science);
	modifyForDifficulty(&science, true);
	if( this->civilization->hasBonus(Civilization::BONUS_TECH) ) {
		science *= 2;
	}
	return science;
}

bool City::improvementHasEffect(const Improvement *improvement) const {
	const Technology *obsoleted_by = improvement->getObsoletedBy();
	if( obsoleted_by != NULL && this->civilization->hasTechnology(obsoleted_by) ) {
		return false;
	}
	const Buildable *replaced_by = improvement->getReplacedBy();
	if( replaced_by != NULL ) {
		ASSERT( replaced_by->getType() == Buildable::TYPE_IMPROVEMENT );
		const Improvement *replaced_by_improvement = static_cast<const Improvement *>(replaced_by);
		if( this->hasImprovement(replaced_by_improvement) ) {
			return false;
		}
	}
	return true;
}

int City::calculatePowerPerTurn() const {
	// n.b., should not be modified for AIs due to difficulty level!
	//int power_per_turn = 0;
	int power_per_turn = getPopulation() / 10000;
	if( power_per_turn > 0 )
		power_per_turn--;
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *improvement = *iter;
		if( !this->improvementHasEffect(improvement) ) {
			continue;
		}

		power_per_turn += improvement->getPowerPerTurn();
	}
	return power_per_turn;
}

int City::getResistance() const {
	ASSERT( this->is_resisting );
	return this->resistance;
}

int City::getPopulationCost() const {
	int cost = 10;
	MapSquare *square = mainGamestate->getMap()->getSquare(pos);
	if( square->getType() == TYPE_ARTIC || square->getType() == TYPE_DESERT ) {
		//cost *= 1.5;
		cost = (int)(cost * 1.5);
	}
	/*if( this->hasImprovement("Hospital") ) {
		//cost *= 0.8;
		cost = (int)(cost * 0.8);
	}*/
	for(vector<const Improvement *>::const_iterator iter = improvements.begin(); iter != improvements.end(); ++iter) {
		const Improvement *improvement = *iter;
		if( this->improvementHasEffect(improvement) ) {
			cost += improvement->getGrowthRate();
		}
	}
	//if( this->civilization->hasTechnology( mainGamestate->findTechnology("Genetics") ) ) {
	if( this->civilization->hasTechnology("Genetics") ) {
		//cost *= 0.8;
		cost = (int)(cost * 0.8);
	}
	for(int i=0;i<this->getNCitySquares();i++) {
		if( i == centre_city_square_c ) {
			continue;
		}
		Pos2D sq_pos = this->getCitySquare(i);
		if( !mainGamestate->getMap()->isValid(sq_pos.x, sq_pos.y) ) {
			continue;
		}
		const MapSquare *square = mainGamestate->getMap()->getSquare(sq_pos);
		const BonusResource *bonus_resource = square->getBonusResource();
		if( bonus_resource != NULL ) {
			/*bool bonus_ok = true;
			if( bonus_ok && bonus_resource->getRequiresImprovement() != NULL ) {
				if( !this->hasImprovement( bonus_resource->getRequiresImprovement() ) ) {
					bonus_ok = false;
				}
			}
			if( bonus_ok && bonus_resource->getRequiresTechnology() != NULL ) {
				if( !this->civilization->hasTechnology( bonus_resource->getRequiresTechnology() ) ) {
					bonus_ok = false;
				}
			}
			if( bonus_ok )*/
			if( bonus_resource->canUse(this) )
				cost -= bonus_resource->getBonusReduceGrowthTime();
		}
	}
	cost = max(cost, 2);
	return cost;
}

void City::addImprovement(const Improvement *improvement) {
	this->improvements.push_back(improvement); // don't copy, just pass a pointer

	//if( improvement->getImprovementType() == IMPROVEMENT_HARBOUR ) { // test not necessary, but just to help performance
	if( improvement->getTravelRange() > 0 ) { // test not necessary, but just to help performance
		initTravel(true, NULL);
	}

	if( this->civilization == mainGamestate->getPlayer() ) {
		mainGamestate->centre(this->pos);
		stringstream text;
		if( improvement->getRaceSpecific() != NULL ) {
			text << this->getName() << " has built a great project!: " << buildable->getName() << ".";
		}
		else {
			text << this->getName() << " has built: " << buildable->getName() << ".";
		}
		mainGamestate->playSound(MainGamestate::SOUND_COMPLETE);
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, 128);
		window->doModal();
		delete window;
	}
	else if( improvement->getRaceSpecific() != NULL ) {
		// n.b., notify player even if we haven't made contact with the civilization
		stringstream text;
		text << "The " << improvement->getRaceSpecific()->getNameAdjective() << " city of " << this->getName() << " has built a great project!: " << buildable->getName() << ".";
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, 128);
		window->doModal();
		delete window;
	}
}

// Called by the Test routines - this resets the current buildable if it's the improvement being added, but doesn't choose or ask for a new buildable.
void City::addImprovementTest(const Improvement *improvement) {
	this->addImprovement(improvement);
	if( this->buildable == improvement ) {
		// can't build this anymore
		this->buildable = NULL;
		this->progress_buildable = 0;
	}
}

void City::update() {
	/*if( !needs_update ) {
		return;
	}*/
	//VI_log("Updating the city of %s ( %d )\n", getName(), this);
	ASSERT( needs_update ); // should only be called if update is needed!
	bool open_citywindow = false;

	// update element stocks
	for(size_t i=0;i<game_g->getGameData()->getNElements();i++) {
		const Element *element = game_g->getGameData()->getElement(i);
		if( element->getRequiresTechnology() != NULL && !this->civilization->hasTechnology(element->getRequiresTechnology()) ) {
			continue;
		}
		if( element->getRequiresImprovement() != NULL && !this->hasImprovementOrNewer(element->getRequiresImprovement()) ) {
			continue;
		}
		this->element_stocks[element] += element->getBaseRate();
		int total = 0;
		for(int j=0;j<this->getNCitySquares();j++) {
			Pos2D pos = this->getCitySquare(j);
			if( this->mainGamestate->getMap()->isValid(pos.x, pos.y) ) {
				const MapSquare *square = this->mainGamestate->getMap()->getSquare(pos);
				int terrain_rate = element->getTerrainRate( square->getType() );
				this->element_stocks[element] += terrain_rate;
				int bonus_rate = element->getBonusRate( square->getBonusResource() );
				this->element_stocks[element] += bonus_rate;
				total += terrain_rate;
				total += bonus_rate;
			}
		}
		/*if( total > 0 ) {
			VI_log("%s city of %s receives %d of %s\n", this->civilization->getNameAdjective(), this->getName(), total, element->getName());
		}*/

		const int max_stock_c = 200;
		if( this->element_stocks[element] > max_stock_c ) {
			if( this->civilization->hasTechnology( game_g->findTechnology("Steam Power") ) ) {
				//set<City *, 
				//VI_log("%s city of %s distributes %s:\n", this->civilization->getNameAdjective(), this->getName(), element->getName());
				// we use a multimap, to sort the Cities by the stocks for this element, so we give to the cities with smaller stock first
				multimap<int, City *> cities;
				for(size_t j=0;j<this->civilization->getNCities() && this->element_stocks[element] > max_stock_c;j++) {
					City *city = this->civilization->getCity(j);
					if( city == this )
						continue;
					if( city != this && city->element_stocks[element] < max_stock_c ) {
						cities.insert(pair<int, City *>(city->element_stocks[element], city));
					}
				}
				/*{
					// debug
					VI_log("candidate cities:\n");
					for(multimap<int, City *>::iterator iter = cities.begin(); iter != cities.end(); ++iter) {
						City *city = iter->second;
						VI_log("    %s : %d\n", city->getName(), city->element_stocks[element]);
					}
				}*/
				while( this->element_stocks[element] > max_stock_c ) {
					bool any = false;
					for(multimap<int, City *>::iterator iter = cities.begin(); iter != cities.end() && this->element_stocks[element] > max_stock_c; ++iter) {
						City *city = iter->second;
						if( city->element_stocks[element] < max_stock_c ) {
							// need to check again, as it may have increased to the maximum
							// also note that increasing stocks won't change its ordering in the cities multimap (as we don't change a key), but this is intended behaviour
							//VI_log("%s city of %s gave 1 of %s to %s\n", this->civilization->getNameAdjective(), this->getName(), element->getName(), city->getName());
							this->element_stocks[element]--;
							city->element_stocks[element]++;
							any = true;
						}
					}
					if( !any ) {
						// no more cities with spare stock space to give to!
						//VI_log("no more cities with spare stock space to give to\n");
						break;
					}
				}
				/*while( this->element_stocks[element] > max_stock_c ) {
					VI_log(">>>>\n");
					bool any = false;
					for(size_t j=0;j<this->civilization->getNCities() && this->element_stocks[element] > max_stock_c;j++) {
						City *city = this->civilization->getCity(j);
						if( city == this )
							continue;
						if( city->element_stocks[element] < max_stock_c ) {
							VI_log("%s city of %s gave 1 of %s to %s\n", this->civilization->getName(), this->getName(), element->getName(), city->getName());
							this->element_stocks[element]--;
							city->element_stocks[element]++;
							any = true;
						}
					}
					if( !any ) {
						// no more cities with spare stock space to give to!
						VI_log("no more cities with spare stock space to give to\n");
						break;
					}
					VI_log("<<<<\n");
				}*/
				//VI_log("done\n");
			}
			this->element_stocks[element] = min(this->element_stocks[element], max_stock_c);
		}
	}

	if( this->hasImprovement("Barracks") ) {
		this->counter_barracks++;
		if( this->counter_barracks >= barracks_turns_per_unit_c ) {
			const UnitTemplate *unit_template = NULL;
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Modern Infantry"));
			if( !this->canBuild(unit_template) ) {
				unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
			}
			if( !this->canBuild(unit_template) ) {
				unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Riflemen"));
			}
			if( !this->canBuild(unit_template) ) {
				unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Musketeers"));
			}
			if( !this->canBuild(unit_template) ) {
				unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Men-At-Arms"));
			}
			if( !this->canBuild(unit_template) ) {
				unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Swordsmen"));
			}
			if( !this->canBuild(unit_template) ) {
				unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
			}

			VI_log("The %s city of %s produces barracks unit: %s\n", this->civilization->getNameAdjective(), this->getName(), unit_template->getName());

			new Unit(mainGamestate, this->civilization, unit_template, this->getPos());
			// n.b., units produced from barracks aren't ever veterans, even if we have auto_veteran bonus
			useUpElements(unit_template);
			this->counter_barracks = 0;
		}
	}

	/*if( this->buildable == NULL ) {
		new CityView(this);
	}
	else*/
	if( this->buildable != NULL ) {
		// first check if we can build a replacement
		const Buildable *replaced_by = this->buildable->getReplacedBy();
		if( replaced_by != NULL && this->canBuild(replaced_by) ) {
			this->buildable = replaced_by;
			// also need to use up any elements!
			useUpElements(this->buildable);
		}

		if( this->progress_buildable == 0 ) {
			// just started building, so use up the elements
			/*if( this->buildable->getRequiresElement() != NULL ) {
				const Element *element = this->buildable->getRequiresElement();
				int amount = this->buildable->getRequiresElementAmount();
				T_ASSERT( amount > 0 );
				// it's possible to have too little stock, e.g., if multiple cities are building the same thing, and the stock runs too low; or if loading a game from an old version, when the required elements have since changed
				// as a workaround, we simply allow the player this one for free...
				this->element_stocks[element] -= amount;
				this->element_stocks[element] = max(this->element_stocks[element], 0);
			}*/
			useUpElements(this->buildable);
		}

		this->progress_buildable += calculateProduction();

		if( this->progress_buildable >= buildable->getCost() ) {
			// built
			VI_log("The %s city of %s builds %s\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
			if( this->buildable->getType() == Buildable::TYPE_IMPROVEMENT ) {
				const Improvement *improvement = static_cast<const Improvement *>(this->buildable);
				this->addImprovement(improvement);
				this->setBuildable(NULL); // can't build improvement anymore
				if( civilization == mainGamestate->getPlayer() ) {
					// ask for new buildable
					open_citywindow = true;
					// if built an improvement, use the AI to pick a default next buildable
					this->chooseAndSetBuildAI();
					if( this->buildable == NULL ) {
						this->setDefaultBuildable();
					}
				}
			}
			else if( this->buildable->getType() == Buildable::TYPE_UNIT ) {
				const UnitTemplate *unit_template = static_cast<const UnitTemplate *>(this->buildable);
				Unit *unit = new Unit(mainGamestate, this->civilization, unit_template, this->getPos());
				bool auto_veteran = this->civilization->getCivBonusAutoVeteran();
				for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end() && !auto_veteran; ++iter) {
					const Improvement *improvement = *iter;
					if( !this->improvementHasEffect(improvement) ) {
						continue;
					}
					if( improvement->getAutoVeteranBonus() ) {
						auto_veteran = true;
					}
				}
				if( auto_veteran ) {
					unit->setVeteran(true);
				}
				if( !this->canBuild( this->buildable ) ) {
					// can't build this anymore - could happen if necessary improvement destroyed, for example
					this->buildable = NULL;
					if( civilization == mainGamestate->getPlayer() ) {
						// n.b., don't open a window for units
						this->setDefaultBuildable();
					}
				}
			}
			else {
				VI_log("Unknown type: %s\n", this->buildable->getName());
				ASSERT(false);
			}
			// reset
			this->progress_buildable = 0;
			/*if( !this->canBuild( this->buildable ) ) {
				// can't build this anymore (usually means it's an Improvement, but could apply to Units hypothetically...
				this->buildable = NULL;
				if( civilization == mainGamestate->getPlayer() ) {
					open_citywindow = true;
					this->setDefaultBuildable();
				}
			}*/
			if( civilization != mainGamestate->getPlayer() ) {
				// do AI
				this->chooseAndSetBuildAI();
			}
		}
	}

	if( civilization->getTechnology() == NULL ) {
		if( civilization == mainGamestate->getPlayer() ) {
			mainGamestate->askTechnology();
		}
		else {
			civilization->chooseTechnologyAI();
		}
	}
	if( civilization->getTechnology() != NULL ) {
		const Technology *was_researching = civilization->getTechnology(); // need to save, as it will be made NULL if we discover it!
		if( this->civilization->updateProgressTechnology( calculateScience() ) ) {
			VI_log("%s discover %s\n", civilization->getName(), was_researching->getName());
			if( civilization == mainGamestate->getPlayer() ) {
				stringstream text;
				Age age  = civilization->getAge();
				if( (int)age >= (int)AGE_INDUSTRIAL )
					text << "Our scientists have discovered the secrets of ";
				else
					text << "Our wise men have discovered the secrets of ";
				text << was_researching->getName();
				text << "!";
				text << "\n\n";
				text << was_researching->getInfo();
				mainGamestate->playSound(MainGamestate::SOUND_COMPLETE);
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, 320);
				window->doModal();
				delete window;
				mainGamestate->askTechnology();
			}
			else {
				civilization->chooseTechnologyAI();
			}
		}
	}

	this->civilization->updatePower( this->calculatePowerPerTurn() );

	int max_size = 0;
	if( !this->hasImprovement("Farmland") )
		max_size = size_limit_farmland_c;
	else if( !this->hasImprovement("Aqueduct") )
		max_size = size_limit_aqueduct_c;
	//else if( !this->civilization->hasTechnology( mainGamestate->findTechnology("Agricultural Revolution") ) )
	else if( !this->civilization->hasTechnology("Agricultural Revolution") )
		max_size = size_limit_agriculture_c;
	//else if( !this->hasImprovement("Factory") || !this->civilization->hasTechnology( mainGamestate->findTechnology("Combustion") ) )
	else if( !this->hasImprovement("Factory") || !this->civilization->hasTechnology("Combustion") )
		max_size = size_limit_combustion_c;
	else
		max_size = n_size_c;
	if( this->size <= max_size ) {
		int increase = 1;
		if( this->civilization->hasBonus(Civilization::BONUS_GROWTH) )
			increase = 2;
		this->progress_population += increase;
		int population_cost = getPopulationCost();
		if( this->progress_population >= population_cost ) {
			if( this->size < max_size ) {
				this->progress_population = 0;
				//this->size++;
				this->setSize( size+1 );
				VI_log("%s city of %s grows to size %d\n", this->civilization->getNameAdjective(), this->getName(), this->size);
				if( civilization != mainGamestate->getPlayer() && this->progress_buildable == 0 ) {
					// call AI routine again, as different city size may change our decision
					this->chooseAndSetBuildAI();
				}
			}
			else {
				this->progress_population = population_cost;
			}
		}
	}
	else if( this->size > 1 ) {
		// city reduces in size!
		this->progress_population--;
		if( this->progress_population < 0 ) {
			int population_cost = getPopulationCost();
			this->progress_population = population_cost - 1;
			//this->size--;
			this->setSize( size-1 );
			VI_log("%s city of %s shrinks to size %d\n", this->civilization->getNameAdjective(), this->getName(), this->size);
		}
	}

	// update resistance
	if( this->is_resisting ) {
		const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt( this->pos );
		int count = 1;
		for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter) {
			const Unit *unit = *iter;
			if( unit->getTemplate()->getAttack() > 0 ) {
				count++;
			}
		}
		count = min(count, 4);
		this->resistance -= 10 * count;
		if( this->resistance <= 0 ) {
			this->is_resisting = false;
			this->resisting_civilization = NULL;
			this->resistance = 0;
		}
		VI_log("resistance of %s falls to %d\n", this->getName(), this->resistance);
	}

	needs_update = false;
	if( open_citywindow ) {
		new CityWindow(mainGamestate, this);
	}
}

void City::useUpElements(const Buildable *b) {
	for(size_t i=0;i<game_g->getGameData()->getNElements();i++) {
		const Element *element = game_g->getGameData()->getElement(i);
		int amount = b->getRequiresElementAmount(element);
		if( amount > 0 ) {
			this->element_stocks[element] -= amount;
			// theoretically the stock should never fall below 0 (we should only be calling this on buildables that the city can build!), but just in case, allow the player to get away with it...
			this->element_stocks[element] = max(this->element_stocks[element], 0);
		}
	}
}

bool City::canLaunch(const UnitTemplate *unit_template) const {
	// whether air/missile units can bomb from this city; whether fighter units can defend this city; whether air units can rebase from/to this city
	// we go by whether the "required" improvement is present
	//ASSERT( unit_template->isAir() );
	if( !unit_template->isAir() ) {
		return false;
	}
	const Improvement *improvement = unit_template->getRequiresImprovement();
	if( improvement == NULL ) {
		return true;
	}
	else if( this->hasImprovement(improvement) ) {
		return true;
	}
	return false;
}

/*bool City::canTravel(const UnitTemplate *unit_template) const {
	// can this unit travel from/to this city?
	// if unit_template==NULL, then consider a generic land unit
	if( unit_template != NULL && unit_template->isMissile() ) {
		return false; // disallow for now
	}
	bool by_air = unit_template != NULL && unit_template->isAir();
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *this_improvement = *iter;
		if( this_improvement->isTravelByAir() != by_air )
			continue;
		int this_range = this_improvement->getTravelRange();
		if( this_range > 0 )
			return true;
	}
	return false;
}*/

/*bool City::canTravel(bool by_air) const {
	// can we travel from/to this city?
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *this_improvement = *iter;
		if( this_improvement->isTravelByAir() != by_air )
			continue;
		int this_range = this_improvement->getTravelRange();
		if( this_range > 0 )
			return true;
	}
	return false;
}*/

int City::getTravelRange(bool by_air) const {
	// travel range for land units, by sea or air
	int range = 0;
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *this_improvement = *iter;
		int this_range = this_improvement->getTravelRange();
		if( this_range == 0 )
			continue;
		if( this_improvement->isTravelByAir() != by_air )
			continue;
		//if( this_improvement->getImprovementType() == IMPROVEMENT_PORT ) {
		if( this_improvement->equals("Port") ) {
			//if( this->civilization->hasTechnology( mainGamestate->findTechnology("Steam Power") ) ) {
			if( this->civilization->hasTechnology("Steam Power") ) {
				this_range = 9999;
			}
			else if( this->civilization->hasTechnology("Magnetism") ) {
				this_range *= 4;
			}
		}
		//else if( this_improvement->getImprovementType() == IMPROVEMENT_AIRPORT ) {
		else if( this_improvement->isTravelByAir() ) {
			//if( this->civilization->hasTechnology( mainGamestate->findTechnology("Rocketry") ) ) {
			if( this->civilization->hasTechnology("Rocketry") ) {
				this_range = jet_travel_range_c;
			}
			//else if( this->civilization->hasTechnology( mainGamestate->findTechnology("Advanced Flight") ) ) {
			else if( this->civilization->hasTechnology("Advanced Flight") ) {
				this_range = bombers_travel_range_c;
			}
		}
		if( this_range > range )
			range = this_range;
	}
	return range;
}

void City::getTravelRanges(int *sea_range, int *air_range, const UnitTemplate *unit_template) const {
	// unit_template may still be NULL, indicating a generic land unit
	*sea_range = 0;
	*air_range = 0;
	if( unit_template != NULL && unit_template->isMissile() ) {
		return; // no travel for missiles, for now
	}
	if( unit_template != NULL && unit_template->isAir() ) {
		// air units travel according to their own range, and can only travel by air
		*sea_range = 0;
		if( this->canLaunch(unit_template) ) {
			*air_range = unit_template->getAirRange();
		}
		else {
			// no longer an airport!
			*air_range = 0;
		}
	}
	else {
		*sea_range = this->getTravelRange(false);
		*air_range = this->getTravelRange(true);
	}
}

void City::initTravel(bool reveal, const UnitTemplate *unit_template, bool sea_defending) const {
	// a good idea to lock, if reveal is true, and calling repeatedly
	//VI_log("City::initTravel() for %s\n", this->getName());
	if( reveal ) {
		ASSERT( unit_template == NULL );
	}
	/*if( unit_template != NULL && unit_template->isSea() ) { // test
		sea_defending = true;
	}*/
	// otherwise, unit_template may still be NULL, indicating a generic land unit (that's capable of attack)
	if( sea_defending ) {
		// if sea_defending is set to true, this is the special case where we're seeing what squares the sea unit can defend
		ASSERT( unit_template != NULL && unit_template->isSea() );
	}
	int sea_range = 0;
	int air_range = 0;
	if( sea_defending ) {
		sea_range = unit_template->getSeaDefenceRange();
	}
	else {
		this->getTravelRanges(&sea_range, &air_range, unit_template);
	}
	/*if( unit_template != NULL && unit_template->isAir() ) {
		sea_range = 0;
		air_range = unit_template->getAirRange();
		ASSERT( this->getTravelRange(true) > 0 );
	}
	else {
		sea_range = this->getTravelRange(false);
		air_range = this->getTravelRange(true);
	}*/

	if( !reveal ) {
		//ASSERT( civilization == game->getPlayer() );
		mainGamestate->getMap()->resetTargets();
	}

	if( sea_range > 0 ) {
		bool ship_travel = !sea_defending && unit_template != NULL && unit_template->isSea();
		bool can_invade = unit_template == NULL || ( !ship_travel && unit_template->getAttack() > 0 && unit_template->isFoot() );
		// Invading means whether we can attack an enemy directly by sea.
		// If unit_template == NULL, this should mean it's being called from the City AI, when deciding to build a unit. So we assume a unit that can invade.
		// This does mean a risk that we end up building a unit that can't invade, but such a unit will still be useful, and could still in general attack the enemy by landing first.

		bool lock = false;
		if( reveal && this->civilization == mainGamestate->getPlayer() && game_g->getGraphicsEnvironment()->getWorld() != NULL && game_g->getGraphicsEnvironment()->getWorld()->getTerrain() != NULL ) {
			lock = true;
			VI_lock();
		}
		const Distance *dists = mainGamestate->getMap()->calculateDistanceMap(civilization, NULL, false, true, pos.x, pos.y, -1, -1, true, true);
		for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
			for(int x=0;x<mainGamestate->getMap()->getWidth();x++) {
				ASSERT( mainGamestate->getMap()->isValidBase(x, y) );
				int this_dist = dists[y * mainGamestate->getMap()->getWidth() + x].cost;
				if( this_dist >= 0 &&  this_dist < sea_range*road_move_scale_c && !mainGamestate->getMap()->getSquare(x, y)->isLand() ) {
					if( reveal ) {
						//this->civilization->setMapVisible(x, y, true);
						this->civilization->uncover(x, y, 1);
					}
					else {
						for(int cy=y-1;cy<=y+1;cy++) {
							for(int cx=x-1;cx<=x+1;cx++) {
								int base_cx = cx, base_cy = cy;
								mainGamestate->getMap()->reduceToBase(&base_cx, &base_cy);
								if( base_cx == this->pos.x && base_cy == this->pos.y ) {
									continue; // no point travelling to same square!
								}
								if( mainGamestate->getMap()->isValid(base_cx, base_cy) ) {
									ASSERT( civilization->isExplored(base_cx, base_cy) ); // should have been revealed when Harbour etc was built
									MapSquare *square = mainGamestate->getMap()->getSquare(base_cx, base_cy);
									if( ( sea_defending && square->isLand() ) || ( square->canMoveTo(unit_template) && ( can_invade || !square->hasForeignUnits(this->civilization) ) ) ) {
										if( ship_travel ) {
											const City *city = square->getCity();
											if( city != NULL && city->getCivilization() == this->civilization ) {
												int targ_sea_range = 0;
												int targ_air_range = 0;
												city->getTravelRanges(&targ_sea_range, &targ_air_range, unit_template);
												if( targ_sea_range > 0 ) {
													square->setTarget(true);
												}
											}
										}
										else {
											square->setTarget(true);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if( lock ) {
			VI_unlock();
		}
		delete [] dists;
	}
	if( air_range > 0 ) {
		if( reveal ) {
			this->civilization->uncover(pos.x, pos.y, air_range);
		}
		else {
			for(int cy=pos.y-air_range;cy<=pos.y+air_range;cy++) {
				for(int cx=pos.x-air_range;cx<=pos.x+air_range;cx++) {
					if( cx == this->pos.x && cy == this->pos.y ) {
						continue; // no point travelling to same square!
					}
					if( mainGamestate->getMap()->isValid(cx, cy) ) {
						ASSERT( civilization->isExplored(cx, cy) ); // should have been revealed when Airport etc was built
						MapSquare *square = mainGamestate->getMap()->getSquare(cx, cy);
						const City *city = square->getCity();
						//if( square->canMoveTo(unit_template) && !square->hasForeignUnits(this->civilization) ) {
						//if( city != NULL && city->getCivilization() == this->civilization && city->getTravelRange(true) > 0 && square->canMoveTo(unit_template) ) {
						//if( city != NULL && city->getCivilization() == this->civilization && city->canTravel(true) > 0 && square->canMoveTo(unit_template) ) {
						if( city != NULL && city->getCivilization() == this->civilization ) {
							int targ_sea_range = 0;
							int targ_air_range = 0;
							city->getTravelRanges(&targ_sea_range, &targ_air_range, unit_template);
							if( targ_air_range > 0 ) {
								square->setTarget(true);
							}
						}
					}
				}
			}
		}
	}
}

const Improvement *City::getBestAirDefenceImprovement() const {
	const Improvement *improvement = NULL;
	int best_defence = 0;
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *this_improvement = *iter;
		int defence = this_improvement->getAirDefence();
		if( defence > 0 && ( improvement == NULL || defence > best_defence ) ) {
			improvement = this_improvement;
			best_defence = defence;
		}
	}
	return improvement;
}

void City::setBuildable(const Buildable *buildable) {
	if( this->buildable == buildable )
		return;
	if( buildable != NULL ) {
		ASSERT( this->canBuild( buildable ) );
	}
	this->buildable = buildable;
	this->progress_buildable = 0;
}

void City::setDefaultBuildable() {
	// start building best defensive unit
	const UnitTemplate *defensiveUnit = NULL;
	for(size_t i=0;i<mainGamestate->getNUnitTemplates();i++) {
		const UnitTemplate *unit_template = mainGamestate->getUnitTemplate(i);
		if( !this->canBuild(unit_template) ) {
			continue;
		}
		if( defensiveUnit == NULL || unit_template->getDefence() > defensiveUnit->getDefence() ||
			( unit_template->getDefence() == defensiveUnit->getDefence() && unit_template->getCost() < defensiveUnit->getCost() )
			) {
				defensiveUnit = unit_template;
		}
	}
	if( defensiveUnit != NULL ) {
		this->setBuildable(defensiveUnit);
	}
}

bool City::hasImprovement(const Improvement *improvement) const {
	// TODO: improvements should be a set?
	ASSERT( improvement != NULL );
	bool found = false;
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end() && !found; ++iter) {
		const Improvement *this_improvement = *iter;
		if( this_improvement->equals(improvement) ) {
			found = true;
		}
	}
	return found;
}

// whether a city has an improvement, or any improvement which replace the required improvement
bool City::hasImprovementOrNewer(const Improvement *improvement) const {
	ASSERT( improvement != NULL );
	bool has_improvement = true;
	if( !this->hasImprovement( improvement ) ) {
		has_improvement = false;
		// check for improvements which replace the required improvement
		while( improvement->getReplacedBy() != NULL && !has_improvement ) {
			ASSERT( improvement->getReplacedBy()->getType() == Buildable::TYPE_IMPROVEMENT );
			improvement = static_cast<const Improvement *>(improvement->getReplacedBy());
			if( this->hasImprovement( improvement ) ) {
				has_improvement = true;
			}
		}
	}
	return has_improvement;
}

bool City::hasImprovement(const char *improvement) const {
	bool found = false;
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end() && !found; ++iter) {
		const Improvement *this_improvement = *iter;
		if( this_improvement->equals(improvement) ) {
			found = true;
		}
	}
	return found;
}

/*bool City::hasImprovement(ImprovementType improvementType) const {
	bool found = false;
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end() && !found; ++iter) {
		const Improvement *this_improvement = *iter;
		if( this_improvement->getImprovementType() == improvementType ) {
			found = true;
		}
	}
	return found;
}*/

bool City::canBuild(const Buildable *candidate_buildable) const {
	bool can_build = false;
	if( candidate_buildable->getType() == Buildable::TYPE_UNIT ) {
		can_build = true;
		const UnitTemplate *unit_template = static_cast<const UnitTemplate *>(candidate_buildable);
		if( unit_template->canBuildCity() && this->getSize() <= 1 ) {
			can_build = false; // need at least size 2 city
		}
	}
	else if( candidate_buildable->getType() == Buildable::TYPE_IMPROVEMENT ) {
		const Improvement *improvement = static_cast<const Improvement *>(candidate_buildable);
		if( !hasImprovement(improvement) ) {
			can_build = true;
		}
		// further checks
		if( can_build && improvement->requiresCoastal() && !this->isOnOrAdjacent(TYPE_OCEAN) ) {
			can_build = false;
		}
		/*if( can_build && ( improvement->getImprovementType() == IMPROVEMENT_FORT || improvement->getImprovementType() == IMPROVEMENT_CASTLE || improvement->getImprovementType() == IMPROVEMENT_WALLS ) ) {
			const Technology *tech = game_g->getGameData()->findTechnology("Gunpowder");
			if( this->civilization->hasTechnology(tech) ) {
				// Gunpowder obsoletes these improvements (note, they still have an effect for cities that have built them, and cities already building them can do so - we just disallow the option to build new ones)
				can_build = false;
			}
		}*/
		// obsoleted checks now done below
		/*if( can_build ) {
			if( improvement->getImprovementType() == IMPROVEMENT_FORT && hasImprovement(IMPROVEMENT_WALLS) ) {
				// walls replaces fort
				can_build = false;
			}
		}*/
	}
	else {
		ASSERT( false );
	}

	if( can_build ) {
		const Race *race_specific = candidate_buildable->getRaceSpecific();
		if( race_specific != NULL && race_specific != this->civilization->getRace() ) {
			can_build = false;
		}
		else if( race_specific != NULL && race_specific == this->civilization->getRace() ) {
			if( candidate_buildable->getType() == Buildable::TYPE_IMPROVEMENT ) {
				const Improvement *improvement = static_cast<const Improvement *>(candidate_buildable);
				// check not already built, or not already building it
				for(size_t i=0;i<civilization->getNCities() && can_build;i++) {
					const City *city = civilization->getCity(i);
					if( city == this )
						continue;
					if( city->hasImprovement(improvement) ) {
						can_build = false;
					}
					else if( city->getBuildable() == improvement ) {
						can_build = false;
					}
				}
			}
		}
	}

	if( can_build ) {
		const Technology *obsoleted_by = candidate_buildable->getObsoletedBy();
		if( obsoleted_by != NULL && this->civilization->hasTechnology(obsoleted_by) ) {
			can_build = false;
		}
	}

	if( can_build ) {
		const Improvement *requires_improvement = candidate_buildable->getRequiresImprovement();
		const Technology *requires_technology = candidate_buildable->getRequiresTechnology();
		//const Improvement *replaced_by = static_cast<const Improvement *>(candidate_buildable->getReplacedBy()); // must be same type
		const Buildable *replaced_by = candidate_buildable->getReplacedBy();
		if( requires_improvement != NULL && !hasImprovement(requires_improvement) ) {
			can_build = false;
		}
		else if( requires_technology != NULL && !civilization->hasTechnology(requires_technology) ) {
			can_build = false;
		}
		/*else if( replaced_by != NULL && ( this->hasImprovement(replaced_by) || this->canBuild(replaced_by) ) ) {
			can_build = false;
		}
		while( can_build && replaced_by != NULL ) {
			// but we also need to look at if this unit has been replaced - e.g., Cuirassiers replaces Knights, which replaces Horsemen; if a Civ can build Cuirassiers, then that should replace Horsemen even if they can't build Knights (due to no Armoury)!
			replaced_by = static_cast<const Improvement *>(replaced_by->getReplacedBy());
			if( replaced_by != NULL && ( this->hasImprovement(replaced_by) || this->canBuild(replaced_by) ) ) {
				can_build = false;
			}
		}*/
		while( can_build && replaced_by != NULL ) {
			if( this->canBuild(replaced_by) )
				can_build = false;
			else if( replaced_by->getType() == Buildable::TYPE_IMPROVEMENT ) {
				const Improvement *replaced_by_improvement = static_cast<const Improvement *>(replaced_by);
				if( this->hasImprovement(replaced_by_improvement) ) {
					can_build = false;
				}
			}
			// but we also need to look at if this unit has been replaced - e.g., Cuirassiers replaces Knights, which replaces Horsemen; if a Civ can build Cuirassiers, then that should replace Horsemen even if they can't build Knights (due to no Armoury)!
			replaced_by = replaced_by->getReplacedBy();
		}
		/*else if( replaced_by != NULL ) {
			const Technology *replaced_by_tech = replaced_by->getRequiresTechnology();
			ASSERT( replaced_by_tech != NULL );
			// A new technology will replace older units, even if the city can't build that unit (e.g., due to lacking the improvement).
			// Whilst seemingly a step backwards, this is to encourage to build those improvements, and to avoid anachronisms (e.g., cities still building chariots in medieval times, because the city doesn't have an armourer, to build knights).
			if( civilization->hasTechnology(replaced_by_tech) ) {
				can_build = false;
			}
		}*/
	}

	// now check element stocks
	/*if( can_build && candidate_buildable->getRequiresElement() != NULL ) {
		const Element *element = candidate_buildable->getRequiresElement();
		int amount = candidate_buildable->getRequiresElementAmount();
		T_ASSERT( amount > 0 );
		int stock = this->element_stocks.find(element)->second;
		if( stock < amount ) {
			can_build = false;
		}
	}*/
	for(size_t i=0;i<game_g->getGameData()->getNElements() && can_build;i++) {
		const Element *element = game_g->getGameData()->getElement(i);
		int amount = candidate_buildable->getRequiresElementAmount(element);

		int stock = this->element_stocks.find(element)->second;
		if( stock < amount ) {
			can_build = false;
		}
	}

	// now check bonus_resource
	if( can_build && candidate_buildable->getRequiresBonusResource() != NULL ) {
		const BonusResource *requires_bonus_resource = candidate_buildable->getRequiresBonusResource();
		bool found = false;
		for(int i=0;i<this->getNCitySquares() && !found;i++) {
			Pos2D pos = this->getCitySquare(i);
			if( this->mainGamestate->getMap()->isValid(pos.x, pos.y) ) {
				const MapSquare *square = this->mainGamestate->getMap()->getSquare(pos);
				const BonusResource *this_bonus_resource = square->getBonusResource();
				if( this_bonus_resource != NULL && requires_bonus_resource->equals(this_bonus_resource->getName()) ) {
					found = true;
				}
			}
		}
		if( !found ) {
			can_build = false;
		}
	}
	return can_build;
}

int City::getDefenceBonus(const Unit *attacker) const {
	// as a percentage
	int bonus = 0;
	const Civilization *attacker_civ = attacker->getCivilization();
	if( !attacker_civ->hasTechnology("Gunpowder") ) {
		// gunpowder makes these obsolete
		if( this->hasImprovement("Walls") ) {
			bonus = 50;
		}
		else if( this->hasImprovement("Castle") ) {
			bonus = 50;
		}
		else if( this->hasImprovement("Fort") ) {
			// n.b. Fort is replaced by Walls, so isn't cumulative
			bonus = 25;
		}
	}
	// improvements
	bonus = max(bonus, this->civilization->getCivBonusDefence());
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *improvement = *iter;
		if( !this->improvementHasEffect(improvement) ) {
			continue;
		}
		if( !improvement->getDefenceBonusAllCities() ) {
			bonus = max(bonus, improvement->getDefenceBonus());
		}
	}
	return bonus;
}

bool City::losePopulation() const {
	if( this->size <= size_limit_aqueduct_c && this->hasImprovement("Walls") ) {
		return false;
	}
	return true;
}

void City::doAI() {
	ASSERT( this->civilization != mainGamestate->getPlayer() );
	if( this->buildable == NULL ) {
		this->chooseAndSetBuildAI();
	}
}

//const int min_defenders_c = 1;

bool City::needDefendersAI(bool *undefended, const Unit *this_unit, int min_defence) const {
	ASSERT( this->civilization != mainGamestate->getPlayer() );
	//return false;
	// TODO: best defenders
	/*Unit::Type def_type = city->bestBuildableUnit(City::DEFENCE);
	Unit *best_defender = (Unit *)Buildable::getBuildable(Buildable::UNIT, def_type);
	if( this->type == def_type || this->getBaseDefence() > best_defender->getBaseDefence() ) {*/
	//const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(pos.x, pos.y);
	const vector<Unit *> *units = AIInterface::getUnits(pos.x, pos.y);
	int n_defenders = 0;
	if( undefended != NULL ) {
		*undefended = true; // initialise
	}
	if( units != NULL ) {
		for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter) {
			const Unit *unit = *iter;
			if( unit == this_unit )
				continue;
			if( this_unit != NULL && unit->getTemplate()->getDefence() < this_unit->getTemplate()->getDefence() ) {
				continue; // ignore units not as good as this_unit, so that we prefer to keep stronger units defending the city
			}
			if( this_unit != NULL && unit->getTemplate()->getDefence() == this_unit->getTemplate()->getDefence() && unit->getTemplate()->getAttack() > this_unit->getTemplate()->getAttack() ) {
				continue; // for units of equal defence, prefer to keep the units of weaker attack - so the stronger attack units can be used to leave the city
			}
			//if( unit->getType() == def_type || unit->getBaseDefence() > best_defender->getBaseDefence() ) {
			if( unit->getTemplate()->getAttack() > 0 && unit->getTemplate()->getDefence() > 0 ) {
				if( undefended != NULL ) {
					*undefended = false;
				}
				if( unit->getTemplate()->getDefence() < min_defence ) {
					// setting min_defence allows us to only look at defenders of a certain defence when counting (however, we still set the 'undefended' flag)
					continue;
				}
				n_defenders++;
			}
		}
	}
	int min_defenders_c = 0;
	if( this->size <= 2 )
		min_defenders_c = 1;
	else if( this_unit == NULL && min_defence == 1 )
		min_defenders_c = 1; // if we can only build Peasants, better to wait until we have better units
	else {
		min_defenders_c = 2;
		if( ( this->size >= 6 && this == this->civilization->getCity(0) ) || this->size >= 8 ) {
			bool is_war = false;
			/*for(int i=0;i<mainGamestate->getNCivilizations() && !is_war;i++) {
				const Civilization *civ = mainGamestate->getCivilization(i);
				if( this->civilization != civ && !civ->isDead() ) {
					const Relationship *relationship = mainGamestate->findRelationship(this->civilization, civ);
					if( relationship->getStatus() == Relationship::STATUS_WAR )
						is_war = true;
				}
			}*/
			vector<Civilization *> civilizations = AIInterface::getCivilizations(AIInterface::GETCIVILIZATIONS_WAR);
			if( civilizations.size() > 0 ) {
				is_war = true;
			}
			if( is_war ) {
				// a capital medium sized city, or large city; and we're at war!
				min_defenders_c = 3;
			}
		}
	}
	if( n_defenders < min_defenders_c ) {
		return true;
	}
	return false;
}

/*bool City::wantWorkerAt(int indx, const Unit *unit) const {
	Pos2D pos = this->getCitySquare(indx);
	//if( !unit->canMoveTo(pos) ) {
	//	// no point trying to move to it
	//	return false;
	//}
	// no need to check if we can move here, as that should be tested separately!
	const MapSquare *square = game->getMap()->getSquare(pos);
	if( square->canBeImproved(unit->getTemplate()) ) {
		return true;
	}
	return false;
}*/

#ifndef USE_LUA_CITY_AI

bool City::chooseBuildPopLimits() {
	// need improvements for city growth
	if(mainGamestate->getYear() <= 50 ) {
		return false; // prefer to focus on Settlers
	}
	//if( this->getSize() >= size_limit_farmland_c && !this->hasImprovement(IMPROVEMENT_FARMLAND) ) {
	if( this->getSize() >= size_limit_farmland_c ) {
		const Buildable *b = mainGamestate->findImprovement(IMPROVEMENT_FARMLAND);
		if( this->canBuild(b) ) {
			this->buildable = b;
			VI_log("%s city of %s starts building %s [need farmland]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
			return true;
		}
	}
	//if( this->getSize() >= size_limit_aqueduct_c && !this->hasImprovement(IMPROVEMENT_AQUEDUCT) ) {
	if( this->getSize() >= size_limit_aqueduct_c ) {
		const Buildable *b = mainGamestate->findImprovement(IMPROVEMENT_AQUEDUCT);
		if( this->canBuild(b) ) {
			this->buildable = b;
			VI_log("%s city of %s starts building %s [need aqueduct]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
			return true;
		}
	}
	return false;
}

const Improvement *City::chooseImprovementAI() {
	vector<const Improvement *> candidates;
	int max_value = 0;
	for(int i=0;i<mainGamestate->getNImprovements();i++) {
		const Improvement *b = mainGamestate->getImprovement(i);
		if( this->canBuild(b) ) {
			int r = rand() % 20;
			int this_value = b->getAIWeight() + r;

			if( this_value > 0 && b->getAIHint() == Improvement::AIHINT_RESEARCH ) {
				if( this->civilization->hasAllTechnology() ) {
					// no point building if we have all technology
					this_value = 0;
				}
			}

			if( this_value <= 0 ) {
				// don't build
			}
			else if( this_value > max_value ) {
				candidates.clear();
				candidates.push_back(b);
				max_value = this_value;
			}
			else if( this_value == max_value ) {
				candidates.push_back(b);
			}
		}
	}
	if( candidates.size() > 0 ) {
		int r = rand() % candidates.size();
		return candidates.at(r);
	}
	return NULL;
}

const UnitTemplate *City::chooseAirUnitAI(const UnitTemplate *bestFighterAirUnit, const UnitTemplate *bestBomberAirUnit, const UnitTemplate *bestConventionalMissileUnit, const UnitTemplate *bestNuclearMissileUnit, bool war_only) {
	vector<const UnitTemplate *> choices;
	if( bestFighterAirUnit != NULL )
		choices.push_back(bestFighterAirUnit);
	if( bestBomberAirUnit != NULL )
		choices.push_back(bestBomberAirUnit);
	if( bestConventionalMissileUnit != NULL )
		choices.push_back(bestConventionalMissileUnit);
	if( bestNuclearMissileUnit != NULL )
		choices.push_back(bestNuclearMissileUnit);
	ASSERT( choices.size() > 0 );
	int r = rand() % choices.size();
	const UnitTemplate *unit = choices.at(r);
	if( unit == bestFighterAirUnit ) {
		// defensive unit, so don't need to see if any cities to attack
		VI_log("build a defensive air unit\n");
		return unit;
	}
	/*for(int i=0;i<this->mainGamestate->getNCivilizations();i++) {
		const Civilization *civ = this->mainGamestate->getCivilization(i);
		if( civ == civilization )
			continue;
		if( civ->isDead() )
			continue;
		if( war_only ) {
			const Relationship *relationship = mainGamestate->findRelationship(this->civilization, civ);
			if( relationship->getStatus() == Relationship::STATUS_PEACE )
				continue;
		}*/
	vector<Civilization *> civs = AIInterface::getCivilizations(war_only ? AIInterface::GETCIVILIZATIONS_WAR : AIInterface::GETCIVILIZATIONS_ALL);
	for(vector<Civilization *>::iterator iter = civs.begin(); iter != civs.end(); ++iter) {
		Civilization *civ = *iter;
		// bomb cities?
		/*for(int j=0;j<civ->getNCities();j++) {
			const City *city = civ->getCity(j);
			Pos2D city_pos = city->getPos();
			if( !this->civilization->isExplored(city_pos.x, city_pos.y) )
				continue;*/
		vector<City *> cities = AIInterface::getCities(civ);
		for(vector<City *>::const_iterator iter2 = cities.begin(); iter2 != cities.end(); ++iter2) {
			const City *city = *iter2;
			Pos2D city_pos = city->getPos();
			/*int dist = mainGamestate->getMap()->distanceChebyshev(city_pos, this->pos);
			if( dist <= unit->getAirRange() ) {*/
			if( AIInterface::canBomb(unit, this, city_pos, true) ) {
				VI_log("%s: %s is within range %d\n", this->getName(), city->getName(), unit->getAirRange());
				return unit;
			}
		}
	}
	return NULL;
}

#endif

void City::chooseAndSetBuildAI() {
	ASSERT( this->progress_buildable == 0 );

	this->buildable = getBuildChoiceAI();
	/*if( this->buildable != NULL )
		VI_log("AI: %s city of %s starts building %s\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
	else
		VI_log("AI: %s city of %s isn't building anything!\n", this->civilization->getNameAdjective(), this->getName());*/
	if( this->buildable == NULL ) {
		VI_log("AI: %s city of %s isn't building anything!\n", this->civilization->getNameAdjective(), this->getName());
	}
}

const Buildable *City::getBuildChoiceAI() {
	const Buildable *choice = NULL;

	AIInterface::setMainGamestate(this->mainGamestate);
	AIInterface::setAICivilization(this->civilization);

#ifdef USE_LUA_CITY_AI
	{
		// Lua code
		game_g->pushScriptFunction("chooseBuild");
		lua_pushlightuserdata(game_g->getLuaState(), this);
		//lua_pushnumber(game_g->getLuaState(), (int)this);
		if( !game_g->runScriptFunction(1) ) {
			ASSERT(false);
		}
		AIInterface::setMainGamestate(NULL);
		AIInterface::setAICivilization(NULL);
		{
			// read result
			int arg_index = 1;

			if( !lua_islightuserdata(game_g->getLuaState(), arg_index) ) {
				VI_log("City::getBuildChoiceAI(): script return argument %d not a pointer\n", arg_index);
			}
			else {
				void *ptr = lua_touserdata(game_g->getLuaState(), arg_index);
				choice = static_cast<const Buildable *>(ptr);
			}
			arg_index++;

			lua_settop(game_g->getLuaState(), 0); // important - clear the stack!
		}
	}
	return choice;

#else

	int n_production = this->getProduction();
	const int ignore_dist_c = 16;

	// get unit templates
	const UnitTemplate *bestUnit = NULL;
	//const UnitTemplate *bestWorkerUnit = NULL; // best attacker unit that can also work
	const UnitTemplate *defensiveUnit = NULL;
	const UnitTemplate *fastestUnit = NULL;
	const UnitTemplate *cheapestUnit = NULL;
	const UnitTemplate *bestFighterAirUnit = NULL;
	const UnitTemplate *bestBomberAirUnit = NULL;
	const UnitTemplate *bestConventionalMissileUnit = NULL;
	const UnitTemplate *bestNuclearMissileUnit = NULL;
	const UnitTemplate *settlersUnit = NULL;
	const UnitTemplate *potentialSettlersUnit = NULL; // the Settlers unit, even if we can't yet build it
	const UnitTemplate *workerUnit = NULL;
	vector<const Buildable *> buildables = AIInterface::cityGetBuildables(this, AIInterface::GETBUILDABLES_UNITS);
	/*for(int i=0;i<mainGamestate->getNUnitTemplates();i++) {
		const UnitTemplate *unit_template = mainGamestate->getUnitTemplate(i);*/
	for(vector<const Buildable *>::const_iterator iter = buildables.begin(); iter != buildables.end(); ++iter) {
		const Buildable *buildable = *iter;
		ASSERT( buildable->getType() == Buildable::TYPE_UNIT );
		const UnitTemplate *unit_template = static_cast<const UnitTemplate *>(buildable);

		if( unit_template->canBuildCity() ) {
			ASSERT( potentialSettlersUnit == NULL ); // should only be one
			potentialSettlersUnit = unit_template;
		}

		if( !this->canBuild(unit_template) ) {
			continue;
		}
		if( !unit_template->isAir() ) {
			if( bestUnit == NULL || unit_template->getAttack() > bestUnit->getAttack() ||
				( unit_template->getAttack() == bestUnit->getAttack() && unit_template->getCost() < bestUnit->getCost() )
				) {
					bestUnit = unit_template;
			}
			if( defensiveUnit == NULL || unit_template->getDefence() > defensiveUnit->getDefence() ||
				( unit_template->getDefence() == defensiveUnit->getDefence() && unit_template->getCost() < defensiveUnit->getCost() )
				) {
					defensiveUnit = unit_template;
			}
			if( fastestUnit == NULL || unit_template->getMoves() > fastestUnit->getMoves() ||
				( unit_template->getMoves() == fastestUnit->getMoves() && unit_template->getCost() < fastestUnit->getCost() )
				) {
					fastestUnit = unit_template;
			}
			if( cheapestUnit == NULL || unit_template->getCost() < cheapestUnit->getCost() ||
				( unit_template->getCost() == cheapestUnit->getCost() && unit_template->getAttack() > cheapestUnit->getAttack() )
				) {
					cheapestUnit = unit_template;
			}
		}
		if( unit_template->isAir() && !unit_template->isMissile() ) {
			if( bestFighterAirUnit == NULL || unit_template->getAirDefence() > bestFighterAirUnit->getAirDefence() ) {
				bestFighterAirUnit = unit_template;
			}
		}
		if( unit_template->isAir() && !unit_template->isMissile() ) {
			if( bestBomberAirUnit == NULL || unit_template->getBombardPower() > bestBomberAirUnit->getBombardPower() ||
				( unit_template->getBombardPower() == bestBomberAirUnit->getBombardPower() && unit_template->getBombard() > bestBomberAirUnit->getBombard() )
				) {
					bestBomberAirUnit = unit_template;
			}
		}
		if( unit_template->isAir() && unit_template->isMissile() && unit_template->getNuclearType() == UnitTemplate::NUCLEARTYPE_NONE ) {
			if( bestConventionalMissileUnit == NULL || unit_template->getBombardPower() > bestConventionalMissileUnit->getBombardPower() ||
				( unit_template->getBombardPower() == bestConventionalMissileUnit->getBombardPower() && unit_template->getBombard() > bestConventionalMissileUnit->getBombard() )
				) {
					bestConventionalMissileUnit = unit_template;
			}
		}
		if( unit_template->isAir() && unit_template->isMissile() && unit_template->getNuclearType() != UnitTemplate::NUCLEARTYPE_NONE ) {
			if( bestNuclearMissileUnit == NULL ||
				( bestNuclearMissileUnit->getNuclearType() == UnitTemplate::NUCLEARTYPE_FISSION && unit_template->getNuclearType() == UnitTemplate::NUCLEARTYPE_FUSION ) ) {
					bestNuclearMissileUnit = unit_template;
			}
		}
		if( unit_template->canBuildRoads() ) {
			/*if( bestWorkerUnit == NULL || unit_template->getAttack() > bestWorkerUnit->getAttack() ||
				( unit_template->getAttack() == bestWorkerUnit->getAttack() && unit_template->getCost() < bestWorkerUnit->getCost() )
				) {
					bestWorkerUnit = unit_template;
			}*/
			if( workerUnit == NULL || unit_template->getCost() < workerUnit->getCost() ||
				( unit_template->getCost() == workerUnit->getCost() && unit_template->getDefence() > workerUnit->getDefence() )
				) {
					workerUnit = unit_template;
			}
		}
		if( unit_template->canBuildCity() ) {
			ASSERT( settlersUnit == NULL ); // should only be one
			settlersUnit = unit_template;
		}
	}
	/*if( bestUnit == NULL && fastestUnit != NULL ) {
		bestUnit = fastestUnit;
	}
	else if( bestUnit == NULL && cheapestUnit != NULL ) {
		bestUnit = cheapestUnit;
	}
	else if( bestUnit != NULL && fastestUnit != NULL ) {
		// we should mainly build the best attacker, but having a few fastest units is good too, so do a substitution
		int r = rand() % 3;
		if( r == 0 ) {
			bestUnit = fastestUnit;
		}
	}*/
	ASSERT( bestUnit != NULL );
	ASSERT( fastestUnit != NULL );
	//ASSERT( bestWorkerUnit != NULL );
	{
		// having a few fastest and worker units is good too, so do a substitution
		// update: we build enough worker units either directly, or through barracks - so need more faster units
		int r = rand() % 4;
		if( r == 0 || r == 1 ) {
			// keep with best
		}
		else {
			bestUnit = fastestUnit;
			VI_log("AI: %s: best unit is fastest\n", this->getName());
		}
		/*else if( r == 2 ) {
			bestUnit = fastestUnit;
			VI_log("AI: %s: best unit is fastest\n", this->getName());
		}
		else if( r == 3 ) {
			bestUnit = bestWorkerUnit;
			VI_log("AI: %s: best unit is worker\n", this->getName());
		}*/
	}

	int n_settlers = 0;
	int n_explorers = 0;
	//int n_workers = 0;
	int n_nukes = 0;
	for(int i=0;i<civilization->getNUnits();i++) {
		const Unit *unit = civilization->getUnit(i);
		const UnitTemplate *unit_template = unit->getTemplate();
		if( unit_template->canBuildCity() ) {
			n_settlers++;
		}
		if( bestNuclearMissileUnit != NULL && unit_template->getNuclearType() == bestNuclearMissileUnit->getNuclearType() ) {
			n_nukes++;
		}
		//if( mainGamestate->getMap()->findCity(unit->getX(), unit->getY()) != NULL ) {
		if( AIInterface::getCity(unit->getX(), unit->getY()) != NULL ) {
			// ignore units defending cities
			continue;
		}
		if( unit_template == fastestUnit ) {
			n_explorers++;
		}
		/*if( unit_template == workerUnit ) {
			n_workers++;
		}*/
	}

	//int max_explorers = 2 * civilization->getNCities();
	//int max_explorers = 3 * civilization->getNCities();
	int max_explorers = civilization->getNCities() + 4; // favour explorers when fewer cities
	if( mainGamestate->getYear() > 100 && civilization->getNCities() > 4 ) {
		max_explorers = 8 + ( civilization->getNCities() - 4 ) / 5;
	}
	//int max_workers = 2 * civilization->getNCities();
	int max_settlers = civilization->getNCities()+1; // +1 so that a single City can still build a 2nd Settlers unit when we already have 1 Settlers unit
	if( mainGamestate->getYear() > 100 && civilization->getNCities() > 8 ) {
		max_settlers = 9 + ( civilization->getNCities() - 8 ) / 5;
	}

	int n_settlers_being_built = 0;
	int n_explorers_being_built = 0;
	int n_nukes_being_built = 0;
	for(int i=0;i<civilization->getNCities();i++) {
		const City *city = civilization->getCity(i);
		if( city != this ) {
			const Buildable *city_buildable = city->getBuildable();
			if( city_buildable != NULL && city_buildable->getType() == Buildable::TYPE_UNIT ) {
				const UnitTemplate *unit_template = static_cast<const UnitTemplate *>(city_buildable);
				if( unit_template->canBuildCity() ) {
					n_settlers_being_built++;
				}
				if( unit_template->getMoves() >= fastestUnit->getMoves() ) {
					n_explorers_being_built++;
				}
				if( bestNuclearMissileUnit != NULL && unit_template->getNuclearType() == bestNuclearMissileUnit->getNuclearType() ) {
					n_nukes_being_built++;
				}
			}
		}
	}

	// build defensive units
	if( defensiveUnit != NULL ) {
		bool undefended = true;
		int min_defence = this->civilization->getNCities() > 1 || n_settlers > 0 ? defensiveUnit->getDefence() : 0;
			// if only 1 city and 0 Settlers, we only care about getting a minimum defence, as building Settlers is better
		if( needDefendersAI(&undefended, NULL, min_defence) ) {
			//int n_turns = n_production / defensiveUnit->getCost();
			if( n_production > 0 ) {
				int n_turns = defensiveUnit->getCost() / n_production;
				VI_log("n_turns = %d\n", n_turns);
				if( n_turns > ( undefended ? 40 : 20 ) ) {
					VI_log("too long, try growing first?\n");
					// too long to build this unit, try growing first (needed for later stages in game for small cities)
					/*const Buildable *aqueduct = mainGamestate->findImprovement(IMPROVEMENT_AQUEDUCT);
					if( defensiveUnit->getCost() > aqueduct->getCost() ) {
						// and only do this if the growth improvement is cheaper!
						if( this->chooseBuildPopLimits() ) {
							VI_log("(too long to build defensive unit)\n");
							AIInterface::setMainGamestate(NULL);
							AIInterface::setAICivilization(NULL);
							return;
						}
					}*/
					if( this->chooseBuildPopLimits() ) {
						// and only do this if the growth improvement is cheaper!
						if( defensiveUnit->getCost() > this->buildable->getCost() ) {
							// okay
							VI_log("(too long to build defensive unit)\n");
							AIInterface::setMainGamestate(NULL);
							AIInterface::setAICivilization(NULL);
							return;
						}
						else {
							// cancel build
							this->buildable = NULL;
						}
					}
				}
			}
			this->buildable = defensiveUnit;
			VI_log("AI: %s city of %s starts building %s [defender]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
			//delete [] dists;
			AIInterface::setMainGamestate(NULL);
			AIInterface::setAICivilization(NULL);
			return;
		}

	}

	if( this->civilization->getNCities() > 1 || n_settlers > 0 ) {
		// need improvements for city growth
		if( this->chooseBuildPopLimits() ) {
			AIInterface::setMainGamestate(NULL);
			AIInterface::setAICivilization(NULL);
			return;
		}
	}

	if( bestNuclearMissileUnit != NULL && n_nukes + n_nukes_being_built < 5 && n_production > 0 ) {
		// rush building for nukes
		int n_turns = bestNuclearMissileUnit->getCost() / n_production;
		if( n_turns <= 10 ) {
			this->buildable = bestNuclearMissileUnit;
			VI_log("%s city of %s starts building %s [rush build nukes]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
			AIInterface::setMainGamestate(NULL);
			AIInterface::setAICivilization(NULL);
			return;
		}
	}

	bool tried_improvements = false;
	// build settlers
	// TODO: improve performance?
	bool build_settler = false;
	if( mainGamestate->getYear() <= 100 ) {
		// prefer building Settlers in early game phase
		// although note this does give the problem of overproduction, on Civs that are on small islands...
		build_settler = true;
	}
	else {
		// in later stages of game, for non-small cities, try doing city improvements before other choices, some of the time
		if( this->size >= 3 && rand() % 2 == 0 ) {
			tried_improvements = true;
			const Improvement *improvement = chooseImprovementAI();
			if( improvement != NULL ) {
				this->buildable = improvement;
				VI_log("%s city of %s starts building %s [improvement] (1st phase)\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
				AIInterface::setMainGamestate(NULL);
				AIInterface::setAICivilization(NULL);
				return;
			}
		}
	}

	const Map::Distance *dists = mainGamestate->getMap()->calculateDistanceMap(civilization, NULL, true, true, pos.x, pos.y);
	//if( this->getSize() > 1 )
	//if( settlersUnit != NULL )
	// we try to see if we want settlers, even if we can't build them, because in such case, it's better to build a cheap unit rather than wasting time with an expensive improvement!
	if( !build_settler && n_settlers + n_settlers_being_built < max_settlers )
	{
		for(int y=0;y<mainGamestate->getMap()->getHeight() && !build_settler;y++) {
			for(int x=0;x<mainGamestate->getMap()->getWidth() && !build_settler;x++) {
				/*int diffx = abs(x - pos.x);
				int diffy = abs(y - pos.y);
				if( diffx + diffy > ignore_dist_c )
					continue; // don't bother considering*/
				ASSERT( mainGamestate->getMap()->isValidBase(x, y) );
				int this_dist = dists[y * mainGamestate->getMap()->getWidth() + x].cost;
				if( this_dist == -1 )
					continue;
				if( this_dist > ignore_dist_c * road_move_scale_c )
					continue; // don't bother considering
				const MapSquare *square = mainGamestate->getMap()->getSquare(x, y);
				/*if( !square->canMoveTo(potentialSettlersUnit) )
					continue;*/
				ASSERT( square->canMoveTo(potentialSettlersUnit) );
				/*if( !this->civilization->isExplored(x, y) ) {
					if( n_settlers == 0 && n_settlers_being_built == 0 ) {
						build_settler = true; // if 0 settlers, always build 1 to try exploring unexplored area
					}
					continue;
				}*/
				/*MapSquare *mapsq = map->get(x,y);
				if( !game->validCitySquare(x,y) )
				continue;*/
				//if( !game->getMap()->canBuildCity(x,y) )
				/*if( !mainGamestate->getMap()->getSquare(x, y)->canBuildCity() )
					continue;*/
				if( !AIInterface::canBuildCity(x, y) )
					continue;
				/*if( diffx + diffy > ignore_dist_c )
					continue; // don't bother considering*/
				/*if( this_dist > ignore_dist_c * road_move_scale_c )
					continue; // don't bother considering*/
				build_settler = true;
			}
		}
	}
	if( build_settler && settlersUnit != NULL ) {
		this->buildable = settlersUnit;
		VI_log("%s city of %s starts building %s [build a new city]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
		delete [] dists;
		AIInterface::setMainGamestate(NULL);
		AIInterface::setAICivilization(NULL);
		return;
	}

	// build workers
	int n_nearby_workers = 0;
	int n_improved_squares = 0; // also includes those currently being improved
	bool need_workers = false;
	/*for(int i=0;i<this->getNCitySquares();i++) {
		if( i == centre_city_square_c )
			continue;
		Pos2D pos = this->getCitySquare(i);
		if( !mainGamestate->getMap()->isValid(pos.x, pos.y) ) {
			continue;
		}*/
	vector<Pos2D> city_squares = AIInterface::getCitySquares(this->pos.x, this->pos.y, false);
	for(vector<Pos2D>::iterator iter = city_squares.begin(); iter != city_squares.end(); ++iter) {
		Pos2D adj_pos = *iter;

		//const MapSquare *square = mainGamestate->getMap()->getSquare(adj_pos);
		//bool can_be_improved = square->canBeImproved( this->getCivilization() );
		bool can_be_improved = AIInterface::canBeImproved(adj_pos.x, adj_pos.y);
		if( !need_workers && can_be_improved ) {
			ASSERT( mainGamestate->getMap()->isValidBase(adj_pos.x, adj_pos.y) );
			int this_dist = dists[adj_pos.y * mainGamestate->getMap()->getWidth() + adj_pos.x].cost;
			/*const MapSquare *square = mainGamestate->getMap()->getSquare(adj_pos);
			if( this_dist != -1 && square->canMoveTo( workerUnit ) ) {*/
			if( this_dist != -1 ) {
				need_workers = true;
			}
		}
		bool is_improved = false;
		const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(adj_pos.x, adj_pos.y);
		for(vector<Unit *>::const_iterator iter2 = units->begin(); iter2 != units->end(); ++iter2) {
			const Unit *unit = *iter2;
			if( unit->getCivilization() == this->civilization && unit->getTemplate()->canBuildRoads() ) {
				n_nearby_workers++;
				is_improved = true;
			}
		}
		if( !is_improved ) {
			Road road = AIInterface::getRoad(adj_pos.x, adj_pos.y);
			//if( !can_be_improved && square->getRoad() != ROAD_NONE ) {
			if( !can_be_improved && road != ROAD_UNKNOWN && road != ROAD_NONE ) {
			//if( !can_be_improved ) {
				is_improved = true;
			}
		}
		//if( is_improved && i != this->getCentreCitySquareIndex() ) {
		if( is_improved ) {
			n_improved_squares++;
		}
		//delete units;
	}
	if( this->size <= 2 && n_improved_squares >= max_road_bonus_c ) {
		need_workers = false; // small cities need fewer improved squares - this allows us to start on settlers sooner (also see corresponding code for Unit AI)
	}
	if( n_nearby_workers == 0 && need_workers ) {
		this->buildable = workerUnit;
		VI_log("%s city of %s starts building %s [worker]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
		delete [] dists;
		AIInterface::setMainGamestate(NULL);
		AIInterface::setAICivilization(NULL);
		return;
	}

	// build explorers
	bool build_explorer = false;
	bool all_explored  = true;
	for(int y=0;y<mainGamestate->getMap()->getHeight() && !build_explorer;y++) {
		for(int x=0;x<mainGamestate->getMap()->getWidth() && !build_explorer;x++) {
			/*int diffx = abs(x - pos.x);
			int diffy = abs(y - pos.y);
			if( diffx + diffy > ignore_dist_c )
				continue; // don't bother considering*/
			if( this->civilization->isExplored(x, y) )
				continue;
			all_explored = false;
			ASSERT( mainGamestate->getMap()->isValidBase(x, y) );
			/*int this_dist = dists[y * mainGamestate->getMap()->getWidth() + x].cost;
			if( this_dist == -1 )
				continue;*/
			// need to look at adjacent squares for dist!
			bool found_dist = false;
			int this_dist = 0;
			vector<Pos2D> adjacent_squares = AIInterface::getAdjacentSquares(x, y, false);
			for(vector<Pos2D>::iterator iter = adjacent_squares.begin(); iter != adjacent_squares.end(); ++iter) {
				Pos2D adjacent_square = *iter;
				int adj_dist = dists[adjacent_square.y * mainGamestate->getMap()->getWidth() + adjacent_square.x].cost;
				if( adj_dist != -1 ) {
					if( !found_dist || adj_dist < this_dist ) {
						found_dist = true;
						this_dist = adj_dist;
					}
				}
			}
			if( !found_dist ) {
				// the square is surrounded by unexplored squares, so there must be another one closer
				continue;
			}
			if( this_dist > ignore_dist_c * road_move_scale_c )
				continue; // don't bother considering
			/*const MapSquare *square = mainGamestate->getMap()->getSquare(x, y);
			if( !square->canMoveTo(fastestUnit) )
				continue;*/
			build_explorer = true;
		}
	}
	// don't build more explorers if we have lots of nearby workers who could do the job later (if the explorer unit can also work, we just end up building loads of explorers...)
	if( n_nearby_workers < 2 && n_explorers + n_explorers_being_built < max_explorers && build_explorer ) {
		/*Unit::Type move_type = this->bestBuildableUnit(MOVEMENT);
		this->building = Buildable::getBuildable(Buildable::UNIT, move_type);*/
		this->buildable = fastestUnit;
		VI_log("%s city of %s starts building %s [explorer]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
		delete [] dists;
		AIInterface::setMainGamestate(NULL);
		AIInterface::setAICivilization(NULL);
		return;
	}

	// now try again
	if( this->chooseBuildPopLimits() ) {
		delete [] dists;
		AIInterface::setMainGamestate(NULL);
		AIInterface::setAICivilization(NULL);
		return;
	}

	if( !build_explorer && !all_explored ) {
		// if build_explorer is false, it means there are no nearby squares left to explore - try building sea improvements
		// if all explored, not as urgent to do this (still useful for travel, but we'll build them with the other improvements)
		if( !this->hasImprovement(IMPROVEMENT_PORT) ) {
			const Buildable *b = mainGamestate->findImprovement(IMPROVEMENT_PORT);
			if( this->canBuild(b) ) {
				this->buildable = b;
				VI_log("%s city of %s starts building %s [port]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
				delete [] dists;
				AIInterface::setMainGamestate(NULL);
				AIInterface::setAICivilization(NULL);
				return;
			}
		}
		if( !this->hasImprovement(IMPROVEMENT_HARBOUR) ) {
			const Buildable *b = mainGamestate->findImprovement(IMPROVEMENT_HARBOUR);
			if( this->canBuild(b) ) {
				this->buildable = b;
				VI_log("%s city of %s starts building %s [harbour]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
				delete [] dists;
				AIInterface::setMainGamestate(NULL);
				AIInterface::setAICivilization(NULL);
				return;
			}
		}
	}

	if( this->size == 1 && build_settler ) {
		// remember, only a viable tactic if the city is going to grow to size 2
		this->buildable = cheapestUnit;
		VI_log("%s city of %s starts building %s [cheap unit whilst growing]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
		delete [] dists;
		AIInterface::setMainGamestate(NULL);
		AIInterface::setAICivilization(NULL);
		return;
	}

	// check already build enough air units
	{
		/*const MapSquare *square = mainGamestate->getMap()->getSquare(pos);
		const vector<Unit *> *units = square->getUnits();*/
		const vector<Unit *> *units = AIInterface::getUnits(pos.x, pos.y);
		int n_fighters = 0, n_bombers = 0;
		for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter) {
			const Unit *unit = *iter;
			if( bestFighterAirUnit != NULL && unit->getTemplate() == bestFighterAirUnit ) {
				n_fighters++;
			}
			if( bestBomberAirUnit != NULL && unit->getTemplate() == bestBomberAirUnit ) {
				n_bombers++;
			}
		}
		if( n_fighters >= 4 ) {
			VI_log("%s city of %s: already have %d fighters\n", this->civilization->getNameAdjective(), this->getName(), n_fighters);
			bestFighterAirUnit = NULL;
		}
		if( n_bombers >= 4 ) {
			VI_log("%s city of %s: already have %d bombers\n", this->civilization->getNameAdjective(), this->getName(), n_bombers);
			bestBomberAirUnit = NULL;
		}
	}

	// air/missile units for civs at war
	if( ( bestFighterAirUnit != NULL || bestBomberAirUnit != NULL || bestConventionalMissileUnit != NULL || bestNuclearMissileUnit != NULL )
		&& ( rand() % 2 == 0 ) // only build air/missile units some of the time
		) {
		const UnitTemplate *unit = chooseAirUnitAI(bestFighterAirUnit, bestBomberAirUnit, bestConventionalMissileUnit, bestNuclearMissileUnit, true);
		if( unit != NULL ) {
			this->buildable = unit;
			VI_log("%s city of %s starts building %s [bomb enemy city, at war]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
			delete [] dists;
			AIInterface::setMainGamestate(NULL);
			AIInterface::setAICivilization(NULL);
			return;
		}
	}

	// build attackers for nearby enemies
	if( bestUnit != NULL ) {
		/*for(int i=0;i<mainGamestate->getNCivilizations();i++) {
			const Civilization *civ = mainGamestate->getCivilization(i);
			if( civ == this->civilization )
				continue;
			if( civ->isDead() )
				continue;
			const Relationship *relationship = mainGamestate->findRelationship(this->civilization, civ);
			if( relationship->getStatus() == Relationship::STATUS_PEACE )
				continue;*/
		vector<Civilization *> civs = AIInterface::getCivilizations(AIInterface::GETCIVILIZATIONS_WAR);
		for(vector<Civilization *>::iterator iter = civs.begin(); iter != civs.end(); ++iter) {
			Civilization *civ = *iter;
			// attack cities?
			/*for(int j=0;j<civ->getNCities();j++) {
				const City *city = civ->getCity(j);
				int c_xpos = city->getX();
				int c_ypos = city->getY();
				if( !this->civilization->isExplored(c_xpos, c_ypos) )
					continue;*/
			vector<City *> cities = AIInterface::getCities(civ);
			for(vector<City *>::const_iterator iter2 = cities.begin(); iter2 != cities.end(); ++iter2) {
				const City *city = *iter2;
				int c_xpos = city->getX();
				int c_ypos = city->getY();
				ASSERT( mainGamestate->getMap()->isValidBase(c_xpos, c_ypos) );
				int this_dist = dists[c_ypos * mainGamestate->getMap()->getWidth() + c_xpos].cost;
				if( this_dist == -1 )
					continue;
				if( this_dist > ignore_dist_c * road_move_scale_c )
					continue; // don't bother considering
				this->buildable = bestUnit;
				VI_log("%s city of %s starts building %s [attack enemy city of %s]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName(), city->getName());
				delete [] dists;
				AIInterface::setMainGamestate(NULL);
				AIInterface::setAICivilization(NULL);
				return;
			}
			// attack units?
			vector<Unit *> units = AIInterface::getUnits(civ);
			/*for(int j=0;j<civ->getNUnits();j++) {
				const Unit *unit = civ->getUnit(j);
				int u_xpos = unit->getX();
				int u_ypos = unit->getY();
				if( !this->civilization->isExplored(u_xpos, u_ypos) )
					continue;*/
			for(vector<Unit *>::const_iterator iter2 = units.begin(); iter2 != units.end(); ++iter2) {
				const Unit *unit = *iter2;
				int u_xpos = unit->getX();
				int u_ypos = unit->getY();
				ASSERT( mainGamestate->getMap()->isValidBase(u_xpos, u_ypos) );
				int this_dist = dists[u_ypos * mainGamestate->getMap()->getWidth() + u_xpos].cost;
				if( this_dist == -1 )
					continue;
				if( this_dist > ignore_dist_c * road_move_scale_c )
					continue; // don't bother considering
				this->buildable = bestUnit;
				VI_log("%s city of %s starts building %s [attack enemy unit]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
				delete [] dists;
				AIInterface::setMainGamestate(NULL);
				AIInterface::setAICivilization(NULL);
				return;
			}
		}
	}

	// build improvements
	//for(int i=0;i<N_IMPROVEMENTS;i++) {
	/*const Improvement *cheapest_b = NULL;
	int cheapest_b_cost = 0;
	for(int i=0;i<game->getNImprovements();i++) {
		//Improvement *b = (Improvement *)Improvement::getImprovement(static_cast<ImprovementType>(i));
		const Improvement *b = game->getImprovement(i);
		if( this->canBuild( b ) ) {
			if( cheapest_b == NULL || b->getCost() < cheapest_b_cost ) {
				cheapest_b = b;
				cheapest_b_cost = b->getCost();
			}
		}
	}
	if( cheapest_b != NULL ) {
		this->buildable = cheapest_b;
		VI_log("%s city of %s starts building %s [improvement]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
		delete [] dists;
		AIInterface::setMainGamestate(NULL);
		AIInterface::setAICivilization(NULL);
		return;
	}*/

	if( !tried_improvements ) {
		const Improvement *improvement = chooseImprovementAI();
		if( improvement != NULL ) {
			this->buildable = improvement;
			VI_log("%s city of %s starts building %s [improvement] (2nd phase)\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
			delete [] dists;
			AIInterface::setMainGamestate(NULL);
			AIInterface::setAICivilization(NULL);
			return;
		}
	}

	// air/missile units for civs at peace
	if( ( bestFighterAirUnit != NULL || bestBomberAirUnit != NULL || bestConventionalMissileUnit != NULL || bestNuclearMissileUnit != NULL )
		&& ( bestUnit == NULL || rand() % 2 == 0 ) // only build air/missile units some of the time
		) {
		const UnitTemplate *unit = chooseAirUnitAI(bestFighterAirUnit, bestBomberAirUnit, bestConventionalMissileUnit, bestNuclearMissileUnit, false);
		if( unit != NULL ) {
			this->buildable = unit;
			VI_log("%s city of %s starts building %s [bomb enemy city, at peace]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
			delete [] dists;
			AIInterface::setMainGamestate(NULL);
			AIInterface::setAICivilization(NULL);
			return;
		}
	}

	// build attackers
	if( bestUnit != NULL ) {
		this->buildable = bestUnit;
		VI_log("%s city of %s starts building %s [attacker]\n", this->civilization->getNameAdjective(), this->getName(), this->buildable->getName());
		delete [] dists;
		AIInterface::setMainGamestate(NULL);
		AIInterface::setAICivilization(NULL);
		return;
	}

	delete [] dists;
	AIInterface::setMainGamestate(NULL);
	AIInterface::setAICivilization(NULL);

#endif
}
