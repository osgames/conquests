#include "conquests_stdafx.h"

#include "citywindow.h"
#include "city.h"
#include "civilization.h"
#include "buildable.h"
#include "unit.h"
#include "map.h"
#include "maingamestate.h"
#include "infowindow.h"

#include "../Vision/VisionIface.h"

#include <sstream>
using std::stringstream;

CityWindow::CityWindow(MainGamestate *mainGamestate, City *city) :
mainGamestate(mainGamestate), city(city), floatingInfoWindow(NULL),
okayButton(NULL), escapeHiddenButton(NULL), destroyButton(NULL), statsButton(NULL),
buildableListbox(NULL), changeBuildButton(NULL), infoBuildButton(NULL), adviseBuildButton(NULL), improvementsListbox(NULL),
unitsListbox(NULL), activateUnitButton(NULL), activateAllUnitsButton(NULL), fortifyAllUnitsButton(NULL),
elementsListbox(NULL),
cached_production(0), cached_science(0), cached_power_per_turn(0) {
	/*if( InfoWindow::getActiveWindow() != NULL ) {
	// delete existing one
	delete InfoWindow::getActiveWindow();
	}
	*/

	this->cached_production = city->calculateProduction();
	this->cached_science = city->calculateScience();
	this->cached_power_per_turn = city->calculatePowerPerTurn();

	this->panel = VI_createPanel();
	this->panel->setPaintFunc(paintPanel, this);
	//const int w = 480, h = 480;
	const int panel_x = 128, panel_y = 32;
	int w = game_g->getGraphicsEnvironment()->getWidth() - 2 * panel_x;
	//int w = game_g->getGraphicsEnvironment()->getWidth() - panel_x - 64;
	w = max(w, 496);
	w = min(w, 640);
	int h = game_g->getGraphicsEnvironment()->getHeight() - panel_y - 16;
	h = min(h, 480);
	this->panel->setSize(w, h);
	this->panel->setBackground(1.0f, 1.0f, 1.0f, 0.9f);
	this->panel->setTexture(mainGamestate->getCityPanelTexture());

	int xpos = 16;

	okayButton = VI_createButton("Okay", game_g->getFont());
	okayButton->setUserData(this);
	okayButton->setAction(action);
	okayButton->setPopupText("Close the city window.", game_g->getFont());
	okayButton->setKeyShortcut(V_RETURN);
	this->panel->addChildPanel(okayButton, xpos, h - 32);
	xpos += okayButton->getWidth() + 16;

	escapeHiddenButton = VI_createPanel();
	escapeHiddenButton->setUserData(this);
	escapeHiddenButton->setAction(action);
	escapeHiddenButton->setKeyShortcut(V_ESCAPE);
	this->panel->addChildPanel(escapeHiddenButton, 0, 0);

	destroyButton = VI_createButton("Destroy City", game_g->getFont());
	destroyButton->setUserData(this);
	destroyButton->setAction(action);
	destroyButton->setPopupText("Get rid of this city if you no longer want it.", game_g->getFont());
	this->panel->addChildPanel(destroyButton, xpos, h - 32);
	xpos += destroyButton->getWidth() + 16;

	statsButton = VI_createButton("City Stats", game_g->getFont());
	statsButton->setUserData(this);
	statsButton->setAction(action);
	statsButton->setPopupText("Displays information about bonuses and penalties for this city.", game_g->getFont());
	this->panel->addChildPanel(statsButton, xpos, h - 32);
	xpos += statsButton->getWidth() + 16;

	vector<const Buildable *> *buildables = new vector<const Buildable *>();
	vector<string> buildable_names;
	mainGamestate->fillBuildablesList(buildables, &buildable_names, city);
	/*for(int i=0;i<mainGamestate->getNBuildables();i++) {
		const Buildable *buildable = mainGamestate->getBuildable(i);
		if( city->canBuild(buildable) ) {
			buildables->push_back(buildable);
		}
	}
	// convert to names
	vector<string> buildable_names;
	for(vector<const Buildable *>::const_iterator iter = buildables->begin();iter != buildables->end(); ++iter) {
		const Buildable *buildable = *iter;
		//const char *name = buildable->getName();
		stringstream name;
		name << buildable->getName();
		if( buildable->getType() == Buildable::TYPE_UNIT ) {
			const UnitTemplate *unit_template = static_cast<const UnitTemplate *>(buildable);
			name << " " << MainGamestate::getUnitStatsString(unit_template, true);
		}
		name << " (";
		name << buildable->getCost();
		name << ")";
		buildable_names.push_back(name.str());
	}*/
	//delete buildables;
	int ypos = 112;
	if( buildable_names.size() > 0 ) {
		buildableListbox = VI_createListbox(256, 128, &*buildable_names.begin(), buildable_names.size(), game_g->getFont());
		buildableListbox->setHasInputFocus(true);
		buildableListbox->setUserData(buildables);
		buildableListbox->setForeground(0.0f, 0.0f, 0.0f);
		//buildableListbox->setPopupText("Select an item that you wish to build, then click \"Change Build\" below.", game->getFont());
		buildableListbox->setActive(0);
		this->panel->addChildPanel(buildableListbox, 16, ypos);

		changeBuildButton = VI_createButton("Change Build", game_g->getFont());
		changeBuildButton->setUserData(this);
		changeBuildButton->setAction(action);
		changeBuildButton->setPopupText("Change the current build item to the item selected in the list above.", game_g->getFont());
		this->panel->addChildPanel(changeBuildButton, 16, ypos + 136);

		infoBuildButton = VI_createButton("Info.", game_g->getFont());
		infoBuildButton->setUserData(this);
		infoBuildButton->setAction(action);
		infoBuildButton->setPopupText("Show information about the item selected in the list above.", game_g->getFont());
		this->panel->addChildPanel(infoBuildButton, 120, ypos + 136);

		adviseBuildButton = VI_createButton("Advisor", game_g->getFont());
		adviseBuildButton->setUserData(this);
		adviseBuildButton->setAction(action);
		adviseBuildButton->setPopupText("Ask for advice on what to build.", game_g->getFont());
		this->panel->addChildPanel(adviseBuildButton, 160, ypos + 136);
	}
	else {
		buildableListbox = NULL;
		changeBuildButton = NULL;
		infoBuildButton = NULL;
		adviseBuildButton = NULL;
	}

	if( city->getNImprovements() > 0 ) {
		vector<string> improvement_names;
		//improvement_names.push_back("Improvements:");
		for(size_t i=0;i<city->getNImprovements();i++) {
			const Improvement *improvement = city->getImprovement(i);
			stringstream name;
			name << improvement->getName();
			improvement_names.push_back(name.str());
		}
		//improvementsListbox = VI_createListbox(256, 224, &*improvement_names.begin(), improvement_names.size(), game_g->getFont());
		//improvementsListbox = VI_createListbox(w - 336, 224, &*improvement_names.begin(), improvement_names.size(), game_g->getFont());
		improvementsListbox = VI_createListbox(w - 336, 140, &*improvement_names.begin(), improvement_names.size(), game_g->getFont());
		improvementsListbox->setForeground(0.0f, 0.0f, 0.0f);
		improvementsListbox->setAllowSelect(false); // so the user can't select items (as no point in doing so)
		this->panel->addChildPanel(improvementsListbox, 320, 16);
	}
	else {
		improvementsListbox = NULL;
	}

	vector<string> element_stocks_names;
	for(size_t i=0;i<game_g->getGameData()->getNElements();i++) {
		const Element *element = game_g->getGameData()->getElement(i);
		int amount = this->city->getElementStocks(element);
		if( amount == 0 && element->getRequiresTechnology() != NULL && !this->city->getCivilization()->hasTechnology( element->getRequiresTechnology() ) ) {
			continue;
		}
		stringstream str;
		str << element->getName() << ": " << amount;
		element_stocks_names.push_back(str.str());
	}
	elementsListbox = VI_createListbox(w - 336, 104, &*element_stocks_names.begin(), element_stocks_names.size(), game_g->getFont());
	elementsListbox->setForeground(0.0f, 0.0f, 0.0f);
	elementsListbox->setAllowSelect(false); // so the user can't select items (as no point in doing so)
	this->panel->addChildPanel(elementsListbox, 320, 160);

	const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(city->getX(), city->getY());
	vector<Unit *> *units_copy = new vector<Unit *>(); // hack as setUserData only accepts non-const pointer
	for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter) {
		Unit *unit = *iter;
		units_copy->push_back(unit);
	}
	// convert to names
	vector<string> unit_names = MainGamestate::getUnitNames(units, false);
	//ypos += 168;
	ypos += 160;
	if( unit_names.size() > 0 ) {
		//int listbox_h = h - 352;
		//int listbox_h = h - 344;
		int listbox_h = h - 336;
		//int listbox_h = h - 320;
		unitsListbox = VI_createListbox(w - 32, listbox_h, &*unit_names.begin(), unit_names.size(), game_g->getFont());
		// don't give input focus
		unitsListbox->setForeground(0.0f, 0.0f, 0.0f);
		unitsListbox->setUserData(units_copy);
		unitsListbox->setActive(0);
		this->panel->addChildPanel(unitsListbox, 16, ypos);

		int xpos = 16;
		activateUnitButton = VI_createButton("Activate Unit", game_g->getFont());
		activateUnitButton->setUserData(this);
		activateUnitButton->setAction(action);
		activateUnitButton->setPopupText("Activate (and unfortify) the unit selected in the list above.", game_g->getFont());
		//this->panel->addChildPanel(activateUnitButton, 16, ypos + 136);
		this->panel->addChildPanel(activateUnitButton, xpos, ypos + listbox_h + 8);
		xpos += activateUnitButton->getWidth() + 8;

		activateAllUnitsButton = VI_createButton("Activate All Units", game_g->getFont());
		activateAllUnitsButton->setUserData(this);
		activateAllUnitsButton->setAction(action);
		activateAllUnitsButton->setPopupText("Activate (and unfortify) all the units in the list above.", game_g->getFont());
		//this->panel->addChildPanel(activateAllUnitsButton, 112, ypos + 136);
		//this->panel->addChildPanel(activateAllUnitsButton, 112, ypos + listbox_h + 8);
		this->panel->addChildPanel(activateAllUnitsButton, xpos, ypos + listbox_h + 8);
		xpos += activateAllUnitsButton->getWidth() + 8;

		fortifyAllUnitsButton = VI_createButton("Fortify All Units", game_g->getFont());
		fortifyAllUnitsButton->setUserData(this);
		fortifyAllUnitsButton->setAction(action);
		fortifyAllUnitsButton->setPopupText("Fortify all the units in the list above.", game_g->getFont());
		this->panel->addChildPanel(fortifyAllUnitsButton, xpos, ypos + listbox_h + 8);
		xpos += fortifyAllUnitsButton->getWidth() + 8;

	}
	else {
		unitsListbox = NULL;
		activateUnitButton = NULL;
		activateAllUnitsButton = NULL;
		fortifyAllUnitsButton = NULL;
	}

	mainGamestate->getPanel()->addChildPanel(this->panel, panel_x, panel_y);

	//game->lockGUI();
	game_g->getGraphicsEnvironment()->setModalPanel( this->panel );
}

CityWindow::~CityWindow() {
	if( this->floatingInfoWindow != NULL ) {
		delete floatingInfoWindow;
	}
	/*if( InfoWindow::getActiveWindow() != NULL ) {
		delete InfoWindow::getActiveWindow();
	}*/
	if( this->buildableListbox != NULL ) {
		vector<const Buildable *> *buildables = static_cast<vector<const Buildable *> *>(this->buildableListbox->getUserData());
		delete buildables;
	}
	if( this->unitsListbox != NULL ) {
		vector<Unit *> *units = static_cast<vector<Unit *> *>(this->unitsListbox->getUserData());
		delete units;
	}

	//mainGamestate->getPanel()->remove(this->panel);
	delete this->panel;
	/*delete this->okayButton;
	delete this->escapeHiddenButton;
	delete this->destroyButton;
	delete this->statsButton;
	delete this->buildableListbox;
	delete this->changeBuildButton;
	delete this->infoBuildButton;
	delete this->unitsListbox;
	delete this->activateUnitButton;
	delete this->activateAllUnitsButton;
	delete this->fortifyAllUnitsButton;
	delete this->elementsListbox;*/
	//game->unlockGUI();
	//game->getGraphicsEnvironment()->setModalPanel(NULL);
}

void CityWindow::paintPanel(VI_Graphics2D *g2d, void *data) {
	CityWindow *cityWindow = static_cast<CityWindow *>(data);
	const City *city = cityWindow->city;
	g2d->setColor3i(0, 0, 0);
	int xpos = 16, ypos = 16;
	const int ydiff = 16;
	game_g->getFont()->writeTextExt(xpos, ypos, "%s", city->getName()); ypos += ydiff;
	//game_g->getFont()->writeTextExt(xpos, ypos, "Size: %d", city->getSize()); ypos += ydiff;
	//game_g->getFont()->writeTextExt(xpos, ypos, "Population: %d", city->getPopulation()); ypos += ydiff;
	game_g->getFont()->writeTextExt(xpos, ypos, "Population: %s", city->getPopulationString().c_str()); ypos += ydiff;
	game_g->getFont()->writeTextExt(xpos+8, ypos, "(Growth: %d / %d)", city->getProgressPopulation(), city->getPopulationCost()); ypos += ydiff;
		// n.b., offset xpos, to avoid clash with darkened background!
	//ypos += ydiff;
	ypos += ydiff;
	const Buildable *buildable = city->getBuildable();
	if( buildable == NULL ) {
		g2d->setColor3i(255, 0, 0);
		game_g->getFont()->writeText(xpos, ypos, "Building: Nothing"); ypos+= ydiff;
		g2d->setColor3i(0, 0, 0);
	}
	else {
		game_g->getFont()->writeTextExt(xpos, ypos, "Building: %s", buildable->getName()); ypos += ydiff;
		game_g->getFont()->writeTextExt(xpos, ypos, "(Built: %d / %d)", city->getProgressBuildable(), buildable->getCost());
	}

	xpos = 160;
	ypos = 16;
	//game_g->getFont()->writeTextExt(xpos, ypos, "Production: %d", city->calculateProduction()); ypos += ydiff;
	//game_g->getFont()->writeTextExt(xpos, ypos, "Science: %d", city->calculateScience()); ypos += ydiff;
	//game_g->getFont()->writeTextExt(xpos, ypos, "Power Per Turn: %d", city->calculatePowerPerTurn()); ypos += ydiff;
	game_g->getFont()->writeTextExt(xpos, ypos, "Production: %d", cityWindow->cached_production); ypos += ydiff;
	game_g->getFont()->writeTextExt(xpos, ypos, "Science: %d", cityWindow->cached_science); ypos += ydiff;
	game_g->getFont()->writeTextExt(xpos, ypos, "Power Per Turn: %d", cityWindow->cached_power_per_turn); ypos += ydiff;
	if( city->isResisting() ) {
		g2d->setColor3i(255, 0, 0);
		game_g->getFont()->writeTextExt(xpos, ypos, "RESISTANCE: %d%%", city->getResistance());
		g2d->setColor3i(0, 0, 0);
	}
	ypos += ydiff;

	/*xpos = 320;
	ypos = 16;
	for(int i=0;i<city->getNImprovements();i++) {
		const Improvement *improvement = city->getImprovement(i);
		if( i == 0 ) {
			game_g->getFont()->writeText(xpos, ypos, "Improvements:"); ypos+= ydiff;
		}
		game_g->getFont()->writeTextExt(xpos, ypos, "%s", improvement->getName()); ypos+= ydiff;
	}
	ypos+= ydiff;*/
}

void CityWindow::action(VI_Panel *source) {
	CityWindow *cityWindow = static_cast<CityWindow *>(source->getUserData());
	cityWindow->mainGamestate->playSound(MainGamestate::SOUND_BLIP);
	if( source == cityWindow->okayButton || source == cityWindow->escapeHiddenButton ) {
		MainGamestate *mainGamestate = cityWindow->mainGamestate;
		delete cityWindow;
		//game_g->getGraphicsEnvironment()->flushInput(); // needed to avoid problem where the mouse click is registered after the window closes - is there a better way to do this?
		game_g->getGraphicsEnvironment()->flushInput(); // needed to avoid re-registering of key presses for shortcuts - is there a better way to do this?
		//return;
	}
	else if( source == cityWindow->destroyButton ) {
		MainGamestate *mainGamestate = cityWindow->mainGamestate;
		stringstream str;
		str << "Are you sure you wish to destroy the city of " << cityWindow->city->getName() << "?";
		if( InfoWindow::confirm(game_g->getGraphicsEnvironment(), cityWindow->panel, game_g->getFont(), game_g->getPanelTexture(), str.str().c_str(), "Yes, destroy the city", "No") ) {
			City *city = cityWindow->city;
			mainGamestate->addFireEffect(city->getPos());
			// need to also destroy any air and sea units
			const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(city->getPos());
			// n.b. - count backwards as the units vector is modified when we delete units!
			//for(int i=units->size()-1;i>=0;i--) {
			for(size_t i=units->size(); i-- > 0 ; ) {
				const Unit *unit = units->at(i);
				if( unit->getTemplate()->isAir() || unit->getTemplate()->isSea() ) {
					if( mainGamestate->getActiveUnit() == unit ) {
						mainGamestate->setActiveUnit(NULL);
					}
					delete unit;
				}
			}
			delete cityWindow;
			delete city;
		}
		//return;
	}
	else if( source == cityWindow->statsButton ) {
		MainGamestate *mainGamestate = cityWindow->mainGamestate;
		string text = cityWindow->city->getStatsInfo();
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), cityWindow->panel, text, game_g->getFont(), game_g->getPanelTexture());
		window->doModal();
		delete window;
	}
	else if( source == cityWindow->changeBuildButton ) {
		int active = cityWindow->buildableListbox->getActive();
		if( active != -1 ) {
			vector<const Buildable *> *buildables = static_cast<vector<const Buildable *> *>(cityWindow->buildableListbox->getUserData());
			const Buildable *buildable = buildables->at(active);
			bool ok = true;
			int current_progress = cityWindow->city->getProgressBuildable();
			if( buildable != cityWindow->city->getBuildable() && current_progress > 0 ) {
				stringstream text;
				text << "Are you sure you wish to change production and waste " << current_progress << " resources?";
				if( !InfoWindow::confirm(game_g->getGraphicsEnvironment(), cityWindow->panel, game_g->getFont(), game_g->getPanelTexture(), text.str(), "Yes, Change Production", "Cancel") ) {
					ok = false;
				}
			}
			if( ok ) {
				cityWindow->city->setBuildable(buildable);
			}
		}
	}
	else if( source == cityWindow->infoBuildButton ) {
		int active = cityWindow->buildableListbox->getActive();
		if( active != -1 ) {
			vector<const Buildable *> *buildables = static_cast<vector<const Buildable *> *>(cityWindow->buildableListbox->getUserData());
			const Buildable *buildable = buildables->at(active);
			string text = buildable->getHelp();
			cityWindow->createInfoWindow(text);
		}
	}
	else if( source == cityWindow->adviseBuildButton ) {
		const Buildable *suggestion = cityWindow->city->getBuildChoiceAI();
		stringstream text;
		if( suggestion == NULL ) {
			text << "I can't offer any advice right now.";
		}
		else {
			if( suggestion->getType() == Buildable::TYPE_UNIT ) {
				text << "I suggest we build ";
				text << prependAorAn( suggestion->getName() );
				text << " unit.";
			}
			else {
				text << "I suggest we build: ";
				text << prependAorAn( suggestion->getName() );
				text << ".";
			}
			const char *advice = suggestion->getAdvice();
			if( *advice != '\0' ) {
				text << " " << advice;
			}
		}
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), cityWindow->panel, text.str(), game_g->getFont(), game_g->getPanelTexture(), 64, 64, 320, 128);
		window->doModal();
		delete window;
	}
	else if( source == cityWindow->activateUnitButton && cityWindow->city->getCivilization() == cityWindow->mainGamestate->getPlayer() ) {
		// need check for player in case of debug mode
		int active = cityWindow->unitsListbox->getActive();
		if( active != -1 ) {
			vector<Unit *> *units = static_cast<vector<Unit *> *>(cityWindow->unitsListbox->getUserData());
			Unit *unit = units->at(active);
			if( cityWindow->mainGamestate->getPhase() == MainGamestate::PHASE_PLAYERTURN && unit->hasMovesLeft() ) {
				cityWindow->mainGamestate->activateUnit(unit, false);
			}
			else {
				// still activate (for next turn?)
				//unit->setStatus(Unit::STATUS_NORMAL);
				cityWindow->mainGamestate->wakeupUnit(unit);
			}
			vector<string> unit_names = MainGamestate::getUnitNames(units, false);
			cityWindow->unitsListbox->reset(&*unit_names.begin(), unit_names.size());
			//cityWindow->unitsListbox->setActive(active);
		}
	}
	else if( source == cityWindow->activateAllUnitsButton && cityWindow->city->getCivilization() == cityWindow->mainGamestate->getPlayer() ) {
		// need check for player in case of debug mode
		vector<Unit *> *units = static_cast<vector<Unit *> *>(cityWindow->unitsListbox->getUserData());
		for(vector<Unit *>::iterator iter = units->begin(); iter != units->end(); ++iter) {
			Unit *unit = *iter;
			if( cityWindow->mainGamestate->getPhase() == MainGamestate::PHASE_PLAYERTURN && unit->hasMovesLeft() ) {
				cityWindow->mainGamestate->activateUnit(unit, false);
			}
			else {
				// still activate (for next turn?)
				//unit->setStatus(Unit::STATUS_NORMAL);
				cityWindow->mainGamestate->wakeupUnit(unit);
			}
			vector<string> unit_names = MainGamestate::getUnitNames(units, false);
			cityWindow->unitsListbox->reset(&*unit_names.begin(), unit_names.size());
		}
	}
	else if( source == cityWindow->fortifyAllUnitsButton && cityWindow->city->getCivilization() == cityWindow->mainGamestate->getPlayer() ) {
		// need check for player in case of debug mode
		vector<Unit *> *units = static_cast<vector<Unit *> *>(cityWindow->unitsListbox->getUserData());
		for(vector<Unit *>::iterator iter = units->begin(); iter != units->end(); ++iter) {
			Unit *unit = *iter;
			unit->setStatus(Unit::STATUS_FORTIFIED);
			if( unit == cityWindow->mainGamestate->getActiveUnit() ) {
				cityWindow->mainGamestate->setActiveUnit(NULL);
			}
			vector<string> unit_names = MainGamestate::getUnitNames(units, false);
			cityWindow->unitsListbox->reset(&*unit_names.begin(), unit_names.size());
		}
	}
}

/*void CityWindow::actionInfoWindow(VI_Panel *source) {
	void *ptr = source->getUserData();
	CityWindow *cityWindow = static_cast<CityWindow *>(ptr);
	cityWindow->mainGamestate->playSound(MainGamestate::SOUND_BLIP);
	ASSERT( cityWindow->floatingInfoWindow != NULL );
	delete cityWindow->floatingInfoWindow;
	cityWindow->floatingInfoWindow = NULL;
	game_g->getGraphicsEnvironment()->flushInput(); // needed to avoid problem where the mouse click is registered after the window closes - is there a better way to do this?
}*/

void CityWindow::actionInfoWindow(InfoWindow *infoWindow) {
	//void *ptr = source->getUserData();
	void *ptr = infoWindow->getUserData();
	CityWindow *cityWindow = static_cast<CityWindow *>(ptr);
	//cityWindow->mainGamestate->playSound(MainGamestate::SOUND_BLIP);
	T_ASSERT( cityWindow->floatingInfoWindow != NULL );
	//delete cityWindow->floatingInfoWindow;
	cityWindow->floatingInfoWindow = NULL;
	//game_g->getGraphicsEnvironment()->flushInput(); // needed to avoid problem where the mouse click is registered after the window closes - is there a better way to do this?
	game_g->getGraphicsEnvironment()->flushInput(); // needed to avoid re-registering of key presses for shortcuts - is there a better way to do this?
}

void CityWindow::createInfoWindow(string text) {
	if( this->floatingInfoWindow != NULL ) {
		// delete existing one
		delete floatingInfoWindow;
	}
	/*if( InfoWindow::getActiveWindow() != NULL ) {
		// delete existing one
		delete InfoWindow::getActiveWindow();
	}*/
	string okay = "Okay";
	int window_x = 312;
	int window_w = 200;
	this->floatingInfoWindow = new InfoWindow(game_g->getGraphicsEnvironment(), this->panel, text, game_g->getFont(), game_g->getPanelTexture(), &okay, 1, window_x, 16, window_w, 320);
	/*this->floatingInfoWindow->getButton(0)->setAction(actionInfoWindow);
	this->floatingInfoWindow->getButton(0)->setUserData(this);*/
	this->floatingInfoWindow->setCloseAction(actionInfoWindow);
	this->floatingInfoWindow->setUserData(this);
}
