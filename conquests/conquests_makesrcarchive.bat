rd /S /Q c:\temp\conquests_src\

mkdir c:\temp\conquests_src\

REM set src="c:\documents and settings\mark\work\programming\vision3"
REM set src="u:\programming\vision3"
set src=".."
set dst="c:\temp\conquests_src"

copy %src%\gpl.txt %dst%
copy %src%\SDL_main.c %dst%

mkdir %dst%\conquests

copy %src%\conquests\conquests.cpp %dst%\conquests
copy %src%\conquests\conquests.vcxproj %dst%\conquests
copy %src%\conquests\conquests.vcxproj.filters %dst%\conquests
copy %src%\conquests\conquests.sln %dst%\conquests
copy %src%\conquests\resource.rc %dst%\conquests
copy %src%\conquests\conquests.ico %dst%\conquests

REM N.B., moving the text file to the root directory
copy %src%\conquests\conquests_source.txt %dst%

REM copy %src%\conquests\conquests.cbp %dst%\conquests
copy %src%\conquests\makefile %dst%\conquests
copy %src%\conquests\conquests64.png %dst%\conquests

copy %src%\conquests\conquests_makearchive.bat %dst%\conquests
copy %src%\conquests\conquests_makesrcarchive.bat %dst%\conquests
copy %src%\conquests\readme.html %dst%\conquests
copy %src%\conquests\reference.html %dst%\conquests
copy %src%\conquests\vision_paths.config %dst%\conquests

copy %src%\conquests\buildable.cpp %dst%\conquests
copy %src%\conquests\buildable.h %dst%\conquests
copy %src%\conquests\city.cpp %dst%\conquests
copy %src%\conquests\city.h %dst%\conquests
copy %src%\conquests\citywindow.cpp %dst%\conquests
copy %src%\conquests\citywindow.h %dst%\conquests
copy %src%\conquests\civilization.cpp %dst%\conquests
copy %src%\conquests\civilization.h %dst%\conquests
copy %src%\conquests\common.h %dst%\conquests
copy %src%\conquests\conquests_stdafx.cpp %dst%\conquests
copy %src%\conquests\conquests_stdafx.h %dst%\conquests
copy %src%\conquests\game.cpp %dst%\conquests
copy %src%\conquests\game.h %dst%\conquests
copy %src%\conquests\infowindow.cpp %dst%\conquests
copy %src%\conquests\infowindow.h %dst%\conquests
copy %src%\conquests\maingamestate.cpp %dst%\conquests
copy %src%\conquests\maingamestate.h %dst%\conquests
copy %src%\conquests\map.cpp %dst%\conquests
copy %src%\conquests\map.h %dst%\conquests
copy %src%\conquests\optionsgamestate.cpp %dst%\conquests
copy %src%\conquests\optionsgamestate.h %dst%\conquests
copy %src%\conquests\scripts.cpp %dst%\conquests
copy %src%\conquests\scripts.h %dst%\conquests
copy %src%\conquests\technology.cpp %dst%\conquests
copy %src%\conquests\technology.h %dst%\conquests
copy %src%\conquests\tests.cpp %dst%\conquests
copy %src%\conquests\tests.h %dst%\conquests
copy %src%\conquests\unit.cpp %dst%\conquests
copy %src%\conquests\unit.h %dst%\conquests
copy %src%\conquests\utils.cpp %dst%\conquests
copy %src%\conquests\utils.h %dst%\conquests

mkdir %dst%\Vision

copy %src%\Vision\Entity.cpp %dst%\Vision
copy %src%\Vision\Entity.h %dst%\Vision
copy %src%\Vision\GraphicsEngine.cpp %dst%\Vision
copy %src%\Vision\GraphicsEngine.h %dst%\Vision
copy %src%\Vision\GraphicsEnvironment.cpp %dst%\Vision
copy %src%\Vision\GraphicsEnvironment.h %dst%\Vision
copy %src%\Vision\InputHandler.cpp %dst%\Vision
copy %src%\Vision\InputHandler.h %dst%\Vision
copy %src%\Vision\Loader.cpp %dst%\Vision
copy %src%\Vision\Loader.h %dst%\Vision
copy %src%\Vision\Misc.cpp %dst%\Vision
copy %src%\Vision\Misc.h %dst%\Vision
copy %src%\Vision\Modeller.cpp %dst%\Vision
copy %src%\Vision\Modeller.h %dst%\Vision
copy %src%\Vision\Object3D.cpp %dst%\Vision
copy %src%\Vision\Object3D.h %dst%\Vision
copy %src%\Vision\Panel.cpp %dst%\Vision
copy %src%\Vision\Panel.h %dst%\Vision
copy %src%\Vision\RenderData.cpp %dst%\Vision
copy %src%\Vision\RenderData.h %dst%\Vision
copy %src%\Vision\Renderer.cpp %dst%\Vision
copy %src%\Vision\Renderer.h %dst%\Vision
copy %src%\Vision\Resource.cpp %dst%\Vision
copy %src%\Vision\Resource.h %dst%\Vision
copy %src%\Vision\SDL_GraphicsEnvironment.cpp %dst%\Vision
copy %src%\Vision\SDL_GraphicsEnvironment.h %dst%\Vision
copy %src%\Vision\Sound.cpp %dst%\Vision
copy %src%\Vision\Sound.h %dst%\Vision
copy %src%\Vision\Terrain.cpp %dst%\Vision
copy %src%\Vision\Terrain.h %dst%\Vision
copy %src%\Vision\Texture.cpp %dst%\Vision
copy %src%\Vision\Texture.h %dst%\Vision
copy %src%\Vision\VisionIFace.cpp %dst%\Vision
copy %src%\Vision\VisionIFace.h %dst%\Vision
copy %src%\Vision\VisionPrefs.h %dst%\Vision
copy %src%\Vision\VisionShared.h %dst%\Vision
copy %src%\Vision\VisionUtils.cpp %dst%\Vision
copy %src%\Vision\VisionUtils.h %dst%\Vision
copy %src%\Vision\World.cpp %dst%\Vision
copy %src%\Vision\World.h %dst%\Vision

mkdir %dst%\Vision\Cg

copy %src%\Vision\Cg\CGShader.cpp %dst%\Vision\Cg
copy %src%\Vision\Cg\CGShader.h %dst%\Vision\Cg

mkdir %dst%\Vision\DirectX

copy %src%\Vision\DirectX\D3D9Renderer.cpp %dst%\Vision\DirectX
copy %src%\Vision\DirectX\D3D9Renderer.h %dst%\Vision\DirectX
copy %src%\Vision\DirectX\SDL_D3D9GraphicsEnvironment.cpp %dst%\Vision\DirectX
copy %src%\Vision\DirectX\SDL_D3D9GraphicsEnvironment.h %dst%\Vision\DirectX
copy %src%\Vision\DirectX\SoftwareRenderer.cpp %dst%\Vision\DirectX
copy %src%\Vision\DirectX\SoftwareRenderer.h %dst%\Vision\DirectX

mkdir %dst%\Vision\OpenGL

copy %src%\Vision\OpenGL\GLRenderer.cpp %dst%\Vision\OpenGL
copy %src%\Vision\OpenGL\GLRenderer.h %dst%\Vision\OpenGL
copy %src%\Vision\OpenGL\SDL_GLGraphicsEnvironment.cpp %dst%\Vision\OpenGL
copy %src%\Vision\OpenGL\SDL_GLGraphicsEnvironment.h %dst%\Vision\OpenGL

mkdir %dst%\Vision\TinyXML
copy %src%\Vision\TinyXML\tinyxml.cpp %dst%\Vision\TinyXML
copy %src%\Vision\TinyXML\tinyxmlerror.cpp %dst%\Vision\TinyXML
copy %src%\Vision\TinyXML\tinyxmlparser.cpp %dst%\Vision\TinyXML
copy %src%\Vision\TinyXML\tinyxml.h %dst%\Vision\TinyXML

mkdir %dst%\Vision\shaders
copy %src%\Vision\shaders\*.cg %dst%\Vision\shaders\

REM Preferences

mkdir %dst%\VisionPreferences
copy %src%\VisionPreferences\main.cpp %dst%\VisionPreferences
copy %src%\VisionPreferences\mainwindow.cpp %dst%\VisionPreferences
copy %src%\VisionPreferences\mainwindow.h %dst%\VisionPreferences
copy %src%\VisionPreferences\VisionPreferences.pro %dst%\VisionPreferences
copy %src%\VisionPreferences\Makefile %dst%\VisionPreferences

REM game data

mkdir %dst%\conquests\conquests

mkdir %dst%\conquests\conquests\shaders
copy %src%\conquests\conquests\shaders\shaders.cg %dst%\conquests\conquests\shaders

mkdir %dst%\conquests\conquests\data

mkdir %dst%\conquests\conquests\data\gfx
copy %src%\conquests\conquests\data\gfx\*.* %dst%\conquests\conquests\data\gfx

mkdir %dst%\conquests\conquests\data\gfx\civs
copy %src%\conquests\conquests\data\gfx\civs\*.* %dst%\conquests\conquests\data\gfx\civs

mkdir %dst%\conquests\conquests\data\sound
copy %src%\conquests\conquests\data\sound\*.* %dst%\conquests\conquests\data\sound

mkdir %dst%\conquests\conquests\data\music
copy %src%\conquests\conquests\data\music\*.* %dst%\conquests\conquests\data\music

mkdir %dst%\conquests\conquests\data\scripts
copy %src%\conquests\conquests\data\scripts\*.lua %dst%\conquests\conquests\data\scripts

mkdir %dst%\conquests\conquests\data\maps
copy %src%\conquests\conquests\data\maps\*.* %dst%\conquests\conquests\data\maps

REM linux stuff

copy %src%\conquests\conquests.desktop %dst%\conquests
copy %src%\conquests\conquests_preferences.desktop %dst%\conquests
REM N.B., debian/ folder should be moved to root
mkdir %dst%\debian
copy %src%\conquests\debian\*.* %dst%\debian
REM Also move to root makefile
copy %src%\conquests\makefile_root %dst%\makefile

