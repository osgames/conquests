#pragma once

#include "common.h"

class MainGamestate;

class Tests {
public:
	static void run(MainGamestate *mainGamestate, const char *filename, TestID index);
};
