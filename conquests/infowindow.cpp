#include "conquests_stdafx.h"

#include "infowindow.h"
#include "game.h"

#include "../Vision/VisionIface.h"

#include <ctime>

//InfoWindow *InfoWindow::activeWindow = NULL;

//InfoWindow::InfoWindow(string text, bool do_modal) : do_modal(do_modal), close(false) {

InfoWindow::InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, VI_Texture *image, const string *button_texts, int n_buttons, int x, int y, int w, int h, bool allow_multiline) : is_modal(false), close(false), close_value(-1) {
	init(genv, parent, text, font, panel_texture, image, button_texts, n_buttons, x, y, w, h, allow_multiline);
}

InfoWindow::InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, const string *button_texts, int n_buttons, int x, int y, int w, int h, bool allow_multiline) : is_modal(false), close(false), close_value(-1) {
	init(genv, parent, text, font, panel_texture, NULL, button_texts, n_buttons, x, y, w, h, allow_multiline);
}

InfoWindow::InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, const string *button_texts, int n_buttons, int x, int y, int w, int h) : is_modal(false), close(false), close_value(-1) {
	init(genv, parent, text, font, panel_texture, NULL, button_texts, n_buttons, x, y, w, h, false);
}

InfoWindow::InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, int x, int y, int w, int h) : is_modal(false), close(false), close_value(-1) {
	string button = "Okay";
	init(genv, parent, text, font, panel_texture, NULL, &button, 1, x, y, w, h, false);
}

InfoWindow::InfoWindow(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture) : is_modal(false), close(false), close_value(-1) {
	string button = "Okay";
	init(genv, parent, text, font, panel_texture, NULL, &button, 1, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, infowindow_def_h_c, false);
}

void InfoWindow::init(VI_GraphicsEnvironment *genv, VI_Panel *parent, string text, VI_Font *font, VI_Texture *panel_texture, VI_Texture *image, const string *button_texts, int n_buttons, int x, int y, int w, int h, bool allow_multiline) {
	VI_log("create InfoWindow %d\n", this);
	/*if( activeWindow == NULL ) {
		activeWindow = this;
	}*/
	this->genv = genv;
	this->panel = NULL;
	this->update = NULL;
	/*this->n_buttons = NULL;
	this->buttons = NULL;*/
	this->hiddenCloseButton = NULL;
	this->textfield = NULL;
	this->image_button = NULL;
	this->close_action = NULL;
	this->userData = NULL;

	this->panel = VI_createPanel();
	this->panel->setPaintFunc(paintPanel, this);
	this->panel->setSize(w, h);
	this->panel->setBackground(1.0f, 1.0f, 1.0f, 0.9f);
	if( panel_texture != NULL )
		this->panel->setTexture(panel_texture);

	if( n_buttons > 0 ) {
		const int start_xpos = 16;
		const int ydiff = 32;
		int n_lines = 1;
		if( allow_multiline )
			n_lines = (n_buttons-1) / 3 + 1;
		int xpos = start_xpos;
		int ypos = h - n_lines * ydiff;
		for(int i=0;i<n_buttons;i++) {
			VI_Button *button = VI_createButton(button_texts[i].c_str(), font);
			buttons.push_back(button);
			button->setUserData(this);
			button->setAction(action);
			this->panel->addChildPanel(button, xpos, ypos);
			if( (i+1) % 3 == 0 && allow_multiline ) {
				xpos = start_xpos;
				ypos += ydiff;
			}
			else {
				xpos += button->getWidth() + 8;
			}
		}
		this->buttons.at(0)->setKeyShortcut(V_RETURN);
		if( n_buttons > 1 ) {
			this->buttons.at(n_buttons-1)->setKeyShortcut(V_ESCAPE);
		}
		else if( n_buttons == 1 ) {
			// we want both return and escape to activate the first button, so we do it via a hidden button shortcut
			this->createHiddenEscapeClose();
		}
	}

	if( text.length() > 0 ) {
		textfield = VI_createTextfield(font, w - 32, h - 64);
		textfield->setHasInputFocus(true);
		textfield->addText(text.c_str());
		textfield->setCPos(0);
		//textfield->setForeground(1.0f, 1.0f, 0.0f);
		textfield->setForeground(1.0f, 1.0f, 1.0f);
		this->panel->addChildPanel(textfield, 16, 16);
		
	}

	if( image != NULL ) {
		const int image_w = image->getWidth();
		const int image_h = image->getHeight();
		image_button = VI_createImageButton(image, image_w, image_h);
		this->panel->addChildPanel(image_button, w - 16 - image_w - 1, h - 48 - image_h - 1);
	}

	parent->addChildPanel(this->panel, x, y);
}

InfoWindow::~InfoWindow() {
	VI_log("delete InfoWindow %d\n", this);
	if( this->close_action != NULL ) {
		(*this->close_action)(this);
	}
	//game->getPanel()->remove(this->panel);
	/*if( this->panel != NULL )
		this->panel->detachFromParentPanel();
	delete this->panel;*/
	delete this->panel;
	/*for(vector<VI_Button *>::iterator iter = buttons.begin(); iter != buttons.end(); ++iter) {
		VI_Button *button = *iter;
		delete button;
	}
	if( this->hiddenCloseButton != NULL ) {
		delete this->hiddenCloseButton;
	}
	delete this->textfield;
	if( this->image_button != NULL )
		delete this->image_button;*/
	/*if( activeWindow == this ) {
		activeWindow = NULL;
	}*/
	/*if( !saved_mainGUILocked ) {
	game->unlockGUI();
	}*/
}

void InfoWindow::createHiddenEscapeClose() {
	hiddenCloseButton = VI_createPanel();
	hiddenCloseButton->setUserData(this);
	hiddenCloseButton->setAction(action);
	hiddenCloseButton->setKeyShortcut(V_ESCAPE);
	this->panel->addChildPanel(hiddenCloseButton, 0, 0);
}

const VI_Button *InfoWindow::getButton(size_t index) const {
	ASSERT( index >= 0 && index < buttons.size() );
	return buttons.at(index);
}

VI_Button *InfoWindow::getButton(size_t index) {
	ASSERT( index >= 0 && index < buttons.size() );
	return buttons.at(index);
}

int InfoWindow::doModal() {
	this->is_modal = true;

	genv->setModalPanel(this->panel);

	if( game_g->isTestmodeSilent() ) {
		return 0;
	}

	int t1 = genv->getRealTimeMS();
	int curr_t = 0;

	close = false;
	close_value = -1;

	//VI_log("ping\n");
	//while( !quit && !close ) {
	while( !close ) {
		//VI_log("loop\n");
		if( genv->shouldQuit() ) {
			// don't allow quit when in modal mode!
			game_g->getGraphicsEnvironment()->clearQuitRequest();
			//game->quitGame();
			/*this->close = true;
			this->close_value = n_buttons-1;*/
		}
		else if( genv->isActive() ) {
			//VI_log("draw\n");
			int t_start = genv->getRealTimeMS();
			genv->drawFrame();
			genv->swapBuffers();
			//frames++;

			int time_diff = genv->getRealTimeMS() - t_start;
			curr_t += time_diff;

			VI_update(curr_t);
			game_g->getGamestate()->update();
			if( update != NULL ) {
				update(this);
			}
			/*if( VI_keypressed(V_ESCAPE) && buttons.size() == 1 ) {
				game_g->getGraphicsEnvironment()->flushInput(); // so the escape doesn't reregister
				this->close = true;
				this->close_value = 0;
			}*/
			/*if( VI_keypressed(V_RETURN) ) {
			genv->flushInput();
			this->action(buttons[0]);
			}*/

			curr_t = 0;
			genv->handleInput(); // must be done last due to resetting input
		}
		else {
			//VI_log("waiting\n");
			genv->waitForEvents();
		}
		genv->processEvents();
	}

	return close_value;
}

void InfoWindow::paintPanel(VI_Graphics2D *g2d, void *data) {
	InfoWindow *window = static_cast<InfoWindow *>(data);
}

void InfoWindow::action(VI_Panel *source) {
	InfoWindow *window = static_cast<InfoWindow *>(source->getUserData());
	//window->genv->flushInput(); // needed to avoid problem where the mouse click is registered after the window closes - is there a better way to do this? / also needed to avoid re-registering of key presses for shortcuts
	window->genv->flushInput(); // needed to avoid re-registering of key presses for shortcuts - is there a better way to do this?
	if( window->is_modal ) {
		if( source == window->hiddenCloseButton ) {
			// this means Escape was pressed
			window->close = true;
			window->close_value = 0;
			return;
		}
		for(size_t i=0;i<window->buttons.size() && window->close_value==-1;i++) {
			if( source == window->buttons.at(i) ) {
				window->close = true;
				window->close_value = i;
				return;
			}
		}
	}
	else {
		delete window;
		return;
	}

	/*if( source == window->okayButton ) {
	if( window->is_modal ) {
	window->close = true;
	window->close_value = 0;
	}
	else {
	delete window;
	return;
	}
	}*/
	T_ASSERT( false );
}

bool InfoWindow::confirm(VI_GraphicsEnvironment *genv, VI_Panel *parent, VI_Font *font, VI_Texture *panel_texture, string text, string yes, string no) {
	string buttons[] = { yes, no };
	InfoWindow *window = new InfoWindow(genv, parent, text, font, panel_texture, buttons, 2, infowindow_def_x_c, infowindow_def_y_c, 300, 300);
	int res = window->doModal();
	//int res = 1;
	delete window;
	return ( res == 0 );
}

int InfoWindow::select(VI_GraphicsEnvironment *genv, VI_Panel *parent, VI_Font *font, VI_Texture *panel_texture, string text, string yes, string no, int window_h, const vector<string> *list) {
	ASSERT( list->size() > 0 );

	string buttons[] = { yes, no };
	int n_buttons = no.length() > 0 ? 2 : 1;
	InfoWindow *window = new InfoWindow(genv, parent, "", font, panel_texture, buttons, n_buttons, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, window_h);
	VI_Panel *windowPanel = window->getPanel();

	VI_Textfield *textfield = VI_createTextfield(font, infowindow_def_w_c - 32, 64);
	textfield->addText(text.c_str());
	//textfield->setForeground(1.0f, 1.0f, 0.0f);
	textfield->setForeground(1.0f, 1.0f, 1.0f);
	windowPanel->addChildPanel(textfield, 16, 16);

	VI_Listbox *listbox = VI_createListbox(infowindow_def_w_c - 32, window_h - 128, &*(list->begin()), list->size(), font);
	listbox->setHasInputFocus(true);
	//listbox->setForeground(1.0f, 1.0f, 0.0f);
	listbox->setForeground(1.0f, 1.0f, 1.0f);
	listbox->setActive(0);
	windowPanel->addChildPanel(listbox, 16, 80);

	int active = -1;
	int res = window->doModal();
	if( res == 0 ) {
		active = listbox->getActive();
		ASSERT( active >= 0 && active < list->size() );
	}

	delete window;

	return active;
}
