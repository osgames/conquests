#include "conquests_stdafx.h"

#include "utils.h"

#include "../Vision/VisionShared.h"

#ifdef _WIN32
#include <io.h> // for access
#define access _access
#endif

#include <cstring> // needed for Linux at least

#include <sstream>
using std::stringstream;

/*int Pos2D::distanceChebyshev(const Pos2D *p) const {
	int dx = abs(x - p->x);
	int dy = abs(y - p->y);
	return max(dx, dy);
}*/

Rational::Rational() : num(1), den(1) {
}

Rational::Rational(int num) : num(num), den(1) {
}

Rational::Rational(int num, int den) : num(num), den(den) {
	ASSERT( den > 0 );
}

void parseFileLine(char *word, const char *line, const char *pref) {
	//VI_log("parseFileLine: %s\n", line);
	//VI_log("    pref: %s\n", pref);
	size_t offset = strlen(pref);
	char *dest = word;
	const char *src = &line[offset];
	while( *src != '\0' && *src != '\r' && *src != '\n' )
		*dest++ = *src++;
	*dest++ = '\0';
	//VI_log("    return: %s\n", word);
}

bool parseBool(bool *ok, const char *word) {
	*ok = true;
	if( strcmp(word, "0") == 0 ) {
		return false;
	}
	else if( strcmp(word, "1") == 0 ) {
		return true;
	}
	*ok = false;
	return false;
}

bool matchFileLine(const char *line, const char *pref) {
	if( strnicmp(line, pref, strlen(pref)) == 0 ) {
		return true;
	}
	else {
		return false;
	}
}

void getDir(int *dx, int *dy, int c) {
	switch( c ) {
		case 0:
			*dx = 0;
			*dy = -1;
			break;
		case 1:
			*dx = 1;
			*dy = -1;
			break;
		case 2:
			*dx = 1;
			*dy = 0;
			break;
		case 3:
			*dx = 1;
			*dy = 1;
			break;
		case 4:
			*dx = 0;
			*dy = 1;
			break;
		case 5:
			*dx = -1;
			*dy = 1;
			break;
		case 6:
			*dx = -1;
			*dy = 0;
			break;
		case 7:
			*dx = -1;
			*dy = -1;
			break;
		default:
			ASSERT(false);
	}
}

string formatNumber(int number) {
	stringstream str, base_str;
	base_str << number;
	int n = 3 - (base_str.str().length() % 3);
	bool first = true;
	for(size_t i=0;i<base_str.str().length();i++) {
		if( !first && n % 3 == 0 ) {
			str << ",";
		}
		first = false;
		n++;
		str << base_str.str().at(i);
	}
	return str.str();
}

string prependAorAn(string text) {
	ASSERT( text.length() > 0 );
	char ch = text.at(0);
	ch = tolower(ch);
	if( ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' )
		return "an " + text;
	return "a " + text;
}

//bool checked_application_folder = false;
string data_folder = "conquests/";

string getFullPath(const char *filename) {
	/*if( !checked_application_folder ) {
		VI_log("looking for application folder\n");
		bool ok = false;
		if( access(application_folder.c_str(), 0) == 0 ) {
			// folder exists
			ok = true;
		}
#ifdef __linux
		if( !ok ) {
			// try alternative location
			application_folder = "/usr/share/" + application_folder;
			if( access(application_folder.c_str(), 0) == 0 ) {
				// folder exists
				ok = true;
			}
		}
#endif
		if( ok ) {
			VI_log("using application folder: %s\n", application_folder.c_str());
		}
		else {
			VI_log("can't find application folder! will probably fail...\n");
		}
		checked_application_folder = true;
	}
	string full_path = application_folder + filename;
	return full_path;*/
	string full_path = VI_getApplicationDataFilename(data_folder.c_str(), filename);
	return full_path;
}
