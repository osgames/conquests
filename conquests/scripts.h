#pragma once

/** Lua interface functions.
*/

extern "C" {
#ifdef _WIN32
	#include <lua.h>
	#include <lualib.h>
	#include <lauxlib.h>
#endif
#ifdef __linux
	#include <lua5.1/lua.h>
	#include <lua5.1/lualib.h>
	#include <lua5.1/lauxlib.h>
#endif
}

void registerScripts(lua_State *ls);
