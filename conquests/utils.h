#pragma once

/** Generic utilities.
*/

#include <cstdio>

#include <string>
using std::string;

#ifndef _WIN32
	#define stricmp strcasecmp
	#define strnicmp strncasecmp
#endif

class Pos2D {
public:
	int x, y;

	Pos2D() : x(0), y(0) {
	}
	Pos2D(int x, int y) : x(x), y(y) {
	}

	const bool operator== (const Pos2D& p) const {
		return (p.x==x && p.y==y);
	}
	const bool operator!= (const Pos2D& p) const {
		return (p.x!=x || p.y!=y);
	}
	const Pos2D& operator+= (const Pos2D& p) {
		x+=p.x;
		y+=p.y;
		return *this;
	}
	const Pos2D& operator-= (const Pos2D& p) {
		x-=p.x;
		y-=p.y;
		return *this;
	}
	const Pos2D operator+ (const Pos2D& p) const {
		return Pos2D(x + p.x, y + p.y);
	}
	const Pos2D operator- (const Pos2D& p) const {
		return Pos2D(x - p.x, y - p.y);
	}
	//int distanceChebyshev(const Pos2D *p) const;
};

/** Used for handling moves along roads which only use 1/3 moves. Probably overkill; note that some parts of the
  * code assume that the denominator is 3.
  */
class Rational {
	int num, den;
public:
	Rational();
	Rational(int num);
	Rational(int num, int den);

	/*float value() const {
		return num / den;
	}*/
	void value(int *integer, int *remainder_num, int *remainder_den) const {
		*integer = num/den;
		*remainder_num = num - (*integer) * den;
		*remainder_den = den;
	}

	const bool operator== (const Rational& r) const {
		return (r.num==num && r.den==den);
	}
	const bool operator!= (const Rational& r) const {
		return !(r.num==num && r.den==den);
	}
	const bool operator< (const int& n) const {
		return (num < n * den);
	}
	const bool operator> (const int& n) const {
		return (num > n * den);
	}
	Rational operator+ () const {
		return Rational(num, den);
	}
	Rational operator- () const {
		return Rational(-num, den);
	}
	const Rational& operator= (const Rational& r) {
		num = r.num;
		den = r.den;
		return *this;
	}
	Rational& operator+= (const Rational& r) {
		if( den == r.den ) {
			num += r.num;
		}
		else {
			int n_num = num * r.den + r.num * den;
			int n_den = den * r.den;
			num = n_num;
			den = n_den;
		}
		return *this;
	}
	Rational& operator-= (const Rational& r) {
		if( den == r.den ) {
			num -= r.num;
		}
		else {
			int n_num = num * r.den - r.num * den;
			int n_den = den * r.den;
			num = n_num;
			den = n_den;
		}
		return *this;
	}
	Rational& operator+= (const int& n) {
		num += n * den;
		return *this;
	}
	Rational& operator-= (const int& n) {
		num -= n * den;
		return *this;
	}
	Rational& operator*= (const int& s) {
		num *= s;
		return *this;
	}
	Rational& operator/= (const int& s) {
		den *= s;
		return *this;
	}
	Rational operator+ (const Rational& r) const {
		if( den == r.den ) {
			return Rational(num + r.num, den);
		}
		else {
			int n_num = num * r.den + r.num * den;
			int n_den = den * r.den;
			return Rational(n_num, n_den);
		}
	}
	Rational operator- (const Rational& r) const {
		if( den == r.den ) {
			return Rational(num - r.num, den);
		}
		else {
			int n_num = num * r.den - r.num * den;
			int n_den = den * r.den;
			return Rational(n_num, n_den);
		}
	}
	Rational operator* (const int& s) const {
		return Rational(num*s, den);
	}
};

void parseFileLine(char *word, const char *line, const char *pref);
bool parseBool(bool *ok, const char *word);
bool matchFileLine(const char *line, const char *pref);

void getDir(int *dx, int *dy, int c);

string formatNumber(int number);
string prependAorAn(string text);

string getFullPath(const char *filename);
