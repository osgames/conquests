#pragma once

/** Gamestate for the main game.
*/

#include "game.h"

class Effect;

#include "../Vision/VisionUtils.h"

class VI_Panel;
class VI_ImageButton;
class VI_GeneralParticlesystem;
class VI_SceneGraphNode;
class VI_MouseClickEvent;
class VI_MouseReleaseEvent;
class VI_Graphics2D;

extern const char maps_ext[];

class MainGamestate : public Gamestate {
public:
	enum Difficulty {
		// N.B., if future types are added, the numeric values of existing types should not be changed, to avoid breaking backwards compatibility with save game files (i.e., it's better to not have numeric order if necessary)
		DIFFICULTY_UNDEFINED = -1,
		DIFFICULTY_EASY      = 0,
		DIFFICULTY_MEDIUM    = 1,
		DIFFICULTY_HARD      = 2,
		DIFFICULTY_INSANE    = 3,
		N_DIFFICULTY         = 4
	};
	static string getDifficultyString(Difficulty difficulty);
	string getDifficultyString() {
		return getDifficultyString( difficulty );
	}
	static int getDifficultyShortcut(Difficulty difficulty);
	enum AIAggression {
		// N.B., if future types are added, the numeric values of existing types should not be changed, to avoid breaking backwards compatibility with save game files (i.e., it's better to not have numeric order if necessary)
		AIAGGRESSION_UNDEFINED  = -1,
		AIAGGRESSION_AGGRESSIVE = 0,
		AIAGGRESSION_NORMAL     = 1,
		AIAGGRESSION_PEACEFUL   = 2,
		N_AIAGGRESSION          = 3,
	};
	static string getAIAggressionString(AIAggression aiaggression);
	static int getAIAggressionShortcut(AIAggression aiaggression);

	enum Phase {
		PHASE_PLAYERTURN = 0,
		PHASE_CPUTURN    = 1,
		PHASE_ENDTURN    = 2
	};

protected:
	static MainGamestate *mainGamestate; // singleton pointer, needed for static member functions
	/*VI_GraphicsEnvironment *genv;
	VI_Sound               *sound;
	VI_Font                *font;*/
	//VI_World               *world;
	//Gamestate              *gamestate;

	VI_Panel               *gamePanel;
	VI_Button              *endturnButton;
	VI_Button              *newgameButton;
	VI_Button              *loadgameButton;
	VI_Button              *savegameButton;
	VI_Button              *savemapButton;
	VI_Button              *quitgameButton;
	VI_Button              *citiesAdvisorButton;
	VI_Button              *unitsAdvisorButton;
	VI_Button              *civsAdvisorButton;
	VI_Button              *techsAdvisorButton;
	VI_Button              *scoresAdvisorButton;
	VI_Button              *mapButton;
	struct MapInfo {
		const MainGamestate *mainGamestate;
		int image_w, image_h;
		MapInfo() : mainGamestate(NULL), image_w(-1), image_h(-1) {
		}
	};
	MapInfo                 mapInfo;
	bool                    map_needs_update;
	VI_ImageButton         *mapImageButton;
	//VI_Button              *saveMapButton;
	VI_Button              *gridButton;
	VI_Button              *zoomOutButton;
	VI_Button              *zoomResetButton;
	VI_Button              *zoomInButton;
	VI_Button              *viewButton;
	VI_Button              *helpButton;
	VI_Button              *waitButton;
	VI_Button              *skipTurnButton;
	VI_Button              *fortifyButton;
	VI_Button              *buildCityButton;
	VI_Button              *buildRoadButton;
	VI_Button              *buildRailwaysButton;
	VI_Button              *automateButton;
	VI_Button              *gotoButton;
	VI_Button              *travelButton;
	VI_Button              *airRaidButton;
	VI_Button              *reconnaissanceButton;
	VI_Button              *move1Button;
	VI_Button              *move2Button;
	VI_Button              *move3Button;
	VI_Button              *move4Button;
	VI_Button              *move6Button;
	VI_Button              *move7Button;
	VI_Button              *move8Button;
	VI_Button              *move9Button;
	VI_Panel               *homeHiddenButton;
	VI_Panel               *centreHiddenButton;
	VI_Panel               *move1HiddenButton;
	VI_Panel               *move2HiddenButton;
	VI_Panel               *move3HiddenButton;
	VI_Panel               *move4HiddenButton;
	VI_Panel               *move6HiddenButton;
	VI_Panel               *move7HiddenButton;
	VI_Panel               *move8HiddenButton;
	VI_Panel               *move9HiddenButton;
	VI_Panel               *moveLHiddenButton;
	VI_Panel               *moveRHiddenButton;
	VI_Panel               *moveUHiddenButton;
	VI_Panel               *moveDHiddenButton;
	VI_Panel               *returnHiddenButton;

	InfoWindow             *infoWindow;
	//Shader *splat_pixel_shader_ambient;

	VI_Texture *cityTexture;
	VI_Texture *cityModernTexture;
	//VI_Texture *panelTexture;
	VI_Texture *nukeTexture;
	VI_Texture *cityPanelTexture;
	VI_Texture *smokeTexture;
	VI_GeneralParticlesystem *smokeParticlesystem_2d;
	VI_SceneGraphNode *smokeEntity_2d;
	VI_GeneralParticlesystem *fireParticlesystem_2d;
	VI_SceneGraphNode *fireEntity_2d;
	VI_GeneralParticlesystem *smokeParticlesystem_3d;
	VI_SceneGraphNode *smokeEntity_3d;
	VI_GeneralParticlesystem *fireParticlesystem_3d;
	VI_SceneGraphNode *fireEntity_3d;
	vector<Effect *> effects;

	bool gamestate_started;
	int seed; // recorded so we can log it - saved
	int year; // saved
	bool help_buildcity; // saved
	bool help_buildroad; // saved
	bool help_bomb; // saved
	Map *map; // saved
	Civilization *player; // saved
	vector<Civilization *> civilizations; // saved
	vector<Relationship *> relationships; // saved
	Difficulty difficulty; // saved
	AIAggression aiaggression; // saved
	int game_n_turns; // saved
	Civilization *rebel_civ; // not saved

	enum InputMode {
		INPUTMODE_NORMAL = 0,
		INPUTMODE_TRAVEL = 1,
		INPUTMODE_BOMBARD = 2,
		INPUTMODE_RECONNAISSANCE = 3,
		INPUTMODE_GOTO = 4
	};
	InputMode inputMode;
	bool mouse_dragging;
	bool was_dragged;
	int mouse_dragging_x;
	int mouse_dragging_y;
	float mouse_dragging_map_pos_x;
	float mouse_dragging_map_pos_y;
	Vector3D mouse_dragging_map_pos_3d;

	Phase phase;
	//bool mainGUILocked;

	// timing info
	int time_endplayerturn_s_ms;
	int time_store_ms;
	void logEndturnTime(const char *text);

	bool test_mode;

	int blink_time_start_ms;
	Unit *cache_active_unit;
	Unit *active_unit;
	bool moved_units_this_turn;
	//Pos2D map_pos;

	bool view_3d;
	// 3d view:
	bool init_view_dirs;
	Vector3D view_right, view_up;
	// 2d view:
	float map_pos_x, map_pos_y;
	int def_sq_width, def_sq_height;
	float scale_width, scale_height;

	bool grid; // not saved

	/*void addTechnology(Technology *technology);
	void addBuildable(Buildable *buildable);*/
	/*void addTechnology(Technology *technology) {
		this->getGameData()->addTechnology(technology);
	}
	void addBuildable(Buildable *buildable) {
		this->getGameData()->addBuildable(buildable);
	}*/

	Pos2D getScreenPos(float x, float y) const;
	void getMapPosf(float *mx, float *my, int x, int y) const;
	Pos2D getMapPos(int x, int y) const;
	void switchView();
	static void helpAction(VI_Panel *source);
	void help();
	static void renderQuad2D(int pos_x, int pos_z, const unsigned char *rgba);
	static void renderQuad2D(float pos_x, float pos_z, float w, float h, const unsigned char *rgba);
	static void renderQuad(Pos2D pos, bool flat, const unsigned char *rgba = NULL);
	static void renderQuad(float pos_x, float pos_z, float w, float h, bool flat, const unsigned char *rgba = NULL);
	static void renderFunc();
	void handleMouseClickEvent(const VI_MouseClickEvent *mouse_click_event);
	static void mouseClickEventFunc(VI_MouseClickEvent *mouse_click_event, void *data);
	void handleMouseReleaseEvent(const VI_MouseReleaseEvent *mouse_release_event);
	static void mouseReleaseEventFunc(VI_MouseReleaseEvent *mouse_release_event, void *data);
	static void paintPanel(VI_Graphics2D *g2d, void *data);
	void drawGUIText(VI_Graphics2D *g2d) const;
	static void action(VI_Panel *source);
	//static void actionInfoWindow(VI_Panel *source);
	static void actionInfoWindow(InfoWindow *infoWindow);
	static void askTechnologyAction(VI_Panel *source);
	//void generateRelationships();
	void reset3DZoom(float preferred_width);
	void zoom(bool zoom_in);
	//bool initWorld();
	bool init();
	void clear();
	void checkMapView();

	void save(const char *filename, bool autosave);
	bool load(const char *filename);
	struct FilenameInfo {
		string fullfilename; // filename to actually load, including path
		string filename; // filename to actually load (not including path)
		string filedescription; // text that is displayed
		string date; // date stored as string (for sorting by date)
	};
	class LoadDialogSortData {
	public:
		vector<FilenameInfo> *gamefiles;
		VI_Listbox *listbox;
		VI_Button *sort_button;
		bool sort_by_name;
		LoadDialogSortData(vector<FilenameInfo> *gamefiles, VI_Listbox *listbox, VI_Button *sort_button, bool sort_by_name) : gamefiles(gamefiles), listbox(listbox), sort_button(sort_button), sort_by_name(sort_by_name) {
		}
	};
	static bool sortLoadDialogFilesByName(const FilenameInfo& f1, const FilenameInfo& f2);
	static bool sortLoadDialogFilesByDate(const FilenameInfo& f1, const FilenameInfo& f2);
	static void loadDialogSort(LoadDialogSortData *data);
	static void loadDialogSortAction(VI_Panel *source);
	//static vector<FilenameInfo> getFiles(const char *dirname, const char *ext);
	static void getFiles(vector<FilenameInfo> *files, const char *dirname, const char *ext);
	void newGame();
	void saveGame();
	void saveMap();

	void clearGameData();
	void endPlayerTurn();
	void endTurn();
	void updateButtons() const;
	void updateUnitButtons() const;
	void home();
	void setInputMode(InputMode inputMode);
	bool askDeclareWar(const Civilization *civilization, bool enter_territory);
	int getMinTurnsWar(const Civilization *civ1, const Civilization *civ2) const;
	bool movePlayerActiveUnit(const bool *request_move);

	static void cityListClickAction(VI_Panel *source);
	void citiesAdvisor();
	static void unitListClickAction(VI_Panel *source);
	void unitsAdvisorGetUnits(vector<Unit *> *units, vector<string> *unit_names, vector< vector<Unit *> > list_of_lists);
	void unitsAdvisor();
	void civsAdvisor();
	void techsAdvisor();
	void scoresAdvisor();
	static void graphPaintPanel(VI_Graphics2D *g2d, void *data);
	void drawGraph(HistoryType history_type, const Civilization *civilization, VI_Image *image, const Civilization **civs, int *years, int *values, int y_offset, int orig_image_width, int orig_image_height, int max_score) const;
	void showHistory(HistoryType history_type);
	static void mapPaintPanel(VI_Graphics2D *g2d, void *data);
	static void mapMouseClickEventFunc(VI_MouseClickEvent *mouse_click_event, void *data);
	void showMap();
	void createInfoWindow(string text);
	void scrollView();

public:
	MainGamestate() {
		mainGamestate = this;
		gamestate_started = false;
		/*genv = NULL;
		sound = NULL;
		font = NULL;
		world = NULL;*/
		//gamestate = NULL;

		gamePanel = NULL;
		endturnButton = NULL;
		newgameButton = NULL;
		loadgameButton = NULL;
		savegameButton = NULL;
		savemapButton = NULL;
		quitgameButton = NULL;
		citiesAdvisorButton = NULL;
		unitsAdvisorButton = NULL;
		civsAdvisorButton = NULL;
		techsAdvisorButton = NULL;
		scoresAdvisorButton = NULL;
		map_needs_update = false;
		mapButton = NULL;
		mapImageButton = NULL;
		gridButton = NULL;
		zoomOutButton = NULL;
		zoomResetButton = NULL;
		zoomInButton = NULL;
		viewButton = NULL;
		helpButton = NULL;
		waitButton = NULL;
		skipTurnButton = NULL;
		fortifyButton = NULL;
		buildCityButton = NULL;
		buildRoadButton = NULL;
		buildRailwaysButton = NULL;
		automateButton = NULL;
		gotoButton = NULL;
		travelButton = NULL;
		airRaidButton = NULL;
		reconnaissanceButton = NULL;
		homeHiddenButton = NULL;
		centreHiddenButton = NULL;
		move1Button = NULL;
		move2Button = NULL;
		move3Button = NULL;
		move4Button = NULL;
		move6Button = NULL;
		move7Button = NULL;
		move8Button = NULL;
		move9Button = NULL;
		move1HiddenButton = NULL;
		move2HiddenButton = NULL;
		move3HiddenButton = NULL;
		move4HiddenButton = NULL;
		move6HiddenButton = NULL;
		move7HiddenButton = NULL;
		move8HiddenButton = NULL;
		move9HiddenButton = NULL;
		moveLHiddenButton = NULL;
		moveRHiddenButton = NULL;
		moveUHiddenButton = NULL;
		moveDHiddenButton = NULL;
		returnHiddenButton = NULL;

		infoWindow = NULL;

		cityTexture = NULL;
		cityModernTexture = NULL;
		//panelTexture = NULL;
		cityPanelTexture = NULL;
		nukeTexture = NULL;
		smokeTexture = NULL;
		smokeParticlesystem_2d = NULL;
		smokeEntity_2d = NULL;
		fireParticlesystem_2d = NULL;
		fireEntity_2d = NULL;
		smokeParticlesystem_3d = NULL;
		smokeEntity_3d = NULL;
		fireParticlesystem_3d = NULL;
		fireEntity_3d = NULL;
		view_3d = true;
		init_view_dirs = false;
		map_pos_x = 0.0f;
		map_pos_y = 0.0f;
		def_sq_width = -1;
		def_sq_height = -1;
		scale_width = 1.0f;
		scale_height = 1.0f;
		grid = false;
		phase = PHASE_PLAYERTURN;
		inputMode = INPUTMODE_NORMAL;
		mouse_dragging = false;
		was_dragged = false;
		mouse_dragging_x = 0;
		mouse_dragging_y = 0;
		mouse_dragging_map_pos_x = 0;
		mouse_dragging_map_pos_y = 0;
		//mainGUILocked = false;
		blink_time_start_ms = 0;
		cache_active_unit = NULL;
		active_unit = NULL;
		moved_units_this_turn = false;
		test_mode = false;
		time_endplayerturn_s_ms = 0;
		time_store_ms = 0;

		seed = 0;
		year = 0;
		help_buildcity = false;
		help_buildroad = false;
		help_bomb = false;
		map = NULL;
		player = NULL;
		difficulty = DIFFICULTY_EASY;
		aiaggression = AIAGGRESSION_NORMAL;
		game_n_turns = 0;
		rebel_civ = NULL;
	}
	virtual ~MainGamestate() {
		mainGamestate = NULL;
	}

	enum LoadDialogStatus {
		LOADDIALOGSTATUS_OK = 0,
		LOADDIALOGSTATUS_CANCEL = 1,
		LOADDIALOGSTATUS_NOFILES = 2
	};
	static string loadDialog(LoadDialogStatus *status, const char *load_text, const char *dirname, const char *ext, bool want_sort_button, bool default_sort_by_name);
	static string loadDialog(LoadDialogStatus *status, const char *load_text, const vector<string> *dirnames, const char *ext, bool want_sort_button, bool default_sort_by_name);
	bool initGameData(bool std_game, bool new_map, int map_width, int map_height, Topology topology, const char *map_filename, const Race *player_race, int n_opponent_civs);
	bool loadGame();
	void beginTurn();
	void createRebelCiv();
	void runTests(TestID id);
	void runTest(const char *filename, TestID index); // needs to be public just so we can make it a friend of Unit?!
	void setMap(Map *map) {
		this->map = map;
	}
	bool checkRelationships();
	void generateTerrain();

	/*VI_GraphicsEnvironment *getGraphicsEnvironment() const {
		return genv;
	}*/
	virtual VI_Panel *getPanel() {
		return gamePanel;
	}
	/*VI_Font *getFont() const {
		return font;
	}*/
	/*VI_Texture *getPanelTexture() const {
		return panelTexture;
	}*/
	VI_Texture *getCityPanelTexture() const {
		// n.b., return non-const needed due to draw functions being non-const
		return cityPanelTexture;
	}
	enum SoundID {
		SOUND_FOOT = 0,
		SOUND_WORKER = 1,
		SOUND_SWORDS = 2,
		SOUND_WOODBRK = 3,
		SOUND_GUNSHOT = 4,
		SOUND_NUKE_EXPLOSION = 5,
		SOUND_BLIP = 6,
		SOUND_COMPLETE = 7
	};
	void playSound(SoundID soundID) const;
	/*void lockGUI();
	void unlockGUI();*/
	bool isGUILocked() const;
	GameData *getGameData();
	const GameData *getGameData() const;
	Phase getPhase() const {
		return phase;
	}
	Map *getMap() {
		return map;
	}
	const Map *getMap() const {
		return map;
	}
	int getYear() const {
		return year;
		//return getGameData()->getYear();
	}
	void setYear(int year) {
		this->year = year;
	}
	Civilization *getRebelCiv() const {
		return rebel_civ;
	}
	void calculateTerritory();
	void refreshMapDisplay();
	void setMapNeedsUpdate() {
		this->map_needs_update = true;
	}
	void addCivilization(Civilization *civilization) {
		this->civilizations.push_back(civilization);
		//getGameData()->addCivilization(civilization);
	}
	//void removeCivilization(Civilization *civilization);
	Civilization *getPlayer() {
		return player;
	}
	const Civilization *getPlayer() const {
		return player;
	}
	Civilization *getCivilization(size_t i) {
		return civilizations.at(i);
	}
	const Civilization *getCivilization(size_t i) const {
		return civilizations.at(i);
	}
	size_t getNCivilizations() const {
		return civilizations.size();
		//return getGameData()->getNCivilizations();
	}
	const Relationship *findRelationship(const Civilization *civ1, const Civilization *civ2) const;
	Relationship *findRelationship(const Civilization *civ1, const Civilization *civ2);
	void addRelationship(Relationship *relationship) {
		relationships.push_back(relationship);
		//getGameData()->addRelationship(relationship);
	}
	bool relationshipsDefined() const {
		return relationships.size() > 0;
	}

	/*void addRace(const Race *race) {
		//this->races.push_back(race);
		getGameData()->addRace(race);
	}*/
	const Technology *getTechnology(size_t i) const {
		//return technologies.at(i);
		return getGameData()->getTechnology(i);
	}
	size_t getNTechnologies() const {
		//return technologies.size();
		return getGameData()->getNTechnologies();
	}
	const Buildable *getBuildable(size_t i) const {
		//return buildables.at(i);
		return getGameData()->getBuildable(i);
	}
	size_t getNBuildables() const {
		//return buildables.size();
		return getGameData()->getNBuildables();
	}
	const Improvement *getImprovement(size_t i) const {
		//return improvements.at(i);
		return getGameData()->getImprovement(i);
	}
	size_t getNImprovements() const {
		//return improvements.size();
		return getGameData()->getNImprovements();
	}
	const UnitTemplate *getUnitTemplate(size_t i) const {
		//return unit_templates.at(i);
		return getGameData()->getUnitTemplate(i);
	}
	size_t getNUnitTemplates() const {
		//return unit_templates.size();
		return getGameData()->getNUnitTemplates();
	}
	/*const Technology *findTechnology(const char *name) const {
		return getGameData()->findTechnology(name);
	}*/
	/*Buildable *findBuildable(const char *name) {
		return getGameData()->findBuildable(name);
	}*/
	/*const Improvement *findImprovement(const char *name) const {
		return getGameData()->findImprovement(name);
	}*/
	/*const Improvement *findImprovement(ImprovementType improvementType) const {
		return getGameData()->findImprovement(improvementType);
	}*/
	/*const UnitTemplate *findUnitTemplate(const char *name) const {
		return getGameData()->findUnitTemplate(name);
	}*/
	/*const Race *findRace(const char *name) const {
		return getGameData()->findRace(name);
	}*/

	void moveTo(Unit *unit, Pos2D newPos, bool travelling);
	bool canBomb(const UnitTemplate *unit_template, Pos2D source, Pos2D target) const;
	void bomb(Unit *unit, Pos2D target);
	void activateUnit(Unit *unit, bool centre);
	const Unit *getActiveUnit() const {
		return active_unit;
	}
	void setActiveUnit(Unit *unit) {
		this->active_unit = unit;
	}
	void wakeupUnit(Unit *unit);
	void askTechnology();
	void askBuildable(City *city);
	void fillBuildablesList(vector<const Buildable *> *buildables, vector<string> *buildable_names, const City *city);
	void centre(float mx, float my);
	void centre(Pos2D pos);
	void centreUnit();
	bool isTestMode() const {
		return test_mode;
	}
	void setDifficulty(Difficulty difficulty) {
		this->difficulty = difficulty;
	}
	Difficulty getDifficulty() const {
		return this->difficulty;
	}
	void setAIAggression(AIAggression aiaggression) {
		this->aiaggression = aiaggression;
	}
	void setGameNTurns(int game_n_turns) {
		this->game_n_turns = game_n_turns;
	}
	void addFireEffect(Pos2D pos);
	void updateTerrainForFOW();
	int getSqWidth() const;
	int getSqHeight() const;
	//bool runScript(const char *script);
	
	//virtual void run();
	virtual bool start();
	virtual void stop();
	virtual void quitGame();
	virtual void update();

	static string getUnitStatsString(const UnitTemplate *unit_template, bool include_moves);
	static string getUnitName(const Unit *unit, bool want_civname);
	static vector<string> getUnitNames(const vector<Unit *> *units, bool want_civname);
};
