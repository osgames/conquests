#pragma once

/** Gamestate for the options screen.
*/

#include "game.h"

class VI_Texture;
class VI_Panel;
class VI_Button;
class VI_ImageButton;

class OptionsGamestate : public Gamestate {
	static OptionsGamestate *optionsGamestate; // singleton pointer, needed for static member functions
	VI_Panel  *gamePanel;
	VI_Button *startgameButton;
	VI_Button *loadgameButton;
	VI_Button *quitgameButton;

	VI_Texture *imageTexture;
	VI_ImageButton *imageButton;
	VI_Button *titleButton;

	static void action(VI_Panel *source);
public:
	OptionsGamestate() {
		optionsGamestate = this;
		gamePanel = NULL;
		startgameButton = NULL;
		loadgameButton = NULL;
		quitgameButton = NULL;
		imageTexture = NULL;
		imageButton = NULL;
		titleButton = NULL;
	}
	virtual ~OptionsGamestate() {
		optionsGamestate = NULL;
	}

	virtual VI_Panel *getPanel() {
		return gamePanel;
	}

	virtual bool start();
	virtual void stop();
	virtual void quitGame();
	virtual void update();
};
