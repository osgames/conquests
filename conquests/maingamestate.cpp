#include "conquests_stdafx.h"

#include "maingamestate.h"
#include "optionsgamestate.h"
#include "city.h"
#include "citywindow.h"
#include "buildable.h"
#include "unit.h"
#include "civilization.h"
#include "map.h"
#include "technology.h"
#include "infowindow.h"
#include "tests.h"

#include "../Vision/Renderer.h"

#if _WIN32
#include <io.h>
#include <direct.h>
#define access _access
#endif

#if __linux
#include <sys/stat.h> // for mkdir
#include <dirent.h>
#include <errno.h>
#endif

#include <ctime>
#include <iomanip>
#include <cstring> // needed for Linux at least

#include <sstream>
using std::stringstream;

MainGamestate *MainGamestate::mainGamestate = NULL;

const float xscale = 1.0f;
const float zscale = 1.0f;

const float def_view3d_scale = 1.5f; // scaling of default zoom level, compared with 2d (it's useful to set the 3d zoom level to being more zoomed out, due to the angle of the viewpoint)

//const int fog_alpha_c = 63; // (int rather than unsigned char, to avoid risk of overflow during calculations)
const int fog_alpha_c = 127; // (int rather than unsigned char, to avoid risk of overflow during calculations)
//const int fog_alpha_c = 160; // (int rather than unsigned char, to avoid risk of overflow during calculations)

const unsigned char rgba_fog[] = {fog_alpha_c, fog_alpha_c, fog_alpha_c, 255};
const unsigned char rgba_white[] = {255, 255, 255, 255};

//const bool CHEAT = true;
//const bool SPY = true;
//const bool RANDOMSEED = false;

const bool CHEAT = false;
const bool SPY = false;
const bool RANDOMSEED = true;

//const bool SAVE_FIRST_TURN = false;
const bool SAVE_FIRST_TURN = true;

const char savegames_ext[] = ".sav";
const char maps_ext[] = ".tmx";
//const char filter[] = "Save Game Files\0*.sav\0";

const int blink_speed_ms_c = (int)(1000  * 0.5);
const int fire_effect_duration_ms_c = (int)(1000 * 2.0);

class Effect {
public:
	VI_SceneGraphNode *node_2d;
	VI_SceneGraphNode *node_3d;
	int xpos, ypos;
	int time_e_ms;

	Effect(VI_SceneGraphNode *node_2d, VI_SceneGraphNode *node_3d, int xpos, int ypos, int duration_ms);
};

Effect::Effect(VI_SceneGraphNode *node_2d, VI_SceneGraphNode *node_3d, int xpos, int ypos, int duration_ms) : node_2d(node_2d), node_3d(node_3d), xpos(xpos), ypos(ypos) {
	time_e_ms = VI_getGameTimeMS() + duration_ms;
}

string MainGamestate::getDifficultyString(Difficulty difficulty) {
	if( difficulty == DIFFICULTY_EASY )
		return "Easy";
	else if( difficulty == DIFFICULTY_MEDIUM )
		return "Medium";
	else if( difficulty == DIFFICULTY_HARD )
		return "Hard";
	else if( difficulty == DIFFICULTY_INSANE )
		return "Insane";
	ASSERT(false);
	return NULL;
}

int MainGamestate::getDifficultyShortcut(Difficulty difficulty) {
	if( difficulty == DIFFICULTY_EASY )
		return V_E;
	else if( difficulty == DIFFICULTY_MEDIUM )
		return V_M;
	else if( difficulty == DIFFICULTY_HARD )
		return V_H;
	else if( difficulty == DIFFICULTY_INSANE )
		return V_I;
	ASSERT(false);
	return NULL;
}

string MainGamestate::getAIAggressionString(AIAggression aiaggression) {
	if( aiaggression == AIAGGRESSION_AGGRESSIVE )
		return "Aggressive Warmongers";
	else if( aiaggression == AIAGGRESSION_NORMAL )
		return "Normal";
	else if( aiaggression == AIAGGRESSION_PEACEFUL )
		return "Peaceful";
	ASSERT(false);
	return NULL;
}

int MainGamestate::getAIAggressionShortcut(AIAggression aiaggression) {
	if( aiaggression == AIAGGRESSION_AGGRESSIVE )
		return V_A;
	else if( aiaggression == AIAGGRESSION_NORMAL )
		return V_N;
	else if( aiaggression == AIAGGRESSION_PEACEFUL )
		return V_P;
	ASSERT(false);
	return NULL;
}

void MainGamestate::fillBuildablesList(vector<const Buildable *> *buildables, vector<string> *buildable_names, const City *city) {
	for(size_t i=0;i<this->getNBuildables();i++) {
		const Buildable *buildable = this->getBuildable(i);
		if( city->canBuild(buildable) ) {
			buildables->push_back(buildable);
		}
	}
	// convert to names
	for(vector<const Buildable *>::const_iterator iter = buildables->begin();iter != buildables->end(); ++iter) {
		const Buildable *buildable = *iter;
		//const char *name = buildable->getName();
		stringstream name;
		name << buildable->getName();
		if( buildable->getType() == Buildable::TYPE_UNIT ) {
			const UnitTemplate *unit_template = static_cast<const UnitTemplate *>(buildable);
			/*name << " [";
			name << unit_template->getAttack();
			name << "/";
			name << unit_template->getDefence();
			name << "/";
			name << unit_template->getMoves();
			name << "]";*/
			name << " " << MainGamestate::getUnitStatsString(unit_template, true);
		}
		name << " (";
		name << buildable->getCost();
		int n_production = city->calculateProduction();
		if( n_production > 0 ) {
			int n_turns = (int)ceil(((float)buildable->getCost()) / (float)n_production);
			name << ": ";
			name << n_turns;
			name << " turn";
			if( n_turns != 1 ) {
				name << "s";
			}
			name << ")";
		}
		else {
			name << ")";
		}
		buildable_names->push_back(name.str());
	}
}

//void MainGamestate::actionInfoWindow(VI_Panel *source) {
void MainGamestate::actionInfoWindow(InfoWindow *infoWindow) {
	//mainGamestate->playSound(SOUND_BLIP);
	T_ASSERT( mainGamestate->infoWindow != NULL );
	//delete mainGamestate->infoWindow;
	mainGamestate->infoWindow = NULL;
	//game_g->getGraphicsEnvironment()->flushInput(); // needed to avoid problem where the mouse click is registered after the window closes - is there a better way to do this?
	game_g->getGraphicsEnvironment()->flushInput(); // needed to avoid re-registering of key presses for shortcuts - is there a better way to do this?
}

void MainGamestate::createInfoWindow(string text) {
	/*if( InfoWindow::getActiveWindow() != NULL ) {
		// delete existing one
		delete InfoWindow::getActiveWindow();
	}*/
	if( infoWindow != NULL ) {
		// delete existing one
		delete infoWindow;
	}
	string okay = "Close Help Window";
	const int wid = 200;
	infoWindow = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text, game_g->getFont(), game_g->getPanelTexture(), &okay, 1, game_g->getGraphicsEnvironment()->getWidth() - wid - 16, 16, wid, 320);
	//this->infoWindow->getButton(0)->setAction(actionInfoWindow);
	this->infoWindow->setCloseAction(actionInfoWindow);
}

string MainGamestate::getUnitStatsString(const UnitTemplate *unit_template, bool include_moves) {
	stringstream text;
	if( unit_template->isAir() ) {
		text << "[" << unit_template->getAirAttack() << "/" << unit_template->getAirDefence() << "/" << unit_template->getAirRange() << "/" << unit_template->getBombard() << "%";
		if( include_moves ) {
			text << "/" << unit_template->getMoves();
		}
		text << "]";
	}
	else if( unit_template->isSea() ) {
		text << "[" << unit_template->getSeaAttack() << "/" << unit_template->getSeaDefenceRange();
		if( include_moves ) {
			text << "/" << unit_template->getMoves();
		}
		text << "]";
	}
	else {
		text << "[" << unit_template->getAttack() << "/" << unit_template->getDefence();
		if( include_moves ) {
			text << "/" << unit_template->getMoves();
		}
		text << "]";
	}
	return text.str();
}

string MainGamestate::getUnitName(const Unit *unit, bool want_civname) {
	const UnitTemplate *unit_template = unit->getTemplate();
	stringstream name;
	if( want_civname ) {
		name << unit->getCivilization()->getNameAdjective();
		name << " ";
	}
	if( unit->isVeteran() ) {
		name << "Veteran ";
	}
	name << unit_template->getName();
	name << " (Moves: ";
	Rational moves_left = unit->movesLeft();
	int moves_left_integer, moves_left_num, moves_left_den;
	moves_left.value(&moves_left_integer, &moves_left_num, &moves_left_den);
	ASSERT( moves_left_num == 0 || moves_left_den == road_move_scale_c );
	if( moves_left_num == 0 ) {
		name << moves_left_integer;
	}
	else if( moves_left_integer == 0 ) {
		name << moves_left_num << "/" << moves_left_den;
	}
	else {
		name << moves_left_integer << " " << moves_left_num << "/" << moves_left_den;
	}
	name << " out of " << unit_template->getMoves() << ") ";
	name << getUnitStatsString(unit_template, false);
	if( unit->getStatus() == Unit::STATUS_NORMAL ) {
		// skip
	}
	else if( unit->getStatus() == Unit::STATUS_FORTIFIED ) {
		name << " [FORTIFIED]";
	}
	else if( unit->getStatus() == Unit::STATUS_BUILDING_ROAD ) {
		name << " [BUILDING ROAD]";
	}
	else if( unit->getStatus() == Unit::STATUS_BUILDING_RAILWAYS ) {
		name << " [BUILDING RAILWAYS]";
	}
	else {
		T_ASSERT(false);
	}
	if( unit->isAutomated() ) {
		Pos2D goto_target;
		if( unit->hasGotoTarget(&goto_target) ) {
			name << " [GOTO " << goto_target.x << "," << goto_target.y << "]";
		}
		else {
			name << " [AUTOMATED]";
		}
	}
	return name.str();
}

vector<string> MainGamestate::getUnitNames(const vector<Unit *> *units, bool want_civname) {
	vector<string> unit_names;
	for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end(); ++iter) {
		const Unit *unit = *iter;
		string name = getUnitName(unit, want_civname);
		unit_names.push_back(name.c_str());
	}
	return unit_names;
}

int MainGamestate::getSqWidth() const {
	return (int)(this->scale_width * this->def_sq_width);
}

int MainGamestate::getSqHeight() const {
	return (int)(this->scale_height * this->def_sq_height);
}

// given (fractional) map position x, y, return the screen coords
Pos2D MainGamestate::getScreenPos(float x, float y) const {
	/*if( view_3d ) {
		//x = y = 0.0f;
		Vector3D world_pos(x, 0.0f, y);
		Pos2D eye_pos;
		game_g->getGraphicsEnvironment()->worldToScreen(&eye_pos.x, &eye_pos.y, &world_pos);
		return Pos2D(eye_pos.x, eye_pos.y);
	}*/
	ASSERT( !view_3d );
	// must be floats, not ints, for smooth movement of units to work!
	//this->map->reduceToBase(&x, &y);
	int sq_width = this->getSqWidth();
	int sq_height = this->getSqHeight();
	//Pos2D pos(game->sq_width * (x - map_pos_x), game->sq_height * ( y - map_pos_y ));
	int off_x = (int)(map_pos_x * sq_width);
	int off_y = (int)(map_pos_y * sq_height);
	// take into account wrap around
	Pos2D pos((int)(sq_width * x - off_x), (int)(sq_height * y - off_y));
	this->map->reduceScreenPosToBase(&pos.x, &pos.y);
	return pos;
}

// given screen coords x, y, return the (fractional) map position
void MainGamestate::getMapPosf(float *mx, float *my, int x, int y) const {
	if( view_3d ) {
		VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
		Vector3D view_dir = game_g->getGraphicsEnvironment()->getDirectionOfPoint(vp, x, y);
		Vector3D pos = vp->getPosition();
		double lambda = - pos.y / view_dir.y;
		T_ASSERT( lambda >= 0.0 );
		pos.x += (float)(lambda * view_dir.x);
		pos.z += (float)(lambda * view_dir.z);
		pos.x /= xscale;
		pos.z /= zscale;
		*mx = pos.x;
		*my = pos.z;
	}
	else {
		int sq_width = this->getSqWidth();
		int sq_height = this->getSqHeight();
		int off_x = (int)(map_pos_x * sq_width);
		int off_y = (int)(map_pos_y * sq_height);
		*mx = ((float)(x+off_x))/(float)sq_width;
		*my = ((float)(y+off_y))/(float)sq_height;
		//this->map->reduceToBase(mx, my);
	}
}

Pos2D MainGamestate::getMapPos(int x, int y) const {
	/*int sq_width = this->getSqWidth();
	int sq_height = this->getSqHeight();
	int off_x = (int)(map_pos_x * sq_width);
	int off_y = (int)(map_pos_y * sq_height);
	Pos2D pos(((float)(x+off_x))/(float)sq_width, ((float)(y+off_y))/(float)sq_height);
	return pos;*/
	float mx = 0.0f, my = 0.0f;
	getMapPosf(&mx, &my, x, y);
	Pos2D pos((int)mx, (int)my);
	return pos;
}

void MainGamestate::switchView() {
	if( view_3d ) {
		float mx0 = 0.0f, mx1 = 0.0f, my = 0.0f;
		getMapPosf(&mx0, &my, 0, 0);
		getMapPosf(&mx1, &my, game_g->getGraphicsEnvironment()->getWidth()-1, 0);

		view_3d = false;

		VI_Terrain *terrain = game_g->getGraphicsEnvironment()->getWorld()->getTerrain();
		ASSERT( terrain != NULL );
		delete terrain;
		game_g->getGraphicsEnvironment()->getWorld()->setTerrain(NULL);

		// sort out position
		this->map_pos_x = mx0;
		this->map_pos_y = my;

		// sort out zoom
		float width = mx1 - mx0;
		// g_width / ( def_sq_width * scale_width ) == width
		this->scale_width = game_g->getGraphicsEnvironment()->getWidth() / ( width * this->def_sq_width );
		this->scale_height = this->scale_width;
	}
	else {
		float o_mx = 0.0f, o_my = 0.0f;
		getMapPosf(&o_mx, &o_my, 0, 0);
		//getMapPosf(&o_mx, &o_my, game_g->getGraphicsEnvironment()->getWidth()/2, game_g->getGraphicsEnvironment()->getHeight()/2);

		view_3d = true;

		this->generateTerrain();

		// sort out zoom
		float prev_width = ((float)game_g->getGraphicsEnvironment()->getWidth()) / (float)this->getSqWidth();
		reset3DZoom(prev_width);

		// sort out position
		float n_mx = 0.0f, n_my = 0.0f;
		getMapPosf(&n_mx, &n_my, 0, 0);
		VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
		Vector3D vp_pos = vp->getPosition();
		vp_pos.x += o_mx - n_mx;
		vp_pos.z += o_my - n_my;
		vp->setPosition(vp_pos);

	}
	this->checkMapView();
}

struct HelpInfo {
	VI_Textfield *textfieldInfo;
	const vector<string> *texts;
	HelpInfo() : textfieldInfo(NULL), texts(NULL) {
	}
};

void MainGamestate::helpAction(VI_Panel *source) {
	HelpInfo *helpInfo = static_cast<HelpInfo *>(source->getUserData());
	ASSERT( helpInfo != NULL );
	VI_Textfield *textfieldInfo = helpInfo->textfieldInfo;
	ASSERT( textfieldInfo != NULL );
	const vector<string> *texts = helpInfo->texts;
	ASSERT( texts != NULL );
	VI_Listbox *listbox = dynamic_cast<VI_Listbox *>(source);
	int active = listbox->getActive();
	if( active != -1 ) {
		ASSERT( active >= 0 && active < texts->size() );
		string text = texts->at(active);
		textfieldInfo->clear();
		textfieldInfo->addText( text.c_str() );
	}
}

void MainGamestate::help() {
	const int n_buttons_c = 4;
	string buttons[n_buttons_c] = {"Technology", "Units", "Improvements", "Cancel"};
	string text = "Please select a topic for help.";
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text, game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, infowindow_def_h_c);
	int res = window->doModal();
	delete window;

	if( res != n_buttons_c-1 ) {
		vector<string> items;
		vector<string> texts;

		if( res == 0 ) {
			for(size_t i=0;i<this->getNTechnologies();i++) {
				const Technology *technology = this->getTechnology(i);
				items.push_back(technology->getName());
				texts.push_back(technology->getHelp());
			}
		}
		else if( res == 1 ) {
			for(size_t i=0;i<this->getNUnitTemplates();i++) {
				const UnitTemplate *unit_template = this->getUnitTemplate(i);
				items.push_back(unit_template->getName());
				texts.push_back(unit_template->getHelp());
			}
		}
		else if( res == 2 ) {
			for(size_t i=0;i<this->getNImprovements();i++) {
				const Improvement *improvement = this->getImprovement(i);
				items.push_back(improvement->getName());
				texts.push_back(improvement->getHelp());
			}
		}

		string okay = "Close";
		int window_w_c = 512;
		int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), &okay, 1, infowindow_def_x_c, infowindow_def_y_c, window_w_c, window_h_c);
		VI_Panel *windowPanel = window->getPanel();

		VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), window_w_c - 32, 32);
		textfield->addText("Please select a topic:");
		textfield->setForeground(1.0f, 1.0f, 1.0f);
		windowPanel->addChildPanel(textfield, 16, 16);

		VI_Listbox *listbox = VI_createListbox(window_w_c/2 - 16, window_h_c - 96, &*items.begin(), items.size(), game_g->getFont());
		initListbox(listbox);
		listbox->setHasInputFocus(true);
		//listbox->setActive(0);
		windowPanel->addChildPanel(listbox, 16, 48);

		VI_Textfield *textfieldInfo = VI_createTextfield(game_g->getFont(), window_w_c/2 - 16, window_h_c - 96);
		textfieldInfo->setForeground(1.0f, 1.0f, 1.0f);
		windowPanel->addChildPanel(textfieldInfo, window_w_c/2, 48);

		HelpInfo helpInfo;
		helpInfo.textfieldInfo = textfieldInfo;
		helpInfo.texts = &texts;
		listbox->setUserData(&helpInfo);
		listbox->setAction(helpAction);
		//askTechnologyAction(listbox);

		window->doModal();

		delete window;
		//delete textfield;
		//delete listbox;
	}
}

void MainGamestate::beginTurn() {
	VI_log("Begin turn\n");
	// called at start of new turn
	for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		civilization->calculateCivBonuses();
		for(size_t i=0;i<civilization->getNUnits();i++) {
			Unit *unit = civilization->getUnit(i);
			unit->reset();
		}
	}
	moved_units_this_turn = false;

	year++;
	VI_log("Year is now %d\n", year);
	for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		civilization->recordHistory();
	}

	if( this->game_n_turns != 0 && this->year == this->game_n_turns+1 ) {
		VI_log("end of game!\n");
		std::map<int, const Civilization *> scores;
		for(size_t i=0;i<this->getNCivilizations();i++) {
			const Civilization *civ = this->getCivilization(i);
			if( !civ->isDead() ) {
				scores.insert(std::pair<int, const Civilization *>(civ->getPower(), civ));
			}
		}
		stringstream text;
		text << "We have reached the end of the game! The scores are as follows:\n\n";
		/*for(size_t i=0;i<this->getNCivilizations();i++) {
			const Civilization *civ = this->getCivilization(i);
			if( !civ->isDead() ) {
				text << i << ". " << civ->getName() << ", " << civ->getPower() << "\n";
			}
		}*/
		int index = 1;
		for(std::map<int, const Civilization *>::const_reverse_iterator iter = scores.rbegin(); iter != scores.rend(); ++iter, index++) {
			const Civilization *civ = iter->second;
			text << index << ". " << civ->getName() << ", " << iter->first << "\n";
		}
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
		window->doModal();
		delete window;
	}

	phase = PHASE_PLAYERTURN;
	if( !test_mode && ( year > 1 || SAVE_FIRST_TURN ) ) {
		string autofilename = savegames_dirname;
		autofilename += "/auto.sav";
		string auto2filename = savegames_dirname;
		auto2filename += "/auto2.sav";
		// either of these two functions may be expected to fail if files don't exist
		if( remove(auto2filename.c_str()) != 0 ) {
			VI_log("failed to remove old autosave2 file - error: %d\n", errno);
		}
		if( rename(autofilename.c_str(), auto2filename.c_str()) != 0 ) {
			VI_log("failed to rename old autosave file - error: %d\n", errno);
		}
		this->save(autofilename.c_str(), true);
		/*char *full_autofilename = VI_getApplicationFilename(autofilename.c_str());
		char *full_auto2filename = VI_getApplicationFilename(auto2filename.c_str());
		if( remove(full_auto2filename) != 0 ) {
			VI_log("failed to remove old autosave2 file - error: %d\n", errno);
		}
		if( rename(full_autofilename, full_auto2filename) != 0 ) {
			VI_log("failed to rename old autosave file - error: %d\n", errno);
		}
		this->save(full_autofilename, true);
		delete [] full_autofilename;
		delete [] full_auto2filename;*/
	}

	// currently called per-frame anyway - but needed here for testing in "silent" mode
	//this->calculateTerritory();

	if( this->player != NULL ) {
		this->player->calculateFogOfWar(true);
	}
}

void MainGamestate::logEndturnTime(const char *text) {
	int time_new_ms = VI_getGameTimeMS();
	VI_log("### %s: %d\n", text, time_new_ms - time_store_ms);
	time_store_ms = time_new_ms;
}

void MainGamestate::endPlayerTurn() {
	VI_log("End player's turn\n");
	T_ASSERT(phase == PHASE_PLAYERTURN);

	// test:
	/*AIInterface::setMainGamestate(this);
	AIInterface::setAICivilization(this->getCivilization(1));
	game_g->runScript("conquests/data/scripts/ai.lua", "test");
	AIInterface::setMainGamestate(NULL);
	AIInterface::setAICivilization(NULL);*/

	time_store_ms = time_endplayerturn_s_ms = VI_getGameTimeMS();
	active_unit = NULL;
	setInputMode(INPUTMODE_NORMAL);
	phase = PHASE_CPUTURN;
	//updateUnitButtons();
	updateButtons();
	if( infoWindow != NULL ) {
		delete infoWindow;
		infoWindow = NULL;
	}

	// consider declaring war, or making peace, and right of passage agreements
	int permille_chance_war_per_turn_c = 15;
	if( this->aiaggression == AIAGGRESSION_PEACEFUL )
		permille_chance_war_per_turn_c /= 2;
	const int permille_chance_peace_per_turn_c = 20; // only for between AIs
	for(vector<Civilization *>::const_iterator iter1=civilizations.begin();iter1 != civilizations.end(); ++iter1) {
		Civilization *civ1 = *iter1;
		if( civ1 == player )
			continue;
		if( civ1->isDead() )
			continue;
		if( civ1->getRace()->isAlwaysHostile() )
			continue;
		for(vector<Civilization *>::const_iterator iter2=civilizations.begin();iter2 != civilizations.end(); ++iter2) {
			Civilization *civ2 = *iter2;
			if( civ1 == civ2 )
				continue;
			if( civ2->isDead() )
				continue;
			if( civ2->getRace()->isAlwaysHostile() )
				continue;
			Relationship *relationship = this->findRelationship(civ1, civ2);
			if( !relationship->madeContact() ) {
				continue;
			}
			bool inform_player = false;
			if( civ2 == player ) {
				inform_player = true;
			}
			else if( !player->isDead() ) {
				const Relationship *r = this->findRelationship(civ1, player);
				if( r->madeContact() )
					inform_player = true;
				else {
					r = this->findRelationship(civ2, player);
					if( r->madeContact() )
						inform_player = true;
				}
			}
			int dummy = 0;
			int civ1_s = civ1->calculateMilitary(&dummy);
			int civ2_s = civ2->calculateMilitary(&dummy);
			int civ2_n = civ2->calculateNuclear();
			if( relationship->getStatus() == Relationship::STATUS_WAR ) {
				// we have to take the maximum of from the point of view of both civs
				int min_turns_war1 = this->getMinTurnsWar(civ1, civ2);
				int min_turns_war2 = this->getMinTurnsWar(civ2, civ1);
				int min_turns_war = max(min_turns_war1, min_turns_war2);
				VI_log("%s vs %s: min_turns_war: %d, %d -> %d\n", civ1->getName(), civ2->getName(), min_turns_war1, min_turns_war2, min_turns_war);
				if( civ2 != player && year >= relationship->getYearDeclaredWar() + min_turns_war ) {
					//if( civ2 != player ) {
					// consider making peace
					int chance_make_peace = permille_chance_peace_per_turn_c;
					if( civ2_n >= 2 ) {
						chance_make_peace *= 10;
					}
					else if( civ1_s > 1.5 * civ2_s ) {
						chance_make_peace /= 2;
					}
					else if( 1.5 * civ1_s < civ2_s ) {
						chance_make_peace *= 2;
					}
					int prob = rand() % 1000;
					//prob = 0;
					VI_log("peace? rolled a %d ; chance_make_peace %d\n", prob, chance_make_peace);
					if( prob < chance_make_peace ) {
						relationship->setStatus(Relationship::STATUS_PEACE);
						VI_log("%s makes peace with %s\n", civ1->getName(), civ2->getName());
						if( inform_player ) {
							stringstream text;
							text << "The war between the ";
							text << civ1->getName();
							text << " and the ";
							text << civ2->getName();
							text << " is over.";
							InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
							window->doModal();
							delete window;
						}
					}
				}
			}
			else {
				int chance_declare_war = permille_chance_war_per_turn_c;
				if( civ1->getNCities() == 0 ) {
					chance_declare_war = 0; // don't declare war if no cities!
				}
				if( civ2_n >= 2 ) {
					chance_declare_war /= 5;
				}
				else if( civ1_s > 1.5 * civ2_s ) {
					chance_declare_war *= 2;
				}
				else if( 1.5 * civ1_s < civ2_s ) {
					chance_declare_war /= 2;
				}
				if( civ1_s >= civ2_s && this->year <= 100 ) {
					// declare war if we think we can attack a weak city?
					for(size_t i=0;i<civ2->getNCities();i++) {
						const City *city = civ2->getCity(i);
						if( city->getSize() == 1 ) {
							Pos2D pos = city->getPos();
							for(int y=pos.y-1;y<=pos.y+1;y++) {
								for(int x=pos.x-1;x<=pos.x+1;x++) {
									if( map->isValid(x, y) ) {
										map->getSquare(x, y)->setTarget(true);
									}
								}
							}
						}
					}
					for(size_t i=0;i<civ1->getNUnits() && chance_declare_war < 100;i++) {
						const Unit *unit = civ1->getUnit(i);
						if( map->getSquare(unit->getPos())->isTarget() && unit->getTemplate()->getAttack() > 0 ) {
							int this_chance = 0;
							if( civ1_s > 1.5 * civ2_s && unit->getTemplate()->getAttack() > 1 ) {
								this_chance = 1000;
							}
							else if( unit->getTemplate()->getAttack() > 1 ) {
								this_chance = 750;
							}
							else {
								this_chance = 500;
							}
							if( this->aiaggression == AIAGGRESSION_PEACEFUL )
								this_chance /= 2;
							VI_log("%s consider sneak attack, chance %d\n", civ1->getName(), this_chance);
							chance_declare_war = max(chance_declare_war, this_chance);
						}
					}
					map->resetTargets();
				}
				int prob = rand() % 1000;
				//prob = 0;
				VI_log("%s vs %s: war? rolled a %d ; chance_declare_war %d\n", civ1->getName(), civ2->getName(), prob, chance_declare_war);
				if( prob < chance_declare_war ) {
					relationship->setStatus(Relationship::STATUS_WAR);
					VI_log("%s declare war on %s\n", civ1->getName(), civ2->getName());
					if( inform_player ) {
						stringstream text;
						text << civ1->getName();
						if( civ2 == player ) {
							text << " have declared war on us!";
						}
						else {
							text << " declare war on the ";
							text << civ2->getName();
							text << "!";
						}
						//game_g->getSound()->playMedia("conquests/data/music/Battle March.ogg");
						game_g->getSound()->playMedia(getFullPath("data/music/Battle March.ogg").c_str());
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
						window->doModal();
						delete window;
						game_g->getSound()->stopMedia();
					}
				}
			}

			if( relationship->getStatus() == Relationship::STATUS_PEACE && civ2 != player ) {
				if( !relationship->hasRightOfPassage() && relationship->canMakeRightOfPassage() ) {
					int year_rop1 = relationship->getYearConsiderROP(civ1);
					int year_rop2 = relationship->getYearConsiderROP(civ2);
					if( ( year_rop1 == -1 || this->year >= year_rop1 ) &&
						( year_rop2 == -1 || this->year >= year_rop2 ) )
					{
						VI_log("%s make right of passage agreement with %s\n", civ1->getName(), civ2->getName());
						relationship->setRightOfPassage(true);
					}
				}
			}
		}
	}

	// exchange technologies, share maps?
	for(vector<Civilization *>::const_iterator iter1=civilizations.begin();iter1 != civilizations.end(); ++iter1) {
		Civilization *civ1 = *iter1;
		if( civ1 == player )
			continue;
		if( civ1->isDead() )
			continue;
		for(vector<Civilization *>::const_iterator iter2=iter1+1;iter2 != civilizations.end(); ++iter2) {
			Civilization *civ2 = *iter2;
			if( civ2 == player )
				continue;
			if( civ2->isDead() )
				continue;
			Relationship *relationship = this->findRelationship(civ1, civ2);
			if( !relationship->madeContact() ) {
				continue;
			}
			if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
				bool done = false;
				while( !done ) {
					vector<const Technology *> civ1_distinct_technologies;
					vector<const Technology *> civ2_distinct_technologies;
					Civilization::getDistinctTechnologies(this, &civ1_distinct_technologies, &civ2_distinct_technologies, civ1, civ2);
					if( civ1_distinct_technologies.size() > 0 && civ2_distinct_technologies.size() > 0 ) {
						// exchange
						int choose1 = rand() % civ2_distinct_technologies.size();
						const Technology *tech1 = civ2_distinct_technologies.at( choose1 );
						civ1->addTechnology( tech1 );
						int choose2 = rand() % civ1_distinct_technologies.size();
						const Technology *tech2 = civ1_distinct_technologies.at( choose2 );
						civ2->addTechnology( tech2 );
						VI_log("civs exchange technology: %s take %s; %s take %s\n", civ1->getName(), tech1->getName(), civ2->getName(), tech2->getName());
					}
					else {
						done = true;
					}
				}
				if( civ1->canShareMaps(civ2) && civ1->compareMaps(civ2) ) {
					civ1->shareMaps(civ2);
					VI_log("civs share maps: %s and %s\n", civ1->getName(), civ2->getName());
				}
			}
		}
	}

	// reset AI that needs doing
	for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		civilization->resetAI();
	}
	// do city AI here
	// not strictly accurate, as all players have their cities updated, then they move, but will do for now
	for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		if( civilization == player ) {
			continue;
		}
		for(size_t i=0;i<civilization->getNCities();i++) {
			City *city = civilization->getCity(i);
			city->doAI();
		}
	}
	for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		if( civilization == player ) {
			continue;
		}
		for(size_t i=0;i<civilization->getNUnits();i++) {
			Unit *unit = civilization->getUnit(i);
			//unit->setAIStartPos();
			unit->setAIMoveCount();
			unit->setAIDest();
		}
	}
	logEndturnTime("city based AI");
	VI_log("Done city-based AI\n");
}

// only used by MainGamestate::endTurn(), but can't be local type as we want to use it in template (vector) - won't compile on Linux (though okay in C++11)
struct Candidate {
    Pos2D pos;
    const City *city;
    Candidate(Pos2D pos, const City *city) : pos(pos), city(city) {
    }
};

void MainGamestate::endTurn() {
	logEndturnTime("unit based AI");
	// called when all players' turns end
	VI_log("End turn\n");
	T_ASSERT( active_unit == NULL );

	// new rebel armies
	ASSERT( rebel_civ != NULL );
	for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		if( civilization->getRace()->isDummy() ) {
			continue;
		}
		if( civilization->getNCities() == 0 ) {
			continue;
		}
		bool prevents_rebellion = false;
		for(int i=0;i<civilization->getNTechnologies() && !prevents_rebellion;i++) {
			if( civilization->getTechnology(i)->getPreventsRebellion() ) {
				prevents_rebellion = true;
			}
		}
		if( prevents_rebellion ) {
			continue;
		}
		//VI_log("rebellion for %s\n", civilization->getName());
		float permille_chance = 20.0f;
		int civ_pop = civilization->calculateTotalPopulation();
		if( civilization->getNCities() < 3 || civ_pop < 100000 ) {
			// no rebellion chance for small civs
			permille_chance = 0.0f;
		}
		else {
			for(int i=0;i<civilization->getNCities();i++) {
				const City *city = civilization->getCity(i);
				for(int j=0;j<city->getNImprovements();j++) {
					const Improvement *improvement = city->getImprovement(j);
					int bonus = improvement->getRebellionBonus();
					if( bonus > 0 ) {
						float scale = ((float)(100 - bonus)) / 100.0f;
						//VI_log("    %s has %s, scale by %f\n", city->getName(), improvement->getName(), scale);
						scale = pow(scale, 1.0f / (float)civilization->getNCities() );
						//VI_log("    for %d cities, scaled to %f\n", civilization->getNCities(), scale);
						permille_chance *= scale;
					}
				}
			}
		}
		int r = rand() % 1000;
		//VI_log("    chance %f\n", permille_chance);
		VI_log("rebellion for %s? chance %f rolled a %d\n", civilization->getName(), permille_chance, r);
		if( r < (int)permille_chance ) {
			int city_indx = rand() % civilization->getNCities();
			const City *build_city = civilization->getCity(city_indx);
			vector<const UnitTemplate *> candidate_units;
			bool cheapest = ( rand() % 4 ) != 0;
			int min_cost = -1;
			for(int i=0;i<game_g->getGameData()->getNUnitTemplates();i++) {
				const UnitTemplate *unit_template = game_g->getGameData()->getUnitTemplate(i);
				if( unit_template->getAttack() > 0 && build_city->canBuild(unit_template) ) {
					if( cheapest ) {
						if( min_cost == -1 || unit_template->getCost() <= min_cost ) {
							if( min_cost != -1 && unit_template->getCost() < min_cost )
								candidate_units.clear();
							candidate_units.push_back(unit_template);
							min_cost = unit_template->getCost();
						}
					}
					else {
						candidate_units.push_back(unit_template);
					}
				}
			}
			if( candidate_units.size() > 0 ) {
				int n_units = 1;
				if( civ_pop >= 200000 && ( rand() % 4 ) == 0 )
					n_units = 2;
				VI_log("%d units; cheapest? %d; %d candidate units\n", n_units, cheapest, candidate_units.size());
				vector<const UnitTemplate *> new_units;
				for(int i=0;i<n_units;i++) {
					int indx = rand() % candidate_units.size();
					new_units.push_back( candidate_units.at(indx) );
				}
				vector<Candidate> candidates;
				for(int i=0;i<civilization->getNCities();i++) {
					const City *city = civilization->getCity(i);
					for(int j=0;j<City::getNCitySquares();j++) {
						if( j == City::getCentreCitySquareIndex() )
							continue;
						Pos2D pos = city->getCitySquare(j);
						if( !this->map->isValid(pos.x, pos.y) )
							continue;
						const MapSquare *square = map->getSquare(pos);
						if( square->getCity() != NULL || square->getUnits()->size() > 0 ) {
							continue;
						}
						// check if units can move here
						bool can_move_to = true;
						for(vector<const UnitTemplate *>::const_iterator iter = new_units.begin(); iter != new_units.end() && can_move_to; ++iter) {
							const UnitTemplate *unit_template = *iter;
							if( !square->canMoveTo(unit_template) ) {
								can_move_to = false;
							}
						}
						if( !can_move_to )
							continue;
						Candidate candidate(pos, city);
						candidates.push_back(candidate);
					}
				}
				if( candidates.size() > 0 ) {
					int indx = rand() % candidates.size();
					Candidate candidate = candidates.at(indx);
					VI_log("rebel uprising near the %s city of %s\n", candidate.city->getCivilization()->getNameAdjective(), candidate.city->getName());
					for(vector<const UnitTemplate *>::const_iterator iter = new_units.begin(); iter != new_units.end(); ++iter) {
						const UnitTemplate *unit_template = *iter;
						new Unit(this, rebel_civ, unit_template, candidate.pos);
					}
					if( civilization == player ) {
						this->centre(candidate.city->getPos());
						stringstream text;
						text << "There is a rebel uprising near the city of " << candidate.city->getName() << "! Act quickly to crush the rebellion!";
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, 128);
						window->doModal();
						delete window;
					}
				}
			}
		}
	}

	// set all objects that need updating
	for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		for(size_t i=0;i<civilization->getNCities();i++) {
			City *city = civilization->getCity(i);
			//city->update();
			city->setNeedsUpdate();
		}
		for(size_t i=0;i<civilization->getNUnits();i++) {
			Unit *unit = civilization->getUnit(i);
			unit->update();
		}
		civilization->update();
	}
	logEndturnTime("updating all cities and units");
	phase = PHASE_ENDTURN;
}

void MainGamestate::updateButtons() const {
	updateUnitButtons();

	bool buttons_visible = this->phase == PHASE_PLAYERTURN;

	this->endturnButton->setVisible(buttons_visible);
	this->newgameButton->setVisible(buttons_visible);
	this->loadgameButton->setVisible(buttons_visible);
	this->savegameButton->setVisible(buttons_visible);
	this->savemapButton->setVisible(buttons_visible);
	this->citiesAdvisorButton->setVisible(buttons_visible);
	this->unitsAdvisorButton->setVisible(buttons_visible);
	this->civsAdvisorButton->setVisible(buttons_visible);
	this->techsAdvisorButton->setVisible(buttons_visible);
	this->scoresAdvisorButton->setVisible(buttons_visible);
	this->mapButton->setVisible(buttons_visible);
	this->gridButton->setVisible(buttons_visible);
	this->zoomOutButton->setVisible(buttons_visible);
	this->zoomResetButton->setVisible(buttons_visible);
	this->zoomInButton->setVisible(buttons_visible);
	this->viewButton->setVisible(buttons_visible);
	this->helpButton->setVisible(buttons_visible);

	this->homeHiddenButton->setVisible(buttons_visible);
	this->centreHiddenButton->setVisible(buttons_visible);
	this->move1HiddenButton->setVisible(buttons_visible);
	this->move2HiddenButton->setVisible(buttons_visible);
	this->move3HiddenButton->setVisible(buttons_visible);
	this->move4HiddenButton->setVisible(buttons_visible);
	this->move6HiddenButton->setVisible(buttons_visible);
	this->move7HiddenButton->setVisible(buttons_visible);
	this->move8HiddenButton->setVisible(buttons_visible);
	this->move9HiddenButton->setVisible(buttons_visible);
	this->moveLHiddenButton->setVisible(buttons_visible);
	this->moveRHiddenButton->setVisible(buttons_visible);
	this->moveUHiddenButton->setVisible(buttons_visible);
	this->moveDHiddenButton->setVisible(buttons_visible);
	this->returnHiddenButton->setVisible(buttons_visible);

}

void MainGamestate::updateUnitButtons() const {
	bool enabled = this->active_unit != NULL;
	this->waitButton->setVisible(enabled);
	this->skipTurnButton->setVisible(enabled);
	this->fortifyButton->setVisible(enabled);
	//this->buildCityButton->setVisible(enabled && active_unit->getTemplate()->canBuildCity() && map->canBuildCity(active_unit->getX(), active_unit->getY()));
	this->buildCityButton->setVisible(enabled && active_unit->getTemplate()->canBuildCity() && map->getSquare(active_unit->getX(), active_unit->getY())->canBuildCity());
	this->buildRoadButton->setVisible(enabled && active_unit->getTemplate()->canBuildRoads() && map->getSquare(active_unit->getX(), active_unit->getY())->getRoad() == ROAD_NONE);
	//const Technology *tech = game_g->getGameData()->findTechnology("Steam Power");
	//this->buildRailwaysButton->setVisible(enabled && active_unit->getTemplate()->canBuildRoads() && map->getSquare(active_unit->getX(), active_unit->getY())->getRoad() == ROAD_BASIC && player->hasTechnology(tech));
	this->buildRailwaysButton->setVisible(enabled && active_unit->getTemplate()->canBuildRoads() && map->getSquare(active_unit->getX(), active_unit->getY())->getRoad() == ROAD_BASIC && player->hasTechnology("Steam Power"));
	this->automateButton->setVisible(enabled && active_unit->getTemplate()->canBuildRoads());
	this->gotoButton->setVisible(enabled && !active_unit->getTemplate()->isAir() && !active_unit->getTemplate()->isSea());

	bool show_move_buttons = enabled;
	if( show_move_buttons ) {
		if( view_3d ) {
			VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
			/*Vector3D pos = vp->getPosition();
			LOG("pos.y = %f\n", pos.y);
			if( pos.y >= 20.0f ) {*/
			float mx0 = 0.0f, mx1 = 0.0f, my = 0.0f;
			getMapPosf(&mx0, &my, 0, 0);
			getMapPosf(&mx1, &my, game_g->getGraphicsEnvironment()->getWidth()-1, 0);
			float width = mx1 - mx0;
			float preferred_width = def_view3d_scale * game_g->getGraphicsEnvironment()->getWidth() / mainGamestate->def_sq_width;
			float ratio = preferred_width / width;
			//LOG("ratio = %f\n", ratio);
			if( ratio < 0.5f ) {
				show_move_buttons = false;
			}
		}
		else {
			//LOG("scale_width = %f\n", scale_width);
			if( this->scale_width < 0.5f ) {
				show_move_buttons = false;
			}
		}
	}
	if( show_move_buttons ) {
		bool checkEnemies = ( this->active_unit->getTemplate()->getAttack() == 0 );
		Pos2D unit_pos = this->active_unit->getPos();
		// check for enemies blocking, if we can't attack
		this->move1Button->setVisible( this->active_unit->canMoveTo( unit_pos + Pos2D(-1, 1), checkEnemies, false ) );
		this->move2Button->setVisible( this->active_unit->canMoveTo( unit_pos + Pos2D(0, 1), checkEnemies, false ) );
		this->move3Button->setVisible( this->active_unit->canMoveTo( unit_pos + Pos2D(1, 1), checkEnemies, false ) );
		this->move4Button->setVisible( this->active_unit->canMoveTo( unit_pos + Pos2D(-1, 0), checkEnemies, false ) );
		this->move6Button->setVisible( this->active_unit->canMoveTo( unit_pos + Pos2D(1, 0), checkEnemies, false ) );
		this->move7Button->setVisible( this->active_unit->canMoveTo( unit_pos + Pos2D(-1, -1), checkEnemies, false ) );
		this->move8Button->setVisible( this->active_unit->canMoveTo( unit_pos + Pos2D(0, -1), checkEnemies, false ) );
		this->move9Button->setVisible( this->active_unit->canMoveTo( unit_pos + Pos2D(1, -1), checkEnemies, false ) );
	}
	else {
		this->move1Button->setVisible(false);
		this->move2Button->setVisible(false);
		this->move3Button->setVisible(false);
		this->move4Button->setVisible(false);
		this->move6Button->setVisible(false);
		this->move7Button->setVisible(false);
		this->move8Button->setVisible(false);
		this->move9Button->setVisible(false);
	}

	bool can_travel = false;
	bool can_bomb = false;
	bool can_recon = false;
	//if( enabled && !active_unit->getTemplate()->isAir() ) {
	if( enabled ) {
		const City *city = map->findCity(active_unit->getPos());
		//if( city != NULL && city->canTravel(active_unit->getTemplate()) ) {
		/*if( city != NULL && city->canTravel(by_air) ) {
			can_travel = true;
		}*/
		if( city != NULL ) {
			int sea_range = 0;
			int air_range = 0;
			city->getTravelRanges(&sea_range, &air_range, active_unit->getTemplate());
			if( sea_range > 0 || air_range > 0 ) {
				can_travel = true;
			}
		}
		if( active_unit->getTemplate()->isAir() ) {
			ASSERT( city != NULL );
			if( active_unit->getTemplate()->canBomb() && city->canLaunch(active_unit->getTemplate()) ) {
				can_bomb = true;
			}
			if( active_unit->getTemplate()->getAirRange() > 0 && !active_unit->getTemplate()->isMissile() && city->canLaunch(active_unit->getTemplate()) ) {
				can_recon = true;
			}
		}
	}
	this->travelButton->setVisible(can_travel);
	//bool can_air_raid = enabled && active_unit->getTemplate()->isAir() && ( active_unit->getTemplate()->getBombard() > 0 || active_unit->getTemplate()->getNuclearType() != UnitTemplate::NUCLEARTYPE_NONE );
	this->airRaidButton->setVisible( can_bomb && active_unit->hasMovesLeft() );
	//this->reconnaissanceButton->setVisible( can_recon && active_unit->hasMovesLeft() && !active_unit->getCivilization()->hasTechnology( findTechnology("Satellites") ) );
	this->reconnaissanceButton->setVisible( can_recon && active_unit->hasMovesLeft() && !active_unit->getCivilization()->hasTechnology("Satellites") );
}

void MainGamestate::activateUnit(Unit *unit, bool centre) {
	VI_log("MainGamestate::activateUnit\n");
	T_ASSERT( unit->getCivilization() == player );
	T_ASSERT( phase == PHASE_PLAYERTURN );
	T_ASSERT( unit->hasMovesLeft() );
	unit->setStatus(Unit::STATUS_NORMAL);
	unit->setAutomated(false); // also unsets goto
	blink_time_start_ms = VI_getGameTimeMS();
	active_unit = unit;
	updateUnitButtons();
	cache_active_unit = active_unit;
	moved_units_this_turn = true;
	if( centre ) {
		centreUnit();
	}
	bool created_window = false;
	if( active_unit->getTemplate()->canBuildCity() ) {
		if( !help_buildcity ) {
			this->createInfoWindow("Settlers can be used to build cities. Press 'B', or click 'Build City'.\n\nYou can also move units around, using the arrow keys, the numeric keypad, clicking the move buttons surrounding the unit, or right clicking on an adjacent square.");
			created_window = true;
			help_buildcity = true;
		}
	}
	else if( active_unit->getTemplate()->canBuildRoads() ) {
		if( !help_buildroad ) {
			this->createInfoWindow("This unit can be used to build roads. Press 'R', or click 'Build Road'.");
			created_window = true;
			help_buildroad = true;
		}
	}
	else if( active_unit->getTemplate()->canBomb() ) {
		if( !help_bomb ) {
			this->createInfoWindow("This unit can bomb enemy cities! Press 'B', or click 'Bomb', then select one of the highlighted red squares (an enemy city within the unit's range).");
			created_window = true;
			help_bomb = true;
		}
	}

	/*if( !created_window && InfoWindow::getActiveWindow() != NULL ) {
		delete InfoWindow::getActiveWindow();
	}*/
	if( !created_window && infoWindow != NULL ) {
		delete infoWindow;
		infoWindow = NULL;
	}
	//VI_addtag();
}

void MainGamestate::wakeupUnit(Unit *unit) {
	// makes the unit available - e.g., unfortifying, unautomating - without making it the active unit
	T_ASSERT( unit->getCivilization() == player );
	unit->setStatus(Unit::STATUS_NORMAL);
	unit->setAutomated(false); // also unsets goto
}

void MainGamestate::home() {
	if( player->getNCities() > 0 ) {
		const City *city = player->getCity(0);
		centre(city->getPos());
	}
	else if( player->getNUnits() > 0 ) {
		const Unit *unit = player->getUnit(0);
		centre(unit->getPos());
	}
}

void MainGamestate::setInputMode(InputMode inputMode) {
	if( this->inputMode != inputMode ) {
		this->inputMode = inputMode;
/*#ifdef _WIN32
		HCURSOR cursor = NULL;
		if( inputMode == INPUTMODE_TRAVEL || inputMode == INPUTMODE_BOMBARD )
			cursor = LoadCursor( NULL, IDC_CROSS );
		else {
			cursor = LoadCursor( NULL, IDC_ARROW );
		}
		SetCursor(cursor);
		SetClassLong(game_g->getGraphicsEnvironment()->getHWND(), GCL_HCURSOR, (LONG)cursor);
#endif*/
		if( inputMode == INPUTMODE_TRAVEL || inputMode == INPUTMODE_BOMBARD || inputMode == INPUTMODE_RECONNAISSANCE )
			game_g->setCursor(cursor_target);
		else {
			game_g->setCursor(cursor_arrow);
		}
		/*if( inputMode == INPUTMODE_TRAVEL || inputMode == INPUTMODE_BOMBARD || inputMode == INPUTMODE_RECONNAISSANCE) {
		}
		else {*/
		if( inputMode == INPUTMODE_NORMAL ) {
			map->resetTargets();
		}
	}
}

void MainGamestate::centre(Pos2D pos) {
	this->centre(pos.x + 0.5f, pos.y + 0.5f);
}

void MainGamestate::centre(float mx, float my) {
	if( view_3d ) {
		VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
		Vector3D pos = vp->getPosition();
		Vector3D dir = game_g->getGraphicsEnvironment()->getDirectionOfPoint(vp, game_g->getGraphicsEnvironment()->getWidth()/2, game_g->getGraphicsEnvironment()->getHeight()/2);
		T_ASSERT( dir.y < 0.0f );
		float scale = pos.y / dir.y;
		pos.set(mx, 0.0f, my);
		pos += dir * scale;
		vp->setPosition(pos);
	}
	else {
		/*int n_x = ( game_g->getGraphicsEnvironment()->getWidth() / this->getSqWidth() ) + 1;
		int n_y = ( game_g->getGraphicsEnvironment()->getHeight() / this->getSqHeight() ) + 1;*/
		float n_x = ( game_g->getGraphicsEnvironment()->getWidth() / (float)this->getSqWidth() );
		float n_y = ( game_g->getGraphicsEnvironment()->getHeight() / (float)this->getSqHeight() );
		mx -= n_x/2;
		my -= n_y/2;
		/*if( mx < 0 )
			mx = 0;
		if( my < 0 )
			my = 0;*/
		map_pos_x = mx;
		map_pos_y = my;
	}

	checkMapView();
}

void MainGamestate::centreUnit() {
	if( active_unit != NULL ) {
		Pos2D pos = active_unit->getPos();
		centre(pos);
	}
}

void MainGamestate::addFireEffect(Pos2D pos) {
	Effect *effect = new Effect(this->fireEntity_2d, this->fireEntity_3d, pos.x, pos.y, fire_effect_duration_ms_c);
	this->effects.push_back(effect);
}

struct AskTechnologyInfo {
	VI_Textfield *textfieldInfo;
	const vector<const Technology *> *player_technologies;
	AskTechnologyInfo() : textfieldInfo(NULL), player_technologies(NULL) {
	}
};

void MainGamestate::askTechnologyAction(VI_Panel *source) {
	/*VI_Textfield *textfieldInfo = static_cast<VI_Textfield *>(source->getUserData());
	ASSERT( textfieldInfo != NULL );*/
	AskTechnologyInfo *askTechnologyInfo = static_cast<AskTechnologyInfo *>(source->getUserData());
	ASSERT( askTechnologyInfo != NULL );
	VI_Textfield *textfieldInfo = askTechnologyInfo->textfieldInfo;
	ASSERT( textfieldInfo != NULL );
	const vector<const Technology *> *player_technologies = askTechnologyInfo->player_technologies;
	ASSERT( player_technologies != NULL );
	VI_Listbox *listbox = dynamic_cast<VI_Listbox *>(source);
	int active = listbox->getActive();
	ASSERT( active >= 0 && active < player_technologies->size() );
	const Technology *technology = player_technologies->at(active);
	textfieldInfo->clear();
	textfieldInfo->addText( technology->getHelp().c_str() );
	textfieldInfo->setCPos(0);
}

void MainGamestate::askTechnology() {
	int science = player->calculateTotalScience();
	vector<const Technology *> *player_technologies = new vector<const Technology *>();
	/*for(vector<Technology *>::const_iterator iter = technologies.begin(); iter != technologies.end(); ++iter) {
		const Technology *technology = *iter;*/
	for(size_t i=0;i<this->getNTechnologies();i++) {
		const Technology *technology = this->getTechnology(i);
		if( player->canResearch(technology, true) ) {
			player_technologies->push_back(technology);
		}
	}
	if( player_technologies->size() == 0 ) {
		delete player_technologies;
		return;
	}

	// convert to names
	vector<string> technology_names;
	for(vector<const Technology *>::const_iterator iter = player_technologies->begin();iter != player_technologies->end(); ++iter) {
		const Technology *technology = *iter;
		/*const char *name = technology->getName();
		technology_names.push_back(name);*/
		stringstream name;
		name << technology->getName();
		name << " (";
		int n_turns = (int)ceil( ((double)technology->getCost(this)) / (double)science );
		name << n_turns;
		name << " turns)";
		technology_names.push_back(name.str().c_str());
	}

	string okay = "Okay";
	//const int window_h_c = 512;
	int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), &okay, 1, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, window_h_c);
	VI_Panel *windowPanel = window->getPanel();

	VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), infowindow_def_w_c - 32, 64);
	int age = (int)player->getAge();
	if( age >= (int)AGE_INDUSTRIAL )
		textfield->addText("What should our scientists be researching?");
	else
		textfield->addText("What should our wise men be researching?");
	//textfield->addText("bfhh r rhk-rhk -kn-g k-k- kvk -kvk-k -b -bko-kb[ ko k[k[o kg [ogk[ kg[o gk [gokdo[gk g[ok [godk o[gdk [gd k[gdk o[k [g k[gd k[gd kgd[ kgd[ kg[dkgd gd okg[d g[d gd ok-ek-e.\n\n\nbfkbsf-bskfb[ofs of[kfokf [kf [fs ksf[ fsko sf.\n"); // test for scrollbars
	textfield->setForeground(1.0f, 1.0f, 1.0f);
	windowPanel->addChildPanel(textfield, 16, 16);

	//VI_Listbox *listbox = VI_createListbox(infowindow_def_w_c - 32, window_h_c - 128, &*technology_names.begin(), technology_names.size(), this->getFont());
	VI_Listbox *listbox = VI_createListbox(infowindow_def_w_c/2 - 16, window_h_c - 128, &*technology_names.begin(), technology_names.size(), game_g->getFont());
	initListbox(listbox);
	listbox->setHasInputFocus(true);
	//listbox->setForeground(1.0f, 1.0f, 1.0f);
	listbox->setActive(0);
	windowPanel->addChildPanel(listbox, 16, 80);

	VI_Textfield *textfieldInfo = VI_createTextfield(game_g->getFont(), infowindow_def_w_c/2 - 16, window_h_c - 128);
	textfieldInfo->setForeground(1.0f, 1.0f, 1.0f);
	windowPanel->addChildPanel(textfieldInfo, infowindow_def_w_c/2, 80);
	//textfieldInfo->addText("blah blah");

	AskTechnologyInfo askTechnologyInfo;
	askTechnologyInfo.textfieldInfo = textfieldInfo;
	askTechnologyInfo.player_technologies = player_technologies;
	listbox->setUserData(&askTechnologyInfo);
	listbox->setAction(askTechnologyAction);
	askTechnologyAction(listbox);

	window->doModal();

	int active = listbox->getActive();

	delete window;
	//delete textfield;
	//delete listbox;

	//int active = InfoWindow::select("What should our wise men be researching?", "Okay", "", 512, &technology_names);

	ASSERT( active >= 0 && active < player_technologies->size() );
	player->setTechnology( player_technologies->at( active ) );
	delete player_technologies;
}

void MainGamestate::askBuildable(City *city) {
	vector<const Buildable *> *buildables = new vector<const Buildable *>();
	vector<string> buildable_names;
	mainGamestate->fillBuildablesList(buildables, &buildable_names, city);
	if( buildables->size() == 0 ) {
		delete buildables;
		return;
	}

	string buttons[] = {"Change Production", "Cancel"};
	int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, 2, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, window_h_c);
	VI_Panel *windowPanel = window->getPanel();

	VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), infowindow_def_w_c - 32, 64);
	stringstream str;
	str << "What should the city of " << city->getName() << " build?";
	if( city->getBuildable() != NULL ) {
		str << " (Currently building: " << city->getBuildable()->getName() << ", " << city->getProgressBuildable() << " out of " << city->getBuildable()->getCost() << ".)";
	}
	textfield->addText(str.str().c_str());
	textfield->setForeground(1.0f, 1.0f, 1.0f);
	windowPanel->addChildPanel(textfield, 16, 16);

	VI_Listbox *listbox = VI_createListbox(infowindow_def_w_c - 32, window_h_c - 128, &*buildable_names.begin(), buildable_names.size(), game_g->getFont());
	initListbox(listbox);
	listbox->setHasInputFocus(true);
	//listbox->setForeground(1.0f, 1.0f, 1.0f);
	listbox->setActive(0);
	windowPanel->addChildPanel(listbox, 16, 80);

	if( window->doModal() == 0 ) {
		int active = listbox->getActive();
		ASSERT( active >= 0 && active < buildables->size() );
		//city->setBuildable( buildables->at( active ) );
		const Buildable *buildable = buildables->at( active );
		bool ok = true;
		int current_progress = city->getProgressBuildable();
		if( buildable != city->getBuildable() && current_progress > 0 ) {
			stringstream text;
			text << "Are you sure you wish to change production and waste " << current_progress << " resources?";
			if( !InfoWindow::confirm(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), game_g->getFont(), game_g->getPanelTexture(), text.str(), "Yes, Change Production", "Cancel") ) {
				ok = false;
			}
		}
		if( ok ) {
			city->setBuildable(buildable);
		}
	}

	delete window;
	//delete textfield;
	//delete listbox;

	delete buildables;
}

void MainGamestate::calculateTerritory() {
	VI_log("MainGamestate::calculateTerritory()\n");
	for(int y=0;y<this->map->getHeight();y++) {
		for(int x=0;x<this->map->getWidth();x++) {
			MapSquare *square = this->map->getSquare(x, y);
			square->setTerritory(NULL);
		}
	}
	for(vector<Civilization *>::iterator iter = this->civilizations.begin(); iter != this->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		for(size_t i=0;i<civilization->getNCities();i++) {
			const City *city = civilization->getCity(i);
			for(int j=0;j<city->getNCitySquares();j++) {
				Pos2D pos = city->getCitySquare(j);
				if( this->map->isValid(pos.x, pos.y) ) {
					MapSquare *square = this->map->getSquare(pos);
					if( square->getTerritory() != NULL ) {
						VI_log("### OVERLAPPING TERRITORY at %d, %d\n", pos.x, pos.y);
						T_ASSERT( square->getTerritory() == NULL ); // shouldn't have overlapping territory!
					}
					square->setTerritory(civilization);
				}
			}
		}
	}
	/*const Civilization *civ = this->map->getSquare(32, 9)->getTerritory();
	VI_log("### %s\n", civ==NULL ? "NULL" : civ->getName());*/
}

void MainGamestate::refreshMapDisplay() {
	if( this->mapImageButton != NULL ) {
		VI_log("MainGamestate::refreshMapDisplay()\n");
		// Simplest way to refresh is to just hide and show the map display.
		// Done by calling the function twice - so yes this is intentional, not a
		// copy/paste bug!
		this->showMap();
		this->showMap();
	}
	this->map_needs_update = false;
}

void MainGamestate::renderQuad2D(int pos_x, int pos_z, const unsigned char *rgba) {
	renderQuad2D((float)pos_x, (float)pos_z, 1.0f, 1.0f, rgba);
}

void MainGamestate::renderQuad2D(float pos_x, float pos_z, float w, float h, const unsigned char *rgba) {
	Renderer *renderer = game_g->getGraphicsEnvironment()->getRenderer();
	const float texcoord_data[] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
	};
	if( rgba == NULL )
		rgba = rgba_white;
	int sq_width = mainGamestate->getSqWidth();
	int sq_height = mainGamestate->getSqHeight();
	const float vertex_data[] = {
		pos_x, (pos_z+h*sq_height), 0.0f,
		(pos_x+w*sq_width), (pos_z+h*sq_height), 0.0f,
		(pos_x+w*sq_width), pos_z, 0.0f,
		pos_x, pos_z, 0.0f
	};
	renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4, rgba, true, 4);
}

void MainGamestate::renderQuad(Pos2D pos, bool flat, const unsigned char *rgba) {
	renderQuad((float)pos.x, (float)pos.y, 1.0f, 1.0f, flat, rgba);
}

void MainGamestate::renderQuad(float pos_x, float pos_z, float w, float h, bool flat, const unsigned char *rgba) {
	Renderer *renderer = game_g->getGraphicsEnvironment()->getRenderer();
	const float texcoord_data[] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
	};
	if( rgba == NULL )
		rgba = rgba_white;
	if( flat ) {
		const float vertex_data[] = {
			pos_x*xscale, 0.0f, (pos_z+h)*zscale,
			(pos_x+w)*xscale, 0.0f, (pos_z+h)*zscale,
			(pos_x+w)*xscale, 0.0f, pos_z*zscale,
			pos_x*xscale, 0.0f, pos_z*zscale
		};
		renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4, rgba, true, 4);
	}
	else {
		/*Vector3D right, up;
		//for(int i=0;i<100;i++) {
		renderer->getViewOrientation(&right, &up); // TODO don't repeatedly recalculate with each function call
		*/
		//}
		//const float y_off = 0.8f;
		const float y_off = 1.0f;
		/*const float vertex_data[] = {
			pos.x*xscale, 0.0f, (pos.y+y_off)*zscale,
			(pos.x+1)*xscale, 0.0f, (pos.y+y_off)*zscale,
			(pos.x+1)*xscale, 1.0f, (pos.y+y_off)*zscale,
			pos.x*xscale, 1.0f, (pos.y+y_off)*zscale
		};*/
		Vector3D bottom(pos_x*xscale, 0.0f, (pos_z+y_off)*zscale);
		Vector3D top = bottom + mainGamestate->view_up;
		const float vertex_data[] = {
			bottom.x, bottom.y, bottom.z,
			bottom.x + xscale, bottom.y, bottom.z,
			top.x + xscale, top.y, top.z,
			top.x, top.y, top.z
		};
		renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4, rgba, true, 4);
	}
}

void MainGamestate::renderFunc() {
	if( mainGamestate->map == NULL ) {
		return;
	}
	if( !mainGamestate->view_3d ) {
		return;
	}

	if( !mainGamestate->init_view_dirs ) {
		// must be done from within the 3D loop!
		game_g->getGraphicsEnvironment()->getRenderer()->getViewOrientation(&mainGamestate->view_right, &mainGamestate->view_up);
		mainGamestate->init_view_dirs = true;
	}

	const float eps = 1.0e-5f;

	Renderer *renderer = game_g->getGraphicsEnvironment()->getRenderer();
	renderer->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_NONE);
	renderer->enableFixedFunctionLighting(false);
	renderer->enableTexturing(false);
	//renderer->setColor4(255, 255, 255, 255);

	Vector3D pos[3];
	mainGamestate->getMapPosf(&pos[0].x, &pos[0].z, 0, 0);
	mainGamestate->getMapPosf(&pos[1].x, &pos[1].z, game_g->getGraphicsEnvironment()->getWidth()-1, 0);
	mainGamestate->getMapPosf(&pos[2].x, &pos[2].z, 0, game_g->getGraphicsEnvironment()->getHeight()-1);
	int start_x = (int)floor(pos[0].x);
	int start_y = (int)floor(pos[0].z);
	int end_x = (int)ceil(pos[1].x);
	int end_y = (int)ceil(pos[2].z);
	start_x = max(0, start_x);
	start_y = max(0, start_y);
	if( mainGamestate->map->getTopology() == TOPOLOGY_FLAT ) {
		end_x = min(mainGamestate->getMap()->getWidth()-1, end_x);
	}
	end_y = min(mainGamestate->getMap()->getHeight()-1, end_y);

	//VI_log("%d, %d to %d\n", start_x, start_y, end_x);

	if( mainGamestate->active_unit != NULL ) {
		Pos2D unit_pos = mainGamestate->active_unit->getPos();
		mainGamestate->getMap()->reduceToBase(&unit_pos.x, &unit_pos.y, start_x, start_y);
		Pos2D screen_pos;
		//Pos2D screen_pos = mainGamestate->getScreenPos(unit_pos.x, unit_pos.y);
		game_g->getGraphicsEnvironment()->worldToScreen(&screen_pos.x, &screen_pos.y, Vector3D(unit_pos.x - 0.5f, 0.0f, unit_pos.y + 1.5f));
		mainGamestate->move1Button->setPosCentred(screen_pos.x, screen_pos.y);
		game_g->getGraphicsEnvironment()->worldToScreen(&screen_pos.x, &screen_pos.y, Vector3D(unit_pos.x + 0.5f, 0.0f, unit_pos.y + 1.5f));
		mainGamestate->move2Button->setPosCentred(screen_pos.x, screen_pos.y);
		game_g->getGraphicsEnvironment()->worldToScreen(&screen_pos.x, &screen_pos.y, Vector3D(unit_pos.x + 1.5f, 0.0f, unit_pos.y + 1.5f));
		mainGamestate->move3Button->setPosCentred(screen_pos.x, screen_pos.y);
		game_g->getGraphicsEnvironment()->worldToScreen(&screen_pos.x, &screen_pos.y, Vector3D(unit_pos.x - 0.5f, 0.0f, unit_pos.y + 0.5f));
		mainGamestate->move4Button->setPosCentred(screen_pos.x, screen_pos.y);
		game_g->getGraphicsEnvironment()->worldToScreen(&screen_pos.x, &screen_pos.y, Vector3D(unit_pos.x + 1.5f, 0.0f, unit_pos.y + 0.5f));
		mainGamestate->move6Button->setPosCentred(screen_pos.x, screen_pos.y);
		game_g->getGraphicsEnvironment()->worldToScreen(&screen_pos.x, &screen_pos.y, Vector3D(unit_pos.x - 0.5f, 0.0f, unit_pos.y - 0.5f));
		mainGamestate->move7Button->setPosCentred(screen_pos.x, screen_pos.y);
		game_g->getGraphicsEnvironment()->worldToScreen(&screen_pos.x, &screen_pos.y, Vector3D(unit_pos.x + 0.5f, 0.0f, unit_pos.y - 0.5f));
		mainGamestate->move8Button->setPosCentred(screen_pos.x, screen_pos.y);
		game_g->getGraphicsEnvironment()->worldToScreen(&screen_pos.x, &screen_pos.y, Vector3D(unit_pos.x + 1.5f, 0.0f, unit_pos.y - 0.5f));
		mainGamestate->move9Button->setPosCentred(screen_pos.x, screen_pos.y);
	}

	// calculate territory (TODO: don't recalculate every frame!)
	//mainGamestate->calculateTerritory();

	if( mainGamestate->grid ) {
		renderer->enableMultisample(false);
		// grid
		unsigned char rgba[] = {0, 0, 0, 255};
		//for(int x=1;x<mainGamestate->getMap()->getWidth();x++) {
		for(int x=start_x+1;x<=end_x;x++) {
			/*Vector3D pos0(x*xscale, 0.0f, 0*zscale);
			Vector3D pos1(x*xscale, 0.0f, mainGamestate->getMap()->getHeight()*zscale);*/
			Vector3D pos0(x*xscale, 0.0f, start_y*zscale);
			Vector3D pos1(x*xscale, 0.0f, (end_y+1)*zscale);
			renderer->draw(&pos0, &pos1, rgba);
		}
		//for(int y=1;y<mainGamestate->getMap()->getHeight();y++) {
		for(int y=start_y+1;y<=end_y;y++) {
			/*Vector3D pos0(0*xscale, 0.0f, y*zscale);
			Vector3D pos1(mainGamestate->getMap()->getWidth()*xscale, 0.0f, y*zscale);*/
			Vector3D pos0(start_x*xscale, 0.0f, y*zscale);
			Vector3D pos1((end_x+1)*xscale, 0.0f, y*zscale);
			renderer->draw(&pos0, &pos1, rgba);
		}
		renderer->enableMultisample(true);
	}

	// draw other map features
	//for(int cy=0;cy<mainGamestate->getMap()->getHeight();cy++) {
	//	for(int cx=0;cx<mainGamestate->getMap()->getWidth();cx++) {
	for(int cy=start_y;cy<=end_y;cy++) {
		for(int cx=start_x;cx<=end_x;cx++) {
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(cx, cy) ) {
				continue;
			}
			const unsigned char *rgba = NULL;
			if( mainGamestate->player !=NULL && !mainGamestate->player->isFogOfWarVisible(cx, cy) ) {
				rgba = rgba_fog;
			}
			else {
				rgba = rgba_white;
			}
			Pos2D pos(cx, cy);
			const MapSquare *square = mainGamestate->map->getSquare(cx, cy);
			//VI_Texture *map_texture = game->map->getSquare(cx, cy)->getTexture();
			//map_texture->draw(pos.x, pos.y + game->sq_height); // add sq_height due to texture coord location
			//map_texture->draw(game->sq_width * (x - frac_x), game->sq_height * ( y + 1 - frac_y ));
			//int overlay_index = ( cx % 31 + 7 * cy ) % 2;
			const int overlay_indices_lookup[] = { 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0 };
			const int n_overlay_indices_lookup_c = sizeof(overlay_indices_lookup) / sizeof(overlay_indices_lookup[0]);
			int base_cx = cx, base_cy = cy;
			mainGamestate->map->reduceToBase(&base_cx, &base_cy);
			int overlay_index = overlay_indices_lookup[( base_cx + base_cy ) % n_overlay_indices_lookup_c];
			VI_Texture *mapoverlay_texture = square->getOverlayTexture(overlay_index);
			if( mapoverlay_texture != NULL && !square->drawAboveRoad() ) {
				renderer->enableTexturing(true);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				mapoverlay_texture->enable();
				renderQuad(pos, true, rgba);
				renderer->enableTexturing(false);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			}

			// draw roads
			if( square->getRoad() != ROAD_NONE ) {
				int index = 0;
				for(int c=0;c<8;c++) {
					int dx = 0, dy = 0;
					getDir(&dx, &dy, c);
					int tx = cx + dx;
					int ty = cy + dy;
					//if( tx < 0 || tx >= mainGamestate->map->getWidth() || ty < 0 || ty >= mainGamestate->map->getHeight() ) {
					if( !mainGamestate->map->isValid(tx, ty) ) {
						continue;
					}
					if( mainGamestate->map->getSquare(tx, ty)->getRoad() != ROAD_NONE ) {
						int mask = 1 << c;
						index += mask;
					}
				}
				ASSERT( index < 256 );
				VI_Texture *texture = NULL;
				if( square->getRoad() == ROAD_BASIC ) {
					texture = MapSquare::getRoadTexture(index);
				}
				else if( square->getRoad() == ROAD_RAILWAYS ) {
					texture = MapSquare::getRailwayTexture(index);
				}
				else {
					ASSERT(false);
				}
				renderer->enableTexturing(true);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				texture->enable();
				renderQuad(pos, true, rgba);
				renderer->enableTexturing(false);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			}

			if( mapoverlay_texture != NULL && square->drawAboveRoad() ) {
				renderer->enableTexturing(true);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				mapoverlay_texture->enable();
				renderQuad(pos, false, rgba);
				renderer->enableTexturing(false);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			}

			const BonusResource *bonus_resource = square->getBonusResource();
			if( bonus_resource != NULL ) {
				/*bool bonus_ok = true;
				if( !SPY && mainGamestate->player != NULL && bonus_resource->getRequiresTechnology() != NULL ) {
					if( !mainGamestate->player->hasTechnology( bonus_resource->getRequiresTechnology() ) ) {
						bonus_ok = false;
					}
				}
				if( bonus_ok ) {*/
				if( SPY || mainGamestate->player == NULL || bonus_resource->canSee(mainGamestate->player) ) {
					renderer->enableTexturing(true);
					//renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
					renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
					bonus_resource->getTexture()->enable();
					renderQuad(pos.x+0.5f, (float)pos.y, 0.5f, 0.5f, true, rgba);
					renderer->enableTexturing(false);
					renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
				}
			}

			// targets / territory borders
			const Civilization *civilization = square->getTerritory();
			if( square->isTarget() ) {
				unsigned char target_rgba[] = {127, 0, 0, 127};
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				renderQuad(pos, true, target_rgba);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
				//g2d->drawRect(pos.x, pos.y, sq_width, sq_height);
			}
			else if( civilization != NULL ) {
				unsigned char civ_rgba[] = {0, 0, 0, 255};
				const unsigned char *civ_rgb = civilization->getColor();
				const float px = 1.0f / 128.0f;
				for(int i=0;i<3;i++)
					civ_rgba[i] = civ_rgb[i];
				if( mainGamestate->map->isValid(cx, cy-1) && mainGamestate->map->getSquare(cx, cy-1)->getTerritory() != civilization ) {
					// top
					Vector3D pos0(cx*xscale, 0.0f, cy*zscale);
					Vector3D pos1((cx+1)*xscale, 0.0f, cy*zscale);
					renderer->draw(&pos0, &pos1, civ_rgba);
				}
				if( mainGamestate->map->isValid(cx, cy+1) && mainGamestate->map->getSquare(cx, cy+1)->getTerritory() != civilization ) {
					// bottom
					Vector3D pos0(cx*xscale, 0.0f, (cy+1-px)*zscale);
					Vector3D pos1((cx+1)*xscale, 0.0f, (cy+1-px)*zscale);
					renderer->draw(&pos0, &pos1, civ_rgba);
				}
				if( mainGamestate->map->isValid(cx-1, cy) && mainGamestate->map->getSquare(cx-1, cy)->getTerritory() != civilization ) {
					// left
					Vector3D pos0(cx*xscale, 0.0f, cy*zscale);
					Vector3D pos1(cx*xscale, 0.0f, (cy+1)*zscale);
					renderer->draw(&pos0, &pos1, civ_rgba);
				}
				if( mainGamestate->map->isValid(cx+1, cy) && mainGamestate->map->getSquare(cx+1, cy)->getTerritory() != civilization ) {
					// right
					Vector3D pos0((cx+1-px)*xscale, 0.0f, cy*zscale);
					Vector3D pos1((cx+1-px)*xscale, 0.0f, (cy+1)*zscale);
					renderer->draw(&pos0, &pos1, civ_rgba);
				}
			}
		}
	}

	// Settler city square
	if( mainGamestate->active_unit != NULL && !mainGamestate->isGUILocked() && mainGamestate->active_unit->getTemplate()->canBuildCity() && mainGamestate->map->getSquare(mainGamestate->active_unit->getPos())->canBuildCity() ) {
		int time_ms = VI_getGameTimeMS() - mainGamestate->blink_time_start_ms;
		if( ((int)( time_ms / blink_speed_ms_c )) % 2 != 0 ) {
			//game->genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
			renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
			//renderer->setColor4(255, 255, 255, 31);
			unsigned char settler_square_rgba[] = {255, 255, 255, 31};
			for(int i=0;i<City::getNCitySquares();i++) {
				Pos2D pos = City::getCitySquare(mainGamestate->getMap(), i, mainGamestate->active_unit->getPos());
				mainGamestate->getMap()->reduceToBase(&pos.x, &pos.y, start_x, start_y);
				if( mainGamestate->map->isValid(pos.x, pos.y) ) {
					renderQuad(pos, true, settler_square_rgba);
				}
			}
			renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
		}
	}

	// initialise drawn flag
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		for(size_t i=0;i<civilization->getNUnits();i++) {
			Unit *unit = civilization->getUnit(i);
			unit->setMark(false);
		}
	}

	// draw cities
	renderer->enableTexturing(true);
	renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
	//VI_Texture *city_texture = mainGamestate->cityTexture;
	//VI_Texture *city_texture = mainGamestate->cityModernTexture;
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		VI_Texture *city_texture = NULL;
		if( civilization->getNCities() > 0 ) {
			int age = (int)civilization->getAge();
			city_texture = age >= (int)AGE_MODERN ? mainGamestate->cityModernTexture : mainGamestate->cityTexture;
			city_texture->enable();
		}
		for(size_t i=0;i<civilization->getNCities();i++) {
			const City *city = civilization->getCity(i);
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(city->getX(), city->getY()) ) {
				continue;
			}
			Pos2D pos = city->getPos();
			mainGamestate->getMap()->reduceToBase(&pos.x, &pos.y, start_x, start_y);
			if( pos.x < start_x || pos.x > end_x || pos.y < start_y || pos.y > end_y ) {
				continue;
			}
			const unsigned char *rgba = NULL;
			if( mainGamestate->player !=NULL && !mainGamestate->player->isFogOfWarVisible(pos.x, pos.y) ) {
				rgba = rgba_fog;
			}
			else {
				rgba = rgba_white;
			}
			renderQuad(pos, false, rgba);
			// text is drawn later, so that it appears on top

			const vector<Unit *> *units = mainGamestate->map->findUnitsAt(city->getX(), city->getY());
			for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter) {
				Unit *unit = *iter;
				if( unit != mainGamestate->active_unit ) {
					// don't draw non-active units that are located at a city
					unit->setMark(true);
				}
			}
		}
	}
	renderer->enableTexturing(false);
	renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);

	// draw units
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		const unsigned char *rgb = civilization->getColor();
		for(size_t i=0;i<civilization->getNUnits();i++) {
			Unit *unit = civilization->getUnit(i);
			if( unit->isMarked() ) {
				// either already drawn, or shouldn't draw
				continue;
			}
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(unit->getX(), unit->getY()) ) {
				continue;
			}
			if( mainGamestate->player != NULL && !mainGamestate->player->isFogOfWarVisible(unit->getX(), unit->getY()) ) {
				continue;
			}
			Pos2D pos = unit->getPos();
			mainGamestate->getMap()->reduceToBase(&pos.x, &pos.y, start_x, start_y);
			if( pos.x < start_x || pos.x > end_x || pos.y < start_y || pos.y > end_y ) {
				continue;
			}
			if( unit->isMoving() ) {
				continue;
			}

			// find other units at this square
			vector<Unit *> units;
			const vector<Unit *> *units_at = mainGamestate->getMap()->findUnitsAt(pos);
			for(vector<Unit *>::const_iterator iter = units_at->begin(); iter != units_at->end(); ++iter) {
				Unit *unit2 = *iter;
				// n.b., don't include units already marked (units at a city other than the active_unit)
				if( !unit2->isMarked() ) {
					units.push_back(unit2);
					unit2->setMark(true); // so we don't deal with this unit later on
				}
			}
			// either find active unit, moving unit, or unit with best defence if active or moving unit isn't at this square
			Unit *draw_unit = NULL;
			for(vector<Unit *>::const_iterator iter_unit = units.begin();iter_unit != units.end();++iter_unit) {
				Unit *unit2 = *iter_unit;
				if( unit2->isMoving() ) {
					// don't draw moving units here
				}
				else if( unit2 == mainGamestate->active_unit ) {
					draw_unit = unit2;
					break;
				}
				else if( draw_unit == NULL || unit2->getTemplate()->getDefence() > draw_unit->getTemplate()->getDefence()
					|| ( unit2->getTemplate()->getDefence() == draw_unit->getTemplate()->getDefence() && unit2->getTemplate()->getAttack() > draw_unit->getTemplate()->getAttack() )
					) {
						draw_unit = unit2;
				}
			}

			unit = draw_unit;
			int time_ms = VI_getGameTimeMS() - mainGamestate->blink_time_start_ms;
			if( unit == mainGamestate->active_unit && !mainGamestate->isGUILocked() && ((int)( time_ms / blink_speed_ms_c )) % 2 == 0 ) {
				continue;
			}
			// draw colour bar
			const float bar_frac = 0.25;
			unsigned char rgba[] = {rgb[0], rgb[1], rgb[2], 255};
			Vector3D bottom(pos.x*xscale, 0.0f, (pos.y+1.0f)*zscale);
			Vector3D top = bottom + mainGamestate->view_up*bar_frac;
			const float vertex_data[] = {
				bottom.x, bottom.y, bottom.z,
				bottom.x + xscale, bottom.y, bottom.z,
				top.x + xscale, top.y, top.z,
				top.x, top.y, top.z
			};
			renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, NULL, 4, rgba, true, 4);
			unsigned char rgba_black[] = {0, 0, 0, 255};
			renderer->render(VertexArray::DRAWINGMODE_LINE_LOOP, vertex_data, NULL, 4, rgba_black, true, 4);
			if( units.size() > 1 ) {
				// draw indicator for multiple units
				const float px = 1.0f/32.0f;
				float indicator_h = (units.size() - 1) * px;
				indicator_h = min(indicator_h, bar_frac - px);
				bottom = top + mainGamestate->view_up * px;
				top = bottom + mainGamestate->view_up * indicator_h;
				const float vertex_data[] = {
					bottom.x, bottom.y, bottom.z,
					bottom.x + xscale*px, bottom.y, bottom.z,
					top.x + xscale*px, top.y, top.z,
					top.x, top.y, top.z
				};
				unsigned char rgba_white[] = {255, 255, 255, 255};
				renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, NULL, 4, rgba_white, true, 4);
			}
			// draw unit texture
			renderer->enableTexturing(true);
			renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
			unit->getTemplate()->getTexture()->enable();
			renderQuad(pos, false);
			renderer->enableTexturing(false);
			renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
		}
	}

	// draw moving units
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		for(size_t i=0;i<civilization->getNUnits();i++) {
			Unit *unit = civilization->getUnit(i);
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(unit->getX(), unit->getY()) ) {
				continue;
			}
			if( unit->isMoving() ) {
				T_ASSERT( civilization == mainGamestate->player );
				float mx = 0.0f, my = 0.0f;
				unit->getMovingPosition(&mx, &my);
				mainGamestate->getMap()->reduceToBase(&mx, &my, (float)start_x, (float)start_y);
				renderer->enableTexturing(true);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				unit->getTemplate()->getTexture()->enable();
				renderQuad(mx, my, 1.0f, 1.0f, false);
				renderer->enableTexturing(false);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			}
		}
	}

	// draw smoke
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		if( civilization->getNCities() > 0 ) {
			// no smoke for modern cities
			int age = (int)civilization->getAge();
			if( age >= (int)AGE_MODERN )
				continue;
		}
		for(size_t i=0;i<civilization->getNCities();i++) {
			const City *city = civilization->getCity(i);
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(city->getX(), city->getY()) ) {
				continue;
			}
			Pos2D pos = city->getPos();
			mainGamestate->getMap()->reduceToBase(&pos.x, &pos.y, start_x, start_y);
			// N.B. Slightly modified test, so that smoke doesn't suddenly appear/disappear off the top or left hand side of the screen.
			if( pos.x < start_x-1 || pos.x > end_x || pos.y < start_y-1 || pos.y > end_y ) {
				continue;
			}

			//if( mainGamestate->scale_width >= 0.6f-eps && mainGamestate->scale_height >= 0.6f-eps )
			{
				renderer->enableTexturing(true);
				game_g->getGraphicsEnvironment()->render(mainGamestate->smokeEntity_3d, (pos.x + 0.5f)*xscale, 0.5f, (pos.y + 0.5f)*zscale);
				renderer->enableTexturing(false); // need to disable texturing again!
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE); // need to disable blending again!
				renderer->enableFixedFunctionLighting(false); // need to disable lighting again!
			}
		}
	}

	// draw effects
	int time_ms = VI_getGameTimeMS();
	//for(vector<Effect *>::iterator iter = mainGamestate->effects.begin(); iter != mainGamestate->effects.end(); ) {
	for(size_t i=0;i<mainGamestate->effects.size();) {
		Effect *effect = mainGamestate->effects.at(i);
		if( time_ms >= effect->time_e_ms ) {
			// delete effect
			delete effect;
			mainGamestate->effects.erase( mainGamestate->effects.begin() + i );
			continue;
		}
		else {
			if( mainGamestate->player == NULL || mainGamestate->player->isExplored(effect->xpos, effect->ypos) ) {
				Pos2D pos(effect->xpos, effect->ypos);
				renderer->enableTexturing(true);
				game_g->getGraphicsEnvironment()->render(effect->node_3d, (pos.x + 0.5f)*xscale, 0.5f, (pos.y + 0.5f)*zscale);
				renderer->enableTexturing(false); // need to disable texturing again!
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE); // need to disable blending again!
				renderer->enableFixedFunctionLighting(false); // need to disable lighting again!
			}
			i++;
		}
	}
}

void MainGamestate::paintPanel(VI_Graphics2D *g2d, void *data) {
	if( mainGamestate->map == NULL ) {
		return;
	}
	if( mainGamestate->view_3d ) {
		Vector3D pos[3];
		mainGamestate->getMapPosf(&pos[0].x, &pos[0].z, 0, 0);
		mainGamestate->getMapPosf(&pos[1].x, &pos[1].z, game_g->getGraphicsEnvironment()->getWidth()-1, 0);
		mainGamestate->getMapPosf(&pos[2].x, &pos[2].z, 0, game_g->getGraphicsEnvironment()->getHeight()-1);
		int start_x = (int)floor(pos[0].x);
		int start_y = (int)floor(pos[0].z);
		int end_x = (int)ceil(pos[1].x);
		int end_y = (int)ceil(pos[2].z);
		start_x = max(0, start_x);
		start_y = max(0, start_y);
		if( mainGamestate->map->getTopology() == TOPOLOGY_FLAT ) {
			end_x = min(mainGamestate->getMap()->getWidth()-1, end_x);
		}
		end_y = min(mainGamestate->getMap()->getHeight()-1, end_y);

		// draw city text
		for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
			Civilization *civilization = *iter;
			for(size_t i=0;i<civilization->getNCities();i++) {
				const City *city = civilization->getCity(i);
				if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(city->getX(), city->getY()) ) {
					continue;
				}
				Pos2D pos = city->getPos();
				mainGamestate->getMap()->reduceToBase(&pos.x, &pos.y, start_x, start_y);
				// N.B. Slightly modified test, so that text doesn't suddenly appear/disappear off the top or left hand side of the screen.
				if( pos.x < start_x-1 || pos.x > end_x || pos.y < start_y-1 || pos.y > end_y ) {
					continue;
				}

				Vector3D pos3d(pos.x*xscale, 0.0f, (pos.y + 1.0f)*zscale);
				Pos2D screen_pos;
				game_g->getGraphicsEnvironment()->worldToScreen(&screen_pos.x, &screen_pos.y, pos3d);
				if( screen_pos.x + 160 < 0 || screen_pos.x >= game_g->getGraphicsEnvironment()->getWidth() || screen_pos.y + 32 < 0 || screen_pos.y >= game_g->getGraphicsEnvironment()->getHeight() ) {
					continue;
				}
				//game_g->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y, "%s (%d)", city->getName(), city->getSize());
				//game_g->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y, "%s (%d)", city->getName(), city->getPopulation());
				game_g->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y, "%s (%s)", city->getName(), city->getPopulationString().c_str());
				//if( mainGamestate->scale_width >= 0.4f-eps && mainGamestate->scale_height >= 0.4f-eps )
				{
					game_g->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y + 16, "(%s)", city->getCivilization()->getName());
					if( SPY || city->getCivilization() == mainGamestate->player ) {
						if( city->getBuildable() != NULL ) {
							game_g->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y + 32, "%s (%d / %d)", city->getBuildable()->getName(), city->getProgressBuildable(), city->getBuildable()->getCost());
						}
					}
				}
			}
		}

		mainGamestate->drawGUIText(g2d);

		return;
	}

	if( mainGamestate->active_unit != NULL ) {
		Pos2D unit_pos = mainGamestate->active_unit->getPos();
		Pos2D screen_pos = mainGamestate->getScreenPos(unit_pos.x - 0.5f, unit_pos.y + 1.5f);
		mainGamestate->move1Button->setPosCentred(screen_pos.x, screen_pos.y);
		screen_pos = mainGamestate->getScreenPos(unit_pos.x + 0.5f, unit_pos.y + 1.5f);
		mainGamestate->move2Button->setPosCentred(screen_pos.x, screen_pos.y);
		screen_pos = mainGamestate->getScreenPos(unit_pos.x + 1.5f, unit_pos.y + 1.5f);
		mainGamestate->move3Button->setPosCentred(screen_pos.x, screen_pos.y);
		screen_pos = mainGamestate->getScreenPos(unit_pos.x - 0.5f, unit_pos.y + 0.5f);
		mainGamestate->move4Button->setPosCentred(screen_pos.x, screen_pos.y);
		screen_pos = mainGamestate->getScreenPos(unit_pos.x + 1.5f, unit_pos.y + 0.5f);
		mainGamestate->move6Button->setPosCentred(screen_pos.x, screen_pos.y);
		screen_pos = mainGamestate->getScreenPos(unit_pos.x - 0.5f, unit_pos.y - 0.5f);
		mainGamestate->move7Button->setPosCentred(screen_pos.x, screen_pos.y);
		screen_pos = mainGamestate->getScreenPos(unit_pos.x + 0.5f, unit_pos.y - 0.5f);
		mainGamestate->move8Button->setPosCentred(screen_pos.x, screen_pos.y);
		screen_pos = mainGamestate->getScreenPos(unit_pos.x + 1.5f, unit_pos.y - 0.5f);
		mainGamestate->move9Button->setPosCentred(screen_pos.x, screen_pos.y);
	}

	const float eps = 1.0e-5f;

	int sq_width = mainGamestate->getSqWidth();
	int sq_height = mainGamestate->getSqHeight();

	const int n_x = ( game_g->getGraphicsEnvironment()->getWidth() / sq_width ) + 2;
	const int n_y = ( game_g->getGraphicsEnvironment()->getHeight() / sq_height ) + 2;
	/*int off_x = game->map_pos.x;
	int off_y = game->map_pos.y;*/
	const int off_x = (int)mainGamestate->map_pos_x;
	const int off_y = (int)mainGamestate->map_pos_y;
	Renderer *renderer = game_g->getGraphicsEnvironment()->getRenderer();
	// draw map
	{
		/*float frac_x = game->map_pos_x - off_x;
		float frac_y = game->map_pos_y - off_y;*/
		// draw base map
	//#if 0
		float vertex_data[12] = {
			0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f
		};
		/*float texcoord_data[] = {
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f
		};*/
		float texcoord_data[8];
		//VI_Texture *splat_alpha = NULL;
		VI_Texture *splat_alpha = mainGamestate->map->getAlphamap();
		int splat_alpha_w = -1;
		int splat_alpha_h = -1;
		//bool texture_splatting = game_g->getSplatPixelShaderAmbient() != NULL && game_g->getSplatVertexShaderAmbient() != NULL;
		bool texture_splatting = game_g->getSplatShaderAmbient() != NULL;
		if( texture_splatting ) {
			/*game_g->getSplatVertexShaderAmbient()->enable();
			game_g->getSplatPixelShaderAmbient()->enable();*/
			game_g->getSplatShaderAmbient()->enable(renderer);

			//game_g->getSplatVertexShaderAmbient()->setMatrices(); // needed for vertex transformation to work
			game_g->getSplatShaderAmbient()->setMatrices("mx.ModelViewProj", "mx.ModelViewIT", "mx.ModelView"); // needed for vertex transformation to work

			splat_alpha_w = splat_alpha->getWidth();
			splat_alpha_h = splat_alpha->getHeight();
			/*game_g->getSplatPixelShaderAmbient()->SetUniformParameter1f("splat_detail_scale_w", (float)mainGamestate->map->getWidth());
			game_g->getSplatPixelShaderAmbient()->SetUniformParameter1f("splat_detail_scale_h", (float)mainGamestate->map->getHeight());*/
			game_g->getSplatShaderAmbient()->SetUniformParameter1f("splat_detail_scale_w", (float)mainGamestate->map->getWidth());
			game_g->getSplatShaderAmbient()->SetUniformParameter1f("splat_detail_scale_h", (float)mainGamestate->map->getHeight());
			int current_time_ms = VI_getGameTimeMS();
			//VI_log(">>> %d\n", current_time);
			game_g->getSplatShaderAmbient()->SetUniformParameter1f("time", ((float)current_time_ms)/1000.0f);
		}
		for(int y=0;y<n_y;y++) {
			int cy = off_y + y;
			if( cy >= mainGamestate->map->getHeight() ) {
				break;
			}
			for(int x=0;x<n_x;x++) {
				int cx = off_x + x;
				if( mainGamestate->map->getTopology() == TOPOLOGY_FLAT && cx >= mainGamestate->map->getWidth() ) {
					break;
				}
				if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(cx, cy) ) {
					// player may be NULL in test mode, in which case show everything
					continue;
				}
				Pos2D pos = mainGamestate->getScreenPos((float)cx, (float)cy);
				//VI_Texture *map_texture = game->map->getSquare(cx, cy)->getTexture();
				//map_texture->draw(pos.x, pos.y + game->sq_height); // add sq_height due to texture coord location
				vertex_data[0] = (float)pos.x;
				vertex_data[1] = (float)pos.y;
				vertex_data[3] = (float)pos.x;
				vertex_data[4] = (float)(pos.y + sq_height);
				vertex_data[6] = (float)(pos.x + sq_width);
				vertex_data[7] = (float)(pos.y + sq_height);
				vertex_data[9] = (float)(pos.x + sq_width);
				vertex_data[10] = (float)pos.y;
				texcoord_data[0] = (float)cx;
				texcoord_data[1] = (float)cy;
				texcoord_data[2] = (float)cx;
				texcoord_data[3] = (float)cy+1.0f;
				texcoord_data[4] = (float)cx+1.0f;
				texcoord_data[5] = (float)cy+1.0f;
				texcoord_data[6] = (float)cx+1.0f;
				texcoord_data[7] = (float)cy;
				for(int i=0;i<8 && texture_splatting;i+=2) {
					texcoord_data[i] /= (float)mainGamestate->map->getWidth();
				}
				for(int i=1;i<8 && texture_splatting;i+=2) {
					texcoord_data[i] /= (float)mainGamestate->map->getHeight();
				}
				renderer->enableTexturing(true);

				if( texture_splatting ) {
					//VI_Texture *map_texture = MapSquare::getTexture(TYPE_OCEAN);
					VI_Texture *map_texture = MapSquare::getTexture(TYPE_GRASSLAND);
					map_texture->enable(); // base texture
					renderer->activeTextureUnit(1);
					splat_alpha->enable(); // alpha texture

					renderer->activeTextureUnit(2);
					//map_texture = MapSquare::getTexture(TYPE_GRASSLAND);
					map_texture = MapSquare::getTexture(TYPE_OCEAN);
					map_texture->enable();

					renderer->activeTextureUnit(3);
					map_texture = MapSquare::getTexture(TYPE_DESERT);
					map_texture->enable();

					renderer->activeTextureUnit(4);
					map_texture = MapSquare::getTexture(TYPE_ARTIC);
					map_texture->enable();

					renderer->activeTextureUnit(0);
				}
				else {
					VI_Texture *map_texture = mainGamestate->map->getSquare(cx, cy)->getTexture();
					map_texture->enable();
				}
				const unsigned char *rgba = NULL;
				if( mainGamestate->player !=NULL && !mainGamestate->player->isFogOfWarVisible(cx, cy) ) {
					rgba = rgba_fog;
				}
				else {
					rgba = rgba_white;
				}
				//renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4);
				renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4, rgba, false, 4);
				renderer->enableTexturing(false);
			}
		}
		if( texture_splatting ) {
			/*game_g->getSplatPixelShaderAmbient()->disable();
			game_g->getSplatVertexShaderAmbient()->disable();*/
			//game_g->getSplatShaderAmbient()->disable(renderer);
			renderer->disableShaders();
			//delete splat_alpha;
		}
	}
//#endif
	// calculate territory (TODO: don't recalculate every frame!)
	//for(int iter=0;iter<10;iter++)
	{
		//mainGamestate->calculateTerritory();
	}

	if( mainGamestate->grid ) {
		// grid
		g2d->setColor3i(0, 0, 0);
		for(int x=1;x<n_x;x++) {
			int cx = off_x + x;
			if( mainGamestate->map->getTopology() == TOPOLOGY_FLAT && cx >= mainGamestate->map->getWidth() ) {
				break;
			}
			Pos2D pos = mainGamestate->getScreenPos((float)cx, 0.0f);
			g2d->draw(pos.x, 0, pos.x, game_g->getGraphicsEnvironment()->getHeight()-1);
		}
		for(int y=1;y<n_y;y++) {
			int cy = off_y + y;
			if( cy >= mainGamestate->map->getHeight() ) {
				break;
			}
			Pos2D pos = mainGamestate->getScreenPos(0.0f, (float)cy);
			g2d->draw(0, pos.y, game_g->getGraphicsEnvironment()->getWidth()-1, pos.y);
		}
	}

	// draw other map features
	for(int y=0;y<n_y;y++) {
		int cy = off_y + y;
		if( cy >= mainGamestate->map->getHeight() ) {
			break;
		}
		for(int x=0;x<n_x;x++) {
			int cx = off_x + x;
			if( mainGamestate->map->getTopology() == TOPOLOGY_FLAT && cx >= mainGamestate->map->getWidth() ) {
				break;
			}
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(cx, cy) ) {
				continue;
			}
			const unsigned char *rgba = NULL;
			if( mainGamestate->player !=NULL && !mainGamestate->player->isFogOfWarVisible(cx, cy) ) {
				rgba = rgba_fog;
			}
			else {
				rgba = rgba_white;
			}
			Pos2D pos = mainGamestate->getScreenPos((float)cx, (float)cy);
			const MapSquare *square = mainGamestate->map->getSquare(cx, cy);
			//VI_Texture *map_texture = game->map->getSquare(cx, cy)->getTexture();
			//map_texture->draw(pos.x, pos.y + game->sq_height); // add sq_height due to texture coord location
			//map_texture->draw(game->sq_width * (x - frac_x), game->sq_height * ( y + 1 - frac_y ));
			//int overlay_index = ( cx % 31 + 7 * cy ) % 2;
			const int overlay_indices_lookup[] = { 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0 };
			const int n_overlay_indices_lookup_c = sizeof(overlay_indices_lookup) / sizeof(overlay_indices_lookup[0]);
			int base_cx = cx, base_cy = cy;
			mainGamestate->map->reduceToBase(&base_cx, &base_cy);
			int overlay_index = overlay_indices_lookup[( base_cx + base_cy ) % n_overlay_indices_lookup_c];
			VI_Texture *mapoverlay_texture = square->getOverlayTexture(overlay_index);
			if( mapoverlay_texture != NULL && !square->drawAboveRoad() ) {
				//mapoverlay_texture->draw(pos.x, pos.y + sq_height, sq_width, sq_height);
				renderer->enableTexturing(true);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				mapoverlay_texture->enable();
				renderQuad2D(pos.x, pos.y, rgba);
				renderer->enableTexturing(false);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			}

			// draw roads
			if( square->getRoad() != ROAD_NONE ) {
				/*bool any = false;
				for(int c=0;c<8;c++) {
					int dx = 0, dy = 0;
					getDir(&dx, &dy, c);
					int tx = cx + dx;
					int ty = cy + dy;
					if( tx < 0 || tx >= game->map->getWidth() || ty < 0 || ty >= game->map->getHeight() ) {
						continue;
					}
					if( game->map->getSquare(tx, ty)->getRoad() != ROAD_NONE ) {
						MapSquare::getRoadTexture(c)->draw(pos.x, pos.y + game->sq_height);
						any = true;
					}
				}
				if( !any ) {
					MapSquare::getRoadTexture(8)->draw(pos.x, pos.y + game->sq_height);
				}*/
				int index = 0;
				for(int c=0;c<8;c++) {
					int dx = 0, dy = 0;
					getDir(&dx, &dy, c);
					int tx = cx + dx;
					int ty = cy + dy;
					//if( tx < 0 || tx >= mainGamestate->map->getWidth() || ty < 0 || ty >= mainGamestate->map->getHeight() ) {
					if( !mainGamestate->map->isValid(tx, ty) ) {
						continue;
					}
					if( mainGamestate->map->getSquare(tx, ty)->getRoad() != ROAD_NONE ) {
						int mask = 1 << c;
						index += mask;
					}
				}
				ASSERT( index < 256 );
				VI_Texture *road_texture = NULL;
				if( square->getRoad() == ROAD_BASIC ) {
					//MapSquare::getRoadTexture(index)->draw(pos.x, pos.y + sq_height, sq_width, sq_height);
					road_texture = MapSquare::getRoadTexture(index);
				}
				else if( square->getRoad() == ROAD_RAILWAYS ) {
					//MapSquare::getRailwayTexture(index)->draw(pos.x, pos.y + sq_height, sq_width, sq_height);
					road_texture = MapSquare::getRailwayTexture(index);
				}
				else {
					ASSERT(false);
				}

				renderer->enableTexturing(true);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				road_texture->enable();
				renderQuad2D(pos.x, pos.y, rgba);
				renderer->enableTexturing(false);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			}

			if( mapoverlay_texture != NULL && square->drawAboveRoad() ) {
				//mapoverlay_texture->draw(pos.x, pos.y + sq_height, sq_width, sq_height);
				renderer->enableTexturing(true);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
				mapoverlay_texture->enable();
				renderQuad2D(pos.x, pos.y, rgba);
				renderer->enableTexturing(false);
				renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			}
			const BonusResource *bonus_resource = square->getBonusResource();
			if( bonus_resource != NULL ) {
				/*bool bonus_ok = true;
				if( !SPY && mainGamestate->player != NULL && bonus_resource->getRequiresTechnology() != NULL ) {
					if( !mainGamestate->player->hasTechnology( bonus_resource->getRequiresTechnology() ) ) {
						bonus_ok = false;
					}
				}
				if( bonus_ok ) {*/
				if( SPY || mainGamestate->player == NULL || bonus_resource->canSee(mainGamestate->player) ) {
					renderer->enableTexturing(true);
					//renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
					renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
					bonus_resource->getTexture()->enable();
					renderQuad2D(pos.x+0.5f*sq_width, (float)pos.y, 0.5f, 0.5f, rgba);
					renderer->enableTexturing(false);
					renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
				}
			}

			/*if( game->grid ) {
				// grid
				if( x > 0 || y > 0 )
					g2d->setColor3i(0, 0, 0);
				if( x > 0 )
					g2d->draw(pos.x, pos.y, pos.x, pos.y + game->sq_height-1);
				if( y > 0 )
					g2d->draw(pos.x, pos.y, pos.x + game->sq_width-1, pos.y);
			}*/

			// targets / territory borders
			const Civilization *civilization = square->getTerritory();
			if( square->isTarget() ) {
				g2d->setColor3i(127, 0, 0);
				g2d->drawRect(pos.x, pos.y, sq_width, sq_height);
			}
			else if( civilization != NULL ) {
				const unsigned char *rgb = civilization->getColor();
				g2d->setColor3i(rgb[0], rgb[1], rgb[2]);
				if( mainGamestate->map->isValid(cx, cy-1) && mainGamestate->map->getSquare(cx, cy-1)->getTerritory() != civilization ) {
					g2d->draw(pos.x, pos.y, pos.x + sq_width-1, pos.y);
				}
				if( mainGamestate->map->isValid(cx, cy+1) && mainGamestate->map->getSquare(cx, cy+1)->getTerritory() != civilization ) {
					g2d->draw(pos.x, pos.y + sq_height-1, pos.x + sq_width-1, pos.y + sq_height-1);
				}
				if( mainGamestate->map->isValid(cx-1, cy) && mainGamestate->map->getSquare(cx-1, cy)->getTerritory() != civilization ) {
					g2d->draw(pos.x, pos.y, pos.x, pos.y + sq_height-1);
				}
				if( mainGamestate->map->isValid(cx+1, cy) && mainGamestate->map->getSquare(cx+1, cy)->getTerritory() != civilization ) {
					g2d->draw(pos.x + sq_width-1, pos.y, pos.x + sq_width-1, pos.y + sq_height-1);
				}
			}
		}
	}

	// Settler city square
	if( mainGamestate->active_unit != NULL && !mainGamestate->isGUILocked() && mainGamestate->active_unit->getTemplate()->canBuildCity() && mainGamestate->map->getSquare(mainGamestate->active_unit->getPos())->canBuildCity() ) {
		int time_ms = VI_getGameTimeMS() - mainGamestate->blink_time_start_ms;
		if( ((int)( time_ms / blink_speed_ms_c )) % 2 != 0 ) {
			//game->genv->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
			game_g->getGraphicsEnvironment()->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
			g2d->setColor4i(255, 255, 255, 31);
			for(int i=0;i<City::getNCitySquares();i++) {
				Pos2D pos = City::getCitySquare(mainGamestate->getMap(), i, mainGamestate->active_unit->getPos());
				if( mainGamestate->map->isValid(pos.x, pos.y) ) {
					Pos2D screen_pos = mainGamestate->getScreenPos((float)pos.x, (float)pos.y);
					g2d->fillRect(screen_pos.x, screen_pos.y, sq_width, sq_height);
				}
			}
			game_g->getGraphicsEnvironment()->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
		}
	}
	//return;

	// initialise drawn flag
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		for(size_t i=0;i<civilization->getNUnits();i++) {
			Unit *unit = civilization->getUnit(i);
			unit->setMark(false);
		}
	}

	// draw cities
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		VI_Texture *city_texture = NULL;
		if( civilization->getNCities() > 0 ) {
			int age = (int)civilization->getAge();
			city_texture = age >= (int)AGE_MODERN ? mainGamestate->cityModernTexture : mainGamestate->cityTexture;
		}
		for(size_t i=0;i<civilization->getNCities();i++) {
			const City *city = civilization->getCity(i);
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(city->getX(), city->getY()) ) {
				continue;
			}
			Pos2D pos = city->getPos();
			mainGamestate->getMap()->reduceToBase(&pos.x, &pos.y, off_x, off_y);
			if( pos.x < off_x || pos.x >= off_x + n_x || pos.y < off_y || pos.y >= off_y + n_y ) {
				continue;
			}
			const unsigned char *rgba = NULL;
			if( mainGamestate->player !=NULL && !mainGamestate->player->isFogOfWarVisible(pos.x, pos.y) ) {
				rgba = rgba_fog;
			}
			else {
				rgba = rgba_white;
			}
			Pos2D screen_pos = mainGamestate->getScreenPos((float)pos.x, (float)pos.y);
			//VI_Texture *city_texture = mainGamestate->cityTexture;
			//VI_Texture *city_texture = mainGamestate->cityModernTexture;
			//city_texture->draw(screen_pos.x, screen_pos.y + sq_height, sq_width, sq_height);
			renderer->enableTexturing(true);
			renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
			city_texture->enable();
			renderQuad2D(screen_pos.x, screen_pos.y, rgba);
			renderer->enableTexturing(false);
			renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			// text is drawn later, so that it appears on top

			const vector<Unit *> *units = mainGamestate->map->findUnitsAt(city->getX(), city->getY());
			for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter) {
				Unit *unit = *iter;
				if( unit != mainGamestate->active_unit ) {
					// don't draw non-active units that are located at a city
					unit->setMark(true);
				}
			}
		}
	}

	// draw units
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		const unsigned char *rgb = civilization->getColor();
		for(size_t i=0;i<civilization->getNUnits();i++) {
			Unit *unit = civilization->getUnit(i);
			if( unit->isMarked() ) {
				// either already drawn, or shouldn't draw
				continue;
			}
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(unit->getX(), unit->getY()) ) {
				continue;
			}
			if( mainGamestate->player != NULL && !mainGamestate->player->isFogOfWarVisible(unit->getX(), unit->getY()) ) {
				continue;
			}
			Pos2D pos = unit->getPos();
			mainGamestate->getMap()->reduceToBase(&pos.x, &pos.y, off_x, off_y);
			if( pos.x < off_x || pos.x >= off_x + n_x || pos.y < off_y || pos.y >= off_y + n_y ) {
				continue;
			}
			if( unit->isMoving() ) {
				continue;
			}

			// find other units at this square
			/*vector<Unit *> units;
			units.push_back(civilization->getUnit(i));
			for(int j=i+1;j<civilization->getNUnits();j++) {
				Unit *unit2 = civilization->getUnit(j);
				// n.b., don't include units already marked (units at a city other than the active_unit)
				if( !unit2->isMarked() && unit2->getPos() == unit->getPos() ) {
					units.push_back(unit2);
					unit2->setMark(true); // so we don't deal with this unit later on
				}
			}*/
			vector<Unit *> units;
			const vector<Unit *> *units_at = mainGamestate->getMap()->findUnitsAt(pos);
			for(vector<Unit *>::const_iterator iter = units_at->begin(); iter != units_at->end(); ++iter) {
				Unit *unit2 = *iter;
				// n.b., don't include units already marked (units at a city other than the active_unit)
				if( !unit2->isMarked() ) {
					units.push_back(unit2);
					unit2->setMark(true); // so we don't deal with this unit later on
				}
			}
			// either find active unit, moving unit, or unit with best defence if active or moving unit isn't at this square
			Unit *draw_unit = NULL;
			for(vector<Unit *>::const_iterator iter_unit = units.begin();iter_unit != units.end();++iter_unit) {
				Unit *unit2 = *iter_unit;
				if( unit2->isMoving() ) {
					// don't draw moving units here
				}
				else if( unit2 == mainGamestate->active_unit ) {
					draw_unit = unit2;
					break;
				}
				/*else if( unit2->isMoving() ) {
				draw_unit = unit2;
				break;
				}*/
				else if( draw_unit == NULL || unit2->getTemplate()->getDefence() > draw_unit->getTemplate()->getDefence()
					|| ( unit2->getTemplate()->getDefence() == draw_unit->getTemplate()->getDefence() && unit2->getTemplate()->getAttack() > draw_unit->getTemplate()->getAttack() )
					) {
						draw_unit = unit2;
				}
			}

			unit = draw_unit;
			Pos2D screen_pos = mainGamestate->getScreenPos((float)pos.x, (float)pos.y);
			int time_ms = VI_getGameTimeMS() - mainGamestate->blink_time_start_ms;
			if( unit == mainGamestate->active_unit && !mainGamestate->isGUILocked() && ((int)( time_ms / blink_speed_ms_c )) % 2 == 0 ) {
				continue;
			}
			//VI_log("%d units\n", units.size());
			// draw colour bar
			int rect_w = sq_width, rect_h = (int)(sq_height*0.25);
			rect_h = max(1, rect_h);
			int rect_x = screen_pos.x, rect_y = screen_pos.y + sq_height - 1 - rect_h;
			g2d->setColor3i(rgb[0], rgb[1], rgb[2]);
			g2d->fillRect(rect_x, rect_y, rect_w, rect_h);
			g2d->setColor3i(0, 0, 0);
			g2d->drawRect(rect_x, rect_y, rect_w, rect_h);
			if( units.size() > 1 ) {
				// draw indicator for multiple units
				g2d->setColor3i(255, 255, 255);
				size_t indicator_h = units.size() - 1;
				indicator_h = min(indicator_h, (size_t)(rect_y - screen_pos.y - 1)); // avoid going too high
				g2d->drawRect(screen_pos.x, rect_y - indicator_h - 1, 1, indicator_h);
			}
			// draw unit texture
			unit->getTemplate()->getTexture()->draw(screen_pos.x, screen_pos.y + sq_height, sq_width, sq_height);
			/*g2d->setColor3i(255, 255, 255);
			renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
			g2d->setTexture(unit->getTemplate()->getTexture());
			g2d->fillRect(pos.x, pos.y, mainGamestate->sq_width, mainGamestate->sq_height);
			g2d->setTexture(NULL);
			renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);*/
		}
	}

	// draw moving units
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		const unsigned char *rgb = civilization->getColor();
		for(size_t i=0;i<civilization->getNUnits();i++) {
			Unit *unit = civilization->getUnit(i);
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(unit->getX(), unit->getY()) ) {
				continue;
			}
			if( unit->isMoving() ) {
				T_ASSERT( civilization == mainGamestate->player );
				float mx = 0.0f, my = 0.0f;
				unit->getMovingPosition(&mx, &my);
				Pos2D pos = mainGamestate->getScreenPos(mx, my);
				/*int rect_w = game->sq_width, rect_h = 16;
				int rect_x = pos.x, rect_y = pos.y + game->sq_height - 1 - rect_h;
				g2d->setColor3i(rgb[0], rgb[1], rgb[2]);
				g2d->fillRect(rect_x, rect_y, rect_w, rect_h);
				g2d->setColor3i(0, 0, 0);
				g2d->drawRect(rect_x, rect_y, rect_w, rect_h);*/
				unit->getTemplate()->getTexture()->draw(pos.x, pos.y + sq_height, sq_width, sq_height);
			}
		}
	}

	// draw city text and smoke
	g2d->setColor3i(255, 255, 255);
	for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		//const unsigned char *rgb = civilization->getColor();
		//g2d->setColor3i(rgb[0], rgb[1], rgb[2]);
		int age = -1;
		if( civilization->getNCities() > 0 ) {
			age = (int)civilization->getAge();
		}
		for(size_t i=0;i<civilization->getNCities();i++) {
			const City *city = civilization->getCity(i);
			if( mainGamestate->player != NULL && !mainGamestate->player->isExplored(city->getX(), city->getY()) ) {
				continue;
			}
			Pos2D pos = city->getPos();
			mainGamestate->getMap()->reduceToBase(&pos.x, &pos.y, off_x, off_y);
			// N.B. Slightly modified test, so that text doesn't suddenly appear/disappear off the top or left hand side of the screen.
			if( pos.x < off_x-1 || pos.x >= off_x + n_x || pos.y < off_y-1 || pos.y >= off_y + n_y ) {
				continue;
			}
			Pos2D screen_pos = mainGamestate->getScreenPos((float)pos.x, (float)pos.y);

			//g2d->setColor3i(rgb[0], rgb[1], rgb[2]);
			//g2d->fillRect(screen_pos.x - 16, screen_pos.y + mainGamestate->sq_height, 16, 16);

			//g2d->setColor3i(255, 255, 255);
			//game_g->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y + sq_height, "%s (%d)", city->getName(), city->getSize());
			game_g->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y + sq_height, "%s (%s)", city->getName(), city->getPopulationString().c_str());
			//game->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y + game->sq_height + 16, "Size: %d", city->getSize());
			if( mainGamestate->scale_width >= 0.4f-eps && mainGamestate->scale_height >= 0.4f-eps ) {
				game_g->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y + sq_height + 16, "(%s)", city->getCivilization()->getName());
				if( SPY || city->getCivilization() == mainGamestate->player ) {
					if( city->getBuildable() != NULL ) {
						game_g->getFont()->writeTextExt(screen_pos.x - 0, screen_pos.y + sq_height + 32, "%s (%d / %d)", city->getBuildable()->getName(), city->getProgressBuildable(), city->getBuildable()->getCost());
					}
				}
			}

			if( age < (int)AGE_MODERN && mainGamestate->scale_width >= 0.6f-eps && mainGamestate->scale_height >= 0.6f-eps ) {
				game_g->getGraphicsEnvironment()->getRenderer()->enableTexturing(true); // need to enable texturing again (OpenGL)
				game_g->getGraphicsEnvironment()->render(mainGamestate->smokeEntity_2d, screen_pos.x + 0.5f*sq_width, screen_pos.y + 16.0f, 0.0f);
				//game->smokeTexture->draw(screen_pos.x + 0.5f*game->sq_width, screen_pos.y + 16.0f);
				game_g->getGraphicsEnvironment()->getRenderer()->enableTexturing(false); // need to disable texturing again!
				game_g->getGraphicsEnvironment()->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE); // need to disable blending again!
				game_g->getGraphicsEnvironment()->getRenderer()->enableFixedFunctionLighting(false); // need to disable lighting again!
				game_g->getGraphicsEnvironment()->getRenderer()->enableCullFace(false); // need to disable cull face again!
			}
		}
	}

	// draw effects
	int time_ms = VI_getGameTimeMS();
	//for(vector<Effect *>::iterator iter = mainGamestate->effects.begin(); iter != mainGamestate->effects.end(); ) {
	for(size_t i=0;i<mainGamestate->effects.size();) {
		Effect *effect = mainGamestate->effects.at(i);
		if( time_ms >= effect->time_e_ms ) {
			// delete effect
			delete effect;
			mainGamestate->effects.erase( mainGamestate->effects.begin() + i );
			continue;
		}
		else {
			if( mainGamestate->player == NULL || mainGamestate->player->isExplored(effect->xpos, effect->ypos) ) {
				Pos2D pos = mainGamestate->getScreenPos((float)effect->xpos, (float)effect->ypos);
				game_g->getGraphicsEnvironment()->getRenderer()->enableTexturing(true); // need to enable texturing again (OpenGL)
				game_g->getGraphicsEnvironment()->render(effect->node_2d, pos.x + 0.5f*sq_width, pos.y + 16.0f, 0.0f);
				game_g->getGraphicsEnvironment()->getRenderer()->enableTexturing(false); // need to disable texturing again!
				game_g->getGraphicsEnvironment()->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE); // need to disable blending again!
				game_g->getGraphicsEnvironment()->getRenderer()->enableFixedFunctionLighting(false); // need to disable lighting again!
			}
			i++;
		}
	}
//#endif
	mainGamestate->drawGUIText(g2d);
}

#if 0
void MainGamestate::handleMouseClickEvent(const VI_MouseClickEvent *mouse_click_event) {
	if( this->phase == PHASE_PLAYERTURN && mouse_click_event->right ) {
		// right click disables input mode
		this->setInputMode(INPUTMODE_NORMAL);
	}

	if( mouse_click_event->wheel_up ) {
		//this->action(this->zoomInButton);
		this->zoom(true);
	}
	else if( mouse_click_event->wheel_down ) {
		//this->action(this->zoomOutButton);
		this->zoom(false);
	}

	if( this->isGUILocked() ) {
		return;
	}
	for(size_t i=0;i<player->getNUnits();i++) {
		const Unit *unit = player->getUnit(i);
		if( unit->isMoving() ) {
			return;
		}
	}

	if( (mouse_click_event->left || mouse_click_event->right ) && this->phase == PHASE_PLAYERTURN && game_g->getGraphicsEnvironment()->isActive() ) {
		int m_x = mouse_click_event->mouse_x;
		int m_y = mouse_click_event->mouse_y;
		//ASSERT( !mainGUILocked );
		T_ASSERT( !this->isGUILocked() );
		Pos2D m_map_pos = this->getMapPos(m_x, m_y);
		map->reduceToBase(&m_map_pos.x, &m_map_pos.y); // important!
		VI_log("clicked %d, %d (%d, %d)\n", m_map_pos.x, m_map_pos.y, mouse_click_event->left, mouse_click_event->right);
		bool done_click = false;
		if( !map->isValid(m_map_pos.x, m_map_pos.y) )
			done_click = true;

		if( !done_click && inputMode == INPUTMODE_TRAVEL && mouse_click_event->left ) {
			ASSERT( active_unit != NULL );
			const MapSquare *square = map->getSquare(m_map_pos);
			//if( square->isLand() && !square->hasEnemies(player) ) {
			if( square->isTarget() ) {
				// need to call setInputMode after calling isTarget(), as setting the inputmode resets the targets
				// but we also call it now rather than later, so the crosshairs pointer isn't showing when we display request windows
				setInputMode(INPUTMODE_NORMAL);
				done_click = true;
				ASSERT( square->isLand() );
				//ASSERT( !square->hasForeignUnits(player) );
				bool travel_ok = true;
				const Civilization *enemy_civ = this->getMap()->findCivilizationAt(m_map_pos);
				if( enemy_civ == player )
					enemy_civ = NULL;
				else if( enemy_civ != NULL ) {
					const Relationship *relationship = this->findRelationship(enemy_civ, player);
					if( relationship->getStatus() != Relationship::STATUS_PEACE ) {
						enemy_civ = NULL;
					}
				}
				if( enemy_civ != NULL ) {
					// break alliance?
					if( !askDeclareWar(enemy_civ, false) ) {
						travel_ok = false;
					}
				}

				if( travel_ok ) {
					// also check for entering enemy territory
					// n.b., possible to require declaring war on two civs (if attacking an enemy, that's in another civ's territory!)
					//this->calculateTerritory();
					const Civilization *target_territory = this->getMap()->getSquare(m_map_pos)->getTerritory();
					if( target_territory != NULL && target_territory != enemy_civ && target_territory != player ) {
						// entering enemy territory - not allowed if at peace
						const Relationship *relationship = mainGamestate->findRelationship(target_territory, player);
						if( relationship->getStatus() == Relationship::STATUS_PEACE && !relationship->hasRightOfPassage() ) {
							// break alliance?
							if( !askDeclareWar(target_territory, true) ) {
								travel_ok = false;
							}
						}
					}
				}

				if( travel_ok ) {
					this->moveTo(active_unit, m_map_pos, true);
					if( active_unit != NULL ) {
						//active_unit->useMoves(); // now done in moveTo()
						active_unit = NULL;
					}
				}
			}
			else {
				setInputMode(INPUTMODE_NORMAL);
			}
		}
		else if( !done_click && inputMode == INPUTMODE_BOMBARD && mouse_click_event->left ) {
			ASSERT( active_unit != NULL );
			ASSERT( active_unit->getTemplate()->getBombard() > 0 || active_unit->getTemplate()->getNuclearType() != UnitTemplate::NUCLEARTYPE_NONE );
			const MapSquare *square = map->getSquare(m_map_pos);
			if( square->isTarget() ) {
				// need to call setInputMode after calling isTarget(), as setting the inputmode resets the targets
				// but we also call it now rather than later, so the crosshairs pointer isn't showing when we display request windows
				setInputMode(INPUTMODE_NORMAL);
				done_click = true;
				const City *city = square->getCity();
				ASSERT( city != NULL );
				const Relationship *relationship = this->findRelationship(city->getCivilization(), active_unit->getCivilization());
				bool bomb_ok = true;
				if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
					if( !askDeclareWar(city->getCivilization(), false) ) {
						bomb_ok = false;
					}
				}
				if( bomb_ok ) {
					//this->bomb(this->active_unit, square);
					this->bomb(this->active_unit, m_map_pos);
					if( active_unit != NULL && !active_unit->hasMovesLeft() ) {
						this->active_unit = NULL;
						updateUnitButtons();
					}
					else {
						centreUnit();
					}
				}
			}
			else {
				setInputMode(INPUTMODE_NORMAL);
			}
		}
		else if( !done_click && inputMode == INPUTMODE_RECONNAISSANCE && mouse_click_event->left ) {
			ASSERT( active_unit != NULL );
			ASSERT( active_unit->getTemplate()->isAir() );
			ASSERT( active_unit->getTemplate()->getAirRange() > 0 );
			const MapSquare *square = map->getSquare(m_map_pos);
			if( square->isTarget() ) {
				// need to call setInputMode after calling isTarget(), as setting the inputmode resets the targets
				// but we also call it now rather than later, so the crosshairs pointer isn't showing when we display request windows
				setInputMode(INPUTMODE_NORMAL);
				done_click = true;
				player->updateFogOfWar(m_map_pos);
				active_unit->useMove(1);
				if( !active_unit->hasMovesLeft() ) {
					this->active_unit = NULL;
					updateUnitButtons();
				}
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "Fog of war revealed by air reconnaissance.", game_g->getFont(), game_g->getPanelTexture(), infowindow_def_x_c, infowindow_def_y_c, 300, 128);
				window->doModal();
				delete window;
			}
			else {
				setInputMode(INPUTMODE_NORMAL);
			}
		}
		else if( !done_click && inputMode == INPUTMODE_GOTO && mouse_click_event->left ) {
			ASSERT( active_unit != NULL );
			ASSERT( !active_unit->getTemplate()->isAir() );
			ASSERT( !active_unit->getTemplate()->isSea() );
			ASSERT( !active_unit->isAutomated() );
			const MapSquare *square = map->getSquare(m_map_pos);
			if( square->isTarget() ) {
				// need to call setInputMode after calling isTarget(), as setting the inputmode resets the targets
				// but we also call it now rather than later, so the crosshairs pointer isn't showing when we display request windows
				setInputMode(INPUTMODE_NORMAL);
				done_click = true;
				active_unit->setGotoTarget(m_map_pos);
				this->active_unit->automate();
				Pos2D dummy;
				if( this->active_unit->hasGotoTarget(&dummy) ) { // only do this is we haven't reached destination yet
					this->cache_active_unit = this->active_unit; // so we can cope with additional turns of this unit
					this->active_unit = NULL;
					this->updateUnitButtons();
				}
				else if( !active_unit->hasMovesLeft() ) {
					// important - if we reached our destination straight away, but used up all the moves, this should no longer be the active unit!
					this->active_unit = NULL;
					this->updateUnitButtons();
				}

				if( cache_active_unit->isMoving() ) {
					// wait for smooth movement to finish!
					return;
				}
			}
			else {
				setInputMode(INPUTMODE_NORMAL);
			}
		}

		bool is_shift = VI_keydown(V_LSHIFT) || VI_keydown(V_RSHIFT);
		bool is_control = VI_keydown(V_LCONTROL) || VI_keydown(V_RCONTROL);
		if( !done_click && !is_shift && mouse_click_event->left ) {
			City *city = map->findCity(m_map_pos.x, m_map_pos.y);
			if( city != NULL ) {
				if( SPY || city->getCivilization() == player ) {
					VI_log("CITY!\n");
					if( !is_control )
						new CityWindow(this, city);
					else
						this->askBuildable(city);
					done_click = true;
				}
			}
		}
		if( !done_click && mouse_click_event->left && player->isExplored(m_map_pos.x, m_map_pos.y) ) {
			stringstream terrain_info;
			const MapSquare *square = map->getSquare(m_map_pos);
			terrain_info << "Terrain: " << square->getName();
			const BonusResource *bonus_resource = square->getBonusResource();
			if( bonus_resource != NULL ) {
				/*bool bonus_ok = true;
				if( !SPY && player != NULL && bonus_resource->getRequiresTechnology() != NULL ) {
					if( !player->hasTechnology( bonus_resource->getRequiresTechnology() ) ) {
						bonus_ok = false;
					}
				}
				if( bonus_ok ) {*/
				if( SPY || bonus_resource->canSee(player) ) {
					terrain_info << " (" << bonus_resource->getName() << ")";
				}
			}
			Road road = square->getRoad();
			if( road == ROAD_BASIC )
				terrain_info << " (Road)";
			else if( road == ROAD_RAILWAYS )
				terrain_info << " (Railways)";
			terrain_info << ".\n";
			if( /*CHEAT &&*/ square->getProgressRoad() > 0 ) {
				if( road == ROAD_NONE )
					terrain_info << "Road progress: ";
				else if( road == ROAD_BASIC )
					terrain_info << "Railways progress: ";
				else {
					T_ASSERT( false );
				}
				terrain_info << square->getProgressRoad() << " / " << square->getRoadCost() << ".\n";
			}
			terrain_info << "(" << m_map_pos.x << "," << m_map_pos.y << ")";
			bool fow_visible = player->isFogOfWarVisible(m_map_pos.x, m_map_pos.y);
			if( !fow_visible ) {
				terrain_info << "\n\n(Unable to see units here)";
			}
			City *city = map->findCity(m_map_pos.x, m_map_pos.y);
			if( is_shift && city->getCivilization() == player )
				city = NULL; // if shift clicking on player city, allow looking at the units
			const vector<Unit *> *units = map->findUnitsAt(m_map_pos.x, m_map_pos.y);
			// don't show units at enemy city
			if( city == NULL && fow_visible && units->size() > 0 ) {
				//const int window_h_c = 512;
				int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
				Unit *first_unit = units->at(0);
				//if( first_unit->getCivilization() == player )
				{
					done_click = true;
					Unit *selected_unit = NULL;
					enum SelectType {
						SELECT_NONE = 0,
						SELECT_ONE = 1,
						SELECT_ALL = 2,
						FORTIFY_ALL = 3
					};
					SelectType selectType = SELECT_NONE;
					const int window_width_c = 500;
					bool show_single_info_window = false;
					if( first_unit->getCivilization() == player )
						show_single_info_window = units->size() == 1 && ( first_unit == active_unit || !first_unit->hasMovesLeft() );
					else
						show_single_info_window = true;
					if( first_unit->getCivilization() == player && units->size() > 1 ) {
						// choose which unit
						string buttons[] = {"Activate Unit", "Activate All", "Fortify All", "Cancel"};
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), terrain_info.str().c_str(), game_g->getFont(), game_g->getPanelTexture(), buttons, 4, infowindow_def_x_c, infowindow_def_y_c, window_width_c, window_h_c);
						/*InfoWindow *window = NULL;
						if( units->size() > 1 ) {
						string buttons[] = {"Activate Unit", "Activate All", "Cancel"};
						window = new InfoWindow(this->getPanel(), "", buttons, 3, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, window_h_c);
						}
						else {
						string buttons[] = {"Okay"};
						window = new InfoWindow(this->getPanel(), "", buttons, 1, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, window_h_c);
						}*/
						VI_Panel *windowPanel = window->getPanel();

						vector<string> unit_names = this->getUnitNames(units, true);
						ASSERT( unit_names.size() > 0 );
						//VI_Listbox *listbox = VI_createListbox(window_width_c - 32, window_h_c - 96, &*unit_names.begin(), unit_names.size(), game_g->getFont());
						VI_Listbox *listbox = VI_createListbox(window_width_c - 32, window_h_c - 112, &*unit_names.begin(), unit_names.size(), game_g->getFont());
						initListbox(listbox);
						listbox->setHasInputFocus(true);
						//listbox->setForeground(1.0f, 1.0f, 1.0f);
						listbox->setActive(0);
						// find first unit that still has moves
						for(size_t i=0;i<units->size();i++) {
							Unit *unit = units->at(i);
							if( unit->hasMovesLeft() ) {
								listbox->setActive(i);
								break;
							}
						}
						windowPanel->addChildPanel(listbox, 16, 64);

						int res = window->doModal();
						/*if( units->size() == 1 ) {
						// keep with the selected unit
						}
						else*/ if( res == 0 ) {
							int active = listbox->getActive();
							ASSERT( active >= 0 && active < units->size() );
							selected_unit = units->at(active);
							selectType = SELECT_ONE;
						}
						else if( res == 1 ) {
							selected_unit = NULL;
							selectType = SELECT_ALL;
						}
						else if( res == 2 ) {
							selected_unit = NULL;
							selectType = FORTIFY_ALL;
						}
						else {
							selected_unit = NULL;
							selectType = SELECT_NONE;
						}
						delete window;
						//delete listbox;
					}
					else {
						bool activate = true;
						if( first_unit->getCivilization() == player && ( first_unit->getStatus() == Unit::STATUS_BUILDING_ROAD || first_unit->getStatus() == Unit::STATUS_BUILDING_RAILWAYS ) ) {
							// warn
							stringstream text;
							text << "This " << first_unit->getTemplate()->getName() << " unit is currently building " << (first_unit->getStatus() == Unit::STATUS_BUILDING_ROAD ? "a road" : "railways") << ". Are you sure you wish to wake it?";
							if( !InfoWindow::confirm(game_g->getGraphicsEnvironment(), this->getPanel(), game_g->getFont(), game_g->getPanelTexture(), text.str(), "Activate", "Cancel") ) {
								activate = false;
							}
						}
						if( activate ) {
							selected_unit = first_unit;
							selectType = SELECT_ONE;
						}
						else {
							selected_unit = NULL;
							selectType = SELECT_NONE;
							show_single_info_window = false;
						}
					}
					if( first_unit->getCivilization() != player ) {
						// nothing
					}
					else if( selectType == SELECT_ONE ) {
						ASSERT( selected_unit != NULL );
						if( selected_unit->hasMovesLeft() ) {
							this->activateUnit(selected_unit, false);
						}
						else {
							//selected_unit->setStatus(Unit::STATUS_NORMAL);
							this->wakeupUnit(selected_unit);
						}
					}
					else if( selectType == SELECT_ALL ) {
						bool found = false;
						for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end();++iter) {
							Unit *unit = *iter;
							if( !found && unit->hasMovesLeft() ) {
								// activate the first one we can
								found = true;
								this->activateUnit(unit, false);
							}
							else {
								// deal with the rest
								//unit->setStatus(Unit::STATUS_NORMAL);
								this->wakeupUnit(unit);
							}
						}
					}
					else if( selectType == FORTIFY_ALL ) {
						for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end();++iter) {
							Unit *unit = *iter;
							unit->setStatus(Unit::STATUS_FORTIFIED);
							if( unit == this->active_unit ) {
								this->active_unit = NULL;
								this->updateUnitButtons();
							}
						}
					}

					// now show info window if there was only one unit (we do afterwards, to allow for activation of workers), or if viewing enemy player
					if( ( first_unit->getCivilization() != player || units->size() == 1 ) && show_single_info_window ) {
						string buttons[] = {"Okay"};
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), terrain_info.str().c_str(), game_g->getFont(), game_g->getPanelTexture(), buttons, 1, infowindow_def_x_c, infowindow_def_y_c, window_width_c, window_h_c);
						VI_Panel *windowPanel = window->getPanel();

						vector<string> unit_names = this->getUnitNames(units, true);
						ASSERT( unit_names.size() > 0 );
						//VI_Listbox *listbox = VI_createListbox(window_width_c - 32, window_h_c - 96, &*unit_names.begin(), unit_names.size(), game_g->getFont());
						VI_Listbox *listbox = VI_createListbox(window_width_c - 32, window_h_c - 112, &*unit_names.begin(), unit_names.size(), game_g->getFont());
						initListbox(listbox);
						listbox->setHasInputFocus(true);
						//listbox->setForeground(1.0f, 1.0f, 1.0f);
						listbox->setActive(0);
						windowPanel->addChildPanel(listbox, 16, 64);

						window->doModal();
						delete window;
						//delete listbox;
					}

					/*bool made_active = false;
					for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end(); ++iter) {
					Unit *unit = *iter;
					if( unit->hasMovesLeft() ) {
					if( made_active ) {
					unit->setStatus(Unit::STATUS_NORMAL);
					// un-fortify the remainder
					}
					else {
					// make the first available unit active
					activateUnit(unit, false);
					made_active = true;
					}
					}
					}*/
				}
			}
			else {
				done_click = true;
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), terrain_info.str().c_str(), game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
			}
		}

		if( mouse_click_event->right && this->active_unit != NULL ) {
			// right click to move?
			bool request_move[8] = {false, false, false, false, false, false, false, false}; // start from numpad 1, then go clockwise
			T_ASSERT( this->phase == PHASE_PLAYERTURN );
			const Pos2D pos = this->active_unit->getPos();
			if( pos != m_map_pos ) {
				Pos2D diff = m_map_pos - pos;
				this->map->reduceDiff(&diff.x, &diff.y);
				if( abs(diff.x) <= 1 && abs(diff.y) <= 1 ) {
					VI_log("right click move requested: %d, %d\n", diff.x, diff.y);
					if( diff.x == -1 && diff.y == 1 )
						request_move[0] = true;
					else if( diff.x == -1 && diff.y == 0 )
						request_move[1] = true;
					else if( diff.x == -1 && diff.y == -1 )
						request_move[2] = true;
					else if( diff.x == 0 && diff.y == -1 )
						request_move[3] = true;
					else if( diff.x == 1 && diff.y == -1 )
						request_move[4] = true;
					else if( diff.x == 1 && diff.y == 0 )
						request_move[5] = true;
					else if( diff.x == 1 && diff.y == 1 )
						request_move[6] = true;
					else if( diff.x == 0 && diff.y == 1 )
						request_move[7] = true;
				}
			}
			mainGamestate->movePlayerActiveUnit(request_move);
		}
		//if( mainGUILocked ) {
		if( this->isGUILocked() ) {
			return;
		}
	}
}
#endif

void MainGamestate::handleMouseClickEvent(const VI_MouseClickEvent *mouse_click_event) {
	if( mouse_click_event->wheel_up ) {
		//this->action(this->zoomInButton);
		this->zoom(true);
	}
	else if( mouse_click_event->wheel_down ) {
		//this->action(this->zoomOutButton);
		this->zoom(false);
	}

	if( mouse_click_event->left ) {
		this->mouse_dragging = true;
		this->was_dragged = false;
		this->mouse_dragging_x = mouse_click_event->mouse_x;
		this->mouse_dragging_y = mouse_click_event->mouse_y;
		this->mouse_dragging_map_pos_x = this->map_pos_x;
		this->mouse_dragging_map_pos_y = this->map_pos_y;
		VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
		this->mouse_dragging_map_pos_3d = vp->getPosition();
	}
}

void MainGamestate::mouseClickEventFunc(VI_MouseClickEvent *mouse_click_event, void *data) {
	//VI_log("click\n");

	mainGamestate->handleMouseClickEvent(mouse_click_event);
}

void MainGamestate::handleMouseReleaseEvent(const VI_MouseReleaseEvent *mouse_release_event) {
	if( this->phase == PHASE_PLAYERTURN && mouse_release_event->right ) {
		// right click disables input mode
		this->setInputMode(INPUTMODE_NORMAL);
	}

	if( this->isGUILocked() ) {
		return;
	}
	for(size_t i=0;i<player->getNUnits();i++) {
		const Unit *unit = player->getUnit(i);
		if( unit->isMoving() ) {
			return;
		}
	}

	int m_x = mouse_release_event->mouse_x;
	int m_y = mouse_release_event->mouse_y;

	if( mouse_release_event->left && this->mouse_dragging ) {
		this->mouse_dragging = false;
		if( was_dragged ) {
			this->was_dragged = false;
			return;
		}
	}
	else if( mouse_release_event->left && !this->mouse_dragging ) {
		// only recognise a mouse release event if we also click on this panel (avoid bug where closing an InfoWindow immediately causes it appear again, because we pick up the "release" from the click to close thee window!)
		return;
	}

	if( (mouse_release_event->left || mouse_release_event->right ) && this->phase == PHASE_PLAYERTURN && game_g->getGraphicsEnvironment()->isActive() ) {
		//ASSERT( !mainGUILocked );
		T_ASSERT( !this->isGUILocked() );
		Pos2D m_map_pos = this->getMapPos(m_x, m_y);
		map->reduceToBase(&m_map_pos.x, &m_map_pos.y); // important!
		VI_log("clicked %d, %d (%d, %d)\n", m_map_pos.x, m_map_pos.y, mouse_release_event->left, mouse_release_event->right);
		bool done_click = false;
		if( !map->isValid(m_map_pos.x, m_map_pos.y) )
			done_click = true;

		if( !done_click && inputMode == INPUTMODE_TRAVEL && mouse_release_event->left ) {
			ASSERT( active_unit != NULL );
			const MapSquare *square = map->getSquare(m_map_pos);
			//if( square->isLand() && !square->hasEnemies(player) ) {
			if( square->isTarget() ) {
				// need to call setInputMode after calling isTarget(), as setting the inputmode resets the targets
				// but we also call it now rather than later, so the crosshairs pointer isn't showing when we display request windows
				setInputMode(INPUTMODE_NORMAL);
				done_click = true;
				ASSERT( square->isLand() );
				//ASSERT( !square->hasForeignUnits(player) );
				bool travel_ok = true;
				const Civilization *enemy_civ = this->getMap()->findCivilizationAt(m_map_pos);
				if( enemy_civ == player )
					enemy_civ = NULL;
				else if( enemy_civ != NULL ) {
					const Relationship *relationship = this->findRelationship(enemy_civ, player);
					if( relationship->getStatus() != Relationship::STATUS_PEACE ) {
						enemy_civ = NULL;
					}
				}
				if( enemy_civ != NULL ) {
					// break alliance?
					if( !askDeclareWar(enemy_civ, false) ) {
						travel_ok = false;
					}
				}

				if( travel_ok ) {
					// also check for entering enemy territory
					// n.b., possible to require declaring war on two civs (if attacking an enemy, that's in another civ's territory!)
					//this->calculateTerritory();
					const Civilization *target_territory = this->getMap()->getSquare(m_map_pos)->getTerritory();
					if( target_territory != NULL && target_territory != enemy_civ && target_territory != player ) {
						// entering enemy territory - not allowed if at peace
						const Relationship *relationship = mainGamestate->findRelationship(target_territory, player);
						if( relationship->getStatus() == Relationship::STATUS_PEACE && !relationship->hasRightOfPassage() ) {
							// break alliance?
							if( !askDeclareWar(target_territory, true) ) {
								travel_ok = false;
							}
						}
					}
				}

				if( travel_ok ) {
					this->moveTo(active_unit, m_map_pos, true);
					if( active_unit != NULL ) {
						//active_unit->useMoves(); // now done in moveTo()
						active_unit = NULL;
					}
				}
			}
			else {
				setInputMode(INPUTMODE_NORMAL);
			}
		}
		else if( !done_click && inputMode == INPUTMODE_BOMBARD && mouse_release_event->left ) {
			ASSERT( active_unit != NULL );
			ASSERT( active_unit->getTemplate()->getBombard() > 0 || active_unit->getTemplate()->getNuclearType() != UnitTemplate::NUCLEARTYPE_NONE );
			const MapSquare *square = map->getSquare(m_map_pos);
			if( square->isTarget() ) {
				// need to call setInputMode after calling isTarget(), as setting the inputmode resets the targets
				// but we also call it now rather than later, so the crosshairs pointer isn't showing when we display request windows
				setInputMode(INPUTMODE_NORMAL);
				done_click = true;
				const City *city = square->getCity();
				ASSERT( city != NULL );
				const Relationship *relationship = this->findRelationship(city->getCivilization(), active_unit->getCivilization());
				bool bomb_ok = true;
				if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
					if( !askDeclareWar(city->getCivilization(), false) ) {
						bomb_ok = false;
					}
				}
				if( bomb_ok ) {
					//this->bomb(this->active_unit, square);
					this->bomb(this->active_unit, m_map_pos);
					if( active_unit != NULL && !active_unit->hasMovesLeft() ) {
						this->active_unit = NULL;
						updateUnitButtons();
					}
					else {
						centreUnit();
					}
				}
			}
			else {
				setInputMode(INPUTMODE_NORMAL);
			}
		}
		else if( !done_click && inputMode == INPUTMODE_RECONNAISSANCE && mouse_release_event->left ) {
			ASSERT( active_unit != NULL );
			ASSERT( active_unit->getTemplate()->isAir() );
			ASSERT( active_unit->getTemplate()->getAirRange() > 0 );
			const MapSquare *square = map->getSquare(m_map_pos);
			if( square->isTarget() ) {
				// need to call setInputMode after calling isTarget(), as setting the inputmode resets the targets
				// but we also call it now rather than later, so the crosshairs pointer isn't showing when we display request windows
				setInputMode(INPUTMODE_NORMAL);
				done_click = true;
				player->updateFogOfWar(m_map_pos);
				active_unit->useMove(1);
				if( !active_unit->hasMovesLeft() ) {
					this->active_unit = NULL;
					updateUnitButtons();
				}
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "Fog of war revealed by air reconnaissance.", game_g->getFont(), game_g->getPanelTexture(), infowindow_def_x_c, infowindow_def_y_c, 300, 128);
				window->doModal();
				delete window;
			}
			else {
				setInputMode(INPUTMODE_NORMAL);
			}
		}
		else if( !done_click && inputMode == INPUTMODE_GOTO && mouse_release_event->left ) {
			ASSERT( active_unit != NULL );
			ASSERT( !active_unit->getTemplate()->isAir() );
			ASSERT( !active_unit->getTemplate()->isSea() );
			ASSERT( !active_unit->isAutomated() );
			const MapSquare *square = map->getSquare(m_map_pos);
			if( square->isTarget() ) {
				// need to call setInputMode after calling isTarget(), as setting the inputmode resets the targets
				// but we also call it now rather than later, so the crosshairs pointer isn't showing when we display request windows
				setInputMode(INPUTMODE_NORMAL);
				done_click = true;
				active_unit->setGotoTarget(m_map_pos);
				this->active_unit->automate();
				Pos2D dummy;
				if( this->active_unit->hasGotoTarget(&dummy) ) { // only do this is we haven't reached destination yet
					this->cache_active_unit = this->active_unit; // so we can cope with additional turns of this unit
					this->active_unit = NULL;
					this->updateUnitButtons();
				}
				else if( !active_unit->hasMovesLeft() ) {
					// important - if we reached our destination straight away, but used up all the moves, this should no longer be the active unit!
					this->active_unit = NULL;
					this->updateUnitButtons();
				}

				if( cache_active_unit->isMoving() ) {
					// wait for smooth movement to finish!
					return;
				}
			}
			else {
				setInputMode(INPUTMODE_NORMAL);
			}
		}

		bool is_shift = VI_keydown(V_LSHIFT) || VI_keydown(V_RSHIFT);
		bool is_control = VI_keydown(V_LCONTROL) || VI_keydown(V_RCONTROL);
		if( !done_click && !is_shift && mouse_release_event->left ) {
			City *city = map->findCity(m_map_pos.x, m_map_pos.y);
			if( city != NULL ) {
				if( SPY || city->getCivilization() == player ) {
					VI_log("CITY!\n");
					if( !is_control )
						new CityWindow(this, city);
					else
						this->askBuildable(city);
					done_click = true;
				}
			}
		}
		if( !done_click && mouse_release_event->left && player->isExplored(m_map_pos.x, m_map_pos.y) ) {
			stringstream terrain_info;
			const MapSquare *square = map->getSquare(m_map_pos);
			terrain_info << "Terrain: " << square->getName();
			const BonusResource *bonus_resource = square->getBonusResource();
			if( bonus_resource != NULL ) {
				/*bool bonus_ok = true;
				if( !SPY && player != NULL && bonus_resource->getRequiresTechnology() != NULL ) {
					if( !player->hasTechnology( bonus_resource->getRequiresTechnology() ) ) {
						bonus_ok = false;
					}
				}
				if( bonus_ok ) {*/
				if( SPY || bonus_resource->canSee(player) ) {
					terrain_info << " (" << bonus_resource->getName() << ")";
				}
			}
			Road road = square->getRoad();
			if( road == ROAD_BASIC )
				terrain_info << " (Road)";
			else if( road == ROAD_RAILWAYS )
				terrain_info << " (Railways)";
			terrain_info << ".\n";
			if( /*CHEAT &&*/ square->getProgressRoad() > 0 ) {
				if( road == ROAD_NONE )
					terrain_info << "Road progress: ";
				else if( road == ROAD_BASIC )
					terrain_info << "Railways progress: ";
				else {
					T_ASSERT( false );
				}
				terrain_info << square->getProgressRoad() << " / " << square->getRoadCost() << ".\n";
			}
			terrain_info << "(" << m_map_pos.x << "," << m_map_pos.y << ")";
			bool fow_visible = player->isFogOfWarVisible(m_map_pos.x, m_map_pos.y);
			if( !fow_visible ) {
				terrain_info << "\n\n(Unable to see units here)";
			}
			City *city = map->findCity(m_map_pos.x, m_map_pos.y);
			if( is_shift && city->getCivilization() == player )
				city = NULL; // if shift clicking on player city, allow looking at the units
			const vector<Unit *> *units = map->findUnitsAt(m_map_pos.x, m_map_pos.y);
			// don't show units at enemy city
			if( city == NULL && fow_visible && units->size() > 0 ) {
				//const int window_h_c = 512;
				int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
				Unit *first_unit = units->at(0);
				//if( first_unit->getCivilization() == player )
				{
					done_click = true;
					Unit *selected_unit = NULL;
					enum SelectType {
						SELECT_NONE = 0,
						SELECT_ONE = 1,
						SELECT_ALL = 2,
						FORTIFY_ALL = 3
					};
					SelectType selectType = SELECT_NONE;
					const int window_width_c = 500;
					bool show_single_info_window = false;
					if( first_unit->getCivilization() == player )
						show_single_info_window = units->size() == 1 && ( first_unit == active_unit || !first_unit->hasMovesLeft() );
					else
						show_single_info_window = true;
					if( first_unit->getCivilization() == player && units->size() > 1 ) {
						// choose which unit
						string buttons[] = {"Activate Unit", "Activate All", "Fortify All", "Cancel"};
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), terrain_info.str().c_str(), game_g->getFont(), game_g->getPanelTexture(), buttons, 4, infowindow_def_x_c, infowindow_def_y_c, window_width_c, window_h_c);
						/*InfoWindow *window = NULL;
						if( units->size() > 1 ) {
						string buttons[] = {"Activate Unit", "Activate All", "Cancel"};
						window = new InfoWindow(this->getPanel(), "", buttons, 3, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, window_h_c);
						}
						else {
						string buttons[] = {"Okay"};
						window = new InfoWindow(this->getPanel(), "", buttons, 1, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, window_h_c);
						}*/
						VI_Panel *windowPanel = window->getPanel();

						vector<string> unit_names = this->getUnitNames(units, true);
						ASSERT( unit_names.size() > 0 );
						//VI_Listbox *listbox = VI_createListbox(window_width_c - 32, window_h_c - 96, &*unit_names.begin(), unit_names.size(), game_g->getFont());
						VI_Listbox *listbox = VI_createListbox(window_width_c - 32, window_h_c - 112, &*unit_names.begin(), unit_names.size(), game_g->getFont());
						initListbox(listbox);
						listbox->setHasInputFocus(true);
						//listbox->setForeground(1.0f, 1.0f, 1.0f);
						listbox->setActive(0);
						// find first unit that still has moves
						for(size_t i=0;i<units->size();i++) {
							Unit *unit = units->at(i);
							if( unit->hasMovesLeft() ) {
								listbox->setActive(i);
								break;
							}
						}
						windowPanel->addChildPanel(listbox, 16, 64);

						int res = window->doModal();
						/*if( units->size() == 1 ) {
						// keep with the selected unit
						}
						else*/ if( res == 0 ) {
							int active = listbox->getActive();
							ASSERT( active >= 0 && active < units->size() );
							selected_unit = units->at(active);
							selectType = SELECT_ONE;
						}
						else if( res == 1 ) {
							selected_unit = NULL;
							selectType = SELECT_ALL;
						}
						else if( res == 2 ) {
							selected_unit = NULL;
							selectType = FORTIFY_ALL;
						}
						else {
							selected_unit = NULL;
							selectType = SELECT_NONE;
						}
						delete window;
						//delete listbox;
					}
					else {
						bool activate = true;
						if( first_unit->getCivilization() == player && ( first_unit->getStatus() == Unit::STATUS_BUILDING_ROAD || first_unit->getStatus() == Unit::STATUS_BUILDING_RAILWAYS ) ) {
							// warn
							stringstream text;
							text << "This " << first_unit->getTemplate()->getName() << " unit is currently building " << (first_unit->getStatus() == Unit::STATUS_BUILDING_ROAD ? "a road" : "railways") << ". Are you sure you wish to wake it?";
							if( !InfoWindow::confirm(game_g->getGraphicsEnvironment(), this->getPanel(), game_g->getFont(), game_g->getPanelTexture(), text.str(), "Activate", "Cancel") ) {
								activate = false;
							}
						}
						if( activate ) {
							selected_unit = first_unit;
							selectType = SELECT_ONE;
						}
						else {
							selected_unit = NULL;
							selectType = SELECT_NONE;
							show_single_info_window = false;
						}
					}
					if( first_unit->getCivilization() != player ) {
						// nothing
					}
					else if( selectType == SELECT_ONE ) {
						ASSERT( selected_unit != NULL );
						if( selected_unit->hasMovesLeft() ) {
							this->activateUnit(selected_unit, false);
						}
						else {
							//selected_unit->setStatus(Unit::STATUS_NORMAL);
							this->wakeupUnit(selected_unit);
						}
					}
					else if( selectType == SELECT_ALL ) {
						bool found = false;
						for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end();++iter) {
							Unit *unit = *iter;
							if( !found && unit->hasMovesLeft() ) {
								// activate the first one we can
								found = true;
								this->activateUnit(unit, false);
							}
							else {
								// deal with the rest
								//unit->setStatus(Unit::STATUS_NORMAL);
								this->wakeupUnit(unit);
							}
						}
					}
					else if( selectType == FORTIFY_ALL ) {
						for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end();++iter) {
							Unit *unit = *iter;
							unit->setStatus(Unit::STATUS_FORTIFIED);
							if( unit == this->active_unit ) {
								this->active_unit = NULL;
								this->updateUnitButtons();
							}
						}
					}

					// now show info window if there was only one unit (we do afterwards, to allow for activation of workers), or if viewing enemy player
					if( ( first_unit->getCivilization() != player || units->size() == 1 ) && show_single_info_window ) {
						string buttons[] = {"Okay"};
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), terrain_info.str().c_str(), game_g->getFont(), game_g->getPanelTexture(), buttons, 1, infowindow_def_x_c, infowindow_def_y_c, window_width_c, window_h_c);
						VI_Panel *windowPanel = window->getPanel();

						vector<string> unit_names = this->getUnitNames(units, true);
						ASSERT( unit_names.size() > 0 );
						//VI_Listbox *listbox = VI_createListbox(window_width_c - 32, window_h_c - 96, &*unit_names.begin(), unit_names.size(), game_g->getFont());
						VI_Listbox *listbox = VI_createListbox(window_width_c - 32, window_h_c - 112, &*unit_names.begin(), unit_names.size(), game_g->getFont());
						initListbox(listbox);
						listbox->setHasInputFocus(true);
						//listbox->setForeground(1.0f, 1.0f, 1.0f);
						listbox->setActive(0);
						windowPanel->addChildPanel(listbox, 16, 64);

						window->doModal();
						delete window;
						//delete listbox;
					}

					/*bool made_active = false;
					for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end(); ++iter) {
					Unit *unit = *iter;
					if( unit->hasMovesLeft() ) {
					if( made_active ) {
					unit->setStatus(Unit::STATUS_NORMAL);
					// un-fortify the remainder
					}
					else {
					// make the first available unit active
					activateUnit(unit, false);
					made_active = true;
					}
					}
					}*/
				}
			}
			else {
				done_click = true;
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), terrain_info.str().c_str(), game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
			}
		}

		if( mouse_release_event->right && this->active_unit != NULL ) {
			// right click to move?
			bool request_move[8] = {false, false, false, false, false, false, false, false}; // start from numpad 1, then go clockwise
			T_ASSERT( this->phase == PHASE_PLAYERTURN );
			const Pos2D pos = this->active_unit->getPos();
			if( pos != m_map_pos ) {
				Pos2D diff = m_map_pos - pos;
				this->map->reduceDiff(&diff.x, &diff.y);
				if( abs(diff.x) <= 1 && abs(diff.y) <= 1 ) {
					VI_log("right click move requested: %d, %d\n", diff.x, diff.y);
					if( diff.x == -1 && diff.y == 1 )
						request_move[0] = true;
					else if( diff.x == -1 && diff.y == 0 )
						request_move[1] = true;
					else if( diff.x == -1 && diff.y == -1 )
						request_move[2] = true;
					else if( diff.x == 0 && diff.y == -1 )
						request_move[3] = true;
					else if( diff.x == 1 && diff.y == -1 )
						request_move[4] = true;
					else if( diff.x == 1 && diff.y == 0 )
						request_move[5] = true;
					else if( diff.x == 1 && diff.y == 1 )
						request_move[6] = true;
					else if( diff.x == 0 && diff.y == 1 )
						request_move[7] = true;
				}
			}
			mainGamestate->movePlayerActiveUnit(request_move);
		}
		//if( mainGUILocked ) {
		if( this->isGUILocked() ) {
			return;
		}
	}
}

void MainGamestate::mouseReleaseEventFunc(VI_MouseReleaseEvent *mouse_release_event, void *data) {
	//VI_log("release\n");
	mainGamestate->handleMouseReleaseEvent(mouse_release_event);
}

void MainGamestate::drawGUIText(VI_Graphics2D *g2d) const {
	float fps = game_g->getGraphicsEnvironment()->getFPS();
	/*g2d->setColor3i(255, 255, 255);
	game_g->getFont()->writeTextExt(120, 16, "%.3f", fps);
	return; // TEST
	*/
	int xpos = 16;
	//int ypos = 16;
	int ypos = 8;
	//const int ydiff = 16;
	const int ydiff = game_g->getFont()->getHeight();
	const int ydiff_small = game_g->getSmallFont()->getHeight();
	//const int max_units_c = 10;
	const int max_units_c = 8;
	const int alpha_c = 95;
	//const int button_diff_c = 32;
	const int button_diff_c = 40;
	if( this->player != NULL ) {
		//int total_y_size = 8 * ydiff + 32;
		int total_y_size = 7 * ydiff + button_diff_c;
		game_g->getGraphicsEnvironment()->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
		g2d->setColor4i(0, 0, 0, alpha_c);
		g2d->fillRect(xpos, ypos, 300, total_y_size);
		game_g->getGraphicsEnvironment()->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
		g2d->setColor4i(255, 255, 255, 255);
		game_g->getFont()->writeTextExt(xpos, ypos, "%s, Year %d", this->player->getName(), this->year); ypos += ydiff;
	}
	else {
		ypos += ydiff;
	}

	g2d->setColor3i(255, 255, 255);
	//game_g->getFont()->writeTextExt(120, 16, "%.3f", fps);
	game_g->getFont()->writeTextExt(8, game_g->getGraphicsEnvironment()->getHeight() - ydiff, "%.3f", fps);
	game_g->getFont()->writeTextExt(game_g->getGraphicsEnvironment()->getWidth() - 48, game_g->getGraphicsEnvironment()->getHeight() - ydiff, "v%d.%d", versionMajor, versionMinor);
	if( this->player != NULL ) {
		//game_g->getFont()->writeTextExt(xpos, ypos, "Year: %d", this->year); ypos += ydiff;
		//game_g->getFont()->writeTextExt(xpos, ypos, "Population: %d", this->player->getTotalPopulation()); ypos += ydiff; // n.b., testing suggests time taken for getTotalPopulation() is neglible, so fine to call every frame
		game_g->getFont()->writeTextExt(xpos, ypos, "Population: %s", this->player->getTotalPopulationString().c_str()); ypos += ydiff; // n.b., testing suggests time taken for getTotalPopulation() is neglible, so fine to call every frame
		game_g->getFont()->writeTextExt(xpos, ypos, "Power: %d", this->player->getPower()); ypos += ydiff;
		const Technology *player_technology = this->player->getTechnology();
		if( player_technology != NULL ) {
			game_g->getFont()->writeTextExt(xpos, ypos, "Researching: %s (%d / %d)", player_technology->getName(), this->player->getProgressTechnology(), player_technology->getCost(this));
		}
		ypos += ydiff;
	}
	else {
		ypos += ydiff;
		ypos += ydiff;
		ypos += ydiff;
	}

	if( this->active_unit != NULL ) {
		ypos += button_diff_c;
		T_ASSERT( this->phase == PHASE_PLAYERTURN );
		const UnitTemplate *unit_template = this->getActiveUnit()->getTemplate();
		string unit_info = getUnitStatsString(unit_template, false);
		game_g->getFont()->writeTextExt(xpos, ypos, "Unit: %s %s %s", this->getActiveUnit()->isVeteran() ? "Veteran " : "", unit_template->getName(), unit_info.c_str()); ypos += ydiff;
		Rational moves_left = this->getActiveUnit()->movesLeft();
		int moves_left_integer, moves_left_num, moves_left_den;
		moves_left.value(&moves_left_integer, &moves_left_num, &moves_left_den);
		T_ASSERT( moves_left_num == 0 || moves_left_den == road_move_scale_c );
		if( moves_left_num == 0 ) {
			game_g->getFont()->writeTextExt(xpos, ypos, "Moves: %d out of %d", moves_left_integer, unit_template->getMoves()); ypos += ydiff;
		}
		else if( moves_left_integer == 0 ) {
			game_g->getFont()->writeTextExt(xpos, ypos, "Moves: %d/%d out of %d", moves_left_num, moves_left_den, unit_template->getMoves()); ypos += ydiff;
		}
		else {
			game_g->getFont()->writeTextExt(xpos, ypos, "Moves: %d %d/%d out of %d", moves_left_integer, moves_left_num, moves_left_den, unit_template->getMoves()); ypos += ydiff;
		}
		game_g->getFont()->writeTextExt(xpos, ypos, "%s", this->map->getSquare(this->getActiveUnit()->getX(), this->getActiveUnit()->getY())->getName()); ypos += ydiff;
		ypos += ydiff;

		const vector<Unit *> *units = this->map->findUnitsAt(this->getActiveUnit()->getX(), this->getActiveUnit()->getY());
		if( units->size() > 1 ) {
			size_t n_units = units->size() - 1;
			if( n_units > max_units_c ) {
				n_units = max_units_c + 1;
			}
			//int total_y_size = (n_units+1) * ydiff;
			int total_y_size = (n_units+1) * ydiff_small;
			game_g->getGraphicsEnvironment()->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
			g2d->setColor4i(0, 0, 0, alpha_c);
			//g2d->fillRect(xpos, ypos, 500, total_y_size);
			g2d->fillRect(xpos, ypos, 416, total_y_size);
			game_g->getGraphicsEnvironment()->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
			g2d->setColor4i(255, 255, 255, 255);

			//game_g->getFont()->writeText(xpos, ypos, "Other Units:"); ypos += ydiff;
			game_g->getSmallFont()->writeText(xpos, ypos, "Other Units:"); ypos += ydiff_small;
		}
		int count = 0;
		for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter ) {
			const Unit *unit = *iter;
			if( unit != this->getActiveUnit() ) {
				if( count == max_units_c ) {
					//game_g->getFont()->writeText(xpos, ypos, "more..."); ypos += ydiff;
					game_g->getSmallFont()->writeText(xpos, ypos, "more..."); ypos += ydiff_small;
					break;
				}
				string unit_name = this->getUnitName(unit, false);
				//game_g->getFont()->writeText(xpos, ypos, unit_name.c_str(), unit_name.length()); ypos += ydiff;
				game_g->getSmallFont()->writeText(xpos, ypos, unit_name.c_str(), unit_name.length()); ypos += ydiff_small;
				count++;
			}
		}
	}
	//else if( game->phase != PHASE_PLAYERTURN && !game->mainGUILocked ) {
	else if( this->phase != PHASE_PLAYERTURN && !this->isGUILocked() ) {
		int time_ms = VI_getGameTimeMS() - this->time_endplayerturn_s_ms;
		//VI_log("%d - %d = %d; / %d = %d\n", VI_getGameTimeMS(), this->time_endplayerturn_s_ms, time_ms, blink_speed_ms_c, (int)( time_ms / blink_speed_ms_c ));
		if( ((int)( time_ms / blink_speed_ms_c )) % 2 == 0 ) {
			game_g->getFont()->writeText(xpos, ypos, "Thinking...");
		}
		ypos += ydiff;
	}
	//else if( !this->moved_units_this_turn && this->phase == PHASE_PLAYERTURN && !this->isGUILocked() ) {
	else if( this->active_unit == NULL && !this->moved_units_this_turn && this->phase == PHASE_PLAYERTURN && !this->isGUILocked() && mainGamestate->endturnButton->isVisible() ) {
		// endturnButton check is so that this doesn't display when moving automated units
		// There's no active unit, we haven't moved units, we're in the player turn phase, and GUI isn't locked...
		ypos += 32;
		game_g->getFont()->writeText(xpos, ypos, "Press RETURN or click End Turn"); ypos += ydiff;
	}

	/*if( game->phase != PHASE_PLAYERTURN ) {
	game->endturnButton->setVisible(false);
	}
	else {
	game->endturnButton->setVisible(true);
	}*/
	/*bool buttons_visible = game->phase == PHASE_PLAYERTURN;
	game->endturnButton->setVisible(buttons_visible);
	game->newgameButton->setVisible(buttons_visible);
	game->loadgameButton->setVisible(buttons_visible);
	game->savegameButton->setVisible(buttons_visible);
	game->citiesAdvisorButton->setVisible(buttons_visible);
	game->unitsAdvisorButton->setVisible(buttons_visible);
	game->civsAdvisorButton->setVisible(buttons_visible);
	game->techsAdvisorButton->setVisible(buttons_visible);*/
	/*int poly_count = 0, vertex_count = 0, distinct_vertex_count = 0, render_calls_count = 0;
	game->genv->getFrameStats(&poly_count, &vertex_count, &distinct_vertex_count, &render_calls_count);
	game->font->writeTextExt(140,32,"POLYS: %d",poly_count);
	game->font->writeTextExt(380,32,"CALLS: %d",render_calls_count);*/
}

void MainGamestate::action(VI_Panel *source) {
	// always reset to normal input mode
	mainGamestate->setInputMode(INPUTMODE_NORMAL);
	/*if( game->mainGUILocked ) {
	return;
	}*/
	if( source == mainGamestate->endturnButton ) {
		//game->endturn();
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->endPlayerTurn();
	}
	else if( source == mainGamestate->returnHiddenButton ) {
		if( mainGamestate->active_unit == NULL && !mainGamestate->moved_units_this_turn && mainGamestate->endturnButton->isVisible() ) {
			mainGamestate->endPlayerTurn();
		}
	}
	else if( source == mainGamestate->newgameButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->newGame();
	}
	else if( source == mainGamestate->loadgameButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->loadGame();
	}
	else if( source == mainGamestate->savegameButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->saveGame();
	}
	else if( source == mainGamestate->savemapButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->saveMap();
	}
	else if( source == mainGamestate->quitgameButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->quitGame();
	}
	else if( source == mainGamestate->citiesAdvisorButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->citiesAdvisor();
	}
	else if( source == mainGamestate->unitsAdvisorButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->unitsAdvisor();
	}
	else if( source == mainGamestate->civsAdvisorButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->civsAdvisor();
	}
	else if( source == mainGamestate->techsAdvisorButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->techsAdvisor();
	}
	else if( source == mainGamestate->scoresAdvisorButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->scoresAdvisor();
	}
	else if( source == mainGamestate->mapButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->showMap();
	}
	else if( source == mainGamestate->gridButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->grid = !mainGamestate->grid;
	}
	else if( source == mainGamestate->zoomOutButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->zoom(false);
	}
	else if( source == mainGamestate->zoomResetButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		//Pos2D pos = mainGamestate->getMapPos(game_g->getGraphicsEnvironment()->getWidth()/2, game_g->getGraphicsEnvironment()->getHeight()/2);
		float mx = 0.0, my = 0.0;
		mainGamestate->getMapPosf(&mx, &my, game_g->getGraphicsEnvironment()->getWidth()/2, game_g->getGraphicsEnvironment()->getHeight()/2);
		if( mainGamestate->view_3d ) {
			/*VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
			Vector3D pos = vp->getPosition();
			pos.y = def_vp_height_c;
			vp->setPosition(&pos);*/
			float preferred_width = def_view3d_scale * game_g->getGraphicsEnvironment()->getWidth() / mainGamestate->def_sq_width;
			mainGamestate->reset3DZoom(preferred_width);
		}
		else {
			mainGamestate->scale_width = 1.0f;
			mainGamestate->scale_height = 1.0f;
		}
		//mainGamestate->centre(pos);
		mainGamestate->centre(mx, my);
	}
	else if( source == mainGamestate->zoomInButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->zoom(true);
	}
	else if( source == mainGamestate->viewButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->switchView();
	}
	else if( source == mainGamestate->helpButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->help();
	}
	else if( source == mainGamestate->waitButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		ASSERT( mainGamestate->active_unit != NULL );
		mainGamestate->active_unit = NULL;
		mainGamestate->updateUnitButtons();
	}
	else if( source == mainGamestate->skipTurnButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		ASSERT( mainGamestate->active_unit != NULL );
		mainGamestate->active_unit->useMoves();
		mainGamestate->active_unit = NULL;
		mainGamestate->updateUnitButtons();
	}
	else if( source == mainGamestate->fortifyButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		ASSERT( mainGamestate->active_unit != NULL );
		mainGamestate->active_unit->setStatus(Unit::STATUS_FORTIFIED);
		mainGamestate->active_unit = NULL;
		mainGamestate->updateUnitButtons();
	}
	else if( source == mainGamestate->buildCityButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		ASSERT( mainGamestate->active_unit != NULL );
		ASSERT( mainGamestate->active_unit->getTemplate()->canBuildCity() );
		//ASSERT( game->map->canBuildCity(game->active_unit->getX(), game->active_unit->getY()) );
		ASSERT( mainGamestate->map->getSquare(mainGamestate->active_unit->getX(), mainGamestate->active_unit->getY())->canBuildCity() );
		// build city
		string city_name = mainGamestate->active_unit->getCivilization()->makeCityName();

		// get city name
		string buttons[] = {"Build city", "Cancel"};
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, 2, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, 128);
		VI_Panel *windowPanel = window->getPanel();

		VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), infowindow_def_w_c - 32, 32);
		textfield->addText("Build new city:");
		windowPanel->addChildPanel(textfield, 16, 16);

		VI_Stringgadget *stringgadget = VI_createStringgadget(game_g->getFont(), infowindow_def_w_c - 32);
		stringgadget->addText(city_name.c_str());
		stringgadget->setReadOnly(false);
		stringgadget->setMaxLen(64);
		windowPanel->addChildPanel(stringgadget, 16, 64);

		//game_g->getGraphicsEnvironment()->flushInput(); // so we don't get an "B" keypress...
		int res = window->doModal();
		if( res == 0 ) {
			mainGamestate->active_unit->getCivilization()->advanceCityName();
			City *city = new City(mainGamestate, mainGamestate->active_unit->getCivilization(), stringgadget->getText(), mainGamestate->active_unit->getX(), mainGamestate->active_unit->getY());
			delete mainGamestate->active_unit;
			mainGamestate->active_unit = NULL;
			mainGamestate->updateUnitButtons();
			new CityWindow(mainGamestate, city);
		}

		delete window;
		//delete textfield;
		//delete stringgadget;
	}
	else if( source == mainGamestate->buildRoadButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		ASSERT( mainGamestate->active_unit != NULL );
		ASSERT( mainGamestate->active_unit->getTemplate()->canBuildRoads() );
		ASSERT( mainGamestate->map->getSquare(mainGamestate->active_unit->getX(), mainGamestate->active_unit->getY())->getRoad() == ROAD_NONE );
		ASSERT( mainGamestate->map->getSquare(mainGamestate->active_unit->getX(), mainGamestate->active_unit->getY())->canBeImproved( mainGamestate->active_unit->getCivilization() ) );
		// build road
		//square->setRoad(ROAD_BASIC);
		//active_unit->useMoves();
		//active_unit = NULL;
		mainGamestate->playSound(SOUND_WORKER);
		mainGamestate->active_unit->setStatus(Unit::STATUS_BUILDING_ROAD);
		mainGamestate->active_unit->update();
		mainGamestate->active_unit = NULL;
		mainGamestate->updateUnitButtons();
	}
	else if( source == mainGamestate->buildRailwaysButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		ASSERT( mainGamestate->active_unit != NULL );
		ASSERT( mainGamestate->active_unit->getTemplate()->canBuildRoads() );
		ASSERT( mainGamestate->map->getSquare(mainGamestate->active_unit->getX(), mainGamestate->active_unit->getY())->getRoad() == ROAD_BASIC );
		ASSERT( mainGamestate->map->getSquare(mainGamestate->active_unit->getX(), mainGamestate->active_unit->getY())->canBeImproved( mainGamestate->active_unit->getCivilization() ) );
		// build railways
		mainGamestate->playSound(SOUND_WORKER);
		mainGamestate->active_unit->setStatus(Unit::STATUS_BUILDING_RAILWAYS);
		mainGamestate->active_unit->update();
		mainGamestate->active_unit = NULL;
		mainGamestate->updateUnitButtons();
	}
	else if( source == mainGamestate->automateButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		ASSERT( mainGamestate->active_unit != NULL );
		ASSERT( mainGamestate->active_unit->getTemplate()->canBuildRoads() );
		mainGamestate->active_unit->setAutomated(true);
		mainGamestate->active_unit->automate();
		mainGamestate->cache_active_unit = mainGamestate->active_unit; // so we can cope with additional turns of this unit
		mainGamestate->active_unit = NULL;
		mainGamestate->updateUnitButtons();
	}
	else if( source == mainGamestate->gotoButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		ASSERT( mainGamestate->active_unit != NULL );
		ASSERT( !mainGamestate->active_unit->getTemplate()->isAir() );
		ASSERT( !mainGamestate->active_unit->getTemplate()->isSea() );
		/*mainGamestate->active_unit->setAutomated(true);
		mainGamestate->active_unit->automate();
		mainGamestate->cache_active_unit = mainGamestate->active_unit; // so we can cope with additional turns of this unit
		mainGamestate->active_unit = NULL;
		mainGamestate->updateUnitButtons();*/
		mainGamestate->setInputMode(INPUTMODE_GOTO);
		// set up goto targets
		// get dists - n.b., don't include travel
		Pos2D pos = mainGamestate->active_unit->getPos();
		const Distance *dists = mainGamestate->getMap()->calculateDistanceMap(mainGamestate->active_unit->getCivilization(), mainGamestate->active_unit, false, true, pos.x, pos.y);
		for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
			for(int x=0;x<mainGamestate->getMap()->getWidth();x++) {
				int this_dist = dists[y * mainGamestate->getMap()->getWidth() + x].cost;;
				if( ( pos.x != x || pos.y != y ) && this_dist != -1 && mainGamestate->player->isExplored(x, y) ) {
					mainGamestate->map->getSquare(x, y)->setTarget(true);
				}
			}
		}
		delete [] dists;
	}
	else if( source == mainGamestate->travelButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->setInputMode(INPUTMODE_TRAVEL);
		ASSERT( mainGamestate->active_unit != NULL );
		City *city = mainGamestate->map->findCity(mainGamestate->active_unit->getPos());
		ASSERT( city != NULL );
		city->initTravel(false, mainGamestate->active_unit->getTemplate());
	}
	else if( source == mainGamestate->airRaidButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->setInputMode(INPUTMODE_BOMBARD);
		// set up bombard targets
		ASSERT( mainGamestate->active_unit != NULL );
		Pos2D unit_pos = mainGamestate->active_unit->getPos();
		for(vector<Civilization *>::iterator iter = mainGamestate->civilizations.begin(); iter != mainGamestate->civilizations.end(); ++iter) {
			Civilization *civilization = *iter;
			if( civilization == mainGamestate->player )
				continue;
			for(size_t i=0;i<civilization->getNCities();i++) {
				City *city = civilization->getCity(i);
				Pos2D city_pos = city->getPos();
				//int dist = unit_pos.distanceChebyshev(&city_pos);
				int dist = mainGamestate->getMap()->distanceChebyshev(city_pos, unit_pos);
				if( dist <= mainGamestate->active_unit->getTemplate()->getAirRange() ) {
					mainGamestate->map->getSquare(city_pos)->setTarget(true);
				}
			}
		}
	}
	else if( source == mainGamestate->reconnaissanceButton ) {
		mainGamestate->playSound(SOUND_BLIP);
		mainGamestate->setInputMode(INPUTMODE_RECONNAISSANCE);
		// set up reconnaissance targets
		ASSERT( mainGamestate->active_unit != NULL );
		Pos2D unit_pos = mainGamestate->active_unit->getPos();
		// TODO:
		int range = mainGamestate->active_unit->getTemplate()->getAirRange();
		for(int y=unit_pos.y-range;y<=unit_pos.y+range;y++) {
			for(int x=unit_pos.x-range;x<=unit_pos.x+range;x++) {
				if( mainGamestate->map->isValid(x, y) ) {
					mainGamestate->map->getSquare(x, y)->setTarget(true);
				}
			}
		}
	}
	else if( source == mainGamestate->homeHiddenButton ) {
		mainGamestate->home();
	}
	else if( source == mainGamestate->centreHiddenButton ) {
		mainGamestate->centreUnit();
	}
	else {
		if( mainGamestate->active_unit != NULL && mainGamestate->phase == PHASE_PLAYERTURN ) {
			bool request_move[8] = {false, false, false, false, false, false, false, false}; // start from numpad 1, then go clockwise
			if( source == mainGamestate->moveLHiddenButton || source == mainGamestate->move4HiddenButton || source == mainGamestate->move4Button ) {
				request_move[1] = true;
			}
			else if( source == mainGamestate->moveRHiddenButton || source == mainGamestate->move6HiddenButton || source == mainGamestate->move6Button ) {
				request_move[5] = true;
			}
			else if( source == mainGamestate->moveUHiddenButton || source == mainGamestate->move8HiddenButton || source == mainGamestate->move8Button ) {
				request_move[3] = true;
			}
			else if( source == mainGamestate->moveDHiddenButton || source == mainGamestate->move2HiddenButton || source == mainGamestate->move2Button ) {
				request_move[7] = true;
			}
			else if( source == mainGamestate->move7HiddenButton || source == mainGamestate->move7Button ) {
				request_move[2] = true;
			}
			else if( source == mainGamestate->move9HiddenButton || source == mainGamestate->move9Button ) {
				request_move[4] = true;
			}
			else if( source == mainGamestate->move3HiddenButton || source == mainGamestate->move3Button ) {
				request_move[6] = true;
			}
			else if( source == mainGamestate->move1HiddenButton || source == mainGamestate->move1Button ) {
				request_move[0] = true;
			}
			mainGamestate->movePlayerActiveUnit(request_move);
		}
	}

	//game_g->getGraphicsEnvironment()->flushInput(); // needed to avoid problem where the mouse click is registered after clicking - is there a better way to do this?
	/*if( source != game->travelButton ) {
		game->setInputMode(INPUTMODE_NORMAL);
	}*/
}

void MainGamestate::zoom(bool zoom_in) {
	//Pos2D pos = mainGamestate->getMapPos(game_g->getGraphicsEnvironment()->getWidth()/2, game_g->getGraphicsEnvironment()->getHeight()/2);
	float mx = 0.0f, my = 0.0f;
	mainGamestate->getMapPosf(&mx, &my, game_g->getGraphicsEnvironment()->getWidth()/2, game_g->getGraphicsEnvironment()->getHeight()/2);
	if( mainGamestate->view_3d ) {
		VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
		Vector3D pos = vp->getPosition();
		pos.y += zoom_in ? -0.4f : 0.4f;
		pos.y = max(pos.y, 4.0f);
		pos.y = min(pos.y, 25.0f);
		vp->setPosition(pos);
	}
	else {
		if( zoom_in ) {
			mainGamestate->scale_width += 0.1f;
			mainGamestate->scale_height += 0.1f;
			mainGamestate->scale_width = min(mainGamestate->scale_width, 2.0f);
			mainGamestate->scale_height = min(mainGamestate->scale_height, 2.0f);
		}
		else {
			mainGamestate->scale_width -= 0.1f;
			mainGamestate->scale_height -= 0.1f;
			mainGamestate->scale_width = max(mainGamestate->scale_width, 0.3f);
			mainGamestate->scale_height = max(mainGamestate->scale_height, 0.3f);
		}
	}
	//mainGamestate->centre(pos);
	mainGamestate->centre(mx, my);
	/*VI_log("%f, %f\n", mx, my);
	mainGamestate->getMapPosf(&mx, &my, game_g->getGraphicsEnvironment()->getWidth()/2, game_g->getGraphicsEnvironment()->getHeight()/2);
	VI_log(" -> %f, %f\n", mx, my);*/
}

void MainGamestate::newGame() {
	/*string buttons[] = { "New Game", "Cancel" };
	InfoWindow *window = new InfoWindow(this->getPanel(), "Are you sure you wish to start a new game?", buttons, 2, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, infowindow_def_h_c);
	int res = window->doModal();
	delete window;
	if( res == 0 ) {*/
	if( InfoWindow::confirm(game_g->getGraphicsEnvironment(), this->getPanel(), game_g->getFont(), game_g->getPanelTexture(), "Are you sure you wish to start a new game?", "New Game", "Cancel") ) {
		/*this->clearGameData();
		//if( !this->initGameData(true) ) {
		if( !game_g->setupNewGame(this) ) {
			quit = true;
		}
		else {
			//beginTurn();
			// beginTurn() now called by setupNewGame()
			if( this->gamestate_started ) {
				this->generateTerrain();
			}
		}*/
		// we return to the main intro screen, rather than setting up a new game on the fly
		// this makes it easier should the new game wizard fail (e.g., loading an invalid world map, or no maps available, or the user cancels, etc)
		OptionsGamestate *optionsGamestate = new OptionsGamestate();
		game_g->pushNewGamestate(optionsGamestate);
	}
}

void MainGamestate::quitGame() {
	VI_log("MainGamestate::quitGame()\n");
	if( !this->isGUILocked() ) {
		//VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), 32, 64);
		//VI_Button *button = VI_createButton("", game_g->getFont());
		//VI_createSceneGraphNode();
		//delete textfield;
		//VI_addtag();
		this->setInputMode(INPUTMODE_NORMAL);
		if( InfoWindow::confirm(game_g->getGraphicsEnvironment(), this->getPanel(), game_g->getFont(), game_g->getPanelTexture(), "Are you sure you wish to quit?", "Quit", "Cancel") ) {
			quit = true;
		}
	}
}

//vector<MainGamestate::FilenameInfo> MainGamestate::getFiles(const char *dirname, const char *ext) {
void MainGamestate::getFiles(vector<MainGamestate::FilenameInfo> *files, const char *dirname, const char *ext) {
	game_g->setCursor(cursor_wait);

	//vector<FilenameInfo> savegamefiles;

	if( access(dirname, 0) != 0 ) {
		VI_log("folder doesn't seem to exist: %s\n", dirname);
	}
	else {
#ifdef _WIN32
		WIN32_FIND_DATAA findFileData;
		char dirname_w[256] = "";
		//sprintf(dirname_w, "%s/*.sav", dirname);
		sprintf(dirname_w, "%s/*%s", dirname, ext);
		HANDLE handle = FindFirstFileA(dirname_w, &findFileData);
		if( handle == INVALID_HANDLE_VALUE ) {
			VI_log("Invalid File Handle. GetLastError reports %d\n", GetLastError());
			// try to create the folder
		}
		else {
			while(true) {
				//VI_log("found file: %s\n", findFileData.cFileName);
				bool time_ok = true;
				SYSTEMTIME stUTC, stLocal;
				if( FileTimeToSystemTime(&findFileData.ftLastWriteTime, &stUTC) == 0 ) {
					time_ok = false;
					VI_log("FileTimeToSystemTime failed for %s\n", findFileData.cFileName);
					DWORD error = GetLastError();
					VI_log("error code: %d\n", error);
				}
				else if( SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal) == 0 ) {
					time_ok = false;
					VI_log("SystemTimeToTzSpecificLocalTime failed for %s\n", findFileData.cFileName);
					DWORD error = GetLastError();
					VI_log("error code: %d\n", error);
				}
				stringstream filedescription_str;
				stringstream date_str;
				if( time_ok ) {
					filedescription_str << std::setfill('0') << "[" << stLocal.wYear << "/" << std::setw(2) << stLocal.wMonth << "/" << std::setw(2) << stLocal.wDay << " - " << std::setw(2) << stLocal.wHour << ":" << std::setw(2) << stLocal.wMinute << "] " << findFileData.cFileName;
					date_str << std::setfill('0') << stLocal.wYear << std::setw(2) << stLocal.wMonth << std::setw(2) << stLocal.wDay << std::setw(2) << stLocal.wHour << std::setw(2) << stLocal.wMinute << std::setw(2) << stLocal.wSecond;
				}
				else {
					filedescription_str << findFileData.cFileName;
				}
				stringstream fullfilename;
				fullfilename << dirname << "/" << findFileData.cFileName;
				FilenameInfo filenameInfo;
				filenameInfo.filename = findFileData.cFileName;
				filenameInfo.fullfilename = fullfilename.str();
				filenameInfo.filedescription = filedescription_str.str();
				filenameInfo.date = date_str.str();
				//savegamefiles.push_back(filenameInfo);
				files->push_back(filenameInfo);
				if( FindNextFileA(handle, &findFileData) == 0 ) {
					FindClose(handle);
					DWORD error = GetLastError();
					if( error != ERROR_NO_MORE_FILES ) {
						VI_log("error reading directory: %d\n", error);
					}
					break;
				}
			}
		}
#elif __linux
		DIR *dir = opendir(dirname);
		if( dir == NULL ) {
			VI_log("failed to open directory: %s\n", dirname);
		}
		else {
			for(;;) {
				errno = 0;
				dirent *ent = readdir(dir);
				if( ent == NULL ) {
					closedir(dir);
					if( errno ) {
						VI_log("error reading directory: %d\n", errno);
					}
					break;
				}
				//if( strstr(ent->d_name, ".sav") != NULL ) {
				if( strstr(ent->d_name, ext) != NULL ) {
					//VI_log("found file: %s\n", ent->d_name);
					stringstream fullfilename;
					fullfilename << dirname << "/" << ent->d_name;
                    struct stat attrib;
                    if( stat(fullfilename.str().c_str(), &attrib) == -1 ) {
                        VI_log("stat failed for: %s\n", fullfilename.str().c_str());
                    }
                    struct tm *clock = localtime(&(attrib.st_mtime));
                    /*VI_log("%d : %d : %d\n", attrib.st_atime, attrib.st_mtime, attrib.st_ctime);
                    VI_log("%d : %d : %d\n", clock->tm_year + 1900, clock->tm_mon+1, clock->tm_mday+1);
                    VI_log("The date/time is: %s", asctime(clock));*/
                    stringstream filedescription_str;
					stringstream date_str;
                    filedescription_str << std::setfill('0') << "[" << clock->tm_year + 1900 << "/" << std::setw(2) << clock->tm_mon+1 << "/" << std::setw(2) << clock->tm_mday+1 << " - " << std::setw(2) << clock->tm_hour << ":" << std::setw(2) << clock->tm_min << "] " << ent->d_name;
					date_str << std::setfill('0') << clock->tm_year + 1900 << std::setw(2) << clock->tm_mon+1 << std::setw(2) << clock->tm_mday+1 << std::setw(2) << clock->tm_hour << std::setw(2) << clock->tm_min << std::setw(2) << clock->tm_sec;
					FilenameInfo filenameInfo;
					filenameInfo.filename = ent->d_name;
					filenameInfo.fullfilename = fullfilename.str();
					filenameInfo.filedescription = filedescription_str.str();
					filenameInfo.date = date_str.str();
					//savegamefiles.push_back(filenameInfo);
					files->push_back(filenameInfo);
				}
			}
		}
#endif
	}

/*#ifdef _WIN32
	cursor = LoadCursor( NULL, IDC_ARROW );
	SetCursor(cursor);
	SetClassLong(game_g->getGraphicsEnvironment()->getHWND(), GCL_HCURSOR, (LONG)cursor);
#endif*/
	game_g->setCursor(cursor_arrow);
	//return savegamefiles;
}

bool MainGamestate::sortLoadDialogFilesByName(const FilenameInfo& f1, const FilenameInfo& f2) {
	return f1.filename < f2.filename;
}

bool MainGamestate::sortLoadDialogFilesByDate(const FilenameInfo& f1, const FilenameInfo& f2) {
	return f1.date > f2.date;
}

void MainGamestate::loadDialogSort(LoadDialogSortData *data) {
	vector<FilenameInfo> *gamefiles = data->gamefiles;

	if( data->sort_button != NULL ) {
		data->sort_button->setText(data->sort_by_name ? "Sort by Date" : "Sort by Name");
		data->sort_button->setPopupText(data->sort_by_name ? "Click to sort by date" : "Click to sort by name", game_g->getFont());
	}
	std::sort(gamefiles->begin(), gamefiles->end(), data->sort_by_name ? sortLoadDialogFilesByName : sortLoadDialogFilesByDate);

	vector<string> strings;
	for(vector<FilenameInfo>::const_iterator iter = gamefiles->begin(); iter != gamefiles->end(); ++iter ) {
		const FilenameInfo filenameInfo = *iter;
		strings.push_back( filenameInfo.filedescription );
	}
	data->listbox->reset(&*strings.begin(), strings.size());
}

void MainGamestate::loadDialogSortAction(VI_Panel *source) {
	LoadDialogSortData *data = static_cast<LoadDialogSortData *>(source->getUserData());
	data->sort_by_name = !data->sort_by_name;
	loadDialogSort(data);
}

/*string MainGamestate::loadDialog(LoadDialogStatus *status, const char *load_text, const char *dirname, const char *ext, bool want_sort) {
	*status = LOADDIALOGSTATUS_NOFILES;
	vector<FilenameInfo> gamefiles = getFiles(dirname, ext);
	if( gamefiles.size() > 0 ) {
		const int max_buttons_c = 3;
		string buttons[max_buttons_c];
		int n_buttons = 0;
		buttons[n_buttons++] = load_text;
		if( want_sort ) {
			buttons[n_buttons++] = "Sort by XXXX"; // text filled in later - put dummy text to add spacing
		}
		buttons[n_buttons++] = "Cancel";
		int window_w_c = 504;
		int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons, infowindow_def_x_c, infowindow_def_y_c, window_w_c, window_h_c);
		VI_Panel *windowPanel = window->getPanel();

		vector<string> strings;
		for(vector<FilenameInfo>::const_iterator iter = gamefiles.begin(); iter != gamefiles.end(); ++iter ) {
			const FilenameInfo filenameInfo = *iter;
			strings.push_back( filenameInfo.filedescription );
		}
		//VI_Listbox *listbox = VI_createListbox(window_w_c - 32, window_h_c - 64, &*savegamefiles.begin(), savegamefiles.size(), game_g->getFont());
		VI_Listbox *listbox = VI_createListbox(window_w_c - 32, window_h_c - 64, &*strings.begin(), strings.size(), game_g->getFont());
		//VI_Listbox *listbox = VI_createListbox(infowindow_def_w_c/2 - 16, window_h_c - 128, &*technology_names.begin(), technology_names.size(), game_g->getFont());
		initListbox(listbox);
		listbox->setHasInputFocus(true);
		//listbox->setForeground(1.0f, 1.0f, 1.0f);
		listbox->setActive(0);
		windowPanel->addChildPanel(listbox, 16, 16);

		if( want_sort ) {
			LoadDialogSortData loadDialogSortData(&gamefiles, listbox, window->getButton(1), false);
			loadDialogSort(&loadDialogSortData);
			window->getButton(1)->setUserData(&loadDialogSortData);
			window->getButton(1)->setAction(loadDialogSortAction);
		}
		int res = window->doModal();
		if( res == 0 ) {
			int active = listbox->getActive();
			ASSERT( active >= 0 && active < gamefiles.size() );
			string filename = dirname;
			//filename += "/" + savegamefiles.at(active);
			filename += "/" + gamefiles.at(active).filename;
			VI_log("%d: selected file: %s\n", active, filename.c_str());
			delete window;
			//delete textfield;
			//delete listbox;
			*status = LOADDIALOGSTATUS_OK;
			return filename;
		}

		delete window;
		//delete textfield;
		//delete listbox;
		*status = LOADDIALOGSTATUS_CANCEL;
	}
	return "";
}*/

string MainGamestate::loadDialog(LoadDialogStatus *status, const char *load_text, const char *dirname, const char *ext, bool want_sort_button, bool default_sort_by_name) {
	vector<string> dirnames;
	dirnames.push_back(dirname);
	return loadDialog(status, load_text, &dirnames, ext, want_sort_button, default_sort_by_name);
}

string MainGamestate::loadDialog(LoadDialogStatus *status, const char *load_text, const vector<string> *dirnames, const char *ext, bool want_sort_button, bool default_sort_by_name) {
	*status = LOADDIALOGSTATUS_NOFILES;
	//vector<FilenameInfo> gamefiles = getFiles(dirnames, ext);
	vector<FilenameInfo> gamefiles;
	for(vector<string>::const_iterator iter = dirnames->begin(); iter != dirnames->end(); ++iter) {
		string dirname = *iter;
		getFiles(&gamefiles, dirname.c_str(), ext);
	}
	if( gamefiles.size() > 0 ) {
		const int max_buttons_c = 3;
		string buttons[max_buttons_c];
		int n_buttons = 0;
		buttons[n_buttons++] = load_text;
		if( want_sort_button ) {
			buttons[n_buttons++] = "Sort by XXXX"; // text filled in later - put dummy text to add spacing
		}
		buttons[n_buttons++] = "Cancel";
		int window_w_c = 504;
		int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons, infowindow_def_x_c, infowindow_def_y_c, window_w_c, window_h_c);
		VI_Panel *windowPanel = window->getPanel();

		vector<string> strings;
		for(vector<FilenameInfo>::const_iterator iter = gamefiles.begin(); iter != gamefiles.end(); ++iter ) {
			const FilenameInfo filenameInfo = *iter;
			strings.push_back( filenameInfo.filedescription );
		}
		//VI_Listbox *listbox = VI_createListbox(window_w_c - 32, window_h_c - 64, &*savegamefiles.begin(), savegamefiles.size(), game_g->getFont());
		VI_Listbox *listbox = VI_createListbox(window_w_c - 32, window_h_c - 64, &*strings.begin(), strings.size(), game_g->getFont());
		//VI_Listbox *listbox = VI_createListbox(infowindow_def_w_c/2 - 16, window_h_c - 128, &*technology_names.begin(), technology_names.size(), game_g->getFont());
		initListbox(listbox);
		listbox->setHasInputFocus(true);
		//listbox->setForeground(1.0f, 1.0f, 1.0f);
		listbox->setActive(0);
		windowPanel->addChildPanel(listbox, 16, 16);

		LoadDialogSortData loadDialogSortData(&gamefiles, listbox, want_sort_button ? window->getButton(1) : NULL, default_sort_by_name);
		loadDialogSort(&loadDialogSortData);
		if( want_sort_button ) {
			window->getButton(1)->setUserData(&loadDialogSortData);
			window->getButton(1)->setAction(loadDialogSortAction);
		}
		int res = window->doModal();
		if( res == 0 ) {
			int active = listbox->getActive();
			ASSERT( active >= 0 && active < gamefiles.size() );
			/*string filename = dirname;
			//filename += "/" + savegamefiles.at(active);
			filename += "/" + gamefiles.at(active).filename;*/
			string filename = gamefiles.at(active).fullfilename;
			VI_log("%d: selected file: %s\n", active, filename.c_str());
			delete window;
			//delete textfield;
			//delete listbox;
			*status = LOADDIALOGSTATUS_OK;
			return filename;
		}

		delete window;
		//delete textfield;
		//delete listbox;
		*status = LOADDIALOGSTATUS_CANCEL;
	}
	return "";
}

bool MainGamestate::loadGame() {
	bool ok = false;
	LoadDialogStatus loadDialogStatus = LOADDIALOGSTATUS_NOFILES;
	string filename = loadDialog(&loadDialogStatus, "Load Game", savegames_dirname, savegames_ext, true, false);
	if( loadDialogStatus == LOADDIALOGSTATUS_NOFILES ) {
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "No save game files available", game_g->getFont(), game_g->getPanelTexture());
		window->doModal();
		delete window;
	}
	else if( loadDialogStatus == LOADDIALOGSTATUS_OK ) {
		VI_log("loading: %s\n", filename.c_str());
		ok = this->load(filename.c_str());
	}

	/*bool ok = false;

	vector<FilenameInfo> savegamefiles = getFiles(savegames_dirname, savegames_ext);

	if( savegamefiles.size() > 0 ) {
		string buttons[] = {"Load game", "Cancel"};
		//string buttons[] = {"Load game", "Delete file", "Cancel"};
		//const int window_h_c = 512;
		int window_w_c = 504;
		int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, 2, infowindow_def_x_c, infowindow_def_y_c, window_w_c, window_h_c);
		VI_Panel *windowPanel = window->getPanel();

		vector<string> strings;
		for(vector<FilenameInfo>::const_iterator iter = savegamefiles.begin(); iter != savegamefiles.end(); ++iter ) {
			const FilenameInfo filenameInfo = *iter;
			strings.push_back( filenameInfo.filedescription );
		}
		//VI_Listbox *listbox = VI_createListbox(window_w_c - 32, window_h_c - 64, &*savegamefiles.begin(), savegamefiles.size(), game_g->getFont());
		VI_Listbox *listbox = VI_createListbox(window_w_c - 32, window_h_c - 64, &*strings.begin(), strings.size(), game_g->getFont());
		//VI_Listbox *listbox = VI_createListbox(infowindow_def_w_c/2 - 16, window_h_c - 128, &*technology_names.begin(), technology_names.size(), game_g->getFont());
		initListbox(listbox);
		listbox->setHasInputFocus(true);
		//listbox->setForeground(1.0f, 1.0f, 1.0f);
		listbox->setActive(0);
		windowPanel->addChildPanel(listbox, 16, 16);

		int res = window->doModal();
		if( res == 0 ) {
			int active = listbox->getActive();
			ASSERT( active >= 0 && active < savegamefiles.size() );
			string filename = savegames_dirname;
			//filename += "/" + savegamefiles.at(active);
			filename += "/" + savegamefiles.at(active).filename;
			VI_log("%d: loading: %s\n", active, filename.c_str());
			delete window;
			ok = this->load(filename.c_str());
		}
		else {
			delete window;
		}

		//delete textfield;
		delete listbox;
	}
	else {
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "No save game files available", game_g->getFont(), game_g->getPanelTexture());
		window->doModal();
		delete window;
	}*/

	/*DIR *dir = opendir(savegames_dirname);
	if( dir == NULL ) {
	VI_log("failed to open savegames folder\n");
	}*/


	/*#ifdef _WIN32
	char filename[1024] = "";
	OPENFILENAMEA openfilename;
	memset(&openfilename, 0, sizeof(OPENFILENAME));
	openfilename.lStructSize = sizeof(OPENFILENAME);
	openfilename.hwndOwner = game_g->getGraphicsEnvironment()->getHWND();
	openfilename.lpstrFilter = filter;
	openfilename.nFilterIndex = 1;
	openfilename.lpstrDefExt = ext;
	openfilename.lpstrFile = filename;
	openfilename.nMaxFile = sizeof(filename)/sizeof(*filename);
	openfilename.lpstrInitialDir = ".";
	openfilename.Flags = OFN_SHOWHELP | OFN_OVERWRITEPROMPT;
	//openfilename.lpstrTitle = szTitle;
	//ShowCursor(TRUE);
	if( game_g->getGraphicsEnvironment()->isTrueFullscreen() ) {
	ShowWindow(game_g->getGraphicsEnvironment()->getHWND(), SW_MINIMIZE); // needed for D3D9 fullscreen mode
	}

	char current_directory[65536];
	getcwd(current_directory, sizeof(current_directory));

	if( GetOpenFileNameA(&openfilename) ) {
	chdir(current_directory);
	game_g->getGraphicsEnvironment()->flushInput(); // needed because we miss key releases when dialog is open, meaning we may think a key is still being pressed!
	ok = this->load(filename);
	}
	else {
	chdir(current_directory);
	game_g->getGraphicsEnvironment()->flushInput(); // needed because we miss key releases when dialog is open, meaning we may think a key is still being pressed!
	VI_log("file dialog failed/cancelled\n");
	ok = false;
	}

	if( game_g->getGraphicsEnvironment()->isTrueFullscreen() ) {
	ShowWindow(game_g->getGraphicsEnvironment()->getHWND(), SW_RESTORE);
	}
	//ShowCursor(FALSE);
	//this->player->revealMap(); // uncomment to reveal map
	#else
	ok = false;
	#endif*/
	if( CHEAT && ok ) {
		this->player->revealMap(); // uncomment to reveal map
	}
	return ok;
}

void MainGamestate::saveMap() {
	VI_log("save map requested...\n");

	//char filename[1024] = "";
	//sprintf(filename, "%s Year %d", player->getName(), year);

	string buttons[] = {"Save map", "Cancel"};
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, 2, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, 128);
	VI_Panel *windowPanel = window->getPanel();

	VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), infowindow_def_w_c - 32, 32);
	textfield->addText("Save map as:");
	windowPanel->addChildPanel(textfield, 16, 16);

	VI_Stringgadget *stringgadget = VI_createStringgadget(game_g->getFont(), infowindow_def_w_c - 32);
	//stringgadget->addText(filename);
	stringgadget->setReadOnly(false);
	stringgadget->setBlockedCharacters("\\/:*?\"<>|");
	stringgadget->setMaxLen(30); // to avoid text overrun
	windowPanel->addChildPanel(stringgadget, 16, 64);

	int res = window->doModal();

	if( res == 0 ) {
		string filename = usermaps_dirname;
		filename += "/";
		filename += stringgadget->getText();
		filename += maps_ext;
		VI_log("save map as: %s\n", filename.c_str());

		FILE *file = fopen(filename.c_str(), "w");
		if( file == NULL ) {
			VI_log("Unable to open file\n");
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "Unable to create save file", game_g->getFont(), game_g->getPanelTexture());
			window->doModal();
			delete window;
		}
		else {
			game_g->mapWriteHeader(file, map->getWidth(), map->getHeight());
			for(int y=0;y<map->getHeight();y++) {
				for(int x=0;x<map->getWidth();x++) {
					Type type = map->getSquare(x, y)->getType();
					fprintf(file, "%d", ((int)type) + 1);
					if( y != map->getHeight()-1 || x != map->getWidth()-1 ) {
						// last entry has no comma
						fprintf(file, ",");
					}
				}
				fprintf(file, "\n");
			}
			game_g->mapWriteFooter(file);
			fclose(file);
		}
	}

	delete window;
	//delete textfield;
	//delete stringgadget;

}

void MainGamestate::saveGame() {
	VI_log("save game requested...\n");

	char filename[1024] = "";
	sprintf(filename, "%s Year %d", player->getName(), year);

	string buttons[] = {"Save game", "Cancel"};
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, 2, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, 128);
	VI_Panel *windowPanel = window->getPanel();

	VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), infowindow_def_w_c - 32, 32);
	textfield->addText("Save game as:");
	windowPanel->addChildPanel(textfield, 16, 16);

	VI_Stringgadget *stringgadget = VI_createStringgadget(game_g->getFont(), infowindow_def_w_c - 32);
	stringgadget->addText(filename);
	stringgadget->setReadOnly(false);
	stringgadget->setBlockedCharacters("\\/:*?\"<>|");
	stringgadget->setMaxLen(255);
	windowPanel->addChildPanel(stringgadget, 16, 64);

	//game_g->getGraphicsEnvironment()->flushInput(); // so we don't get an "S" keypress...
	int res = window->doModal();
	if( res == 0 ) {
		string filename = savegames_dirname;
		filename += "/";
		filename += stringgadget->getText();
		filename += savegames_ext;
		this->save(filename.c_str(), false);
	}

	delete window;
	//delete textfield;
	//delete stringgadget;

/*#ifdef _WIN32
	//const int ext_len = strlen(ext);
	char filename[1024] = "";
	sprintf(filename, "%s Year %d.sav", player->getName(), year);
	OPENFILENAMEA openfilename;
	memset(&openfilename, 0, sizeof(OPENFILENAME));
	openfilename.lStructSize = sizeof(OPENFILENAME);
	openfilename.hwndOwner = game_g->getGraphicsEnvironment()->getHWND();
	openfilename.lpstrFilter = filter;
	openfilename.nFilterIndex = 1;
	openfilename.lpstrDefExt = ext;
	openfilename.lpstrFile = filename;
	//openfilename.nMaxFile = sizeof(filename)/sizeof(*filename) - ext_len; // -ext_len to allow adding extension
	openfilename.nMaxFile = sizeof(filename)/sizeof(*filename);
	openfilename.lpstrInitialDir = ".";
	openfilename.Flags = OFN_SHOWHELP | OFN_OVERWRITEPROMPT;
	//openfilename.lpstrTitle = szTitle;
	//ShowCursor(TRUE);
	if( game_g->getGraphicsEnvironment()->isTrueFullscreen() ) {
		ShowWindow(game_g->getGraphicsEnvironment()->getHWND(), SW_MINIMIZE); // needed for D3D9 fullscreen mode
	}

	char current_directory[65536];
	getcwd(current_directory, sizeof(current_directory));

	if( GetSaveFileNameA(&openfilename) ) {
		chdir(current_directory);
		if( game_g->getGraphicsEnvironment()->isTrueFullscreen() ) {
			ShowWindow(game_g->getGraphicsEnvironment()->getHWND(), SW_RESTORE);
		}
		game_g->getGraphicsEnvironment()->flushInput(); // needed because we miss key releases when dialog is open, meaning we may think a key is still being pressed!
		this->save(filename);
		stringstream text;
		text << "Successfully saved game under filename:\n\n" << filename;
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
		window->doModal();
		delete window;
	}
	else {
		chdir(current_directory);
		if( game_g->getGraphicsEnvironment()->isTrueFullscreen() ) {
			ShowWindow(game_g->getGraphicsEnvironment()->getHWND(), SW_RESTORE);
		}
		game_g->getGraphicsEnvironment()->flushInput(); // needed because we miss key releases when dialog is open, meaning we may think a key is still being pressed!
		VI_log("file dialog failed/cancelled\n");
	}

	//ShowCursor(FALSE);
#endif*/
}

const char file_header_string_c[] = "CONQUESTSSAVEGAME";

void MainGamestate::save(const char *filename, bool autosave) {
	VI_log("MainGamestate::save(%s, %d)\n", filename, autosave);

	/*if( access(savegames_dirname, 0) != 0 ) {
		VI_log("save game folder doesn't seem to exist - try creating it\n");
#if _WIN32
		int res = mkdir(savegames_dirname);
#elif __linux
        //mode_t mask = umask(0);
        //VI_log("mask %d\n", mask);
        //VI_log("mask %d\n", umask(0));
		int res = mkdir(savegames_dirname, S_IRWXU | S_IRWXG | S_IRWXO);
		//umask(mask);
#endif
		if( res != 0 ) {
			VI_log("mkdir failed: %d\n", errno);
			if( !autosave ) {
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "Unable to create save game folder", game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
			}
			return;
		}
		else if( access(savegames_dirname, 0) != 0 ) {
			VI_log("still can't access it!\n");
			if( !autosave ) {
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "Unable to access save game folder", game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
			}
			return;
		}
	}*/
	if( !VI_createFolder(savegames_dirname) ) {
		VI_log("failed to create or access save game folder\n");
		if( !autosave ) {
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "Unable to create or access save game folder", game_g->getFont(), game_g->getPanelTexture());
			window->doModal();
			delete window;
		}
		return;
	}

	if( !autosave && access(filename, 0) == 0 ) {
		// file already exists!
		stringstream str;
		str << "The file " << filename << " already exists. Do you wish to overwrite it?";
		if( !InfoWindow::confirm(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), game_g->getFont(), game_g->getPanelTexture(), str.str().c_str(), "Yes, overwrite", "No, cancel") ) {
			return;
		}
	}

	//FILE *file = fopen(filename, "wb");
	FILE *file = fopen(filename, "w");
	if( file == NULL ) {
		VI_log("failed to open file for writing\n");
/*#ifdef _WIN32
		MessageBoxA(NULL, "Failed to open file for writing", "Error", MB_OK|MB_ICONEXCLAMATION);
#endif*/
		if( !autosave ) {
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "Failed to open file for writing", game_g->getFont(), game_g->getPanelTexture());
			window->doModal();
			delete window;
		}
		return;
	}
	fprintf(file, "%s\n", file_header_string_c);
	fprintf(file, "major_version = %d\n", versionMajor);
	fprintf(file, "minor_version = %d\n", versionMinor);
	fprintf(file, "year=%d\n", this->getYear());
	fprintf(file, "seed=%d\n", this->seed);
	fprintf(file, "difficulty=%d\n", (int)this->difficulty);
	fprintf(file, "aiaggression=%d\n", (int)this->aiaggression);
	fprintf(file, "game_n_turns=%d\n", this->game_n_turns);
	fprintf(file, "help_buildcity=%d\n", help_buildcity?1:0);
	fprintf(file, "help_buildroad=%d\n", help_buildroad?1:0);
	fprintf(file, "help_bomb=%d\n", help_bomb?1:0);
	fprintf(file, "player=%s\n", player->getName());
	fprintf(file, "map_width=%d\n", map->getWidth());
	fprintf(file, "map_height=%d\n", map->getHeight());
	fprintf(file, "map_topology=%d\n", map->getTopology());
	fprintf(file, "map=\n");
	for(int y=0;y<map->getHeight();y++) {
		for(int x=0;x<map->getWidth();x++) {
			const MapSquare *square = map->getSquare(x, y);
			fprintf(file, "%d", square->getType());
			if( square->getRoad() == ROAD_BASIC ) {
				fprintf(file, "r");
			}
			else if( square->getRoad() == ROAD_RAILWAYS ) {
				fprintf(file, "R");
			}
			int progress_road = square->getProgressRoad();
			if( progress_road > 0 ) {
				ASSERT( progress_road < 10 ); // can only cope with saving single digit!
				fprintf(file, "p%d", progress_road);
			}
			const BonusResource *bonus_resource = square->getBonusResource();
			if( bonus_resource != NULL ) {
				fprintf(file, "*%s*", bonus_resource->getName());
			}
		}
		fprintf(file, "\n");
	}
	for(vector<Civilization *>::const_iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		const Civilization *civilization = *iter;
		civilization->save(file);
	}
	for(vector<Relationship *>::const_iterator iter = relationships.begin(); iter != relationships.end(); ++iter) {
		const Relationship *relationship = *iter;
		relationship->save(file);
	}
	fclose(file);
}

bool MainGamestate::load(const char *filename) {
	VI_log("MainGamestate::load(%s)\n", filename);
	//FILE *file = fopen(filename, "rb");
	FILE *file = fopen(filename, "r");
	if( file == NULL ) {
		VI_log("failed to open file for reading\n");
/*#ifdef _WIN32
		MessageBoxA(NULL, "Failed to open file", "Error", MB_OK|MB_ICONEXCLAMATION);
#endif*/
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "Failed to open file", game_g->getFont(), game_g->getPanelTexture());
		window->doModal();
		delete window;
		return false;
	}
/*#ifdef _WIN32
	HCURSOR cursor = LoadCursor( NULL, IDC_WAIT );
	SetCursor(cursor);
	SetClassLong(game_g->getGraphicsEnvironment()->getHWND(), GCL_HCURSOR, (LONG)cursor);
#endif*/
	game_g->setCursor(cursor_wait);

	this->clearGameData();

	cache_active_unit = NULL;
	active_unit = NULL;
	//updateUnitButtons(); // disabled for when called from options screen? But is this needed during standard game play?
	moved_units_this_turn = false;

	//map = new Map(100, 100);
	/*map = new Map();
	const char mapfile[] = "conquests/data/world.map";
	if( !map->readFile(mapfile) ) {
	VI_log("Failed to read map file: %s\n", mapfile);
	quit = true;
	return;
	}*/

	const int max_line_c = 4095;
	char line[max_line_c+1] = "";
	char word[max_line_c+1] = "";
	bool ok = true;
	const char field_version_major_c[] = "major_version=";
	const char field_version_minor_c[] = "minor_version=";
	const char field_year_c[] = "year=";
	const char field_seed_c[] = "seed=";
	const char field_difficulty_c[] = "difficulty=";
	const char field_aiaggression_c[] = "aiaggression=";
	const char field_game_n_turns_c[] = "game_n_turns=";
	const char field_help_buildcity_c[] = "help_buildcity=";
	const char field_help_buildroad_c[] = "help_buildroad=";
	const char field_help_bomb_c[] = "help_bomb=";
	const char field_player_c[] = "player=";
	const char field_map_width_c[] = "map_width=";
	const char field_map_height_c[] = "map_height=";
	const char field_map_topology_c[] = "map_topology=";
	const char field_map_c[] = "map=";
	int map_width = -1, map_height = -1;
	Topology map_topology = TOPOLOGY_FLAT; // default to flat for backwards compatibility
	string player_civ;
	bool first_line = true;
	bool invalid_header = false;
	bool invalid_version = false;

	try {
		while( ok && fgets(line, max_line_c, file) != NULL ) {
			if( first_line ) {
				if( matchFileLine(line, file_header_string_c) ) {
					// ok
					first_line = false;
					continue;
				}
				else {
					VI_log("header id missing, failed to recognise file\n");
					invalid_header = true;
					ok = false;
					break;
				}
			}
			if( matchFileLine(line, field_version_major_c) ) {
				parseFileLine(word, line, field_version_major_c);
				int this_versionMajor = atoi(word);
				VI_log("filename has major version %d\n", this_versionMajor);
				if( this_versionMajor != versionMajor ) {
					invalid_version = true;
					ok = false;
				}
			}
			else if( matchFileLine(line, field_version_minor_c) ) {
				parseFileLine(word, line, field_version_minor_c);
				int this_versionMinor = atoi(word);
				VI_log("filename has minor version %d\n", this_versionMinor);
				if( this_versionMinor != versionMinor ) {
					invalid_version = true;
					ok = false;
				}
			}
			else if( matchFileLine(line, field_year_c) ) {
				parseFileLine(word, line, field_year_c);
				year = atoi(word);
				VI_log("year = %d\n", year);
			}
			else if( matchFileLine(line, field_seed_c) ) {
				parseFileLine(word, line, field_seed_c);
				seed = atoi(word);
				VI_log("seed = %d\n", seed);
			}
			else if( matchFileLine(line, field_difficulty_c) ) {
				parseFileLine(word, line, field_difficulty_c);
				int difficulty_int = atoi(word);
				VI_log("difficulty = %d\n", difficulty_int);
				if( difficulty_int < 0 || difficulty_int >= N_DIFFICULTY ) {
					VI_log("unrecognised difficulty!\n");
					ok = false;
				}
				else {
					difficulty = (Difficulty)difficulty_int;
				}
			}
			else if( matchFileLine(line, field_aiaggression_c) ) {
				parseFileLine(word, line, field_aiaggression_c);
				int aiaggression_int = atoi(word);
				VI_log("aiaggression = %d\n", aiaggression_int);
				if( aiaggression_int < 0 || aiaggression_int >= N_AIAGGRESSION ) {
					VI_log("unrecognised aiaggression!\n");
					ok = false;
				}
				else {
					aiaggression = (AIAggression)aiaggression_int;
				}
			}
			else if( matchFileLine(line, field_game_n_turns_c) ) {
				parseFileLine(word, line, field_game_n_turns_c);
				game_n_turns = atoi(word);
				VI_log("game_n_turns = %d\n", game_n_turns);
			}
			else if( matchFileLine(line, field_help_buildcity_c) ) {
				parseFileLine(word, line, field_help_buildcity_c);
				help_buildcity = parseBool(&ok, word);
			}
			else if( matchFileLine(line, field_help_buildroad_c) ) {
				parseFileLine(word, line, field_help_buildroad_c);
				help_buildroad = parseBool(&ok, word);
			}
			else if( matchFileLine(line, field_help_bomb_c) ) {
				parseFileLine(word, line, field_help_bomb_c);
				help_bomb = parseBool(&ok, word);
			}
			else if( matchFileLine(line, field_player_c) ) {
				parseFileLine(word, line, field_player_c);
				player_civ = word;
			}
			else if( matchFileLine(line, field_map_width_c) ) {
				parseFileLine(word, line, field_map_width_c);
				map_width = atoi(word);
			}
			else if( matchFileLine(line, field_map_height_c) ) {
				parseFileLine(word, line, field_map_height_c);
				map_height = atoi(word);
			}
			else if( matchFileLine(line, field_map_topology_c) ) {
				parseFileLine(word, line, field_map_topology_c);
				map_topology = (Topology)atoi(word);
			}
			else if( matchFileLine(line, field_map_c) ) {
				if( map_width == -1 || map_height == -1 ) {
					VI_log("map width/height not set\n");
					ok = false;
					break;
				}
				map = new Map(this, map_width, map_height);
				map->setTopology(map_topology);
				for(int y=0;y<map->getHeight() && ok;y++ ) {
					if( fgets(line, max_line_c, file) == NULL ) {
						VI_log("MainGamestate::load unexpected eof when reading map\n");
						ok = false;
						break;
					}
					for(int x=-1,c=0;ok;c++) {
						if( line[c] == '\r' || line[c] == '\n' ) {
							if( x != map->getWidth()-1 ) {
								VI_log("MainGamestate::load unexpected newline when reading map\n");
								ok = false;
							}
							break;
						}
						else if( line[c] >= '0' && line[c] <= '9' ) {
							x++;
							if( x >= map->getWidth() ) {
								VI_log("MainGamestate::load unexpected line too long when reading map\n");
								ok = false;
							}
							else {
								MapSquare *square = map->getSquare(x, y);
								square->setType((Type)(line[c] - '0'));
							}
						}
						else if( line[c] == 'r' || line[c] == 'R' ) {
							MapSquare *square = map->getSquare(x, y);
							if( line[c] == 'r' )
								square->setRoad(ROAD_BASIC);
							else
								square->setRoad(ROAD_RAILWAYS);
						}
						else if( line[c] == 'p' ) {
							MapSquare *square = map->getSquare(x, y);
							if( line[c+1] >= '1' && line[c+1] <= '9' ) {
								// read progress_road
								int progress_road = (int)(line[c+1] - '0');
								square->setProgressRoad(progress_road);
								c++;
							}
							else {
								VI_log("MainGamestate::load unrecognised progress_road\n");
								ok = false;
							}
						}
						else if( line[c] == '*' ) {
							string bonus_name;
							c++;
							for(;;) {
								if( line[c] == '*' )
									break;
								else if( line[c] == '\n' || line[c] == '\r' ) {
									VI_log("MainGamestate::load unexpected newline when reading bonus\n");
									ok = false;
									break;
								}
								else {
									bonus_name.push_back(line[c]);
								}
								c++;
							}
							if( ok ) {
								const BonusResource *bonus_resource = game_g->getGameData()->findBonusResource(bonus_name.c_str());
								if( bonus_resource == NULL ) {
									VI_log("MainGamestate::load unknown bonus resource: %s\n", bonus_name.c_str());
									ok = false;
								}
								else {
									MapSquare *square = map->getSquare(x, y);
									square->setBonusResource(bonus_resource);
								}
							}
						}
						else {
							VI_log("MainGamestate::load unrecognised map symbol\n");
							ok = false;
						}
					}
				}
				//map->createRandomBonuses(); // uncomment to regenerate bonuses
				map->createAlphamap();
			}
			//else if( strcmp(line, "[civilization]\n") == 0 ) {
			else if( matchFileLine(line, "[civilization]") ) {
				if( map == NULL ) {
					VI_log("MainGamestate::load map should be present before any civilizations\n");
					ok = false;
					break;
				}
				Civilization *civilization = Civilization::load(this, file);
				if( civilization == NULL ) {
					VI_log("MainGamestate::load failed to load civilization\n");
					ok = false;
					break;
				}
				else if( strcmp(civilization->getName(), player_civ.c_str()) == 0 ) {
					player = civilization;
					VI_log("found player civilization: %s\n", player->getName());
				}
			}
			//else if( strcmp(line, "[relationship]\n") == 0 ) {
			else if( matchFileLine(line, "[relationship]") ) {
				Relationship *relationship = Relationship::load(this, file);
				if( relationship == NULL ) {
					VI_log("MainGamestate::load failed to load relationship\n");
					ok = false;
					break;
				}
				else {
					this->relationships.push_back(relationship);
				}
			}
			else if( line[0] == '[' ) {
				VI_log("MainGamestate::load: Unexpected '[' start of new section!\n");
				ok = false;
				break;
			}
		}
	}
	catch(const char *error) {
		VI_log("exception loading game: %s\n", error);
		ok = false;
	}
	VI_log("done loading save game file, ok?: %d\n", ok);
	// TODO: check all fields have been set
	if( ok && player == NULL ) {
		VI_log("MainGamestate::load failed to find player civilization\n");
		ok = false;
	}
	if( ok && map == NULL ) {
		VI_log("MainGamestate::load failed to find map\n");
		ok = false;
	}
	if( ok ) {
		// find Rebel army
		rebel_civ = NULL;
		for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end() && rebel_civ == NULL; ++iter) {
			Civilization *civ = *iter;
			if( civ->getRace()->equals(rebel_race_name) ) {
				rebel_civ = civ;
			}
		}
		if( rebel_civ == NULL ) {
			// won't be present if loading from earlier version of game than 0.16
			this->createRebelCiv();
		}
		else {
			LOG("found rebel civ: %d\n", rebel_civ);
			if( rebel_civ == player ) {
				LOG("player can't be the rebel civ!\n");
				ok = false;
			}
			else if( rebel_civ->getNCities() != 0 ) {
				LOG("error, rebel civ has %d cities\n", rebel_civ->getNCities());
				ok = false;
			}
			else {
				// should already be done, but just in case...
				rebel_civ->revealMap();
			}
		}
	}

	// check all relationships present
	if( ok && !checkRelationships() ) {
		VI_log("MainGamestate::load not all relationships present\n");
		ok = false;
	}

	fclose(file);

	if( ok ) {
		for(vector<Civilization *>::iterator iter = this->civilizations.begin(); iter != this->civilizations.end(); ++iter) {
			Civilization *civilization = *iter;
			civilization->calculateCivBonuses();
			// calling initTravel() shouldn't be needed, but a safeguard to repair corrupt save game files!
			for(size_t i=0;i<civilization->getNCities();i++) {
				const City *city = civilization->getCity(i);
				city->initTravel(true, NULL);
			}
		}
	}

	VI_log("MainGamestate::load ok? %d\n", ok);

/*#ifdef _WIN32
	cursor = LoadCursor( NULL, IDC_ARROW );
	SetCursor(cursor);
	SetClassLong(game_g->getGraphicsEnvironment()->getHWND(), GCL_HCURSOR, (LONG)cursor);
#endif*/
	game_g->setCursor(cursor_arrow);

	if( !ok ) {
		if( invalid_header )
			VI_log("Failed to load - not a valid save game file\n");
		else if( invalid_version )
			VI_log("Failed to load - save game file is from a different version of Conquests\n");
		else
			VI_log("Failed to load - game data corrupt or outdated\n");
/*#ifdef _WIN32
		if( invalid_header )
			MessageBoxA(NULL, "Failed to load - not a valid save game file", "Error", MB_OK|MB_ICONEXCLAMATION);
		else if( invalid_version )
			MessageBoxA(NULL, "Failed to load - save game file is from a different version of Conquests", "Error", MB_OK|MB_ICONEXCLAMATION);
		else
			MessageBoxA(NULL, "Failed to load - game data corrupt or outdated", "Error", MB_OK|MB_ICONEXCLAMATION);
#endif*/
		string message;
		if( invalid_header )
			message = "Failed to load - not a valid save game file";
		else if( invalid_version )
			message = "Failed to load - save game file is from a different version of Conquests";
		else
			message = "Failed to load - game data corrupt or outdated";
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), message.c_str(), game_g->getFont(), game_g->getPanelTexture());
		window->doModal();
		delete window;
		if( this->gamestate_started ) {
			// can't recover from corrupt game file when in main game, need to quit!
			//quit = true;
			// can't recover from corrupt game file when in main game, so we return to the main intro screen
			OptionsGamestate *optionsGamestate = new OptionsGamestate();
			game_g->pushNewGamestate(optionsGamestate);
		}
		else {
			this->clearGameData();
		}
	}

	if( ok ) {
		this->calculateTerritory();
		this->refreshMapDisplay();
	}
	if( ok && this->gamestate_started ) {
		this->generateTerrain();
		this->home();
	}
	if( ok && this->player != NULL ) {
		this->player->calculateFogOfWar(true);
	}

	return ok;
}

void MainGamestate::cityListClickAction(VI_Panel *source) {
	vector<const City *> *cities = static_cast<vector<const City *> *>(source->getUserData());
	ASSERT( cities != NULL );
	VI_Listbox *listbox = dynamic_cast<VI_Listbox *>(source);
	int active = listbox->getActive();
	ASSERT( active >= 0 && active < cities->size() );
	const City *city = cities->at(active);
	ASSERT( city != NULL );
	mainGamestate->centre(city->getPos());
}

void MainGamestate::citiesAdvisor() {
	vector<const City *> cities;
	vector<string> city_names;
	for(size_t i=0;i<player->getNCities();i++) {
		const City *city = player->getCity(i);
		stringstream text;
		//text << city->getName() << ": Size " << city->getSize() << ", Production: " << city->getProduction() << ", Science: " << city->getScience();
		//text << city->getName() << ": Pop " << city->getPopulation() << ", Production: " << city->getProduction() << ", Science: " << city->getScience();
		text << city->getName() << ": Pop " << city->getPopulationString() << ", Production: " << city->calculateProduction() << ", Science: " << city->calculateScience();
		if( city->getBuildable() != NULL ) {
			//text << ", Building " << city->getBuildable()->getName() << " ( " << city->getProgressBuildable() << " / " << city->getBuildable()->getCost() << " )";
			text << ", " << city->getBuildable()->getName() << " ( " << city->getProgressBuildable() << " / " << city->getBuildable()->getCost() << " )";
		}
		cities.push_back( city );
		city_names.push_back( text.str() );
	}

	string buttons[2];
	int n_buttons = 0;
	if( city_names.size() == 0 ) {
		buttons[0] = "Okay";
		n_buttons = 1;
	}
	else {
		buttons[0] = "View";
		buttons[1] = "Close";
		n_buttons = 2;
	}
	const int window_x_c = city_names.size() == 0 ? infowindow_def_x_c : 16;
	const int window_w_c = city_names.size() == 0 ? infowindow_def_w_c : game_g->getGraphicsEnvironment()->getWidth() - 2*window_x_c;
	const int window_h_c = city_names.size() == 0 ? infowindow_def_h_c : game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons, window_x_c, infowindow_def_y_c, window_w_c, window_h_c);
	VI_Panel *windowPanel = window->getPanel();

	VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), window_w_c - 32, city_names.size() == 0 ? window_h_c - 64 : 64);
	if( city_names.size() == 0 ) {
		textfield->addText("We have yet to build any cities.");
	}
	else {
		textfield->addText("You have the following cities:");
	}
	textfield->setForeground(1.0f, 1.0f, 1.0f);
	windowPanel->addChildPanel(textfield, 16, 16);

	VI_Listbox *listbox = NULL;
	if( city_names.size() > 0 ) {
		listbox = VI_createListbox(window_w_c - 32, window_h_c - 128, &*city_names.begin(), city_names.size(), game_g->getFont());
		initListbox(listbox);
		listbox->setHasInputFocus(true);
		//listbox->setForeground(1.0f, 1.0f, 1.0f);
		listbox->setActive(0);
		listbox->setAction(cityListClickAction);
		listbox->setUserData(&cities);
		windowPanel->addChildPanel(listbox, 16, 80);
	}

	int res = window->doModal();
	int active = -1;
	if( listbox != NULL ) {
		active = listbox->getActive() ;
		ASSERT( active >= 0 && active < player->getNCities() );
	}

	delete window;
	/*delete textfield;
	if( listbox != NULL ) {
		delete listbox;
	}*/

	if( res == 0 && active != -1 ) {
		City *city = player->getCity(active);
		this->centre(city->getPos());
		new CityWindow(this, city);
	}
}

void MainGamestate::unitListClickAction(VI_Panel *source) {
	vector<Unit *> *units = static_cast<vector<Unit *> *>(source->getUserData());
	ASSERT( units != NULL );
	VI_Listbox *listbox = dynamic_cast<VI_Listbox *>(source);
	int active = listbox->getActive();
	ASSERT( active >= 0 && active < units->size() );
	const Unit *unit = units->at(active);
	if( unit != NULL ) {
		mainGamestate->centre(unit->getPos());
	}
}

void MainGamestate::unitsAdvisorGetUnits(vector<Unit *> *units, vector<string> *unit_names, vector< vector<Unit *> > list_of_lists) {
	units->clear();
	unit_names->clear();
	for(vector< vector<Unit *> >::iterator iter = list_of_lists.begin(); iter != list_of_lists.end(); ++iter) {
		vector<Unit *> *list = &*iter;
		bool any = false;
		for(vector<Unit *>::iterator iter2 = list->begin(); iter2 != list->end(); ++iter2) {
			Unit *unit = *iter2;
			if( !any ) {
				any = true;
				units->push_back(NULL);
				stringstream str;
				str << unit->getTemplate()->getName() << " (" << list->size() << " units):";
				unit_names->push_back(str.str().c_str());
			}
			units->push_back(unit);
			stringstream name;
			name << "    " << getUnitName(unit, false);
			unit_names->push_back(name.str().c_str());
		}
	}
}

void MainGamestate::unitsAdvisor() {
	//vector<string> unit_names = game->getUnitNames( player->getUnits() );
	vector< vector<Unit *> > list_of_lists;
	// full with initial dummy lists
	/*for(vector<const UnitTemplate *>::iterator iter = this->unit_templates.begin(); iter != this->unit_templates.end(); ++iter) {
		const UnitTemplate *unit_template = *iter;*/
	for(size_t i=0;i<this->getNUnitTemplates();i++) {
		const UnitTemplate *unit_template = this->getUnitTemplate(i);
		vector<Unit *> list;
		list_of_lists.push_back(list);
	}
	// populate lists
	for(size_t i=0;i<player->getNUnits();i++) {
		Unit *unit = player->getUnit(i);
		const UnitTemplate *unit_template = unit->getTemplate();
		bool found = false;
		/*for(int j=0;j<this->unit_templates.size() && !found;j++) {
			const UnitTemplate *store_unit_template = this->unit_templates.at(j);*/
		for(size_t j=0;j<this->getNUnitTemplates() && !found;j++) {
			const UnitTemplate *store_unit_template = this->getUnitTemplate(j);
			if( store_unit_template == unit_template ) {
				found = true;
				vector<Unit *> *list = &list_of_lists.at(j);
				list->push_back(unit);
			}
		}
		ASSERT( found );
		/*for(vector< vector<Unit *> >::iterator iter = list_of_lists.begin(); iter != list_of_lists.end() && !found; ++iter) {
		vector<Unit *> *list = &*iter;
		if( unit_template == list->at(0)->getTemplate() ) {
		list->push_back(unit);
		found = true;
		}
		}
		if( !found ) {
		vector<Unit *> list;
		list.push_back(unit);
		list_of_lists.push_back(list);
		}*/
	}
	vector<Unit *> units;
	vector<string> unit_names;
	unitsAdvisorGetUnits(&units, &unit_names, list_of_lists);

	//vector<string> unit_names = game->getUnitNames( &units );
	/*vector<string> unit_names;
	for(vector<Unit *>::const_iterator iter = units.begin();iter != units.end(); ++iter) {
	const Unit *unit = *iter;
	string name = getUnitName(unit);
	unit_names.push_back(name.c_str());
	}*/

	string buttons[5];
	int n_buttons = 0;
	if( unit_names.size() == 0 ) {
		buttons[0] = "Okay";
		n_buttons = 1;
	}
	else {
		buttons[0] = "Activate";
		buttons[1] = "Activate All Of This Type";
		buttons[2] = "Activate All Automated Units Of This Type";
		buttons[3] = "Close";
		n_buttons = 4;
	}
	const int window_x_c = unit_names.size() == 0 ? infowindow_def_x_c : 16;
	const int window_w_c = unit_names.size() == 0 ? infowindow_def_w_c : game_g->getGraphicsEnvironment()->getWidth() - 2*window_x_c;
	//const int window_w_c = 560;
	const int window_h_c = unit_names.size() == 0 ? infowindow_def_h_c : 400;
	//const int window_h_c = infowindow_def_h_c;
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons, window_x_c, infowindow_def_y_c, window_w_c, window_h_c);
	//window->clearActiveWindow(); // shouldn't be set as the active window, otherwise may get deleted!
	VI_Panel *windowPanel = window->getPanel();

	VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), window_w_c - 32, unit_names.size() == 0 ? window_h_c - 64 : 32);
	if( unit_names.size() == 0 ) {
		textfield->addText("We have no units.");
	}
	else {
		textfield->addText("You have the following units:");
	}
	textfield->setForeground(1.0f, 1.0f, 1.0f);
	windowPanel->addChildPanel(textfield, 16, 16);

	VI_Listbox *listbox = NULL;
	if( unit_names.size() > 0 ) {
		listbox = VI_createListbox(window_w_c - 32, window_h_c - 96, &*unit_names.begin(), unit_names.size(), game_g->getFont());
		initListbox(listbox);
		listbox->setHasInputFocus(true);
		//listbox->setForeground(1.0f, 1.0f, 1.0f);
		listbox->setActive(0);
		listbox->setAction(unitListClickAction);
		listbox->setUserData(&units);
		windowPanel->addChildPanel(listbox, 16, 48);
	}

	bool done = false;
	int res = -1;
	int active = -1;
	while( !done ) {
		done = true;
		res = window->doModal();
		if( listbox != NULL ) {
			active = listbox->getActive() ;
			ASSERT( active >= 0 && active < units.size() );
			if( res == 0 ) {
				// activate
				Unit *unit = units.at(active);
				if( unit != NULL && unit->getStatus() != Unit::STATUS_NORMAL ) {
					done = false; // don't close window, only activate the unit
				}
				else if( unit != NULL && unit->isAutomated() ) {
					done = false; // don't close window, only activate the unit
				}

				if( unit == NULL ) {
					// no unit
					done = false; // don't close window
				}
				else if( unit->hasMovesLeft() ) {
					this->activateUnit(unit, true);
				}
				else {
					this->wakeupUnit(unit);
				}
			}
			else if( res == 1 || res == 2 ) {
				// activate all [automated units] of this type
				Unit *selected_unit = units.at(active);
				if( selected_unit == NULL ) {
					// no unit
					done = false; // don't close window
				}
				else {
					done = false; // don't close window, only activate the unit(s)
					bool found = false;
					for(size_t i=0;i<player->getNUnits();i++) {
						Unit *unit = player->getUnit(i);
						if( unit->getTemplate() != selected_unit->getTemplate() ) {
							// not of same type
						}
						else if( res == 2 && !unit->isAutomated() ) {
							// not automated
						}
						else if( !found && unit->hasMovesLeft() ) {
							// activate the first one we can
							found = true;
							this->activateUnit(unit, false);
						}
						else {
							// deal with the rest
							//unit->setStatus(Unit::STATUS_NORMAL);
							this->wakeupUnit(unit);
						}
					}
				}
			}
		}
		if( !done ) {
			// reset
			unitsAdvisorGetUnits(&units, &unit_names, list_of_lists);
			listbox->reset(&*unit_names.begin(), unit_names.size());
		}
	}

	delete window;
	/*delete textfield;
	if( listbox != NULL ) {
		delete listbox;
	}*/
}

int MainGamestate::getMinTurnsWar(const Civilization *civ1, const Civilization *civ2) const {
	// how long until civ1 considers making peace with civ2?
	const int min_turns_war_c = 8;
	int min_turns_war = min_turns_war_c;
	if( this->aiaggression == AIAGGRESSION_AGGRESSIVE )
		min_turns_war *= 2;
	int dummy = 0;
	int civ1_strength = civ1->calculateMilitary(&dummy);
	int civ2_strength = civ2->calculateMilitary(&dummy);
	int civ2_nuclear_strength = civ2->calculateNuclear();
	if( civ2_nuclear_strength >= 2 ) {
		min_turns_war = 1;
	}
	else if( 1.5 * civ1_strength < civ2_strength ) {
		min_turns_war /= 2;
	}
	else if( civ1_strength > 1.5 * civ2_strength ) {
		min_turns_war *= 2;
	}
	return min_turns_war;
}

// n.b., TalkCode is a global rather than local to MainGamestate, otherwise on Linux we are unable to use it in a vector
enum TalkCode {
	TALKCODE_DECLAREWAR = 0,
	TALKCODE_MAKEPEACE = 1,
	TALKCODE_MAKERIGHTOFPASSAGE = 2,
	TALKCODE_BREAKRIGHTOFPASSAGE = 3,
	TALKCODE_TECHEXCHANGE = 4,
	TALKCODE_MAPSEXCHANGE = 5,
	TALKCODE_GOODBYE = 6
};

void MainGamestate::civsAdvisor() {
	if( !CHEAT && player->isDead() ) {
		return;
	}
	vector<Civilization *> contact_civs;
	vector<string> civilization_names;
	for(vector<Civilization *>::const_iterator iter = this->civilizations.begin();iter != this->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		if( civilization != player && !civilization->isDead() && !civilization->getRace()->isDummy() ) {
			stringstream name;
			name << civilization->getName();
			//if( !CHEAT ) {
			if( !player->isDead() ) {
				const Relationship *relationship = this->findRelationship(player, civilization);
				if( !CHEAT && !relationship->madeContact() ) {
					continue;
				}
				if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
					if( relationship->hasRightOfPassage() )
						name << " (PEACE, RIGHT OF PASSAGE)";
					else
						name << " (PEACE)";
				}
				else if( relationship->getStatus() == Relationship::STATUS_WAR )
					name << " (AT WAR)";
			}
			bool any = false;
			for(vector<Civilization *>::const_iterator iter2 = this->civilizations.begin();iter2 != this->civilizations.end(); ++iter2) {
				Civilization *civilization2 = *iter2;
				if( civilization2 != civilization && civilization2 != player && !civilization2->isDead() && !civilization2->getRace()->isDummy() ) {
					Relationship *relationship2 = this->findRelationship(civilization2, civilization);
					if( relationship2->getStatus() == Relationship::STATUS_WAR ) {
						if( !any ) {
							any = true;
							name << " (War with ";
						}
						else {
							name << ", ";
						}
						name << civilization2->getName();
					}
				}
			}
			if( any ) {
				name << ")";
			}
			any = false;
			for(vector<Civilization *>::const_iterator iter2 = this->civilizations.begin();iter2 != this->civilizations.end(); ++iter2) {
				Civilization *civilization2 = *iter2;
				if( civilization2 != civilization && civilization2 != player && !civilization2->isDead() ) {
					Relationship *relationship2 = this->findRelationship(civilization2, civilization);
					if( relationship2->hasRightOfPassage() ) {
						if( !any ) {
							any = true;
							name << " (ROP with ";
						}
						else {
							name << ", ";
						}
						name << civilization2->getName();
					}
				}
			}
			if( any ) {
				name << ")";
			}
			civilization_names.push_back(name.str().c_str());
			contact_civs.push_back(civilization);
		}
	}

	string buttons[2];
	int n_buttons = 0;
	if( civilization_names.size() == 0 || player->isDead() ) {
		buttons[0] = "Okay";
		n_buttons = 1;
	}
	else {
		buttons[0] = "Contact";
		buttons[1] = "Cancel";
		n_buttons = 2;
	}

	int res = -1;
	int active = -1;
	{
		const int window_x_c = civilization_names.size() == 0 ? infowindow_def_x_c : 16;
		const int window_w_c = civilization_names.size() == 0 ? infowindow_def_w_c : game_g->getGraphicsEnvironment()->getWidth() - 2*window_x_c;
		const int window_h_c = civilization_names.size() == 0 ? infowindow_def_h_c : game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons, window_x_c, infowindow_def_y_c, window_w_c, window_h_c);
		VI_Panel *windowPanel = window->getPanel();

		VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), window_w_c - 32, civilization_names.size() == 0 ? window_h_c - 64 : 64);
		if( civilization_names.size() == 0 ) {
			textfield->addText("We know of no other Civilizations!");
		}
		else {
			textfield->addText("We have made contact with the following civilizations:");
		}
		//textfield->setForeground(1.0f, 1.0f, 0.0f);
		textfield->setForeground(1.0f, 1.0f, 1.0f);
		windowPanel->addChildPanel(textfield, 16, 16);

		VI_Listbox *listbox = NULL;
		if( civilization_names.size() > 0 ) {
			//listbox = VI_createListbox(window_w_c - 32, window_h_c - 128, &*civilization_names.begin(), civilization_names.size(), game_g->getFont());
			listbox = VI_createListbox(window_w_c - 32, window_h_c - 128, &*civilization_names.begin(), civilization_names.size(), game_g->getSmallFont());
			initListbox(listbox);
			listbox->setHasInputFocus(true);
			//listbox->setForeground(1.0f, 1.0f, 1.0f);
			listbox->setActive(0);
			windowPanel->addChildPanel(listbox, 16, 80);
		}

		res = window->doModal();
		if( listbox != NULL ) {
			active = listbox->getActive() ;
			ASSERT( active >= 0 && active < contact_civs.size() );
		}

		delete window;
		/*delete textfield;
		if( listbox != NULL ) {
			delete listbox;
		}*/
	}

	if( res == 0 && active != -1 && !player->isDead() ) {
		Civilization *civilization = contact_civs.at(active);
		Relationship *relationship = this->findRelationship(player, civilization);
		if( !relationship->madeContact() ) {
			T_ASSERT( CHEAT );
		}
		else {
			//ASSERT( relationship->madeContact() );
			//string buttons[] = {"Declare War", "Cancel"};
			stringstream text;
			vector<string> talkButtons;
			vector<TalkCode> talkCodes;
			vector<const Technology *> player_distinct_technologies;
			vector<const Technology *> civ_distinct_technologies;

			text << civilization->getRace()->getNameLeader();
			text << ": ";
			if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
				/*text << "Greetings from the ";
				text << civilization->getNameAdjective();
				text << " people. How may we help you?";*/
				text << civilization->getAIText(Civilization::AITEXT_OPENING_AT_PEACE);

				talkButtons.push_back("Declare War");
				talkCodes.push_back(TALKCODE_DECLAREWAR);

				if( relationship->hasRightOfPassage() ) {
					talkButtons.push_back("Break Right Of Passage");
					talkCodes.push_back(TALKCODE_BREAKRIGHTOFPASSAGE);
				}
				else if( relationship->canMakeRightOfPassage() ) {
					talkButtons.push_back("Make Right Of Passage");
					talkCodes.push_back(TALKCODE_MAKERIGHTOFPASSAGE);
				}

				Civilization::getDistinctTechnologies(this, &player_distinct_technologies, &civ_distinct_technologies, player, civilization);
				if( player_distinct_technologies.size() > 0 && civ_distinct_technologies.size() > 0 ) {
					talkButtons.push_back("Exchange Technology");
					talkCodes.push_back(TALKCODE_TECHEXCHANGE);
				}

				if( player->canShareMaps(civilization) && player->compareMaps(civilization) ) {
					talkButtons.push_back("Exchange Maps");
					talkCodes.push_back(TALKCODE_MAPSEXCHANGE);
				}
			}
			else if( relationship->getStatus() == Relationship::STATUS_WAR ) {
				/*text << "You are an enemy of ";
				text << civilization->getName();
				text << " and we will wipe you from the face of the Earth!";*/
				//text << "What do you want?";
				text << civilization->getAIText(Civilization::AITEXT_OPENING_AT_WAR);
				talkButtons.push_back("Make Peace");
				talkCodes.push_back(TALKCODE_MAKEPEACE);
			}
			else {
				ASSERT( false );
			}

			talkButtons.push_back("Goodbye");
			talkCodes.push_back(TALKCODE_GOODBYE);

			VI_Texture *civ_texture = civilization->getTexture();
			int n_lines = (talkButtons.size()-1) / 3 + 1;
			int talk_window_h_c = civ_texture->getHeight() + 64 + 32*n_lines;
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), &*talkButtons.begin(), talkButtons.size(), infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, talk_window_h_c, true);
			window->getButton(0)->disableKeyShortcut(); // disable key shortcut - probably not best to do something like declaring war if the user hits Return!
			VI_Panel *windowPanel = window->getPanel();

			VI_ImageButton *imagebutton = VI_createImageButton(civ_texture);
			windowPanel->addChildPanel(imagebutton, 16, 48);

			//VI_Stringgadget *title = VI_createStringgadget(this->getFont(), infowindow_def_w_c - 32);
			VI_Stringgadget *title = VI_createStringgadget(game_g->getFont(), imagebutton->getWidth());
			const unsigned char *rgb = civilization->getColor();
			title->setBackground(rgb[0]/255.0f, rgb[1]/255.0f, rgb[2]/255.0f);
			title->setForeground(0.0f, 0.0f, 0.0f);
			//title->setForeground(rgb[0]/255.0f, rgb[1]/255.0f, rgb[2]/255.0f);
			title->addText(civilization->getName());
			windowPanel->addChildPanel(title, 16, 16);

			//VI_Textfield *textfield = VI_createTextfield(game_g->getFont(),  infowindow_def_w_c - imagebutton->getWidth() - 48, talk_window_h_c - 64);
			VI_Textfield *textfield = VI_createTextfield(game_g->getFont(),  infowindow_def_w_c - imagebutton->getWidth() - 48, civilization->getTexture()->getHeight() + 32);
			//textfield->setForeground(1.0f, 1.0f, 0.0f);
			textfield->setForeground(1.0f, 1.0f, 1.0f);
			textfield->addText(text.str().c_str());
			windowPanel->addChildPanel(textfield, imagebutton->getWidth() + 32, 16);

			res = window->doModal();
			delete window;
			//delete imagebutton;

			TalkCode talkCode = talkCodes.at(res);
			if( talkCode == TALKCODE_DECLAREWAR ) {
				relationship->setStatus(Relationship::STATUS_WAR);
				string response = civilization->getAIText(Civilization::AITEXT_DECLARED_WAR);
				//InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "Prepare to die!", game_g->getFont(), game_g->getPanelTexture());
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), response.c_str(), game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
			}
			else if( talkCode == TALKCODE_BREAKRIGHTOFPASSAGE ) {
				relationship->setRightOfPassage(false);
				string response = civilization->getAIText(Civilization::AITEXT_BREAK_RIGHTOFPASSAGE);
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), response.c_str(), game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
			}
			else if( talkCode == TALKCODE_MAKERIGHTOFPASSAGE ) {
				int year_rop = relationship->getYearConsiderROP(player);
				if( year_rop == -1 || this->year >= year_rop ) {
					relationship->setRightOfPassage(true);
					string response = civilization->getAIText(Civilization::AITEXT_MAKE_RIGHTOFPASSAGE_YES);
					InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), response.c_str(), game_g->getFont(), game_g->getPanelTexture());
					window->doModal();
					delete window;
				}
				else {
					string response = civilization->getAIText(Civilization::AITEXT_MAKE_RIGHTOFPASSAGE_NO);
					InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), response.c_str(), game_g->getFont(), game_g->getPanelTexture());
					window->doModal();
					delete window;
				}
			}
			else if( talkCode == TALKCODE_TECHEXCHANGE ) {
				ASSERT( player_distinct_technologies.size() > 0 && civ_distinct_technologies.size() > 0 );
				vector<string> technology_names;
				for(vector<const Technology *>::const_iterator iter = civ_distinct_technologies.begin(); iter != civ_distinct_technologies.end(); ++iter) {
					const Technology *technology = *iter;
					technology_names.push_back(technology->getName());
				}
				int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
				int active = InfoWindow::select(game_g->getGraphicsEnvironment(), this->getPanel(), game_g->getFont(), game_g->getPanelTexture(), "Which technology would you like?", "Okay", "Cancel", window_h_c, &technology_names);
				if( active != -1 ) {
					ASSERT( active >= 0 && active < civ_distinct_technologies.size() );
					player->addTechnology( civ_distinct_technologies.at( active ) );

					// now AI picks one of the player's technologies
					int choose = rand() % player_distinct_technologies.size();
					const Technology *technology = player_distinct_technologies.at( choose );
					civilization->addTechnology( technology );
					stringstream text;
					text << civilization->getName() << " take " << technology->getName() << ".";
					InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
					window->doModal();
					delete window;
				}
			}
			else if( talkCode == TALKCODE_MAPSEXCHANGE ) {
				player->shareMaps(civilization);
			}
			else if( talkCode == TALKCODE_MAKEPEACE ) {
				string text;
				int min_turns_war = this->getMinTurnsWar(civilization, player);
				if( year >= relationship->getYearDeclaredWar() + min_turns_war ) {
					//text = "Let us put an end to this bloodshed.";
					text = civilization->getAIText(Civilization::AITEXT_MAKE_PEACE_YES);
					relationship->setStatus(Relationship::STATUS_PEACE);
				}
				else {
					//text = "Never! We will keep fighting until you are destroyed!";
					text = civilization->getAIText(Civilization::AITEXT_MAKE_PEACE_NO);
				}
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text, game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
			}
			else if( talkCode == TALKCODE_GOODBYE ) {
				// simply close window
			}
			else {
				ASSERT( false );
			}
		}
	}
}

void MainGamestate::techsAdvisor() {
	vector<string> technology_names;
	vector<const Technology *> technologies;
	/*for(vector<Technology *>::const_iterator iter = this->technologies.begin();iter != this->technologies.end(); ++iter) {
		const Technology *technology = *iter;*/
	for(size_t i=0;i<this->getNTechnologies();i++) {
		const Technology *technology = this->getTechnology(i);
		if( player->hasTechnology(technology) ) {
			stringstream name;
			name << technology->getName();
			name << " (" << technology->getAgeName() << ")";
			technology_names.push_back(name.str().c_str());
			technologies.push_back(technology); // need to make a separate list, as the order maintained in the Civilization class will in general be a different order!
		}
	}

	//int window_h = technology_names.size() == 0 ? 128 : 512;
	int window_h = technology_names.size() == 0 ? 128 : game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 16;
	const int cheat_info_h_c = 256;
	if( technology_names.size() == 0 && CHEAT ) {
		window_h += cheat_info_h_c + 16;
	}
	int list_h = technology_names.size() == 0 ? 0 : window_h - 128;
	if( CHEAT && technology_names.size() > 0 ) {
		list_h -= cheat_info_h_c - 16;
	}
	string okay = "Okay";
	int width_c = game_g->getGraphicsEnvironment()->getWidth() - 128;
	width_c = min(width_c, 640);
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), &okay, 1, 64, infowindow_def_y_c, width_c, window_h);
	VI_Panel *windowPanel = window->getPanel();

	VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), width_c - 32, 64);
	if( technology_names.size() == 0 ) {
		textfield->addText("We have yet to discover anything!");
	}
	else {
		textfield->addText("We have discovered the following technologies:");
	}
	//textfield->setForeground(1.0f, 1.0f, 0.0f);
	textfield->setForeground(1.0f, 1.0f, 1.0f);
	windowPanel->addChildPanel(textfield, 16, 16);

	VI_Listbox *listbox = NULL;
	if( technology_names.size() > 0 ) {
		//listbox = VI_createListbox(width_c - 32, list_h, &*technology_names.begin(), technology_names.size(), game_g->getFont());
		listbox = VI_createListbox((width_c - 32)/2, list_h, &*technology_names.begin(), technology_names.size(), game_g->getFont());
		initListbox(listbox);
		listbox->setHasInputFocus(true);
		//listbox->setForeground(1.0f, 1.0f, 1.0f);
		listbox->setActive(0);
		windowPanel->addChildPanel(listbox, 16, 80);

		VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), (width_c - 32)/2, list_h);
		windowPanel->addChildPanel(textfield, 16 + (width_c - 32)/2, 80);

		AskTechnologyInfo askTechnologyInfo;
		askTechnologyInfo.textfieldInfo = textfield;
		//askTechnologyInfo.player_technologies = player->getTechnologies();
		askTechnologyInfo.player_technologies = &technologies;
		listbox->setUserData(&askTechnologyInfo);
		listbox->setAction(askTechnologyAction);
		askTechnologyAction(listbox);
	}

	VI_Textfield *textfield_cheat = NULL;
	if( CHEAT ) {
		textfield_cheat = VI_createTextfield(game_g->getFont(), width_c - 32, cheat_info_h_c);
		stringstream str;
		for(vector<Civilization *>::const_iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
			const Civilization *civilization = *iter;
			if( civilization == player || civilization->isDead() ) {
				continue;
			}
			str << civilization->getName() << " (researching ";
			const Technology *tech = civilization->getTechnology();
			if( tech == NULL )
				str << "NOTHING";
			else {
				str << tech->getName();
				str << " " << civilization->getProgressTechnology() << " / " << tech->getCost(this->mainGamestate);
			}
			str << ") :\n\n";
			for(size_t i=0;i<this->getNTechnologies();i++) {
				const Technology *technology = this->getTechnology(i);
				if( civilization->hasTechnology(technology) ) {
					str << technology->getName() << "\n";
				}
			}
			str << "\n";
		}
		textfield_cheat->addText(str.str().c_str());
		textfield_cheat->setCPos(0);
		windowPanel->addChildPanel(textfield_cheat, 16, 80 + list_h);
	}

	window->doModal();

	delete window;
	/*delete textfield;
	if( listbox != NULL ) {
		delete listbox;
	}
	if( textfield_cheat != NULL ) {
		delete textfield_cheat;
	}*/
}

void MainGamestate::scoresAdvisor() {
	if( !CHEAT && player->isDead() ) {
		return;
	}
	stringstream text;
	text << "Difficulty Level: ";
	text << this->getDifficultyString();
	text << "\n\n";

	for(vector<Civilization *>::const_iterator iter = this->civilizations.begin();iter != this->civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		if( civilization->isDead() || civilization->getRace()->isDummy() ) {
			continue;
		}
		if( !CHEAT && civilization != player ) {
			const Relationship *relationship = this->findRelationship(player, civilization);
			if( !relationship->madeContact() ) {
				continue;
			}
		}
		text << civilization->getName() << ":\n";
		text << "Population: " << civilization->getTotalPopulationString() << "\n";
		text << "Cities: " << civilization->getNCities() << "\n";
		text << "Power: " << civilization->getPower() << " (" << civilization->calculatePowerPerTurn() << " per turn)\n";
		text << "Territory: " << civilization->calculateTerritory() << "\n";
		int n_land_units = 0;
		int land_power = civilization->calculateMilitary(&n_land_units);
		text << "Military Strength: " << land_power << " (" << n_land_units << " units)\n";
		int n_air_units = 0;
		int air_power = civilization->calculateAir(&n_air_units);
		if( air_power > 0 ) {
			text << "Air Strength: " << air_power << " (" << n_air_units << " units)\n";
		}
		int n_sea_units = 0;
		int sea_power = civilization->calculateSea(&n_sea_units);
		if( sea_power > 0 ) {
			text << "Sea Strength: " << sea_power << " (" << n_sea_units << " units)\n";
		}
		int nuclear_power = civilization->calculateNuclear();
		if( nuclear_power > 0 ) {
			text << "Nuclear Strength: " << nuclear_power << "\n";
		}
		text << "Science: " << civilization->calculateTotalScience() << "\n";
		text << "Production: " << civilization->calculateTotalProduction() << "\n";
		text << "\n";
	}
	const int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - 2*infowindow_def_y_c;
	/*const int n_buttons_c = 2;
	string buttons[n_buttons_c] = {"Okay", "History"};*/
	const int n_buttons_c = 7;
	string buttons[n_buttons_c] = {"Okay", "Power", "Population", "Production", "Science", "Territory", "Military"};
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, 500, window_h_c);
	window->createHiddenEscapeClose();
	//window->getButton(1)->setKeyShortcut(V_H);
	window->getButton(1)->setKeyShortcut(V_P);
	window->getButton(2)->setKeyShortcut(V_O);
	window->getButton(3)->setKeyShortcut(V_R);
	window->getButton(4)->setKeyShortcut(V_S);
	window->getButton(5)->setKeyShortcut(V_T);
	window->getButton(6)->setKeyShortcut(V_M);
	int res = window->doModal();
	delete window;
	/*if( res == 1 ) {
		this->showHistory(HISTORYTYPE_POWER);
	}*/
	if( res == 1 ) {
		this->showHistory(HISTORYTYPE_POWER);
	}
	else if( res == 2 ) {
		this->showHistory(HISTORYTYPE_POPULATION);
	}
	else if( res == 3 ) {
		this->showHistory(HISTORYTYPE_PRODUCTION);
	}
	else if( res == 4 ) {
		this->showHistory(HISTORYTYPE_SCIENCE);
	}
	else if( res == 5 ) {
		this->showHistory(HISTORYTYPE_TERRITORY);
	}
	else if( res == 6 ) {
		this->showHistory(HISTORYTYPE_MILITARYPOWER);
	}
}

void MainGamestate::mapPaintPanel(VI_Graphics2D *g2d, void *data) {
	const MapInfo *mapInfo = static_cast<const MapInfo *>(data);
	const MainGamestate *mainGamestate = mapInfo->mainGamestate;
	const Map *map = mainGamestate->getMap();

	if( mainGamestate->view_3d ) {
		float mx[4] = {0.0f, 0.0f, 0.0f, 0.0f};
		float my[4] = {0.0f, 0.0f, 0.0f, 0.0f};
		mainGamestate->getMapPosf(&mx[0], &my[0], 0, 0);
		mainGamestate->getMapPosf(&mx[1], &my[1], game_g->getGraphicsEnvironment()->getWidth()-1, 0);
		mainGamestate->getMapPosf(&mx[2], &my[2], 0, game_g->getGraphicsEnvironment()->getHeight()-1);
		mainGamestate->getMapPosf(&mx[3], &my[3], game_g->getGraphicsEnvironment()->getWidth()-1, game_g->getGraphicsEnvironment()->getHeight()-1);
		for(int i=0;i<4;i++) {
			mx[i] *=  mapInfo->image_w / (float)map->getWidth();
			my[i] *=  mapInfo->image_h / (float)map->getHeight();
		}
		g2d->setColor3i(255, 255, 255);
		bool clip_left = true;
		if( map->getTopology() == TOPOLOGY_FLAT && mx[0] < 0 ) {
			// so that we can use the same clipping code for the left hand side
			clip_left = false;
			for(int i=0;i<4;i++)
				mx[i] += mapInfo->image_w;
		}
		if( mx[2] >= mapInfo->image_w ) {
			// partial left wraparound
			float scale = ((float)(mx[2] - mapInfo->image_w)) / (float)(mx[2] - mx[0]);
			int iy = (int)(my[2] + (my[0] - my[2])*scale);
			if( map->getTopology() == TOPOLOGY_CYLINDER || !clip_left ) {
				g2d->draw(0, (short)my[0], (short)(mx[1] - mapInfo->image_w), (short)my[1]);
				g2d->draw((short)(mx[1] - mapInfo->image_w), (short)my[1], (short)(mx[3] - mapInfo->image_w), (short)my[3]);
				g2d->draw((short)(mx[3] - mapInfo->image_w), (short)my[3], (short)(mx[2] - mapInfo->image_w), (short)my[2]);
				g2d->draw((short)(mx[2] - mapInfo->image_w), (short)my[2], 0, iy);
			}
			if( map->getTopology() == TOPOLOGY_CYLINDER || clip_left ) {
				g2d->draw((short)mx[0], (short)my[0], mapInfo->image_w, (short)my[1]);
				g2d->draw(mapInfo->image_w, iy, (short)mx[0], (short)my[0]);
			}
		}
		else if( mx[3] >= mapInfo->image_w ) {
			// full wraparound
			if( map->getTopology() == TOPOLOGY_CYLINDER || !clip_left ) {
				g2d->draw(0, (short)my[0], (short)(mx[1] - mapInfo->image_w), (short)my[1]);
				g2d->draw((short)(mx[1] - mapInfo->image_w), (short)my[1], (short)(mx[3] - mapInfo->image_w), (short)my[3]);
				g2d->draw((short)(mx[3] - mapInfo->image_w), (short)my[3], 0, (short)my[2]);
			}
			if( map->getTopology() == TOPOLOGY_CYLINDER || clip_left ) {
				g2d->draw((short)mx[0], (short)my[0], (short)(mapInfo->image_w), (short)my[1]);
				g2d->draw(mapInfo->image_w, (short)my[3], (short)mx[2], (short)my[2]);
				g2d->draw((short)mx[2], (short)my[2], (short)mx[0], (short)my[0]);
			}
		}
		else if( mx[1] >= mapInfo->image_w ) {
			// partial right wraparound
			float scale = ((float)(mx[1] - mapInfo->image_w)) / (float)(mx[1] - mx[3]);
			int iy = (int)(my[1] + (my[3] - my[1])*scale);
			if( map->getTopology() == TOPOLOGY_CYLINDER || !clip_left ) {
				g2d->draw(0, (short)my[0], (short)(mx[1] - mapInfo->image_w), (short)my[1]);
				g2d->draw((short)(mx[1] - mapInfo->image_w), (short)my[1], 0, iy);
			}
			if( map->getTopology() == TOPOLOGY_CYLINDER || clip_left ) {
				g2d->draw((short)mx[0], (short)my[0], mapInfo->image_w, (short)my[1]);
				g2d->draw(mapInfo->image_w, iy, (short)mx[3], (short)my[3]);
				g2d->draw((short)mx[3], (short)my[3], (short)mx[2], (short)my[2]);
				g2d->draw((short)mx[2], (short)my[2], (short)mx[0], (short)my[0]);
			}
		}
		else {
			g2d->draw((short)mx[0], (short)my[0], (short)mx[1], (short)my[1]);
			g2d->draw((short)mx[1], (short)my[1], (short)mx[3], (short)my[3]);
			g2d->draw((short)mx[3], (short)my[3], (short)mx[2], (short)my[2]);
			g2d->draw((short)mx[2], (short)my[2], (short)mx[0], (short)my[0]);
		}
	}
	else {
		float n_x = ((float)game_g->getGraphicsEnvironment()->getWidth()) / (float)mainGamestate->getSqWidth();
		float n_y = ((float)game_g->getGraphicsEnvironment()->getHeight()) / (float)mainGamestate->getSqHeight();
		float frac_w = n_x / (float)map->getWidth();
		float frac_h = n_y / (float)map->getHeight();
		int box_w = (int)(frac_w * mapInfo->image_w);
		int box_h = (int)(frac_h * mapInfo->image_h);
		float frac_x = mainGamestate->map_pos_x / (float)map->getWidth();
		float frac_y = mainGamestate->map_pos_y / (float)map->getHeight();
		int box_x = (int)(frac_x * mapInfo->image_w);
		int box_y = (int)(frac_y * mapInfo->image_h);

		int wraparound_box_w = 0;
		// handle wraparound!
		if( map->getTopology() == TOPOLOGY_CYLINDER ) {
			if( box_x + box_w > mapInfo->image_w-1 ) {
				int new_box_w = mapInfo->image_w-1 - box_x;
				wraparound_box_w = box_w - new_box_w;
				box_w = new_box_w;
			}
		}

		g2d->setColor3i(255, 255, 255);
		if( box_w > 0 ) {
			g2d->drawRect(box_x, box_y, box_w, box_h);
		}
		if( wraparound_box_w > 0 ) {
			g2d->drawRect(0, box_y, wraparound_box_w, box_h);
		}
	}
}

void MainGamestate::mapMouseClickEventFunc(VI_MouseClickEvent *mouse_click_event, void *data) {
	if( mouse_click_event->left || mouse_click_event->right || mouse_click_event->middle ) {
		int mx = mouse_click_event->mouse_x - mainGamestate->mapImageButton->getX();
		int my = mouse_click_event->mouse_y - mainGamestate->mapImageButton->getY();
		if( mx >= 0 && mx < mainGamestate->mapImageButton->getWidth() && my >= 0 && my < mainGamestate->mapImageButton->getHeight() ) { // should always be true, but just in case...
			mouse_click_event->handled = true;
			float map_x = ((float)(mx * mainGamestate->getMap()->getWidth())) / (float)mainGamestate->mapImageButton->getWidth();
			float map_y = ((float)(my * mainGamestate->getMap()->getHeight())) / (float)mainGamestate->mapImageButton->getHeight();
			mainGamestate->centre(map_x, map_y);
		}
	}
}

class GraphInfo {
public:
	string text;
	const VI_ImageButton *imageButton;
	int orig_image_width, orig_image_height;
	const Civilization **civs;
	const int *years;
	const int *values;

	GraphInfo(string text, const VI_ImageButton *imageButton, int orig_image_width, int orig_image_height, const Civilization **civs, const int *years, const int *values) : text(text), imageButton(imageButton), orig_image_width(orig_image_width), orig_image_height(orig_image_height), civs(civs), years(years), values(values) {
	}
};

void MainGamestate::graphPaintPanel(VI_Graphics2D *g2d, void *data) {
	const GraphInfo *graphInfo = static_cast<const GraphInfo *>(data);
	game_g->getFont()->writeTextExt(8, 8, "%s", graphInfo->text.c_str());
	bool done_civ = false;
	int m_x = 0, m_y = 0;
	bool m_buttons[3] = {false, false, false};
	game_g->getGraphicsEnvironment()->getMouseInfo(&m_x, &m_y, m_buttons);
	m_x -= graphInfo->imageButton->getX();
	m_y -= graphInfo->imageButton->getY();
	if( m_x >= 0 && m_x < graphInfo->imageButton->getWidth() && m_y >= 0 && m_y < graphInfo->imageButton->getHeight() ) {
		m_x *= graphInfo->orig_image_width;
		m_x /= graphInfo->imageButton->getWidth();
		m_x = min(m_x, graphInfo->orig_image_width-1);
		m_y *= graphInfo->orig_image_height;
		m_y /= graphInfo->imageButton->getHeight();
		m_y = min(m_y, graphInfo->orig_image_height-1);
		m_y = graphInfo->orig_image_height-1-m_y;
		//VI_log("%d, %d\n", m_x, m_y);
		const Civilization *civ = graphInfo->civs[m_y * graphInfo->orig_image_width + m_x];
		if( civ != NULL ) {
			done_civ = true;
			int value = graphInfo->values[m_y * graphInfo->orig_image_width + m_x];
			game_g->getFont()->writeTextExt(8, 24, "Year %d: %s: %d", graphInfo->years[m_x], civ->getName(), value);
		}
	}
	if( !done_civ ) {
		game_g->getFont()->writeText(8, 24, "(Move the mouse over the graph to see Civilization names)");
	}
}

void MainGamestate::drawGraph(HistoryType history_type, const Civilization *civilization, VI_Image *image, const Civilization **civs, int *years, int *values, int y_offset, int orig_image_width, int orig_image_height, int max_score) const {
	const unsigned char *rgb = civilization->getColor();
	const std::map<int, History> *history = civilization->getHistory();
	float y_scale = ((float)orig_image_height)/(float)(max_score+1);
	for(std::map<int, History>::const_iterator iter = history->begin(); iter != history->end(); ++iter) {
		int this_year = iter->first;
		const History *entry = &iter->second;
		T_ASSERT( this_year > 0 && this_year <= year );
		if( this_year > 0 && this_year <= year ) {
			//int score = entry->power;
			//int score = entry->science;
			int score = entry->getScore(history_type);
			int x = this_year-1;
			int y = score;
			// be careful of overflow!
			/*y *= orig_image_height;
			y /= (max_score+1);*/
			y *= y_scale;
			ASSERT( x >= 0 );
			ASSERT( x < orig_image_width );
			ASSERT( y >= 0 );
			ASSERT( y < orig_image_height );
			civs[y*orig_image_width+x] = civilization;
			years[x] = this_year;
			values[y*orig_image_width+x] = score;
			y += y_offset;
			//VI_log("%d, %d: %d, %d, %d\n", x, y, rgb[0], rgb[1], rgb[2]);
			ASSERT( x >= 0 );
			ASSERT( x < image->getWidth() );
			ASSERT( y >= 0 );
			ASSERT( y < image->getHeight() );
			image->putRGB(x, y, rgb[0], rgb[1], rgb[2]);
		}
	}
}

void MainGamestate::showHistory(HistoryType history_type) {
	int max_score = 0;
	for(vector<Civilization *>::const_iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		const Civilization *civilization = *iter;
		if( civilization->getRace()->isDummy() ) {
			continue;
		}
		const std::map<int, History> *history = civilization->getHistory();
		for(std::map<int, History>::const_iterator iter = history->begin(); iter != history->end(); ++iter) {
			const History *entry = &iter->second;
			//int score = entry->power;
			//int score = entry->science;
			int score = entry->getScore(history_type);
			max_score = max(max_score, score);
		}
	}
	VI_log("year: %d\n", year);
	VI_log("max_score: %d\n", max_score);

	// n.b., we resize the texture to a power of 2 size, rather than rescaling, as this doesn't look as good, and is harder to see detail in the map
	int orig_image_width = year;
	int orig_image_height = max_score+1;
	/*orig_image_width = max(orig_image_width, 10);
	orig_image_height = max(orig_image_height, 10);*/

	int window_x = 64;
	int window_y = 64;
	int border_x = 16, border_y = 16;
	int extra_x = 2 * border_x;
	int extra_y = 2 * border_y + 32;
	int onscreen_image_w = 64 * orig_image_width;
	int onscreen_image_h = 64 * orig_image_height;
	//VI_log("%d x %d\n", onscreen_image_w, onscreen_image_h);
	if( onscreen_image_w > game_g->getGraphicsEnvironment()->getWidth() - 2*window_x - extra_x ) {
		onscreen_image_w = game_g->getGraphicsEnvironment()->getWidth() - 2*window_x - extra_x;
	}
	if( onscreen_image_h > game_g->getGraphicsEnvironment()->getHeight() - 2*window_y - extra_y ) {
		onscreen_image_h = game_g->getGraphicsEnvironment()->getHeight() - 2*window_y - extra_y;
	}
	//onscreen_image_h = 50; // test

	// mustn't have orig_image_height > onscreen_image_h, otherwise we may not be able to see the 1-pixel-high graphs!
	// ...and in fact need it to be onscreen_image_h-1 for some reason
	orig_image_height = min(orig_image_height, onscreen_image_h-1);

	VI_log("%d x %d\n", orig_image_width, orig_image_height);
	int image_width = VI_getNextPower(orig_image_width, 2);
	int image_height = VI_getNextPower(orig_image_height, 2);
	int y_offset = image_height - orig_image_height;
	//VI_log("%d x %d\n", image_width, image_height);
	VI_Image *image = VI_createImage(image_width, image_height);
	image->setAllToi(0, 0, 0);
	const Civilization **civs = new const Civilization *[orig_image_width*orig_image_height];
	int *years = new int[orig_image_width];
	int *values = new int[orig_image_width*orig_image_height];
	for(int i=0;i<orig_image_width*orig_image_height;i++) {
		civs[i] = NULL;
		values[i] = 0;
	}
	for(int i=0;i<orig_image_width;i++) {
		years[i] = 0;
	}
	for(vector<Civilization *>::const_iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		const Civilization *civilization = *iter;
		if( civilization->getRace()->isDummy() ) {
			continue;
		}
		if( civilization != player ) {
			Relationship *relationship = this->findRelationship(player, civilization);
			ASSERT( relationship != NULL );
			if( SPY || relationship->madeContact() ) {
				drawGraph(history_type, civilization, image, civs, years, values, y_offset, orig_image_width, orig_image_height, max_score);
			}
		}
	}
	// do player last, so it can always been seen
	drawGraph(history_type, player, image, civs, years, values, y_offset, orig_image_width, orig_image_height, max_score);

	//image->save("test1.png", "png");
	if( !image->makePowerOf2() ) {
	    VI_log("Failed to create image\n");
#ifdef _WIN32
		MessageBoxA(NULL,"Failed to create image","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
		delete image;
		return;
	}
	//image->save("test2.png", "png");
	//VI_log("%d x %d\n", image->getWidth(), image->getHeight());
	VI_Texture *texture = VI_createTexture(image);
	texture->setDrawMode(VI_Texture::DRAWMODE_BLOCKY);
	texture->setWrapMode(VI_Texture::WRAPMODE_CLAMP);

	string okay = "Okay";
	int window_w = onscreen_image_w + extra_x;
	int window_h = onscreen_image_h + extra_y;
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), &okay, 1, window_x, window_y, window_w, window_h);
	VI_Panel *windowPanel = window->getPanel();
	VI_ImageButton *imageButton = VI_createImageButton(texture, onscreen_image_w, onscreen_image_h);
	float t_width = ((float)orig_image_width)/(float)texture->getWidth();
	float t_height = ((float)orig_image_height)/(float)texture->getHeight();
	float tu[4] = {0.0f, 0.0f, t_width, t_width};
	float tv[4] = {1.0f, 1.0f-t_height, 1.0f, 1.0f-t_height};
	imageButton->setTexCoords(tu, tv);
	string text;
	if( history_type == HISTORYTYPE_POWER ) {
		text = "Power";
	}
	else if( history_type == HISTORYTYPE_POPULATION ) {
		text = "Population";
	}
	else if( history_type == HISTORYTYPE_PRODUCTION ) {
		text = "Production";
	}
	else if( history_type == HISTORYTYPE_SCIENCE ) {
		text = "Science";
	}
	else if( history_type == HISTORYTYPE_TERRITORY ) {
		text = "Territory";
	}
	GraphInfo graphInfo(text, imageButton, orig_image_width, orig_image_height, civs, years, values);
	imageButton->setPaintFunc(graphPaintPanel, &graphInfo);

	windowPanel->addChildPanel(imageButton, border_x, border_y);

	window->doModal();

	delete window;
	delete texture;
	delete [] civs;
	delete [] years;
	delete [] values;
}

void MainGamestate::showMap() {
	if( this->mapImageButton != NULL ) {
		//this->mapImageButton->detach();
		delete this->mapImageButton;
		this->mapImageButton = NULL;
		return;
	}
	if( player == NULL ) {
		// no player, can't open map - happens when clearing game data
		return;
	}
	// n.b., we resize the texture to a power of 2 size, rather than rescaling, as this doesn't look as good, and is harder to see detail in the map
	int orig_image_width = map->getWidth();
	int orig_image_height = map->getHeight();
	int image_width = VI_getNextPower(orig_image_width, 2);
	int image_height = VI_getNextPower(orig_image_height, 2);
	VI_Image *image = VI_createImage(image_width, image_height);
	for(int y=0;y<map->getHeight();y++) {
		for(int x=0;x<map->getWidth();x++) {
			unsigned char r = 0, g = 0, b = 0;
			if( player->isExplored(x, y) ) {
				const MapSquare *square = map->getSquare(x, y);
				const City *city = square->getCity();
				if( city != NULL ) {
					const unsigned char *rgb = city->getCivilization()->getColor();
					r = rgb[0];
					g = rgb[1];
					b = rgb[2];
				}
				else if( square->isLand() ) {
					//g = 127;
					r = 65;
					g = 96;
					b = 53;
				}
				else {
					/*g = 64;
					b = 255;*/
					r = 63;
					g = 127;
					b = 200;
				}
			}
			//int fy = map->getHeight() - 1 - y; // texture coords are inverted
			int fy = image_height - 1 - y; // texture coords are inverted
			image->putRGB(x, fy, r, g, b);
		}
	}
	if( !image->makePowerOf2() ) {
	    VI_log("Failed to create image\n");
#ifdef _WIN32
		MessageBoxA(NULL,"Failed to create image","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
		delete image;
		return;
	}
	VI_Texture *texture = VI_createTexture(image);

	/*
	int window_x = 64;
	int window_y = 64;
	int border_x = 16, border_y = 16;
	int extra_x = 2 * border_x;
	int extra_y = 2 * border_y + 32;
	//int image_w = map->getWidth();
	//int image_h = map->getHeight();
	int image_w = 10 * map->getWidth();
	int image_h = 10 * map->getHeight();
	if( image_w > game_g->getGraphicsEnvironment()->getWidth() - 2*window_x - extra_x ) {
		image_w = game_g->getGraphicsEnvironment()->getWidth() - 2*window_x - extra_x;
	}
	if( image_h > game_g->getGraphicsEnvironment()->getHeight() - 2*window_y - extra_y ) {
		image_h = game_g->getGraphicsEnvironment()->getHeight() - 2*window_y - extra_y;
	}
	//int image_w = 2 * max(map->getWidth(), map->getHeight());
	//int image_h = image_w;
	string okay = "Okay";
	int window_w = image_w + extra_x;
	int window_h = image_h + extra_y;
	InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), &okay, 1, window_x, window_y, window_w, window_h);
	VI_Panel *windowPanel = window->getPanel();
	VI_ImageButton *imageButton = VI_createImageButton(texture, image_w, image_h);
	float t_width = ((float)orig_image_width)/(float)texture->getWidth();
	float t_height = ((float)orig_image_height)/(float)texture->getHeight();
	float tu[4] = {0.0f, 0.0f, t_width, t_width};
	//float tv[4] = {t_height, 0.0f, t_height, 0.0f};
	float tv[4] = {1.0f, 1.0f-t_height, 1.0f, 1.0f-t_height};
	imageButton->setTexCoords(tu, tv);

	MapInfo mapInfo;
	mapInfo.mainGamestate = this;
	mapInfo.image_w = image_w;
	mapInfo.image_h = image_h;
	imageButton->setPaintFunc(mapPaintPanel, &mapInfo);

	windowPanel->addChildPanel(imageButton, border_x, border_y);

	window->doModal();

	delete window;
	delete texture;
	delete imageButton;
	*/

	int image_w = 300;
	int image_h = 120;
	this->mapImageButton = VI_createImageButton(texture, image_w, image_h);
	float t_width = ((float)orig_image_width)/(float)texture->getWidth();
	float t_height = ((float)orig_image_height)/(float)texture->getHeight();
	float tu[4] = {0.0f, 0.0f, t_width, t_width};
	//float tv[4] = {t_height, 0.0f, t_height, 0.0f};
	float tv[4] = {1.0f, 1.0f-t_height, 1.0f, 1.0f-t_height};
	mapImageButton->setTexCoords(tu, tv);
	int xpos = game_g->getGraphicsEnvironment()->getWidth() - 100 - image_w;
	int ypos = game_g->getGraphicsEnvironment()->getHeight() - 44 - image_h;
	gamePanel->addChildPanel(mapImageButton, xpos, ypos);

	mapInfo.mainGamestate = this;
	mapInfo.image_w = image_w;
	mapInfo.image_h = image_h;
	mapImageButton->setPaintFunc(mapPaintPanel, &mapInfo);
	mapImageButton->setMouseClickEventFunc(mapMouseClickEventFunc, NULL);
}

void MainGamestate::checkMapView() {
	//VI_log("was: %f, %f\n", this->map_pos_x, this->map_pos_y);

	if( view_3d ) {
		VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
		/*Vector3D pos = vp->getPosition();
		Vector3D dir_l = game_g->getGraphicsEnvironment()->getDirectionOfPoint(vp, 0, 0);
		Vector3D dir_r = game_g->getGraphicsEnvironment()->getDirectionOfPoint(vp, game_g->getGraphicsEnvironment()->getWidth()-1, 0);*/
		float mx_l = 0.0f, mx_r = 0.0f, my_t = 0.0f, my_b = 0.0f, dummy = 0.0f;
		getMapPosf(&dummy, &my_t, 0, 0);
		getMapPosf(&mx_l, &my_b, 0, game_g->getGraphicsEnvironment()->getHeight()-1);
		getMapPosf(&mx_r, &dummy, game_g->getGraphicsEnvironment()->getWidth()-1, game_g->getGraphicsEnvironment()->getHeight()-1);
		if( map->getTopology() == TOPOLOGY_FLAT ) {
			if( mx_l < 0.0f ) {
				vp->move(-mx_l, 0.0f, 0.0f);
			}
			else if( mx_r > (float)map->getWidth() ) {
				vp->move(((float)map->getWidth()) - mx_r, 0.0f, 0.0f);
			}
		}
		if( my_t < 0.0f ) {
				vp->move(0.0f, 0.0f, -my_t);
		}
		else if( my_b > (float)map->getHeight() ) {
			vp->move(0.0f, 0.0f, ((float)map->getHeight()) - my_b);
		}

		getMapPosf(&mx_l, &my_t, 0, 0);
		float old_mx_l = mx_l;
		float old_my_t = my_t;
		this->getMap()->reduceToBase(&mx_l, &my_t);
		vp->move(mx_l - old_mx_l, 0.0f, my_t - old_my_t);
	}
	else {
		if( map->getTopology() == TOPOLOGY_FLAT ) {
			float map_limit_x = max(0.0f, map->getWidth() - ((float)game_g->getGraphicsEnvironment()->getWidth())/(float)this->getSqWidth());
			this->map_pos_x = max(this->map_pos_x, 0.0f);
			this->map_pos_x = min(this->map_pos_x, map_limit_x);
		}

		float map_limit_y = max(0.0f, map->getHeight() - ((float)game_g->getGraphicsEnvironment()->getHeight())/(float)this->getSqHeight());
		this->map_pos_y = max(this->map_pos_y, 0.0f);
		this->map_pos_y = min(this->map_pos_y, map_limit_y);
		//VI_log("    now: %f, %f\n", this->map_pos_x, this->map_pos_y);

		this->getMap()->reduceToBase(&this->map_pos_x, &this->map_pos_y);
	}
}

bool MainGamestate::canBomb(const UnitTemplate *unit_template, Pos2D source, Pos2D target) const {
	if( !unit_template->canBomb() ) {
		return false;
	}
	int dist = this->getMap()->distanceChebyshev(source, target);
	if( dist <= unit_template->getAirRange() ) {
		return true;
	}
	return false;
}

void MainGamestate::bomb(Unit *unit, Pos2D target) {
	{
		// checks
		City *src_city = map->findCity(unit->getPos());
		ASSERT( src_city != NULL );
		ASSERT( src_city->canLaunch(unit->getTemplate()) );
		ASSERT( unit->getTemplate()->canBomb() );
		ASSERT( this->canBomb(unit->getTemplate(), unit->getPos(), target) );
	}
	//const MapSquare *square = map->getSquare(target);
	//City *city = square->getCity();
	City *city = map->findCity(target);
	ASSERT( city != NULL );
	VI_log("%s %s bombards %s city of %s\n", unit->getCivilization()->getNameAdjective(), unit->getTemplate()->getName(), city->getCivilization()->getNameAdjective(), city->getName());
	ASSERT( city->getCivilization() != unit->getCivilization() );
	Relationship *relationship = this->findRelationship(city->getCivilization(), unit->getCivilization());
	ASSERT( relationship->getStatus() == Relationship::STATUS_WAR ); // should already have declared war before calling this function
	bool is_missile = unit->getTemplate()->isMissile();
	ASSERT( is_missile || unit->getTemplate()->getAirAttack() > 0 );

	//if( city->getCivilization() == player || unit->getCivilization() == player ) {
	if( city->getCivilization() == player ) {
		this->centre(city->getPos());
	}

	unit->incNBombed();

	if( !is_missile ) {
		unit->getCivilization()->updateFogOfWar(city->getPos(), true);
	}

	if( is_missile && unit->getTemplate()->getNuclearType() != UnitTemplate::NUCLEARTYPE_NONE && city->hasImprovement("SDI Lasers") ) {
		int r = rand() % 100;
		VI_log("SDI Lasers roll: %d\n", r);
		if( r < 80 ) {
			stringstream text;
			bool show_window = false;
			if( unit->getCivilization() == player ) {
				text << "Your nuke was shot down by enemy SDI Lasers!";
				show_window = true;
			}
			else if( city->getCivilization() == player ) {
				text << city->getName() << ": Your SDI Lasers have shot down ";
				text << unit->getCivilization()->getNameAdjective();
				text << " nukes!";
				show_window = true;
			}
			if( show_window ) {
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), 64, 16, infowindow_def_w_c, 180);
				window->doModal();
				delete window;
			}
			if( unit->getCivilization() == player && unit == active_unit ) {
				active_unit = NULL;
				updateUnitButtons();
			}
			delete unit;
			unit = NULL;
			return; // attack destroyed, so quit
		}
	}

	const Improvement *air_defence_improvement = city->getBestAirDefenceImprovement();
	if( air_defence_improvement != NULL && !is_missile ) {
		int best_defence = air_defence_improvement->getAirDefence();
		T_ASSERT( best_defence > 0 );
		VI_log("%s %s attack %s %s\n", unit->getCivilization()->getNameAdjective(), unit->getTemplate()->getName(), city->getCivilization()->getNameAdjective(), air_defence_improvement->getName());
		int attack = unit->getTemplate()->getAirAttack();
		VI_log("attack: %d\n", attack);
		VI_log("defence: %d\n", best_defence);
		int total = attack + best_defence;
		int r = rand() % total;
		if( r < attack ) {
			// attacker wins! Improvement isn't destroyed however; no need to inform player
		}
		else {
			// attacker loses
			stringstream text;
			bool show_window = false;
			if( unit->getCivilization() == player ) {
				text << "You were shot down by enemy ";
				text << air_defence_improvement->getName();
				text << ".";
				show_window = true;
			}
			else if( city->getCivilization() == player ) {
				text << city->getName() << ": Your ";
				text << air_defence_improvement->getName();
				text << " have shot down ";
				text << unit->getCivilization()->getNameAdjective();
				text << " ";
				text << unit->getTemplate()->getName();
				text << ".";
				show_window = true;
			}
			if( show_window ) {
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), 64, 16, infowindow_def_w_c, 180);
				window->doModal();
				delete window;
			}
			if( unit->getCivilization() == player && unit == active_unit ) {
				active_unit = NULL;
				updateUnitButtons();
			}
			delete unit;
			unit = NULL;
			return; // attack destroyed, so quit
		}
	}

	const vector<Unit *> *units = map->findUnitsAt(city->getPos());
	if( units->size() > 0 && !is_missile ) {
		// check for defending air units
		Unit *defender = NULL;
		int best_defence = 0;
		for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter) {
			Unit *this_unit = *iter;
			if( this_unit->getTemplate()->isAir() && !this_unit->getTemplate()->isMissile() && this_unit->getTemplate()->getAirDefence() > 0 ) {
				if( !city->canLaunch(this_unit->getTemplate()) ) {
					// fighter unit can't be launched, as airport no longer available
					continue;
				}
				int defence = this_unit->getTemplate()->getAirDefence();
				// find best air defender
				if( defender == NULL || defence > best_defence ) {
					defender = this_unit;
					best_defence = defence;
				}
			}
		}
		if( defender != NULL ) {
			VI_log("%s %s attack %s %s\n", unit->getCivilization()->getNameAdjective(), unit->getTemplate()->getName(), defender->getCivilization()->getNameAdjective(), defender->getTemplate()->getName());
			ASSERT( defender->getCivilization() == city->getCivilization() );
			int attack = unit->getTemplate()->getAirAttack();
			VI_log("attack: %d\n", attack);
			VI_log("defence: %d\n", best_defence);
			int total = attack + best_defence;
			stringstream text;
			bool show_window = false;
			bool attacker_wins = false;
			int r = rand() % total;
			if( r < attack ) {
				// attacker wins!
				if( unit->getCivilization() == player ) {
					text << "You have shot down enemy ";
					text << defender->getTemplate()->getName();
					text << ".";
					show_window = true;
				}
				else if( defender->getCivilization() == player ) {
					text << city->getName() << ": Your ";
					text << defender->getTemplate()->getName();
					text << " have been shot down by ";
					text << unit->getCivilization()->getNameAdjective();
					text << " ";
					text << unit->getTemplate()->getName();
					text << "!";
					show_window = true;
				}
				attacker_wins = true;
			}
			else {
				// attacker loses
				if( unit->getCivilization() == player ) {
					text << "You were shot down by enemy ";
					text << defender->getTemplate()->getName();
					text << ".";
					show_window = true;
				}
				else if( defender->getCivilization() == player ) {
					text << city->getName() << ": Your ";
					text << defender->getTemplate()->getName();
					text << " have been victorious against an attack by ";
					text << unit->getCivilization()->getNameAdjective();
					text << " ";
					text << unit->getTemplate()->getName();
					text << ".";
					show_window = true;
				}
			}
			if( show_window ) {
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), 64, 16, infowindow_def_w_c, 180);
				window->doModal();
				delete window;
			}
			// delete the units afterwards, so messages about wiping out civilizations comes after the message about winning or losing the attack
			if( attacker_wins ) {
				delete defender;
				defender = NULL;
			}
			else {
				if( unit->getCivilization() == player && unit == active_unit ) {
					active_unit = NULL;
					updateUnitButtons();
				}
				delete unit;
				unit = NULL;
				return; // attack destroyed, so quit
			}
		}
	}

	ASSERT( unit != NULL );
	if( unit->getTemplate()->getNuclearType() == UnitTemplate::NUCLEARTYPE_NONE ) {
		VI_log("conventional attack\n");
		ASSERT( unit->getTemplate()->getBombard() > 0 );
		stringstream text;
		bool success = false;
		int size = city->getSize();
		if( size > 1 && unit->getTemplate()->getBombardPower() > 100 && rand() % 2 == 0 ) {
			// population damage
			int r = rand() % 100;
			VI_log("r = %d\n", r);
			if( r < unit->getTemplate()->getBombard() ) {
				success = true;
				size--;
				ASSERT( size >= 1 );
				int old_pop = city->getPopulation();
				city->setSize(size);
				VI_log("bombing reduces city of %s to size %d\n", city->getName(), size);
				int new_pop = city->getPopulation();
				if( city->getCivilization() == player || unit->getCivilization() == player ) {
					// inform player
					string havehas = is_missile ? "has" : "have";
					if( city->getCivilization() == player ) {
						text << unit->getCivilization()->getNameAdjective() << " " << unit->getTemplate()->getName() << " " << havehas << " killed " << (old_pop - new_pop) << " citizens of " << city->getName() << "!";
					}
					else {
						text << "Your " << unit->getTemplate()->getName() << " " << havehas << " killed " << (old_pop - new_pop) << " citizens of " << city->getName() << "!";
					}
				}
			}
		}
		else {
			// improvement damage
			const Improvement *destroy_improvement = NULL;
			int r = rand() % 100;
			VI_log("r = %d\n", r);
			if( r < unit->getTemplate()->getBombard() ) {
				int bombard_power = unit->getTemplate()->getBombardPower();
				vector<const Improvement *> candidates;
				for(size_t i=0;i<city->getNImprovements();i++) {
					const Improvement *improvement = city->getImprovement(i);
					if( improvement->isImmuneFromBombing() )
						continue;
					if( improvement->getCost() <= bombard_power ) {
						candidates.push_back(improvement);
					}
				}
				if( candidates.size() > 0 ) {
					int r = rand() % candidates.size();
					destroy_improvement = candidates.at(r);
					if( player != NULL && player->isExplored(city->getX(), city->getY()) ) {
						/*Effect *effect = new Effect(this->fireEntity, city->getX(), city->getY(), fire_effect_duration_c);
						this->effects.push_back(effect);*/
						this->addFireEffect(city->getPos());
					}
				}
				else {
					VI_log("no candidates\n");
				}
			}
			if( destroy_improvement != NULL ) {
				success = true;
				VI_log("bombard destroys %s\n", destroy_improvement->getName());
				city->removeImprovement(destroy_improvement);
			}
			if( city->getCivilization() == player || unit->getCivilization() == player ) {
				// inform player
				if( destroy_improvement != NULL ) {
					string havehas = is_missile ? "has" : "have";
					if( city->getCivilization() == player ) {
						text << unit->getCivilization()->getNameAdjective() << " " << unit->getTemplate()->getName() << " " << havehas << " destroyed " << city->getName() << "'s " << destroy_improvement->getName() << "!";
					}
					else {
						text << "Your " << unit->getTemplate()->getName() << " " << havehas << " destroyed the city's " << destroy_improvement->getName() << "!";
					}
				}
			}
		}
		if( city->getCivilization() == player || unit->getCivilization() == player ) {
			// inform player
			mainGamestate->playSound(SOUND_WOODBRK); // TODO - better sound for bombing?
			if( !success ) {
				if( city->getCivilization() == player ) {
					if( is_missile ) {
						text << unit->getCivilization()->getNameAdjective() << " " << unit->getTemplate()->getName() << " lands on " << city->getName() << ", but nothing is destroyed.";
					}
					else {
						text << unit->getCivilization()->getNameAdjective() << " " << unit->getTemplate()->getName() << " bomb " << city->getName() << ", but nothing is destroyed.";
					}
				}
				else {
					if( is_missile ) {
						text << "The missile was unsuccessful.";
					}
					else {
						text << "The air raid was unsuccessful.";
					}
				}
			}
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), 64, 16, infowindow_def_w_c, 180);
			window->doModal();
			delete window;
		}
	}
	else {
		VI_log("nuclear attack\n");
		ASSERT( is_missile );
		if( player != NULL && player->isExplored(city->getX(), city->getY()) ) {
			/*Effect *effect = new Effect(this->fireEntity, city->getX(), city->getY(), fire_effect_duration_c);
			this->effects.push_back(effect);*/
			this->addFireEffect(city->getPos());
		}
		int percent_chance = unit->getTemplate()->getNuclearType() == UnitTemplate::NUCLEARTYPE_FISSION ? 25 : 50;
		int pop_damage = unit->getTemplate()->getNuclearType() == UnitTemplate::NUCLEARTYPE_FISSION ? 1 : 3;
		vector<const Improvement *> destroyed;
		for(size_t i=0;i<city->getNImprovements();i++) {
			const Improvement *improvement = city->getImprovement(i);
			if( improvement->isImmuneFromBombing() )
				continue;
			int r = rand() % 100;
			if( r < percent_chance ) {
				destroyed.push_back(improvement);
			}
		}
		for(vector<const Improvement *>::iterator iter = destroyed.begin(); iter != destroyed.end(); ++iter) {
			const Improvement *improvement = *iter;
			VI_log("nuke destroys %s\n", improvement->getName());
			city->removeImprovement(improvement);
		}
		bool bombshelter_helped = false;
		if( city->hasImprovement("Bomb Shelter") ) {
			if( rand() % 2 == 0 ) {
				VI_log("bomb shelter helped against nuclear attack\n");
				bombshelter_helped = true;
				pop_damage--;
			}
		}
		int size = city->getSize();
		int old_pop = city->getPopulation();
		T_ASSERT( pop_damage >= 0 );
		if( pop_damage >= 0 ) {
			size -= pop_damage;
			size = max(size, 1);
			city->setSize(size);
		}
		VI_log("nuke reduces city of %s to size %d\n", city->getName(), size);
		int new_pop = city->getPopulation();
		//if( city->getCivilization() == player || unit->getCivilization() == player )
		{
			// inform player
			mainGamestate->playSound(SOUND_NUKE_EXPLOSION);
			if( unit->getCivilization() != player ) {
				this->centre(city->getPos());
			}
			stringstream text;

			if( city->getCivilization() == player ) {
				text << unit->getCivilization()->getName() << " have nuked the city of " << city->getName() << "!\n";
			}
			else if( unit->getCivilization() == player ) {
				text << "You have nuked the " << city->getCivilization()->getNameAdjective() << " city of " << city->getName() << "!\n";
			}
			else {
				text << unit->getCivilization()->getName() << " have nuked the " << city->getCivilization()->getNameAdjective() << " city of " << city->getName() << "!\n";
			}
			if( new_pop != old_pop ) {
				text << "\nDeaths: " << formatNumber(old_pop - new_pop) << "\n";
			}
			if( bombshelter_helped ) {
				text << "(Bomb shelter helped against attack.)\n";
			}
			if( destroyed.size() > 0 ) {
				text << "\nImprovements Destroyed:\n\n";
				for(vector<const Improvement *>::iterator iter = destroyed.begin(); iter != destroyed.end(); ++iter) {
					const Improvement *improvement = *iter;
					text << improvement->getName() << "\n";
				}
			}

			//int window_h = destroyed.size() > 0 ? 440 : 180;
			const int window_h = 440;
			string button = "Okay";
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), nukeTexture, &button, 1, 64, 16, infowindow_def_w_c, window_h, false);
			window->doModal();
			delete window;
		}
	}

	if( is_missile ) {
		if( unit->getCivilization() == player && unit == active_unit ) {
			active_unit = NULL;
			updateUnitButtons();
		}
		delete unit;
		unit = NULL;
	}
	else {
		unit->useMove(1);
	}
}

bool MainGamestate::askDeclareWar(const Civilization *civilization, bool enter_territory) {
	Relationship *relationship = this->findRelationship(civilization, player);
	T_ASSERT( relationship->getStatus() == Relationship::STATUS_PEACE );
	stringstream text;
	if( enter_territory ) {
		text << "Entering enemy territory is an act of war. ";
	}
	text << "Are you sure you wish to declare war on " << civilization->getName() << "?";
	if( InfoWindow::confirm(game_g->getGraphicsEnvironment(), this->getPanel(), game_g->getFont(), game_g->getPanelTexture(), text.str(), "Declare War", "Cancel") ) {
		relationship->setStatus(Relationship::STATUS_WAR);
		return true;
	}
	return false;
}

void MainGamestate::scrollView() {
	if( game_g->getGraphicsEnvironment()->isActive() ) {
		int time_ms = VI_getGameTimeLastFrameMS();
		int m_x = 0, m_y = 0;
		bool m_buttons[3] = {false, false, false};
		game_g->getGraphicsEnvironment()->getMouseInfo(&m_x, &m_y, m_buttons);

		float scroll_speed = 10.0f;
		float scroll_dist = scroll_speed * time_ms / 1000.0f;
		const int gap = 8;
		//float scroll_dist = 0.02f;
		bool moved = false;
		//VI_log("mouse pos: %d, %d\n", m_x, m_y);
		//VI_log("time %d, scroll dist: %f\n", time, scroll_dist);

		if( this->mouse_dragging ) {
			int dx = m_x - this->mouse_dragging_x;
			int dy = m_y - this->mouse_dragging_y;
			if( dx != 0 || dy != 0 ) {
				this->was_dragged = true;
				moved = true;
				if( view_3d ) {
					const float drag_scale_3d_c = 0.05f;
					VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
					vp->move(-dx*drag_scale_3d_c, 0.0f, -dy*drag_scale_3d_c);
				}
				else {
					const float drag_scale_2d_c = 0.05f;
					this->map_pos_x -= dx*drag_scale_2d_c;
					this->map_pos_y -= dy*drag_scale_2d_c;
				}
				this->mouse_dragging_x = m_x;
				this->mouse_dragging_y = m_y;
			}
		}
		else if( m_x <= gap ) {
			//this->map_pos_x = max(this->map_pos_x, 0);
			moved = true;
			if( view_3d ) {
				VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
				vp->move(-scroll_dist, 0.0f, 0.0f);
			}
			else
				this->map_pos_x -= scroll_dist;
		}
		else if( m_x >= game_g->getGraphicsEnvironment()->getWidth()-1-gap ) {
			//float map_limit_x = max(0, map->getWidth() - ((float)genv->getWidth())/(float)this->sq_width);
			//this->map_pos_x = min(this->map_pos_x, map_limit_x);
			moved = true;
			if( view_3d ) {
				VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
				vp->move(scroll_dist, 0.0f, 0.0f);
			}
			else
				this->map_pos_x += scroll_dist;
		}
		if( m_y <= gap ) {
			//this->map_pos_y = max(this->map_pos_y, 0);
			moved = true;
			if( view_3d ) {
				VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
				vp->move(0.0f, 0.0f, -scroll_dist);
			}
			else
				this->map_pos_y -= scroll_dist;
		}
		else if( m_y >= game_g->getGraphicsEnvironment()->getHeight()-1-gap ) {
			//float map_limit_y = max(0, map->getHeight() - ((float)genv->getHeight())/(float)this->sq_height);
			//this->map_pos_y = min(this->map_pos_y, map_limit_y);
			moved = true;
			if( view_3d ) {
				VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
				vp->move(0.0f, 0.0f, scroll_dist);
			}
			else
				this->map_pos_y += scroll_dist;
		}

		if( moved )
			checkMapView();
	}
}

void MainGamestate::update() {
	int time_ms = VI_getGameTimeLastFrameMS();
	if( view_3d ) {
		smokeEntity_3d->update(time_ms, false);
		fireEntity_3d->update(time_ms, false);
	}
	else {
		smokeEntity_2d->update(time_ms, false);
		fireEntity_2d->update(time_ms, false);
	}

	if( this->map_needs_update ) {
		refreshMapDisplay();
	}
	/*if( VI_keyany() ) {
	VI_log("!\n");
	}*/
	/*int m_x = 0, m_y = 0;
	bool m_buttons[3];
	game_g->getGraphicsEnvironment()->getMouseInfo(&m_x, &m_y, m_buttons);
	game_g->getGraphicsEnvironment()->getMouseClick(m_buttons); // needed to avoid bug where city window reopens as soon as closed, if city is at same location as CityWindow's Okay button!
	*/

	if( this->map != NULL ) {
		// map will be NULL when creating a new game
		this->scrollView();
	}

	if( this->isGUILocked() ) {
		return;
	}

	// check for moving units (TODO: speed up)
	for(size_t i=0;i<player->getNUnits();i++) {
		const Unit *unit = player->getUnit(i);
		if( unit->isMoving() ) {
			this->endturnButton->setVisible(false);
			return;
		}
	}

	// update Game buttons (TODO: avoid doing this every frame?)
	updateButtons();

	if( phase == PHASE_CPUTURN ) {
		// do Unit based AI here...
		bool all_done = true;
		int count = 0;
		for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end() && all_done; ++iter) {
			Civilization *civilization = *iter;
			if( civilization == player || civilization->doneAI() ) {
				continue;
			}
			civilization->calculateFogOfWar(false); // calculate the fog of war, if not yet done this turn
			// do missiles first
			//for(int i=civilization->getNUnits()-1;i>=0 && all_done;i--) { // go backwards, to allow for deletion
			for(size_t i=civilization->getNUnits(); i-- > 0 && all_done; ) { // go backwards, to allow for deletion
				Unit *unit = civilization->getUnit(i);
				if( !unit->getTemplate()->isMissile() )
					continue;
				if( unit->hasMovesLeft() ) {
					if( unit->doAI() ) { // unit may be deleted (e.g., loses a battle)!
						all_done = false;
					}
					unit = NULL; // as may be deleted
					if( this->isGUILocked() ) {
						// need to recheck in case set by AI
						return;
					}
				}
			}
			// do all remaining units
			//for(int i=civilization->getNUnits()-1;i>=0 && all_done;i--) { // go backwards, to allow for deletion
			for(size_t i=civilization->getNUnits(); i-- > 0 && all_done; ) { // go backwards, to allow for deletion
				Unit *unit = civilization->getUnit(i);
				if( unit->hasMovesLeft() ) {
					if( unit->doAI() ) { // unit may be deleted (e.g., loses a battle)!
						all_done = false;
					}
					unit = NULL; // as may be deleted
					if( this->isGUILocked() ) {
						// need to recheck in case set by AI
						return;
					}
					count++;
					if( count == 20 ) {
						all_done = false; // force an update to the screen drawing / UI occasionally, to avoid long pauses!
					}
				}
			}
			if( all_done ) {
				civilization->setDoneAI();
			}
		}
		if( all_done ) {
			this->endTurn();
			return;
		}
	}
	if( phase == PHASE_ENDTURN ) {
		// If the main GUI is locked, we must be waiting on user input, so we temporarily suspend updating (otherwise
		// we risk, e.g., multiple requesters opening).
		// We must be careful to check the flag for every call to an update function, because any of them could open
		// a new requester!
		bool any_updated = false;
		for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end() && !this->isGUILocked(); ++iter) {
			Civilization *civilization = *iter;
			for(size_t i=0;i<civilization->getNCities() && !this->isGUILocked();i++) {
				City *city = civilization->getCity(i);
				if( city->needsUpdate() ) {
					city->update();
					any_updated = true;
				}
			}
		}
		if( !this->isGUILocked() && !any_updated ) {
			//the GUI isn't locked, and nothing needs to be updated - we can pass control back to the player!
			this->beginTurn();
		}
		else {
			// still not the player's turn yet
			return;
		}
	}

	// non unit key-presses [most of these now done via [hidden-]button key shortcuts]:

	/*if( VI_keypressed(V_H) ) {
		this->home();
	}*/

	/*if( this->phase == PHASE_PLAYERTURN && m_buttons[2] ) {
		// right click disables input mode
		this->setInputMode(INPUTMODE_NORMAL);
	}

	if( game_g->getGraphicsEnvironment()->readMouseWheelUp() ) {
		//this->action(this->zoomInButton);
		this->zoom(true);
	}
	else if( game_g->getGraphicsEnvironment()->readMouseWheelDown() ) {
		//this->action(this->zoomOutButton);
		this->zoom(false);
	}*/

	if( this->inputMode == INPUTMODE_TRAVEL || this->inputMode == INPUTMODE_BOMBARD || this->inputMode == INPUTMODE_RECONNAISSANCE || this->inputMode == INPUTMODE_GOTO ) {
		int m_x = 0, m_y = 0;
		bool m_buttons[3];
		game_g->getGraphicsEnvironment()->getMouseInfo(&m_x, &m_y, m_buttons);
		Pos2D m_map_pos = this->getMapPos(m_x, m_y);
		if( map->isValid(m_map_pos.x, m_map_pos.y) ) {
			const MapSquare *square = map->getSquare(m_map_pos);
/*#ifdef _WIN32
			HCURSOR cursor = NULL;
			if( square->isTarget() )
				cursor = LoadCursor( NULL, IDC_CROSS );
			else
				cursor = LoadCursor( NULL, IDC_NO );
			SetCursor(cursor);
			SetClassLong(game_g->getGraphicsEnvironment()->getHWND(), GCL_HCURSOR, (LONG)cursor);
#endif*/
			if( square->isTarget() )
				game_g->setCursor(cursor_target);
			else
				game_g->setCursor(cursor_block);
		}
	}

	//bool request_move[8] = {false, false, false, false, false, false, false, false}; // start from numpad 1, then go clockwise
	/*if( this->active_unit != NULL ) {
		ASSERT( this->phase == PHASE_PLAYERTURN );
		//const Pos2D pos = this->active_unit->getPos();
		if( ( VI_keypressed(V_LEFT) || VI_keypressed(V_NUMPAD4) ) ) {
			request_move[1] = true;
		}
		else if( ( VI_keypressed(V_RIGHT) || VI_keypressed(V_NUMPAD6) ) ) {
			request_move[5] = true;
		}
		else if( ( VI_keypressed(V_UP) || VI_keypressed(V_NUMPAD8) ) ) {
			request_move[3] = true;
		}
		else if( ( VI_keypressed(V_DOWN) || VI_keypressed(V_NUMPAD2) ) ) {
			request_move[7] = true;
		}
		else if( VI_keypressed(V_NUMPAD7) ) {
			request_move[2] = true;
		}
		else if( VI_keypressed(V_NUMPAD9) ) {
			request_move[4] = true;
		}
		else if( VI_keypressed(V_NUMPAD3) ) {
			request_move[6] = true;
		}
		else if( VI_keypressed(V_NUMPAD1) ) {
			request_move[0] = true;
		}
		//else if( VI_keypressed(V_C) ) {
		//	centreUnit();
		//}
		// actual processing of moves done later
	}*/

	/*if( this->active_unit != NULL ) {
		if( movePlayerActiveUnit(request_move) ) {
			return;
		}
	}*/

	if( this->active_unit == NULL && this->phase == PHASE_PLAYERTURN )  {
		bool found_unit = false;
		T_ASSERT( !this->isGUILocked() );
		// find new active unit
		//VI_log("find new active unit\n");
		if( cache_active_unit != NULL ) {
			// search after cache first
			bool found_cache = false;
			for(size_t i=0;i<player->getNUnits() && !found_unit;i++) {
				Unit *unit = player->getUnit(i);
				if( unit == cache_active_unit ) {
					found_cache = true;
					// if non-automated, we want to move onto the next unit (so that Wait works), otherwise we stick with this one, so that the automated unit can do multiple moves
					/*if( unit->isAutomated() && unit->hasMovesLeft() && unit->getStatus() == Unit::STATUS_NORMAL )
						; // keep with this unit
					else*/
					if( !unit->isAutomated() )
						continue;
				}
				if( found_cache && unit->hasMovesLeft() && unit->getStatus() == Unit::STATUS_NORMAL ) {
					if( unit->isAutomated() ) {
						/*while( unit->hasMovesLeft() && unit->automate() ) {
						}*/
						/// ?
						centre( unit->getPos() );
						this->endturnButton->setVisible(false);
						unit->automate();
						cache_active_unit = unit;
						found_unit = true;
					}
					else {
						activateUnit(unit, true);
						found_unit = true;
					}
				}
			}
		}
		// either no cached unit, or need wraparound
		for(size_t i=0;i<player->getNUnits() && !found_unit;i++) {
			Unit *unit = player->getUnit(i);
			if( unit->hasMovesLeft() && unit->getStatus() == Unit::STATUS_NORMAL ) {
				if( unit->isAutomated() ) {
					/*while( unit->hasMovesLeft() && unit->automate() ) {
					}*/
					centre( unit->getPos() );
					unit->automate();
					this->endturnButton->setVisible(false);
					cache_active_unit = unit;
					found_unit = true;
				}
				else {
					activateUnit(unit, true);
					found_unit = true;
				}
			}
		}
		if( active_unit == NULL ) {
			// what does this do???
			/*if( InfoWindow::getActiveWindow() != NULL ) {
				delete InfoWindow::getActiveWindow();
			}*/
			if( infoWindow != NULL ) {
				delete infoWindow;
				infoWindow = NULL;
			}
		}
		// moved_units_this_turn is a flag to stop us immediately ending the turn, when the player has no units to move!
		//if( active_unit == NULL && moved_units_this_turn ) {
		if( !found_unit && moved_units_this_turn ) {
			// no more units left to move
			this->endPlayerTurn();
		}
		//else if( active_unit == NULL ) {
		/*else if( !found_unit ) {
			// waiting for user to end turn
			if( VI_keypressed( V_RETURN ) ) {
				// Return also ends turn, if all units are moved
				game_g->getGraphicsEnvironment()->flushInput(); // need to fix bug where requesters would disappear, due to repeated detection of Return key!
				this->endPlayerTurn();
			}
		}*/
	}
}

bool MainGamestate::movePlayerActiveUnit(const bool *request_move) {
	T_ASSERT( this->phase == PHASE_PLAYERTURN );
	ASSERT( this->active_unit != NULL );
	Pos2D pos = this->active_unit->getPos();
	bool move = false;
	if( request_move[1] ) {
		pos.x--;
		move = true;
	}
	else if( request_move[5] ) {
		pos.x++;
		move = true;
	}
	else if( request_move[3] ) {
		pos.y--;
		move = true;
	}
	else if( request_move[7] ) {
		pos.y++;
		move = true;
	}
	else if( request_move[2] ) {
		pos.x--;
		pos.y--;
		move = true;
	}
	else if( request_move[4] ) {
		pos.x++;
		pos.y--;
		move = true;
	}
	else if( request_move[6] ) {
		pos.x++;
		pos.y++;
		move = true;
	}
	else if( request_move[0] ) {
		pos.x--;
		pos.y++;
		move = true;
	}
	if( move ) {
		if( !this->map->isValid(pos.x, pos.y) ) {
			move = false;
		}
	}
	if( move ) {
		map->reduceToBase(&pos.x, &pos.y);
		bool checkEnemies = ( this->active_unit->getTemplate()->getAttack() == 0 );
		// check for enemies blocking, if we can't attack
		if( this->active_unit->canMoveTo( pos, checkEnemies, false ) ) {
			bool move_ok = true;
			const Civilization *enemy_civ = NULL;
			if( !checkEnemies ) {
				// check for attacking enemies (TODO: improve performance?
				/*Civilization *enemy_civ = NULL;
				const vector<Unit *> *units = this->getMap()->findUnitsAt(pos.x, pos.y);
				if( units->size() > 0 && units->at(0)->getCivilization() != this->active_unit->getCivilization() ) {
					const Relationship *relationship = this->findRelationship(units->at(0)->getCivilization(), this->active_unit->getCivilization());
					if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
						enemy_civ = units->at(0)->getCivilization();
					}
				}
				if( enemy_civ == NULL ) {
					const City *city = this->getMap()->findCity(pos.x, pos.y);
					if( city != NULL && city->getCivilization() != this->active_unit->getCivilization() ) {
						const Relationship *relationship = this->findRelationship(city->getCivilization(), this->active_unit->getCivilization());
						if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
							enemy_civ = city->getCivilization();
						}
					}
				}
				//delete units;
				*/
				enemy_civ = this->getMap()->findCivilizationAt(pos);
				if( enemy_civ == player )
					enemy_civ = NULL;
				else if( enemy_civ != NULL ) {
					const Relationship *relationship = this->findRelationship(enemy_civ, player);
					if( relationship->getStatus() != Relationship::STATUS_PEACE ) {
						enemy_civ = NULL;
					}
				}

				if( enemy_civ != NULL ) {
					// break alliance?
					if( !askDeclareWar(enemy_civ, false) ) {
						move_ok = false;
					}
				}
			}

			if( move_ok ) {
				// also check for entering enemy territory
				// n.b., possible to require declaring war on two civs (if attacking an enemy, that's in another civ's territory!)
				//this->calculateTerritory();
				const Civilization *target_territory = this->getMap()->getSquare(pos)->getTerritory();
				if( target_territory != NULL && target_territory != enemy_civ && target_territory != player && this->getMap()->getSquare(this->active_unit->getPos())->getTerritory() != target_territory ) {
					// entering enemy territory - not allowed if at peace
					const Relationship *relationship = this->findRelationship(target_territory, player);
					if( relationship->getStatus() == Relationship::STATUS_PEACE && !relationship->hasRightOfPassage() ) {
						// break alliance?
						if( !askDeclareWar(target_territory, true) ) {
							move_ok = false;
						}
					}
				}
			}

			if( move_ok ) {
				//this->active_unit->moveTo(pos);
				this->moveTo(active_unit, pos, false); // unit may be deleted (e.g., loses a battle), in which case, active_unit will be set to NULL
				bool smooth_moving = active_unit != NULL && active_unit->isMoving();
				if( active_unit != NULL && !active_unit->hasMovesLeft() ) {
					this->active_unit = NULL;
					updateUnitButtons();
				}
				else if( active_unit != NULL ) {
					centreUnit();
				}
				//if( mainGUILocked ) {
				if( this->isGUILocked() ) {
					return true;
				}
				if( smooth_moving ) {
					return true;
				}
			}
		}
		else {
			this->playSound(SOUND_BLIP);
		}
	}
	return false;
}

void MainGamestate::moveTo(Unit *unit, Pos2D newPos, bool travelling) {
	map->reduceToBase(&newPos.x, &newPos.y);
	ASSERT( unit->canMoveTo( newPos, true, true ) );
	T_ASSERT( unit->movesLeft() > 0 );

	/*int xdist = abs(newPos.x - unit->getX());
	int ydist = abs(newPos.y - unit->getY());
	ASSERT(xdist > 0 || ydist > 0);
	bool travelling = xdist > 1 || ydist > 1;*/

	// if travelling, and landing in an enemy territory, see if any enemy ships can defend
	if( travelling ) {
		// if travelling, this should always be by sea, as we don't allow travelling by air to attack (if this is ever changed, we need to rethink this code!)
		const Civilization *t_civ = map->getSquare(newPos)->getTerritory();
		if( t_civ != NULL && t_civ != unit->getCivilization() ) {
			const Relationship *relationship = this->findRelationship(unit->getCivilization(), t_civ);
			if( relationship->getStatus() == Relationship::STATUS_WAR ) {
				Unit *attacker_sea_unit = map->getSquare(unit->getPos())->getBestSeaUnit();
				ASSERT( attacker_sea_unit == NULL || attacker_sea_unit->getCivilization() == unit->getCivilization() );
				Unit *defender_sea_unit = NULL;
				int defender_sea_attack = 0;
				for(size_t i=0;i<t_civ->getNCities();i++) {
					const City *o_city = t_civ->getCity(i);
					Unit *sea_unit = map->getSquare(o_city->getPos())->getBestSeaUnit();
					if( sea_unit != NULL ) {
						// within range?
						bool within_range = false;
						if( o_city->getPos() == newPos )
							within_range = true;
						else {
							o_city->initTravel(false, sea_unit->getTemplate(), true);
							if( map->getSquare(newPos)->isTarget() )
								within_range = true;
							map->resetTargets();
						}
						if( within_range ) {
							int sea_attack = sea_unit->getTemplate()->getSeaAttack();
							if( defender_sea_unit == NULL || sea_attack > defender_sea_attack ) {
								defender_sea_unit = sea_unit;
								defender_sea_attack = sea_attack;
							}
						}
					}
				}
				ASSERT( defender_sea_unit == NULL || defender_sea_unit->getCivilization() == t_civ );
				if( defender_sea_unit != NULL ) {
					VI_log("unit %d (%s) travels to %d, %d - intercepted by %s %s\n", unit, unit->getTemplate()->getName(), newPos.x, newPos.y, t_civ->getName(), defender_sea_unit->getTemplate()->getName());
					bool attacker_wins = false;
					if( attacker_sea_unit == NULL ) {
						VI_log("invading force destroyed by defending %s\n", defender_sea_unit->getTemplate()->getName());
						attacker_wins = false;
					}
					else {
						VI_log("sea combat: attacking %s versus defending %s\n", attacker_sea_unit->getTemplate()->getName(), defender_sea_unit->getTemplate()->getName());
						int attacker_sea_attack = attacker_sea_unit->getTemplate()->getSeaAttack();
						VI_log("sea attack: %d\n", attacker_sea_attack);
						VI_log("sea defence: %d\n", defender_sea_attack);
						int total = attacker_sea_attack + defender_sea_attack;
						int r = rand() % total;
						if( r < attacker_sea_attack ) {
							// attacker wins!
							attacker_wins = true;
						}
						else {
							attacker_wins = false;
						}
					}
					if( t_civ == player ) {
						this->centre( newPos );
					}
					if( unit->getCivilization() == player || t_civ == player ) {
						this->playSound(SOUND_WOODBRK);
					}
					stringstream text;
					bool show_window = false;
					if( attacker_wins ) {
						if( unit->getCivilization() == player ) {
							text << "Your " << attacker_sea_unit->getTemplate()->getName() << " have been victorious against enemy " << defender_sea_unit->getTemplate()->getName() << "!";
							show_window = true;
						}
						else if( t_civ == player ) {
							text << "Your " << defender_sea_unit->getTemplate()->getName() << " have been destroyed by " << attacker_sea_unit->getCivilization()->getNameAdjective() << " " << attacker_sea_unit->getTemplate()->getName() << "!";
							show_window = true;
						}
						delete defender_sea_unit;
						defender_sea_unit = NULL;
					}
					else {
						// note, attacker_sea_unit may be false
						if( unit->getCivilization() == player ) {
							text << "Your invasion force has been destroyed by enemy " << defender_sea_unit->getTemplate()->getName() << "!";
							show_window = true;
						}
						else if( t_civ == player ) {
							text << "Your " << defender_sea_unit->getTemplate()->getName() << " have destroyed an invasion force from " << unit->getCivilization()->getName() << "!";
							show_window = true;
						}
						if( attacker_sea_unit != NULL ) {
							delete attacker_sea_unit;
							attacker_sea_unit = NULL;
						}
						// if the attacker loses, then the invading unit is also destroyed!
						if( unit->getCivilization() == player && unit == active_unit ) {
							active_unit = NULL;
							updateUnitButtons();
						}
						delete unit;
						unit = NULL;
					}
					if( show_window ) {
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), 64, 16, infowindow_def_w_c, 180);
						window->doModal();
						delete window;
					}
				}
			}
		}
	}

	// unit will be NULL if destroyed by enemy ship
	if( unit != NULL ) {
		const vector<Unit *> *units = map->findUnitsAt(newPos.x, newPos.y);
		Civilization *o_civ = units->size() == 0 ? NULL : units->at(0)->getCivilization();
		if( o_civ == NULL || o_civ == unit->getCivilization() ) {
			//VI_log("unit %d (%s) moves to %d, %d\n", unit, unit->getTemplate()->getName(), newPos.x, newPos.y);
			// move
			/*int ox = unit->getX();
			int oz = unit->getY();*/
			if( unit->getCivilization() == player && map->getSquare(newPos)->getRoad() != ROAD_RAILWAYS ) {
				mainGamestate->playSound(SOUND_FOOT);
			}
			unit->setPos(newPos);
			if( travelling )
				unit->useMoves();
			else {
				Rational movement_cost = map->getSquare(newPos.x, newPos.y)->getMovementCost(unit->getCivilization());
				unit->useMove(movement_cost);
			}
			//board->updateArmy(army);
			//if( army->getCivilization() == player g)
			/*{
			board->setArmyVisibles(ox, oz); // old pos
			board->setArmyVisibles(army->pos_x, army->pos_z); // new pos
			}*/
			//bool smooth_move = unit->getCivilization() == player && !unit->isAutomated();
			bool smooth_move = unit->getCivilization() == player;

			City *city = map->findCity(newPos.x, newPos.y);
			if( city != NULL ) {
				if( city->getCivilization() != unit->getCivilization() ) {
					// capture city
					smooth_move = false;
					/*char text[MAX_LONG_TEXT+1];
					if( army->getCivilization() == player ) {
					char *racename = Race::getNameAdjective(city->getCivilization()->race);
					sprintf(text, "We have conquered the %s city of %s!\n", racename, city->getName());
					}
					else {
					char *racename = Race::getNameAdjective(army->getCivilization()->race);
					sprintf(text, "The %s have captured the city of %s!\n", racename, city->getName());
					}*/

					//delete city;
					/*Listselect *ls = new Listselect(listselect_x, listselect_y, listselect_w, listselect_h, text, NULL, NULL, NULL, false);
					ls->doModal();*/

					bool lose_population = city->losePopulation();
					bool destroy = false;
					int size = city->getSize();
					if( lose_population && size == 1 ) {
						destroy = true;
					}
					else if( unit->getCivilization() == rebel_civ ) {
						destroy = true;
					}

					if( unit->getCivilization() == player ) {
						//string text = "You have captured the ";
						stringstream text;
						text << "You have ";
						text << ( destroy ? "destroyed" : "captured" );
						text << " the ";
						text << city->getCivilization()->getNameAdjective();
						text << " city of ";
						text << city->getName();
						text << ".";
						//text.append("Here is a lot of long very long long long text blah blah long text ending in a new line:\n");
						//text.append("Here is another lot of long very long long long text blah blah long text, but not ending in a new line.");
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
						window->doModal();
						delete window;
					}
					else if( city->getCivilization() == player ) {
						this->centre( unit->getPos() );
						stringstream text;
						text << unit->getCivilization()->getName();
						text << " have ";
						text << ( destroy ? "destroyed" : "captured" );
						text << " the city of ";
						text << city->getName();
						text << "!";
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
						window->doModal();
						delete window;
					}

					if( player != NULL && player->isExplored(city->getX(), city->getY()) ) {
						/*Effect *effect = new Effect(mainGamestate->fireEntity, city->getX(), city->getY(), fire_effect_duration_c);
						mainGamestate->effects.push_back(effect);*/
						this->addFireEffect(city->getPos());
					}
					if( destroy ) {
						// need to also destroy any air and sea units
						const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(city->getPos());
						// n.b. - count backwards as the units vector is modified when we delete units!
						//for(int i=units->size()-1;i>=0;i--) {
						for(size_t i=units->size(); i-- > 0; ) {
							const Unit *unit = units->at(i);
							if( unit->getTemplate()->isAir() || unit->getTemplate()->isSea() ) {
								delete unit;
							}
						}
						// destroy city
						delete city;
						city = NULL;
					}
					else {
						if( lose_population ) {
							// reduce population
							city->setSize(size-1);
						}
						city->capture(unit->getCivilization());
					}
				}
			}

			if( smooth_move ) {
				unit->setMoving();
			}

			//setNextArmyButton();
			/*if( map->uncover(army->pos_x, army->pos_z) ) {
			board->updateMap();
			}*/
			//city_active = NULL; // we clear to NULL, even if the new square has a city
			//V_PANEL_set_visible(changeBuildButton, city_active!=NULL);

	#if 0
			if( army->getCivilization() == player && !army->movesLeft() ) {
				Army *new_army_active = nextArmy();
				if( new_army_active != NULL ) {
					//board->updateArmy(army_active);
					setArmyActive(new_army_active);
				}
				else {
					endTurn();
				}
			}
	#endif

		}
		else {
			VI_log("unit %d (%s) attacks at %d, %d\n", unit, unit->getTemplate()->getName(), newPos.x, newPos.y);
			T_ASSERT( unit->getTemplate()->getAttack() > 0 );
			const Relationship *relationship = this->findRelationship(unit->getCivilization(), o_civ);
			ASSERT( relationship->madeContact() );
			ASSERT( relationship->getStatus() == Relationship::STATUS_WAR );

			City *city = map->findCity(newPos.x, newPos.y);
			ASSERT( city == NULL || city->getCivilization() != unit->getCivilization() );

			// attack
			/*Unit *defender = NULL;
			int best_defence = 0;
			for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter) {
				Unit *this_unit = *iter;
				int defence = this_unit->getTemplate()->getDefence();
				// find best defender - for equal defences, choose one with lower attack (so only the weaker unit is put at risk of being destroyed)
				if( defender == NULL || defence > best_defence || ( defence == best_defence && this_unit->getTemplate()->getAttack() < defender->getTemplate()->getAttack() ) ) {
					defender = this_unit;
					best_defence = defence;
				}
			}*/
			Unit *defender = map->getSquare(newPos)->getBestDefender();
			ASSERT( defender != NULL );
			int best_defence = defender->getTemplate()->getDefence();

			best_defence *= 100; // to allow for fractional comparisons
			int city_bonus = 0;
			if( city != NULL ) {
				city_bonus = city->getDefenceBonus(unit);
				VI_log("City defence bonus %d\n", city_bonus);
				best_defence = ( best_defence * ( 100 + city_bonus ) ) / 100;
			}

			if( defender->getCivilization() == player ) {
				this->centre( defender->getPos() );
			}
			int attack = unit->getTemplate()->getAttack() * 100;
			const float veteran_bonus_c = 1.25f;
			if( unit->isVeteran() ) {
				VI_log("attacker is veteran\n");
				//attack *= veteran_bonus_c;
				attack = (int)(attack * veteran_bonus_c);
			}
			if( defender->isVeteran() ) {
				VI_log("defender is veteran\n");
				//best_defence *= veteran_bonus_c;
				best_defence = (int)(best_defence * veteran_bonus_c);
			}
			const float medicine_bonus_c = 1.2f;
			//const Technology *medicineTech = this->findTechnology("Medicine");
			//if( unit->getCivilization()->hasTechnology(medicineTech) ) {
			if( unit->getCivilization()->hasTechnology("Medicine") ) {
				VI_log("attacker has medicine\n");
				//attack *= medicine_bonus_c;
				attack = (int)(attack * medicine_bonus_c);
			}
			//if( defender->getCivilization()->hasTechnology(medicineTech) ) {
			if( defender->getCivilization()->hasTechnology("Medicine") ) {
				VI_log("defender has medicine\n");
				//best_defence *= medicine_bonus_c;
				best_defence = (int)(best_defence * medicine_bonus_c);
			}
			const float satellites_bonus_c = 1.2f;
			//const Technology *satellitesTech = this->findTechnology("Satellites");
			//if( unit->getCivilization()->hasTechnology(satellitesTech) ) {
			if( unit->getCivilization()->hasTechnology("Satellites") ) {
				VI_log("attacker has satellites\n");
				//attack *= satellites_bonus_c;
				attack = (int)(attack * satellites_bonus_c);
			}
			//if( defender->getCivilization()->hasTechnology(satellitesTech) ) {
			if( defender->getCivilization()->hasTechnology("Satellites") ) {
				VI_log("defender has satellites\n");
				//best_defence *= satellites_bonus_c;
				best_defence = (int)(best_defence * satellites_bonus_c);
			}
			if( defender->getCivilization()->hasBonus(Civilization::BONUS_MILITARY) ) {
				VI_log("defender has military bonus\n");
				//best_defence *= 1.5f;
				best_defence = (int)(best_defence * 1.5f);
			}
			if( travelling ) {
				VI_log("defence bonus due to travelling by sea\n");
				if( city != NULL && city->hasImprovement("Coastal Defence") ) {
					VI_log("...and with coastal defence\n");
					//best_defence *= 1.5;
					best_defence = (int)(best_defence * 1.5f);
				}
				else {
					//best_defence *= 1.25;
					best_defence = (int)(best_defence * 1.25f);
				}
			}
			VI_log("attack: %d\n", attack);
			VI_log("defence: %d\n", best_defence);
			int total = attack + best_defence;
			stringstream text;
			bool show_window = false;
			bool attacker_wins = false;
			int r = rand() % total;
			const Civilization *cpu = NULL;
			bool defeated_rebels = false;
			bool rebels_in_territory = false;
			if( travelling && defender->getCivilization() == player ) {
				// if travelling, this should always be by sea, as we don't allow travelling by air to attack (if this is ever changed, we need to rethink this code!)
				text << unit->getCivilization()->getName() << " invade ";
				if( city != NULL ) {
					text << "the city of " << city->getName() << " ";
				}
				text << "by sea!";
				text << "\n\n";
			}
			if( r < attack ) {
				// attacker wins!
				if( unit->getCivilization() == player ) {
					cpu = defender->getCivilization();
					defeated_rebels = cpu == rebel_civ;
					if( defeated_rebels ) {
						rebels_in_territory = map->getSquare( defender->getPos() )->getTerritory() == player;
						text << "You have defeated the rebel ";
					}
					else
						text << "You have defeated the enemy ";
					text << defender->getTemplate()->getName();
					text << ".";
					show_window = true;
				}
				else if( defender->getCivilization() == player ) {
					text << "Your ";
					text << defender->getTemplate()->getName();
					text << " have been destroyed by ";
					text << unit->getCivilization()->getNameAdjective();
					text << " ";
					text << unit->getTemplate()->getName();
					text << "!";
					show_window = true;
					cpu = unit->getCivilization();
				}
				attacker_wins = true;
			}
			else {
				// attacker loses
				if( unit->getCivilization() == player ) {
					text << "You were defeated by the enemy ";
					text << defender->getTemplate()->getName();
					text << ".";
					show_window = true;
					cpu = defender->getCivilization();
				}
				else if( defender->getCivilization() == player ) {
					text << "Your ";
					text << defender->getTemplate()->getName();
					text << " have been victorious against an attack by ";
					text << unit->getCivilization()->getNameAdjective();
					text << " ";
					text << unit->getTemplate()->getName();
					text << ".";
					show_window = true;
					cpu = unit->getCivilization();
					defeated_rebels = cpu == rebel_civ;
					if( defeated_rebels ) {
						rebels_in_territory = map->getSquare( unit->getPos() )->getTerritory() == player;
					}
				}
			}
			if( unit->getCivilization() == player || defender->getCivilization() == player ) {
				const Unit *unit_winner = attacker_wins ? unit : defender;
				UnitTemplate::SoundEffect sound_effect = unit_winner->getTemplate()->getSoundEffect();
				if( sound_effect == UnitTemplate::SOUNDEFFECT_SWORDS ) {
					this->playSound(SOUND_SWORDS);
				}
				else if( sound_effect == UnitTemplate::SOUNDEFFECT_BREAK ) {
					this->playSound(SOUND_WOODBRK);
				}
				else if( sound_effect == UnitTemplate::SOUNDEFFECT_GUNSHOT ) {
					this->playSound(SOUND_GUNSHOT);
				}
				else {
					VI_log("unknown sound effect\n");
					T_ASSERT( false );
				}
			}
			if( show_window ) {
				text << "\n";
				if( player == unit->getCivilization() ? unit->isVeteran() : defender->isVeteran() ) {
					text << "\nYour veteran unit bonus: 25%";
				}
				//if( player->hasTechnology(medicineTech) ) {
				if( player->hasTechnology("Medicine") ) {
					text << "\nYour medicine bonus: 20%";
				}
				//if( player->hasTechnology(satellitesTech) ) {
				if( player->hasTechnology("Satellites") ) {
					text << "\nYour satellites bonus: 20%";
				}
				if( player == defender->getCivilization() && defender->getCivilization()->hasBonus(Civilization::BONUS_MILITARY) ) {
					text << "\nYour military defence bonus: 50%";
				}
				if( city_bonus > 0 ) {
					text << "\nCity Defence Bonus: ";
					text << city_bonus;
					text << "%";
				}
				if( travelling ) {
					if( player == defender->getCivilization() )
						text << "\nYour";
					else
						text << "\nEnemy's";
					text << " sea defence bonus: ";
					if( city != NULL && city->hasImprovement("Coastal Defence") )
						text << "50%";
					else
						text << "25%";
				}
				if( cpu == unit->getCivilization() ? unit->isVeteran() : defender->isVeteran() ) {
					text << "\nEnemy's veteran unit bonus: 25%";
				}
				//if( cpu->hasTechnology(medicineTech) ) {
				if( cpu->hasTechnology("Medicine") ) {
					text << "\nEnemy's medicine bonus: 20%";
				}
				//if( cpu->hasTechnology(satellitesTech) ) {
				if( cpu->hasTechnology("Satellites") ) {
					text << "\nEnemy's satellites bonus: 20%";
				}
				if( cpu == defender->getCivilization() && defender->getCivilization()->hasBonus(Civilization::BONUS_MILITARY) ) {
					text << "\nEnemy's military defence bonus: 50%";
				}
			}
			if( show_window ) {
				//InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), 64, 16, infowindow_def_w_c, 360);
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), 64, 16, infowindow_def_w_c, 240);
				window->doModal();
				delete window;
				if( defeated_rebels && rebels_in_territory ) {
					// have we defeated all rebels in player's territory?
					bool defeated_all = true;
					for(int y=0;y<map->getHeight() && defeated_all;y++) {
						for(int x=0;x<map->getWidth() && defeated_all;x++) {
							const MapSquare *square = map->getSquareBase(x, y);
							if( square->getTerritory() == player ) {
								const vector<Unit *> *units = square->getUnits();
								for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end() && defeated_all;++iter) {
									const Unit *this_unit = *iter;
									if( this_unit != (attacker_wins ? defender : unit) && this_unit->getCivilization() == rebel_civ )
										defeated_all = false;
								}
							}
						}
					}
					if( defeated_all ) {
						VI_Texture *executionTexture = game_g->getExecutionTexture( player->getAge() );
						string button = "Okay";
						InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), "We have wiped out the rebellion! The surviving ringleaders have been rounded up and executed, as a deterrent to other troublemakers!", game_g->getFont(), game_g->getPanelTexture(), executionTexture, &button, 1, 64, 16, 440, 440, false);
						window->doModal();
						delete window;
					}
				}
			}
			// delete the units/cities afterwards, so messages about wiping out civilizations comes after the message about winning or losing the attack
			if( attacker_wins ) {
				if( !unit->isVeteran() && defender->getTemplate()->getDefence() > 0 ) {
					VI_log("attacker becomes veteran\n");
					unit->setVeteran(true);
				}

				delete defender;
				defender = NULL;

				if( city != NULL && city->losePopulation() ) {
					/*Effect *effect = new Effect(mainGamestate->fireEntity, city->getX(), city->getY(), fire_effect_duration_c);
					mainGamestate->effects.push_back(effect);*/
					this->addFireEffect(city->getPos());
					int size = city->getSize();
					if( size == 1 ) {
						// destroy city
						if( show_window ) {
							text.str("");
							text << "The city of " << city->getName() << " has been destroyed!";
							InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), 64, 16, infowindow_def_w_c, 180);
							window->doModal();
							delete window;
						}
						// need to also destroy any air and sea units
						const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(city->getPos());
						// n.b. - count backwards as the units vector is modified when we delete units!
						//for(int i=units->size()-1;i>=0;i--) {
						for(size_t i=units->size(); i-- > 0; ) {
							const Unit *unit = units->at(i);
							if( unit->getTemplate()->isAir() || unit->getTemplate()->isSea() ) {
								delete unit;
							}
						}
						delete city;
						city = NULL;
					}
					else {
						// reduce population
						city->setSize(size-1);
					}
				}
			}
			else {
				if( unit->getCivilization() == player && unit == active_unit ) {
					active_unit = NULL;
					updateUnitButtons();
				}
				if( !defender->isVeteran() ) {
					VI_log("defender becomes veteran\n");
					defender->setVeteran(true);
				}
				delete unit;
				unit = NULL;
			}
			if( attacker_wins && units->size() == 0 ) {
				// no defenders left (note that "units" is automatically updated, as it is a pointer to the Map's list!)
				City *city = map->findCity(newPos.x, newPos.y);
				if( city == NULL ) {
					// okay to move
					unit->setPos(newPos);
					if( travelling )
						unit->useMoves();
					else {
						Rational movement_cost = map->getSquare(newPos.x, newPos.y)->getMovementCost(unit->getCivilization());
						if( movement_cost < 1 ) {
							// must cost at least 1 full move, due to the attack
							movement_cost = 1;
						}
						unit->useMove(movement_cost);
					}
				}
				else {
					if( travelling )
						unit->useMoves();
					else {
						unit->useMove(1);
					}
				}
			}
			else if( attacker_wins ) {
				if( travelling )
					unit->useMoves();
				else {
					unit->useMove(1);
				}
			}
		}
	}
}

bool MainGamestate::checkRelationships() {
	bool ok = true;
	for(size_t i=0;i<this->getNCivilizations() && ok;i++) {
		const Civilization *civ1 = this->getCivilization(i);
		if( civ1->isDead() ) {
			continue;
		}
		for(size_t j=i+1;j<this->getNCivilizations() && ok;j++) {
			const Civilization *civ2 = this->getCivilization(j);
			if( civ2->isDead() ) {
				continue;
			}
			const Relationship *relationship = this->findRelationship(civ1, civ2);
			if( relationship == NULL ) {
				VI_log("MainGamestate::checkRelationships not all relationships present\n");
				ok = false;
			}
		}
	}
	return ok;
}

/*void MainGamestate::removeCivilization(Civilization *civilization) {
for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
Civilization *this_civilization = *iter;
if( this_civilization == civilization ) {
civilizations.erase(iter);
return;
}
}
VI_log("Failed to find Civilization to remove\n");
ASSERT(false);
}*/

void MainGamestate::playSound(SoundID soundID) const {
	if( game_g->getSound() != NULL ) {
		game_g->getSound()->PlaySound(soundID, false, 0);
	}
}

bool MainGamestate::isGUILocked() const {
	//return mainGUILocked || genv->getModalPanel() != NULL;
	return game_g->getGraphicsEnvironment()->getModalPanel() != NULL;
}

GameData *MainGamestate::getGameData() {
	return game_g->getGameData();
}

const GameData *MainGamestate::getGameData() const {
	return game_g->getGameData();
}

const Race *GameData::findRace(const char *name) const {
	for(vector<const Race *>::const_iterator iter = races.begin();iter != races.end(); ++iter) {
		const Race *race = *iter;
		if( race->equals(name) ) {
			return race;
		}
	}
	VI_log("GameData::findRace(%s) failed\n", name);
	T_ASSERT( false );
	return NULL;
}

const BonusResource *GameData::findBonusResource(const char *name) const {
	for(vector<const BonusResource *>::const_iterator iter = bonus_resources.begin();iter != bonus_resources.end(); ++iter) {
		const BonusResource *bonus_resource = *iter;
		if( bonus_resource->equals(name) ) {
			return bonus_resource;
		}
	}
	// n.b., don't assert fail, as this is used by loading routines, so want to fail gracefully
	return NULL;
}

const Element *GameData::findElement(const char *name) const {
	for(vector<const Element *>::const_iterator iter = elements.begin();iter != elements.end(); ++iter) {
		const Element *element = *iter;
		if( element->equals(name) ) {
			return element;
		}
	}
	// n.b., don't assert fail, as this is used by loading routines, so want to fail gracefully
	return NULL;
}

const Technology *GameData::findTechnology(const char *name) const {
	for(vector<Technology *>::const_iterator iter = technologies.begin();iter != technologies.end(); ++iter) {
		const Technology *technology = *iter;
		if( technology->equals(name) ) {
			return technology;
		}
	}
	VI_log("GameData::findTechnology(%s) failed\n", name);
	T_ASSERT( false );
	return NULL;
}

Buildable *GameData::findBuildable(const char *name) {
	for(vector<Buildable *>::iterator iter = buildables.begin();iter != buildables.end(); ++iter) {
		Buildable *buildable = *iter;
		if( buildable->equals(name) ) {
			return buildable;
		}
	}
	VI_log("GameData::findBuildable(%s) failed\n", name);
	T_ASSERT( false );
	return NULL;
}

const Improvement *GameData::findImprovement(const char *name) const {
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *improvement = *iter;
		if( improvement->equals(name) ) {
			return improvement;
		}
	}
	VI_log("GameData::findImprovement(%s) failed\n", name);
	T_ASSERT( false );
	return NULL;
}

/*const Improvement *GameData::findImprovement(ImprovementType improvementType) const {
	for(vector<const Improvement *>::const_iterator iter = improvements.begin();iter != improvements.end(); ++iter) {
		const Improvement *improvement = *iter;
		if( improvement->getImprovementType() == improvementType ) {
			return improvement;
		}
	}
	VI_log("GameData::findImprovement(%d) failed\n", improvementType);
	T_ASSERT( false );
	return NULL;
}*/

const UnitTemplate *GameData::findUnitTemplate(const char *name) const {
	/*for(vector<const UnitTemplate *>::const_iterator iter = unit_templates.begin();iter != unit_templates.end(); ++iter) {
		const UnitTemplate *unit_template = *iter;*/
	for(size_t i=0;i<this->getNUnitTemplates();i++) {
		const UnitTemplate *unit_template = this->getUnitTemplate(i);
		if( unit_template->equals(name) ) {
			return unit_template;
		}
	}
	VI_log("GameData::findUnitTemplate(%s) failed\n", name);
	T_ASSERT( false );
	return NULL;
}

const Relationship *MainGamestate::findRelationship(const Civilization *civ1, const Civilization *civ2) const {
	T_ASSERT( civ1 != civ2 );
	// n.b., allowed for dead civilizations (e.g., for displaying history)
	for(vector<Relationship *>::const_iterator iter = relationships.begin(); iter != relationships.end(); ++iter) {
		Relationship *relationship = *iter;
		if( relationship->equals(civ1, civ2) ) {
			return relationship;
		}
	}
	VI_log("GameData::findRelationship(%s, %s) failed\n", civ1->getName(), civ2->getName());
	T_ASSERT( false );
	return NULL;
}

Relationship *MainGamestate::findRelationship(const Civilization *civ1, const Civilization *civ2) {
	T_ASSERT( civ1 != civ2 );
	T_ASSERT( !civ1->isDead() );
	T_ASSERT( !civ2->isDead() );
	for(vector<Relationship *>::const_iterator iter = relationships.begin(); iter != relationships.end(); ++iter) {
		Relationship *relationship = *iter;
		if( relationship->equals(civ1, civ2) ) {
			return relationship;
		}
	}
	VI_log("GameData::findRelationship(%s, %s) failed\n", civ1->getName(), civ2->getName());
	T_ASSERT( false );
	return NULL;
}

/*bool MainGamestate::initWorld() {
	//VI_flush(0);

	world = VI_createWorld();

	// setup world here

	game_g->getGraphicsEnvironment()->linkWorld(world);

	VI_check();
	VI_debug_mem_usage_all();
	return true;
}*/

bool MainGamestate::init() {
	/*VisionPrefs visionPrefs;
	visionPrefs.load();
	visionPrefs.want_std_shaders = false;
	//visionPrefs.shaders = false; // no need for shaders
	//genv = VI_createGraphicsEnvironment(width, height, multisample, fullscreen, hdr);
	genv = VI_createGraphicsEnvironment(&visionPrefs);
	if( genv == NULL ) {
		bool done = false;
		if( visionPrefs.fullscreen ) {
			MessageBoxA(NULL,"Failed to run in fullscreen mode","Error",MB_OK|MB_ICONEXCLAMATION);
			visionPrefs.fullscreen = false;
			genv = VI_createGraphicsEnvironment(&visionPrefs);
			if( genv != NULL )
				done = true;
		}
		if( !done ) {
			MessageBoxA(NULL,"Failed to open graphics window","Error",MB_OK|MB_ICONEXCLAMATION);
			return false;
		}
	}

	sound = VI_Sound::create( game_g->getGraphicsEnvironment()->getHWND() );
	if( sound == NULL ) {
		MessageBoxA(NULL,"Failed to initialise sound","Error",MB_OK|MB_ICONEXCLAMATION);
	}

	//ShowCursor(FALSE); // always hide cursor
	char buffer[4096] = "";
	sprintf_s(buffer, "Conquests v%d.%d | (c) 2009-2010 Mark Harman", versionMajor, versionMinor);
	SetWindowTextA(game_g->getGraphicsEnvironment()->getHWND(), buffer);

	//VI_Texture *cursor = VI_createTextureWithMask("textures/arrow.png", 255, 255, 255);
	//genv->setMouseTexture(cursor);

	//font = genv->createFont("Verdana",12,FW_BOLD);
	font = game_g->getGraphicsEnvironment()->createFont("Verdana",12,FW_NORMAL);*/

	// create game panels

	VI_Font *font = game_g->getFont();

	gamePanel = VI_createPanel();
	gamePanel->setPaintFunc(paintPanel, NULL);
	gamePanel->setMouseClickEventFunc(mouseClickEventFunc, NULL);
	gamePanel->setMouseReleaseEventFunc(mouseReleaseEventFunc, NULL);
	game_g->getGraphicsEnvironment()->setPanel(gamePanel);
	game_g->getGraphicsEnvironment()->setRenderFunc(renderFunc);

	endturnButton = VI_createButton("End turn", font);
	initButton(endturnButton);
	endturnButton->setAction(action);
	//endturnButton->setKeyShortcut(V_RETURN);
	endturnButton->setPopupText("Finish your turn", font);
	//gamePanel->addChildPanel(endturnButton, 16, 96);
	gamePanel->addChildPanel(endturnButton, 16, 80);

	const int xdiff = 16, ydiff = 30;
	int xpos = 0, ypos = 0;

	//xpos = 332;
	//ypos = 8;
	xpos = game_g->getGraphicsEnvironment()->getWidth() - 96;
	ypos = 8;

	newgameButton = VI_createButton("New Game", font);
	initButton(newgameButton);
	newgameButton->setAction(action);
	newgameButton->setKeyShortcut(V_N);
	newgameButton->setPopupText("Start a new game (N)", font);
	gamePanel->addChildPanel(newgameButton, xpos, ypos); ypos += ydiff;
	//gamePanel->addChildPanel(newgameButton, xpos, ypos);  xpos += newgameButton->getWidth() + xdiff;

	loadgameButton = VI_createButton("Load", font);
	initButton(loadgameButton);
	loadgameButton->setAction(action);
	loadgameButton->setKeyShortcut(V_L);
	loadgameButton->setPopupText("Load a previously saved game (L)", font);
	gamePanel->addChildPanel(loadgameButton, xpos, ypos); ypos += ydiff;
	//gamePanel->addChildPanel(loadgameButton, xpos, ypos);  xpos += loadgameButton->getWidth() + xdiff;

	savegameButton = VI_createButton("Save", font);
	initButton(savegameButton);
	savegameButton->setAction(action);
	savegameButton->setKeyShortcut(V_S);
	savegameButton->setPopupText("Save the current game (S)", font);
	gamePanel->addChildPanel(savegameButton, xpos, ypos); ypos += ydiff;
	//gamePanel->addChildPanel(savegameButton, xpos, ypos);  xpos += savegameButton->getWidth() + xdiff;

	/*savemapButton = VI_createButton("Save Map", font);
	initButton(savemapButton);
	savemapButton->setAction(action);
	savemapButton->setPopupText("Save the world map", font);
	gamePanel->addChildPanel(savemapButton, xpos + savegameButton->getWidth() + xdiff, ypos); ypos += ydiff;*/

	quitgameButton = VI_createButton("Quit", font);
	initButton(quitgameButton);
	quitgameButton->setAction(action);
	quitgameButton->setPopupText("Quit the game (Escape)", font);
	gamePanel->addChildPanel(quitgameButton, xpos, ypos); ypos += ydiff;
	//gamePanel->addChildPanel(quitgameButton, xpos, ypos);  xpos += quitgameButton->getWidth() + xdiff;

	//xpos = game_g->getGraphicsEnvironment()->getWidth() - 96;
	//ypos += ydiff;

	citiesAdvisorButton = VI_createButton("Cities", font);
	initButton(citiesAdvisorButton);
	citiesAdvisorButton->setAction(action);
	citiesAdvisorButton->setKeyShortcut(V_F1);
	citiesAdvisorButton->setPopupText("List your cities (F1)", font);
	gamePanel->addChildPanel(citiesAdvisorButton, xpos, ypos); ypos += ydiff;

	unitsAdvisorButton = VI_createButton("Units", font);
	initButton(unitsAdvisorButton);
	unitsAdvisorButton->setAction(action);
	unitsAdvisorButton->setKeyShortcut(V_F2);
	unitsAdvisorButton->setPopupText("List your units (F2)", font);
	gamePanel->addChildPanel(unitsAdvisorButton, xpos, ypos); ypos += ydiff;

	civsAdvisorButton = VI_createButton("Civilizations", font);
	initButton(civsAdvisorButton);
	civsAdvisorButton->setAction(action);
	civsAdvisorButton->setKeyShortcut(V_F3);
	civsAdvisorButton->setPopupText("View and contact other Civilizations you have met (F3)", font);
	gamePanel->addChildPanel(civsAdvisorButton, xpos, ypos); ypos += ydiff;

	techsAdvisorButton = VI_createButton("Technologies", font);
	initButton(techsAdvisorButton);
	techsAdvisorButton->setAction(action);
	techsAdvisorButton->setKeyShortcut(V_F4);
	techsAdvisorButton->setPopupText("List your technologies (F4)", font);
	gamePanel->addChildPanel(techsAdvisorButton, xpos, ypos); ypos += ydiff;

	scoresAdvisorButton = VI_createButton("Scores", font);
	initButton(scoresAdvisorButton);
	scoresAdvisorButton->setAction(action);
	scoresAdvisorButton->setKeyShortcut(V_F5);
	scoresAdvisorButton->setPopupText("Compare civilizations (F5)", font);
	gamePanel->addChildPanel(scoresAdvisorButton, xpos, ypos); ypos += ydiff;

	mapButton = VI_createButton("Map", font);
	initButton(mapButton);
	mapButton->setAction(action);
	mapButton->setKeyShortcut(V_M);
	mapButton->setPopupText("Show or hide map (M)", font);
	gamePanel->addChildPanel(mapButton, xpos, ypos);
	//ypos += ydiff;
	//gamePanel->addChildPanel(mapButton, xpos, ypos); xpos += waitButton->getWidth() + xdiff;

	savemapButton = VI_createButton("S", font);
	initButton(savemapButton);
	savemapButton->setAction(action);
	savemapButton->setPopupText("Save map", font);
	gamePanel->addChildPanel(savemapButton, xpos + mapButton->getWidth() + xdiff, ypos); ypos += ydiff;

	gridButton = VI_createButton("Grid", font);
	initButton(gridButton);
	gridButton->setAction(action);
	gridButton->setPopupText("Toggle Grid", font);
	gamePanel->addChildPanel(gridButton, xpos, ypos); ypos += ydiff;

	zoomOutButton = VI_createButton("Zoom Out", font);
	initButton(zoomOutButton);
	zoomOutButton->setAction(action);
	zoomOutButton->setKeyShortcut(V_Z);
	zoomOutButton->setPopupText("Zoom Out (Z)", font);
	gamePanel->addChildPanel(zoomOutButton, xpos, ypos); ypos += ydiff;

	zoomResetButton = VI_createButton("Reset Zoom", font);
	initButton(zoomResetButton);
	zoomResetButton->setAction(action);
	zoomResetButton->setPopupText("Reset Zoom To Default", font);
	gamePanel->addChildPanel(zoomResetButton, xpos, ypos); ypos += ydiff;

	zoomInButton = VI_createButton("Zoom In", font);
	initButton(zoomInButton);
	zoomInButton->setAction(action);
	zoomInButton->setKeyShortcut(V_X);
	zoomInButton->setPopupText("Zoom In (X)", font);
	gamePanel->addChildPanel(zoomInButton, xpos, ypos); ypos += ydiff;

	viewButton = VI_createButton("View 3D/2D", font);
	initButton(viewButton);
	viewButton->setAction(action);
	viewButton->setPopupText("Switch between 3D and 2D view", font);
	gamePanel->addChildPanel(viewButton, xpos, ypos); ypos += ydiff;

	helpButton = VI_createButton("?", font);
	initButton(helpButton);
	helpButton->setAction(action);
	helpButton->setPopupText("Help", font);
	gamePanel->addChildPanel(helpButton, xpos, ypos); ypos += ydiff;

	xpos = 16;
	ypos = game_g->getGraphicsEnvironment()->getHeight() - 40; // need to also allow room for scrolling down "gap"

	waitButton = VI_createButton("Wait", font);
	initButton(waitButton);
	waitButton->setAction(action);
	waitButton->setKeyShortcut(V_W);
	waitButton->setPopupText("Wait, and move another unit first (W)", font);
	gamePanel->addChildPanel(waitButton, xpos, ypos); xpos += waitButton->getWidth() + xdiff;

	skipTurnButton = VI_createButton("Skip", font);
	initButton(skipTurnButton);
	skipTurnButton->setAction(action);
	skipTurnButton->setKeyShortcut(V_SPACE);
	skipTurnButton->setPopupText("Skip this unit's turn (SPACE)", font);
	gamePanel->addChildPanel(skipTurnButton, xpos, ypos); xpos += skipTurnButton->getWidth() + xdiff;

	fortifyButton = VI_createButton("Fortify", font);
	initButton(fortifyButton);
	fortifyButton->setAction(action);
	fortifyButton->setKeyShortcut(V_F);
	fortifyButton->setPopupText("Causes the unit to wait until activated again (F)", font);
	gamePanel->addChildPanel(fortifyButton, xpos, ypos); xpos += fortifyButton->getWidth() + xdiff;

	buildCityButton = VI_createButton("Build City", font);
	initButton(buildCityButton);
	buildCityButton->setAction(action);
	buildCityButton->setKeyShortcut(V_B);
	buildCityButton->setPopupText("Build a city at this location (B)", font);
	gamePanel->addChildPanel(buildCityButton, xpos, ypos); xpos += buildCityButton->getWidth() + xdiff;

	buildRoadButton = VI_createButton("Build Road", font);
	initButton(buildRoadButton);
	buildRoadButton->setAction(action);
	buildRoadButton->setKeyShortcut(V_R);
	buildRoadButton->setPopupText("Roads allow faster movement along them (R)", font);
	gamePanel->addChildPanel(buildRoadButton, xpos, ypos); // don't update xpos

	buildRailwaysButton = VI_createButton("Build Railways", font);
	initButton(buildRailwaysButton);
	buildRailwaysButton->setAction(action);
	buildRailwaysButton->setKeyShortcut(V_R);
	buildRailwaysButton->setPopupText("Railways allow instantaneous movement along them (R)", font);
	gamePanel->addChildPanel(buildRailwaysButton, xpos, ypos);

	xpos += max(buildRoadButton->getWidth(), buildRailwaysButton->getWidth() ) + xdiff;

	automateButton = VI_createButton("Automate", font);
	initButton(automateButton);
	automateButton->setAction(action);
	automateButton->setKeyShortcut(V_A);
	automateButton->setPopupText("Automate this unit (A)", font);
	gamePanel->addChildPanel(automateButton, xpos, ypos); xpos += automateButton->getWidth() + xdiff;

	gotoButton = VI_createButton("Goto", font);
	initButton(gotoButton);
	gotoButton->setAction(action);
	gotoButton->setKeyShortcut(V_G);
	gotoButton->setPopupText("Automatically move this unit to another square (G)", font);
	gamePanel->addChildPanel(gotoButton, xpos, ypos); xpos += gotoButton->getWidth() + xdiff;

	travelButton = VI_createButton("Travel", font);
	initButton(travelButton);
	travelButton->setAction(action);
	travelButton->setKeyShortcut(V_T);
	travelButton->setPopupText("Travel by sea (T)", font);
	gamePanel->addChildPanel(travelButton, xpos, ypos); xpos += travelButton->getWidth() + xdiff;

	xpos = 16;
	ypos -= 32;

	airRaidButton = VI_createButton("Bomb", font);
	initButton(airRaidButton);
	airRaidButton->setAction(action);
	airRaidButton->setKeyShortcut(V_B);
	airRaidButton->setPopupText("Bomb enemy city (B)", font);
	gamePanel->addChildPanel(airRaidButton, xpos, ypos); xpos += airRaidButton->getWidth() + xdiff;

	reconnaissanceButton = VI_createButton("Reconnaissance", font);
	initButton(reconnaissanceButton);
	reconnaissanceButton->setAction(action);
	reconnaissanceButton->setKeyShortcut(V_R);
	reconnaissanceButton->setPopupText("Reveal fog of war for an area (R)", font);
	gamePanel->addChildPanel(reconnaissanceButton, xpos, ypos); xpos += airRaidButton->getWidth() + xdiff;

	move1Button = VI_createButton("SW", font);
	initButton(move1Button);
	move1Button->setAction(action);
	move1Button->setPopupText("Move unit south-west", font);
	gamePanel->addChildPanel(move1Button, 0, 0);

	move2Button = VI_createButton("S", font);
	initButton(move2Button);
	move2Button->setAction(action);
	move2Button->setPopupText("Move unit south", font);
	gamePanel->addChildPanel(move2Button, 0, 0);

	move3Button = VI_createButton("SE", font);
	initButton(move3Button);
	move3Button->setAction(action);
	move3Button->setPopupText("Move unit south-east", font);
	gamePanel->addChildPanel(move3Button, 0, 0);

	move4Button = VI_createButton("W", font);
	initButton(move4Button);
	move4Button->setAction(action);
	move4Button->setPopupText("Move unit west", font);
	gamePanel->addChildPanel(move4Button, 0, 0);

	move6Button = VI_createButton("E", font);
	initButton(move6Button);
	move6Button->setAction(action);
	move6Button->setPopupText("Move unit east", font);
	gamePanel->addChildPanel(move6Button, 0, 0);

	move7Button = VI_createButton("NW", font);
	initButton(move7Button);
	move7Button->setAction(action);
	move7Button->setPopupText("Move unit north-west", font);
	gamePanel->addChildPanel(move7Button, 0, 0);

	move8Button = VI_createButton("N", font);
	initButton(move8Button);
	move8Button->setAction(action);
	move8Button->setPopupText("Move unit north", font);
	gamePanel->addChildPanel(move8Button, 0, 0);

	move9Button = VI_createButton("NE", font);
	initButton(move9Button);
	move9Button->setAction(action);
	move9Button->setPopupText("Move unit north-east", font);
	gamePanel->addChildPanel(move9Button, 0, 0);

	homeHiddenButton = VI_createPanel();
	homeHiddenButton->setAction(action);
	homeHiddenButton->setKeyShortcut(V_H);
	gamePanel->addChildPanel(homeHiddenButton, 0, 0);

	centreHiddenButton = VI_createPanel();
	centreHiddenButton->setAction(action);
	centreHiddenButton->setKeyShortcut(V_C);
	gamePanel->addChildPanel(centreHiddenButton, 0, 0);

	move1HiddenButton = VI_createPanel();
	move1HiddenButton->setAction(action);
	move1HiddenButton->setKeyShortcut(V_NUMPAD1);
	gamePanel->addChildPanel(move1HiddenButton, 0, 0);

	move2HiddenButton = VI_createPanel();
	move2HiddenButton->setAction(action);
	move2HiddenButton->setKeyShortcut(V_NUMPAD2);
	gamePanel->addChildPanel(move2HiddenButton, 0, 0);

	move3HiddenButton = VI_createPanel();
	move3HiddenButton->setAction(action);
	move3HiddenButton->setKeyShortcut(V_NUMPAD3);
	gamePanel->addChildPanel(move3HiddenButton, 0, 0);

	move4HiddenButton = VI_createPanel();
	move4HiddenButton->setAction(action);
	move4HiddenButton->setKeyShortcut(V_NUMPAD4);
	gamePanel->addChildPanel(move4HiddenButton, 0, 0);

	move6HiddenButton = VI_createPanel();
	move6HiddenButton->setAction(action);
	move6HiddenButton->setKeyShortcut(V_NUMPAD6);
	gamePanel->addChildPanel(move6HiddenButton, 0, 0);

	move7HiddenButton = VI_createPanel();
	move7HiddenButton->setAction(action);
	move7HiddenButton->setKeyShortcut(V_NUMPAD7);
	gamePanel->addChildPanel(move7HiddenButton, 0, 0);

	move8HiddenButton = VI_createPanel();
	move8HiddenButton->setAction(action);
	move8HiddenButton->setKeyShortcut(V_NUMPAD8);
	gamePanel->addChildPanel(move8HiddenButton, 0, 0);

	move9HiddenButton = VI_createPanel();
	move9HiddenButton->setAction(action);
	move9HiddenButton->setKeyShortcut(V_NUMPAD9);
	gamePanel->addChildPanel(move9HiddenButton, 0, 0);

	moveLHiddenButton = VI_createPanel();
	moveLHiddenButton->setAction(action);
	moveLHiddenButton->setKeyShortcut(V_LEFT);
	gamePanel->addChildPanel(moveLHiddenButton, 0, 0);

	moveRHiddenButton = VI_createPanel();
	moveRHiddenButton->setAction(action);
	moveRHiddenButton->setKeyShortcut(V_RIGHT);
	gamePanel->addChildPanel(moveRHiddenButton, 0, 0);

	moveUHiddenButton = VI_createPanel();
	moveUHiddenButton->setAction(action);
	moveUHiddenButton->setKeyShortcut(V_UP);
	gamePanel->addChildPanel(moveUHiddenButton, 0, 0);

	moveDHiddenButton = VI_createPanel();
	moveDHiddenButton->setAction(action);
	moveDHiddenButton->setKeyShortcut(V_DOWN);
	gamePanel->addChildPanel(moveDHiddenButton, 0, 0);

	returnHiddenButton = VI_createPanel();
	returnHiddenButton->setAction(action);
	returnHiddenButton->setKeyShortcut(V_RETURN);
	gamePanel->addChildPanel(returnHiddenButton, 0, 0);

	// load textures
	try {
		//MapSquare::loadTextures();
		cityTexture = VI_createTextureWithMask(getFullPath("data/gfx/city.png").c_str(), 255, 0, 255);
		cityModernTexture = VI_createTextureWithMask(getFullPath("data/gfx/city_modern.png").c_str(), 255, 0, 255);
		cityPanelTexture = VI_createTexture(getFullPath("data/gfx/marble.jpg").c_str());
		nukeTexture = VI_createTexture(getFullPath("data/gfx/nuke.jpg").c_str());

		// particle systems

		unsigned char smoke_min[] = {0, 0, 0};
		//unsigned char smoke_max[] = {255, 255, 255};
		//unsigned char smoke_max[] = {181, 181, 181};
		unsigned char smoke_max[] = {186, 186, 186};
		VI_Image *smokeImage = VI_Image::createRadial(64, 64, smoke_max, smoke_min, true, false);
		//smokeImage->save("test.png", "png");
		smokeTexture = VI_createTexture(smokeImage);
		const unsigned char grey = 51;
		//const unsigned char grey = 255;
		const float scale_3d = 1.2f;
		const float scale_2d = 48.0f;
		// &ps_ent,&ps,30,ps_tx,color,color,255,0,0.2*0.5*scale,1.0*0.5*scale,10,3.0,-0.5,0.1);
		smokeParticlesystem_2d = VI_GeneralParticlesystem::create(30);
		smokeParticlesystem_2d->setColoriStart(grey, grey, grey);
		smokeParticlesystem_2d->setColoriEnd(grey, grey, grey);
		smokeParticlesystem_2d->setAlphaiEnd(255);
		smokeParticlesystem_2d->setAlphaiEnd(127);
		smokeParticlesystem_2d->setSizeStart(0.3f*scale_2d);
		smokeParticlesystem_2d->setSizeEnd(scale_2d);
		smokeParticlesystem_2d->setBirthRate(10);
		smokeParticlesystem_2d->setLife(3.0f);
		smokeParticlesystem_2d->setMass(1.0f);
		smokeParticlesystem_2d->setInitialVelocity(0.05f);
		smokeParticlesystem_2d->setTexture(smokeTexture);
		smokeEntity_2d = VI_createSceneGraphNode(smokeParticlesystem_2d);

		smokeParticlesystem_3d = VI_GeneralParticlesystem::create(30);
		smokeParticlesystem_3d->setColoriStart(grey, grey, grey);
		smokeParticlesystem_3d->setColoriEnd(grey, grey, grey);
		smokeParticlesystem_3d->setAlphaiEnd(255);
		smokeParticlesystem_3d->setAlphaiEnd(127);
		smokeParticlesystem_3d->setSizeStart(0.3f*scale_3d);
		smokeParticlesystem_3d->setSizeEnd(scale_3d);
		smokeParticlesystem_3d->setBirthRate(5);
		smokeParticlesystem_3d->setLife(2.5f);
		smokeParticlesystem_3d->setMass(-0.03125f*1.5f);
		smokeParticlesystem_3d->setInitialVelocity(-0.0015625f*2.0f);
		smokeParticlesystem_3d->setTexture(smokeTexture);
		smokeEntity_3d = VI_createSceneGraphNode(smokeParticlesystem_3d);

		fireParticlesystem_2d = VI_GeneralParticlesystem::create(50);
		fireParticlesystem_2d->setColoriStart(255, 0, 0);
		fireParticlesystem_2d->setColoriEnd(255, 127, 0);
		fireParticlesystem_2d->setAlphaiStart(255);
		fireParticlesystem_2d->setAlphaiEnd(127);
		fireParticlesystem_2d->setSizeStart(0.2f*scale_2d);
		fireParticlesystem_2d->setSizeEnd(2.0f*scale_2d);
		fireParticlesystem_2d->setBirthRate(10);
		fireParticlesystem_2d->setLife(4.0f);
		fireParticlesystem_2d->setMass(1.0f);
		fireParticlesystem_2d->setInitialVelocity(0.05f);
		fireParticlesystem_2d->setTexture(smokeTexture);
		fireEntity_2d = VI_createSceneGraphNode(fireParticlesystem_2d);

		fireParticlesystem_3d = VI_GeneralParticlesystem::create(50);
		fireParticlesystem_3d->setColoriStart(255, 0, 0);
		fireParticlesystem_3d->setColoriEnd(255, 127, 0);
		fireParticlesystem_3d->setAlphaiStart(255);
		fireParticlesystem_3d->setAlphaiEnd(127);
		fireParticlesystem_3d->setSizeStart(0.2f*scale_3d);
		fireParticlesystem_3d->setSizeEnd(2.0f*scale_3d);
		fireParticlesystem_3d->setBirthRate(3);
		fireParticlesystem_3d->setLife(4.0f);
		fireParticlesystem_3d->setMass(-0.015625f*1.5f);
		fireParticlesystem_3d->setInitialVelocity(-0.0015625f*2.0f);
		fireParticlesystem_3d->setTexture(smokeTexture);
		fireEntity_3d = VI_createSceneGraphNode(fireParticlesystem_3d);

		// load sounds
		if( game_g->getSound() != NULL ) {
			game_g->getSound()->LoadSound(SOUND_FOOT, getFullPath("data/sound/foot3.wav").c_str());
			game_g->getSound()->LoadSound(SOUND_WORKER, getFullPath("data/sound/worker.wav").c_str());
			game_g->getSound()->LoadSound(SOUND_SWORDS, getFullPath("data/sound/swords_clashing.wav").c_str());
			game_g->getSound()->LoadSound(SOUND_WOODBRK, getFullPath("data/sound/woodbrk.wav").c_str());
			game_g->getSound()->LoadSound(SOUND_GUNSHOT, getFullPath("data/sound/gunshot.wav").c_str());
			game_g->getSound()->LoadSound(SOUND_NUKE_EXPLOSION, getFullPath("data/sound/bomb.wav").c_str());
			game_g->getSound()->LoadSound(SOUND_BLIP, getFullPath("data/sound/mouse_click.wav").c_str());
			game_g->getSound()->LoadSound(SOUND_COMPLETE, getFullPath("data/sound/gmae.wav").c_str());
		}

		//VI_Texture *map_texture = game->map->getSquare(0, 0)->getTexture();
		VI_Texture *map_texture = MapSquare::getTexture(TYPE_GRASSLAND);
		def_sq_width = map_texture->getWidth();
		def_sq_height = map_texture->getHeight();
	}
	catch(VisionException *ve) {
		VI_log("Failed - something went wrong loading main game state data!\n");
#ifdef _WIN32
		MessageBoxA(NULL,"Failed to load Main Game State data","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
		delete ve;
		return false;
	}

	if( map != NULL ) {
		VI_log("map is %d x %d\n", map->getWidth(), map->getHeight());
	}

	VI_World *world = VI_createWorld();
	game_g->getGraphicsEnvironment()->linkWorld(world);
	VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
	vp->rotateToEuler(-60.0f, 0.0f, 0.0f);
	if( view_3d ) {
		if( this->map != NULL )
			generateTerrain();
		float preferred_width = def_view3d_scale * game_g->getGraphicsEnvironment()->getWidth() / this->def_sq_width;
		reset3DZoom(preferred_width);
	}
	try	{
		unsigned char filter_max[3] = {32, 32, 64};
		unsigned char filter_min[3] = {0, 0, 0};
		VI_Image *image = VI_Image::createNoise(256, 256, 4.0f, 4.0f, filter_max, filter_min, V_NOISEMODE_SMOKE, 4, false);
		VI_Texture *texture = VI_createTexture(image);
		VI_Sky *sky = VI_createSky1(texture);
		sky->setMode(V_SKYMODE_FULL);
		world->setSky(sky);
	}
	catch(VisionException *ve) {
		VI_log("Failed - something went wrong loading main game state data (sky)!\n");
#ifdef _WIN32
		MessageBoxA(NULL,"Failed to load Main Game State data (sky)","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
		delete ve;
		return false;
	}

	//VI_set_persistence_all(-1);
	/*if( !initWorld() ) {
		return false;
	}*/

	return true;
}

void MainGamestate::generateTerrain() {
	VI_log("MainGamestate::generateTerrain()\n");
	if( game_g->getGraphicsEnvironment()->getWorld() == NULL ) {
		return;
	}
	if( view_3d ) {
		T_ASSERT( game_g->getGraphicsEnvironment()->getWorld()->getTerrain() == NULL );
		VI_lock();
		int extended_width = map->getTopology() == TOPOLOGY_CYLINDER ? 2*map->getWidth()+1 : map->getWidth()+1;
		VI_Terrain *terrain = VI_createTerrain(game_g->getGraphicsEnvironment(), extended_width, map->getHeight()+1, xscale, zscale, 1.0f);
		terrain->setLOD(false);
		terrain->setLighting(false);
		//terrain->setAmbientPixelShader(game_g->getSplat3DPixelShaderAmbient());
		terrain->setAmbientShader(game_g->getSplat3DShaderAmbient());
		terrain->setSplatSortTextures(false); // needed so the water animation works, so we can assume that Ocean texture is as set below (index 1)
		terrain->setTexture(0, MapSquare::getTexture(TYPE_GRASSLAND));
		terrain->setTexture(1, MapSquare::getTexture(TYPE_OCEAN));
		terrain->setTexture(2, MapSquare::getTexture(TYPE_DESERT));
		terrain->setTexture(3, MapSquare::getTexture(TYPE_ARTIC));
		for(int y=0;y<map->getHeight();y++) {
			for(int x=0;x<map->getWidth();x++) {
				const MapSquare *square = map->getSquare(x, y);
				/*unsigned char type = (unsigned char)square->getType();
				terrain->setTextureMap(x, y, type);*/
				unsigned char v = 0;
				if( square->getType() == TYPE_OCEAN ) {
					v = 1;
				}
				else if( square->getType() == TYPE_DESERT ) {
					v = 2;
				}
				else if( square->getType() == TYPE_ARTIC ) {
					v = 3;
				}
				terrain->setTextureMap(x, y, v);
				if( map->getTopology() == TOPOLOGY_CYLINDER ) {
					terrain->setTextureMap(x + map->getWidth(), y, v);
				}
				if( this->player != NULL && !this->player->isExplored(x, y) ) {
					terrain->setVisible(x, y, false);
					if( map->getTopology() == TOPOLOGY_CYLINDER ) {
						terrain->setVisible(x + map->getWidth(), y, false);
					}
				}
			}
		}

		game_g->getGraphicsEnvironment()->getWorld()->setTerrain(terrain);
		this->updateTerrainForFOW();

		VI_unlock();

		/*VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
		vp->setPosition(0.5f*terrain->getWidth(), def_vp_height_c, 0.75f*terrain->getDepth());
		vp->rotateToEuler(-60.0f, 0.0f, 0.0f);*/
	}

}

void MainGamestate::updateTerrainForFOW() {
	VI_Terrain *terrain = NULL;
	if( this->player != NULL && game_g->getGraphicsEnvironment()->getWorld() != NULL )
		terrain = game_g->getGraphicsEnvironment()->getWorld()->getTerrain();
	if( terrain != NULL ) {
		VI_lock();
		terrain->setColorMode(V_COLORMODE_GLOBAL);
		terrain->initColor(0, 255, 255, 255);
		for(int y=0;y<=this->map->getHeight();y++) {
			for(int x=0;x<=this->map->getWidth();x++) {
				/*bool fog_visible = this->isFogOfWarVisible(x, y);
				if( !fog_visible ) {
					terrain->setColorAt(0, x, y, 127, 127, 127);
				}*/
				int visible = 0;
				int count = 0;
				for(int cy=y-1;cy<=y;cy++) {
					for(int cx=x-1;cx<=x;cx++) {
						if( this->map->isValid(cx, cy) ) {
							if( this->player->isFogOfWarVisible(cx, cy) ) {
								visible++;
							}
							count++;
						}
					}
				}
				ASSERT( count > 0 );
				float alpha = ((float)visible) / (float)count;
				int v = (int)((1.0f-alpha) * fog_alpha_c + alpha * 255);
				terrain->setColorAt(0, x, y, v, v, v);
				if( map->getTopology() == TOPOLOGY_CYLINDER ) {
					terrain->setColorAt(0, x + map->getWidth(), y, v, v, v);
				}
			}
		}
		VI_unlock();
	}
}

void MainGamestate::reset3DZoom(float preferred_width) {
	ASSERT( view_3d );
	// TODO: fix calculation, not quite correct!
	//vp->setPosition(0.5f*terrain->getWidth(), def_vp_height_c, 0.75f*terrain->getDepth());
	VI_SceneGraphNode *vp = game_g->getGraphicsEnvironment()->getWorld()->getViewpoint();
	Vector3D vp_pos = vp->getPosition();
	// move to reference height
	vp_pos.y = 10.0f;
	vp->setPosition(vp_pos);

	float mx0 = 0.0f, mx1 = 0.0f, my = 0.0f;
	getMapPosf(&mx0, &my, 0, 0);
	getMapPosf(&mx1, &my, game_g->getGraphicsEnvironment()->getWidth()-1, 0);
	float width = mx1 - mx0;
	vp_pos.y *= preferred_width / width;
	vp->setPosition(vp_pos);

}

bool MainGamestate::start() {
	bool ok = true;

/*#ifdef _WIN32
	HCURSOR cursor = LoadCursor( NULL, IDC_WAIT );
	SetCursor(cursor);
	SetClassLong(game_g->getGraphicsEnvironment()->getHWND(), GCL_HCURSOR, (LONG)cursor);
#endif*/
	game_g->setCursor(cursor_wait);

	/*if( !initGameData(true) ) {
		ok = false;
	}
	else*/
	if( !init() ) {
		ok = false;
	}
	/*else if( !initGameData(true) ) {
		ok = false;
	}*/
	else {
		VI_check();
		VI_debug_mem_usage_all();

		game_g->getGraphicsEnvironment()->setVisible(true);
		game_g->getGraphicsEnvironment()->setFade(fade_duration_c, 0, 255);
		home();
		//beginTurn();
		// don't beginTurn() here, as we may be starting from a loaded file!
	}

	if( !ok ) {
		clear();
	}

/*#ifdef _WIN32
	cursor = LoadCursor( NULL, IDC_ARROW );
	SetCursor(cursor);
	SetClassLong(game_g->getGraphicsEnvironment()->getHWND(), GCL_HCURSOR, (LONG)cursor);
#endif*/
	game_g->setCursor(cursor_arrow);

	if( ok )
		gamestate_started = true;
	return ok;
}

void MainGamestate::stop() {
	game_g->setCursor(cursor_wait);
	clear();
	game_g->setCursor(cursor_arrow);
	gamestate_started = false;
}

void MainGamestate::clear() {
	clearGameData();

	VI_flush(0); // delete all the gamestate objects, but leave the game level objects (which should be set at persistence level -1)
	VI_GraphicsEnvironment *genv = game_g->getGraphicsEnvironment();
	//VI_log("%d\n", genv);
	game_g->getGraphicsEnvironment()->setPanel(NULL); // as the main panel is now destroyed
	game_g->getGraphicsEnvironment()->linkWorld(NULL); // as the world is now destroyed
	game_g->getGraphicsEnvironment()->setRenderFunc(NULL);
}

void MainGamestate::clearGameData() {
	for(vector<Effect *>::iterator iter = effects.begin(); iter != effects.end(); ++iter) {
		Effect *effect = *iter;
		delete effect;
	}
	effects.clear();

	/*if( InfoWindow::getActiveWindow() != NULL ) {
		delete InfoWindow::getActiveWindow();
	}*/
	if( infoWindow != NULL ) {
		delete infoWindow;
		infoWindow = NULL;
	}

	for(vector<Relationship *>::iterator iter = relationships.begin(); iter != relationships.end(); ++iter) {
		Relationship *relationship = *iter;
		delete relationship;
	}
	relationships.clear();
	// copy to a temp list, to avoid issues with code called from Civilization destructor still thinking that civilizations are part of the game, even when already deleted!
	vector<Civilization *> temp_civilizations(civilizations);
	civilizations.clear();
	player = NULL;
	//for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
	for(vector<Civilization *>::iterator iter = temp_civilizations.begin(); iter != temp_civilizations.end(); ++iter) {
		Civilization *civilization = *iter;
		delete civilization;
	}
	help_buildcity = false;
	help_buildroad = false;
	help_bomb = false;

	if( map != NULL ) {
		delete map;
		map = NULL;
	}

	difficulty = DIFFICULTY_EASY;
	aiaggression = AIAGGRESSION_NORMAL;
	game_n_turns = 0;
	rebel_civ = NULL;

	scale_width = 1.0f;
	scale_height = 1.0f;

	VI_World *world = game_g->getGraphicsEnvironment()->getWorld();
	if( world != NULL && world->getTerrain() != NULL ) {
		VI_log("about to delete terrain\n");
		delete world->getTerrain();
		VI_log("done\n");
		world->setTerrain(NULL);
	}
}

bool MainGamestate::initGameData(bool std_game, bool new_map, int map_width, int map_height, Topology topology, const char *map_filename, const Race *player_race, int n_opponent_civs) {
	game_g->setCursor(cursor_wait);
	if( RANDOMSEED ) {
		time_t seconds = time(NULL) % 65536;
		this->seed = (int)(clock() + seconds);
		//this->seed = 82915; // "extra" terrain bug with texture splatting on small flat world.
	}
	else {
		this->seed = 114273;
	}
	VI_log("set seed %d\n", seed);
	srand(seed);
	year = 0;
	help_buildcity = false;
	help_buildroad = false;
	help_bomb = false;
	cache_active_unit = NULL;
	active_unit = NULL;
	//updateUnitButtons();
	setInputMode(INPUTMODE_NORMAL);
	moved_units_this_turn = false;

	T_ASSERT( map == NULL );
	if( std_game ) {
		if( new_map ) {
			map = new Map(this, map_width, map_height);
			map->setTopology(topology);
			map->createRandom();
		}
		else {
			VI_XMLTreeNode *tree = VI_parseXML(map_filename);
			bool load_ok = true;
			if( tree == NULL ) {
				VI_log("Failed to read XML file\n");
				load_ok = false;
			}
			else {
				//printTree(tree);
				VI_XMLTreeNode *map_node = tree->findNode("map", true);
				if( map_node == NULL ) {
					VI_log("Failed to find map element\n");
					load_ok = false;
				}
				else {
					//printTree(map_node);
					VI_XMLTreeNode *data_node = map_node->findNode("data", true);
					if( data_node == NULL ) {
						VI_log("Failed to find data element\n");
						load_ok = false;
					}
					else if( data_node->getNChildren() != 1 ) {
						VI_log("Didn't expect data element to have %d children\n", data_node->getNChildren());
						load_ok = false;
					}
					else {
						const VI_XMLTreeNode *data_child = data_node->getChild(0);
						if( !data_child->hasData() ) {
							VI_log("can't find any data\n");
							load_ok = false;
						}
						else {
							VI_log("map width: %s\n", map_node->getAttribute("width").c_str());
							VI_log("map height: %s\n", map_node->getAttribute("height").c_str());
							VI_log("data encoding: %s\n", data_node->getAttribute("encoding").c_str());
							VI_log("data: %s\n", data_child->getData().c_str());
							map_width = atoi( map_node->getAttribute("width").c_str() );
							map_height = atoi( map_node->getAttribute("height").c_str() );
							if( map_width == 0 ) {
								VI_log("invalid map width\n");
								load_ok = false;
							}
							else if( map_height == 0 ) {
								VI_log("invalid map height\n");
								load_ok = false;
							}
							else if( data_node->getAttribute("encoding") != "csv" ) {
								VI_log("only csv encoding is supported\n");
								load_ok = false;
							}
							else {
								topology = TOPOLOGY_CYLINDER;
								map = new Map(this, map_width, map_height);
								map->setTopology(topology);
								std::istringstream iss(data_child->getData());
								string token;
								int index = 0;
								while( load_ok && std::getline(iss, token, ',') ) {
									//std::cout << token << std::endl;
									int terrain = atoi(token.c_str());
									if( terrain > 0 ) { // if 0, default to ocean
										terrain--;
									}
									if( terrain < 0 || terrain >= (int)N_TYPES ) {
										VI_log("unrecognised terrain\n");
										load_ok = false;
									}
									else if( index >= map_width*map_height ) {
										VI_log("too much data\n");
										load_ok = false;
									}
									else {
										map->setSquareIndex(index, (Type)terrain);
										index++;
									}
								}
								if( index < map_width*map_height ) {
									VI_log("not enough data: %d\n", index);
									load_ok = false;
								}
								if( !load_ok ) {
									delete map;
									map = NULL;
								}
								else {
									map->createRandomBonuses();
									map->createAlphamap();
								}
							}
						}
					}
				}
			}
			delete tree;
			if( !load_ok ) {
				game_g->setCursor(cursor_arrow);
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "World map file is corrupt or unknown format", game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
				return false;
			}
		}
		/*map = new Map(this);
		const char mapfile[] = "conquests/data/world.map";
		//const char mapfile[] = "conquests/data/world_32x32.map";
		if( !map->readFile(mapfile) ) {
			VI_log("Failed to read map file: %s\n", mapfile);
			MessageBoxA(NULL,"Failed to load map","Error",MB_OK|MB_ICONEXCLAMATION);
			game_g->setCursor(cursor_arrow);
			return false;
		}*/
	}

	// generate Civilizations

	if( std_game ) {
		/*Pos2D *start_locations = new Pos2D[1+n_opponent_civs];
		for(int i=0;i<1+n_opponent_civs;i++) {
			if( !map->findRandomFreeSquare(&start_locations[i]) ) {
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "Unable to find enough free squares for players for this map", game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
				delete start_locations;
				game_g->setCursor(cursor_arrow);
				return false;
			}
		}*/

		ASSERT( player_race != NULL );
		const UnitTemplate *unit_template = NULL;
		//City *city = NULL;
		Civilization *cpu = NULL;
		Pos2D start;

		player = new Civilization( this, player_race );
		if( CHEAT ) {
			player->revealMap();
		}
		//player->addTechnology(findTechnology("Sailing"));
		//start = Pos2D(25, 9);
		//start = map->findRandomFreeSquare();
		//start = start_locations[0];
		if( !map->findRandomFreeSquare(&start) ) {
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "Unable to find enough free squares for players for this map", game_g->getFont(), game_g->getPanelTexture());
			window->doModal();
			delete window;
			game_g->setCursor(cursor_arrow);
			return false;
		}
		//map->ensureNearbyTerrain(start, TYPE_FOREST);
		//new City(player, 0, 0);
		//city = new City(player, 10, 20);
		//city->setName("Rome");
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
		new Unit(this, player, unit_template, start);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		new Unit(this, player, unit_template, start);
		/*unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Modern Infantry"));
		new Unit(this, player, unit_template, start);
		unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Modern Tanks"));
		new Unit(this, player, unit_template, start);*/
		/*unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Biplanes"));
		new Unit(this, player, unit_template, start);
		unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Fighters"));
		new Unit(this, player, unit_template, start);
		unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Bombers"));
		new Unit(this, player, unit_template, start);*/
		/*unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Riflemen"));
		new Unit(this, player, unit_template, start);
		unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Infantry"));
		new Unit(this, player, unit_template, start);
		unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Tanks"));
		new Unit(this, player, unit_template, start);*/
		/*unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Militia"));
		new Unit(this, player, unit_template, start);*/
		/*unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Cannon"));
		new Unit(player, unit_template, start);*/
		/*unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Pikemen"));
		new Unit(player, unit_template, start);*/
		/*unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Men At Arms"));
		new Unit(player, unit_template, start);*/
		/*unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Catapults"));
		new Unit(player, unit_template, start);
		unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Catapults"));
		new Unit(player, unit_template, start);
		unit_template = static_cast<const UnitTemplate *>(this->findBuildable("Horsemen"));
		new Unit(player, unit_template, start);*/

		vector<const Race *> races;
		for(size_t i=0;i<game_g->getGameData()->getNRaces();i++) {
			const Race *race = game_g->getGameData()->getRace(i);
			if( race == player_race )
				continue;
			if( race->isDummy() )
				continue;
			races.push_back(race);
		}
		ASSERT( races.size() >= (size_t)n_opponent_civs );
		for(int i=0;i<n_opponent_civs;i++) {
			int r = rand () % races.size();
			const Race *race = races.at(r);

			cpu = new Civilization( this, race );

			//start = start_locations[1+i];
			if( !map->findRandomFreeSquare(&start) ) {
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "Unable to find enough free squares for players for this map", game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
				game_g->setCursor(cursor_arrow);
				return false;
			}

			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
			new Unit(this, cpu, unit_template, start);
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
			new Unit(this, cpu, unit_template, start);

			// remove so we don't get duplicates!
			races.erase( races.begin() + r );
		}

		for(size_t i=0;i<this->getNCivilizations();i++) {
			const Civilization *civilization = this->getCivilization(i);
			ASSERT( civilization->getNUnits() > 0 );
			const Unit *unit = civilization->getUnit(0);
			Pos2D start = unit->getPos();
			if( new_map ) {
				map->ensureNearbyTerrain(start, TYPE_FOREST);
			}
			// start locations should be bonus free (to make more fair, and to give incentive to explore and build new cities)
			for(int j=0;j<City::getNCitySquares();j++) {
				Pos2D pos = City::getCitySquare(map, j, start);
				if( map->isValid(pos.x, pos.y) ) {
					map->getSquare(pos)->setBonusResource(NULL);
				}
			}
		}

		{
			// Rebels
			createRebelCiv();
		}

		//generateRelationships();
		if( !checkRelationships() ) {
			game_g->setCursor(cursor_arrow);
			return false;
		}
	}

	game_g->setCursor(cursor_arrow);
	return true;
}

void MainGamestate::createRebelCiv() {
	LOG("create rebel civilization\n");
	ASSERT( rebel_civ == NULL );
	ASSERT( this->map != NULL );
	rebel_civ = new Civilization(this, game_g->findRace(rebel_race_name));
	rebel_civ->revealMap();
}

void MainGamestate::runTest(const char *filename, TestID index) {
	if( !initGameData(false, false, 0, 0, TOPOLOGY_UNDEFINED, "", NULL, 0) ) {
		clear();
		return;
	}
	this->grid = true;
	VI_log(">>> run test %d\n", index);

	game_g->getGraphicsEnvironment()->setVisible(true);

	Tests::run(this, filename, index);

	clearGameData();
}

void MainGamestate::runTests(TestID id) {
	test_mode = true;

	if( !init() ) {
		clear();
		return;
	}

	// write file header
	char filename[] = "conquests_test_results.csv";
	{
		FILE *file = fopen(filename, "w+");
		ASSERT(file != NULL);
		time_t time_val;
		time(&time_val);
		fprintf(file, "%s\n", ctime(&time_val));
		fprintf(file, "TEST,RESULT\n");
		fclose(file);
	}

	if( id == TEST_ALL ) {
		for(int i=0;i<N_TESTS;i++) {
		//for(int i=0;i<50;i++) {
		//for(int i=50;i<75;i++) {
		//for(int i=75;i<133;i++) {
		//for(int i=133;i<151;i++) {
		//for(int i=151;i<N_TESTS;i++) {
			runTest(filename, (TestID)i);
		}
	}
	else {
		ASSERT( id >= 0 && id < N_TESTS );
		runTest(filename, id);
	}

	clear();
}
