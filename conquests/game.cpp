#include "conquests_stdafx.h"

#include "game.h"
#include "optionsgamestate.h"
#include "maingamestate.h"
#include "map.h"
#include "civilization.h"
#include "city.h"
#include "buildable.h"
#include "technology.h"
#include "unit.h"
#include "citywindow.h"
#include "infowindow.h"
#include "scripts.h"

#include <sstream>
using std::stringstream;

#include "../Vision/VisionPrefs.h"
#include "../Vision/Renderer.h"

Game *game_g = NULL;
//MainGamestate *game = NULL;

int frames = 0;
const int frame_delay = 10;
bool quit = false;

//const char savegames_dirname[] = "conquests_savegames";
char *savegames_dirname = NULL;
char *usermaps_dirname = NULL;

const char *rebel_race_name = "The Rebels";

const int cursor_width_c = 32; // must be a multiple of 8
const int cursor_height_c = 32;

const char *cursor_arrow[] = {
  "X                               ",
  "XX                              ",
  "X.X                             ",
  "X..X                            ",
  "X...X                           ",
  "X....X                          ",
  "X.....X                         ",
  "X......X                        ",
  "X.......X                       ",
  "X........X                      ",
  "X.....XXXXX                     ",
  "X..X..X                         ",
  "X.X X..X                        ",
  "XX  X..X                        ",
  "X    X..X                       ",
  "     X..X                       ",
  "      X..X                      ",
  "      X..X                      ",
  "       XX                       ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "0,0"
};

const char *cursor_wait[] = {
  "XXXXXXXXXXX                     ",
  "X.........X                     ",
  "X.........X                     ",
  " X.......X                      ",
  " X.......X                      ",
  " X.......X                      ",
  "  X.....X                       ",
  "   X...X                        ",
  "    X.X                         ",
  "   X...X                        ",
  "  X.....X                       ",
  " X.......X                      ",
  " X.......X                      ",
  " X.......X                      ",
  "X.........X                     ",
  "X.........X                     ",
  "XXXXXXXXXXX                     ",
  "                                ",
  "                                ", // base
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "0,0"
};

const char *cursor_target[] = {
  "     X                          ",
  "    X.X                         ",
  "    X.X                         ",
  "    X.X                         ",
  " XXXX.XXXX                      ",
  "X.........X                     ",
  " XXXX.XXXX                      ",
  "    X.X                         ",
  "    X.X                         ",
  "    X.X                         ",
  "     X                          ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ", // base
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "5,5"
};

/*const char *cursor_target[] = {
  "     X                          ",
  "     X                          ",
  "     X                          ",
  "     X                          ",
  "     X                          ",
  "XXXXXXXXXXX                     ",
  "     X                          ",
  "     X                          ",
  "     X                          ",
  "     X                          ",
  "     X                          ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ", // base
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "5,5"
};*/

const char *cursor_block[] = {
  "         XX                     ",
  "   XXXXXX.X                     ",
  "  X....X.X                      ",
  " X....X.XX                      ",
  " X...X.X.X                      ",
  " X..X.X..X                      ",
  " X.X.X...X                      ",
  " XX.X....X                      ",
  " X.X....X                       ",
  "X.XXXXXX                        ",
  "XX                              ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ", // base
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "5,5"
};

/*
const char *cursor_[] = {
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ", // base
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "                                ",
  "0,0"
};
*/

void Game::setCursor(const char *image[]) {
	unsigned char data[cursor_width_c*cursor_height_c/8] = "";
	unsigned char mask[cursor_width_c*cursor_height_c/8] = "";

	for(int i=-1,row=0;row<cursor_height_c;row++) {
		for(int col=0;col<cursor_width_c;col++) {
			if ( col % 8 ) {
				data[i] <<= 1;
				mask[i] <<= 1;
			}
			else {
				i++;
				data[i] = mask[i] = 0;
			}
			switch (image[row][col]) {
				case 'X':
					data[i] |= 0x01;
					mask[i] |= 0x01;
					break;
				case '.':
					mask[i] |= 0x01;
					break;
				case ' ':
					break;
			}
		}
	}
	int hot_x = -1, hot_y = -1;
	sscanf(image[cursor_height_c], "%d,%d", &hot_x, &hot_y);
	this->genv->setCursor(data, mask, cursor_width_c, cursor_height_c, hot_x, hot_y);
	//return SDL_CreateCursor(data, mask, 32, 32, hot_x, hot_y);
}

void initButton(VI_Button *button) {
	button->setForeground(0.0f, 0.0f, 0.0f);
	button->setBackground(246.0f/255.0f, 172/255.0f, 100.0f/255.0f);
}

void initListbox(VI_Listbox *listbox) {
	listbox->setForeground(1.0f, 1.0f, 1.0f);
	//listbox->setBackground(246.0f/255.0f, 172/255.0f, 100.0f/255.0f);
	listbox->setHighlightColor(127, 0, 32);
}

// Scripting functions

MainGamestate *AIInterface::mainGamestate = NULL;
Civilization *AIInterface::ai_civilization = NULL;

const Distance *AIInterface::calculateDistanceMap(int *width, int *height, const Unit *unit, int xpos, int ypos, bool include_travel) {
	const Distance *dists = mainGamestate->getMap()->calculateDistanceMap(ai_civilization, unit, include_travel, true, xpos, ypos);
	*width = mainGamestate->getMap()->getWidth();
	*height = mainGamestate->getMap()->getHeight();
	return dists;
}

int AIInterface::getYear() {
	return mainGamestate->getYear();
}

Civilization *AIInterface::getRebelCiv() {
	return mainGamestate->getRebelCiv();
}

vector<Civilization *> AIInterface::getCivilizations(GetcivilizationsType type) {
	// N.B., we return civilizations not yet made contact - this is okay, as the AI may still see their cities or units.
	// Technically this is a cheat - we should really check if they have any units/cities in the AI's fog of war visibility - but will do for now.
	// Note that we also return the rebel civ, if it isn't "dead" (which for the rebel civ, means it has units). If the AI doesn't want to deal with the rebel_civ,
	// it should check for this itself (from Lua, via conquests_isRebelCiv).
	vector<Civilization *> civilizations;
	for(size_t i=0;i<mainGamestate->getNCivilizations();i++) {
		Civilization *civ = mainGamestate->getCivilization(i);
		if( civ == ai_civilization )
			continue;
		if( civ->isDead() )
			continue;
		const Relationship *relationship = mainGamestate->findRelationship(ai_civilization, civ);
		if( type == GETCIVILIZATIONS_WAR && relationship->getStatus() != Relationship::STATUS_WAR )
			continue;
		if( type == GETCIVILIZATIONS_PEACE && relationship->getStatus() != Relationship::STATUS_PEACE )
			continue;
		civilizations.push_back(civ);
	}
	return civilizations;
}

vector<City *> AIInterface::getCities(Civilization *civilization) {
	vector<City *> cities;
	/*const Relationship *relationship = mainGamestate->findRelationship(ai_civilization, civilization);
	ASSERT( relationship->madeContact() );*/
	for(size_t i=0;i<civilization->getNCities();i++) {
		City *city = civilization->getCity(i);
		Pos2D city_pos = city->getPos();
		if( !ai_civilization->isExplored(city_pos.x, city_pos.y) )
			continue;
		cities.push_back(city);
	}
	return cities;
}

vector<Unit *> AIInterface::getUnits(Civilization *civilization) {
	vector<Unit *> units;
	for(size_t i=0;i<civilization->getNUnits();i++) {
		Unit *unit = civilization->getUnit(i);
		Pos2D unit_pos = unit->getPos();
		if( !ai_civilization->isExplored(unit_pos.x, unit_pos.y) )
			continue;
		if( !ai_civilization->isFogOfWarVisible(unit_pos.x, unit_pos.y) )
			continue;
		City *city = mainGamestate->getMap()->findCity(unit_pos);
		if( city != NULL && city->getCivilization() != ai_civilization )
			continue; // can't see inside an enemy city!
		units.push_back(unit);
	}
	return units;
}

const Relationship *AIInterface::findRelationship(const Civilization *civilization) {
	const Relationship *relationship = mainGamestate->findRelationship(civilization, ai_civilization);
	return relationship;
}

bool AIInterface::hasAdjacentUnexplored(int xpos, int ypos) {
	for(int iy=ypos-1;iy<=ypos+1;iy++) {
		for(int ix=xpos-1;ix<=xpos+1;ix++) {
			if( !mainGamestate->getMap()->isValid(ix, iy) )
				continue;
			if( ix == xpos && iy == ypos ) {
				continue;
			}
			if( !ai_civilization->isExplored(ix, iy) ) {
				return true;
			}
		}
	}
	return false;
}

bool AIInterface::hasAdjacentExplored(int xpos, int ypos) {
	for(int iy=ypos-1;iy<=ypos+1;iy++) {
		for(int ix=xpos-1;ix<=xpos+1;ix++) {
			if( !mainGamestate->getMap()->isValid(ix, iy) )
				continue;
			if( ix == xpos && iy == ypos ) {
				continue;
			}
			if( ai_civilization->isExplored(ix, iy) ) {
				return true;
			}
		}
	}
	return false;
}

vector<Pos2D> AIInterface::getAdjacentSquares(int xpos, int ypos, bool include_centre) {
	vector<Pos2D> squares;
	for(int iy=ypos-1;iy<=ypos+1;iy++) {
		for(int ix=xpos-1;ix<=xpos+1;ix++) {
			if( !mainGamestate->getMap()->isValid(ix, iy) )
				continue;
			if( !include_centre && ix == xpos && iy == ypos ) {
				continue;
			}
			if( !ai_civilization->isExplored(ix, iy) ) {
				continue;
			}
			int x = ix, y = iy;
			mainGamestate->getMap()->reduceToBase(&x, &y);
			squares.push_back(Pos2D(x, y));
		}
	}
	return squares;
}

vector<Pos2D> AIInterface::getCitySquares(int xpos, int ypos, bool include_centre) {
	vector<Pos2D> squares;
	for(int i=0;i<City::getNCitySquares();i++) {
		if( !include_centre && i == City::getCentreCitySquareIndex() ) {
			continue;
		}
		Pos2D city_pos = City::getCitySquare(mainGamestate->getMap(), i, Pos2D(xpos, ypos));
		if( !mainGamestate->getMap()->isValid(city_pos.x, city_pos.y) ) {
			continue;
		}
		if( !ai_civilization->isExplored(city_pos.x, city_pos.y) ) {
			continue;
		}
		squares.push_back(city_pos);
	}
	return squares;
}

Type AIInterface::getTerrain(int xpos, int ypos) {
	if( ai_civilization->isExplored(xpos, ypos) ) {
		return mainGamestate->getMap()->getSquare(xpos, ypos)->getType();
	}
	return TYPE_UNKNOWN;
}

const BonusResource *AIInterface::getBonusResource(int xpos, int ypos) {
	if( ai_civilization->isExplored(xpos, ypos) ) {
		return mainGamestate->getMap()->getSquare(xpos, ypos)->getBonusResource();
	}
	return NULL;
}

Road AIInterface::getRoad(int xpos, int ypos) {
	if( ai_civilization->isExplored(xpos, ypos) ) {
		return mainGamestate->getMap()->getSquare(xpos, ypos)->getRoad();
	}
	return ROAD_UNKNOWN;
}

const Civilization *AIInterface::getTerritory(int xpos, int ypos) {
	if( ai_civilization->isExplored(xpos, ypos) ) {
		return mainGamestate->getMap()->getSquare(xpos, ypos)->getTerritory();
	}
	return NULL;
}

bool AIInterface::canBuildCity(int xpos, int ypos) {
	bool can_build = false;
	if( ai_civilization->isExplored(xpos, ypos) ) {
		// TODO: still cheating here, as nearby squares might not be explored
		can_build = mainGamestate->getMap()->getSquare(xpos, ypos)->canBuildCity();
	}
	return can_build;
}

bool AIInterface::givesRoadBonus(int xpos, int ypos) {
	if( ai_civilization->isExplored(xpos, ypos) ) {
		return mainGamestate->getMap()->getSquare(xpos, ypos)->givesRoadBonus();
	}
	return false;
}

bool AIInterface::canBeImproved(int xpos, int ypos) {
	if( ai_civilization->isExplored(xpos, ypos) ) {
		return mainGamestate->getMap()->getSquare(xpos, ypos)->canBeImproved(ai_civilization);
	}
	return false;
}

bool AIInterface::hasEnemies(int xpos, int ypos) {
	if( ai_civilization->isExplored(xpos, ypos) && ai_civilization->isFogOfWarVisible(xpos, ypos) ) {
		return mainGamestate->getMap()->getSquare(xpos, ypos)->hasEnemies(ai_civilization);
	}
	return false;
}

const vector<Unit *> *AIInterface::getUnits(int xpos, int ypos) {
	if( !ai_civilization->isExplored(xpos, ypos) )
		return NULL;
	if( !ai_civilization->isFogOfWarVisible(xpos, ypos) )
		return NULL;
	City *city = mainGamestate->getMap()->findCity(xpos, ypos);
	if( city != NULL && city->getCivilization() != ai_civilization )
		return NULL; // can't see inside an enemy city!
	const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(xpos, ypos);
	return units;
}

Unit *AIInterface::getBestDefender(int xpos, int ypos) {
	if( !ai_civilization->isExplored(xpos, ypos) )
		return NULL;
	if( !ai_civilization->isFogOfWarVisible(xpos, ypos) )
		return NULL;
	City *city = mainGamestate->getMap()->findCity(xpos, ypos);
	if( city != NULL && city->getCivilization() != ai_civilization )
		return NULL; // can't see inside an enemy city!

	return mainGamestate->getMap()->getSquare(xpos, ypos)->getBestDefender();
}

City *AIInterface::getCity(int xpos, int ypos) {
	if( ai_civilization->isExplored(xpos, ypos) ) {
		return mainGamestate->getMap()->findCity(xpos, ypos);
	}
	return NULL;
}

vector<const Buildable *> AIInterface::cityGetBuildables(const City *city, GetbuildablesType type) {
	vector<const Buildable *> buildables;
	if( city->getCivilization() != ai_civilization ) {
		return buildables;
	}
	for(size_t i=0;i<game_g->getGameData()->getNBuildables();i++) {
		const Buildable *buildable = game_g->getGameData()->getBuildable(i);
		if( type == GETBUILDABLES_IMPROVEMENTS && buildable->getType() != Buildable::TYPE_IMPROVEMENT )
			continue;
		if( type == GETBUILDABLES_UNITS && buildable->getType() != Buildable::TYPE_UNIT )
			continue;
		if( city->canBuild(buildable) ) {
			buildables.push_back(buildable);
		}
	}
	return buildables;
}

const Buildable *AIInterface::cityGetBuildable(const City *city) {
	if( city->getCivilization() != ai_civilization ) {
		return NULL;
	}
	return city->getBuildable();
}

bool AIInterface::canBomb(const UnitTemplate *unit_template, const City *source_city, Pos2D target, bool check_launch_capability) {
	if( check_launch_capability && !source_city->canLaunch(unit_template) ) {
		return false;
	}
	return mainGamestate->canBomb(unit_template, source_city->getPos(), target);
}

#if 0
bool Game::runScript(const char *script, const char *function) {
	/*int lua_result = luaL_dofile(ls, script);
	if( lua_result != 0 ) {
		VI_log("Failed to do Lua script %s, error %d\n", script, lua_result);
		VI_log("Lua error: %s\n", lua_tostring(ls, -1));
		return false;
	}*/
	// load the file
	int lua_result = luaL_loadfile(ls, script);
	if( lua_result != 0 ) {
		VI_log("Failed to load Lua script %s, error %d\n", script, lua_result);
		VI_log("Lua error: %s\n", lua_tostring(ls, -1));
		return false;
	}
	// run the main (global)
	lua_result = lua_pcall(ls, 0, LUA_MULTRET, 0);
	if( lua_result != 0 ) {
		VI_log("Failed to run Lua script %s, error %d\n", script, lua_result);
		VI_log("Lua error: %s\n", lua_tostring(ls, -1));
		return false;
	}
	if( function != NULL ) {
		pushScriptFunction(function);
		return runScriptFunction(0);
	}
	return true;
}
#endif

/* Loads the script into memory, and runs the globals.
 */
bool Game::loadScript(const char *script) {
	/*int lua_result = luaL_dofile(ls, script);
	if( lua_result != 0 ) {
		VI_log("Failed to do Lua script %s, error %d\n", script, lua_result);
		VI_log("Lua error: %s\n", lua_tostring(ls, -1));
		return false;
	}*/
	// load the file
	int lua_result = luaL_loadfile(ls, script);
	if( lua_result != 0 ) {
		VI_log("Failed to load Lua script %s, error %d\n", script, lua_result);
		VI_log("Lua error: %s\n", lua_tostring(ls, -1));
		return false;
	}
	// run the main (global)
	lua_result = lua_pcall(ls, 0, LUA_MULTRET, 0);
	if( lua_result != 0 ) {
		VI_log("Failed to run Lua script %s, error %d\n", script, lua_result);
		VI_log("Lua error: %s\n", lua_tostring(ls, -1));
		return false;
	}
	return true;
}

void Game::pushScriptFunction(const char *function) {
	lua_getfield(ls, LUA_GLOBALSINDEX, function); // function to be called
}

bool Game::runScriptFunction(int n_args) {
	// run the function previously pushed by pushScriptFunction
	int lua_result = lua_pcall(ls, n_args, LUA_MULTRET, 0);
	if( lua_result != 0 ) {
		VI_log("Failed to run Lua function, error %d\n", lua_result);
		VI_log("Lua error: %s\n", lua_tostring(ls, -1));
		return false;
	}
	return true;
}

/*bool MainGamestate::runScript(const char *script) {
	AIInterface::setMainGamestate(this);
	bool ok = game_g->runScript(script);
	AIInterface::setMainGamestate(NULL);
	return ok;
}*/

// end scripting functions

/*void Game::generateRelationships() {
ASSERT( relationships.size() == 0 );
for(int i=0;i<this->getNCivilizations();i++) {
Civilization *civ1 = this->getCivilization(i);
for(int j=i+1;j<this->getNCivilizations();j++) {
Civilization *civ2 = this->getCivilization(j);
Relationship *relationship = new Relationship(civ1, civ2);
relationships.push_back(relationship);
}
}
}*/

Element::Element(const char *name, const Improvement *requires_improvement, const Technology *requires_technology, int initial_amount, int base_rate) :
name(name), requires_improvement(requires_improvement), requires_technology(requires_technology), initial_amount(initial_amount), base_rate(base_rate)
{
	game_g->addElement(this);
}

BonusResource::BonusResource(const char *name, const unsigned char rgb[3], Type terrain, const Improvement *requires_improvement, const Technology *requires_technology, int bonus_production, int bonus_reduce_growth_time) :
name(name), texture(NULL), //terrain(terrain),
requires_improvement(requires_improvement), requires_technology(requires_technology),
bonus_production(bonus_production), bonus_reduce_growth_time(bonus_reduce_growth_time)
{
	this->addTerrain(terrain);
	unsigned char col_min[] = {0, 0, 0};
	VI_Image *image = VI_Image::createRadial(64, 64, rgb, col_min, true, true);
	texture = VI_createTexture(image);
	game_g->addBonusResource(this);
}

bool BonusResource::canSee(const Civilization *civilization) const {
	// whether a civilization can see a bonus.
	bool bonus_ok = true;
	if( bonus_ok && this->getRequiresTechnology() != NULL ) {
		if( !civilization->hasTechnology( this->getRequiresTechnology() ) ) {
			bonus_ok = false;
		}
	}
	return bonus_ok;
}

bool BonusResource::canUse(const City *city) const {
	// whether a city can make use of a bonus.
	bool bonus_ok = true;
	const Improvement *improvement = this->getRequiresImprovement();
	if( bonus_ok && improvement != NULL ) {
		/*if( !city->hasImprovement( improvement ) ) {
			bonus_ok = false;
			// check for improvements which replace the required improvement
			while( improvement->getReplacedBy() != NULL && !bonus_ok ) {
				ASSERT( improvement->getReplacedBy()->getType() == Buildable::TYPE_IMPROVEMENT );
				improvement = static_cast<const Improvement *>(improvement->getReplacedBy());
				if( city->hasImprovement( improvement ) ) {
					bonus_ok = true;
				}
			}
		}*/
		if( !city->hasImprovementOrNewer( improvement ) ) {
			bonus_ok = false;
		}
	}
	if( bonus_ok && !this->canSee(city->getCivilization()) ) {
		bonus_ok = false;
	}
	return bonus_ok;
}

void GameData::addTechnology(Technology *technology) {
	technologies.push_back(technology);
}

void GameData::addBuildable(Buildable *buildable) {
	buildables.push_back(buildable);
	if( buildable->getType() == Buildable::TYPE_IMPROVEMENT ) {
		const Improvement *improvement = static_cast<const Improvement *>(buildable);
		improvements.push_back(improvement);
	}
	else if( buildable->getType() == Buildable::TYPE_UNIT ) {
		const UnitTemplate *unit_template = static_cast<const UnitTemplate *>(buildable);
		unit_templates.push_back(unit_template);
	}
	else {
		ASSERT(false);
	}
}

/*void Game::lockGUI() {
ASSERT( !mainGUILocked );
VI_log("Game::lockGUI\n");
mainGUILocked = true;
}

void Game::unlockGUI() {
ASSERT( mainGUILocked );
VI_log("Game::unlockGUI\n");
mainGUILocked = false;
}*/

void Game::raceAction(VI_Panel *source) {
	VI_Textfield *textfieldInfo = static_cast<VI_Textfield *>(source->getUserData());
	ASSERT( textfieldInfo != NULL );
	VI_Listbox *listbox = dynamic_cast<VI_Listbox *>(source);
	int active = listbox->getActive();
	ASSERT( active >= 0 && active < game_g->getGameData()->getNRaces() );
	const Race *race = game_g->getGameData()->getRace(active);
	textfieldInfo->clear();
	textfieldInfo->addText( race->getHelp().c_str() );
	textfieldInfo->setCPos(0);
}

bool Game::setupNewGame(MainGamestate *mainGamestate) {
	// receives a new MainGamestate, or one that has clearGameData called on it
	// asks player for new game preferences, and then calls the initGameData method
	// returns false on failure, or if the player cancels
	{
		const int n_buttons_c = MainGamestate::N_DIFFICULTY + 1;
		string buttons[n_buttons_c];
		for(int i=0;i<MainGamestate::N_DIFFICULTY;i++) {
			buttons[i] = MainGamestate::getDifficultyString((MainGamestate::Difficulty)i);
		}
		buttons[n_buttons_c-1] = "Cancel";
		string text = "Please select a difficulty level.";
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getGamestate()->getPanel(), text, game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, 300, 300);
		for(int i=0;i<MainGamestate::N_DIFFICULTY;i++) {
			//window->getButton(i)->disableKeyShortcut();
			window->getButton(i)->setKeyShortcut( MainGamestate::getDifficultyShortcut((MainGamestate::Difficulty)i) );
		}
		int difficulty_level = window->doModal();
		VI_log("difficulty level %d\n", difficulty_level);
		delete window;

		/*MainGamestate::Difficulty difficulty = MainGamestate::DIFFICULTY_UNDEFINED;
		if( difficulty_level == 0 )
			difficulty = MainGamestate::DIFFICULTY_EASY;
		else if( difficulty_level == 1 )
			difficulty = MainGamestate::DIFFICULTY_MEDIUM;
		else if( difficulty_level == 2 )
			difficulty = MainGamestate::DIFFICULTY_HARD;
		else if( difficulty_level == 3 )
			difficulty = MainGamestate::DIFFICULTY_INSANE;
		else {
			VI_log("unrecognised difficulty level\n");
			ASSERT( false );
		}
		mainGamestate->setDifficulty(difficulty);*/

		if( difficulty_level == n_buttons_c-1 ) {
			return false;
		}
		ASSERT( difficulty_level >= 0 && difficulty_level < MainGamestate::N_DIFFICULTY );
		MainGamestate::Difficulty difficulty = (MainGamestate::Difficulty)difficulty_level;
		mainGamestate->setDifficulty(difficulty);
	}

	bool new_map = false;
	{
		const int n_buttons_c = 3;
		string buttons[n_buttons_c] = {"New random map", "Load map", "Cancel"};
		string text = "Do you wish to create a new random world map, or load an existing world map?";
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getGamestate()->getPanel(), text, game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, 300, 300);
		window->getButton(0)->setKeyShortcut(V_N);
		window->getButton(1)->setKeyShortcut(V_L);
		int world_map = window->doModal();
		VI_log("world_map %d\n", world_map);
		delete window;
		if( world_map == 0 ) {
			new_map = true;
		}
		else if( world_map == 1 ) {
			new_map = false;
		}
		else if( world_map == 2 ) {
			return false;
		}
		else {
			VI_log("unrecognised world map\n");
			ASSERT( false );
		}
	}

	int map_width = 0, map_height = 0;
	Topology topology = TOPOLOGY_UNDEFINED;
	string map_filename;
	if( new_map ) {
		{
			const int n_buttons_c = 4;
			string buttons[n_buttons_c] = {"Small", "Medium", "Large", "Cancel"};
			string text = "Please select a map size.";
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getGamestate()->getPanel(), text, game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, 300, 300);
			window->getButton(0)->setKeyShortcut(V_S);
			window->getButton(1)->setKeyShortcut(V_M);
			window->getButton(2)->setKeyShortcut(V_L);
			int map_size = window->doModal();
			VI_log("map_size %d\n", map_size);
			delete window;

			if( map_size == 0 ) {
				map_width = 75;
				map_height = 30;
			}
			else if( map_size == 1 ) {
				map_width = 100;
				map_height = 40;
			}
			else if( map_size == 2 ) {
				map_width = 150;
				map_height = 60;
			}
			else if( map_size == 3 ) {
				return false;
			}
			else {
				VI_log("unrecognised map size\n");
				ASSERT( false );
			}
		}

		{
			const int n_buttons_c = 3;
			string buttons[n_buttons_c] = {"Flat", "Round", "Cancel"};
			string text = "Please select a world type.";
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getGamestate()->getPanel(), text, game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, 300, 300);
			window->getButton(0)->setKeyShortcut(V_F);
			window->getButton(1)->setKeyShortcut(V_R);
			int res = window->doModal();
			delete window;
			VI_log("topology res %d\n", res);
			if( res == 0 ) {
				topology = TOPOLOGY_FLAT;
			}
			else if( res == 1 ) {
				topology = TOPOLOGY_CYLINDER;
			}
			else if( res == 2 ) {
				return false;
			}
			else {
				ASSERT( false );
			}
			VI_log("topology %d\n", topology);
		}

	}
	else {
		MainGamestate::LoadDialogStatus loadDialogStatus = MainGamestate::LOADDIALOGSTATUS_NOFILES;
		//map_filename = MainGamestate::loadDialog(&loadDialogStatus, "Load Map", getFullPath("data/maps/").c_str(), ".tmx", false);
		vector<string> map_dirnames;
		map_dirnames.push_back( getFullPath("data/maps/") );
		map_dirnames.push_back( usermaps_dirname );
		map_filename = MainGamestate::loadDialog(&loadDialogStatus, "Load Map", &map_dirnames, maps_ext, false, true);
		if( loadDialogStatus == MainGamestate::LOADDIALOGSTATUS_NOFILES ) {
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), game_g->getGamestate()->getPanel(), "No world map files available", game_g->getFont(), game_g->getPanelTexture());
			window->doModal();
			delete window;
			return false;
		}
		else if( loadDialogStatus != MainGamestate::LOADDIALOGSTATUS_OK ) {
			return false;
		}

	}

	int n_opponent_civs = 0;
	{
		const int min_civs_c = 4, max_civs_c = 6;
		int n_races = 0;
		for(int i=0;i<game_g->getGameData()->getNRaces();i++) {
			const Race *race = game_g->getGameData()->getRace(i);
			if( !race->isDummy() ) {
				n_races++;
			}
		}
		//ASSERT( max_civs_c+1 <= game_g->getGameData()->getNRaces() );
		ASSERT( max_civs_c+1 <= n_races );
		const int n_buttons_c = max_civs_c - min_civs_c + 1 + 1;
		string buttons[n_buttons_c];
		for(int i=0;i<n_buttons_c-1;i++) {
			stringstream str;
			str << (min_civs_c + i) << " civs";
			buttons[i] = str.str();
		}
		buttons[n_buttons_c-1] = "Cancel";
		string text = "Please select number of opponent civilizations.";
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getGamestate()->getPanel(), text, game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, 300, 300);
		for(int i=0;i<n_buttons_c-1;i++) {
			window->getButton(i)->disableKeyShortcut();
		}
		int res = window->doModal();
		delete window;
		if( res == n_buttons_c-1 ) {
			return false;
		}
		n_opponent_civs = res + min_civs_c;
		VI_log("n_opponent_civs = %d\n", n_opponent_civs);
	}

	{
		const int n_buttons_c = MainGamestate::N_AIAGGRESSION + 1;
		string buttons[n_buttons_c];
		for(int i=0;i<MainGamestate::N_AIAGGRESSION;i++) {
			buttons[i] = MainGamestate::getAIAggressionString((MainGamestate::AIAggression)i);
		}
		buttons[n_buttons_c-1] = "Cancel";
		string text = "Please select the level of AI Aggression.";
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getGamestate()->getPanel(), text, game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, 400, 300);
		for(int i=0;i<n_buttons_c-1;i++) {
			window->getButton(i)->setKeyShortcut( MainGamestate::getAIAggressionShortcut((MainGamestate::AIAggression)i) );
		}
		int aiaggression_level = window->doModal();
		VI_log("aiaggression level %d\n", aiaggression_level);
		delete window;

		if( aiaggression_level == n_buttons_c-1 ) {
			return false;
		}
		ASSERT( aiaggression_level >= 0 && aiaggression_level < MainGamestate::N_AIAGGRESSION );
		MainGamestate::AIAggression aiaggression = (MainGamestate::AIAggression)aiaggression_level;
		mainGamestate->setAIAggression(aiaggression);
	}

	{
		const int n_choices_c = 4;
		const int n_buttons_c = n_choices_c + 1;
		int game_n_turns_choices[n_choices_c] = {300, 400, 500, 0};
		string buttons[n_buttons_c] = {"300 turns", "400 turns", "500 turns", "Unlimited", "Cancel"};
		string text = "Please select the length of the game.";
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getGamestate()->getPanel(), text, game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, 400, 300);
		window->getButton(0)->setKeyShortcut(V_3);
		window->getButton(1)->setKeyShortcut(V_4);
		window->getButton(2)->setKeyShortcut(V_5);
		window->getButton(3)->setKeyShortcut(V_U);
		int res = window->doModal();
		VI_log("game_n_turns res %d\n", res);
		delete window;

		if( res == n_buttons_c-1 ) {
			return false;
		}
		ASSERT( res >= 0 && res < n_choices_c );
		int game_n_turns = game_n_turns_choices[res];
		VI_log("game_n_turns %d\n", game_n_turns);
		mainGamestate->setGameNTurns(game_n_turns);
	}

	const Race *player_race = NULL;
	{
		const int n_buttons_c = 2;
		string buttons[n_buttons_c] = {"Okay", "Cancel"};
		//const int window_h_c = 512;
		int window_h_c = game_g->getGraphicsEnvironment()->getHeight() - infowindow_def_y_c - 32;
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), this->getGamestate()->getPanel(), "", game_g->getFont(), game_g->getPanelTexture(), buttons, n_buttons_c, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, window_h_c);
		VI_Panel *windowPanel = window->getPanel();

		VI_Textfield *textfield = VI_createTextfield(game_g->getFont(), infowindow_def_w_c - 32, 64);
		textfield->addText("Please select a civilization that you wish to play.");
		textfield->setForeground(1.0f, 1.0f, 1.0f);
		windowPanel->addChildPanel(textfield, 16, 16);

		vector<string> entries;
		for(size_t i=0;i<game_g->getGameData()->getNRaces();i++) {
			const Race *race = game_g->getGameData()->getRace(i);
			if( !race->isDummy() ) {
				entries.push_back( race->getName() );
			}
		}

		//VI_Listbox *listbox = VI_createListbox(infowindow_def_w_c - 32, window_h_c - 128, &*entries.begin(), entries.size(), this->getFont());
		VI_Listbox *listbox = VI_createListbox(infowindow_def_w_c/2 - 16, window_h_c - 128, &*entries.begin(), entries.size(), this->getFont());
		initListbox(listbox);
		listbox->setHasInputFocus(true);
		//listbox->setForeground(1.0f, 1.0f, 1.0f);
		listbox->setActive(0);
		windowPanel->addChildPanel(listbox, 16, 80);

		VI_Textfield *textfieldInfo = VI_createTextfield(game_g->getFont(), infowindow_def_w_c/2 - 16, window_h_c - 128);
		textfieldInfo->setForeground(1.0f, 1.0f, 1.0f);
		windowPanel->addChildPanel(textfieldInfo, infowindow_def_w_c/2, 80);

		listbox->setUserData(textfieldInfo);
		listbox->setAction(raceAction);
		raceAction(listbox);

		int res = window->doModal();
		int active = listbox->getActive();
		VI_log("race res %d\n", res);
		VI_log("race index %d\n", active);
		delete window;

		if( res == n_buttons_c-1 ) {
			return false;
		}
		ASSERT( active >= 0 );
		ASSERT( active < game_g->getGameData()->getNRaces() );

		player_race = game_g->getGameData()->getRace(active);

	}

	bool ok = mainGamestate->initGameData(true, new_map, map_width, map_height, topology, map_filename.c_str(), player_race, n_opponent_civs);
	if( !ok ) {
		VI_log("failed to initGameData\n");
	}
	else {
		mainGamestate->beginTurn(); // initialisation for first turn
	}
	return ok;
}

class StdMessageInterface : public MessageInterface {
public:
    virtual void show(const char *message) {
        VI_log("Vision::load() message: %s\n", message);
#ifdef _WIN32
        MessageBoxA(NULL, message, "Warning", MB_OK|MB_ICONEXCLAMATION);
#endif
	}
    virtual void log(const char *message) {
		VI_log(message);
    }
};

void Game::updateProgress(int progress_percentage) {
	//this->display_progress = true;
	this->progress_percentage = progress_percentage;
	this->genv->drawFrame();
	this->genv->swapBuffers();
}

void Game::paintPanel(VI_Graphics2D *g2d, void *data) {
	const int width = (int)(game_g->getGraphicsEnvironment()->getWidth() * 0.25f);
	const int height = 64;
	const int xpos = (int)(game_g->getGraphicsEnvironment()->getWidth()*0.5f - width*0.5f);
	const int ypos = (int)(game_g->getGraphicsEnvironment()->getHeight()*0.5f - height*0.5f);
	g2d->setColor3i(255, 255, 255);
	game_g->getLargefont()->writeText(xpos, ypos - 32, "Loading...");
	g2d->drawRect(xpos, ypos, width, height);
	int progress_width = (int)(((width-2) * game_g->progress_percentage) / 100.0f);
	g2d->setColor3i(127, 0, 0);
	g2d->fillRect(xpos+1, ypos+1, progress_width, height-2);
	/*
	// test drawing
	g2d->setColor3i(255, 255, 255);
	g2d->fillRect(32, 64, 2, 2);
	g2d->fillRect(48, 64, 3, 3);
	g2d->fillRect(64, 64, 4, 4);
	g2d->fillRect(80, 64, 8, 8);
	g2d->fillRect(96, 64, 16, 16);
	g2d->fillRect(160, 64, 31, 31);
	g2d->fillRect(224, 64, 32, 32);
	g2d->setColor3i(255, 0, 0);
	g2d->drawRect(32, 64, 2, 2);
	g2d->drawRect(48, 64, 3, 3);
	g2d->drawRect(64, 64, 4, 4);
	g2d->drawRect(80, 64, 8, 8);
	g2d->drawRect(96, 64, 16, 16);
	g2d->drawRect(160, 64, 31, 31);
	g2d->drawRect(224, 64, 32, 32);
	*/
}

bool Game::init() {
	VI_initialise("conquests");
	const char savegames_subdirname[] = "savegames";
	savegames_dirname = VI_getApplicationFilename(savegames_subdirname);
	const char usermaps_subdirname[] = "usermaps";
	usermaps_dirname = VI_getApplicationFilename(usermaps_subdirname);

	// create usermap folder for user
	if( !VI_createFolder(usermaps_dirname) ) {
		VI_log("failed to create usermaps folder, never mind: %s\n", usermaps_dirname);
	}

	VisionPrefs visionPrefs;
	StdMessageInterface messageInterface;
	visionPrefs.load(&messageInterface);
	// disable the shaders we don't care about
	visionPrefs.atmosphere = false;
	visionPrefs.bumpmapping = false;
	visionPrefs.watereffects = false;

	try {
		genv = VI_createGraphicsEnvironment(&visionPrefs);
	}
	catch(VisionException *ve) {
		delete ve;
		bool done = false;
		if( visionPrefs.fullscreen ) {
			VI_log("Failed to run in fullscreen mode\n");
#ifdef _WIN32
			MessageBoxA(NULL,"Failed to run in fullscreen mode","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
			visionPrefs.fullscreen = false;
			try {
				genv = VI_createGraphicsEnvironment(&visionPrefs);
			}
			catch(VisionException *ve) {
				delete ve;
				done = true;
			}
		}
		if( !done ) {
			VI_log("Failed to open graphics window\n");
#ifdef _WIN32
			MessageBoxA(NULL,"Failed to open graphics window","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
			return false;
		}
	}

/*#ifdef _WIN32
	HCURSOR cursor = LoadCursor( NULL, IDC_WAIT );
	SetCursor(cursor);
	SetClassLong(genv->getHWND(), GCL_HCURSOR, (LONG)cursor);
#endif*/
	this->setCursor(cursor_wait);
	{
		char buffer[4096] = "";
		sprintf(buffer, "Conquests v%d.%d - Loading ...", versionMajor, versionMinor);
		game_g->getGraphicsEnvironment()->setWindowTitle(buffer);
	}

	// must load fonts before drawing the progress bar
#ifdef _WIN32
	/*smallfont = game_g->getGraphicsEnvironment()->createFont("Verdana",10,FW_NORMAL);
	font = game_g->getGraphicsEnvironment()->createFont("Verdana",12,FW_NORMAL);
	largefont = game_g->getGraphicsEnvironment()->createFont("Arial",24,FW_BOLD);*/
	smallfont = game_g->getGraphicsEnvironment()->createFont("c:/windows/fonts/Verdana.ttf",10,FW_NORMAL);
	font = game_g->getGraphicsEnvironment()->createFont("c:/windows/fonts/Verdana.ttf",12,FW_NORMAL);
	largefont = game_g->getGraphicsEnvironment()->createFont("c:/windows/fonts/Arial.ttf",24,FW_BOLD);
#else
	smallfont = game_g->getGraphicsEnvironment()->createFont("/usr/share/fonts/truetype/freefont/FreeSans.ttf",10,0);
	font = game_g->getGraphicsEnvironment()->createFont("/usr/share/fonts/truetype/freefont/FreeSans.ttf",12,0);
	largefont = game_g->getGraphicsEnvironment()->createFont("/usr/share/fonts/truetype/freefont/FreeSans.ttf",24,0);
#endif

	VI_Panel *gamePanel = VI_createPanel();
	gamePanel->setPaintFunc(paintPanel, NULL);
	genv->setPanel(gamePanel);
	updateProgress(0);

	VI_log("initialising lua\n");
	ls = lua_open();
	if( ls == NULL ) {
		this->setCursor(cursor_arrow);
		VI_log("failed to open lua\n");
#ifdef _WIN32
		MessageBoxA(NULL,"Failed to open lua","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
		return false;
	}
	luaL_openlibs(ls);
	registerScripts(ls);
	updateProgress(5);

	try {
		sound = VI_Sound::create();
	}
	catch(VisionException *ve) {
		delete ve;
	    VI_log("Failed to initialise sound\n");
#ifdef _WIN32
		MessageBoxA(NULL,"Failed to initialise sound","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
	}
	updateProgress(10);
/*#ifdef _WIN32
	if( sound != NULL && !sound->initHWND( game_g->getGraphicsEnvironment()->getHWND() ) ) {
	    VI_log("Failed to initialise sound HWND\n");
		MessageBoxA(NULL,"Failed to initialise sound HWND","Error",MB_OK|MB_ICONEXCLAMATION);
	}
#endif*/

	/*VI_Texture *cursor = VI_createTextureWithMask("textures/arrow.png", 255, 255, 255);
	genv->setMouseTexture(cursor);*/

	// load shaders
	/*this->splat_pixel_shader_ambient = NULL;
	this->splat_3d_pixel_shader_ambient = NULL;
	this->splat_vertex_shader_ambient = NULL;*/
	this->splat_shader_ambient = NULL;
	this->splat_3d_shader_ambient = NULL;
	if( game_g->getGraphicsEnvironment()->hasShaders() && visionPrefs.terrainsplatting ) {
#if 0
		try {
			/*this->splat_vertex_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_vs", true);
			this->splat_pixel_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_ps", false);
			this->splat_3d_pixel_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_3d_ps", false);*/
			/*Shader *splat_vertex_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_vs", true);
			Shader *splat_pixel_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_ps", false);
			this->splat_shader_ambient = new ShaderEffect(splat_vertex_shader_ambient, splat_pixel_shader_ambient);
			//Shader *splat_3d_vertex_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_vs", true);
			Shader *splat_3d_pixel_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_3d_ps", false);
			this->splat_3d_shader_ambient = new ShaderEffect(splat_vertex_shader_ambient, splat_3d_pixel_shader_ambient);*/
			this->splat_shader_ambient = ShaderEffect::create(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_vs", "main_splat_ambient_ps");
			this->splat_3d_shader_ambient = ShaderEffect::create(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_vs", "main_splat_ambient_3d_ps");
		}
		catch(VisionException *ve) {
			// carry on without shaders if we can't load it
			VI_log("Failed to load terrain splatting shader\n");
			delete ve;
			// n.b., don't delete the shaders, as they'll be deleted by the GraphicsEnvironment upon exit
			/*if( this->splat_vertex_shader_ambient != NULL ) {
				//delete this->splat_vertex_shader_ambient;
				this->splat_vertex_shader_ambient = NULL;
			}
			if( this->splat_pixel_shader_ambient != NULL ) {
				//delete this->splat_pixel_shader_ambient;
				this->splat_pixel_shader_ambient = NULL;
			}
			if( this->splat_3d_pixel_shader_ambient != NULL ) {
				this->splat_3d_pixel_shader_ambient = NULL;
			}*/
			if( this->splat_shader_ambient != NULL ) {
				this->splat_shader_ambient = NULL;
			}
			if( this->splat_3d_shader_ambient != NULL ) {
				this->splat_3d_shader_ambient = NULL;
			}
		}
#endif
#if 0
		Shader *splat_vertex_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_vs", true);
		Shader *splat_pixel_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_ps", false);
		if( splat_vertex_shader_ambient != NULL && splat_pixel_shader_ambient != NULL )
			this->splat_shader_ambient = new ShaderEffect(splat_vertex_shader_ambient, splat_pixel_shader_ambient);
		else {
			VI_log("Failed to load 2D terrain splatting shader\n");
		}

		//Shader *splat_3d_vertex_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_vs", true);
		Shader *splat_3d_pixel_shader_ambient = Shader::createShader(genv, getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_3d_ps", false);
		if( splat_vertex_shader_ambient != NULL && splat_3d_pixel_shader_ambient != NULL )
			this->splat_3d_shader_ambient = new ShaderEffect(splat_vertex_shader_ambient, splat_3d_pixel_shader_ambient);
		else {
			VI_log("Failed to load 3D terrain splatting shader\n");
		}
#endif
	}
	try {
		splat_shader_ambient = genv->getRenderer()->createShaderEffectSimpleCG(getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_vs", "main_splat_ambient_ps");
	}
	catch(VisionException *ve) {
		VI_log("Failed to load 2D terrain splatting shader\n");
		delete ve;
	}
	updateProgress(15);
	try {
		splat_3d_shader_ambient = genv->getRenderer()->createShaderEffectSimpleCG(getFullPath("shaders/shaders.cg").c_str(), "main_splat_ambient_vs", "main_splat_ambient_3d_ps");
	}
	catch(VisionException *ve) {
		VI_log("Failed to load 3D terrain splatting shader\n");
		delete ve;
	}
	updateProgress(20);

	try {
		MapSquare::loadTextures();
		updateProgress(30);

		unsigned char filter_max_panel[3] = {220, 172, 40};
		unsigned char filter_min_panel[3] = {120, 90, 20};
		//unsigned char filter_max_panel[3] = {176, 138, 32};
		//unsigned char filter_min_panel[3] = {96, 72, 16};
		//VI_Image *image = VI_createImageNoise(64, 64, 16.0f, filter_max_panel, filter_min_panel, V_NOISEMODE_PERLIN, 4);
		VI_Image *image = VI_Image::createNoise(256, 256, 16.0f, 16.0f, filter_max_panel, filter_min_panel, V_NOISEMODE_PERLIN, 4, false);
		panelTexture = VI_createTexture(image);
		updateProgress(35);

		ageMedievalTexture = VI_createTexture(getFullPath("data/gfx/age_medieval.jpg").c_str());
		ageIndustrialTexture = VI_createTexture(getFullPath("data/gfx/age_industrial.jpg").c_str());
		ageModernTexture = VI_createTexture(getFullPath("data/gfx/age_modern.jpg").c_str());
		executionImpalementTexture = VI_createTexture(getFullPath("data/gfx/execution_impalement.jpg").c_str());
		executionBeheadingTexture = VI_createTexture(getFullPath("data/gfx/execution_beheading.jpg").c_str());
		executionHangingTexture = VI_createTexture(getFullPath("data/gfx/execution_hanging.jpg").c_str());

		updateProgress(40);

		// initialise Lua constants

		lua_pushnumber(ls, biplane_travel_range_c);
		lua_setglobal(ls, "biplane_travel_range_c");
		lua_pushnumber(ls, bombers_travel_range_c);
		lua_setglobal(ls, "bombers_travel_range_c");
		lua_pushnumber(ls, jet_travel_range_c);
		lua_setglobal(ls, "jet_travel_range_c");
		lua_pushnumber(ls, road_move_scale_c);
		lua_setglobal(ls, "road_move_scale_c");
		lua_pushnumber(ls, max_road_bonus_c);
		lua_setglobal(ls, "max_road_bonus_c");

		lua_pushnumber(ls, static_cast<int>(Buildable::TYPE_IMPROVEMENT));
		lua_setglobal(ls, "BUILDABLETYPE_IMPROVEMENT");
		lua_pushnumber(ls, static_cast<int>(Buildable::TYPE_UNIT));
		lua_setglobal(ls, "BUILDABLETYPE_UNIT");

		lua_pushnumber(ls, static_cast<int>(UnitTemplate::NUCLEARTYPE_NONE));
		lua_setglobal(ls, "NUCLEARTYPE_NONE");
		lua_pushnumber(ls, static_cast<int>(UnitTemplate::NUCLEARTYPE_FISSION));
		lua_setglobal(ls, "NUCLEARTYPE_FISSION");
		lua_pushnumber(ls, static_cast<int>(UnitTemplate::NUCLEARTYPE_FUSION));
		lua_setglobal(ls, "NUCLEARTYPE_FUSION");

		lua_pushnumber(ls, static_cast<int>(UnitTemplate::SOUNDEFFECT_SWORDS));
		lua_setglobal(ls, "SOUNDEFFECT_SWORDS");
		lua_pushnumber(ls, static_cast<int>(UnitTemplate::SOUNDEFFECT_BREAK));
		lua_setglobal(ls, "SOUNDEFFECT_BREAK");
		lua_pushnumber(ls, static_cast<int>(UnitTemplate::SOUNDEFFECT_GUNSHOT));
		lua_setglobal(ls, "SOUNDEFFECT_GUNSHOT");

		lua_pushnumber(ls, static_cast<int>(AGE_ANCIENT));
		lua_setglobal(ls, "AGE_ANCIENT");
		lua_pushnumber(ls, static_cast<int>(AGE_MEDIEVAL));
		lua_setglobal(ls, "AGE_MEDIEVAL");
		lua_pushnumber(ls, static_cast<int>(AGE_INDUSTRIAL));
		lua_setglobal(ls, "AGE_INDUSTRIAL");
		lua_pushnumber(ls, static_cast<int>(AGE_MODERN));
		lua_setglobal(ls, "AGE_MODERN");

		// need to distinguish between Technology and Improvement AIHint enums!

		lua_pushnumber(ls, static_cast<int>(Improvement::AIHINT_NONE));
		lua_setglobal(ls, "IMPROVEMENT_AIHINT_NONE");
		lua_pushnumber(ls, static_cast<int>(Improvement::AIHINT_RESEARCH));
		lua_setglobal(ls, "IMPROVEMENT_AIHINT_RESEARCH");
		lua_pushnumber(ls, static_cast<int>(Improvement::AIHINT_FOW));
		lua_setglobal(ls, "IMPROVEMENT_AIHINT_FOW");

		lua_pushnumber(ls, static_cast<int>(Technology::AIHINT_NONE));
		lua_setglobal(ls, "TECHNOLOGY_AIHINT_NONE");
		lua_pushnumber(ls, static_cast<int>(Technology::AIHINT_REQUIRES_AIRPORT));
		lua_setglobal(ls, "TECHNOLOGY_AIHINT_REQUIRES_AIRPORT");

		lua_pushnumber(ls, static_cast<int>(TYPE_NONE));
		lua_setglobal(ls, "TYPE_NONE");
		lua_pushnumber(ls, static_cast<int>(TYPE_UNKNOWN));
		lua_setglobal(ls, "TYPE_UNKNOWN");
		lua_pushnumber(ls, static_cast<int>(TYPE_OCEAN));
		lua_setglobal(ls, "TYPE_OCEAN");
		lua_pushnumber(ls, static_cast<int>(TYPE_GRASSLAND));
		lua_setglobal(ls, "TYPE_GRASSLAND");
		lua_pushnumber(ls, static_cast<int>(TYPE_FOREST));
		lua_setglobal(ls, "TYPE_FOREST");
		lua_pushnumber(ls, static_cast<int>(TYPE_HILLS));
		lua_setglobal(ls, "TYPE_HILLS");
		lua_pushnumber(ls, static_cast<int>(TYPE_MOUNTAINS));
		lua_setglobal(ls, "TYPE_MOUNTAINS");
		lua_pushnumber(ls, static_cast<int>(TYPE_DESERT));
		lua_setglobal(ls, "TYPE_DESERT");
		lua_pushnumber(ls, static_cast<int>(TYPE_ARTIC));
		lua_setglobal(ls, "TYPE_ARTIC");

		lua_pushnumber(ls, static_cast<int>(ROAD_UNKNOWN));
		lua_setglobal(ls, "ROAD_UNKNOWN");
		lua_pushnumber(ls, static_cast<int>(ROAD_NONE));
		lua_setglobal(ls, "ROAD_NONE");
		lua_pushnumber(ls, static_cast<int>(ROAD_BASIC));
		lua_setglobal(ls, "ROAD_BASIC");
		lua_pushnumber(ls, static_cast<int>(ROAD_RAILWAYS));
		lua_setglobal(ls, "ROAD_RAILWAYS");

		lua_pushnumber(ls, static_cast<int>(AIInterface::UNITACTION_WAIT));
		lua_setglobal(ls, "UNITACTION_WAIT");
		lua_pushnumber(ls, static_cast<int>(AIInterface::UNITACTION_NOTHING));
		lua_setglobal(ls, "UNITACTION_NOTHING");
		lua_pushnumber(ls, static_cast<int>(AIInterface::UNITACTION_MOVE));
		lua_setglobal(ls, "UNITACTION_MOVE");
		lua_pushnumber(ls, static_cast<int>(AIInterface::UNITACTION_WORK));
		lua_setglobal(ls, "UNITACTION_WORK");
		lua_pushnumber(ls, static_cast<int>(AIInterface::UNITACTION_BUILD_CITY));
		lua_setglobal(ls, "UNITACTION_BUILD_CITY");

		lua_pushnumber(ls, static_cast<int>(AIInterface::GETCIVILIZATIONS_ALL));
		lua_setglobal(ls, "GETCIVILIZATIONS_ALL");
		lua_pushnumber(ls, static_cast<int>(AIInterface::GETCIVILIZATIONS_WAR));
		lua_setglobal(ls, "GETCIVILIZATIONS_WAR");
		lua_pushnumber(ls, static_cast<int>(AIInterface::GETCIVILIZATIONS_PEACE));
		lua_setglobal(ls, "GETCIVILIZATIONS_PEACE");

		lua_pushnumber(ls, static_cast<int>(AIInterface::GETBUILDABLES_ALL));
		lua_setglobal(ls, "GETBUILDABLES_ALL");
		lua_pushnumber(ls, static_cast<int>(AIInterface::GETBUILDABLES_IMPROVEMENTS));
		lua_setglobal(ls, "GETBUILDABLES_IMPROVEMENTS");
		lua_pushnumber(ls, static_cast<int>(AIInterface::GETBUILDABLES_UNITS));
		lua_setglobal(ls, "GETBUILDABLES_UNITS");

		// generate Technologies
		if( !loadScript(getFullPath("data/scripts/generate_technologies.lua").c_str()) ) {
			throw "lua script failed";
		}
		updateProgress(50);

		// generate Improvements, UnitTemplates
		if( !loadScript(getFullPath("data/scripts/generate_buildables.lua").c_str()) ) {
			throw "lua script failed";
		}
		updateProgress(60);

		// generate Races

		if( !loadScript(getFullPath("data/scripts/generate_races.lua").c_str()) ) {
			throw "lua script failed";
		}
		{
			// create Rebel Race
			unsigned char rgb[] = {0, 0,0};
			Race *race = new Race(rebel_race_name, "Rebel", "", rgb, NULL);
			race->setAlwaysHostile(true);
			race->setDummy(true);
			// constructor automatically adds race to the game engine
		}
		updateProgress(70);

		// generate bonus resources

		if( !loadScript(getFullPath("data/scripts/generate_bonuses.lua").c_str()) ) {
			throw "lua script failed";
		}
		updateProgress(80);

		// generate elements

		if( !loadScript(getFullPath("data/scripts/generate_elements.lua").c_str()) ) {
			throw "lua script failed";
		}
		updateProgress(90);

		// pre-load other scripts
		if( !loadScript(getFullPath("data/scripts/ai.lua").c_str()) ) {
			throw "lua script failed";
		}
		updateProgress(100);
	}
	catch(VisionException *ve) {
		VI_log("Failed - something went wrong loading game data!\n");
		this->setCursor(cursor_arrow);
#ifdef _WIN32
		MessageBoxA(NULL,"Failed to load Game data","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
		delete ve;
		return false;
	}
	catch(const char *error) {
		VI_log("Failed - something went wrong loading game data!\n");
		VI_log("Error: %s\n", error);
		this->setCursor(cursor_arrow);
#ifdef _WIN32
		MessageBoxA(NULL,"Failed to load Game data","Error",MB_OK|MB_ICONEXCLAMATION);
#endif
		this->genv->setPanel(NULL);
		delete gamePanel;
		return false;
	}

	VI_set_persistence_all(-1);

/*#ifdef _WIN32
	cursor = LoadCursor( NULL, IDC_ARROW );
	SetCursor(cursor);
	SetClassLong(genv->getHWND(), GCL_HCURSOR, (LONG)cursor);
#endif*/
	{
		char buffer[4096] = "";
		sprintf(buffer, "Conquests v%d.%d", versionMajor, versionMinor);
		game_g->getGraphicsEnvironment()->setWindowTitle(buffer);
	}
	this->setCursor(cursor_arrow);
	this->genv->setPanel(NULL);
	delete gamePanel;
	return true;
}

VI_Texture *Game::getAgeTexture(Age age) const {
	if( age == AGE_MEDIEVAL )
		return ageMedievalTexture;
	else if( age == AGE_INDUSTRIAL )
		return ageIndustrialTexture;
	else if( age == AGE_MODERN )
		return ageModernTexture;
	ASSERT(false);
	return NULL;
}

VI_Texture *Game::getExecutionTexture(Age age) const {
	if( age == AGE_ANCIENT ) {
		return this->executionImpalementTexture;
	}
	else if( age == AGE_MEDIEVAL ) {
		return ( rand() % 2 == 0 ) ? this->executionBeheadingTexture : this->executionHangingTexture;
	}
	return executionHangingTexture;
}

void GameData::clear() {
	for(vector<const Race *>::const_iterator iter = races.begin(); iter != races.end(); ++iter) {
		const Race *race = *iter;
		delete race;
	}
	races.clear();

	for(vector<Buildable *>::iterator iter = buildables.begin();iter != buildables.end(); ++iter) {
		Buildable *buildable = *iter;
		delete buildable;
	}
	buildables.clear();

	for(vector<Technology *>::const_iterator iter = technologies.begin();iter != technologies.end(); ++iter) {
		const Technology *technology = *iter;
		delete technology;
	}
	technologies.clear();
}

void Game::clear() {
	gameData.clear();
	if( ls != NULL ) {
		lua_close(ls);
		ls = NULL;
	}
	VI_cleanup();
	delete [] savegames_dirname;
	savegames_dirname = NULL;
	delete [] usermaps_dirname;
	usermaps_dirname = NULL;
}

/*void printTree(const VI_XMLTreeNode *node) {
	VI_log("node: %s\n", node->getName().c_str());
	if( node->hasData() ) {
		VI_log("data: %s\n", node->getData().c_str());
	}
	if( node->getNChildren() > 0 ) {
		VI_log("### CHILDREN\n");
		for(int i=0;i<node->getNChildren();i++) {
			VI_log("child %d:\n", i);
			printTree(node->getChild(i));
		}
		VI_log("### END CHILDREN\n");
	}
}*/

void Game::run() {
	if( !init() ) {
		VI_log("failed to initialise!\n");
		clear();
		return;
	}

	/*gamestate = ::game = new MainGamestate();
	::game->run();
	delete ::game;
	::game = NULL;*/
	/*MainGamestate *mainGamestate = new MainGamestate();
	gamestate = mainGamestate;
	mainGamestate->run();
	delete mainGamestate;
	gamestate = NULL;*/

	/*MainGamestate *mainGamestate = new MainGamestate();
	gamestate = mainGamestate;
	//this->setupNewGame(mainGamestate);
	mainGamestate->initGameData(true, 100, 100, TOPOLOGY_CYLINDER, this->findRace("The Romans"), 4);*/

	gamestate = new OptionsGamestate();
	//gamestate->run();

	if( !gamestate->start() ) {
		VI_log("failed to start initial gamestate!\n");
		delete gamestate;
		gamestate = NULL;
		clear();
		return;
	}

	bool timedemo = false;
	//timedemo = true;
	int last_update_time = genv->getRealTimeMS();
	int t1 = genv->getRealTimeMS();
	const int n_frames = 500;
	//bool hack = false;

	// program main loop
	while( !quit ) {
	if( game_g->getGraphicsEnvironment()->shouldQuit() ) {
			VI_log("should quit\n");
			game_g->getGraphicsEnvironment()->clearQuitRequest();
			gamestate->quitGame();
		}
		else if( game_g->getGraphicsEnvironment()->isActive() ) {
			//ASSERT( _heapchk() == _HEAPOK );
			game_g->getGraphicsEnvironment()->drawFrame();
			//ASSERT( _heapchk() == _HEAPOK );
			game_g->getGraphicsEnvironment()->swapBuffers();
			frames++;
			if( timedemo && frames >= n_frames )
				quit = true;
		}
		else {
			// we don't call VI_GraphicsEnvironment::waitForEvents(), as we still want to process even when not active (see below)
			// however, we want to avoid consuming too much CPU when not active, so sleep for a bit
			//Sleep(10); // don't consume 100% CPU
			game_g->getGraphicsEnvironment()->sleep(10);
		}

		// we update even if not active, so that the game still continues processing (useful for between player turns)
		int time_diff = genv->getRealTimeMS() - last_update_time;
		if( time_diff > 0 ) {
			last_update_time = genv->getRealTimeMS();
			//VI_log("update %d\n", time_diff);
			VI_update(time_diff);
			gamestate->update();
			if( game_g->getGraphicsEnvironment()->isActive() )
			{
				game_g->getGraphicsEnvironment()->handleInput(); // must be done last due to resetting input
			}
		}
		game_g->getGraphicsEnvironment()->processEvents();

		if( this->newGamestate != NULL ) {
			VI_log("switch to new gamestate\n");
			// stop the current one
			gamestate->stop();
			delete gamestate;
			// assign the new one
			gamestate = newGamestate;
			newGamestate = NULL;
			// start the new one
			if( !gamestate->start() ) {
				VI_log("failed to start the new gamestate!\n");
				delete gamestate;
				gamestate = NULL;
				this->clear();
				return;
			}
		}
	}

	int t2 = genv->getRealTimeMS();

	gamestate->stop();

	game_g->setCursor(cursor_wait);
	delete gamestate;
	gamestate = NULL;

	clear();

	//ShowCursor(TRUE);
#ifdef _DEBUG
#ifdef _WIN32
	char buffer[256] = "";
	sprintf_s(buffer,"%d frames in time %d\nFPS : %f",frames,t2-t1,(frames*1000)/((float)(t2-t1)));
	MessageBoxA(NULL,buffer,"Finished",NULL);
#endif
#endif
}

void Game::runTests(TestID id) {
	if( !init() ) {
		clear();
		return;
	}

	/*gamestate = ::game = new MainGamestate();
	::game->runTests(id);
	delete ::game;
	::game = NULL;*/
	MainGamestate *mainGamestate = new MainGamestate();
	gamestate = mainGamestate;
	mainGamestate->runTests(id);
	delete mainGamestate;
	gamestate = NULL;

	clear();
}

void Game::generateDocs() {
	if( !init() ) {
		clear();
		return;
	}

	char filename[] = "reference.html";
	FILE *file = fopen(filename, "w+");
	ASSERT(file != NULL);

	fprintf(file, "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n");
	fprintf(file, "<html>\n");
	fprintf(file, "<head>\n");
	fprintf(file, "<title>Conquests Reference\n");
	fprintf(file, "</title>\n");
	fprintf(file, "<meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1252\">\n");
	fprintf(file, "<style type=\"text/css\">\n");
	fprintf(file, "    body { background: url(\"conquests/data/gfx/marble.jpg\") }\n");
	fprintf(file, "</style>\n");
	fprintf(file, "</head>\n");
	fprintf(file, "\n");
	fprintf(file, "<body>\n");
	fprintf(file, "\n");
	fprintf(file, "<h2>Conquests Reference</h2>\n");
	fprintf(file, "<p><i>v%d.%d</i></p>\n\n", versionMajor, versionMinor);
	fprintf(file, "\n");

	fprintf(file, "<ul>\n");
	fprintf(file, "<li><a href=\"#technologies\">Technologies</a></li>\n");
	fprintf(file, "<li><a href=\"#units\">Units</a></li>\n");
	fprintf(file, "<li><a href=\"#improvements\">Improvements</a></li>\n");
	fprintf(file, "</ul>\n");

	fprintf(file, "<hr>");

	fprintf(file, "<p><a name=\"technologies\"><h3><b>Technologies</b></h3></p>\n\n");
	fprintf(file, "<hr>");
	fprintf(file, "<p>");
	for(size_t i=0;i<this->getGameData()->getNTechnologies();i++) {
		const Technology *technology = this->getGameData()->getTechnology(i);
		fprintf(file, "<a name=\"%s\">%s\n", technology->getName(), technology->getHelp(true).c_str());
		fprintf(file, "<hr>");
	}
	fprintf(file, "</p>");

	fprintf(file, "<p><a name=\"units\"><h3><b>Units</b></h3></p>\n\n");
	fprintf(file, "<hr>");
	fprintf(file, "<p>");
	for(size_t i=0;i<this->getGameData()->getNUnitTemplates();i++) {
		const UnitTemplate *unit_template = this->getGameData()->getUnitTemplate(i);
		fprintf(file, "<a name=\"%s\">%s\n", unit_template->getName(), unit_template->getHelp(true).c_str());
		fprintf(file, "<hr>");
	}
	fprintf(file, "</p>");

	fprintf(file, "<p><a name=\"improvements\"><h3><b>Improvements</b></h3></p>\n\n");
	fprintf(file, "<hr>");
	fprintf(file, "<p>");
	for(size_t i=0;i<this->getGameData()->getNImprovements();i++) {
		const Improvement *improvement = this->getGameData()->getImprovement(i);
		fprintf(file, "<a name=\"%s\">%s\n", improvement->getName(), improvement->getHelp(true).c_str());
		fprintf(file, "<hr>");
	}
	fprintf(file, "</p>");

	fprintf(file, "\n");
	fprintf(file, "</body>\n");
	fprintf(file, "</html>\n");

	fclose(file);

	clear();
}

void Game::mapWriteHeader(FILE *file, int width, int height) {
	fprintf(file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	fprintf(file, "<map version=\"1.0\" orientation=\"orthogonal\" width=\"%d\" height=\"%d\" tilewidth=\"64\" tileheight=\"64\">", width, height);
	fprintf(file, "<tileset firstgid=\"1\" source=\"Conquests Tiles.tsx\"/>");
	fprintf(file, "<layer name=\"Tile Layer 1\" width=\"%d\" height=\"%d\">", width, height);
	fprintf(file, "<data encoding=\"csv\">\n");
}

void Game::mapWriteFooter(FILE *file) {
	fprintf(file, "</data></layer></map>\n");
}

void Game::bitmapToXML(const char *filename, const char *out_filename) {
	VI_Image *image = NULL;
	try {
		image = VI_Image::load(filename);
	}
	catch(VisionException *ve) {
		printf("Unable to load image\n");
		delete ve;
		return;
	}

	FILE *file = fopen(out_filename, "w");
	if( file == NULL ) {
		printf("Unable to open file\n");
		delete image;
		return;
	}

	/*fprintf(file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	fprintf(file, "<map version=\"1.0\" orientation=\"orthogonal\" width=\"%d\" height=\"%d\" tilewidth=\"64\" tileheight=\"64\">", image->getWidth(), image->getHeight());
	fprintf(file, "<tileset firstgid=\"1\" source=\"Conquests Tiles.tsx\"/>");
	fprintf(file, "<layer name=\"Tile Layer 1\" width=\"%d\" height=\"%d\">", image->getWidth(), image->getHeight());
	fprintf(file, "<data encoding=\"csv\">\n");*/
	mapWriteHeader(file, image->getWidth(), image->getHeight());
	for(int y=0;y<image->getHeight();y++) {
		for(int x=0;x<image->getWidth();x++) {
			unsigned char r = 0, g = 0, b = 0;
			image->getRGB(&r, &g, &b, x, image->getHeight()-y-1); // need to flip Y!
			int terrain = 0;
			if( r == 0 && g == 0 && b == 255 ) {
				terrain = 1; // ocean
			}
			else if( r == 0 && g == 127 && b == 0 ) {
				terrain = 2; // grassland
			}
			else if( r == 0 && g == 63 && b == 0 ) {
				terrain = 3; // forest
			}
			else if( r == 0 && g == 255 && b == 0 ) {
				terrain = 4; // hills
			}
			else if( r == 127 && g == 127 && b == 127 ) {
				terrain = 5; // mountains
			}
			else if( r == 255 && g == 255 && b == 0 ) {
				terrain = 6; // desert
			}
			else if( r == 255 && g == 255 && b == 255 ) {
				terrain = 7; // artic
			}
			else {
				printf("unrecognised colour at %d, %d: %d, %d, %d\n", x, y, r, g, b);
				fclose(file);
				delete image;
				return;
			}
			if( y == image->getHeight()-1 && x == image->getWidth()-1 ) {
				// last entry has no comma
				fprintf(file, "%d", terrain);
			}
			else {
				fprintf(file, "%d,", terrain);
			}
		}
		fprintf(file, "\n");
	}
	//fprintf(file, "</data></layer></map>\n");
	mapWriteFooter(file);

	fclose(file);
	delete image;
}

/*void MainGamestate::doGame() {
	::game = new MainGamestate();
	::game->run();
	delete ::game;
}

void MainGamestate::doTests(TestID id) {
	::game = new MainGamestate();
	::game->runTests(id);
	delete ::game;
}*/

void Game::doGame() {
	::game_g = new Game();
	::game_g->run();
	delete ::game_g;
	::game_g = NULL;
}

void Game::doTests(TestID id, bool testmode_silent) {
	::game_g = new Game();
	game_g->testmode_silent = testmode_silent;
	::game_g->runTests(id);
	delete ::game_g;
	::game_g = NULL;
}

void Game::doDocs() {
	::game_g = new Game();
	::game_g->generateDocs();
	delete ::game_g;
	::game_g = NULL;
}

void Game::doBitmapToXML(const char *filename, const char *out_filename) {
	::game_g = new Game();
	::game_g->bitmapToXML(filename, out_filename);
	delete ::game_g;
	::game_g = NULL;
}
