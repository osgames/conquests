#include "conquests_stdafx.h"

#include "civilization.h"
#include "city.h"
#include "buildable.h"
#include "technology.h"
#include "unit.h"
#include "map.h"
#include "maingamestate.h"
#include "infowindow.h"

#include "../Vision/VisionIface.h"

#include <cstring> // needed for Linux at least

#include <sstream>
using std::stringstream;

using std::pair;

//const bool def_visible_c = false;
//const bool def_visible_c = true;
//const bool reveal_player_map = false;
//const bool reveal_player_map = true;

const int power_initial_bonus_c = 10;
const int power_bonus_multiplier_c = 10;

Race::Race(const char *name, const char *name_adj, const char *name_leader, unsigned char rgb[3], const char *image_filename) :
name(name), name_adj(name_adj), name_leader(name_leader), texture(NULL), starting_technology(NULL), always_hostile(false), dummy(false)
{
	for(int i=0;i<3;i++)
		this->rgb[i] = rgb[i];
	if( image_filename == NULL ) {
		texture = NULL;
	}
	else {
		string full_image_filename = getFullPath(image_filename);
		texture = VI_createTexture(full_image_filename.c_str());
	}
	game_g->addRace(this);
}

Race::~Race() {
	delete texture;
}

string Race::getHelp(bool html) const {
	stringstream text;
	string nl = html ? "<br>\n" : "\n";
	string nbsp = html ? "&nbsp;" : " ";
	string bold = html ? "<b>" : "";
	string bold_end = html ? "</b>" : "";

	text << bold << this->getName() << bold_end << nl;
	text << "Led by " << this->getNameLeader() << "." << nl;
	text << nl;
	if( this->starting_technology != NULL ) {
		text << "Starting technology:" << nl;
		text << nbsp << nbsp << nbsp << nbsp << starting_technology->getName() << nl;
	}
	bool any_specific = false;
	for(size_t i=0;i<game_g->getGameData()->getNBuildables();i++) {
		const Buildable *buildable = game_g->getGameData()->getBuildable(i);
		if( buildable->getRaceSpecific() == this ) {
			if( !any_specific ) {
				text << this->name << " can build:" << nl;
				any_specific = true;
			}
			text << nbsp << nbsp << nbsp << nbsp << buildable->getName() << nl;
		}
	}
	text << nl;
	text << info;

	return text.str();
}

string Race::makeCityName(int *looped, size_t *index) const {
	string city_name;
	ASSERT( *index >= 0 && *index < city_names.size() );
	for(int i=0;i<*looped;i++) {
		city_name += "New ";
	}
	city_name += city_names.at(*index);
	return city_name;
}

void Race::advanceCityName(int *looped, size_t *index) const {
	(*index)++;
	if( *index == city_names.size() ) {
		(*looped)++;
		*index = 0;
	}
}

Relationship::Relationship(MainGamestate *mainGamestate, Civilization *civ1, Civilization *civ2) : mainGamestate(mainGamestate), civ1(civ1), civ2(civ2), made_contact(false), status(STATUS_PEACE), has_right_of_passage(false), year_declared_war(-1), year_civ1_allows_rop(-1), year_civ2_allows_rop(-1) {
	if( civ1->getRace()->isAlwaysHostile() || civ2->getRace()->isAlwaysHostile() ) {
		this->setStatus(STATUS_WAR);
	}
}

Relationship::Relationship(MainGamestate *mainGamestate) : mainGamestate(mainGamestate), civ1(NULL), civ2(NULL), made_contact(false), status(STATUS_PEACE), has_right_of_passage(false), year_declared_war(-1), year_civ1_allows_rop(-1), year_civ2_allows_rop(-1) {
	// create dummy Relationship, used by Relationship::load()
}

Relationship *Relationship::load(MainGamestate *mainGamestate, FILE *file) {
	const int max_line_c = 4095;
	char line[max_line_c+1] = "";
	char word[max_line_c+1] = "";
	bool found_end = false;
	bool ok = true;
	const char field_civ1_c[] = "civ1=";
	const char field_civ2_c[] = "civ2=";
	const char field_made_contact_c[] = "made_contact=";
	const char field_status_c[] = "status=";
	const char field_has_right_of_passage_c[] = "has_right_of_passage=";
	const char field_year_c[] = "year=";
	const char field_year_civ1_allows_rop_c[] = "year_civ1_allows_rop=";
	const char field_year_civ2_allows_rop_c[] = "year_civ2_allows_rop=";

	Relationship *relationship = new Relationship(mainGamestate);

	while( ok && fgets(line, max_line_c, file) != NULL ) {
		if( matchFileLine(line, field_civ1_c) ) {
			parseFileLine(word, line, field_civ1_c);
			bool found = false;
			for(size_t i=0;i<mainGamestate->getNCivilizations() && !found;i++) {
				Civilization *civ = mainGamestate->getCivilization(i);
				if( strcmp(civ->getName(), word) == 0 ) {
					found = true;
					relationship->civ1 = civ;
				}
			}
			if( !found ) {
				VI_log("Relationship::load: can't find civilization1: %s\n", word);
				ok = false;
			}
		}
		else if( matchFileLine(line, field_civ2_c) ) {
			parseFileLine(word, line, field_civ2_c);
			bool found = false;
			for(size_t i=0;i<mainGamestate->getNCivilizations() && !found;i++) {
				Civilization *civ = mainGamestate->getCivilization(i);
				if( strcmp(civ->getName(), word) == 0 ) {
					found = true;
					relationship->civ2 = civ;
				}
			}
			if( !found ) {
				VI_log("Relationship::load: can't find civilization2: %s\n", word);
				ok = false;
			}
		}
		else if( matchFileLine(line, field_made_contact_c) ) {
			parseFileLine(word, line, field_made_contact_c);
			relationship->made_contact = parseBool(&ok, word);
			if( !ok ) {
				VI_log("Relationship::load: failed to parse made_contact: %s\n", word);
			}
		}
		else if( matchFileLine(line, field_status_c) ) {
			parseFileLine(word, line, field_status_c);
			relationship->status = static_cast<Relationship::RelationshipStatus>(atoi(word));
		}
		else if( matchFileLine(line, field_has_right_of_passage_c) ) {
			parseFileLine(word, line, field_has_right_of_passage_c);
			relationship->has_right_of_passage = parseBool(&ok, word);
			if( !ok ) {
				VI_log("Relationship::load: failed to parse has_right_of_passage: %s\n", word);
			}
		}
		else if( matchFileLine(line, field_year_c) ) {
			parseFileLine(word, line, field_year_c);
			relationship->year_declared_war = atoi(word);
		}
		else if( matchFileLine(line, field_year_civ1_allows_rop_c) ) {
			parseFileLine(word, line, field_year_civ1_allows_rop_c);
			relationship->year_civ1_allows_rop = atoi(word);
		}
		else if( matchFileLine(line, field_year_civ2_allows_rop_c) ) {
			parseFileLine(word, line, field_year_civ2_allows_rop_c);
			relationship->year_civ2_allows_rop = atoi(word);
		}
		//else if( strcmp(line, "[/relationship]\n") == 0 ) {
		else if( matchFileLine(line, "[/relationship]") ) {
			found_end = true;
			break;
		}
		else if( line[0] == '[' ) {
			VI_log("Relationship::load: Unexpected '[' start of new section!\n");
			ok = false;
			break;
		}
	}
	if( !found_end ) {
		VI_log("Relationship::load: didn't find end section\n");
		ok = false;
	}
	// TODO: check all fields have been set

	if( !ok ) {
		delete relationship;
		relationship = NULL;
	}

	/*if( ok && CHEAT ) {
		if( relationship->civ1 == mainGamestate->getPlayer() || relationship->civ2 == mainGamestate->getPlayer() ) {
			relationship->makeContact();
		}
	}*/

	return relationship;
}

void Relationship::save(FILE *file) const {
	//VI_log("Relationship::save\n");
	fprintf(file, "[relationship]\n");
	fprintf(file, "civ1=%s\n", this->civ1->getName());
	fprintf(file, "civ2=%s\n", this->civ2->getName());
	fprintf(file, "made_contact=%d\n", made_contact?1:0);
	fprintf(file, "status=%d\n", status);
	fprintf(file, "has_right_of_passage=%d\n", has_right_of_passage?1:0);
	fprintf(file, "year=%d\n", year_declared_war);
	fprintf(file, "year_civ1_allows_rop=%d\n", year_civ1_allows_rop);
	fprintf(file, "year_civ2_allows_rop=%d\n", year_civ2_allows_rop);
	fprintf(file, "[/relationship]\n");
}

void Relationship::makeContact() {
	VI_log("%s make contact with %s\n", this->civ1->getName(), this->civ2->getName());
	this->made_contact = true;
}

void Relationship::setStatus(RelationshipStatus status) {
	this->made_contact = true; // just in case not set (this may happen if say declaring war due to travelling by sea, with a civilization not previously made contact with, or for always hostile civs)
	this->status = status;
	if( status == STATUS_WAR ) {
		this->year_declared_war = mainGamestate->getYear();
		this->has_right_of_passage = false; // also breaks any right of passage agreement
	}
	else {
		this->year_declared_war = -1;
		this->year_civ1_allows_rop = mainGamestate->getYear() + 3 + ( rand() % 6 );
		this->year_civ2_allows_rop = mainGamestate->getYear() + 3 + ( rand() % 6 );
		T_ASSERT( !civ1->getRace()->isAlwaysHostile() && !civ2->getRace()->isAlwaysHostile() );
	}
}

bool Relationship::canMakeRightOfPassage() const {
	//const Technology *technology = game_g->findTechnology("Writing");
	//if( civ1->hasTechnology(technology) && civ2->hasTechnology(technology) ) {
	if( civ1->hasTechnology("Writing") && civ2->hasTechnology("Writing") ) {
		return true;
	}
	return false;
}

void Relationship::setRightOfPassage(bool has_right_of_passage) {
	ASSERT( made_contact );
	ASSERT( status == STATUS_PEACE );
	this->has_right_of_passage = has_right_of_passage;
	if( this->has_right_of_passage ) {
		ASSERT( this->canMakeRightOfPassage() );
	}
}

int Relationship::getYearDeclaredWar() const {
	ASSERT( status == STATUS_WAR );
	return this->year_declared_war;
}

int Relationship::getYearConsiderROP(const Civilization *civ) const {
	ASSERT( status == STATUS_PEACE );
	ASSERT( civ == civ1 || civ == civ2 );
	return civ == civ1 ? this->year_civ1_allows_rop : this->year_civ2_allows_rop;
}

int History::getScore(HistoryType history_type) const {
	if( history_type == HISTORYTYPE_POWER ) {
		return power;
	}
	else if( history_type == HISTORYTYPE_POPULATION ) {
		return population;
	}
	else if( history_type == HISTORYTYPE_PRODUCTION ) {
		return production;
	}
	else if( history_type == HISTORYTYPE_SCIENCE ) {
		return science;
	}
	else if( history_type == HISTORYTYPE_TERRITORY ) {
		return territory;
	}
	else if( history_type == HISTORYTYPE_MILITARYPOWER ) {
		return militarypower;
	}
	ASSERT(false);
	return 0;
}

Civilization::Civilization(MainGamestate *mainGamestate, const Race *race) :
mainGamestate(mainGamestate), race(race), progress_technology(0), technology(NULL),
power(0), city_name_looped(0), city_name_index(0)/*, is_dead(false)*/,
map_width(0), map_height(0), map_visible(NULL), fog_of_war(NULL),
done_ai(false), has_calculated_fow(false),
production_bonus(0), defence_bonus(0), auto_veteran_bonus(false), research_multiplier(0)
{
	VI_log("new Civilization: %s\n", race->getName());
	ASSERT( race != NULL );
	mainGamestate->addCivilization(this);

	ASSERT(mainGamestate->getMap() != NULL);
	this->map_width = mainGamestate->getMap()->getWidth();
	this->map_height = mainGamestate->getMap()->getHeight();
	this->map_visible = new bool[mainGamestate->getMap()->getWidth()*mainGamestate->getMap()->getHeight()];
	this->fog_of_war = new bool[mainGamestate->getMap()->getWidth()*mainGamestate->getMap()->getHeight()];
	for(int i=0;i<mainGamestate->getMap()->getWidth()*mainGamestate->getMap()->getHeight();i++) {
		this->map_visible[i] = false;
		this->fog_of_war[i] = false;
	}

	for(int i=0;i<N_BONUSES;i++) {
		this->bonus_turns[i] = 0;
	}

	if( race->getStartingTechnology() != NULL ) {
		this->addTechnology( race->getStartingTechnology() );
	}

	// make Relationships with other players
	// DONE after creating all Civilizations
	for(size_t i=0;i<mainGamestate->getNCivilizations();i++) {
		Civilization *civ = mainGamestate->getCivilization(i);
		if( civ != this ) {
			Relationship *relationship = new Relationship(mainGamestate, this, civ);
			mainGamestate->addRelationship(relationship);
		}
	}
}

Civilization::Civilization(MainGamestate *mainGamestate) :
mainGamestate(mainGamestate), race(NULL), progress_technology(0), technology(NULL),
power(0), city_name_looped(0), city_name_index(0)/*, is_dead(false)*/,
map_width(0), map_height(0), map_visible(NULL), fog_of_war(NULL),
done_ai(false), has_calculated_fow(false),
production_bonus(0), defence_bonus(0), auto_veteran_bonus(false), research_multiplier(0)
{
	// create dummy Civilization, used by Civilization::load()

	ASSERT(mainGamestate->getMap() != NULL);
	this->map_width = mainGamestate->getMap()->getWidth();
	this->map_height = mainGamestate->getMap()->getHeight();
	this->map_visible = new bool[mainGamestate->getMap()->getWidth()*mainGamestate->getMap()->getHeight()];
	this->fog_of_war = new bool[mainGamestate->getMap()->getWidth()*mainGamestate->getMap()->getHeight()];
	for(int i=0;i<mainGamestate->getMap()->getWidth()*mainGamestate->getMap()->getHeight();i++) {
		this->map_visible[i] = false;
		this->fog_of_war[i] = false;
	}
	for(int i=0;i<N_BONUSES;i++) {
		this->bonus_turns[i] = 0;
	}

	// no need to create Relationships, as these are loaded from file too
}

Civilization::~Civilization() {
	VI_log("Civilization::~Civilization : %s\n", race == NULL ? "UNKNOWN" : this->getName());
	// copy to a temp list, to avoid issues with code called from City destructor still thinking the Civilization has cities that are already deleted!
	vector<City *> temp_cities(cities);
	cities.clear();
	//for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end(); ++iter) {
	for(vector<City *>::const_iterator iter = temp_cities.begin(); iter != temp_cities.end(); ++iter) {
		City *city = *iter;
		city->detachCivilization(); // need to avoid city being removed from the list
		delete city;
	}
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		Unit *unit = *iter;
		unit->setCivilization(NULL); // need to avoid unit being removed from the list
		delete unit;
	}
	delete [] this->map_visible;
	delete [] this->fog_of_war;
	// Should be unlinked from MainGamestate manually.
}

void Civilization::checkDead() {
	//ASSERT( !is_dead );
	if( !mainGamestate->isTestMode() && this->cities.size() == 0 && this->units.size() == 0 && !this->getRace()->isDummy() ) {
		//this->is_dead = true;
		stringstream text;
		text << this->getName() << " have been wiped out!";
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
		window->doModal();
		delete window;
		if( this != mainGamestate->getPlayer() ) {
			bool all_dead = true;
			for(size_t i=0;i<mainGamestate->getNCivilizations() && all_dead;i++) {
				const Civilization *civ = mainGamestate->getCivilization(i);
				if( civ != mainGamestate->getPlayer() && !civ->isDead() && !civ->getRace()->isDummy() ) {
					all_dead = false;
				}
			}
			if( all_dead ) {
				stringstream text;
				text << "You have conquered the entire world!";
				InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
				window->doModal();
				delete window;
				mainGamestate->setGameNTurns(0); // no point having a game end now
			}
		}
	}
}

bool Civilization::isDead() const {
	// Note that for the rebel civ, it will become dead or not-dead depending on whether any rebel units currently exist anywhere in the world.
	if( this->cities.size() == 0 && this->units.size() == 0 ) {
		return true;
	}
	return false;
}

Civilization *Civilization::load(MainGamestate *mainGamestate, FILE *file) {
	VI_log("Civilization::load()\n");
	const int max_line_c = 4095;
	char line[max_line_c+1] = "";
	char word[max_line_c+1] = "";
	bool found_end = false;
	bool ok = true;
	const char field_name_c[] = "name=";
	const char field_progress_technology_c[] = "progress_technology=";
	const char field_technology_c[] = "technology=";
	const char field_technologies_c[] = "technologies=";
	const char field_power_c[] = "power=";
	const char field_bonus_c[] = "bonus=";
	const char field_city_name_looped_c[] = "city_name_looped=";
	const char field_city_name_index_c[] = "city_name_index=";
	const char field_history_year_c[] = "year=";
	const char field_history_power_c[] = "power=";
	const char field_history_population_c[] = "population=";
	const char field_history_production_c[] = "production=";
	const char field_history_science_c[] = "science=";
	const char field_history_territory_c[] = "territory=";
	const char field_history_militarypower_c[] = "militarypower=";

	Civilization *civilization = new Civilization(mainGamestate);

	while( ok && fgets(line, max_line_c, file) != NULL ) {
		if( matchFileLine(line, field_name_c) ) {
			parseFileLine(word, line, field_name_c);
			civilization->race = game_g->findRace(word);
			if( civilization->race == NULL ) {
				VI_log("Civilization::load: can't find race: %s\n", word);
				ok = false;
			}
		}
		//else if( strcmp(line, "[/civilization]\n") == 0 ) {
		else if( matchFileLine(line, "[/civilization]") ) {
			found_end = true;
			break;
		}
		else if( matchFileLine(line, "map_visible=") ) {
			for(int y=0;y<mainGamestate->getMap()->getHeight() && ok;y++ ) {
				if( fgets(line, max_line_c, file) == NULL ) {
					VI_log("Civilization::load unexpected eof when reading map visible\n");
					ok = false;
					break;
				}
				/*civilization->map_width = mainGamestate->getMap()->getWidth();
				civilization->map_height = mainGamestate->getMap()->getHeight();*/
				for(int x=0;x<mainGamestate->getMap()->getWidth() && ok;x++ ) {
					if( line[x] == 'y' ) {
						civilization->setMapVisible(x, y, true);
					}
					else if( line[x] == 'n' ) {
						civilization->setMapVisible(x, y, false);
					}
					else {
						VI_log("Civilization::load unrecognised map visible symbol\n");
						ok = false;
					}
					//civilization->setMapVisible(x, y, true); // uncomment to reveal map for all civs
				}
			}
		}
		else if( matchFileLine(line, "fog_of_war=") ) {
			for(int y=0;y<mainGamestate->getMap()->getHeight() && ok;y++ ) {
				if( fgets(line, max_line_c, file) == NULL ) {
					VI_log("Civilization::load unexpected eof when reading fog of war\n");
					ok = false;
					break;
				}
				for(int x=0;x<mainGamestate->getMap()->getWidth() && ok;x++ ) {
					if( line[x] == 'y' ) {
						if( !civilization->isExplored(x, y) ) {
							VI_log("Civilization::load unexpected fog of war visible where map not explored\n");
							ok = false;
						}
						else {
							civilization->setFogOfWar(x, y, true);
						}
					}
					else if( line[x] == 'n' ) {
						civilization->setFogOfWar(x, y, false);
					}
					else {
						VI_log("Civilization::load unrecognised fog of war symbol\n");
						ok = false;
					}
				}
			}
		}
		//else if( strcmp(line, "[city]\n") == 0 ) {
		else if( matchFileLine(line, "[city]") ) {
			City *city = City::load(mainGamestate, civilization, file);
			if( city == NULL ) {
				VI_log("Civilization::load failed to load city\n");
				ok = false;
			}
		}
		//else if( strcmp(line, "[unit]\n") == 0 ) {
		else if( matchFileLine(line, "[unit]") ) {
			Unit *unit = Unit::load(mainGamestate, civilization, file);
			if( unit == NULL ) {
				VI_log("Civilization::load failed to load unit\n");
				ok = false;
			}
		}
		else if( matchFileLine(line, field_progress_technology_c) ) {
			parseFileLine(word, line, field_progress_technology_c);
			civilization->progress_technology = atoi(word);
		}
		else if( matchFileLine(line, field_technology_c) ) {
			parseFileLine(word, line, field_technology_c);
			if( strcmp(word, "null") == 0 ) {
				civilization->technology = NULL;
			}
			else {
				civilization->technology = game_g->findTechnology(word);
				if( civilization->technology == NULL ) {
					VI_log("Civilization::load: can't find technology: %s\n", word);
					ok = false;
				}
			}
		}
		else if( matchFileLine(line, field_technologies_c) ) {
			parseFileLine(word, line, field_technologies_c);
			const Technology *technology = game_g->findTechnology(word);
			if( technology == NULL ) {
				VI_log("Civilization::load: can't find technologies: %s\n", word);
				ok = false;
			}
			else {
				civilization->technologies.push_back(technology); // don't copy, just pass a pointer
			}
		}
		else if( matchFileLine(line, field_power_c) ) {
			parseFileLine(word, line, field_power_c);
			civilization->power = atoi(word);
		}
		else if( matchFileLine(line, field_bonus_c) ) {
			parseFileLine(word, line, field_bonus_c);
			int index = atoi(word);
			if( index < 0 || index >= N_BONUSES ) {
				VI_log("Civilization::load: invalid bonus index: %d\n", index);
				ok = false;
			}
			else {
				// special case, read from the next line
				if( fgets(line, max_line_c, file) == NULL ) {
					VI_log("Civilization::load: unexpected eof after bonus %d\n", index);
					ok = false;
				}
				else {
					civilization->bonus_turns[index] = atoi(line);
				}
			}
		}
		else if( matchFileLine(line, field_city_name_looped_c) ) {
			parseFileLine(word, line, field_city_name_looped_c);
			civilization->city_name_looped = atoi(word);
		}
		else if( matchFileLine(line, field_city_name_index_c) ) {
			parseFileLine(word, line, field_city_name_index_c);
			civilization->city_name_index = atoi(word);
		}
		else if( matchFileLine(line, "history=") ) {
			int year = -1;
			History entry;
			while( ok ) {
				if( fgets(line, max_line_c, file) == NULL ) {
					VI_log("Civilization::load unexpected eof when reading history\n");
					ok = false;
				}
				else if( matchFileLine(line, "endhistory") ) {
					break;
				}
				else if( matchFileLine(line, field_history_year_c) ) {
					if( year != -1 ) {
						civilization->history.insert(pair<int, History>(year, entry));
					}
					parseFileLine(word, line, field_history_year_c);
					year = atoi(word);
				}
				else if( matchFileLine(line, field_history_power_c) ) {
					parseFileLine(word, line, field_history_power_c);
					entry.power = atoi(word);
				}
				else if( matchFileLine(line, field_history_population_c) ) {
					parseFileLine(word, line, field_history_population_c);
					entry.population = atoi(word);
				}
				else if( matchFileLine(line, field_history_production_c) ) {
					parseFileLine(word, line, field_history_production_c);
					entry.production = atoi(word);
				}
				else if( matchFileLine(line, field_history_science_c) ) {
					parseFileLine(word, line, field_history_science_c);
					entry.science = atoi(word);
				}
				else if( matchFileLine(line, field_history_territory_c) ) {
					parseFileLine(word, line, field_history_territory_c);
					entry.territory = atoi(word);
				}
				else if( matchFileLine(line, field_history_militarypower_c) ) {
					parseFileLine(word, line, field_history_militarypower_c);
					entry.militarypower = atoi(word);
				}
				else {
					VI_log("Civilization::load unexpected line when reading history\n");
					ok = false;
				}
			}
			if( ok && year != -1 ) {
				civilization->history.insert(pair<int, History>(year, entry));
			}
		}
		else if( line[0] == '[' ) {
			VI_log("Civilization::load: Unexpected '[' start of new section!\n");
			ok = false;
			break;
		}
	}
	if( ok && !found_end ) {
		VI_log("Civilization::load: didn't find end section\n");
		ok = false;
	}
	// TODO: check all fields have been set

	if( ok ) {
		// shouldn't be needed, but a safeguard to repair corrupt save game files!
		/*for(vector<City *>::const_iterator iter = civilization->cities.begin(); iter != civilization->cities.end(); ++iter) {
			const City *city = *iter;
			city->initTravel(true, NULL);
		}*/

		mainGamestate->addCivilization(civilization);
	}
	else {
		delete civilization;
		civilization = NULL;
	}
	VI_log("Civilization::load() exit\n");
	return civilization;
}

void Civilization::save(FILE *file) const {
	//VI_log("Civilization::save : %s\n", this->getName());
	fprintf(file, "[civilization]\n");
	fprintf(file, "name=%s\n", this->getName());
	fprintf(file, "map_visible=\n");
	for(int y=0;y<mainGamestate->getMap()->getHeight();y++ ) {
		for(int x=0;x<mainGamestate->getMap()->getWidth();x++ ) {
			fprintf(file, "%c", this->isExplored(x, y) ? 'y' : 'n');
		}
		fprintf(file, "\n");
	}
	fprintf(file, "fog_of_war=\n");
	for(int y=0;y<mainGamestate->getMap()->getHeight();y++ ) {
		for(int x=0;x<mainGamestate->getMap()->getWidth();x++ ) {
			fprintf(file, "%c", this->isFogOfWarVisible(x, y) ? 'y' : 'n');
		}
		fprintf(file, "\n");
	}
	for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end(); ++iter) {
		const City *city = *iter;
		city->save(file);
	}
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		const Unit *unit = *iter;
		unit->save(file);
	}
	fprintf(file, "progress_technology=%d\n", progress_technology);
	fprintf(file, "technology=%s\n", technology == NULL ? "null" : technology->getName());
	for(vector<const Technology *>::const_iterator iter = technologies.begin(); iter != technologies.end(); ++iter) {
		const Technology *technology = *iter;
		fprintf(file, "technologies=%s\n", technology->getName());
	}
	fprintf(file, "power=%d\n", power);
	for(int i=0;i<N_BONUSES;i++) {
		fprintf(file, "bonus=%d\n", i);
		fprintf(file, "%d\n", bonus_turns[i]);
	}
	fprintf(file, "city_name_looped=%d\n", city_name_looped);
	fprintf(file, "city_name_index=%d\n", city_name_index);
	fprintf(file, "history=\n");
	for(map<int, History>::const_iterator iter = history.begin(); iter != history.end(); ++iter) {
		int year = iter->first;
		const History *entry = &iter->second;
		fprintf(file,"year=%d\n", year);
		fprintf(file,"power=%d\n", entry->power);
		fprintf(file,"population=%d\n", entry->population);
		fprintf(file,"production=%d\n", entry->production);
		fprintf(file,"science=%d\n", entry->science);
		fprintf(file,"territory=%d\n", entry->territory);
		fprintf(file,"militarypower=%d\n", entry->militarypower);
	}
	fprintf(file, "endhistory\n");
	fprintf(file, "[/civilization]\n");
}

string Civilization::makeCityName() {
	return race->makeCityName(&city_name_looped, &city_name_index);
}

void Civilization::advanceCityName() {
	return race->advanceCityName(&city_name_looped, &city_name_index);
}

void Civilization::addCity(City *city) {
	ASSERT( city->getCivilization() == this ); // city's civilization field must already be set
	this->cities.push_back(city); // must add city before uncovering, so that the city is registered with the civilization
	//this->uncover(city->getX(), city->getY(), 1);
	this->uncoverCity(city->getX(), city->getY(), 1);
	this->updateFogOfWarCity(city);
	if( mainGamestate->relationshipsDefined() ) {
		// Relationships aren't defined yet if cities are being added whilst loading game. No need to call this function then - and we don't want to, as we can't access the relationships for making contact with other civilizations!
		// n.b.: We make contact with any square adjacent to new city territory. This does lead to odd behaviour that we can make contact with a square even without exploring it.
		// But we need to do it this way, to keep things symmetric (from the other civ's point of view, it would be able to make contact due to being able to see our territory).
		//this->makeContact(city->getX(), city->getY(), 1);
		for(int i=0;i<city->getNCitySquares();i++) {
			Pos2D pos = city->getCitySquare(i);
			if( mainGamestate->getMap()->isValid(pos.x, pos.y) ) {
				this->makeContact(pos.x, pos.y, 1);
			}
		}
	}
}

void Civilization::removeCity(const City *city) {
	for(vector<City *>::iterator iter = cities.begin(); iter != cities.end(); ++iter) {
		const City *this_city = *iter;
		if( this_city == city ) {
			cities.erase(iter);
			this->checkDead();
			return;
		}
	}
	ASSERT( false );
}

size_t Civilization::getNCities() const {
	return this->cities.size();
}

const City *Civilization::getCity(size_t i) const {
	return this->cities.at(i);
}

City *Civilization::getCity(size_t i) {
	return this->cities.at(i);
}

void Civilization::addUnit(Unit *unit) {
	this->units.push_back(unit);
}

void Civilization::removeUnit(const Unit *unit, bool check_dead) {
	for(vector<Unit *>::iterator iter = units.begin(); iter != units.end(); ++iter) {
		const Unit *this_unit = *iter;
		if( this_unit == unit ) {
			units.erase(iter);
			if( check_dead ) {
				this->checkDead();
			}
			return;
		}
	}
	ASSERT( false );
}

size_t Civilization::getNUnits() const {
	return this->units.size();
}

const Unit *Civilization::getUnit(size_t i) const {
	return this->units.at(i);
}

Unit *Civilization::getUnit(size_t i) {
	return this->units.at(i);
}

bool Civilization::hasTechnology(const Technology *technology) const {
	ASSERT( technology != NULL );
	bool found = false;
	for(vector<const Technology *>::const_iterator iter = technologies.begin();iter != technologies.end() && !found; ++iter) {
		const Technology *this_technology = *iter;
		if( this_technology->equals(technology) ) {
			found = true;
		}
	}
	return found;
}

bool Civilization::hasTechnology(const char *technology) const {
	ASSERT( technology != NULL );
	bool found = false;
	for(vector<const Technology *>::const_iterator iter = technologies.begin();iter != technologies.end() && !found; ++iter) {
		const Technology *this_technology = *iter;
		if( this_technology->equals(technology) ) {
			found = true;
		}
	}
	return found;
}

bool Civilization::hasAllTechnology() const {
	if( technologies.size() == mainGamestate->getNTechnologies() ) {
		return true;
	}
	return false;
}

bool Civilization::canResearch(const Technology *technology, bool terrain_restriction) const {
	ASSERT( technology != NULL );
	if( hasTechnology(technology) ) {
		return false;
	}
	else if( technology->getRequires1() != NULL && !hasTechnology( technology->getRequires1() ) ) {
		return false;
	}
	else if( technology->getRequires2() != NULL && !hasTechnology( technology->getRequires2() ) ) {
		return false;
	}

	Age age = technology->getAge();
	int age_i = (int)age;
	for(size_t i=0;i<game_g->getGameData()->getNTechnologies();i++) {
		const Technology *tech = game_g->getGameData()->getTechnology(i);
		Age this_age = tech->getAge();
		int this_age_i = (int)this_age;
		if( this_age_i < age_i ) {
			if( !hasTechnology( tech ) ) {
				return false;
			}
		}
	}

	if( terrain_restriction ) {
		Type terrain = technology->getTerrain();
		if( terrain != TYPE_NONE ) {
			bool has_terrain = false;
			for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end() && !has_terrain; ++iter) {
				const City *city = *iter;
				if( mainGamestate->getMap()->isWithinCityRadius(city->getPos(), terrain, true) ) {
					has_terrain = true;
				}
			}
			if( !has_terrain )
				return false;
		}
	}

	return true;
}

Age Civilization::getAge() const {
	int best_age = 0;
	for(vector<const Technology *>::const_iterator iter = this->technologies.begin(); iter != this->technologies.end(); ++iter ) {
		const Technology *tech = *iter;
		Age this_age = tech->getAge();
		int this_age_i = (int)this_age;
		if( this_age_i > best_age ) {
			best_age = this_age_i;
		}
	}
	return (Age)best_age;
}

void Civilization::removeAllTechnologies() {
	ASSERT( mainGamestate->isTestMode() );
	// only for use in test mode
	this->technologies.clear();
}

void Civilization::addTechnology(const Technology *tech) {
	ASSERT( !hasTechnology( tech ) );

	Age old_age = AGE_ANCIENT;
	if( this == mainGamestate->getPlayer() ) {
		old_age = this->getAge();
	}

	this->technologies.push_back( tech );

	if( this->technology == tech ) {
		this->technology = NULL;
		this->progress_technology = 0;
	}
	// reveal squares from port etc, as range may extend due to new technology
	bool lock = false;
	if( this == mainGamestate->getPlayer() && game_g->getGraphicsEnvironment()->getWorld() != NULL && game_g->getGraphicsEnvironment()->getWorld()->getTerrain() != NULL ) {
		lock = true;
		VI_lock();
	}
	for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end(); ++iter) {
		const City *city = *iter;
		city->initTravel(true, NULL);
	}
	if( lock ) {
		VI_unlock();
	}
	if( tech->equals("Spaceflight") ) {
		this->revealMap();
	}
	if( this == mainGamestate->getPlayer() ) {
		Age new_age = this->getAge();
		//if( new_age != AGE_ANCIENT ) {
		if( new_age != old_age ) {
			stringstream text;
			text << "We have entered the ";
			text << Technology::getAgeName(new_age);
			text << " age!";
			//InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, 320);
			VI_Texture *ageTexture = game_g->getAgeTexture(new_age);
			string button = "Okay";
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), ageTexture, &button, 1, infowindow_def_x_c, infowindow_def_y_c, infowindow_def_w_c, 360, false);
			window->doModal();
			delete window;
		}
	}

}

bool Civilization::updateProgressTechnology(int update) {
	ASSERT( this->technology != NULL );
	int cost = this->technology->getCost(this->mainGamestate);
	//ASSERT( this->progress_technology < cost ); // cost may be reduced as other civs discover technology!
	this->progress_technology += update;
	if( this->progress_technology >= cost ) {
		this->addTechnology( this->technology );
		return true;
	}
	return false;
}

int Civilization::calculatePowerPerTurn() const {
	int power_per_turn = 0;
	for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end(); ++iter) {
		const City *city = *iter;
		power_per_turn += city->calculatePowerPerTurn();
	}
	return power_per_turn;
}

void Civilization::updatePower(int update) {
	int old_power = power;
	this->power += update;

	// get a bonus?

	int required = power_initial_bonus_c;
	while( required <= old_power ) {
		required *= power_bonus_multiplier_c;
	}
	if( power >= required ) {
		int r = rand() % (int)N_BONUSES;
		VI_log("%s receive a power bonus: %d\n", this->getName(), r);
		Bonus bonus = (Bonus)r;
		int turns = 0;
		switch( bonus ) {
			case BONUS_TECH:
				turns = 5;
				break;
			case BONUS_MILITARY:
				turns = 10;
				break;
			case BONUS_GROWTH:
				turns = 10;
				break;
			case BONUS_PRODUCTION:
				turns = 5;
				break;
			case BONUS_WORKERS:
				turns = 10;
				break;
			default:
				ASSERT( false );
				break;
		}
		bonus_turns[r] = turns;
		if( this == mainGamestate->getPlayer() ) {
			stringstream text;
			text << "Your civilization grows increasingly powerful!\n\n";
			switch( bonus ) {
				case BONUS_TECH:
					text << "Technology rate doubled for the next 5 turns.";
					break;
				case BONUS_MILITARY:
					text << "Defence of units increased by 50% for the next 10 turns.";
					break;
				case BONUS_GROWTH:
					text << "City growth doubled for the next 10 turns.";
					break;
				case BONUS_PRODUCTION:
					text << "City production increased by 50% for the next 5 turns.";
					break;
				case BONUS_WORKERS:
					text << "Worker rate doubled for the next 10 turns.";
					break;
				default:
					ASSERT( false );
					break;
			}
			InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
			window->doModal();
			delete window;
		}
	}
}

bool Civilization::hasBonus(Bonus bonus) const {
	int index = (int)bonus;
	ASSERT( index >= 0 && index < (int)N_BONUSES );
	return bonus_turns[index] > 0;
}

int Civilization::calculateTotalScience() const {
	int science = 0;
	for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end(); ++iter) {
		const City *city = *iter;
		science += city->calculateScience();
	}
	return science;
}

int Civilization::calculateTotalProduction() const {
	int production = 0;
	for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end(); ++iter) {
		const City *city = *iter;
		production += city->calculateProduction();
	}
	return production;
}

int Civilization::calculateTotalPopulation() const {
	//int time = clock();
	int population = 0;
	for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end(); ++iter) {
		const City *city = *iter;
		population += city->getPopulation();
	}
	//VI_log(">>> %d\n", clock() - time);
	return population;
}

string Civilization::getTotalPopulationString() const {
	int total_pop = this->calculateTotalPopulation();
	return formatNumber(total_pop);
}

int Civilization::calculateTerritory() const {
	int territory = 0;
	for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
		for(int x=0;x<mainGamestate->getMap()->getWidth();x++) {
			if( mainGamestate->getMap()->getSquare(x, y)->getTerritory() == this )
				territory++;
		}
	}
	return territory;
}

int Civilization::calculateMilitary(int *n_units) const {
	int power = 0;
	*n_units = 0;
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		const Unit *unit = *iter;
		const UnitTemplate *unit_template = unit->getTemplate();
		if( unit_template->isAir() )
			continue;
		power += unit_template->getAttack() + unit_template->getDefence();
		*n_units = *n_units + 1;
	}
	power /= 2;
	return power;
}

int Civilization::calculateAir(int *n_units) const {
	int power = 0;
	*n_units = 0;
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		const Unit *unit = *iter;
		const UnitTemplate *unit_template = unit->getTemplate();
		if( !unit_template->isAir() )
			continue;
		power += unit_template->getAirAttack() + unit_template->getAirDefence();
		*n_units = *n_units + 1;
	}
	power /= 2;
	return power;
}

int Civilization::calculateSea(int *n_units) const {
	int power = 0;
	*n_units = 0;
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		const Unit *unit = *iter;
		const UnitTemplate *unit_template = unit->getTemplate();
		if( !unit_template->isSea() )
			continue;
		power += unit_template->getSeaAttack();
		*n_units = *n_units + 1;
	}
	return power;
}

int Civilization::calculateNuclear() const {
	int power = 0;
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		const Unit *unit = *iter;
		const UnitTemplate *unit_template = unit->getTemplate();
		if( unit_template->getNuclearType() == UnitTemplate::NUCLEARTYPE_FISSION )
			power += 1;
		else if( unit_template->getNuclearType() == UnitTemplate::NUCLEARTYPE_FUSION )
			power += 2;
	}
	return power;
}

void Civilization::update() {
	for(int i=0;i<(int)N_BONUSES;i++) {
		if( this->bonus_turns[i] > 0 ) {
			this->bonus_turns[i]--;
			if( this->bonus_turns[i] == 0 ) {
				VI_log("%s bonus index %d expires.\n", this->getName(), i);
			}
		}
	}
	calculateCivBonuses();
}

void Civilization::calculateCivBonuses() {
	// calculates the bonuses from improvements that apply to all cities
	// we should call this for all civs at both the start and end of the turn, and when loading a saved game
	this->production_bonus = 0;
	this->defence_bonus = 0;
	this->auto_veteran_bonus = false;
	this->research_multiplier = 0;
	for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end(); ++iter) {
		const City *city = *iter;
		for(size_t i=0;i<city->getNImprovements();i++) {
			const Improvement *improvement = city->getImprovement(i);
			if( !city->improvementHasEffect(improvement) ) {
				continue;
			}
			if( improvement->getProductionBonusAllCities() ) {
				this->production_bonus += improvement->getProductionBonus();
			}
			if( improvement->getDefenceBonusAllCities() ) {
				this->defence_bonus += improvement->getDefenceBonus();
			}
			if( improvement->getAutoVeteranBonusAllCities() ) {
				if( improvement->getAutoVeteranBonus() ) {
					this->auto_veteran_bonus = true;
				}
			}
			if( improvement->getResearchMultiplierBonusAllCities() ) {
				this->research_multiplier += improvement->getResearchMultiplierBonus();
			}
		}
	}
}

void Civilization::recordHistory() {
	History entry;
	entry.power = this->getPower();
	entry.population = this->calculateTotalPopulation();
	entry.production = this->calculateTotalProduction();
	entry.science = this->calculateTotalScience();
	entry.territory = this->calculateTerritory();
	int n_land_units = 0;
	entry.militarypower = this->calculateMilitary(&n_land_units);
	history.insert(pair<int, History>(mainGamestate->getYear(), entry));
}

bool Civilization::setFogOfWar(int x,int y) {
	return this->setFogOfWar(x, y, true);
}

bool Civilization::setFogOfWar(int x,int y,bool visible) {
	bool changed = false;
	ASSERT( mainGamestate->getMap()->isValid(x, y) );
	mainGamestate->getMap()->reduceToBase(&x, &y);
	ASSERT( x >= 0 && x < map_width && y >= 0 && y < map_height );
	if( visible ) {
		ASSERT( this->map_visible[y * map_width + x] ); // should only be seeing through fog of war on explored squares
	}
	if( this->fog_of_war[y * map_width + x] != visible ) {
		this->fog_of_war[y * map_width + x] = true;
		changed = true;
	}
	return changed;
}

void Civilization::setMapVisible(int x,int y,bool visible) {
	// n.b., should lock Vision if calling multiple times!
	ASSERT( mainGamestate->getMap()->isValid(x, y) );
	mainGamestate->getMap()->reduceToBase(&x, &y);
	ASSERT( x >= 0 && x < map_width && y >= 0 && y < map_height );
	if( this->map_visible[y * map_width + x] != visible ) {
		this->map_visible[y * map_width + x] = visible;

		if( this == mainGamestate->getPlayer() ) {
			mainGamestate->setMapNeedsUpdate();
		}
		VI_Terrain *terrain = NULL;
		if( this == mainGamestate->getPlayer() && game_g->getGraphicsEnvironment()->getWorld() != NULL )
			terrain = game_g->getGraphicsEnvironment()->getWorld()->getTerrain();
		if( terrain != NULL ) {
			//VI_log("modifying terrain visibility at %d, %d\n", x, y);
			terrain->setVisible(x, y, visible);
			if( mainGamestate->getMap()->getTopology() == TOPOLOGY_CYLINDER ) {
				terrain->setVisible(x + map_width, y, visible);
			}
		}
	}
}

/*void Civilization::setDead() {
	ASSERT( this->cities.size() == 0 && this->units.size() == 0 );
	this->is_dead = true;
}*/

void Civilization::calculateFogOfWar(bool force) {
	if( !force && this->has_calculated_fow ) {
		return;
	}
	VI_log("calculate fog of war for %s\n", this->getName());

	//if( /*false &&*/ this->hasTechnology( mainGamestate->findTechnology("Satellites") ) ) {
	if( /*false &&*/ this->hasTechnology("Satellites") ) {
		for(int i=0;i<map_width*map_height;i++) {
			this->fog_of_war[i] = true;
		}
	}
	else {
		for(int i=0;i<map_width*map_height;i++) {
			this->fog_of_war[i] = false;
		}

		for(vector<City *>::const_iterator iter = cities.begin(); iter != cities.end(); ++iter) {
			const City *city = *iter;
			this->updateFogOfWarCity(city, false);
		}
		for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
			const Unit *unit = *iter;
			Pos2D pos = unit->getPos();
			this->updateFogOfWar(pos, false);
		}

	}

	if( this == mainGamestate->getPlayer() && CHEAT ) {
		for(int i=0;i<map_width*map_height;i++) {
			this->fog_of_war[i] = true;
		}
	}

	this->has_calculated_fow = true;

	if( this == mainGamestate->getPlayer() ) {
		mainGamestate->updateTerrainForFOW();
	}
}

void Civilization::updateFogOfWarCity(const City *city, bool update_terrain) {
	bool need_update = false;
	if( city->hasImprovement("Radar Tower") ) {
	//if( true ) {
		const int range = 5;
		Pos2D pos = city->getPos();
		for(int cy=pos.y-range;cy<=pos.y+range;cy++) {
			for(int cx=pos.x-range;cx<=pos.x+range;cx++) {
				//VI_log("%d, %d : %d : %d\n", cx, cy, mainGamestate->getMap()->isValid(cx, cy), this->isExplored(cx, cy));
				if( mainGamestate->getMap()->isValid(cx, cy) && this->isExplored(cx, cy) ) {
					int diffx = abs(cx - pos.x);
					int diffy = abs(cy - pos.y);
					if( diffx < range && diffy < range && diffx + diffy <= range ) {
						if( this->setFogOfWar(cx, cy) ) {
							need_update = true;
						}
					}
				}
			}
		}
	}
	else {
		for(int i=0;i<city->getNCitySquares();i++) {
			Pos2D pos = city->getCitySquare(i);
			if( mainGamestate->getMap()->isValid(pos.x, pos.y) && this->isExplored(pos.x, pos.y) ) {
				if( this->setFogOfWar(pos.x, pos.y) ) {
					need_update = true;
				}
			}
		}
	}

	if( update_terrain && need_update && this == mainGamestate->getPlayer() ) {
		mainGamestate->updateTerrainForFOW();
	}
}

void Civilization::updateFogOfWar(Pos2D pos, bool update_terrain) {
	bool need_update = false;
	const int range = 1;
	for(int cy=pos.y-range;cy<=pos.y+range;cy++) {
		for(int cx=pos.x-range;cx<=pos.x+range;cx++) {
			if( mainGamestate->getMap()->isValid(cx, cy) && this->isExplored(cx, cy) ) {
				if( this->setFogOfWar(cx, cy) ) {
					need_update = true;
				}
			}
		}
	}

	if( update_terrain && need_update && this == mainGamestate->getPlayer() ) {
		mainGamestate->updateTerrainForFOW();
	}
}

bool Civilization::isFogOfWarVisible(int x, int y) const {
	ASSERT( mainGamestate->getMap()->isValid(x, y) );
	mainGamestate->getMap()->reduceToBase(&x, &y);
	ASSERT( x >= 0 && x < map_width && y >= 0 && y < map_height );
	return fog_of_war[y * map_width + x];
}

bool Civilization::isExplored(int x,int y) const {
	ASSERT( mainGamestate->getMap()->isValid(x, y) );
	mainGamestate->getMap()->reduceToBase(&x, &y);
	ASSERT( x >= 0 && x < map_width && y >= 0 && y < map_height );
	return map_visible[y * map_width + x];
}

// use when we know that x and y are in the base range
bool Civilization::isExploredBase(int x,int y) const {
	//ASSERT( mainGamestate->getMap()->isValidBase(x, y) );
	ASSERT( x >= 0 && x < map_width && y >= 0 && y < map_height );
	//return map_visible[y * mainGamestate->getMap()->getWidth() + x];
	return map_visible[y * map_width + x];
}

bool Civilization::isExploredBase(int indx) const {
	ASSERT( indx >= 0 && indx < map_width * map_height );
	return map_visible[indx];
}

bool Civilization::uncover(int x,int y,int range) {
	// a good idea to lock, if calling this repeatedly!
	/*ASSERT( mainGamestate->getMap()->isValid(x, y) );
	mainGamestate->getMap()->reduceToBase(&x, &y);*/
	ASSERT( x >= 0 && x < map_width && y >= 0 && y < map_height );
	bool lock = false;
	if( this == mainGamestate->getPlayer() && game_g->getGraphicsEnvironment()->getWorld() != NULL && game_g->getGraphicsEnvironment()->getWorld()->getTerrain() != NULL ) {
		lock = true;
		VI_lock();
	}
	bool uncovered = false;
	for(int cy=y-range;cy<=y+range;cy++) {
		for(int cx=x-range;cx<=x+range;cx++) {
			if( mainGamestate->getMap()->isValid(cx, cy) ) {
				if( !isExplored(cx, cy) ) {
					uncovered = true;
					setMapVisible(cx, cy, true);
				}
			}
		}
	}
	if( lock ) {
		VI_unlock();
	}
	return uncovered;
}

bool Civilization::uncoverCity(int x,int y,int range) {
	// a good idea to lock, if calling this repeatedly!
	ASSERT( x >= 0 && x < map_width && y >= 0 && y < map_height );
	bool lock = false;
	if( this == mainGamestate->getPlayer() && game_g->getGraphicsEnvironment()->getWorld() != NULL && game_g->getGraphicsEnvironment()->getWorld()->getTerrain() != NULL ) {
		lock = true;
		VI_lock();
	}
	bool uncovered = false;
	for(int i=0;i<City::getNCitySquares();i++) {
		Pos2D pos = City::getCitySquare(mainGamestate->getMap(), i, Pos2D(x, y));
		if( mainGamestate->getMap()->isValid(pos.x, pos.y) && this->uncover(pos.x, pos.y, range) ) {
			uncovered = true;
		}
	}
	if( lock ) {
		VI_unlock();
	}
	return uncovered;
}

void Civilization::makeContact(int x,int y,int range) {
	// n.b., we should never lock Vision outside of this function, as we may need to open a window!
	ASSERT( !VI_islocked() );
	for(int cy=y-range;cy<=y+range;cy++) {
		for(int cx=x-range;cx<=x+range;cx++) {
			if( mainGamestate->getMap()->isValid(cx, cy) ) {
				// make contacts?
				if( cx != x || cy != y ) {
					const Civilization *o_civ = mainGamestate->getMap()->findCivilizationAt(Pos2D(cx, cy));
					if( o_civ != NULL && this != o_civ ) {
						Relationship *relationship = mainGamestate->findRelationship(this, o_civ);
						if( !relationship->madeContact() ) {
							if( this == mainGamestate->getPlayer() || o_civ == mainGamestate->getPlayer() ) {
								const Civilization *cpu_civ = NULL;
								if( this != mainGamestate->getPlayer() )
									cpu_civ = this;
								else if( o_civ != mainGamestate->getPlayer() )
									cpu_civ = o_civ;
								ASSERT( cpu_civ != NULL );
								stringstream text;
								text << "We have made contact with " << cpu_civ->getName() << ".";
								VI_Panel *panel = game_g->getGamestate()->getPanel(); // we use this rather than mainGamestate, as for corrupt save game files, we may need to call this when loading a game, when the mainGamestate doesn't yet have its panel
								InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), panel, text.str(), game_g->getFont(), game_g->getPanelTexture());
								window->doModal();
								delete window;
							}
							relationship->makeContact();
						}
					}
					// now check for territory
					const Civilization *t_civ = mainGamestate->getMap()->getSquare(cx, cy)->getTerritory();
					if( t_civ != NULL && t_civ != o_civ && this != t_civ ) {
						Relationship *relationship = mainGamestate->findRelationship(this, t_civ);
						if( !relationship->madeContact() ) {
							if( this == mainGamestate->getPlayer() || t_civ == mainGamestate->getPlayer() ) {
								const Civilization *cpu_civ = NULL;
								if( this != mainGamestate->getPlayer() )
									cpu_civ = this;
								else if( t_civ != mainGamestate->getPlayer() )
									cpu_civ = t_civ;
								ASSERT( cpu_civ != NULL );
								stringstream text;
								text << "We have made contact with " << cpu_civ->getName() << ".";
								VI_Panel *panel = game_g->getGamestate()->getPanel(); // we use this rather than mainGamestate, as for corrupt save game files, we may need to call this when loading a game, when the mainGamestate doesn't yet have its panel
								InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), panel, text.str(), game_g->getFont(), game_g->getPanelTexture());
								window->doModal();
								delete window;
							}
							relationship->makeContact();
						}
					}
				}
			}
		}
	}
}

void Civilization::revealMap() {
	/*VI_Terrain *terrain = NULL;
	if( game_g->getGraphicsEnvironment()->getWorld() != NULL && this == mainGamestate->getPlayer() )
		terrain = game_g->getGraphicsEnvironment()->getWorld()->getTerrain();*/
	bool lock = false;
	if( this == mainGamestate->getPlayer() && game_g->getGraphicsEnvironment()->getWorld() != NULL && game_g->getGraphicsEnvironment()->getWorld()->getTerrain() != NULL ) {
		lock = true;
		VI_lock();
	}
	/*for(int i=0;i<mainGamestate->getMap()->getWidth()*mainGamestate->getMap()->getHeight();i++) {
		this->map_visible[i] = true;
	}*/
	for(int y=0;y<map_height;y++) {
		for(int x=0;x<map_width;x++) {
			this->setMapVisible(x, y, true);
		}
	}
	if( lock ) {
		VI_unlock();
	}
	/*if( terrain != NULL ) {
		VI_lock();
		for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
			for(int x=0;x<mainGamestate->getMap()->getWidth();x++) {
				terrain->setVisible(x, y, true);
			}
		}
		VI_unlock();
	}*/
}

// returns true iff *both* Civilizations have explored areas the other hasn't (i.e., so one is not equal nor a superset to the other)
bool Civilization::compareMaps(const Civilization *that) const {
	bool this_knows_more = false;
	bool that_knows_more = false;
	for(int i=0;i<map_width*map_height;i++) {
		if( this->map_visible[i] != that->map_visible[i] ) {
			if( this->map_visible[i] )
				this_knows_more = true;
			else if( that->map_visible[i] )
				that_knows_more = true;
			if( this_knows_more && that_knows_more )
				return true;
		}
	}
	return false;
}

bool Civilization::canShareMaps(Civilization *that) const {
	//const Technology *tech = mainGamestate->findTechnology("Maps");
	//if( this->hasTechnology(tech) && that->hasTechnology(tech) ) {
	if( this->hasTechnology("Maps") && that->hasTechnology("Maps") ) {
		return true;
	}
	return false;
}

void Civilization::shareMaps(Civilization *that) {
	ASSERT( this->canShareMaps(that) );
	/*for(int i=0;i<mainGamestate->getMap()->getWidth()*mainGamestate->getMap()->getHeight();i++) {
		if( this->map_visible[i] != that->map_visible[i] ) {
			// one must be visible, so set them to both be visible
			this->map_visible[i] = true;
			that->map_visible[i] = true;
		}
	}*/
	bool lock = false;
	if( ( this == mainGamestate->getPlayer() || that == mainGamestate->getPlayer() ) && game_g->getGraphicsEnvironment()->getWorld() != NULL && game_g->getGraphicsEnvironment()->getWorld()->getTerrain() != NULL ) {
		lock = true;
		VI_lock();
	}
	for(int y=0;y<map_height;y++) {
		for(int x=0;x<map_width;x++) {
			if( this->isExplored(x, y) != that->isExplored(x, y) ) {
				// one must be visible, so set them to both be visible
				this->setMapVisible(x, y, true);
				that->setMapVisible(x, y, true);
			}
		}
	}
	if( lock ) {
		VI_unlock();
	}
}

/*int Civilization::getArmyStrength() const {
	int strength = 0;
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		const Unit *unit = *iter;
		const UnitTemplate *unit_template = unit->getTemplate();
		int this_strength = unit_template->getAttack() + unit_template->getDefence();
		strength += this_strength;
	}
	return strength;
}*/

void Civilization::getDistinctTechnologies(MainGamestate *mainGamestate, vector<const Technology *> *technologies1, vector<const Technology *> *technologies2, const Civilization *civ1, const Civilization *civ2) {
	for(size_t i=0;i<mainGamestate->getNTechnologies();i++) {
		const Technology *technology = mainGamestate->getTechnology(i);
		bool civ1_has = civ1->hasTechnology(technology);
		bool civ2_has = civ2->hasTechnology(technology);
		if( civ1_has != civ2_has ) {
			if( civ1_has && civ2->canResearch(technology, false) )
				technologies1->push_back(technology);
			else if( civ2_has && civ1->canResearch(technology, false) )
				technologies2->push_back(technology);
		}
	}
}

void Civilization::chooseTechnologyAI() {
	ASSERT( this != mainGamestate->getPlayer() );
	ASSERT( this->progress_technology == 0 );
	ASSERT( this->technology == NULL );
	vector<const Technology *> candidates;
	int max_value = 0;
	for(size_t i=0;i<mainGamestate->getNTechnologies();i++) {
		const Technology *tech = mainGamestate->getTechnology(i);
		if( this->canResearch(tech, true) ) {
			//int r = rand() % 20; // old value
			int r = rand() % 30;
			int this_value = tech->getAIWeight() + r;

			// hard-coded hacks
			if( this_value > max_value && tech->getAIHint() == Technology::AIHINT_REQUIRES_AIRPORT ) {
				// no point researching fission until we have at least one airport (in order to build fission bombs)
				bool found_airport = false;
				for(vector<City *>::iterator iter = cities.begin(); iter != cities.end() && !found_airport; ++iter) {
					const City *city = *iter;
					if( city->hasImprovement("Airport") ) {
						found_airport = true;
					}
				}
				if( !found_airport ) {
					this_value = 0;
				}
			}

			if( this_value > max_value ) {
				candidates.clear();
				candidates.push_back(tech);
				max_value = this_value;
			}
			else if( this_value == max_value ) {
				candidates.push_back(tech);
			}
		}
	}
	if( candidates.size() > 0 ) {
		int r = rand() % candidates.size();
		this->technology = candidates.at(r);
		VI_log("AI: %s start researching %s\n", this->getName(), this->technology->getName());
	}
}

string Civilization::getAIText(AIText aiText) {
	vector<string> candidates;
	switch( aiText ) {
		case AITEXT_OPENING_AT_PEACE:
			{
				stringstream str;
				str << "Greetings from the ";
				str << this->getNameAdjective();
				str << " people. How may we help you?";
				candidates.push_back(str.str());
			}
			candidates.push_back("What can we do for you, friend?");
			break;
		case AITEXT_OPENING_AT_WAR:
			candidates.push_back("What do you want?");
			candidates.push_back("Yes?");
			candidates.push_back("Stop wasting my time!");
			break;
		case AITEXT_DECLARED_WAR:
			candidates.push_back("Prepare to die!");
			candidates.push_back("We will wipe you from the face of the earth!");
			if( this->calculateNuclear() >= 10 ) {
				candidates.push_back("Prepare to be nuked back to the stone age!");
			}
			break;
		case AITEXT_MAKE_PEACE_YES:
			candidates.push_back("Let us put an end to this bloodshed.");
			candidates.push_back("There is no point in this bloody war. We will make peace.");
			candidates.push_back("We accept your offer of peace.");
			break;
		case AITEXT_MAKE_PEACE_NO:
			candidates.push_back("Never! We will keep fighting until you are destroyed!");
			candidates.push_back("No I don't think so.");
			candidates.push_back("We refuse your offer of peace.");
			{
				stringstream str;
				str << "Never, you must pay the price for insulting the ";
				str << this->getNameAdjective();
				str << " people!";
				candidates.push_back(str.str());
			}
			break;
		case AITEXT_BREAK_RIGHTOFPASSAGE:
			candidates.push_back("Very well, we will no longer enter your territory.");
			candidates.push_back("Good riddance! Maybe you will stop cluttering up our territory with your units.");
			break;
		case AITEXT_MAKE_RIGHTOFPASSAGE_YES:
			candidates.push_back("We welcome this agreement.");
			candidates.push_back("We agree that this will be beneficial to both our nations.");
			break;
		case AITEXT_MAKE_RIGHTOFPASSAGE_NO:
			candidates.push_back("And let you stab us in the back? I don't think so.");
			candidates.push_back("We would rather not allow you access to our lands.");
			break;
		/*case AITEXT_:
			candidates.push_back("");
			break;*/
		default:
			ASSERT( false );
			break;
	}
	ASSERT( candidates.size() > 0 );
	int r = rand() % candidates.size();
	string text = candidates.at(r);
	VI_log("%s: %s\n", this->getName(), text.c_str());
	return text;
}
