#pragma once

/** Map and related classes.
*/

#include "common.h"
#include "utils.h"

#include "../Vision/VisionShared.h"

#include <vector>
using std::vector;

class MainGamestate;
class Race;
class Civilization;
class Relationship;
class City;
class Technology;
class Buildable;
class Improvement;
class UnitTemplate;
class Unit;
class InfoWindow;
class BonusResource;

class VI_Texture;

const int n_map_overlays_c = 2;

class MapSquare {
/*public:
	enum Type {
		TYPE_NONE = -2,
		TYPE_UNKNOWN = -1, // used as return to AI, when square not yet explored
		TYPE_OCEAN = 0,
		TYPE_GRASSLAND = 1,
		TYPE_FOREST = 2,
		TYPE_HILLS = 3,
		TYPE_MOUNTAINS = 4,
		TYPE_DESERT = 5,
		TYPE_ARTIC = 6,
		N_TYPES = 7
	};
	enum Road {
		ROAD_UNKNOWN = -1, // used as return to AI, when square not yet explored
		ROAD_NONE = 0,
		ROAD_BASIC = 1,
		ROAD_RAILWAYS = 2
	};*/
private:
	MainGamestate *mainGamestate;
	Pos2D pos;
	Type type;
	Road road;
	int progress_road;
	const BonusResource *bonus_resource;
	const Civilization *territory;
	bool target; // flag for GUI

	City *city;
	//list<Unit *> units;
	vector<Unit *> units; // TODO: better as a list?

	static void validateType(Type type);

	static VI_Texture *mapTextures[N_TYPES];
	static VI_Texture *mapOverlays[N_TYPES][n_map_overlays_c];
	//static VI_Texture *roadTextures[9];
	static VI_Texture *combinedRoadTextures[256];
	static VI_Texture *combinedRailwayTextures[256];
public:
	MapSquare(); // must set mainGamestate and pos separately, with setMainGamestate() and setPos()!
	~MapSquare();

	void setMaingameState(MainGamestate *mainGamestate) {
		this->mainGamestate = mainGamestate;
	}
	void setPos(Pos2D pos) {
		this->pos = pos;
	}
	Type getType() const {
		return type;
	}
	void setType(Type type) {
		validateType(type);
		this->type = type;
	}
	const BonusResource *getBonusResource() const {
		return this->bonus_resource;
	}
	void setBonusResource(const BonusResource *bonus_resource);
	const char *getName() const;
	static VI_Texture *getTexture(Type this_type) {
		validateType(this_type);
		return mapTextures[this_type];
	}
	static VI_Texture *getRoadTexture(int index) {
		//return roadTextures[index];
		return combinedRoadTextures[index];
	}
	static VI_Texture *getRailwayTexture(int index) {
		return combinedRailwayTextures[index];
	}
	VI_Texture *getTexture() const {
		// n.b., return non-const needed due to draw functions being non-const
		return mapTextures[type];
	}
	VI_Texture *getOverlayTexture(int index) const;
	// n.b., return non-const needed due to draw functions being non-const
	bool drawAboveRoad() const;
	bool isLand() const {
		return type != TYPE_OCEAN;
	}
	bool givesRoadBonus() const {
		// whether the square would give a bonus for a road (whether or not the road is actually present in this square)
		if( type == TYPE_GRASSLAND || type == TYPE_ARTIC || type == TYPE_DESERT ) {
			return true;
		}
		return false;
	}

	Road getRoad() const {
		return road;
	}
	void setRoad(Road road) {
		this->road = road;
	}
	int getProgressRoad() const {
		return progress_road;
	}
	void setProgressRoad(int progress_road);
	int getRoadCost() const;
	bool buildRoad(const Unit *unit);
	bool buildRailways(const Unit *unit);
	//bool hasWorkers() const;
	bool canBeImproved(const Civilization *civilization) const;
	bool canMoveTo(const UnitTemplate *unit_template) const;
	bool canBuildCity() const;
	const Civilization *getTerritory() const {
		return this->territory;
	}
	void setTerritory(const Civilization *territory) {
		this->territory = territory;
	}

	Rational getMovementCost(const Civilization *civilization) const;
	int getScaledMovementCost(const Civilization *civilization) const;

	City *getCity() const {
		// city isn't owned by this class, so okay to return non-const in const member function
		return city;
	}
	void setCity(City *city, bool update);
	const vector<Unit *> *getUnits() const {
	//vector<Unit *> *getUnits() {
		// units aren't owned by this class, so okay to return non-const in const member function
		return &units;
	}
	// returns true iff there is another civ's unit *or* city (even if unoccupied)
	bool hasForeignUnits(const Civilization *civ) const;
	// returns true iff there is an enemy civ's unit that has attack > 0
	bool hasEnemies(const Civilization *civ) const;
	Unit *getBestDefender() const;
	Unit *getBestSeaUnit() const;
	void addUnit(Unit *unit) {
		this->units.push_back(unit);
	}
	void removeUnit(const Unit *unit);
	bool isTarget() const {
		return target;
	}
	void setTarget(bool target) {
		this->target = target;
	}

	static void loadTextures();
};

class Distance {
public:
	int indx;
	short cost;
	short squares;
	//bool is_travelling;
	Distance() : indx(-1), cost(-1), squares(-1)/*, is_travelling(false)*/ {
	}
};

class Map {
//public:
	/*enum Topology {
		TOPOLOGY_UNDEFINED = -1,
		// n.b., enum values shouldn't be changed, otherwise save game files will break!
		TOPOLOGY_FLAT = 0,
		TOPOLOGY_CYLINDER = 1
	};*/
protected:
	MainGamestate *mainGamestate;
	MapSquare *squares;
	int width;
	int height;
	Topology topology;

	VI_Texture *splat_alpha;

	void init();
	void free();
	//void calculateDistanceMap_updateSquare(const MapSquare *from_square, int cx, int cy, int cost, int squares, Distance *temp, vector<int> *queue, const Civilization *civilization, const Unit *unit, bool forwards, bool by_sea) const;
	//void calculateDistanceMap_processQueueItem(int indx, const Distance *dists, Distance *temp, vector<int> *queue, bool reached_target, const Civilization *civilization, const Unit *unit, bool forwards, int ex, int ey, bool by_sea) const;

public:
	Map(MainGamestate *mainGamestate);
	Map(MainGamestate *mainGamestate, int width, int height);
	~Map();

	void createAlphamap();
	VI_Texture *getAlphamap() const {
		// n.b., return non-const needed due to draw functions being non-const
		return splat_alpha;
	}
	void createRandomBonuses();
	void createRandom();
	bool readFile(const char *filename);

	int getWidth() const {
		return width;
	}
	int getHeight() const {
		return height;
	}
	void setTopology(Topology topology) {
		this->topology = topology;
	}
	Topology getTopology() const {
		return this->topology;
	}
	const MapSquare *getSquare(int x, int y) const {
		ASSERT( isValid(x, y) );
		reduceToBase(&x, &y);
		return &squares[y*width+x];
	}
	MapSquare *getSquare(int x, int y) {
		ASSERT( isValid(x, y) );
		reduceToBase(&x, &y);
		return &squares[y*width+x];
	}
	// use when we know that x and y are in the base range
	MapSquare *getSquareBase(int x, int y) {
		ASSERT( isValidBase(x, y) );
		return &squares[y*width+x];
	}
	const MapSquare *getSquare(Pos2D pos) const {
		ASSERT( isValid(pos.x, pos.y) );
		reduceToBase(&pos.x, &pos.y);
		return getSquare(pos.x, pos.y);
	}
	MapSquare *getSquare(Pos2D pos) {
		ASSERT( isValid(pos.x, pos.y) );
		reduceToBase(&pos.x, &pos.y);
		return getSquare(pos.x, pos.y);
	}
	int distanceChebyshev(Pos2D p0, Pos2D p1) const;
	void setAllTo(Type type) {
		for(int i=0;i<width*height;i++) {
			squares[i].setType(type);
		}
	}
	void setSquareIndex(int index, Type type) {
		ASSERT(index >= 0 && index < width*height );
		squares[index].setType(type);
	}

	bool isValidBase(int x,int y) const {
		return( x >= 0 && x < width && y >= 0 && y < height );
	}
	bool isValid(int x,int y) const {
		if( topology == TOPOLOGY_FLAT ) {
			return( x >= 0 && x < width && y >= 0 && y < height );
		}
		else if( topology == TOPOLOGY_CYLINDER ) {
	        return( y >= 0 && y < height );
		}
		ASSERT( false );
		return false;
	}
	template <class T>
	void reduceToBase(T *x, T *y) const {
		if( topology == TOPOLOGY_CYLINDER ) {
			while( *x < 0 )
				*x = *x + width;
			while( *x >= width )
				*x = *x - width;
		}
	}
	template <class T>
	void reduceToBase(T *x, T *y, T off_x, T off_y) const {
		if( topology == TOPOLOGY_CYLINDER ) {
			while( *x < off_x )
				*x = *x + width;
			while( *x >= off_x + width )
				*x = *x - width;
		}
	}
	void reduceDiff(int *dx, int *dy) const {
		if( topology == TOPOLOGY_CYLINDER ) {
			int hw = width/2;
			while( *dx > hw )
				*dx -= width;
			while( *dx < hw-width)
				*dx += width;
		}
	}
	void reduceScreenPosToBase(int *x, int *y) const;

	City *findCity(int x, int y) const;
	// city isn't owned by this class, so okay to return non-const in const member function
	City *findCity(Pos2D pos) const {
		// city isn't owned by this class, so okay to return non-const in const member function
		return findCity(pos.x, pos.y);
	}
	const vector <Unit *> *findUnitsAt(int x, int y) const;
	// units aren't owned by this class, so okay to return non-const in const member function
	const vector <Unit *> *findUnitsAt(Pos2D pos) const {
		// units aren't owned by this class, so okay to return non-const in const member function
		return this->findUnitsAt(pos.x, pos.y);
	}
	Civilization *findCivilizationAt(Pos2D pos) const;
	// civilization isn't owned by this class, so okay to return non-const in const member function
	//bool canBuildCity(int x, int y) const;
	bool findRandomFreeSquare(Pos2D *pos) const;
	bool isOnOrAdjacent(Pos2D pos, Type mapType) const;
	bool isWithinCityRadius(Pos2D pos, Type mapType, bool include_centre) const;
	void ensureNearbyTerrain(Pos2D pos, Type type);

	Distance *calculateDistanceMap(const Civilization *civilization, const Unit *unit, bool include_travel, bool forwards, int sx, int sy, int ex = -1, int ey = -1, bool by_sea = false, bool by_travel = false);
	//static void calculateDistanceMap_processQueueItem_Thread(void *ptr);
	void resetTargets();
};
