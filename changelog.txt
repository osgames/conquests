1.2 release from the trunk series released 2012-09-22


Release notes:

FIXED Fixed makefile for Ubuntu 12.04.
FIXED Slow time rate on some Linux installations.
FIXED Mousewheel zoom wasn't working when mouse was over minimap.
FIXED FPS now accurate on Linux.
ADDED Application icon.
ADDED Display history for military strength too, under F5 scores screens.
UPDATED Improved method of not displaying the movement buttons (wasn't working
        right for high resolutions).
