#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>

class ResolutionEntry {
public:
    int width, height, bpp;
    ResolutionEntry() : width(-1), height(-1), bpp(-1) {
    }
    ResolutionEntry(int width, int height, int bpp) : width(width), height(height), bpp(bpp) {
    }
    QString toString() {
        QString string = QString::number(width) + "x" + QString::number(height);
        return string;
    }
    bool operator==(const ResolutionEntry &that) const {
        //qDebug("compare %dx%dx%d with %dx%dx%d", width, height, bpp, that.width, that.height, that.bpp);
        if( this->width == that.width && this->height == that.height && this->bpp == that.bpp ) {
            return true;
        }
        else {
            return false;
        }
    }
};
Q_DECLARE_METATYPE(ResolutionEntry)

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QComboBox *itemAPI;
    QComboBox *itemResolution;
    QCheckBox *itemFullscreen;
    QComboBox *itemAntiAliasing;
    QCheckBox *itemSMP;
    QCheckBox *itemWantShaders;
    QCheckBox *itemBumpMapping;
    QCheckBox *itemTerrainSplatting;
    QCheckBox *itemWaterEffects;
    QCheckBox *itemAtmosphere;
    QCheckBox *itemHighDynamicRange;
    QCheckBox *itemAllowHardwareVertexAnimation;
    QCheckBox *itemAllowTwoSidedStencil;
    QCheckBox *itemAllowHardwareTL;
    QCheckBox *itemAllowVBOs;
    QPushButton *itemOkay;
    QPushButton *itemApply;
    QPushButton *itemCancel;

    QList<ResolutionEntry> resolutions;

    void fillResolutions();

private slots:
    void changedWantShaders();
    void clickedOkay();
    void clickedApply();
    void clickedCancel();

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
};

#endif // MAINWINDOW_H
