#ifdef _WIN32
#include <windows.h>
#include <direct.h> // for mkdir
#endif

#include "mainwindow.h"

#include "../Vision/VisionPrefs.h"

const int required_bpp = 32;

class QtMessageInterface : public MessageInterface {
public:
    virtual void show(const char *message) {
        QMessageBox::warning(NULL, "Warning", message);
    }
    virtual void log(const char *message) {
        qDebug(message);
    }
};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    itemAPI(NULL),
    itemResolution(NULL),
    itemFullscreen(NULL),
    itemAntiAliasing(NULL),
    itemSMP(NULL),
    itemWantShaders(NULL),
    itemBumpMapping(NULL),
    itemTerrainSplatting(NULL),
    itemWaterEffects(NULL),
    itemAtmosphere(NULL),
    itemHighDynamicRange(NULL),
    itemAllowHardwareVertexAnimation(NULL),
    itemAllowTwoSidedStencil(NULL),
    itemAllowHardwareTL(NULL),
    itemAllowVBOs(NULL),
    itemOkay(NULL),
    itemApply(NULL),
    itemCancel(NULL)
{
    // create GUI
    this->setWindowTitle("Vision Preferences");

    QWidget *centralWidget = new QWidget(this);
    centralWidget->setContextMenuPolicy(Qt::NoContextMenu); // explicitly forbid usage of context menu so actions item is not shown menu
    this->setCentralWidget(centralWidget);

    QVBoxLayout *layout = new QVBoxLayout();
    centralWidget->setLayout(layout);

    {
        QHBoxLayout *h_layout = new QHBoxLayout();
        layout->addLayout(h_layout);

        QLabel *label = new QLabel(tr("API (Windows only)"));
        h_layout->addWidget(label);

        itemAPI = new QComboBox();
        h_layout->addWidget(itemAPI);
        itemAPI->setToolTip("Direct3D 9 is recommended on most hardware, but OpenGL may be better in some cases.\nSoftware renderer is experimental only.");
        for(int i=0;i<nVisionAPIs;i++) {
            itemAPI->addItem(VisionAPIs[i]);
        }
    }

    {
        QHBoxLayout *h_layout = new QHBoxLayout();
        layout->addLayout(h_layout);

        QLabel *label = new QLabel(tr("Resolution"));
        h_layout->addWidget(label);

        itemResolution = new QComboBox();
        h_layout->addWidget(itemResolution);
        this->fillResolutions();
    }

    itemFullscreen = new QCheckBox(tr("Fullscreen"));
    layout->addWidget(itemFullscreen);

    {
        QHBoxLayout *h_layout = new QHBoxLayout();
        layout->addLayout(h_layout);

        QLabel *label = new QLabel(tr("Anti-Aliasing"));
        h_layout->addWidget(label);

        itemAntiAliasing = new QComboBox();
        h_layout->addWidget(itemAntiAliasing);
        itemAntiAliasing->setToolTip("Gives a smoother appearance and reduces the appearance of jagged edges (not available on some hardware)");
        for(int i=0;i<nVisionMultisample;i++) {
            itemAntiAliasing->addItem(VisionMultisample[i]);
        }
    }

    itemSMP = new QCheckBox(tr("SMP Enabled"));
    layout->addWidget(itemSMP);
    itemSMP->setToolTip("Enables support for more than one processor");

    {
        QHBoxLayout *h_layout = new QHBoxLayout();
        layout->addLayout(h_layout);

        QGroupBox *groupEffects = new QGroupBox(tr("Effects"));
        h_layout->addWidget(groupEffects);
        {
            QVBoxLayout *v_layout = new QVBoxLayout();
            groupEffects->setLayout(v_layout);

            itemWantShaders = new QCheckBox(tr("Want Shaders"));
            v_layout->addWidget(itemWantShaders);
            itemWantShaders->setToolTip("Enables per-pixel lighting, and also required for other effects");
            connect(itemWantShaders, SIGNAL(stateChanged(int)), this, SLOT(changedWantShaders()));

            itemBumpMapping = new QCheckBox(tr("Bump Mapping"));
            v_layout->addWidget(itemBumpMapping);
            itemBumpMapping->setToolTip("Enables bumpy appearance on some surfaces");

            itemTerrainSplatting = new QCheckBox(tr("Terrain Splatting"));
            v_layout->addWidget(itemTerrainSplatting);
            itemTerrainSplatting->setToolTip("Enables smooth blending between different terrain textures");

            itemWaterEffects = new QCheckBox(tr("Water Effects"));
            v_layout->addWidget(itemWaterEffects);
            itemWaterEffects->setToolTip("Enables reflection, waves and fresnel effects on water");

            itemAtmosphere = new QCheckBox(tr("Atmosphere"));
            v_layout->addWidget(itemAtmosphere);
            itemAtmosphere->setToolTip("Enables realistic sun and atmosphere rendering");

            itemHighDynamicRange = new QCheckBox(tr("High Dynamic Range"));
            v_layout->addWidget(itemHighDynamicRange);
            itemHighDynamicRange->setToolTip("Enables high dynamic range rendering");
        }

        QGroupBox *groupDebugging = new QGroupBox(tr("Debugging"));
        h_layout->addWidget(groupDebugging);
        groupDebugging->setToolTip("It is recommended that all the options in the \"Debugging\" section are enabled, unless things aren't working properly on your hardware.\nDisabling options may result in poorer performance.");
        {
            QVBoxLayout *v_layout = new QVBoxLayout();
            groupDebugging->setLayout(v_layout);

            itemAllowHardwareVertexAnimation = new QCheckBox(tr("Allow Hardware Vertex Animation"));
            v_layout->addWidget(itemAllowHardwareVertexAnimation);
            itemAllowHardwareVertexAnimation->setToolTip("Allows per-vertex animation to be done in hardware (faster)");

            itemAllowTwoSidedStencil = new QCheckBox(tr("Allow Two-Sided Stencil"));
            v_layout->addWidget(itemAllowTwoSidedStencil);
            itemAllowTwoSidedStencil->setToolTip("Allows use of two-sided stencils (faster shadow renderering)");

            itemAllowHardwareTL = new QCheckBox(tr("Allow Hardware T&L"));
            v_layout->addWidget(itemAllowHardwareTL);
            itemAllowHardwareTL->setToolTip("Allows Transform & Lighting to be done on the graphics card (Direct3D only)");

            itemAllowVBOs = new QCheckBox(tr("Allow VBOs"));
            v_layout->addWidget(itemAllowVBOs);
            itemAllowVBOs->setToolTip("Allows Vertex Buffer Objects (OpenGL only)");
        }

    }

    {
        QHBoxLayout *h_layout = new QHBoxLayout();
        layout->addLayout(h_layout);

        itemOkay = new QPushButton(tr("Okay"));
        h_layout->addWidget(itemOkay);
        connect(itemOkay, SIGNAL(clicked()), this, SLOT(clickedOkay()));

        itemApply = new QPushButton(tr("Apply"));
        h_layout->addWidget(itemApply);
        connect(itemApply, SIGNAL(clicked()), this, SLOT(clickedApply()));

        itemCancel = new QPushButton(tr("Cancel"));
        h_layout->addWidget(itemCancel);
        connect(itemCancel, SIGNAL(clicked()), this, SLOT(clickedCancel()));
    }

    // load settings
    QtMessageInterface messageInterface;
    VisionPrefs visionPrefs;
    visionPrefs.load(&messageInterface);

    // fill GUI from settings

    qDebug("API: %d", (int)visionPrefs.api);
    this->itemAPI->setCurrentIndex((int)visionPrefs.api);

    {
        qDebug("Resolution: %dx%d", visionPrefs.resolution_width, visionPrefs.resolution_height);
        ResolutionEntry entry(visionPrefs.resolution_width, visionPrefs.resolution_height, required_bpp);
        bool found = false;
        for(int i=0;i<resolutions.size() && !found;i++) {
            QVariant variant = this->itemResolution->itemData(i);
            ResolutionEntry this_entry = variant.value<ResolutionEntry>();
            if( this_entry == entry ) {
                found = true;
                this->itemResolution->setCurrentIndex(i);
            }
        }
        if( !found ) {
            QMessageBox::warning(this, "Warning", "The resolution setting in the configuration file isn't supported on this graphics device");
            this->itemResolution->setCurrentIndex(0);
        }
    }

    this->itemFullscreen->setChecked( visionPrefs.fullscreen );

    {
        bool found = false;
        for(int i=0;i<nVisionMultisample && !found;i++) {
            if( VisionMultisampleValues[i] == visionPrefs.multisample ) {
                found = true;
                this->itemAntiAliasing->setCurrentIndex(i);
            }
        }
        if( !found ) {
            QMessageBox::warning(this, "Warning", "Unrecognised Anti-Aliasing setting");
            this->itemAntiAliasing->setCurrentIndex(0);
        }
    }

    this->itemSMP->setChecked( visionPrefs.smp );

    this->itemWantShaders->setChecked( visionPrefs.shaders );
    this->itemBumpMapping->setChecked( visionPrefs.bumpmapping );
    this->itemTerrainSplatting->setChecked( visionPrefs.terrainsplatting );
    this->itemWaterEffects->setChecked( visionPrefs.watereffects );
    this->itemAtmosphere->setChecked( visionPrefs.atmosphere );
    this->itemHighDynamicRange->setChecked( visionPrefs.hdr );
    changedWantShaders();

    this->itemAllowHardwareVertexAnimation->setChecked( visionPrefs.hardwarevertexanimation );
    this->itemAllowTwoSidedStencil->setChecked( visionPrefs.twosidedstencil );
    this->itemAllowHardwareTL->setChecked( visionPrefs.hardwaretl );
    this->itemAllowVBOs->setChecked( visionPrefs.vbo );
}

MainWindow::~MainWindow() {
}

void MainWindow::fillResolutions() {
    bool done = false;
    resolutions.clear();
#ifdef _WIN32
    for(int i=0;;i++) {
        DEVMODE devmode;
        if( !EnumDisplaySettings(NULL, i, &devmode) ) {
            // all done!
            break;
        }
        else {
            int width = devmode.dmPelsWidth;
            int height = devmode.dmPelsHeight;
            int bpp = devmode.dmBitsPerPel;
            if( bpp == required_bpp ) {
                ResolutionEntry entry(width, height, bpp);
                if( resolutions.indexOf(entry) == -1 ) {
                    resolutions.push_back(entry);
                }
            }
        }
    }
    if( resolutions.size() > 0 ) {
        done = true;
    }
    else {
        QMessageBox::warning(this, "Warning", "Can't find any 32-bit display settings");
    }
#endif
    if( !done ) {
        {
            ResolutionEntry entry(320, 200, 32);
            resolutions.push_back(entry);
        }
        {
            ResolutionEntry entry(640, 480, 32);
            resolutions.push_back(entry);
        }
        {
            ResolutionEntry entry(800, 600, 32);
            resolutions.push_back(entry);
        }
        {
            ResolutionEntry entry(1024, 600, 32);
            resolutions.push_back(entry);
        }
        {
            ResolutionEntry entry(1024, 768, 32);
            resolutions.push_back(entry);
        }
        {
            ResolutionEntry entry(1280, 800, 32);
            resolutions.push_back(entry);
        }
        {
            ResolutionEntry entry(1280, 1024, 32);
            resolutions.push_back(entry);
        }
        {
            ResolutionEntry entry(1440, 900, 32);
            resolutions.push_back(entry);
        }
        {
            ResolutionEntry entry(1680, 1050, 32);
            resolutions.push_back(entry);
        }
    }

    // TODO: sort entries
    foreach(ResolutionEntry entry, resolutions) {
        QVariant variant = qVariantFromValue(entry);
        itemResolution->addItem(entry.toString(), variant);
    }
}

void MainWindow::changedWantShaders() {
    bool effects_enabled = this->itemWantShaders->isChecked();
    this->itemBumpMapping->setEnabled(effects_enabled);
    this->itemTerrainSplatting->setEnabled(effects_enabled);
    this->itemWaterEffects->setEnabled(effects_enabled);
    this->itemAtmosphere->setEnabled(effects_enabled);
    this->itemHighDynamicRange->setEnabled(effects_enabled);
}

void MainWindow::clickedOkay() {
    this->clickedApply();
    this->close();
}

void MainWindow::clickedApply() {
    /*if( !VisionPrefs::createFolder() ) {
        QMessageBox::critical(this, "Error", "Failed to create application folder to store vision.config file");
        return;
    }*/
/*#ifdef _WIN32
    char full_filename[MAX_PATH] = "";
    if( !VisionPrefs::getSaveFolder(full_filename) ) {
        QMessageBox::critical(this, "Error", "Failed to determine application folder to store vision.config file");
        return;
    }
    mkdir(full_filename); // create if it doesn't exist
    PathAppendA(full_filename, config_filename);
    FILE *file = fopen(full_filename, "w");
#else
    FILE *file = fopen(config_filename, "w");
#endif*/
    FILE *file = NULL;
    char *folder_path = VisionPrefs::getSaveFolderPath(true);
    if( folder_path == NULL ) {
        QMessageBox::critical(this, "Error", "Failed to determine application folder to store vision.config file");
        return;
    }
    else if( *folder_path != '\0' ) {
        qDebug("Folder path: %s", folder_path);
        char *full_filename = new char[strlen(folder_path)+1+strlen(config_filename)+1];
        sprintf(full_filename, "%s/%s", folder_path, config_filename);
        qDebug("Full filename: %s", full_filename);
        file = fopen(full_filename, "w");
    }
    else {
        qDebug("Open locally: %s", config_filename);
        file = fopen(config_filename, "w");
    }
    delete [] folder_path;
    folder_path  = NULL;

    if( file == NULL ) {
        QMessageBox::critical(this, "Error", "Failed to open vision.config for writing");
        return;
    }
    const int version = 1;
    fprintf(file, "[version=%d]\n", version);
    fprintf(file, "[api=%s]\n", this->itemAPI->currentText().toLatin1().data() );
    //fprintf(file, "[resolution=%s]\n", this->comboBoxResolution->SelectedItem->ToString());
    QVariant variant = this->itemResolution->itemData( this->itemResolution->currentIndex() );
    ResolutionEntry entry = variant.value<ResolutionEntry>();
    fprintf(file, "[width=%d]\n", entry.width);
    fprintf(file, "[height=%d]\n", entry.height);
    fprintf(file, "[fullscreen=%d]\n", this->itemFullscreen->isChecked() ? 1 : 0);
    fprintf(file, "[multisample=%d]\n", VisionMultisampleValues[this->itemAntiAliasing->currentIndex()]);
    fprintf(file, "[smp=%d]\n", this->itemSMP->isChecked() ? 1 : 0);
    fprintf(file, "[shaders=%d]\n", this->itemWantShaders->isChecked() ? 1 : 0);
    fprintf(file, "[bumpmapping=%d]\n", this->itemBumpMapping->isChecked() ? 1 : 0);
    fprintf(file, "[terrainsplatting=%d]\n", this->itemTerrainSplatting->isChecked() ? 1 : 0);
    fprintf(file, "[watereffects=%d]\n", this->itemWaterEffects->isChecked() ? 1 : 0);
    fprintf(file, "[atmosphere=%d]\n", this->itemAtmosphere->isChecked() ? 1 : 0);
    fprintf(file, "[hdr=%d]\n", this->itemHighDynamicRange->isChecked() ? 1 : 0);
    fprintf(file, "[hardwarevertexanimation=%d]\n", this->itemAllowHardwareVertexAnimation->isChecked() ? 1 : 0);
    fprintf(file, "[twosidedstencil=%d]\n", this->itemAllowTwoSidedStencil->isChecked() ? 1 : 0);
    fprintf(file, "[hardwaretl=%d]\n", this->itemAllowHardwareTL->isChecked() ? 1 : 0);
    fprintf(file, "[vbo=%d]\n", this->itemAllowVBOs->isChecked() ? 1 : 0);
    //fprintf(file, "[=%d]\n", ? 1 : 0);
    fclose(file);
    return;
}

void MainWindow::clickedCancel() {
    this->close();
}
